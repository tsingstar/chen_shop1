/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50553
 Source Host           : 127.0.0.1:3306
 Source Schema         : qmgx

 Target Server Type    : MySQL
 Target Server Version : 50553
 File Encoding         : 65001

 Date: 08/09/2020 16:35:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qmgx_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_admin_log`;
CREATE TABLE `qmgx_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `user_id` int(11) NULL DEFAULT 0 COMMENT '操作有关会员的id或者商家得id（非必填）',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作介绍',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '操作时间',
  `money` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '余额变化',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 351 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_admin_log
-- ----------------------------
INSERT INTO `qmgx_admin_log` VALUES (1, 1, 0, '修改配置文件', 1596102649, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (2, 1, 0, '修改配置文件', 1596102987, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (3, 1, 0, '修改配置文件', 1596103559, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (4, 1, 0, '登录会员水心', 1596185275, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (5, 1, 0, '会员(1-水心)编辑成功！', 1596187859, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (6, 1, 0, '后台重置会员(水心)登录密码！', 1596187863, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (7, 1, 0, '会员(3-xuanfeng)编辑成功！', 1596190331, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (8, 1, 0, '会员(3-xuanfeng)编辑成功！', 1596150840, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (9, 1, 0, '会员(3-xuanfeng)编辑成功！', 1596150885, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (10, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":43200,\"time_end\":43800,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"atcnum\":\"0\",\"', 1596243352, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (11, 1, 0, '修改配置文件', 1596243459, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (12, 1, 0, '添加新闻', 1596245415, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (13, 1, 0, '添加新闻', 1596245476, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (14, 1, 0, '添加新闻', 1596245524, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (15, 1, 0, '登录会员xuanfeng', 1596442958, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (16, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":43200,\"time_end\":43800,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"botnum\":\"2\",\"', 1596461310, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (17, 1, 0, '编辑商品成功，id:1{\"title\":\"\\u706b\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"500\",\"price2\":\"1200\",\"time_start\":46800,\"time_end\":47400,\"point_start\":\"4\",\"point_end\":\"10\",\"daynum\":\"2\",\"daypercent\":\"12\",\"botnum\":\"20\"', 1596461321, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (18, 1, 0, '编辑商品成功，id:3{\"title\":\"\\u5730\\u7403\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"2901\",\"price2\":\"5200\",\"time_start\":50400,\"time_end\":51000,\"point_start\":\"10\",\"point_end\":\"6\",\"daynum\":\"3\",\"daypercent\":\"15\",\"botnum\":\"6\"', 1596461337, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (19, 1, 0, '编辑商品成功，id:7{\"title\":\"\\u91d1\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"1201\",\"price2\":\"2900\",\"time_start\":57600,\"time_end\":58200,\"point_start\":\"6\",\"point_end\":\"12\",\"daynum\":\"1\",\"daypercent\":\"7\",\"botnum\":\"5\",', 1596461348, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (20, 1, 0, '编辑商品成功，id:2{\"title\":\"\\u6d77\\u738b\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"5201\",\"price2\":\"7500\",\"time_start\":61200,\"time_end\":61800,\"point_start\":\"20\",\"point_end\":\"60\",\"daynum\":\"4\",\"daypercent\":\"20\",\"botn', 1596461362, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (21, 1, 0, '编辑商品成功，id:10{\"title\":\"\\u5929\\u738b\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"7501\",\"price2\":\"12000\",\"time_start\":64800,\"time_end\":65400,\"point_start\":\"30\",\"point_end\":\"70\",\"daynum\":\"2\",\"daypercent\":\"12\",\"bo', 1596461459, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (22, 1, 0, '编辑商品成功，id:8{\"title\":\"\\u571f\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"12001\",\"price2\":\"16000\",\"time_start\":0,\"time_end\":0,\"point_start\":\"40\",\"point_end\":\"50\",\"daynum\":\"8\",\"daypercent\":\"42\",\"botnum\":\"5\",\"atc', 1596461468, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (23, 1, 0, '编辑商品成功，id:11{\"title\":\"\\u6728\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"16001\",\"price2\":\"19000\",\"time_start\":0,\"time_end\":0,\"point_start\":\"50\",\"point_end\":\"0\",\"daynum\":\"5\",\"daypercent\":\"27\",\"botnum\":\"7\",\"atc', 1596461481, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (24, 1, 0, '登录会员xuanfeng', 1596462199, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (25, 1, 0, '登录会员xuanfeng', 1596466070, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (26, 1, 0, '编辑商城商品成功，id:23', 1596467345, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (27, 1, 0, '编辑商城商品成功，id:23', 1596467483, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (28, 1, 0, '编辑商城商品成功，id:24', 1596467494, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (29, 1, 0, '登录会员xuanfeng', 1596506735, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (30, 1, 0, '登录会员鹅卵石', 1596509743, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (31, 1, 0, '登录会员xuanfeng', 1596510581, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (32, 1, 0, '编辑商城商品成功，id:32', 1596526149, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (33, 1, 0, '编辑商城商品成功，id:26', 1596526805, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (34, 1, 0, '编辑商城商品成功，id:26', 1596526821, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (35, 1, 0, '修改配置文件', 1596533938, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (36, 1, 0, '修改配置文件', 1596533986, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (37, 1, 0, '登录会员水心', 1596534224, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (38, 1, 0, '会员(2-鹅卵石)编辑成功！', 1596534353, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (39, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":64200,\"time_end\":65400,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"botnum\":\"2\",\"', 1596534402, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (40, 1, 0, '登录会员水心', 1596534409, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (41, 1, 0, '会员(3-xuanfeng)编辑成功！', 1596534425, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (42, 1, 0, '修改配置文件', 1596534464, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (43, 1, 0, '登录会员水心', 1596534474, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (44, 1, 0, '登录会员水心', 1596534486, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (45, 1, 0, '登录会员水心', 1596534499, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (46, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":64500,\"time_end\":65400,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"botnum\":\"2\",\"', 1596534590, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (47, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":64320,\"time_end\":65400,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"botnum\":\"2\",\"', 1596534647, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (48, 1, 0, '指定订单(118)，applyid:23', 1596534723, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (49, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":64440,\"time_end\":65400,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"botnum\":\"2\",\"', 1596534824, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (50, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":64560,\"time_end\":65400,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"botnum\":\"2\",\"', 1596534850, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (51, 1, 0, '编辑商品成功，id:9{\"title\":\"\\u6c34\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"100\",\"price2\":\"499\",\"time_start\":43200,\"time_end\":43800,\"point_start\":\"2\",\"point_end\":\"40\",\"daynum\":\"1\",\"daypercent\":\"10\",\"botnum\":\"2\",\"', 1596534943, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (52, 1, 0, '编辑商品成功，id:1{\"title\":\"\\u706b\\u661f\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"500\",\"price2\":\"1200\",\"time_start\":64680,\"time_end\":65400,\"point_start\":\"4\",\"point_end\":\"10\",\"daynum\":\"2\",\"daypercent\":\"12\",\"botnum\":\"20\"', 1596534965, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (53, 1, 0, '指定订单(558)，applyid:24', 1596534989, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (54, 1, 0, '编辑新闻，id：3', 1596535089, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (55, 1, 0, '登录会员xuanfeng', 1596535965, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (56, 1, 0, '登录会员水心', 1596536435, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (57, 1, 0, '删除订单，id:5', 1596543603, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (58, 1, 0, '删除待发放bot记录，id:5', 1596543603, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (59, 1, 0, '删除订单，id:6', 1596543609, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (60, 1, 0, '删除待发放bot记录，id:6', 1596543609, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (61, 1, 0, '登录会员xuanfeng', 1596543619, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (62, 1, 0, '删除会员,id：1-用户名:水心(16851620656)', 1596543708, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (63, 1, 0, '删除会员,id：3-用户名:xuanfeng(15376068375)', 1596543717, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (64, 1, 0, '删除会员,id：2-用户名:鹅卵石(17061535661)', 1596543722, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (65, 1, 0, '会员人工智能,手机号18888888888添加成功', 1596543992, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (66, 1, 0, '登录会员人工智能', 1596544461, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (67, 1, 0, '会员益达,手机号17009125357添加成功', 1596544717, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (68, 1, 0, '会员花无百日红 006,手机号17013226770添加成功', 1596544897, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (69, 1, 0, '会员花无百日红016,手机号17009125358添加成功', 1596545468, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (70, 1, 0, '登录会员益达', 1596545594, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (71, 1, 0, '会员厚德载物5号,手机号17015568091添加成功', 1596545775, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (72, 1, 0, '会员厚德载物28号,手机号17015568075添加成功', 1596545946, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (73, 1, 0, '会员益哥,手机号16676238055添加成功', 1596546294, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (74, 1, 0, '会员益哥05,手机号16676238051添加成功', 1596546424, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (75, 1, 0, '会员蔚蓝045,手机号18056753356添加成功', 1596546616, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (76, 1, 0, '删除会员,id：12-用户名:蔚蓝045(18056753356)', 1596546735, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (77, 1, 0, '会员金帆038,手机号18097883326添加成功', 1596546814, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (78, 1, 0, '会员刘帆,手机号18922336751添加成功', 1596546947, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (79, 1, 0, '会员万国,手机号19088962350添加成功', 1596547111, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (80, 1, 0, '修改配置文件', 1596547350, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (81, 1, 0, '登录会员花无百日红 006', 1596547368, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (82, 1, 0, '登录会员刘帆', 1596547378, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (83, 1, 0, '编辑商品成功，id:9{\"title\":\"1\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/ef46700857fe6b2b8cdead215dec4e91.png\",\"price1\":\"300\",\"price2\":\"800\",\"time_start\":43200,\"time_end\":43800,\"point_start\":\"3\",\"point_end\":\"40\",\"daynum\":\"4\",\"daypercent\":\"20\",\"', 1596548129, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (84, 1, 0, '编辑商品成功，id:1{\"title\":\"2\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/553b1b1473c4e33ccfdfbfc92dc5ead3.png\",\"price1\":\"801\",\"price2\":\"1600\",\"time_start\":46800,\"time_end\":47400,\"point_start\":\"5\",\"point_end\":\"10\",\"daynum\":\"2\",\"daypercent\":\"9\",\"', 1596548211, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (85, 1, 0, '编辑商品成功，id:3{\"title\":\"3\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/0ef8a3ee081609529b64cff1238335ee.jpg\",\"price1\":\"3201\",\"price2\":\"5200\",\"time_start\":50400,\"time_end\":51000,\"point_start\":\"15\",\"point_end\":\"6\",\"daynum\":\"3\",\"daypercent\":\"12\"', 1596548333, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (86, 1, 0, '编辑商品成功，id:7{\"title\":\"4\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2136d32e6e40dd41bec861c654222c92.png\",\"price1\":\"1601\",\"price2\":\"3200\",\"time_start\":54000,\"time_end\":54600,\"point_start\":\"8\",\"point_end\":\"12\",\"daynum\":\"1\",\"daypercent\":\"5\",', 1596548408, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (87, 1, 0, '编辑商品成功，id:10{\"title\":\"6\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/13509f5daa47fe25fb8285cd130362d0.png\",\"price1\":\"8001\",\"price2\":\"9800\",\"time_start\":61200,\"time_end\":61800,\"point_start\":\"25\",\"point_end\":\"70\",\"daynum\":\"2\",\"daypercent\":\"8', 1596548717, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (88, 1, 0, '编辑商品成功，id:8{\"title\":\"7\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"9801\",\"price2\":\"11800\",\"time_start\":64800,\"time_end\":65400,\"point_start\":\"30\",\"point_end\":\"50\",\"daynum\":\"5\",\"daypercent\":\"1', 1596548815, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (89, 1, 0, '编辑商品成功，id:8{\"title\":\"7\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/89ee14bf1e63719aded68c889f40a838.png\",\"price1\":\"9801\",\"price2\":\"11800\",\"time_start\":64800,\"time_end\":65400,\"point_start\":\"30\",\"point_end\":\"50\",\"daynum\":\"5\",\"daypercent\":\"1', 1596548846, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (90, 1, 0, '编辑商品成功，id:11{\"title\":\"8\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"16001\",\"price2\":\"19000\",\"time_start\":68400,\"time_end\":69000,\"point_start\":\"40\",\"point_end\":\"0\",\"daynum\":\"7\",\"daypercent\":\"', 1596549003, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (91, 1, 0, '编辑商品成功，id:11{\"title\":\"8\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/ac83cb3bac238e94a5cf3bf7ff90adc8.png\",\"price1\":\"11801\",\"price2\":\"13800\",\"time_start\":68400,\"time_end\":69000,\"point_start\":\"40\",\"point_end\":\"0\",\"daynum\":\"7\",\"daypercent\":\"', 1596549109, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (92, 1, 0, '添加产品{\"title\":\"9\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/083e7440ec75b9752b8c1aa0c977c9b6.png\",\"price1\":\"13801\",\"price2\":\"15800\",\"time_start\":72000,\"time_end\":72600,\"point_start\":\"50\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"30\",\"', 1596549217, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (93, 1, 0, '编辑商品成功，id:12{\"title\":\"9\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/083e7440ec75b9752b8c1aa0c977c9b6.png\",\"price1\":\"13801\",\"price2\":\"15800\",\"time_start\":72000,\"time_end\":72600,\"point_start\":\"50\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent', 1596549246, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (94, 1, 0, '添加产品{\"title\":\"10\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/675d96ac893b8dccda8bf3526bc23eb4.png\",\"price1\":\"15801\",\"price2\":\"18000\",\"time_start\":75600,\"time_end\":76200,\"point_start\":\"60\",\"point_end\":\"999\",\"daynum\":\"8\",\"daypercent\":\"26\",\"', 1596549316, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (95, 1, 0, '编辑商品成功，id:13{\"title\":\"10\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/675d96ac893b8dccda8bf3526bc23eb4.png\",\"price1\":\"15801\",\"price2\":\"18000\",\"time_start\":75600,\"time_end\":76200,\"point_start\":\"60\",\"point_end\":\"999\",\"daynum\":\"8\",\"daypercent', 1596549326, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (96, 1, 0, '编辑商品成功，id:2{\"title\":\"5\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/5a2cae6d302a72bf3fb45fd74f6af98d.png\",\"price1\":\"5201\",\"price2\":\"8000\",\"time_start\":57600,\"time_end\":58200,\"point_start\":\"20\",\"point_end\":\"60\",\"daynum\":\"4\",\"daypercent\":\"15', 1596549437, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (97, 1, 0, '编辑商品成功，id:7{\"title\":\"3\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2136d32e6e40dd41bec861c654222c92.png\",\"price1\":\"1601\",\"price2\":\"3200\",\"time_start\":54000,\"time_end\":54600,\"point_start\":\"8\",\"point_end\":\"12\",\"daynum\":\"1\",\"daypercent\":\"5\",', 1596549453, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (98, 1, 0, '编辑商品成功，id:3{\"title\":\"4\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/0ef8a3ee081609529b64cff1238335ee.jpg\",\"price1\":\"3201\",\"price2\":\"5200\",\"time_start\":50400,\"time_end\":51000,\"point_start\":\"15\",\"point_end\":\"6\",\"daynum\":\"3\",\"daypercent\":\"12\"', 1596549461, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (99, 1, 0, '编辑商城商品成功，id:23', 1596549583, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (100, 1, 0, '编辑商城商品成功，id:24', 1596549615, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (101, 1, 0, '编辑商城商品成功，id:25', 1596549643, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (102, 1, 0, '编辑商城商品成功，id:26', 1596549667, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (103, 1, 0, '编辑商城商品成功，id:27', 1596549690, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (104, 1, 0, '编辑商城商品成功，id:28', 1596549695, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (105, 1, 0, '编辑商城商品成功，id:29', 1596549700, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (106, 1, 0, '编辑商城商品成功，id:30', 1596549704, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (107, 1, 0, '编辑商城商品成功，id:31', 1596549708, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (108, 1, 0, '登录会员人工智能', 1596549719, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (109, 1, 0, '编辑商品成功，id:3{\"title\":\"4\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/83b2e829c3c919c4820dc9ad4e1aa004.png\",\"price1\":\"3201\",\"price2\":\"5200\",\"time_start\":50400,\"time_end\":51000,\"point_start\":\"15\",\"point_end\":\"6\",\"daynum\":\"3\",\"daypercent\":\"12\"', 1596549825, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (110, 1, 0, '添加产品{\"title\":\"11\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/331566d7a93823379c57bddffd9c9cb1.png\",\"price1\":\"2000\",\"price2\":\"5000\",\"time_start\":79800,\"time_end\":79920,\"point_start\":\"1\",\"point_end\":\"999\",\"daynum\":\"3\",\"daypercent\":\"10\",\"atc', 1596550116, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (111, 1, 0, '会员(15-万国)编辑成功！', 1596550175, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (112, 1, 0, '指定订单(3000)，applyid:27', 1596550186, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (113, 1, 0, '登录会员人工智能', 1596550654, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (114, 1, 0, '编辑商品成功，id:2{\"title\":\"5\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/fc4f8e3c9f4fc3d6f5cb6ea6fb7602a0.png\",\"price1\":\"5201\",\"price2\":\"8000\",\"time_start\":57600,\"time_end\":58200,\"point_start\":\"20\",\"point_end\":\"60\",\"daynum\":\"4\",\"daypercent\":\"15', 1596550835, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (115, 1, 0, '登录会员人工智能', 1596550841, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (116, 1, 0, '编辑商品成功，id:2{\"title\":\"5\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/59553ac2c92d10fac843f691f20e7105.png\",\"price1\":\"5201\",\"price2\":\"8000\",\"time_start\":57600,\"time_end\":58200,\"point_start\":\"20\",\"point_end\":\"60\",\"daynum\":\"4\",\"daypercent\":\"15', 1596550881, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (117, 1, 0, '删除商品，id：14', 1596551013, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (118, 1, 0, '添加产品{\"title\":\"\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/dedb7055bbfca45f7f895cdb45ca5bea.png\",\"price1\":\"3000\",\"price2\":\"5000\",\"time_start\":80880,\"time_end\":81120,\"point_start\":\"3\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"7\",\"atcnum\":\"0\"', 1596551065, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (119, 1, 0, '编辑商品成功，id:15{\"title\":\"\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/dedb7055bbfca45f7f895cdb45ca5bea.png\",\"price1\":\"3000\",\"price2\":\"5000\",\"time_start\":80880,\"time_end\":81120,\"point_start\":\"3\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"7\",\"bot', 1596551073, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (120, 1, 0, '删除订单，id:7', 1596551098, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (121, 1, 0, '删除待发放bot记录，id:7', 1596551098, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (122, 1, 0, '指定订单(4000)，applyid:35', 1596551195, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (123, 1, 0, '登录会员万国', 1596551308, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (124, 1, 0, '添加产品{\"title\":\"dadadad\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/9939269944d23d26bb69af7b545b3a78.png\",\"price1\":\"5000\",\"price2\":\"10000\",\"time_start\":81360,\"time_end\":81600,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"30\",\"atcnum\":\"0\",\"isshow\"', 1596551426, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (125, 1, 0, '编辑商品成功，id:16{\"title\":\"dadadad\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/9939269944d23d26bb69af7b545b3a78.png\",\"price1\":\"5000\",\"price2\":\"10000\",\"time_start\":81360,\"time_end\":81600,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"30\",\"botnum\":\"222', 1596551437, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (126, 1, 0, '删除商品，id：15', 1596551617, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (127, 1, 0, '删除商品，id：16', 1596551620, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (128, 1, 0, '删除新闻，id:3', 1596551770, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (129, 1, 0, '删除新闻，id:1', 1596551773, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (130, 1, 0, '编辑新闻，id：2', 1596551783, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (131, 1, 0, '添加产品{\"title\":\"\\u6d4b\\u8bd5\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/80ade1644802d94fff491ee1cd5445f9.jpg\",\"price1\":\"100\",\"price2\":\"200\",\"time_start\":81600,\"time_end\":81900,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"1\",\"daypercent\":\"1\",\"atcnum\":\"0\",\"isshow\"', 1596551853, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (132, 1, 0, '编辑商品成功，id:17{\"title\":\"\\u6d4b\\u8bd5\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/80ade1644802d94fff491ee1cd5445f9.jpg\",\"price1\":\"100\",\"price2\":\"200\",\"time_start\":81600,\"time_end\":81900,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"1\",\"daypercent\":\"1\",\"botnum\":\"2\",', 1596551860, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (133, 1, 0, '会员(4-人工智能)编辑成功！', 1596551943, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (134, 1, 0, '指定订单(111)，applyid:41', 1596551954, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (135, 1, 0, '会员(5-益达)编辑成功！', 1596551968, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (136, 1, 0, '指定订单(120)，applyid:40', 1596551978, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (137, 1, 0, '登录会员花无百日红016', 1596551979, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (138, 1, 0, '抹除订单，applyid:40', 1596551983, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (139, 1, 0, '删除商品，id：17', 1596552275, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (140, 1, 0, '删除订单，id:9', 1596553022, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (141, 1, 0, '删除待发放bot记录，id:9', 1596553022, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (142, 1, 0, '删除订单，id:10', 1596553025, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (143, 1, 0, '删除待发放bot记录，id:10', 1596553025, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (144, 1, 0, '编辑新闻，id：2', 1596586584, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (145, 1, 0, '登录会员人工智能', 1596586591, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (146, 1, 0, '添加产品{\"title\":\"0\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/44ff551171c95ce2216b7dbc54be2b66.png\",\"price1\":\"2000\",\"price2\":\"3000\",\"time_start\":32700,\"time_end\":33300,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"30\",\"atc', 1596589142, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (147, 1, 0, '登录会员厚德载物5号', 1596589152, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (148, 1, 0, '登录会员花无百日红 006', 1596589168, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (149, 1, 0, '登录会员刘帆', 1596589184, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (150, 1, 0, '登录会员益哥05', 1596589195, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (151, 1, 0, '指定订单(2200)，applyid:54', 1596589237, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (152, 1, 0, '指定订单(2300)，applyid:51', 1596589252, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (153, 1, 0, '抹除订单，applyid:51', 1596589256, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (154, 1, 0, '登录会员人工智能', 1596589284, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (155, 1, 0, '编辑商城商品成功，id:25', 1596589380, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (156, 1, 0, '登录会员人工智能', 1596589533, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (157, 1, 0, '登录会员益哥05', 1596589553, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (158, 1, 0, '登录会员益哥05', 1596589777, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (159, 1, 0, '编辑商品成功，id:9{\"title\":\"1\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/ef46700857fe6b2b8cdead215dec4e91.png\",\"price1\":\"300\",\"price2\":\"800\",\"time_start\":43200,\"time_end\":43800,\"point_start\":\"3\",\"point_end\":\"40\",\"daynum\":\"4\",\"daypercent\":\"20\",\"', 1596590006, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (160, 1, 0, '编辑商品成功，id:1{\"title\":\"2\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/553b1b1473c4e33ccfdfbfc92dc5ead3.png\",\"price1\":\"801\",\"price2\":\"1600\",\"time_start\":46800,\"time_end\":47400,\"point_start\":\"5\",\"point_end\":\"10\",\"daynum\":\"2\",\"daypercent\":\"9\",\"', 1596590017, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (161, 1, 0, '编辑商品成功，id:3{\"title\":\"4\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/83b2e829c3c919c4820dc9ad4e1aa004.png\",\"price1\":\"3201\",\"price2\":\"5200\",\"time_start\":50400,\"time_end\":51000,\"point_start\":\"15\",\"point_end\":\"6\",\"daynum\":\"3\",\"daypercent\":\"12\"', 1596590028, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (162, 1, 0, '编辑商品成功，id:3{\"title\":\"4\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/83b2e829c3c919c4820dc9ad4e1aa004.png\",\"price1\":\"3201\",\"price2\":\"5200\",\"time_start\":50400,\"time_end\":51000,\"point_start\":\"15\",\"point_end\":\"6\",\"daynum\":\"3\",\"daypercent\":\"12\"', 1596590044, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (163, 1, 0, '编辑商品成功，id:7{\"title\":\"3\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2136d32e6e40dd41bec861c654222c92.png\",\"price1\":\"1601\",\"price2\":\"3200\",\"time_start\":54000,\"time_end\":54600,\"point_start\":\"8\",\"point_end\":\"12\",\"daynum\":\"1\",\"daypercent\":\"5\",', 1596590052, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (164, 1, 0, '编辑商品成功，id:2{\"title\":\"5\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/59553ac2c92d10fac843f691f20e7105.png\",\"price1\":\"5201\",\"price2\":\"8000\",\"time_start\":57600,\"time_end\":58200,\"point_start\":\"20\",\"point_end\":\"60\",\"daynum\":\"4\",\"daypercent\":\"15', 1596590075, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (165, 1, 0, '编辑商品成功，id:2{\"title\":\"5\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/59553ac2c92d10fac843f691f20e7105.png\",\"price1\":\"5201\",\"price2\":\"8000\",\"time_start\":57600,\"time_end\":58200,\"point_start\":\"20\",\"point_end\":\"60\",\"daynum\":\"4\",\"daypercent\":\"15', 1596590084, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (166, 1, 0, '编辑商品成功，id:18{\"title\":\"0\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/44ff551171c95ce2216b7dbc54be2b66.png\",\"price1\":\"2000\",\"price2\":\"3000\",\"time_start\":36000,\"time_end\":36900,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"', 1596592681, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (167, 1, 0, '登录会员花无百日红 006', 1596592699, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (168, 1, 0, '编辑商品成功，id:18{\"title\":\"0\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/44ff551171c95ce2216b7dbc54be2b66.png\",\"price1\":\"2000\",\"price2\":\"3000\",\"time_start\":36300,\"time_end\":36900,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"', 1596592792, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (169, 1, 0, '编辑商品成功，id:9{\"title\":\"1\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/ef46700857fe6b2b8cdead215dec4e91.png\",\"price1\":\"300\",\"price2\":\"800\",\"time_start\":36180,\"time_end\":36600,\"point_start\":\"3\",\"point_end\":\"40\",\"daynum\":\"4\",\"daypercent\":\"20\",\"', 1596592892, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (170, 1, 0, '编辑商品成功，id:18{\"title\":\"0\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/44ff551171c95ce2216b7dbc54be2b66.png\",\"price1\":\"2000\",\"price2\":\"3000\",\"time_start\":36600,\"time_end\":36900,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"', 1596592926, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (171, 1, 0, '指定订单(380)，applyid:14599', 1596592947, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (172, 1, 0, '指定订单(380)，applyid:14598', 1596592970, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (173, 1, 0, '登录会员人工智能', 1596593060, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (174, 1, 0, '登录会员人工智能', 1596593204, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (175, 1, 0, '指定订单(2888)，applyid:28065', 1596593288, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (176, 1, 0, '编辑商品成功，id:9{\"title\":\"1\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/ef46700857fe6b2b8cdead215dec4e91.png\",\"price1\":\"300\",\"price2\":\"800\",\"time_start\":43200,\"time_end\":43800,\"point_start\":\"3\",\"point_end\":\"40\",\"daynum\":\"4\",\"daypercent\":\"20\",\"', 1596594513, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (177, 1, 0, '编辑商品成功，id:18{\"title\":\"0\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/44ff551171c95ce2216b7dbc54be2b66.png\",\"price1\":\"2000\",\"price2\":\"3000\",\"time_start\":36600,\"time_end\":36900,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"10\",\"daypercent\":\"', 1596594566, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (178, 1, 0, '会员(15-万国)编辑成功！', 1596594634, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (179, 1, 0, '删除订单，id:22', 1596595555, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (180, 1, 0, '删除待发放bot记录，id:22', 1596595555, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (181, 1, 0, '删除订单，id:20', 1596595559, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (182, 1, 0, '删除待发放bot记录，id:20', 1596595559, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (183, 1, 0, '删除订单，id:19', 1596595563, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (184, 1, 0, '删除待发放bot记录，id:19', 1596595563, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (185, 1, 0, '删除订单，id:18', 1596595565, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (186, 1, 0, '删除待发放bot记录，id:18', 1596595565, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (187, 1, 0, '删除订单，id:17', 1596595568, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (188, 1, 0, '删除待发放bot记录，id:17', 1596595568, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (189, 1, 0, '删除订单，id:16', 1596595572, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (190, 1, 0, '删除待发放bot记录，id:16', 1596595572, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (191, 1, 0, '删除订单，id:15', 1596595574, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (192, 1, 0, '删除待发放bot记录，id:15', 1596595574, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (193, 1, 0, '删除订单，id:13', 1596595577, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (194, 1, 0, '删除待发放bot记录，id:13', 1596595577, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (195, 1, 0, '删除订单，id:14', 1596595581, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (196, 1, 0, '删除待发放bot记录，id:14', 1596595581, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (197, 1, 0, '修改配置文件', 1596596047, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (198, 1, 0, '登录会员花无百日红016', 1596596498, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (199, 1, 0, '登录会员人工智能', 1596596695, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (200, 1, 0, '登录会员人工智能', 1596596742, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (201, 1, 0, '登录会员花无百日红016', 1596596788, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (202, 1, 0, '登录会员人工智能', 1596596844, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (203, 1, 0, '会员(16-北斗001)编辑成功！', 1596597669, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (204, 1, 0, '会员(20-不二家)编辑成功！', 1596597677, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (205, 1, 0, '登录会员不二家', 1596597751, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (206, 1, 0, '登录会员不二家', 1596597799, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (207, 1, 0, '会员(19-风生水起)编辑成功！', 1596598653, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (208, 1, 0, '登录会员人工智能', 1596605843, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (209, 1, 0, '登录会员万国', 1596606181, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (210, 1, 0, '登录会员看谁跑得快', 1596609697, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (211, 1, 0, '删除订单，id:23', 1596609735, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (212, 1, 0, '删除待发放bot记录，id:23', 1596609735, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (213, 1, 0, '删除商品，id：18', 1596612579, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (214, 1, 0, '添加产品{\"title\":\"\\u6d4b\\u8bd5\\u573a\\u6b21\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2461baf29a68a67e0f49191cadcaab0f.jpg\",\"price1\":\"1000\",\"price2\":\"3000\",\"time_start\":56100,\"time_end\":56460,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"21\",\"botnu', 1596612664, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (215, 1, 0, '登录会员厚德载物5号', 1596612736, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (216, 1, 0, '登录会员金帆038', 1596612758, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (217, 1, 0, '登录会员花无百日红016', 1596612776, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (218, 1, 0, '指定订单(1122)，applyid:7', 1596612813, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (219, 1, 0, '指定订单(1550)，applyid:5', 1596612842, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (220, 1, 0, '抹除订单，applyid:5', 1596612855, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (221, 1, 0, '登录会员人工智能', 1596613099, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (222, 1, 0, '登录会员益达', 1596613160, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (223, 1, 0, '修改配置文件', 1596613199, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (224, 1, 0, '编辑商品成功，id:19{\"title\":\"\\u6d4b\\u8bd5\\u573a\\u6b21\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2461baf29a68a67e0f49191cadcaab0f.jpg\",\"price1\":\"1000\",\"price2\":\"3000\",\"time_start\":57420,\"time_end\":57660,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"21', 1596614133, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (225, 1, 0, '指定订单(2222)，applyid:10', 1596614211, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (226, 1, 0, '编辑商品成功，id:19{\"title\":\"\\u6d4b\\u8bd5\\u573a\\u6b21\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2461baf29a68a67e0f49191cadcaab0f.jpg\",\"price1\":\"1000\",\"price2\":\"3000\",\"time_start\":57600,\"time_end\":58020,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"21', 1596614340, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (227, 1, 0, '指定订单(1888)，applyid:11', 1596614367, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (228, 1, 0, '编辑商品成功，id:19{\"title\":\"\\u6d4b\\u8bd5\\u573a\\u6b21\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2461baf29a68a67e0f49191cadcaab0f.jpg\",\"price1\":\"1000\",\"price2\":\"3000\",\"time_start\":57960,\"time_end\":58140,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"21', 1596614746, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (229, 1, 0, '编辑商品成功，id:19{\"title\":\"\\u6d4b\\u8bd5\\u573a\\u6b21\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2461baf29a68a67e0f49191cadcaab0f.jpg\",\"price1\":\"1000\",\"price2\":\"3000\",\"time_start\":58080,\"time_end\":58260,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"21', 1596614795, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (230, 1, 0, '指定订单(1666)，applyid:15', 1596614814, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (231, 1, 0, '登录会员人工智能', 1596614902, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (232, 1, 0, '编辑新闻，id：2', 1596616661, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (233, 1, 0, '编辑新闻，id：2', 1596616693, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (234, 1, 0, '添加银行', 1596616719, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (235, 1, 0, '登录会员人工智能', 1596616723, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (236, 1, 0, '编辑新闻，id：2', 1596616765, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (237, 1, 0, '编辑商品成功，id:19{\"title\":\"\\u6d4b\\u8bd5\\u573a\\u6b21\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2461baf29a68a67e0f49191cadcaab0f.jpg\",\"price1\":\"1000\",\"price2\":\"3000\",\"time_start\":60300,\"time_end\":60720,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"21', 1596616802, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (238, 1, 0, '会员(14-刘帆)编辑成功！', 1596616958, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (239, 1, 0, '指定订单(1200)，applyid:16', 1596616971, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (240, 1, 0, '抹除订单，applyid:16', 1596616974, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (241, 1, 0, '会员(14-刘帆)编辑成功！', 1596616997, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (242, 1, 0, '登录会员花无百日红 006', 1596617029, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (243, 1, 0, '登录会员益哥', 1596617040, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (244, 1, 0, '登录会员金帆038', 1596617058, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (245, 1, 0, '指定订单，预约id:16;订单id：11', 1596617092, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (246, 1, 0, '登录会员刘帆', 1596617371, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (247, 1, 0, '登录会员益达', 1596617399, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (248, 1, 0, '登录会员xing2', 1596623455, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (249, 1, 0, '登录会员王哈哈', 1596623582, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (250, 1, 0, '登录会员王哈哈', 1596623595, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (251, 1, 0, '登录会员人工智能', 1596623893, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (252, 1, 0, '登录会员益哥', 1596633835, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (253, 1, 0, '编辑商品成功，id:19{\"title\":\"\\u6d4b\\u8bd5\\u573a\\u6b21\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/2461baf29a68a67e0f49191cadcaab0f.jpg\",\"price1\":\"1000\",\"price2\":\"3000\",\"time_start\":77280,\"time_end\":77580,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"21', 1596633987, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (254, 1, 0, '登录会员益哥', 1596633994, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (255, 1, 0, '指定订单(2000)，applyid:1', 1596634010, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (256, 1, 0, '修改配置文件', 1596634022, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (257, 1, 0, '登录会员人工智能', 1596634135, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (258, 1, 0, '登录会员木木木', 1596644292, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (259, 1, 0, '登录会员风生水起', 1596644352, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (260, 1, 0, '登录会员岁月静好1', 1596644406, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (261, 1, 0, '登录会员益哥', 1596671468, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (262, 1, 0, '删除订单，id:1', 1596671870, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (263, 1, 0, '删除待发放bot记录，id:1', 1596671870, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (264, 1, 0, '删除订单，id:2', 1596671873, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (265, 1, 0, '删除待发放bot记录，id:2', 1596671873, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (266, 1, 0, '添加产品{\"title\":\"\\u6d4b\\u8bd5\\u673a\\u5668\\u4eba\\u52ff\\u62cd\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/b08c5601bafb6b5add2c017da7152210.png\",\"price1\":\"0\",\"price2\":\"0\",\"time_start\":31620,\"time_end\":31980,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"1\",\"daypercent\"', 1596674631, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (267, 1, 0, '登录会员万国', 1596674651, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (268, 1, 0, '登录会员益哥', 1596674693, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (269, 1, 0, '登录会员花无百日红 006', 1596674709, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (270, 1, 0, '编辑商品成功，id:20{\"title\":\"\\u6d4b\\u8bd5\\u673a\\u5668\\u4eba\\u52ff\\u62cd\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/b08c5601bafb6b5add2c017da7152210.png\",\"price1\":\"100\",\"price2\":\"300\",\"time_start\":31620,\"time_end\":31980,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"1\",', 1596674743, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (271, 1, 0, '删除商品，id：20', 1596674756, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (272, 1, 0, '删除商品，id：19', 1596674760, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (273, 1, 0, '添加产品{\"title\":\"\\u6d4b\\u8bd5\\u673a\\u5668\\u4eba\\u52ff\\u62cd\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/970d7fbc8d31725b03b027bbe2339c65.png\",\"price1\":\"200\",\"price2\":\"500\",\"time_start\":31800,\"time_end\":32100,\"point_start\":\"2\",\"point_end\":\"999\",\"daynum\":\"1\",\"dayperc', 1596674799, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (274, 1, 0, '登录会员益哥', 1596674828, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (275, 1, 0, '登录会员花无百日红 006', 1596674846, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (276, 1, 0, '登录会员厚德载物5号', 1596674860, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (277, 1, 0, '指定订单(220)，applyid:9', 1596674880, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (278, 1, 0, '登录会员益哥', 1596678813, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (279, 1, 0, '登录会员厚德载物5号', 1596678847, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (280, 1, 0, '登录会员人工智能', 1596678886, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (281, 1, 0, '登录会员不二家', 1596679775, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (282, 1, 0, '登录会员不二家', 1596679797, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (283, 1, 0, '登录会员人工智能', 1596683344, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (284, 1, 0, '登录会员a123456', 1596686194, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (285, 1, 0, '登录会员风生水起', 1596686275, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (286, 1, 0, '登录会员人工智能', 1596694172, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (287, 1, 0, '登录会员哦豁', 1596708580, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (288, 1, 0, '登录会员哦豁', 1596708686, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (289, 1, 0, '编辑新闻，id：2', 1596709605, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (290, 1, 0, '会员(160-天怒)编辑成功！', 1596713971, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (291, 1, 0, '登录会员风生水起', 1596715936, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (292, 1, 0, '登录会员a123456', 1596716011, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (293, 1, 0, '登录会员s141', 1596720065, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (294, 1, 0, '登录会员刘帆', 1596758373, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (295, 1, 0, '登录会员金帆038', 1596758386, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (296, 1, 0, '登录会员益哥', 1596758398, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (297, 1, 0, '登录会员花无百日红 006', 1596758415, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (298, 1, 0, '登录会员厚德载物5号', 1596758569, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (299, 1, 0, '指定订单(210)，applyid:33', 1596759071, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (300, 1, 0, '登录会员厚德载物5号', 1596759083, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (301, 1, 0, '登录会员益哥', 1596759126, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (302, 1, 0, '登录会员厚德载物5号', 1596761772, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (303, 1, 0, '登录会员厚德载物5号', 1596762273, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (304, 1, 0, '登录会员人工智能', 1598979271, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (305, 1, 0, '后台重置会员(人工智能)登录密码！', 1599141490, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (306, 1, 0, '修改配置文件', 1599142199, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (307, 1, 0, '编辑商品成功，id:9{\"title\":\"1\\u53f7\\u673a\\u5668\\u4eba\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/ef46700857fe6b2b8cdead215dec4e91.png\",\"price1\":\"300\",\"price2\":\"800\",\"time_start\":43200,\"time_end\":43800,\"point_start\":\"3\",\"point_end\":\"40\",\"daynum\":\"4\",\"daypercent\":\"20\",\"', 1599145869, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (308, 1, 0, '添加产品{\"title\":\"\\u65b0\\u7684\\u5c0f\\u53ef\\u7231\",\"img_url\":\"http:\\/\\/img.yizitao.cn\\/3e15a0bb481010671706c2288bff97f4.png\",\"price1\":\"10000\",\"price2\":\"12000\",\"time_start\":46800,\"time_end\":54000,\"point_start\":\"10\",\"point_end\":\"999\",\"daynum\":\"4\",\"daypercent\":\"1', 1599145945, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (309, 1, 0, '编辑新闻，id：2', 1599146650, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (310, 1, 0, '编辑新闻，id：2', 1599146656, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (311, 1, 0, '添加新闻', 1599146675, 0.00, 'shandong');
INSERT INTO `qmgx_admin_log` VALUES (312, 1, 0, '删除-分类-2', 1599224282, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (313, 1, 0, '删除-分类-8', 1599224290, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (314, 1, 0, '添加-分类-14', 1599224311, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (315, 1, 0, '添加-分类-15', 1599224349, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (316, 1, 0, '添加-商品-3', 1599225831, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (317, 1, 0, '登录会员人工智能', 1599228191, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (318, 1, 0, '修改配置文件', 1599235158, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (319, 1, 0, '会员(4-人工智能)编辑成功！', 1599235234, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (320, 1, 0, '修改配置文件', 1599314475, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (321, 1, 0, '登录会员人工智能', 1599326242, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (322, 1, 0, '登录会员张晓爬', 1599326406, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (323, 1, 0, '登录会员花无百日红 006', 1599331930, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (324, 1, 0, '登录会员厚德载物5号', 1599332289, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (325, 1, 0, '后台设置订单id:4是否可售', 1599334149, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (326, 1, 0, '后台设置订单id:4是否可售', 1599334154, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (327, 1, 0, '登录会员人工智能', 1599334225, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (328, 1, 0, '登录会员厚德载物5号', 1599336675, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (329, 1, 0, '登录会员人工智能', 1599354581, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (330, 1, 0, '登录会员沙机管速你', 1599356016, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (331, 1, 0, '登录会员hhh', 1599356127, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (332, 1, 0, '登录会员人工智能', 1599368995, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (333, 1, 0, '会员(256-斯留啡我)编辑成功！', 1599375701, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (334, 1, 0, '添加-幻灯片-3', 1599487103, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (335, 1, 0, '添加-幻灯片-4', 1599487108, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (336, 1, 0, '添加-幻灯片-5', 1599487140, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (337, 1, 0, '编辑-幻灯片-5', 1599487161, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (338, 1, 0, '添加-导航-1', 1599487254, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (339, 1, 0, '设置-商品-3-不推荐', 1599488045, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (340, 1, 0, '设置-商品-3-首页推荐', 1599488072, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (341, 1, 0, '设置-商品-2-不推荐', 1599488077, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (342, 1, 0, '设置-商品-2-首页推荐', 1599488093, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (343, 1, 0, '设置-商品-2-不推荐', 1599488094, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (344, 1, 0, '设置-商品-2-首页推荐', 1599489793, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (345, 1, 0, '删除-幻灯片-4', 1599489936, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (346, 1, 0, '删除-幻灯片-3', 1599489940, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (347, 1, 0, '删除-幻灯片-2', 1599489945, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (348, 1, 0, '删除-幻灯片-1', 1599489948, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (349, 1, 0, '编辑-导航-1', 1599526654, 0.00, 'admin');
INSERT INTO `qmgx_admin_log` VALUES (350, 1, 0, '确认发货-订单-3成功', 1599553035, 0.00, 'admin');

-- ----------------------------
-- Table structure for qmgx_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_admin_role`;
CREATE TABLE `qmgx_admin_role`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `authority` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限列表（以逗号分隔）',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述信息',
  `is_valid` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uin_role_name`(`name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员角色表。' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qmgx_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_admin_user`;
CREATE TABLE `qmgx_admin_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员名称',
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `salt` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_root` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为超级管理员',
  `is_valid` tinyint(1) NOT NULL DEFAULT 1,
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `avatar` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '头像地址',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uin_name`(`name`) USING BTREE,
  UNIQUE INDEX `uin_username`(`username`) USING BTREE,
  INDEX `i_user_role`(`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_admin_user
-- ----------------------------
INSERT INTO `qmgx_admin_user` VALUES (1, 0, '超级管理员', '默认密码:123456', 'admin', '76c09af1425c301e5f5d5ee7047b9aa9', '91585', 1, 1, 1534554403, NULL, '');

-- ----------------------------
-- Table structure for qmgx_apply
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_apply`;
CREATE TABLE `qmgx_apply`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `proid` int(11) NULL DEFAULT NULL COMMENT '预约的产品id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名字',
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `isadd` smallint(1) NULL DEFAULT 2 COMMENT '1加2减',
  `delpoint` decimal(20, 2) NULL DEFAULT NULL COMMENT '扣除财分',
  `ctime` int(10) NULL DEFAULT NULL COMMENT '预约时间',
  `nowdate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '今天时间戳',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '0未处理 1 已扣除 2 已退还',
  `returntime` int(11) NULL DEFAULT NULL COMMENT '到期未扣除就退还',
  `point_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '指定状态 1 未指定  2 已指定过',
  `rank` int(10) NULL DEFAULT 0 COMMENT '权重值  发奖的时候以此为基本 并以指定状态为先选条件',
  `ipstr` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `proid`(`proid`, `uid`) USING BTREE COMMENT '商品id+用户id',
  INDEX `return_time`(`returntime`) USING BTREE,
  INDEX `status`(`status`, `returntime`) USING BTREE COMMENT '当前日期+处理状态'
) ENGINE = MyISAM AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '预约记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_apply
-- ----------------------------
INSERT INTO `qmgx_apply` VALUES (1, 19, '[预约]测试场次', 10, 2, 2.00, 1596633999, '20200805', 1, 1596634080, 2, 8390, '47.99.20.120');
INSERT INTO `qmgx_apply` VALUES (2, 9, '[预约]1号机器人', 78, 2, 3.00, 1596649548, '20200806', 2, 1596686400, 1, 8659, '47.99.20.136');
INSERT INTO `qmgx_apply` VALUES (3, 1, '[预约]2号机器人', 78, 2, 5.00, 1596649560, '20200806', 2, 1596690000, 1, 5155, '47.99.20.136');
INSERT INTO `qmgx_apply` VALUES (4, 7, '[预约]3号机器人', 78, 2, 8.00, 1596649576, '20200806', 2, 1596697200, 1, 5704, '47.99.20.136');
INSERT INTO `qmgx_apply` VALUES (5, 20, '[预约]测试机器人勿拍', 10, 2, 2.00, 1596674699, '20200806', 2, 1596674820, 1, 4916, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (6, 20, '[预约]测试机器人勿拍', 6, 2, 2.00, 1596674715, '20200806', 2, 1596674820, 1, 5494, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (7, 20, '[预约]测试机器人勿拍-抢购失败退还', 10, 1, 2.00, 1596674821, '20200806', 2, 1596674820, 1, 4916, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (8, 20, '[预约]测试机器人勿拍-抢购失败退还', 6, 1, 2.00, 1596674821, '20200806', 2, 1596674820, 1, 5494, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (9, 21, '[预约]测试机器人勿拍', 10, 2, 2.00, 1596674833, '20200806', 2, 1596675000, 2, 6253, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (10, 21, '[预约]测试机器人勿拍', 6, 2, 2.00, 1596674852, '20200806', 2, 1596675000, 1, 8685, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (11, 21, '[预约]测试机器人勿拍', 8, 2, 2.00, 1596674863, '20200806', 1, 1596675000, 1, 5240, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (12, 21, '[预约]测试机器人勿拍-抢购失败退还', 10, 1, 2.00, 1596675002, '20200806', 2, 1596675000, 2, 6253, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (13, 21, '[预约]测试机器人勿拍-抢购失败退还', 6, 1, 2.00, 1596675002, '20200806', 2, 1596675000, 1, 8685, '47.111.193.95');
INSERT INTO `qmgx_apply` VALUES (14, 9, '[预约]1号机器人', 30, 2, 3.00, 1596678352, '20200806', 2, 1596686400, 1, 3996, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (15, 1, '[预约]2号机器人', 30, 2, 5.00, 1596678356, '20200806', 2, 1596690000, 1, 2332, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (16, 3, '[预约]4号机器人', 30, 2, 15.00, 1596678360, '20200806', 2, 1596693600, 1, 7543, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (17, 7, '[预约]3号机器人', 30, 2, 8.00, 1596678362, '20200806', 2, 1596697200, 1, 3767, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (18, 2, '[预约]5号机器人', 30, 2, 20.00, 1596678364, '20200806', 2, 1596700800, 1, 7768, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (19, 9, '[预约]1号机器人-抢购失败退还', 30, 1, 3.00, 1596686401, '20200806', 2, 1596686400, 1, 3996, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (20, 9, '[预约]1号机器人-抢购失败退还', 78, 1, 3.00, 1596686401, '20200806', 2, 1596686400, 1, 8659, '47.99.20.136');
INSERT INTO `qmgx_apply` VALUES (21, 1, '[预约]2号机器人-抢购失败退还', 30, 1, 5.00, 1596690002, '20200806', 2, 1596690000, 1, 2332, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (22, 1, '[预约]2号机器人-抢购失败退还', 78, 1, 5.00, 1596690002, '20200806', 2, 1596690000, 1, 5155, '47.99.20.136');
INSERT INTO `qmgx_apply` VALUES (23, 3, '[预约]4号机器人-抢购失败退还', 30, 1, 15.00, 1596693601, '20200806', 2, 1596693600, 1, 7543, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (24, 7, '[预约]3号机器人-抢购失败退还', 30, 1, 8.00, 1596697201, '20200806', 2, 1596697200, 1, 3767, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (25, 7, '[预约]3号机器人-抢购失败退还', 78, 1, 8.00, 1596697201, '20200806', 2, 1596697200, 1, 5704, '47.99.20.136');
INSERT INTO `qmgx_apply` VALUES (26, 2, '[预约]5号机器人-抢购失败退还', 30, 1, 20.00, 1596700802, '20200806', 2, 1596700800, 1, 7768, '42.120.74.100');
INSERT INTO `qmgx_apply` VALUES (27, 9, '[预约]1号机器人', 43, 2, 3.00, 1596754740, '20200807', 0, 1596772800, 1, 9729, '47.111.193.73');
INSERT INTO `qmgx_apply` VALUES (28, 1, '[预约]2号机器人', 43, 2, 5.00, 1596754762, '20200807', 0, 1596776400, 1, 1123, '47.111.193.73');
INSERT INTO `qmgx_apply` VALUES (29, 21, '[预约]测试机器人勿拍', 43, 2, 2.00, 1596754787, '20200807', 1, 1596761400, 1, 4185, '47.111.193.73');
INSERT INTO `qmgx_apply` VALUES (30, 21, '[预约]测试机器人勿拍', 14, 2, 2.00, 1596758378, '20200807', 2, 1596761400, 1, 9747, '47.99.20.139');
INSERT INTO `qmgx_apply` VALUES (31, 21, '[预约]测试机器人勿拍', 13, 2, 2.00, 1596758391, '20200807', 1, 1596761400, 1, 1225, '47.99.20.139');
INSERT INTO `qmgx_apply` VALUES (32, 21, '[预约]测试机器人勿拍', 10, 2, 2.00, 1596758408, '20200807', 2, 1596761400, 1, 7963, '47.99.20.139');
INSERT INTO `qmgx_apply` VALUES (33, 21, '[预约]测试机器人勿拍', 6, 2, 2.00, 1596758419, '20200807', 2, 1596761400, 2, 5888, '47.99.20.139');
INSERT INTO `qmgx_apply` VALUES (34, 21, '[预约]测试机器人勿拍', 8, 2, 2.00, 1596759090, '20200807', 2, 1596761400, 1, 4623, '47.111.193.82');
INSERT INTO `qmgx_apply` VALUES (35, 21, '[预约]测试机器人勿拍-抢购失败退还', 8, 1, 2.00, 1596761403, '20200807', 2, 1596761400, 1, 4623, '47.111.193.82');
INSERT INTO `qmgx_apply` VALUES (36, 21, '[预约]测试机器人勿拍-抢购失败退还', 6, 1, 2.00, 1596761403, '20200807', 2, 1596761400, 2, 5888, '47.99.20.139');
INSERT INTO `qmgx_apply` VALUES (37, 21, '[预约]测试机器人勿拍-抢购失败退还', 10, 1, 2.00, 1596761403, '20200807', 2, 1596761400, 1, 7963, '47.99.20.139');
INSERT INTO `qmgx_apply` VALUES (38, 21, '[预约]测试机器人勿拍-抢购失败退还', 14, 1, 2.00, 1596761403, '20200807', 2, 1596761400, 1, 9747, '47.99.20.139');
INSERT INTO `qmgx_apply` VALUES (39, 9, '[预约]1号机器人', 36, 2, 3.00, 1596763841, '20200807', 0, 1596772800, 1, 2349, '47.111.193.77');
INSERT INTO `qmgx_apply` VALUES (40, 9, '[预约]1号机器人', 46, 2, 3.00, 1596764263, '20200807', 0, 1596772800, 1, 5104, '112.124.159.144');

-- ----------------------------
-- Table structure for qmgx_bank_list
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_bank_list`;
CREATE TABLE `qmgx_bank_list`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行名称',
  `display_order` int(10) NOT NULL COMMENT '显示排序',
  `status` tinyint(1) NOT NULL COMMENT '是否显示  1 显示 2 隐藏',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '银行列表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_bank_list
-- ----------------------------
INSERT INTO `qmgx_bank_list` VALUES (1, '中国人民银行', 0, 1, 1590049301, 1590049301);
INSERT INTO `qmgx_bank_list` VALUES (2, '国家开发银行', 0, 1, 1590049322, 1590049322);
INSERT INTO `qmgx_bank_list` VALUES (3, '中国工商银行', 0, 1, 1590049336, 1590049336);
INSERT INTO `qmgx_bank_list` VALUES (4, '中国农业银行', 0, 1, 1590049346, 1590049346);
INSERT INTO `qmgx_bank_list` VALUES (5, '中国银行', 0, 1, 1590049366, 1590049366);
INSERT INTO `qmgx_bank_list` VALUES (6, '中国建设银行', 0, 1, 1590049382, 1590049382);
INSERT INTO `qmgx_bank_list` VALUES (7, '交通银行', 0, 1, 1590049394, 1590049394);
INSERT INTO `qmgx_bank_list` VALUES (8, '民生银行', 0, 1, 1590049432, 1590049432);
INSERT INTO `qmgx_bank_list` VALUES (9, '中国邮政银行', 0, 1, 1590657318, 1590657318);
INSERT INTO `qmgx_bank_list` VALUES (10, '其他银行', 0, 1, 1596616719, 1596616719);

-- ----------------------------
-- Table structure for qmgx_buyhistory
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_buyhistory`;
CREATE TABLE `qmgx_buyhistory`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `types` int(11) NULL DEFAULT NULL COMMENT '1抢购',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_buyhistory
-- ----------------------------
INSERT INTO `qmgx_buyhistory` VALUES (1, 2, 'a:31:{s:2:\"id\";i:1;s:6:\"buyuid\";i:4;s:9:\"buyuidatc\";i:0;s:6:\"caifen\";i:999;s:9:\"productid\";i:19;s:5:\"title\";s:12:\"测试场次\";s:5:\"price\";s:7:\"1652.89\";s:6:\"daynum\";i:4;s:11:\"earnpercent\";i:21;s:11:\"dayaddprice\";s:6:\"347.11\";s:10:\"basicprice\";s:7:\"2000.00\";s:5:\"ctime\";i:1596288406;s:12:\"peiduistatus\";i:2;s:9:\"peiduiuid\";i:10;s:10:\"peiduitime\";i:1596634081;s:8:\"peiduiid\";N;s:12:\"match_pro_id\";i:19;s:15:\"match_pro_title\";s:12:\"测试场次\";s:9:\"match_oid\";i:2;s:9:\"paystatus\";s:1:\"2\";s:7:\"paytype\";s:9:\"支付宝\";s:7:\"paytime\";i:1596634109;s:6:\"payimg\";s:58:\"http://img.yizitao.cn/b527d4791d8644af41aa12e7d21a645cblob\";s:11:\"cansaletime\";i:1596556800;s:12:\"thatdatetime\";i:1596211200;s:7:\"ordersn\";s:0:\"\";s:6:\"isread\";i:2;s:6:\"myread\";i:2;s:7:\"applyid\";i:1;s:2:\"xs\";i:2;s:9:\"issendbot\";i:2;}');
INSERT INTO `qmgx_buyhistory` VALUES (2, 2, 'a:31:{s:2:\"id\";i:2;s:6:\"buyuid\";i:10;s:9:\"buyuidatc\";i:0;s:6:\"caifen\";i:2;s:9:\"productid\";i:19;s:5:\"title\";s:12:\"测试场次\";s:5:\"price\";s:7:\"2000.00\";s:6:\"daynum\";i:4;s:11:\"earnpercent\";i:21;s:11:\"dayaddprice\";s:6:\"420.00\";s:10:\"basicprice\";s:7:\"2420.00\";s:5:\"ctime\";i:1596634084;s:12:\"peiduistatus\";i:1;s:9:\"peiduiuid\";i:0;s:10:\"peiduitime\";N;s:8:\"peiduiid\";i:1;s:12:\"match_pro_id\";N;s:15:\"match_pro_title\";N;s:9:\"match_oid\";N;s:9:\"paystatus\";s:1:\"0\";s:7:\"paytype\";N;s:7:\"paytime\";i:0;s:6:\"payimg\";s:0:\"\";s:11:\"cansaletime\";i:1596902400;s:12:\"thatdatetime\";i:1596556800;s:7:\"ordersn\";s:0:\"\";s:6:\"isread\";i:1;s:6:\"myread\";i:1;s:7:\"applyid\";i:0;s:2:\"xs\";i:2;s:9:\"issendbot\";i:2;}');

-- ----------------------------
-- Table structure for qmgx_buylog
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_buylog`;
CREATE TABLE `qmgx_buylog`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `datestr` int(11) NULL DEFAULT NULL,
  `uid` int(11) NULL DEFAULT NULL,
  `proid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for qmgx_card_apply
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_card_apply`;
CREATE TABLE `qmgx_card_apply`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `realname` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `selfcard` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '身份证号',
  `mustman` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '紧急联系人',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '0 待审核 1 已审核  2 审核拒绝',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '提交时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '更新时间',
  `is_trash` tinyint(1) NULL DEFAULT 0 COMMENT '是否已经废弃 0 否 1 是',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝原因',
  `card_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证正面',
  `card_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证反面',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 138 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_card_apply
-- ----------------------------
INSERT INTO `qmgx_card_apply` VALUES (4, 3, '杠精邱', '371311199912121212', '18511111111', 1, 1596249338, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (5, 1, '狗蛋', '321321321321321321', '18888888888', 1, 1596534630, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (6, 4, '人工智能', '321123456765456456', '18888888888', 1, 1596544278, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (7, 13, '金帆', '123456789987654321', '18097883326', 1, 1596548256, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (8, 14, '帆帆', '123123123123123123', '18922336751', 1, 1596548380, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (9, 11, '王益', '123123123123123123', '18076238056', 1, 1596549245, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (10, 10, '王益', '123123123123123123', '18876238055', 1, 1596549406, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (11, 9, '吴建德', '123443211234567634', '17015568075', 1, 1596549578, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (12, 8, '吴建德', '111111111111111221', '17015568075', 1, 1596549659, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (13, 7, '竹哥', '112233445566778899', '17009125358', 1, 1596549870, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (14, 6, '竹哥', '123456789987654321', '17009125358', 1, 1596550084, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (15, 15, '万国', '123123123123123123', NULL, 1, 1596550175, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (16, 5, '赵有益', '323345678876567456', '17009125357', 1, 1596550808, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (17, 17, '刘东亭', '360502199408207439', '18146613930', 1, 1596597030, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (18, 18, '君尚', '360502199408207456', '18857970824', 1, 1596597170, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (19, 20, '君临', '360502199308237452', '15523518952', 1, 1596597415, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (20, 19, '张建新', '652324197411042216', '13565360343', 1, 1596597595, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (21, 25, '张建新', '652324197411042216', '18196160520', 1, 1596598397, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (22, 24, '朱佩文', '360421199211042021', '13479260909', 1, 1596598734, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (23, 29, '王者', '340321199106118937', '17326099563', 1, 1596599174, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (24, 30, '王丰', '330304199512022115', '15888738658', 1, 1596599289, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (25, 32, '施阿兴', '350582196312206013', '13059385678', 1, 1596599480, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (26, 33, '潘沿围', '350583199111218336', '19959850596', 1, 1596599532, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (27, 27, '王敏', '332523199110110050', '15168016190', 1, 1596599816, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (28, 34, '施俊兴', '350582196312206013', '18027195555', 1, 1596599969, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (29, 36, '施建兴', '350582196412206013', '13533576777', 1, 1596600378, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (30, 35, '刘照文', '522121197508024410', '15881879193', 1, 1596601077, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (31, 37, '彭海波', '320922198308224413', '13770135227', 1, 1596601306, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (32, 16, '吴斌', '322147258369789', '15852536233', 1, 1596602374, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (33, 42, '金健康', '258258258258258258', '15852536236', 1, 1596602548, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (34, 44, '天龙', '159525362252583691', '15952536225', 1, 1596602950, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (35, 39, '张乃华', '350825198207221116', '13636966250', 1, 1596603260, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (36, 45, '陈来', '320382199012281218', '15951536223', 1, 1596603262, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (37, 46, '叶秋', '230321198604225516', '18128134200', 1, 1596603484, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (38, 47, '常广', '421083199212012515', '17786339521', 1, 1596603622, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (39, 48, '赵老师', '322382258369789256', '15649220223', 1, 1596603830, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (40, 49, '陈龙', '321328199602034528', '15649220205', 1, 1596604059, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (41, 50, '甘焕杰', '452123199711096114', '18676982040', 1, 1596604836, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (42, 41, '小孙', '620302199212210620', '13772145686', 1, 1596604948, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (43, 51, '郑一峰', '332522198705040011', '17786339521', 1, 1596604990, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (44, 52, '鲲鹏', '120113198809303219', '18092011959', 1, 1596605156, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (45, 54, '池善卿', '35042619790906301X', '17786339521', 1, 1596606848, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (46, 55, '卫忠杰', '210602198711260513', '17786339521', 1, 1596606861, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (47, 57, '许家圣', '340103198303072554', '17786339521', 1, 1596606899, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (48, 59, '李靖男', '410183199307210015', '17786339521', 1, 1596606912, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (49, 58, '袁国旗', '342501198111101331', '18655555555', 1, 1596607757, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (50, 64, '朱晓星', '610602198812100310', '17885933724', 1, 1596609503, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (51, 65, '朱贺', '370404199206064053', '18563272732', 1, 1596609569, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (52, 60, '陈侃', '362326198306270039', '17786339521', 1, 1596609620, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (53, 63, '康焕卉', '430503198706130038', '17786339521', 1, 1596609632, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (54, 67, '池鹏', '331081198601210014', '17786339521', 1, 1596609643, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (55, 68, '刘大奇', '230103198509121352', '17786339521', 1, 1596609653, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (56, 66, '叶秋平', '422201198810083324', '19151002353', 1, 1596609655, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (57, 62, '林斌', '332523199312082414', '15967293599', 1, 1596609707, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (58, 69, '高屹君', '332529199104207014', '15988013388', 1, 1596609956, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (59, 74, '毛庆中', '452531199602232514', '15507474770', 1, 1596610939, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (60, 77, '陈国香', '510922199009154662', '15261185382', 1, 1596611379, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (61, 79, '阮凤珍', '450421197610052123', '13977488752', 1, 1596611774, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (62, 80, '程伟雄', '440683198705013031', '13392298144', 1, 1596611986, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (63, 82, '赵鑫伟', '211322198412272774', '13591806765', 1, 1596613912, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (64, 76, '罗世华', '210105198611014923', '13725608186', 1, 1596614310, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (65, 87, '陈芬宜', '440681198705104248', '18942461602', 1, 1596618051, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (66, 92, '阿鲁', '431202198811101720', '17525252525', 1, 1596621176, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (67, 94, '一涵', '452322196611200042', '15877004910', 1, 1596624352, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (68, 95, '爱惜', '452322196611200042', '15877006429', 1, 1596625004, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (69, 96, '成荣', '452322196801090921', '15877007910', 1, 1596625493, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (70, 107, '周盈盈', '410402198808235580', '13977488752', 1, 1596632547, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (71, 108, '周莹', '210204198801043062', '13977488752', 1, 1596632775, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (72, 22, '徐满意', '320923199210126938', '18094444666', 1, 1596635611, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (73, 111, '周愉', '310101198402261029', '13977488752', 1, 1596635817, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (74, 104, '文洪', '51092219940825808X', '18080824501', 1, 1596638803, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (75, 116, '王秋坤', '432802197407068109', '18670546609', 1, 1596639313, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (76, 117, '黄玉燕', '411303199010163924', '18703614501', 1, 1596639780, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (77, 121, '吴束', '630104198707010013', '18778994890', 1, 1596645037, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (78, 78, '黄娇', '441723198611132021', '13424519886', 1, 1596649537, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (79, 126, '郭亮', '452731198110046013', '18191518314', 1, 1596673327, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (80, 102, '庾燕权', '452322198105190928', '13635133929', 1, 1596682643, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (81, 133, '陈春淼', '441624198905244433', '15578189475', 1, 1596682747, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (82, 128, '朱江雪', '410223199311049841', '13152641738', 1, 1596683125, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (83, 134, '刘贞', '320322199603056821', '13553993451', 1, 1596683355, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (84, 137, '刘璐佳', '500112199312046340', '13117612694', 1, 1596683707, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (85, 138, '于禁', '220622198404090013', '13533333333', 1, 1596683832, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (86, 139, '郭新爱', '411327198206194921', '13612525209', 1, 1596684719, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (87, 141, '张建新', '652324197411042216', '18196160520', 1, 1596686715, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (88, 129, '谢榆大', '350181196805041258', '18060719833', 1, 1596686955, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (89, 144, '张永龙', '452322197703071238', '18778856484', 1, 1596688710, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (90, 143, '麦彩霞', '450404198503260320', '13647741485', 1, 1596688820, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (91, 148, '刘照文', '522121197508024410', '15881879193', 1, 1596690790, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (92, 147, '邱桂英', '342222196903102523', '15846963781', 1, 1596691047, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (93, 150, '邱桂英', '342222196903102523', '13704596514', 1, 1596692687, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (94, 155, '吴积锋', '332525199001016316', '15578959354', 1, 1596699893, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (95, 72, '洪志波', '350521198307183039', '13600774732', 1, 1596708681, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (96, 158, '黄礼国', '450821198408072530', '13534098898', 1, 1596709739, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (97, 159, '洪志波', '350521198307183039', '13600774732', 1, 1596709808, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (98, 161, '周玮奇', '362430200106054519', '18575599846', 1, 1596717388, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (99, 43, '王可', '412826199512026615', '17739645099', 1, 1596717948, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (100, 163, '周小勇', '362430198809094518', '18575599846', 1, 1596718634, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (101, 165, '翁丹妮', '330204199207191021', '15113573121', 1, 1596723508, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (102, 168, '盖国庆', '370502197202071655', '18153220977', 1, 1596725093, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (103, 170, '王燕', '411303199010163924', '13037617108', 1, 1596725114, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (104, 169, '张乃华', '340522198311144352', '13636966250', 1, 1596725131, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (105, 171, '张乃华', '220283198107041435', '13636966250', 1, 1596725633, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (106, 153, '廖桂阳', '450421199305103536', '15077498429', 1, 1596725893, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (107, 172, '张乃华', '150725198912318316', '13636966250', 1, 1596725909, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (108, 173, '张乃华', '320206197712169476', '13636966250', 1, 1596726460, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (109, 174, '张乃华', '350122197605265876', '13636966280', 1, 1596727173, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (110, 175, '张乃华', '230882197305180114', '13636966250', 1, 1596727716, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (111, 179, '张乃华', '361102198701060936', '13636966250', 1, 1596728499, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (112, 180, '张乃华', '340522199211183156', '13636966250', 1, 1596729117, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (113, 182, '张乃华', '370983199010238982', '13636966250', 1, 1596729640, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (114, 183, '张乃华', '210103198704303000', '13636966250', 1, 1596729998, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (115, 184, '张乃华', '130684198501164368', '13636966250', 1, 1596730401, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (116, 187, '张乃华', '130131199204196288', '13636966250', 1, 1596730811, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (117, 190, '张乃华', '130131199204196288', '13636966250', 1, 1596731481, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (118, 192, '张乃华', '230403198507235029', '13636966250', 1, 1596731875, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (119, 194, '张乃华', '371102198105032906', '13636966250', 1, 1596732251, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (120, 198, '张乃华', '210103199211091905', '13636966250', 1, 1596733110, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (121, 200, '张乃华', '410422198207040687', '13636966250', 1, 1596733502, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (122, 202, '张乃华', '211081198009011182', '13636966250', 1, 1596733828, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (123, 204, '张乃华', '371102198105032906', '13636966250', 1, 1596734250, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (124, 205, '张乃华', '320312198110012663', '13636966250', 1, 1596734988, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (125, 250, '徐文辉', '441621199104223016', '17875179993', 1, 1596761432, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (126, 261, '黄建红', '360602197411162522', '15170109638', 1, 1596764594, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (127, 258, '窦国军', '371522199006066859', '18865266521', 1, 1596764824, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (128, 262, '巨磊', '610428198911031615', '15501598007', 1, 1596764862, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (129, 264, '封星星', '430422198401290050', '13203075100', 1, 1596764881, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (130, 98, '鲁建', '510923199604147332', '18808108991', 1, 1596766833, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (131, 269, '封星星', '432421198805212325', '13203075100', 1, 1596767329, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (132, 270, '陈国香', '510922199009154662', '15261185382', 1, 1596768305, NULL, 0, NULL, NULL, NULL);
INSERT INTO `qmgx_card_apply` VALUES (135, 272, '著资', '371302199101143153', '18653212111', 2, 1599329403, 1599329417, 1, '二分', 'http://img.yjkpt.cn/998dec3662b62a82baff4a5d084225c9.png', 'http://img.yjkpt.cn/53b6a3caea4f66c99073b2b1aecc15f7.png');
INSERT INTO `qmgx_card_apply` VALUES (136, 272, '著资', '371302199101143153', '15232211111', 1, 1599329452, 1599329464, 0, NULL, 'http://img.yjkpt.cn/9c6eef1d848dc388451c8635055b84a4.png', 'http://img.yjkpt.cn/883fd088a0c545ac3dba58d809314187.png');
INSERT INTO `qmgx_card_apply` VALUES (137, 273, '张三', '371302199101143153', '15866695555', 0, 1599356221, NULL, 0, NULL, 'http://img.yjkpt.cn/8c7ac8ddba0903f45cf19c42809c5181.png', 'http://img.yjkpt.cn/3a5c0ebd9702122645bcf8f0c0b17ae6.png');

-- ----------------------------
-- Table structure for qmgx_category
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_category`;
CREATE TABLE `qmgx_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '上级分类ID（若无上级，则为0）',
  `level` int(11) NOT NULL DEFAULT 1 COMMENT '分类等级',
  `id_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类ID路径',
  `title_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题路径',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类描述信息',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类图标',
  `is_visible` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否可见',
  `is_valid` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否删除',
  `display_order` int(11) NOT NULL DEFAULT 0 COMMENT '排列序号',
  `create_time` int(10) NULL DEFAULT NULL,
  `update_time` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `i_category_parent_id`(`parent_id`) USING BTREE,
  INDEX `i_category_id_path`(`id_path`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品分类表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_category
-- ----------------------------
INSERT INTO `qmgx_category` VALUES (1, 0, 1, '0-1', '男装', '男装', '', '', 1, 1, 0, 1571296497, 1571713364);
INSERT INTO `qmgx_category` VALUES (2, 1, 2, '0-1-2', '男装-羽绒服', '羽绒服', '', '/uploads/image/10/22/2019/5e648d9436b3369a64dbc77eebeb3126.png', 1, 0, 0, 1571296523, 1571713334);
INSERT INTO `qmgx_category` VALUES (3, 0, 1, '0-3', '女装', '女装', '', '', 1, 1, 0, 1571296533, 1571296533);
INSERT INTO `qmgx_category` VALUES (4, 3, 2, '0-3-4', '女装-连衣裙', '连衣裙', '', '/uploads/image/10/22/2019/1b3772f3d1392679f4054c3967ac419d.png', 1, 1, 0, 1571296553, 1571713323);
INSERT INTO `qmgx_category` VALUES (6, 0, 1, '0-6', '箱包手袋', '箱包手袋', '', '', 1, 1, 0, 1571713651, 1571713651);
INSERT INTO `qmgx_category` VALUES (7, 0, 1, '0-7', '美妆护肤', '美妆护肤', '', '', 1, 1, 0, 1571713662, 1571713662);
INSERT INTO `qmgx_category` VALUES (8, 0, 1, '0-8', '个护清洗', '个护清洗', '', '', 1, 0, 0, 1571713678, 1571713678);
INSERT INTO `qmgx_category` VALUES (9, 0, 1, '0-9', '珠宝钟表', '珠宝钟表', '', '', 1, 0, 0, 1571713687, 1571713687);
INSERT INTO `qmgx_category` VALUES (10, 9, 2, '0-9-10', '珠宝钟表-手表', '手表', '', '/uploads/image/10/22/2019/eb2cbb0a6104a8ae7cde45417de9588c.png', 1, 0, 0, 1571713964, 1571713964);
INSERT INTO `qmgx_category` VALUES (11, 8, 2, '0-8-11', '个护清洗-洗面奶', '洗面奶', '', '/uploads/image/10/22/2019/d8d1182d267dc34382cf80100611a51d.png', 1, 0, 0, 1571714014, 1571714014);
INSERT INTO `qmgx_category` VALUES (12, 7, 2, '0-7-12', '美妆护肤-口红', '口红', '', '/uploads/image/10/22/2019/4d1940dceb3d6fa2a459fc7fd39efcb1.png', 1, 1, 0, 1571714045, 1571714045);
INSERT INTO `qmgx_category` VALUES (13, 6, 2, '0-6-13', '箱包手袋-行李箱', '行李箱', '', '/uploads/image/10/22/2019/649dc20e8663ec1e2f95ba43ec01ffe9.png', 1, 1, 0, 1571714082, 1571714082);
INSERT INTO `qmgx_category` VALUES (14, 0, 1, '0-14', '新分类', '新分类', '丰富', '', 1, 1, 0, 1599224311, 1599224311);
INSERT INTO `qmgx_category` VALUES (15, 0, 1, '0-15', '新分类', '新分类', '丰富', '', 1, 1, 0, 1599224349, 1599224349);

-- ----------------------------
-- Table structure for qmgx_complain
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_complain`;
CREATE TABLE `qmgx_complain`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提交内容',
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '验证图片',
  `uid` int(11) NULL DEFAULT NULL COMMENT '提交人',
  `ctime` int(11) NULL DEFAULT NULL COMMENT '提交时间',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '1待处理2已处理',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户投诉记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qmgx_goods
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_goods`;
CREATE TABLE `qmgx_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price1` int(11) NOT NULL COMMENT '领养价值开始范围',
  `price2` int(11) NOT NULL COMMENT '领养价值结束范围',
  `time_start` int(11) NOT NULL COMMENT '领养时间开始范围',
  `time_end` int(11) NOT NULL COMMENT '领养时间结束范围',
  `point_start` int(11) NOT NULL COMMENT '积分开始范围',
  `point_end` int(11) NOT NULL COMMENT '积分结束范围',
  `daynum` int(11) NOT NULL COMMENT '合约收益天数1-15',
  `daypercent` int(11) NOT NULL COMMENT '合约收益百分比1-40',
  `botnum` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '每日bot可得',
  `atcnum` int(11) NOT NULL COMMENT '可挖atc数量',
  `isshow` tinyint(3) NOT NULL DEFAULT 1 COMMENT '1显示2隐藏',
  `sort` int(11) NULL DEFAULT 0,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `isclose` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1正常2关闭购买',
  `closestr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关闭购买提示话语',
  `desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '区块内容' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_goods
-- ----------------------------
INSERT INTO `qmgx_goods` VALUES (1, '2号机器人', 801, 1600, 46800, 47400, 5, 10, 2, 9, 3.20, 0, 1, 21, 'http://img.yizitao.cn/553b1b1473c4e33ccfdfbfc92dc5ead3.png', 1, '', NULL);
INSERT INTO `qmgx_goods` VALUES (2, '5号机器人', 5201, 8000, 57600, 58200, 20, 60, 4, 15, 16.50, 0, 1, 18, 'http://img.yizitao.cn/59553ac2c92d10fac843f691f20e7105.png', 1, '', NULL);
INSERT INTO `qmgx_goods` VALUES (3, '4号机器人', 3201, 5200, 50400, 51000, 15, 6, 3, 12, 12.30, 0, 1, 20, 'http://img.yizitao.cn/83b2e829c3c919c4820dc9ad4e1aa004.png', 1, '', NULL);
INSERT INTO `qmgx_goods` VALUES (7, '3号机器人', 1601, 3200, 54000, 54600, 8, 12, 1, 5, 4.70, 0, 1, 19, 'http://img.yizitao.cn/2136d32e6e40dd41bec861c654222c92.png', 1, '', NULL);
INSERT INTO `qmgx_goods` VALUES (8, '7号机器人', 9801, 11800, 64800, 65400, 30, 50, 5, 18, 25.00, 0, 1, 9, 'http://img.yizitao.cn/89ee14bf1e63719aded68c889f40a838.png', 2, '商品待开放', NULL);
INSERT INTO `qmgx_goods` VALUES (9, '1号机器人', 300, 800, 43200, 43800, 3, 40, 4, 20, 1.80, 0, 1, 22, 'http://img.yizitao.cn/ef46700857fe6b2b8cdead215dec4e91.png', 1, '', '<p>shenme</p>');
INSERT INTO `qmgx_goods` VALUES (10, '6号机器人', 8001, 9800, 61200, 61800, 25, 70, 2, 8, 20.00, 0, 1, 16, 'http://img.yizitao.cn/13509f5daa47fe25fb8285cd130362d0.png', 2, '', NULL);
INSERT INTO `qmgx_goods` VALUES (11, '8号机器人', 11801, 13800, 68400, 69000, 40, 0, 7, 22, 30.00, 0, 1, 8, 'http://img.yizitao.cn/ac83cb3bac238e94a5cf3bf7ff90adc8.png', 2, '商品待开放', NULL);
INSERT INTO `qmgx_goods` VALUES (12, '9号机器人', 13801, 15800, 72000, 72600, 50, 999, 10, 30, 35.00, 0, 1, 0, 'http://img.yizitao.cn/083e7440ec75b9752b8c1aa0c977c9b6.png', 2, '', NULL);
INSERT INTO `qmgx_goods` VALUES (13, '10号机器人', 15801, 18000, 75600, 76200, 60, 999, 8, 26, 40.00, 0, 1, 0, 'http://img.yizitao.cn/675d96ac893b8dccda8bf3526bc23eb4.png', 2, '', NULL);
INSERT INTO `qmgx_goods` VALUES (21, '测试机器人勿拍', 200, 500, 31800, 32100, 2, 999, 1, 1, 2.00, 0, 1, 0, 'http://img.yizitao.cn/970d7fbc8d31725b03b027bbe2339c65.png', 1, '', NULL);
INSERT INTO `qmgx_goods` VALUES (22, '新的小可爱', 10000, 12000, 46800, 54000, 10, 999, 4, 15, 1.00, 0, 1, 0, 'http://img.yizitao.cn/3e15a0bb481010671706c2288bff97f4.png', 1, '', '<p>的服务违法</p>');

-- ----------------------------
-- Table structure for qmgx_index_notice
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_index_notice`;
CREATE TABLE `qmgx_index_notice`  (
  `is_show` tinyint(1) NULL DEFAULT 1,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '首页通知' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_index_notice
-- ----------------------------
INSERT INTO `qmgx_index_notice` VALUES (1, '                                                公  告', '<p>欢迎加入 人工智能大家庭！</p><p><br/></p>');

-- ----------------------------
-- Table structure for qmgx_lottery
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_lottery`;
CREATE TABLE `qmgx_lottery`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖名称',
  `goods_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品名称',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品图片',
  `type` int(11) NOT NULL DEFAULT 1 COMMENT '奖品类型',
  `num` int(11) NOT NULL DEFAULT 0 COMMENT '奖品数值 如1盒 1积分',
  `gl` int(11) NOT NULL DEFAULT 0 COMMENT '抽奖比重 商品比重 除以 所有商品比重和 即为概率',
  `stock` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '库存',
  `is_visible` tinyint(1) NOT NULL DEFAULT 1,
  `price_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中奖后提示',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '中间时间',
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抽奖商品' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_lottery
-- ----------------------------
INSERT INTO `qmgx_lottery` VALUES (1, '一等奖', '五菱宏光', 'https://ss0.baidu.com/73x1bjeh1BF3odCf/it/u=2412519370,2638474476&fm=85&s=9FA34F810E0304CC198DC11B0300C0D3', 1, 1, 0, 0, 1, '恭喜抽中一等奖神车', 1572333887, 1572346709);
INSERT INTO `qmgx_lottery` VALUES (2, '二等奖', '摩托车', '/uploads/image/10/29/2019/dc0d605714dd1a36530c5a73e1545091.jpg', 1, 1, 1, 1, 1, '恭喜抽中二等奖，摩托车', 1572346198, 1572350608);
INSERT INTO `qmgx_lottery` VALUES (3, '三等奖', '购物积分50', '/uploads/image/10/29/2019/646b85d2c2f7573ae21edc695a0ca0da.jpg', 2, 50, 100, 100, 1, '恭喜抽中三等奖', 1572346812, 1572351200);
INSERT INTO `qmgx_lottery` VALUES (4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 100000, 99977, 1, '恭喜抽中消费积分10', 1572346861, 1572350597);

-- ----------------------------
-- Table structure for qmgx_lottery_list
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_lottery_list`;
CREATE TABLE `qmgx_lottery_list`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '抽奖人',
  `lottery_id` int(11) NOT NULL DEFAULT 0 COMMENT '奖品id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖名称',
  `goods_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品名称',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品图片',
  `type` int(11) NOT NULL DEFAULT 1 COMMENT '奖品类型',
  `num` int(11) NOT NULL DEFAULT 0 COMMENT '奖品数量',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '中间时间',
  `send_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实物 收货人',
  `send_mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `send_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货地址',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '会员领取状态 0待1已',
  `status_time` int(11) NULL DEFAULT NULL COMMENT '领取时间',
  `send_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '发放状态 0待1已',
  `send_time` int(11) NULL DEFAULT NULL COMMENT '发放时间',
  `send_company` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '快递',
  `send_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单号',
  `number` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抽奖记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_lottery_list
-- ----------------------------
INSERT INTO `qmgx_lottery_list` VALUES (1, 8, 1, '一等奖', '五菱宏光', 'https://ss0.baidu.com/73x1bjeh1BF3odCf/it/u=2412519370,2638474476&fm=85&s=9FA34F810E0304CC198DC11B0300C0D3', 1, 1, 1572333887, '李', '18300411357', '山东临沂兰山北城新区', 1, 1572337391, 0, NULL, '', '', 0);
INSERT INTO `qmgx_lottery_list` VALUES (2, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572352836, NULL, NULL, NULL, 1, 1572352836, 1, 1572352836, NULL, NULL, 20327);
INSERT INTO `qmgx_lottery_list` VALUES (3, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353035, NULL, NULL, NULL, 1, 1572353035, 1, 1572353035, NULL, NULL, 31514);
INSERT INTO `qmgx_lottery_list` VALUES (4, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353121, NULL, NULL, NULL, 1, 1572353121, 1, 1572353121, NULL, NULL, 456);
INSERT INTO `qmgx_lottery_list` VALUES (5, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353165, NULL, NULL, NULL, 1, 1572353165, 1, 1572353165, NULL, NULL, 66993);
INSERT INTO `qmgx_lottery_list` VALUES (6, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353186, NULL, NULL, NULL, 1, 1572353186, 1, 1572353186, NULL, NULL, 91973);
INSERT INTO `qmgx_lottery_list` VALUES (7, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353212, NULL, NULL, NULL, 1, 1572353212, 1, 1572353212, NULL, NULL, 97926);
INSERT INTO `qmgx_lottery_list` VALUES (8, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353241, NULL, NULL, NULL, 1, 1572353241, 1, 1572353241, NULL, NULL, 13347);
INSERT INTO `qmgx_lottery_list` VALUES (9, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353252, NULL, NULL, NULL, 1, 1572353252, 1, 1572353252, NULL, NULL, 26950);
INSERT INTO `qmgx_lottery_list` VALUES (10, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353263, NULL, NULL, NULL, 1, 1572353263, 1, 1572353263, NULL, NULL, 62927);
INSERT INTO `qmgx_lottery_list` VALUES (11, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353275, NULL, NULL, NULL, 1, 1572353275, 1, 1572353275, NULL, NULL, 29226);
INSERT INTO `qmgx_lottery_list` VALUES (12, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353283, NULL, NULL, NULL, 1, 1572353283, 1, 1572353283, NULL, NULL, 9128);
INSERT INTO `qmgx_lottery_list` VALUES (13, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353289, NULL, NULL, NULL, 1, 1572353289, 1, 1572353289, NULL, NULL, 41363);
INSERT INTO `qmgx_lottery_list` VALUES (14, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353296, NULL, NULL, NULL, 1, 1572353296, 1, 1572353296, NULL, NULL, 35427);
INSERT INTO `qmgx_lottery_list` VALUES (15, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353302, NULL, NULL, NULL, 1, 1572353302, 1, 1572353302, NULL, NULL, 87595);
INSERT INTO `qmgx_lottery_list` VALUES (16, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353308, NULL, NULL, NULL, 1, 1572353308, 1, 1572353308, NULL, NULL, 97813);
INSERT INTO `qmgx_lottery_list` VALUES (17, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353316, NULL, NULL, NULL, 1, 1572353316, 1, 1572353316, NULL, NULL, 46385);
INSERT INTO `qmgx_lottery_list` VALUES (18, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353322, NULL, NULL, NULL, 1, 1572353322, 1, 1572353322, NULL, NULL, 63569);
INSERT INTO `qmgx_lottery_list` VALUES (19, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353329, NULL, NULL, NULL, 1, 1572353329, 1, 1572353329, NULL, NULL, 8059);
INSERT INTO `qmgx_lottery_list` VALUES (20, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353335, NULL, NULL, NULL, 1, 1572353335, 1, 1572353335, NULL, NULL, 68377);
INSERT INTO `qmgx_lottery_list` VALUES (21, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353341, NULL, NULL, NULL, 1, 1572353341, 1, 1572353341, NULL, NULL, 47302);
INSERT INTO `qmgx_lottery_list` VALUES (22, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353348, NULL, NULL, NULL, 1, 1572353348, 1, 1572353348, NULL, NULL, 44592);
INSERT INTO `qmgx_lottery_list` VALUES (23, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353354, NULL, NULL, NULL, 1, 1572353354, 1, 1572353354, NULL, NULL, 98030);
INSERT INTO `qmgx_lottery_list` VALUES (24, 8, 4, '四等奖', '消费积分10', '/uploads/image/10/29/2019/64f1b731260d336d5a7dd819c6187160.jpg', 3, 10, 1572353441, NULL, NULL, NULL, 1, 1572353441, 1, 1572353441, NULL, NULL, 12871);

-- ----------------------------
-- Table structure for qmgx_mybot
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_mybot`;
CREATE TABLE `qmgx_mybot`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `isadd` tinyint(5) NOT NULL DEFAULT 1 COMMENT '1加2减',
  `nums` decimal(20, 2) NOT NULL COMMENT '激活币数量',
  `fromuid` int(11) NULL DEFAULT NULL COMMENT '来源id，0表示系统',
  `ctime` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `info` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推广收益' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_mybot
-- ----------------------------
INSERT INTO `qmgx_mybot` VALUES (1, 1, 1, 12.00, NULL, 1596441925, '管理员操作');
INSERT INTO `qmgx_mybot` VALUES (2, 2, 2, 31.00, NULL, 1596441925, '管理员操作');
INSERT INTO `qmgx_mybot` VALUES (3, 3, 2, 7.00, NULL, 1596441925, '管理员操作');
INSERT INTO `qmgx_mybot` VALUES (4, 3, 2, 8.00, NULL, 1596441925, '管理员操作');
INSERT INTO `qmgx_mybot` VALUES (5, 3, 2, 99.00, NULL, 1596441925, '管理员操作');
INSERT INTO `qmgx_mybot` VALUES (6, 2, 1, 12.00, NULL, 1596441925, '管理员操作');
INSERT INTO `qmgx_mybot` VALUES (7, 1, 1, 17.00, NULL, 1596441925, '管理员操作');
INSERT INTO `qmgx_mybot` VALUES (8, 2, 1, 4.00, 0, 1596442861, '管理员新增BOT');
INSERT INTO `qmgx_mybot` VALUES (9, 3, 1, 3.00, 0, 1596442870, '管理员新增BOT');
INSERT INTO `qmgx_mybot` VALUES (10, 3, 2, 80.00, 0, 1596442879, '管理员减少BOT');
INSERT INTO `qmgx_mybot` VALUES (11, 2, 2, 70.00, 0, 1596442892, '管理员减少BOT');
INSERT INTO `qmgx_mybot` VALUES (12, 3, 1, 99.00, 0, 1596442913, '管理员新增BOT');
INSERT INTO `qmgx_mybot` VALUES (13, 3, 2, 12.00, 0, 1596442924, '管理员减少BOT');
INSERT INTO `qmgx_mybot` VALUES (14, 3, 2, 187.00, 0, 1596442935, '管理员减少BOT');
INSERT INTO `qmgx_mybot` VALUES (15, 2, 1, 8.00, 0, 1596551517, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (16, 3, 1, 9.00, 0, 1596551517, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (17, 1, 1, 7.00, 0, 1596551517, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (18, 1, 1, 6.00, 0, 1596551517, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (19, 2, 1, 8.00, 0, 1596551636, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (20, 1, 1, 6.00, 0, 1596551636, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (21, 3, 2, 188.00, 0, 1596471663, '购买矿机小型矿机2型');
INSERT INTO `qmgx_mybot` VALUES (22, 3, 2, 188.00, 0, 1596471703, '购买矿机小型矿机2型');
INSERT INTO `qmgx_mybot` VALUES (23, 3, 2, 888.00, 0, 1596471812, '购买矿机小型矿机6型');
INSERT INTO `qmgx_mybot` VALUES (24, 3, 2, 388.00, 0, 1596506750, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (25, 3, 2, 388.00, 0, 1596506888, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (26, 3, 2, 388.00, 0, 1596507947, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (27, 3, 2, 188.00, 0, 1596508043, '购买矿机小型矿机2型');
INSERT INTO `qmgx_mybot` VALUES (28, 2, 2, 188.00, 0, 1596509750, '购买矿机小型矿机2型');
INSERT INTO `qmgx_mybot` VALUES (29, 2, 2, 288.00, 0, 1596509769, '购买矿机小型矿机3型');
INSERT INTO `qmgx_mybot` VALUES (30, 3, 2, 588.00, 0, 1596510594, '购买矿机小型矿机5型');
INSERT INTO `qmgx_mybot` VALUES (31, 3, 2, 288.00, 0, 1596520446, '购买矿机小型矿机3型');
INSERT INTO `qmgx_mybot` VALUES (32, 3, 2, 388.00, 0, 1596527243, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (33, 3, 2, 388.00, 0, 1596527541, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (34, 3, 2, 388.00, 0, 1596527550, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (35, 3, 2, 388.00, 0, 1596527557, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (36, 3, 2, 388.00, 0, 1596527563, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (37, 3, 2, 388.00, 0, 1596527569, '购买矿机小型矿机4型');
INSERT INTO `qmgx_mybot` VALUES (38, 3, 2, 288.00, 0, 1596536013, '购买矿机小型矿机3型');
INSERT INTO `qmgx_mybot` VALUES (39, 3, 1, 9.00, 0, 1596556802, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (40, 1, 1, 7.00, 0, 1596556802, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (41, 3, 1, 1.50, 0, 1596556802, '矿机“小型矿机4型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (42, 15, 1, 2.00, 0, 1596556802, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (43, 3, 1, 10.00, 0, 1596557042, '矿机“小型矿机2型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (44, 2, 1, 10.00, 0, 1596557281, '矿机“小型矿机2型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (45, 2, 1, 0.40, 0, 1596557521, '矿机“小型矿机3型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (46, 3, 1, 2.40, 0, 1596557761, '矿机“小型矿机5型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (47, 3, 1, 0.40, 0, 1596558001, '矿机“小型矿机3型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (48, 3, 1, 1.50, 0, 1596558241, '矿机“小型矿机4型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (49, 3, 1, 1.50, 0, 1596558481, '矿机“小型矿机4型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (50, 3, 1, 1.50, 0, 1596558721, '矿机“小型矿机4型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (51, 3, 1, 1.50, 0, 1596558961, '矿机“小型矿机4型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (52, 3, 1, 1.50, 0, 1596559201, '矿机“小型矿机4型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (53, 3, 1, 1.50, 0, 1596559441, '矿机“小型矿机4型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (54, 3, 1, 0.40, 0, 1596559681, '矿机“小型矿机3型”每日收益');
INSERT INTO `qmgx_mybot` VALUES (55, 4, 1, 1000.00, 0, 1596596734, '管理员新增BOT');
INSERT INTO `qmgx_mybot` VALUES (56, 4, 2, 1000.00, 0, 1596596753, '购买矿机小型矿机');
INSERT INTO `qmgx_mybot` VALUES (57, 10, 1, 10000.00, 0, 1596633885, '管理员新增BOT');
INSERT INTO `qmgx_mybot` VALUES (58, 10, 2, 10000.00, 0, 1596633891, '购买矿机神级矿机');
INSERT INTO `qmgx_mybot` VALUES (59, 10, 1, 10000.00, 0, 1596633914, '管理员新增BOT');
INSERT INTO `qmgx_mybot` VALUES (60, 10, 1, 750.00, 0, 1596643201, '矿机“神级矿机”每日收益');
INSERT INTO `qmgx_mybot` VALUES (61, 4, 1, 3.00, 0, 1596643201, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (62, 10, 1, 3.00, 0, 1596643201, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (63, 8, 1, 2.00, 0, 1596729601, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (64, 10, 1, 750.00, 0, 1596729601, '矿机“神级矿机”每日收益');
INSERT INTO `qmgx_mybot` VALUES (65, 4, 1, 2.00, 0, 1596729601, '订单成交每日分红');
INSERT INTO `qmgx_mybot` VALUES (66, 4, 1, 155222.00, 0, 1599368987, '管理员新增SCB');
INSERT INTO `qmgx_mybot` VALUES (67, 4, 2, 1000.00, 0, 1599369017, '购买矿机小型矿机');
INSERT INTO `qmgx_mybot` VALUES (68, 272, 1, 100.00, 0, 1599547744, '管理员新增SCB');
INSERT INTO `qmgx_mybot` VALUES (69, 272, 2, 0.01, NULL, 1599547749, '商城购物支付');
INSERT INTO `qmgx_mybot` VALUES (70, 272, 2, 0.01, NULL, 1599552196, '商城购物支付');
INSERT INTO `qmgx_mybot` VALUES (71, 272, 1, 0.01, NULL, 1599552553, '商城订单取消');

-- ----------------------------
-- Table structure for qmgx_nav
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_nav`;
CREATE TABLE `qmgx_nav`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简介 或 备注都行',
  `type` int(3) NOT NULL DEFAULT 1 COMMENT '跳转类型',
  `link_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '跳转链接 或者 id',
  `is_visible` int(1) NOT NULL DEFAULT 1 COMMENT '1显示',
  `display_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '导航' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_nav
-- ----------------------------
INSERT INTO `qmgx_nav` VALUES (1, 'http://img.yjkpt.cn/040ba7935d20884198c5ca32aa8b3914.png', '1233', 2, '2', 1, 0, 1599487254, 1599526654);

-- ----------------------------
-- Table structure for qmgx_news
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_news`;
CREATE TABLE `qmgx_news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '新闻标题',
  `cate` tinyint(1) NOT NULL DEFAULT 1 COMMENT '新闻分类 1 服务中心  2 系统消息',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NULL DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '新闻状态 1 显示 2 隐藏',
  `display_order` int(10) NOT NULL COMMENT '展示排序',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '新闻详情',
  `is_rec` tinyint(1) NOT NULL DEFAULT 2 COMMENT '是否首推 1 是 2 否',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '新闻公告' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_news
-- ----------------------------
INSERT INTO `qmgx_news` VALUES (2, '人工智能8月5日开放注册，交易所预计8月8日-8月12开放', 1, 1595383172, 1599146656, 1, 1, '<p>人工智能8月5日开放注册，交易所预计8月8日-8月12开放</p>', 1);
INSERT INTO `qmgx_news` VALUES (4, '第二个', 1, 1599146675, 1599146675, 1, 10, '<p>罚恶打撒大嘎尔法而哇嘎</p>', 2);

-- ----------------------------
-- Table structure for qmgx_order_cancel
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_order_cancel`;
CREATE TABLE `qmgx_order_cancel`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `buyuid` int(11) NULL DEFAULT 0 COMMENT '购买人uid',
  `buyuidatc` int(11) NULL DEFAULT 0 COMMENT '购买人卖出可得atc数量',
  `caifen` int(11) NULL DEFAULT NULL COMMENT '消耗财分',
  `productid` int(11) NULL DEFAULT 0 COMMENT '产品id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名字',
  `price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '购买时候的价格',
  `daynum` int(11) NULL DEFAULT 0 COMMENT '几天后可以卖',
  `earnpercent` int(10) NULL DEFAULT 0 COMMENT '能收益百分之几',
  `dayaddprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '能收益多少',
  `basicprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '卖出后本金和收益能得多少',
  `ctime` int(11) NULL DEFAULT 0 COMMENT '创建时间',
  `peiduistatus` smallint(1) NULL DEFAULT 1 COMMENT '1未配对2已配对',
  `peiduiuid` int(11) NULL DEFAULT 0 COMMENT '购买人(到期后再卖给谁了)',
  `peiduitime` int(11) NULL DEFAULT NULL COMMENT '配对时间',
  `peiduiid` int(11) NULL DEFAULT NULL COMMENT '配对的来源订单信息id',
  `match_pro_id` int(11) NULL DEFAULT NULL COMMENT '配对的商品id',
  `match_pro_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配对的商品名称',
  `match_oid` int(11) NULL DEFAULT NULL COMMENT '售出订单信息id',
  `paystatus` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '0未付款1已付款2已完成',
  `paytype` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用什么付款的',
  `paytime` int(11) NULL DEFAULT 0 COMMENT '付款时间',
  `payimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款图片',
  `cansaletime` int(11) NULL DEFAULT 0 COMMENT '什么时候可以卖出',
  `thatdatetime` int(11) NULL DEFAULT 0 COMMENT '购买当天的时间戳',
  `ordersn` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `buyuid`(`buyuid`) USING BTREE COMMENT '订单售出人信息id',
  INDEX `peiduiuid`(`peiduiuid`) USING BTREE COMMENT '购买订单人信息id'
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '超时未支付订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qmgx_orderbot
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_orderbot`;
CREATE TABLE `qmgx_orderbot`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `orderid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `ctime` int(11) NULL DEFAULT NULL COMMENT '发放时间',
  `state` smallint(1) NULL DEFAULT NULL COMMENT '当前状态1未发放2已发放',
  `proid` int(11) NULL DEFAULT NULL COMMENT '产品id',
  `botnum` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '今日发放bot数量',
  `sendtime` int(11) NULL DEFAULT NULL COMMENT '发放时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_orderbot
-- ----------------------------
INSERT INTO `qmgx_orderbot` VALUES (13, '7', 43, 1596816000, 1, 21, 2.00, NULL);
INSERT INTO `qmgx_orderbot` VALUES (12, '6', 13, 1596816000, 1, 21, 2.00, NULL);
INSERT INTO `qmgx_orderbot` VALUES (11, '5', 4, 1596816000, 1, 21, 2.00, NULL);
INSERT INTO `qmgx_orderbot` VALUES (10, '4', 8, 1596729600, 2, 21, 2.00, NULL);
INSERT INTO `qmgx_orderbot` VALUES (9, '3', 4, 1596729600, 2, 21, 2.00, NULL);

-- ----------------------------
-- Table structure for qmgx_orderearn
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_orderearn`;
CREATE TABLE `qmgx_orderearn`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `orderid` int(11) NULL DEFAULT NULL COMMENT '订单编号',
  `isadd` smallint(1) NULL DEFAULT 1 COMMENT '1加2减',
  `earnmoney` decimal(20, 2) NULL DEFAULT NULL COMMENT '获得收益',
  `dotime` int(11) NULL DEFAULT NULL COMMENT '哪一天可以获得收益',
  `status` smallint(255) NULL DEFAULT NULL COMMENT '1未发放2已发放',
  `ctime` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `sendtime` int(11) NULL DEFAULT 0 COMMENT '发放时间',
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收益补充',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '累计收益' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_orderearn
-- ----------------------------
INSERT INTO `qmgx_orderearn` VALUES (5, 8, 4, 1, 2.20, 20200807, 2, 1596675002, 1596729601, NULL);

-- ----------------------------
-- Table structure for qmgx_points
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_points`;
CREATE TABLE `qmgx_points`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `isadd` tinyint(5) NOT NULL DEFAULT 1 COMMENT '1加2减',
  `nums` int(11) NOT NULL COMMENT '激活币数量',
  `fromuid` int(11) NULL DEFAULT NULL COMMENT '来源id，0表示系统',
  `ctime` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `info` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 332 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '吉分记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_points
-- ----------------------------
INSERT INTO `qmgx_points` VALUES (1, 6, 1, 27400, 0, 1596623211, '管理员新增能量');
INSERT INTO `qmgx_points` VALUES (2, 6, 2, 54900, 0, 1596623226, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (3, 7, 2, 46000, 0, 1596623245, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (4, 8, 2, 7000, 0, 1596623265, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (5, 11, 2, 7250, 0, 1596623294, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (6, 14, 2, 7250, 0, 1596623311, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (7, 20, 2, 10000, 0, 1596623554, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (8, 27, 2, 12400, 0, 1596623634, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (9, 30, 2, 272929, 0, 1596623684, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (10, 32, 2, 12400, 0, 1596623730, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (11, 34, 2, 12380, 0, 1596623743, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (12, 36, 2, 12400, 0, 1596623764, '管理员减少能量');
INSERT INTO `qmgx_points` VALUES (13, 94, 1, 5, 0, 1596623829, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (14, 95, 1, 5, 0, 1596624612, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (15, 37, 1, 5000, 4, 1596625016, '18888888888赠送');
INSERT INTO `qmgx_points` VALUES (16, 4, 2, 5000, 0, 1596625016, '赠送给13770135227');
INSERT INTO `qmgx_points` VALUES (17, 96, 1, 5, 0, 1596625155, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (18, 97, 1, 5, 0, 1596627376, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (19, 98, 1, 5, 0, 1596627561, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (20, 99, 1, 5, 0, 1596628048, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (21, 100, 1, 5, 0, 1596628303, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (22, 101, 1, 5, 0, 1596628890, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (23, 102, 1, 5, 0, 1596630772, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (24, 103, 1, 5, 0, 1596631845, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (25, 104, 1, 5, 0, 1596631933, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (26, 105, 1, 5, 0, 1596631996, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (27, 106, 1, 5, 0, 1596632026, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (28, 107, 1, 5, 0, 1596632081, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (29, 108, 1, 5, 0, 1596632621, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (30, 109, 1, 5, 0, 1596633366, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (31, 10, 2, 2, 0, 1596633999, '预约[测试场次]扣除');
INSERT INTO `qmgx_points` VALUES (32, 110, 1, 5, 0, 1596634728, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (33, 111, 1, 5, 0, 1596635641, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (34, 112, 1, 5, 0, 1596636171, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (35, 113, 1, 5, 0, 1596636440, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (36, 114, 1, 5, 0, 1596636830, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (37, 115, 1, 5, 0, 1596637020, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (38, 81, 1, 10, 20, 1596637029, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (39, 20, 2, 10, 0, 1596637029, '赠送给13818287089');
INSERT INTO `qmgx_points` VALUES (40, 115, 1, 10, 20, 1596637123, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (41, 20, 2, 10, 0, 1596637123, '赠送给15153441278');
INSERT INTO `qmgx_points` VALUES (42, 116, 1, 5, 0, 1596638354, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (43, 104, 1, 50, 16, 1596638980, '15852536233赠送');
INSERT INTO `qmgx_points` VALUES (44, 16, 2, 50, 0, 1596638980, '赠送给18080824501');
INSERT INTO `qmgx_points` VALUES (45, 117, 1, 5, 0, 1596639039, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (46, 117, 1, 10, 20, 1596639886, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (47, 20, 2, 10, 0, 1596639886, '赠送给13037617108');
INSERT INTO `qmgx_points` VALUES (48, 118, 1, 5, 0, 1596640025, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (49, 119, 1, 5, 0, 1596642621, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (50, 120, 1, 5, 0, 1596642972, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (51, 121, 1, 5, 0, 1596644647, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (52, 122, 1, 5, 0, 1596644673, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (53, 123, 1, 5, 0, 1596645963, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (54, 124, 1, 5, 0, 1596647279, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (55, 78, 2, 3, 0, 1596649548, '预约[1号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (56, 78, 2, 5, 0, 1596649560, '预约[2号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (57, 78, 2, 8, 0, 1596649576, '预约[3号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (58, 125, 1, 5, 0, 1596653036, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (59, 126, 1, 5, 0, 1596672770, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (60, 126, 1, 50, 20, 1596673420, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (61, 20, 2, 50, 0, 1596673420, '赠送给18191518314');
INSERT INTO `qmgx_points` VALUES (62, 10, 2, 2, 0, 1596674699, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (63, 6, 2, 2, 0, 1596674715, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (64, 10, 1, 2, 0, 1596674821, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (65, 6, 1, 2, 0, 1596674821, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (66, 10, 2, 2, 0, 1596674833, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (67, 6, 2, 2, 0, 1596674852, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (68, 8, 2, 2, 0, 1596674863, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (69, 10, 1, 2, 0, 1596675002, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (70, 6, 1, 2, 0, 1596675002, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (71, 30, 2, 3, 0, 1596678352, '预约[1号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (72, 30, 2, 5, 0, 1596678356, '预约[2号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (73, 30, 2, 15, 0, 1596678360, '预约[4号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (74, 30, 2, 8, 0, 1596678362, '预约[3号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (75, 30, 2, 20, 0, 1596678364, '预约[5号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (76, 127, 1, 5, 0, 1596678653, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (77, 128, 1, 5, 0, 1596680032, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (78, 129, 1, 5, 0, 1596680436, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (79, 130, 1, 5, 0, 1596681314, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (80, 131, 1, 5, 0, 1596681333, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (81, 131, 1, 100, 20, 1596681805, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (82, 20, 2, 100, 0, 1596681805, '赠送给18780598359');
INSERT INTO `qmgx_points` VALUES (83, 94, 1, 10, 19, 1596681968, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (84, 19, 2, 10, 0, 1596681968, '赠送给13878311535');
INSERT INTO `qmgx_points` VALUES (85, 95, 1, 10, 19, 1596681982, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (86, 19, 2, 10, 0, 1596681982, '赠送给18177314836');
INSERT INTO `qmgx_points` VALUES (87, 96, 1, 10, 19, 1596681995, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (88, 19, 2, 10, 0, 1596681995, '赠送给15877004910');
INSERT INTO `qmgx_points` VALUES (89, 79, 1, 10, 19, 1596682198, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (90, 19, 2, 10, 0, 1596682198, '赠送给13977488752');
INSERT INTO `qmgx_points` VALUES (91, 111, 1, 10, 19, 1596682210, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (92, 19, 2, 10, 0, 1596682210, '赠送给15277438698');
INSERT INTO `qmgx_points` VALUES (93, 108, 1, 10, 19, 1596682221, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (94, 19, 2, 10, 0, 1596682221, '赠送给13607741112');
INSERT INTO `qmgx_points` VALUES (95, 107, 1, 10, 19, 1596682232, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (96, 19, 2, 10, 0, 1596682232, '赠送给13324886313');
INSERT INTO `qmgx_points` VALUES (97, 28, 1, 100, 19, 1596682361, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (98, 19, 2, 100, 0, 1596682361, '赠送给18196160520');
INSERT INTO `qmgx_points` VALUES (99, 132, 1, 5, 0, 1596682389, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (100, 132, 1, 100, 20, 1596682624, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (101, 20, 2, 100, 0, 1596682624, '赠送给13232613109');
INSERT INTO `qmgx_points` VALUES (102, 133, 1, 5, 0, 1596682664, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (103, 134, 1, 5, 0, 1596682822, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (104, 135, 1, 5, 0, 1596683032, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (105, 136, 1, 5, 0, 1596683403, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (106, 134, 1, 100, 20, 1596683470, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (107, 20, 2, 100, 0, 1596683470, '赠送给13612525209');
INSERT INTO `qmgx_points` VALUES (108, 137, 1, 5, 0, 1596683643, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (109, 138, 1, 5, 0, 1596683733, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (110, 139, 1, 5, 0, 1596684535, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (111, 139, 1, 10, 134, 1596684764, '13612525209赠送');
INSERT INTO `qmgx_points` VALUES (112, 134, 2, 10, 0, 1596684764, '赠送给13553993451');
INSERT INTO `qmgx_points` VALUES (113, 116, 1, 10, 19, 1596685684, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (114, 19, 2, 10, 0, 1596685684, '赠送给18670546609');
INSERT INTO `qmgx_points` VALUES (115, 140, 1, 5, 0, 1596685693, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (116, 102, 1, 10, 19, 1596685711, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (117, 19, 2, 10, 0, 1596685711, '赠送给13635133929');
INSERT INTO `qmgx_points` VALUES (118, 129, 1, 10, 19, 1596685782, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (119, 19, 2, 10, 0, 1596685782, '赠送给18060719833');
INSERT INTO `qmgx_points` VALUES (120, 30, 1, 3, 0, 1596686401, '[预约]1号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (121, 78, 1, 3, 0, 1596686401, '[预约]1号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (122, 141, 1, 5, 0, 1596686439, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (123, 141, 1, 100, 19, 1596686743, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (124, 19, 2, 100, 0, 1596686743, '赠送给13565360343');
INSERT INTO `qmgx_points` VALUES (125, 142, 1, 5, 0, 1596686859, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (126, 6, 1, 500, 0, 1596686969, '管理员新增能量');
INSERT INTO `qmgx_points` VALUES (127, 140, 1, 10, 6, 1596686998, '17013226770赠送');
INSERT INTO `qmgx_points` VALUES (128, 6, 2, 10, 0, 1596686998, '赠送给15172612130');
INSERT INTO `qmgx_points` VALUES (129, 143, 1, 5, 0, 1596687897, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (130, 144, 1, 5, 0, 1596688237, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (131, 145, 1, 5, 0, 1596688381, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (132, 27, 1, 10, 4, 1596688997, '18888888888赠送');
INSERT INTO `qmgx_points` VALUES (133, 4, 2, 10, 0, 1596688997, '赠送给15157863554');
INSERT INTO `qmgx_points` VALUES (134, 145, 1, 10, 4, 1596689325, '18888888888赠送');
INSERT INTO `qmgx_points` VALUES (135, 4, 2, 10, 0, 1596689325, '赠送给18778856484');
INSERT INTO `qmgx_points` VALUES (136, 144, 1, 10, 20, 1596689350, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (137, 20, 2, 10, 0, 1596689350, '赠送给16252194704');
INSERT INTO `qmgx_points` VALUES (138, 27, 1, 200, 20, 1596689412, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (139, 20, 2, 200, 0, 1596689412, '赠送给15157863554');
INSERT INTO `qmgx_points` VALUES (140, 144, 1, 10, 27, 1596689562, '15157863554赠送');
INSERT INTO `qmgx_points` VALUES (141, 27, 2, 10, 0, 1596689562, '赠送给16252194704');
INSERT INTO `qmgx_points` VALUES (142, 146, 1, 5, 0, 1596689712, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (143, 87, 1, 10, 19, 1596689780, '18909943315赠送');
INSERT INTO `qmgx_points` VALUES (144, 19, 2, 10, 0, 1596689780, '赠送给18942461602');
INSERT INTO `qmgx_points` VALUES (145, 30, 1, 5, 0, 1596690002, '[预约]2号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (146, 78, 1, 5, 0, 1596690002, '[预约]2号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (147, 147, 1, 5, 0, 1596690240, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (148, 148, 1, 5, 0, 1596690588, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (149, 149, 1, 5, 0, 1596692075, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (150, 150, 1, 5, 0, 1596692566, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (151, 30, 1, 15, 0, 1596693601, '[预约]4号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (152, 151, 1, 5, 0, 1596693799, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (153, 152, 1, 5, 0, 1596694397, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (154, 153, 1, 5, 0, 1596695518, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (155, 30, 1, 8, 0, 1596697201, '[预约]3号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (156, 78, 1, 8, 0, 1596697201, '[预约]3号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (157, 154, 1, 5, 0, 1596697392, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (158, 155, 1, 5, 0, 1596699636, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (159, 30, 1, 20, 0, 1596700802, '[预约]5号机器人-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (160, 156, 1, 5, 0, 1596701323, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (161, 157, 1, 5, 0, 1596706839, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (162, 157, 1, 100, 20, 1596706995, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (163, 20, 2, 100, 0, 1596706995, '赠送给18599145467');
INSERT INTO `qmgx_points` VALUES (164, 158, 1, 5, 0, 1596707800, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (165, 158, 1, 100, 20, 1596708290, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (166, 20, 2, 100, 0, 1596708290, '赠送给13534098898');
INSERT INTO `qmgx_points` VALUES (167, 46, 1, 40, 0, 1596708682, '管理员新增能量');
INSERT INTO `qmgx_points` VALUES (168, 159, 1, 5, 0, 1596709024, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (169, 159, 1, 30, 20, 1596709972, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (170, 20, 2, 30, 0, 1596709972, '赠送给15280892732');
INSERT INTO `qmgx_points` VALUES (171, 160, 1, 5, 0, 1596711627, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (172, 160, 1, 5000, 0, 1596714036, '管理员新增能量');
INSERT INTO `qmgx_points` VALUES (173, 161, 1, 5, 0, 1596717146, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (174, 43, 1, 20, 37, 1596717389, '13770135227赠送');
INSERT INTO `qmgx_points` VALUES (175, 37, 2, 20, 0, 1596717389, '赠送给13193741098');
INSERT INTO `qmgx_points` VALUES (176, 162, 1, 5, 0, 1596717478, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (177, 163, 1, 5, 0, 1596718398, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (178, 164, 1, 5, 0, 1596718909, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (179, 161, 1, 100, 20, 1596719402, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (180, 20, 2, 100, 0, 1596719402, '赠送给16625134519');
INSERT INTO `qmgx_points` VALUES (181, 41, 1, 100, 4, 1596719488, '18888888888赠送');
INSERT INTO `qmgx_points` VALUES (182, 4, 2, 100, 0, 1596719488, '赠送给13772145686');
INSERT INTO `qmgx_points` VALUES (183, 163, 1, 10, 161, 1596719576, '16625134519赠送');
INSERT INTO `qmgx_points` VALUES (184, 161, 2, 10, 0, 1596719576, '赠送给18575599846');
INSERT INTO `qmgx_points` VALUES (185, 163, 1, 100, 20, 1596719699, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (186, 20, 2, 100, 0, 1596719699, '赠送给18575599846');
INSERT INTO `qmgx_points` VALUES (187, 165, 1, 5, 0, 1596722769, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (188, 166, 1, 5, 0, 1596722810, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (189, 165, 1, 100, 20, 1596723142, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (190, 20, 2, 100, 0, 1596723142, '赠送给15113573121');
INSERT INTO `qmgx_points` VALUES (191, 167, 1, 5, 0, 1596723565, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (192, 168, 1, 5, 0, 1596724259, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (193, 169, 1, 5, 0, 1596724723, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (194, 168, 1, 10, 161, 1596724797, '16625134519赠送');
INSERT INTO `qmgx_points` VALUES (195, 161, 2, 10, 0, 1596724797, '赠送给13210376667');
INSERT INTO `qmgx_points` VALUES (196, 170, 1, 5, 0, 1596724808, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (197, 171, 1, 5, 0, 1596725297, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (198, 172, 1, 5, 0, 1596725758, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (199, 173, 1, 5, 0, 1596726289, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (200, 174, 1, 5, 0, 1596726877, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (201, 175, 1, 5, 0, 1596727347, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (202, 176, 1, 5, 0, 1596727545, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (203, 177, 1, 5, 0, 1596727853, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (204, 178, 1, 5, 0, 1596728018, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (205, 179, 1, 5, 0, 1596728373, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (206, 180, 1, 5, 0, 1596728976, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (207, 181, 1, 5, 0, 1596729135, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (208, 182, 1, 5, 0, 1596729499, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (209, 183, 1, 5, 0, 1596729856, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (210, 184, 1, 5, 0, 1596730265, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (211, 185, 1, 5, 0, 1596730353, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (212, 186, 1, 5, 0, 1596730667, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (213, 187, 1, 5, 0, 1596730686, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (214, 188, 1, 5, 0, 1596730980, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (215, 189, 1, 5, 0, 1596731294, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (216, 190, 1, 5, 0, 1596731348, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (217, 191, 1, 5, 0, 1596731644, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (218, 192, 1, 5, 0, 1596731746, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (219, 193, 1, 5, 0, 1596731959, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (220, 194, 1, 5, 0, 1596732131, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (221, 195, 1, 5, 0, 1596732275, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (222, 196, 1, 5, 0, 1596732590, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (223, 197, 1, 5, 0, 1596732902, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (224, 198, 1, 5, 0, 1596732946, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (225, 199, 1, 5, 0, 1596733217, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (226, 200, 1, 5, 0, 1596733361, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (227, 201, 1, 5, 0, 1596733529, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (228, 202, 1, 5, 0, 1596733713, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (229, 203, 1, 5, 0, 1596733851, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (230, 204, 1, 5, 0, 1596734081, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (231, 205, 1, 5, 0, 1596734818, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (232, 206, 1, 5, 0, 1596734899, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (233, 207, 1, 5, 0, 1596735214, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (234, 208, 1, 5, 0, 1596735530, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (235, 209, 1, 5, 0, 1596735709, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (236, 210, 1, 5, 0, 1596735843, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (237, 211, 1, 5, 0, 1596736528, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (238, 212, 1, 5, 0, 1596736849, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (239, 213, 1, 5, 0, 1596737162, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (240, 214, 1, 5, 0, 1596737475, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (241, 215, 1, 5, 0, 1596738161, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (242, 216, 1, 5, 0, 1596738472, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (243, 217, 1, 5, 0, 1596738783, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (244, 218, 1, 5, 0, 1596739096, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (245, 219, 1, 5, 0, 1596739405, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (246, 220, 1, 5, 0, 1596739730, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (247, 221, 1, 5, 0, 1596740410, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (248, 222, 1, 5, 0, 1596740722, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (249, 223, 1, 5, 0, 1596741032, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (250, 224, 1, 5, 0, 1596741344, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (251, 225, 1, 5, 0, 1596741654, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (252, 226, 1, 5, 0, 1596741967, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (253, 227, 1, 5, 0, 1596742282, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (254, 228, 1, 5, 0, 1596742603, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (255, 229, 1, 5, 0, 1596742914, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (256, 230, 1, 5, 0, 1596743598, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (257, 231, 1, 5, 0, 1596743911, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (258, 232, 1, 5, 0, 1596753573, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (259, 233, 1, 5, 0, 1596754274, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (260, 234, 1, 5, 0, 1596754585, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (261, 43, 2, 3, 0, 1596754740, '预约[1号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (262, 43, 2, 5, 0, 1596754762, '预约[2号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (263, 43, 2, 2, 0, 1596754787, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (264, 235, 1, 5, 0, 1596754894, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (265, 236, 1, 5, 0, 1596756972, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (266, 237, 1, 5, 0, 1596757301, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (267, 238, 1, 5, 0, 1596757616, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (268, 239, 1, 5, 0, 1596757929, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (269, 240, 1, 5, 0, 1596758239, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (270, 14, 2, 2, 0, 1596758378, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (271, 13, 2, 2, 0, 1596758391, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (272, 10, 2, 2, 0, 1596758408, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (273, 6, 2, 2, 0, 1596758419, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (274, 241, 1, 5, 0, 1596758550, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (275, 242, 1, 5, 0, 1596758861, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (276, 8, 2, 2, 0, 1596759090, '预约[测试机器人勿拍]扣除');
INSERT INTO `qmgx_points` VALUES (277, 243, 1, 5, 0, 1596759174, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (278, 244, 1, 5, 0, 1596759485, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (279, 245, 1, 5, 0, 1596759809, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (280, 246, 1, 5, 0, 1596760121, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (281, 247, 1, 5, 0, 1596760433, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (282, 248, 1, 5, 0, 1596760749, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (283, 249, 1, 5, 0, 1596761060, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (284, 250, 1, 5, 0, 1596761102, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (285, 251, 1, 5, 0, 1596761371, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (286, 8, 1, 2, 0, 1596761403, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (287, 6, 1, 2, 0, 1596761403, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (288, 10, 1, 2, 0, 1596761403, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (289, 14, 1, 2, 0, 1596761403, '[预约]测试机器人勿拍-抢购失败退还');
INSERT INTO `qmgx_points` VALUES (290, 250, 1, 10, 161, 1596761496, '16625134519赠送');
INSERT INTO `qmgx_points` VALUES (291, 161, 2, 10, 0, 1596761496, '赠送给17875179993');
INSERT INTO `qmgx_points` VALUES (292, 252, 1, 5, 0, 1596761684, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (293, 253, 1, 5, 0, 1596761994, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (294, 254, 1, 5, 0, 1596762307, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (295, 255, 1, 5, 0, 1596762621, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (296, 256, 1, 5, 0, 1596762933, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (297, 98, 1, 100, 4, 1596763032, '18888888888赠送');
INSERT INTO `qmgx_points` VALUES (298, 4, 2, 100, 0, 1596763033, '赠送给18808108991');
INSERT INTO `qmgx_points` VALUES (299, 257, 1, 5, 0, 1596763246, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (300, 258, 1, 5, 0, 1596763533, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (301, 36, 2, 3, 0, 1596763841, '预约[1号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (302, 259, 1, 5, 0, 1596763926, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (303, 260, 1, 5, 0, 1596764238, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (304, 261, 1, 5, 0, 1596764245, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (305, 46, 2, 3, 0, 1596764263, '预约[1号机器人]扣除');
INSERT INTO `qmgx_points` VALUES (306, 262, 1, 5, 0, 1596764388, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (307, 263, 1, 5, 0, 1596764550, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (308, 264, 1, 5, 0, 1596764594, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (309, 265, 1, 5, 0, 1596764633, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (310, 265, 1, 100, 20, 1596764751, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (311, 20, 2, 100, 0, 1596764751, '赠送给13553398421');
INSERT INTO `qmgx_points` VALUES (312, 266, 1, 5, 0, 1596764878, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (313, 262, 1, 100, 20, 1596764982, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (314, 20, 2, 100, 0, 1596764982, '赠送给15501598007');
INSERT INTO `qmgx_points` VALUES (315, 261, 1, 100, 20, 1596764992, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (316, 20, 2, 100, 0, 1596764992, '赠送给15083651085');
INSERT INTO `qmgx_points` VALUES (317, 22, 1, 1000, 20, 1596765005, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (318, 20, 2, 1000, 0, 1596765005, '赠送给18094444666');
INSERT INTO `qmgx_points` VALUES (319, 267, 1, 5, 0, 1596765189, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (320, 22, 1, 500, 20, 1596765445, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (321, 20, 2, 500, 0, 1596765445, '赠送给18094444666');
INSERT INTO `qmgx_points` VALUES (322, 268, 1, 5, 0, 1596765500, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (323, 170, 1, 10, 20, 1596765895, '15523518952赠送');
INSERT INTO `qmgx_points` VALUES (324, 20, 2, 10, 0, 1596765895, '赠送给18703614501');
INSERT INTO `qmgx_points` VALUES (325, 269, 1, 5, 0, 1596767161, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (326, 270, 1, 5, 0, 1596768022, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (327, 271, 1, 5, 0, 1596768810, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (328, 272, 1, 5, 0, 1599142941, '注册赠送能量');
INSERT INTO `qmgx_points` VALUES (329, 272, 1, 50, 4, 1599233126, '18888888888赠送');
INSERT INTO `qmgx_points` VALUES (330, 4, 2, 50, 0, 1599233126, '赠送给15554832010');
INSERT INTO `qmgx_points` VALUES (331, 273, 1, 5, 0, 1599339056, '注册赠送能量');

-- ----------------------------
-- Table structure for qmgx_pushslist
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_pushslist`;
CREATE TABLE `qmgx_pushslist`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `isadd` tinyint(5) NOT NULL DEFAULT 1 COMMENT '1加2减',
  `nums` decimal(20, 2) NOT NULL COMMENT '激活币数量',
  `fromuid` int(11) NULL DEFAULT NULL COMMENT '来源id，0表示系统',
  `ctime` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `info` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推广收益' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_pushslist
-- ----------------------------
INSERT INTO `qmgx_pushslist` VALUES (1, 5, 1, 8.40, 0, 1596643201, '一级下级益哥订单成交');
INSERT INTO `qmgx_pushslist` VALUES (2, 4, 1, 5.25, 0, 1596643201, '二级下级益哥订单成交');
INSERT INTO `qmgx_pushslist` VALUES (3, 6, 1, 0.18, 0, 1596729601, '一级下级厚德载物5号订单成交');
INSERT INTO `qmgx_pushslist` VALUES (4, 5, 1, 0.11, 0, 1596729601, '二级下级厚德载物5号订单成交');
INSERT INTO `qmgx_pushslist` VALUES (5, 4, 1, 0.07, 0, 1596729601, '三级下级厚德载物5号订单成交');

-- ----------------------------
-- Table structure for qmgx_qfbaboziex
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_qfbaboziex`;
CREATE TABLE `qmgx_qfbaboziex`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `buyuid` int(11) NULL DEFAULT 0 COMMENT '购买人uid',
  `buyuidatc` int(11) NULL DEFAULT 0 COMMENT '购买人卖出可得atc数量',
  `caifen` int(11) NULL DEFAULT NULL COMMENT '消耗财分',
  `productid` int(11) NULL DEFAULT 0 COMMENT '产品id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名字',
  `price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '购买时候的价格',
  `daynum` int(11) NULL DEFAULT 0 COMMENT '几天后可以卖',
  `earnpercent` int(10) NULL DEFAULT 0 COMMENT '能收益百分之几',
  `dayaddprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '能收益多少',
  `basicprice` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '卖出后本金和收益能得多少',
  `ctime` int(11) NULL DEFAULT 0 COMMENT '创建时间',
  `peiduistatus` smallint(1) NULL DEFAULT 1 COMMENT '1未配对2已配对',
  `peiduiuid` int(11) NULL DEFAULT 0 COMMENT '购买人(到期后再卖给谁了)',
  `peiduitime` int(11) NULL DEFAULT NULL COMMENT '配对时间',
  `peiduiid` int(11) NULL DEFAULT NULL COMMENT '配对的来源订单信息id',
  `match_pro_id` int(11) NULL DEFAULT NULL COMMENT '配对的商品id',
  `match_pro_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配对的商品名称',
  `match_oid` int(11) NULL DEFAULT NULL COMMENT '售出订单信息id',
  `paystatus` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '0未付款1已付款2已完成',
  `paytype` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用什么付款的',
  `paytime` int(11) NULL DEFAULT 0 COMMENT '付款时间',
  `payimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款图片',
  `cansaletime` int(11) NULL DEFAULT 0 COMMENT '什么时候可以卖出',
  `thatdatetime` int(11) NULL DEFAULT 0 COMMENT '购买当天的时间戳',
  `ordersn` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isread` smallint(1) NOT NULL DEFAULT 1 COMMENT '1未读2已读',
  `myread` smallint(1) NOT NULL DEFAULT 1 COMMENT '另一个已读',
  `applyid` int(11) NOT NULL DEFAULT 0 COMMENT '预约表的id',
  `xs` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否显示',
  `issendbot` smallint(1) UNSIGNED NULL DEFAULT 1 COMMENT '1未发放2已发放',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `buyuid`(`buyuid`) USING BTREE COMMENT '订单售出人信息id',
  INDEX `peiduiuid`(`peiduiuid`) USING BTREE COMMENT '购买订单人信息id'
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_qfbaboziex
-- ----------------------------
INSERT INTO `qmgx_qfbaboziex` VALUES (3, 4, 0, 999, 21, '测试机器人勿拍', 217.82, 1, 1, 2.18, 220.00, 1596588476, 2, 8, 1596675001, NULL, 21, '测试机器人勿拍', 4, '2', '支付宝', 1596678874, 'http://img.yizitao.cn/a6af845d0395159024706accee7262ebblob', 1596643200, 1596556800, '', 2, 2, 9, 2, 2);
INSERT INTO `qmgx_qfbaboziex` VALUES (4, 8, 0, 2, 21, '测试机器人勿拍', 220.00, 1, 1, 2.20, 222.00, 1596675002, 1, 0, 0, 3, 0, '0', 0, '0', '', 0, '', 1596729600, 1596643200, '', 2, 1, 0, 2, 2);
INSERT INTO `qmgx_qfbaboziex` VALUES (5, 4, 0, 999, 21, '测试机器人勿拍', 207.92, 1, 1, 2.08, 210.00, 1596672666, 1, 0, 0, NULL, 0, '0', 0, '0', '', 0, '', 1596729600, 1596643200, '', 2, 2, 33, 2, 2);

-- ----------------------------
-- Table structure for qmgx_region
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_region`;
CREATE TABLE `qmgx_region`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` double NOT NULL,
  `level` double NOT NULL,
  `order_num` double NOT NULL,
  `name_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `short_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `id_path` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id路径',
  `name_path` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `is_visible` tinyint(2) NOT NULL DEFAULT 1 COMMENT '暂时不用了 是否开启城市站点  0关闭 1显示  主要用于省市一级 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5063 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '全国省市区' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_region
-- ----------------------------
INSERT INTO `qmgx_region` VALUES (1, '中国', '中国', 0, 0, 0, 'Zhong Guo', '2', '1', '中国', 1);
INSERT INTO `qmgx_region` VALUES (2, '110000', '北京市', 1, 1, 0, 'Beijing Shi', 'BJ', '1-2', '北京市', 1);
INSERT INTO `qmgx_region` VALUES (3, '120000', '天津市', 1, 1, 0, 'Tianjin Shi', 'TJ', '1-3', '天津市', 1);
INSERT INTO `qmgx_region` VALUES (4, '130000', '河北省', 1, 1, 0, 'Hebei Sheng', 'HE', '1-4', '河北省', 1);
INSERT INTO `qmgx_region` VALUES (5, '140000', '山西省', 1, 1, 0, 'Shanxi Sheng ', 'SX', '1-5', '山西省', 1);
INSERT INTO `qmgx_region` VALUES (6, '150000', '内蒙古自治区', 1, 1, 0, 'Nei Mongol Zizhiqu', 'NM', '1-6', '内蒙古自治区', 1);
INSERT INTO `qmgx_region` VALUES (7, '210000', '辽宁省', 1, 1, 0, 'Liaoning Sheng', 'LN', '1-7', '辽宁省', 1);
INSERT INTO `qmgx_region` VALUES (8, '220000', '吉林省', 1, 1, 0, 'Jilin Sheng', 'JL', '1-8', '吉林省', 1);
INSERT INTO `qmgx_region` VALUES (9, '230000', '黑龙江省', 1, 1, 0, 'Heilongjiang Sheng', 'HL', '1-9', '黑龙江省', 1);
INSERT INTO `qmgx_region` VALUES (10, '310000', '上海市', 1, 1, 0, 'Shanghai Shi', 'SH', '1-10', '上海市', 1);
INSERT INTO `qmgx_region` VALUES (11, '320000', '江苏省', 1, 1, 0, 'Jiangsu Sheng', 'JS', '1-11', '江苏省', 1);
INSERT INTO `qmgx_region` VALUES (12, '330000', '浙江省', 1, 1, 0, 'Zhejiang Sheng', 'ZJ', '1-12', '浙江省', 1);
INSERT INTO `qmgx_region` VALUES (13, '340000', '安徽省', 1, 1, 0, 'Anhui Sheng', 'AH', '1-13', '安徽省', 1);
INSERT INTO `qmgx_region` VALUES (14, '350000', '福建省', 1, 1, 0, 'Fujian Sheng ', 'FJ', '1-14', '福建省', 1);
INSERT INTO `qmgx_region` VALUES (15, '360000', '江西省', 1, 1, 0, 'Jiangxi Sheng', 'JX', '1-15', '江西省', 1);
INSERT INTO `qmgx_region` VALUES (16, '370000', '山东省', 1, 1, 0, 'Shandong Sheng ', 'SD', '1-16', '山东省', 1);
INSERT INTO `qmgx_region` VALUES (17, '410000', '河南省', 1, 1, 0, 'Henan Sheng', 'HA', '1-17', '河南省', 1);
INSERT INTO `qmgx_region` VALUES (18, '420000', '湖北省', 1, 1, 0, 'Hubei Sheng', 'HB', '1-18', '湖北省', 1);
INSERT INTO `qmgx_region` VALUES (19, '430000', '湖南省', 1, 1, 0, 'Hunan Sheng', 'HN', '1-19', '湖南省', 1);
INSERT INTO `qmgx_region` VALUES (20, '440000', '广东省', 1, 1, 0, 'Guangdong Sheng', 'GD', '1-20', '广东省', 1);
INSERT INTO `qmgx_region` VALUES (21, '450000', '广西壮族自治区', 1, 1, 0, 'Guangxi Zhuangzu Zizhiqu', 'GX', '1-21', '广西壮族自治区', 1);
INSERT INTO `qmgx_region` VALUES (22, '460000', '海南省', 1, 1, 0, 'Hainan Sheng', 'HI', '1-22', '海南省', 1);
INSERT INTO `qmgx_region` VALUES (23, '500000', '重庆市', 1, 1, 0, 'Chongqing Shi', 'CQ', '1-23', '重庆市', 1);
INSERT INTO `qmgx_region` VALUES (24, '510000', '四川省', 1, 1, 0, 'Sichuan Sheng', 'SC', '1-24', '四川省', 1);
INSERT INTO `qmgx_region` VALUES (25, '520000', '贵州省', 1, 1, 0, 'Guizhou Sheng', 'GZ', '1-25', '贵州省', 1);
INSERT INTO `qmgx_region` VALUES (26, '530000', '云南省', 1, 1, 0, 'Yunnan Sheng', 'YN', '1-26', '云南省', 1);
INSERT INTO `qmgx_region` VALUES (27, '540000', '西藏自治区', 1, 1, 0, 'Xizang Zizhiqu', 'XZ', '1-27', '西藏自治区', 1);
INSERT INTO `qmgx_region` VALUES (28, '610000', '陕西省', 1, 1, 0, 'Shanxi Sheng ', 'SN', '1-28', '陕西省', 1);
INSERT INTO `qmgx_region` VALUES (29, '620000', '甘肃省', 1, 1, 0, 'Gansu Sheng', 'GS', '1-29', '甘肃省', 1);
INSERT INTO `qmgx_region` VALUES (30, '630000', '青海省', 1, 1, 0, 'Qinghai Sheng', 'QH', '1-30', '青海省', 1);
INSERT INTO `qmgx_region` VALUES (31, '640000', '宁夏回族自治区', 1, 1, 0, 'Ningxia Huizu Zizhiqu', 'NX', '1-31', '宁夏回族自治区', 1);
INSERT INTO `qmgx_region` VALUES (32, '650000', '新疆维吾尔自治区', 1, 1, 0, 'Xinjiang Uygur Zizhiqu', 'XJ', '1-32', '新疆维吾尔自治区', 1);
INSERT INTO `qmgx_region` VALUES (33, '110100', '北京市', 2, 2, 0, 'BeiJing', '2', '1-2-33', '北京市-北京', 1);
INSERT INTO `qmgx_region` VALUES (35, '120100', '天津市', 3, 2, 0, 'TianJin', '2', '1-3-35', '天津市-天津', 1);
INSERT INTO `qmgx_region` VALUES (37, '130100', '石家庄市', 4, 2, 0, 'Shijiazhuang Shi', 'SJW', '1-4-37', '河北省-石家庄市', 1);
INSERT INTO `qmgx_region` VALUES (38, '130200', '唐山市', 4, 2, 0, 'Tangshan Shi', 'TGS', '1-4-38', '河北省-唐山市', 1);
INSERT INTO `qmgx_region` VALUES (39, '130300', '秦皇岛市', 4, 2, 0, 'Qinhuangdao Shi', 'SHP', '1-4-39', '河北省-秦皇岛市', 1);
INSERT INTO `qmgx_region` VALUES (40, '130400', '邯郸市', 4, 2, 0, 'Handan Shi', 'HDS', '1-4-40', '河北省-邯郸市', 1);
INSERT INTO `qmgx_region` VALUES (41, '130500', '邢台市', 4, 2, 0, 'Xingtai Shi', 'XTS', '1-4-41', '河北省-邢台市', 1);
INSERT INTO `qmgx_region` VALUES (42, '130600', '保定市', 4, 2, 0, 'Baoding Shi', 'BDS', '1-4-42', '河北省-保定市', 1);
INSERT INTO `qmgx_region` VALUES (43, '130700', '张家口市', 4, 2, 0, 'Zhangjiakou Shi ', 'ZJK', '1-4-43', '河北省-张家口市', 1);
INSERT INTO `qmgx_region` VALUES (44, '130800', '承德市', 4, 2, 0, 'Chengde Shi', 'CDS', '1-4-44', '河北省-承德市', 1);
INSERT INTO `qmgx_region` VALUES (45, '130900', '沧州市', 4, 2, 0, 'Cangzhou Shi', 'CGZ', '1-4-45', '河北省-沧州市', 1);
INSERT INTO `qmgx_region` VALUES (46, '131000', '廊坊市', 4, 2, 0, 'Langfang Shi', 'LFS', '1-4-46', '河北省-廊坊市', 1);
INSERT INTO `qmgx_region` VALUES (47, '131100', '衡水市', 4, 2, 0, 'Hengshui Shi ', 'HGS', '1-4-47', '河北省-衡水市', 1);
INSERT INTO `qmgx_region` VALUES (48, '140100', '太原市', 5, 2, 0, 'Taiyuan Shi', 'TYN', '1-5-48', '山西省-太原市', 1);
INSERT INTO `qmgx_region` VALUES (49, '140200', '大同市', 5, 2, 0, 'Datong Shi ', 'DTG', '1-5-49', '山西省-大同市', 1);
INSERT INTO `qmgx_region` VALUES (50, '140300', '阳泉市', 5, 2, 0, 'Yangquan Shi', 'YQS', '1-5-50', '山西省-阳泉市', 1);
INSERT INTO `qmgx_region` VALUES (51, '140400', '长治市', 5, 2, 0, 'Changzhi Shi', 'CZS', '1-5-51', '山西省-长治市', 1);
INSERT INTO `qmgx_region` VALUES (52, '140500', '晋城市', 5, 2, 0, 'Jincheng Shi ', 'JCG', '1-5-52', '山西省-晋城市', 1);
INSERT INTO `qmgx_region` VALUES (53, '140600', '朔州市', 5, 2, 0, 'Shuozhou Shi ', 'SZJ', '1-5-53', '山西省-朔州市', 1);
INSERT INTO `qmgx_region` VALUES (54, '140700', '晋中市', 5, 2, 0, 'Jinzhong Shi', '2', '1-5-54', '山西省-晋中市', 1);
INSERT INTO `qmgx_region` VALUES (55, '140800', '运城市', 5, 2, 0, 'Yuncheng Shi', '2', '1-5-55', '山西省-运城市', 1);
INSERT INTO `qmgx_region` VALUES (56, '140900', '忻州市', 5, 2, 0, 'Xinzhou Shi', '2', '1-5-56', '山西省-忻州市', 1);
INSERT INTO `qmgx_region` VALUES (57, '141000', '临汾市', 5, 2, 0, 'Linfen Shi', '2', '1-5-57', '山西省-临汾市', 1);
INSERT INTO `qmgx_region` VALUES (58, '141100', '吕梁市', 5, 2, 0, 'Lvliang Shi', '2', '1-5-58', '山西省-吕梁市', 1);
INSERT INTO `qmgx_region` VALUES (59, '150100', '呼和浩特市', 6, 2, 0, 'Hohhot Shi', 'Hhht', '1-6-59', '内蒙古自治区-呼和浩特市', 1);
INSERT INTO `qmgx_region` VALUES (60, '150200', '包头市', 6, 2, 0, 'Baotou Shi ', 'BTS', '1-6-60', '内蒙古自治区-包头市', 1);
INSERT INTO `qmgx_region` VALUES (61, '150300', '乌海市', 6, 2, 0, 'Wuhai Shi', 'WHM', '1-6-61', '内蒙古自治区-乌海市', 1);
INSERT INTO `qmgx_region` VALUES (62, '150400', '赤峰市', 6, 2, 0, 'Chifeng (Ulanhad)Shi', 'CFS', '1-6-62', '内蒙古自治区-赤峰市', 1);
INSERT INTO `qmgx_region` VALUES (63, '150500', '通辽市', 6, 2, 0, 'Tongliao Shi', '2', '1-6-63', '内蒙古自治区-通辽市', 1);
INSERT INTO `qmgx_region` VALUES (64, '150600', '鄂尔多斯市', 6, 2, 0, 'Eerduosi Shi', '2', '1-6-64', '内蒙古自治区-鄂尔多斯市', 1);
INSERT INTO `qmgx_region` VALUES (65, '150700', '呼伦贝尔市', 6, 2, 0, 'Hulunbeier Shi ', '2', '1-6-65', '内蒙古自治区-呼伦贝尔市', 1);
INSERT INTO `qmgx_region` VALUES (66, '150800', '巴彦淖尔市', 6, 2, 0, 'Bayannaoer Shi', '2', '1-6-66', '内蒙古自治区-巴彦淖尔市', 1);
INSERT INTO `qmgx_region` VALUES (67, '150900', '乌兰察布市', 6, 2, 0, 'Wulanchabu Shi', '2', '1-6-67', '内蒙古自治区-乌兰察布市', 1);
INSERT INTO `qmgx_region` VALUES (68, '152200', '兴安盟', 6, 2, 0, 'Hinggan Meng', 'HIN', '1-6-68', '内蒙古自治区-兴安盟', 1);
INSERT INTO `qmgx_region` VALUES (69, '152500', '锡林郭勒盟', 6, 2, 0, 'Xilin Gol Meng', 'XGO', '1-6-69', '内蒙古自治区-锡林郭勒盟', 1);
INSERT INTO `qmgx_region` VALUES (70, '152900', '阿拉善盟', 6, 2, 0, 'Alxa Meng', 'ALM', '1-6-70', '内蒙古自治区-阿拉善盟', 1);
INSERT INTO `qmgx_region` VALUES (71, '210100', '沈阳市', 7, 2, 0, 'Shenyang Shi', 'SHE', '1-7-71', '辽宁省-沈阳市', 1);
INSERT INTO `qmgx_region` VALUES (72, '210200', '大连市', 7, 2, 0, 'Dalian Shi', 'DLC', '1-7-72', '辽宁省-大连市', 1);
INSERT INTO `qmgx_region` VALUES (73, '210300', '鞍山市', 7, 2, 0, 'AnShan Shi', 'ASN', '1-7-73', '辽宁省-鞍山市', 1);
INSERT INTO `qmgx_region` VALUES (74, '210400', '抚顺市', 7, 2, 0, 'Fushun Shi', 'FSN', '1-7-74', '辽宁省-抚顺市', 1);
INSERT INTO `qmgx_region` VALUES (75, '210500', '本溪市', 7, 2, 0, 'Benxi Shi', 'BXS', '1-7-75', '辽宁省-本溪市', 1);
INSERT INTO `qmgx_region` VALUES (76, '210600', '丹东市', 7, 2, 0, 'Dandong Shi', 'DDG', '1-7-76', '辽宁省-丹东市', 1);
INSERT INTO `qmgx_region` VALUES (77, '210700', '锦州市', 7, 2, 0, 'Jinzhou Shi', 'JNZ', '1-7-77', '辽宁省-锦州市', 1);
INSERT INTO `qmgx_region` VALUES (78, '210800', '营口市', 7, 2, 0, 'Yingkou Shi', 'YIK', '1-7-78', '辽宁省-营口市', 1);
INSERT INTO `qmgx_region` VALUES (79, '210900', '阜新市', 7, 2, 0, 'Fuxin Shi', 'FXS', '1-7-79', '辽宁省-阜新市', 1);
INSERT INTO `qmgx_region` VALUES (80, '211000', '辽阳市', 7, 2, 0, 'Liaoyang Shi', 'LYL', '1-7-80', '辽宁省-辽阳市', 1);
INSERT INTO `qmgx_region` VALUES (81, '211100', '盘锦市', 7, 2, 0, 'Panjin Shi', 'PJS', '1-7-81', '辽宁省-盘锦市', 1);
INSERT INTO `qmgx_region` VALUES (82, '211200', '铁岭市', 7, 2, 0, 'Tieling Shi', 'TLS', '1-7-82', '辽宁省-铁岭市', 1);
INSERT INTO `qmgx_region` VALUES (83, '211300', '朝阳市', 7, 2, 0, 'Chaoyang Shi', 'CYS', '1-7-83', '辽宁省-朝阳市', 1);
INSERT INTO `qmgx_region` VALUES (84, '211400', '葫芦岛市', 7, 2, 0, 'Huludao Shi', 'HLD', '1-7-84', '辽宁省-葫芦岛市', 1);
INSERT INTO `qmgx_region` VALUES (85, '220100', '长春市', 8, 2, 0, 'Changchun Shi ', 'CGQ', '1-8-85', '吉林省-长春市', 1);
INSERT INTO `qmgx_region` VALUES (86, '220200', '吉林市', 8, 2, 0, 'Jilin Shi ', 'JLS', '1-8-86', '吉林省-吉林市', 1);
INSERT INTO `qmgx_region` VALUES (87, '220300', '四平市', 8, 2, 0, 'Siping Shi', 'SPS', '1-8-87', '吉林省-四平市', 1);
INSERT INTO `qmgx_region` VALUES (88, '220400', '辽源市', 8, 2, 0, 'Liaoyuan Shi', 'LYH', '1-8-88', '吉林省-辽源市', 1);
INSERT INTO `qmgx_region` VALUES (89, '220500', '通化市', 8, 2, 0, 'Tonghua Shi', 'THS', '1-8-89', '吉林省-通化市', 1);
INSERT INTO `qmgx_region` VALUES (90, '220600', '白山市', 8, 2, 0, 'Baishan Shi', 'BSN', '1-8-90', '吉林省-白山市', 1);
INSERT INTO `qmgx_region` VALUES (91, '220700', '松原市', 8, 2, 0, 'Songyuan Shi', 'SYU', '1-8-91', '吉林省-松原市', 1);
INSERT INTO `qmgx_region` VALUES (92, '220800', '白城市', 8, 2, 0, 'Baicheng Shi', 'BCS', '1-8-92', '吉林省-白城市', 1);
INSERT INTO `qmgx_region` VALUES (93, '222400', '延边朝鲜族自治州', 8, 2, 0, 'Yanbian Chosenzu Zizhizhou', 'YBZ', '1-8-93', '吉林省-延边朝鲜族自治州', 1);
INSERT INTO `qmgx_region` VALUES (94, '230100', '哈尔滨市', 9, 2, 0, 'Harbin Shi', 'HRB', '1-9-94', '黑龙江省-哈尔滨市', 1);
INSERT INTO `qmgx_region` VALUES (95, '230200', '齐齐哈尔市', 9, 2, 0, 'Qiqihar Shi', 'NDG', '1-9-95', '黑龙江省-齐齐哈尔市', 1);
INSERT INTO `qmgx_region` VALUES (96, '230300', '鸡西市', 9, 2, 0, 'Jixi Shi', 'JXI', '1-9-96', '黑龙江省-鸡西市', 1);
INSERT INTO `qmgx_region` VALUES (97, '230400', '鹤岗市', 9, 2, 0, 'Hegang Shi', 'HEG', '1-9-97', '黑龙江省-鹤岗市', 1);
INSERT INTO `qmgx_region` VALUES (98, '230500', '双鸭山市', 9, 2, 0, 'Shuangyashan Shi', 'SYS', '1-9-98', '黑龙江省-双鸭山市', 1);
INSERT INTO `qmgx_region` VALUES (99, '230600', '大庆市', 9, 2, 0, 'Daqing Shi', 'DQG', '1-9-99', '黑龙江省-大庆市', 1);
INSERT INTO `qmgx_region` VALUES (100, '230700', '伊春市', 9, 2, 0, 'Yichun Shi', 'YCH', '1-9-100', '黑龙江省-伊春市', 1);
INSERT INTO `qmgx_region` VALUES (101, '230800', '佳木斯市', 9, 2, 0, 'Jiamusi Shi', 'JMU', '1-9-101', '黑龙江省-佳木斯市', 1);
INSERT INTO `qmgx_region` VALUES (102, '230900', '七台河市', 9, 2, 0, 'Qitaihe Shi', 'QTH', '1-9-102', '黑龙江省-七台河市', 1);
INSERT INTO `qmgx_region` VALUES (103, '231000', '牡丹江市', 9, 2, 0, 'Mudanjiang Shi', 'MDG', '1-9-103', '黑龙江省-牡丹江市', 1);
INSERT INTO `qmgx_region` VALUES (104, '231100', '黑河市', 9, 2, 0, 'Heihe Shi', 'HEK', '1-9-104', '黑龙江省-黑河市', 1);
INSERT INTO `qmgx_region` VALUES (105, '231200', '绥化市', 9, 2, 0, 'Suihua Shi', '2', '1-9-105', '黑龙江省-绥化市', 1);
INSERT INTO `qmgx_region` VALUES (106, '232700', '大兴安岭地区', 9, 2, 0, 'Da Hinggan Ling Diqu', 'DHL', '1-9-106', '黑龙江省-大兴安岭地区', 1);
INSERT INTO `qmgx_region` VALUES (107, '310100', '上海市', 10, 2, 0, 'ShangHai', '2', '1-10-107', '上海市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (109, '320100', '南京市', 11, 2, 0, 'Nanjing Shi', 'NKG', '1-11-109', '江苏省-南京市', 1);
INSERT INTO `qmgx_region` VALUES (110, '320200', '无锡市', 11, 2, 0, 'Wuxi Shi', 'WUX', '1-11-110', '江苏省-无锡市', 1);
INSERT INTO `qmgx_region` VALUES (111, '320300', '徐州市', 11, 2, 0, 'Xuzhou Shi', 'XUZ', '1-11-111', '江苏省-徐州市', 1);
INSERT INTO `qmgx_region` VALUES (112, '320400', '常州市', 11, 2, 0, 'Changzhou Shi', 'CZX', '1-11-112', '江苏省-常州市', 1);
INSERT INTO `qmgx_region` VALUES (113, '320500', '苏州市', 11, 2, 0, 'Suzhou Shi', 'SZH', '1-11-113', '江苏省-苏州市', 1);
INSERT INTO `qmgx_region` VALUES (114, '320600', '南通市', 11, 2, 0, 'Nantong Shi', 'NTG', '1-11-114', '江苏省-南通市', 1);
INSERT INTO `qmgx_region` VALUES (115, '320700', '连云港市', 11, 2, 0, 'Lianyungang Shi', 'LYG', '1-11-115', '江苏省-连云港市', 1);
INSERT INTO `qmgx_region` VALUES (116, '320800', '淮安市', 11, 2, 0, 'Huai,an Xian', '2', '1-11-116', '江苏省-淮安市', 1);
INSERT INTO `qmgx_region` VALUES (117, '320900', '盐城市', 11, 2, 0, 'Yancheng Shi', 'YCK', '1-11-117', '江苏省-盐城市', 1);
INSERT INTO `qmgx_region` VALUES (118, '321000', '扬州市', 11, 2, 0, 'Yangzhou Shi', 'YZH', '1-11-118', '江苏省-扬州市', 1);
INSERT INTO `qmgx_region` VALUES (119, '321100', '镇江市', 11, 2, 0, 'Zhenjiang Shi', 'ZHE', '1-11-119', '江苏省-镇江市', 1);
INSERT INTO `qmgx_region` VALUES (120, '321200', '泰州市', 11, 2, 0, 'Taizhou Shi', 'TZS', '1-11-120', '江苏省-泰州市', 1);
INSERT INTO `qmgx_region` VALUES (121, '321300', '宿迁市', 11, 2, 0, 'Suqian Shi', 'SUQ', '1-11-121', '江苏省-宿迁市', 1);
INSERT INTO `qmgx_region` VALUES (122, '330100', '杭州市', 12, 2, 0, 'Hangzhou Shi', 'HGH', '1-12-122', '浙江省-杭州市', 1);
INSERT INTO `qmgx_region` VALUES (123, '330200', '宁波市', 12, 2, 0, 'Ningbo Shi', 'NGB', '1-12-123', '浙江省-宁波市', 1);
INSERT INTO `qmgx_region` VALUES (124, '330300', '温州市', 12, 2, 0, 'Wenzhou Shi', 'WNZ', '1-12-124', '浙江省-温州市', 1);
INSERT INTO `qmgx_region` VALUES (125, '330400', '嘉兴市', 12, 2, 0, 'Jiaxing Shi', 'JIX', '1-12-125', '浙江省-嘉兴市', 1);
INSERT INTO `qmgx_region` VALUES (126, '330500', '湖州市', 12, 2, 0, 'Huzhou Shi ', 'HZH', '1-12-126', '浙江省-湖州市', 1);
INSERT INTO `qmgx_region` VALUES (127, '330600', '绍兴市', 12, 2, 0, 'Shaoxing Shi', 'SXG', '1-12-127', '浙江省-绍兴市', 1);
INSERT INTO `qmgx_region` VALUES (128, '330700', '金华市', 12, 2, 0, 'Jinhua Shi', 'JHA', '1-12-128', '浙江省-金华市', 1);
INSERT INTO `qmgx_region` VALUES (129, '330800', '衢州市', 12, 2, 0, 'Quzhou Shi', 'QUZ', '1-12-129', '浙江省-衢州市', 1);
INSERT INTO `qmgx_region` VALUES (130, '330900', '舟山市', 12, 2, 0, 'Zhoushan Shi', 'ZOS', '1-12-130', '浙江省-舟山市', 1);
INSERT INTO `qmgx_region` VALUES (131, '331000', '台州市', 12, 2, 0, 'Taizhou Shi', 'TZZ', '1-12-131', '浙江省-台州市', 1);
INSERT INTO `qmgx_region` VALUES (132, '331100', '丽水市', 12, 2, 0, 'Lishui Shi', '2', '1-12-132', '浙江省-丽水市', 1);
INSERT INTO `qmgx_region` VALUES (133, '340100', '合肥市', 13, 2, 0, 'Hefei Shi', 'HFE', '1-13-133', '安徽省-合肥市', 1);
INSERT INTO `qmgx_region` VALUES (134, '340200', '芜湖市', 13, 2, 0, 'Wuhu Shi', 'WHI', '1-13-134', '安徽省-芜湖市', 1);
INSERT INTO `qmgx_region` VALUES (135, '340300', '蚌埠市', 13, 2, 0, 'Bengbu Shi', 'BBU', '1-13-135', '安徽省-蚌埠市', 1);
INSERT INTO `qmgx_region` VALUES (136, '340400', '淮南市', 13, 2, 0, 'Huainan Shi', 'HNS', '1-13-136', '安徽省-淮南市', 1);
INSERT INTO `qmgx_region` VALUES (137, '340500', '马鞍山市', 13, 2, 0, 'Ma,anshan Shi', 'MAA', '1-13-137', '安徽省-马鞍山市', 1);
INSERT INTO `qmgx_region` VALUES (138, '340600', '淮北市', 13, 2, 0, 'Huaibei Shi', 'HBE', '1-13-138', '安徽省-淮北市', 1);
INSERT INTO `qmgx_region` VALUES (139, '340700', '铜陵市', 13, 2, 0, 'Tongling Shi', 'TOL', '1-13-139', '安徽省-铜陵市', 1);
INSERT INTO `qmgx_region` VALUES (140, '340800', '安庆市', 13, 2, 0, 'Anqing Shi', 'AQG', '1-13-140', '安徽省-安庆市', 1);
INSERT INTO `qmgx_region` VALUES (141, '341000', '黄山市', 13, 2, 0, 'Huangshan Shi', 'HSN', '1-13-141', '安徽省-黄山市', 1);
INSERT INTO `qmgx_region` VALUES (142, '341100', '滁州市', 13, 2, 0, 'Chuzhou Shi', 'CUZ', '1-13-142', '安徽省-滁州市', 1);
INSERT INTO `qmgx_region` VALUES (143, '341200', '阜阳市', 13, 2, 0, 'Fuyang Shi', 'FYS', '1-13-143', '安徽省-阜阳市', 1);
INSERT INTO `qmgx_region` VALUES (144, '341300', '宿州市', 13, 2, 0, 'Suzhou Shi', 'SUZ', '1-13-144', '安徽省-宿州市', 1);
INSERT INTO `qmgx_region` VALUES (145, '341400', '巢湖市', 13, 2, 0, 'Chaohu Shi', '2', '1-13-145', '安徽省-巢湖市', 1);
INSERT INTO `qmgx_region` VALUES (146, '341500', '六安市', 13, 2, 0, 'Liu,an Shi', '2', '1-13-146', '安徽省-六安市', 1);
INSERT INTO `qmgx_region` VALUES (147, '341600', '亳州市', 13, 2, 0, 'Bozhou Shi', '2', '1-13-147', '安徽省-亳州市', 1);
INSERT INTO `qmgx_region` VALUES (148, '341700', '池州市', 13, 2, 0, 'Chizhou Shi', '2', '1-13-148', '安徽省-池州市', 1);
INSERT INTO `qmgx_region` VALUES (149, '341800', '宣城市', 13, 2, 0, 'Xuancheng Shi', '2', '1-13-149', '安徽省-宣城市', 1);
INSERT INTO `qmgx_region` VALUES (150, '350100', '福州市', 14, 2, 0, 'Fuzhou Shi', 'FOC', '1-14-150', '福建省-福州市', 1);
INSERT INTO `qmgx_region` VALUES (151, '350200', '厦门市', 14, 2, 0, 'Xiamen Shi', 'XMN', '1-14-151', '福建省-厦门市', 1);
INSERT INTO `qmgx_region` VALUES (152, '350300', '莆田市', 14, 2, 0, 'Putian Shi', 'PUT', '1-14-152', '福建省-莆田市', 1);
INSERT INTO `qmgx_region` VALUES (153, '350400', '三明市', 14, 2, 0, 'Sanming Shi', 'SMS', '1-14-153', '福建省-三明市', 1);
INSERT INTO `qmgx_region` VALUES (154, '350500', '泉州市', 14, 2, 0, 'Quanzhou Shi', 'QZJ', '1-14-154', '福建省-泉州市', 1);
INSERT INTO `qmgx_region` VALUES (155, '350600', '漳州市', 14, 2, 0, 'Zhangzhou Shi', 'ZZU', '1-14-155', '福建省-漳州市', 1);
INSERT INTO `qmgx_region` VALUES (156, '350700', '南平市', 14, 2, 0, 'Nanping Shi', 'NPS', '1-14-156', '福建省-南平市', 1);
INSERT INTO `qmgx_region` VALUES (157, '350800', '龙岩市', 14, 2, 0, 'Longyan Shi', 'LYF', '1-14-157', '福建省-龙岩市', 1);
INSERT INTO `qmgx_region` VALUES (158, '350900', '宁德市', 14, 2, 0, 'Ningde Shi', '2', '1-14-158', '福建省-宁德市', 1);
INSERT INTO `qmgx_region` VALUES (159, '360100', '南昌市', 15, 2, 0, 'Nanchang Shi', 'KHN', '1-15-159', '江西省-南昌市', 1);
INSERT INTO `qmgx_region` VALUES (160, '360200', '景德镇市', 15, 2, 0, 'Jingdezhen Shi', 'JDZ', '1-15-160', '江西省-景德镇市', 1);
INSERT INTO `qmgx_region` VALUES (161, '360300', '萍乡市', 15, 2, 0, 'Pingxiang Shi', 'PXS', '1-15-161', '江西省-萍乡市', 1);
INSERT INTO `qmgx_region` VALUES (162, '360400', '九江市', 15, 2, 0, 'Jiujiang Shi', 'JIU', '1-15-162', '江西省-九江市', 1);
INSERT INTO `qmgx_region` VALUES (163, '360500', '新余市', 15, 2, 0, 'Xinyu Shi', 'XYU', '1-15-163', '江西省-新余市', 1);
INSERT INTO `qmgx_region` VALUES (164, '360600', '鹰潭市', 15, 2, 0, 'Yingtan Shi', '2', '1-15-164', '江西省-鹰潭市', 1);
INSERT INTO `qmgx_region` VALUES (165, '360700', '赣州市', 15, 2, 0, 'Ganzhou Shi', 'GZH', '1-15-165', '江西省-赣州市', 1);
INSERT INTO `qmgx_region` VALUES (166, '360800', '吉安市', 15, 2, 0, 'Ji,an Shi', '2', '1-15-166', '江西省-吉安市', 1);
INSERT INTO `qmgx_region` VALUES (167, '360900', '宜春市', 15, 2, 0, 'Yichun Shi', '2', '1-15-167', '江西省-宜春市', 1);
INSERT INTO `qmgx_region` VALUES (168, '361000', '抚州市', 15, 2, 0, 'Wuzhou Shi', '2', '1-15-168', '江西省-抚州市', 1);
INSERT INTO `qmgx_region` VALUES (169, '361100', '上饶市', 15, 2, 0, 'Shangrao Shi', '2', '1-15-169', '江西省-上饶市', 1);
INSERT INTO `qmgx_region` VALUES (170, '370100', '济南市', 16, 2, 0, 'Jinan Shi', 'TNA', '1-16-170', '山东省-济南市', 1);
INSERT INTO `qmgx_region` VALUES (171, '370200', '青岛市', 16, 2, 0, 'Qingdao Shi', 'TAO', '1-16-171', '山东省-青岛市', 1);
INSERT INTO `qmgx_region` VALUES (172, '370300', '淄博市', 16, 2, 0, 'Zibo Shi', 'ZBO', '1-16-172', '山东省-淄博市', 1);
INSERT INTO `qmgx_region` VALUES (173, '370400', '枣庄市', 16, 2, 0, 'Zaozhuang Shi', 'ZZG', '1-16-173', '山东省-枣庄市', 1);
INSERT INTO `qmgx_region` VALUES (174, '370500', '东营市', 16, 2, 0, 'Dongying Shi', 'DYG', '1-16-174', '山东省-东营市', 1);
INSERT INTO `qmgx_region` VALUES (175, '370600', '烟台市', 16, 2, 0, 'Yantai Shi', 'YNT', '1-16-175', '山东省-烟台市', 1);
INSERT INTO `qmgx_region` VALUES (176, '370700', '潍坊市', 16, 2, 0, 'Weifang Shi', 'WEF', '1-16-176', '山东省-潍坊市', 1);
INSERT INTO `qmgx_region` VALUES (177, '370800', '济宁市', 16, 2, 0, 'Jining Shi', 'JNG', '1-16-177', '山东省-济宁市', 1);
INSERT INTO `qmgx_region` VALUES (178, '370900', '泰安市', 16, 2, 0, 'Tai,an Shi', 'TAI', '1-16-178', '山东省-泰安市', 1);
INSERT INTO `qmgx_region` VALUES (179, '371000', '威海市', 16, 2, 0, 'Weihai Shi', 'WEH', '1-16-179', '山东省-威海市', 1);
INSERT INTO `qmgx_region` VALUES (180, '371100', '日照市', 16, 2, 0, 'Rizhao Shi', 'RZH', '1-16-180', '山东省-日照市', 1);
INSERT INTO `qmgx_region` VALUES (181, '371200', '莱芜市', 16, 2, 0, 'Laiwu Shi', 'LWS', '1-16-181', '山东省-莱芜市', 1);
INSERT INTO `qmgx_region` VALUES (182, '371300', '临沂市', 16, 2, 0, 'Linyi Shi', 'LYI', '1-16-182', '山东省-临沂市', 1);
INSERT INTO `qmgx_region` VALUES (183, '371400', '德州市', 16, 2, 0, 'Dezhou Shi', 'DZS', '1-16-183', '山东省-德州市', 1);
INSERT INTO `qmgx_region` VALUES (184, '371500', '聊城市', 16, 2, 0, 'Liaocheng Shi', 'LCH', '1-16-184', '山东省-聊城市', 1);
INSERT INTO `qmgx_region` VALUES (185, '371600', '滨州市', 16, 2, 0, 'Binzhou Shi', '2', '1-16-185', '山东省-滨州市', 1);
INSERT INTO `qmgx_region` VALUES (186, '371700', '菏泽市', 16, 2, 0, 'Heze Shi', 'HZ', '1-16-186', '山东省-菏泽市', 1);
INSERT INTO `qmgx_region` VALUES (187, '410100', '郑州市', 17, 2, 0, 'Zhengzhou Shi', 'CGO', '1-17-187', '河南省-郑州市', 1);
INSERT INTO `qmgx_region` VALUES (188, '410200', '开封市', 17, 2, 0, 'Kaifeng Shi', 'KFS', '1-17-188', '河南省-开封市', 1);
INSERT INTO `qmgx_region` VALUES (189, '410300', '洛阳市', 17, 2, 0, 'Luoyang Shi', 'LYA', '1-17-189', '河南省-洛阳市', 1);
INSERT INTO `qmgx_region` VALUES (190, '410400', '平顶山市', 17, 2, 0, 'Pingdingshan Shi', 'PDS', '1-17-190', '河南省-平顶山市', 1);
INSERT INTO `qmgx_region` VALUES (191, '410500', '安阳市', 17, 2, 0, 'Anyang Shi', 'AYS', '1-17-191', '河南省-安阳市', 1);
INSERT INTO `qmgx_region` VALUES (192, '410600', '鹤壁市', 17, 2, 0, 'Hebi Shi', 'HBS', '1-17-192', '河南省-鹤壁市', 1);
INSERT INTO `qmgx_region` VALUES (193, '410700', '新乡市', 17, 2, 0, 'Xinxiang Shi', 'XXS', '1-17-193', '河南省-新乡市', 1);
INSERT INTO `qmgx_region` VALUES (194, '410800', '焦作市', 17, 2, 0, 'Jiaozuo Shi', 'JZY', '1-17-194', '河南省-焦作市', 1);
INSERT INTO `qmgx_region` VALUES (195, '410900', '濮阳市', 17, 2, 0, 'Puyang Shi', 'PYS', '1-17-195', '河南省-濮阳市', 1);
INSERT INTO `qmgx_region` VALUES (196, '411000', '许昌市', 17, 2, 0, 'Xuchang Shi', 'XCS', '1-17-196', '河南省-许昌市', 1);
INSERT INTO `qmgx_region` VALUES (197, '411100', '漯河市', 17, 2, 0, 'Luohe Shi', 'LHS', '1-17-197', '河南省-漯河市', 1);
INSERT INTO `qmgx_region` VALUES (198, '411200', '三门峡市', 17, 2, 0, 'Sanmenxia Shi', 'SMX', '1-17-198', '河南省-三门峡市', 1);
INSERT INTO `qmgx_region` VALUES (199, '411300', '南阳市', 17, 2, 0, 'Nanyang Shi', 'NYS', '1-17-199', '河南省-南阳市', 1);
INSERT INTO `qmgx_region` VALUES (200, '411400', '商丘市', 17, 2, 0, 'Shangqiu Shi', 'SQS', '1-17-200', '河南省-商丘市', 1);
INSERT INTO `qmgx_region` VALUES (201, '411500', '信阳市', 17, 2, 0, 'Xinyang Shi', 'XYG', '1-17-201', '河南省-信阳市', 1);
INSERT INTO `qmgx_region` VALUES (202, '411600', '周口市', 17, 2, 0, 'Zhoukou Shi', '2', '1-17-202', '河南省-周口市', 1);
INSERT INTO `qmgx_region` VALUES (203, '411700', '驻马店市', 17, 2, 0, 'Zhumadian Shi', '2', '1-17-203', '河南省-驻马店市', 1);
INSERT INTO `qmgx_region` VALUES (204, '420100', '武汉市', 18, 2, 0, 'Wuhan Shi', 'WUH', '1-18-204', '湖北省-武汉市', 1);
INSERT INTO `qmgx_region` VALUES (205, '420200', '黄石市', 18, 2, 0, 'Huangshi Shi', 'HIS', '1-18-205', '湖北省-黄石市', 1);
INSERT INTO `qmgx_region` VALUES (206, '420300', '十堰市', 18, 2, 0, 'Shiyan Shi', 'SYE', '1-18-206', '湖北省-十堰市', 1);
INSERT INTO `qmgx_region` VALUES (207, '420500', '宜昌市', 18, 2, 0, 'Yichang Shi', 'YCO', '1-18-207', '湖北省-宜昌市', 1);
INSERT INTO `qmgx_region` VALUES (208, '420600', '襄樊市', 18, 2, 0, 'Xiangfan Shi', 'XFN', '1-18-208', '湖北省-襄樊市', 1);
INSERT INTO `qmgx_region` VALUES (209, '420700', '鄂州市', 18, 2, 0, 'Ezhou Shi', 'EZS', '1-18-209', '湖北省-鄂州市', 1);
INSERT INTO `qmgx_region` VALUES (210, '420800', '荆门市', 18, 2, 0, 'Jingmen Shi', 'JMS', '1-18-210', '湖北省-荆门市', 1);
INSERT INTO `qmgx_region` VALUES (211, '420900', '孝感市', 18, 2, 0, 'Xiaogan Shi', 'XGE', '1-18-211', '湖北省-孝感市', 1);
INSERT INTO `qmgx_region` VALUES (212, '421000', '荆州市', 18, 2, 0, 'Jingzhou Shi', 'JGZ', '1-18-212', '湖北省-荆州市', 1);
INSERT INTO `qmgx_region` VALUES (213, '421100', '黄冈市', 18, 2, 0, 'Huanggang Shi', 'HE', '1-18-213', '湖北省-黄冈市', 1);
INSERT INTO `qmgx_region` VALUES (214, '421200', '咸宁市', 18, 2, 0, 'Xianning Xian', 'XNS', '1-18-214', '湖北省-咸宁市', 1);
INSERT INTO `qmgx_region` VALUES (215, '421300', '随州市', 18, 2, 0, 'Suizhou Shi', '2', '1-18-215', '湖北省-随州市', 1);
INSERT INTO `qmgx_region` VALUES (216, '422800', '恩施土家族苗族自治州', 18, 2, 0, 'Enshi Tujiazu Miaozu Zizhizhou', 'ESH', '1-18-216', '湖北省-恩施土家族苗族自治州', 1);
INSERT INTO `qmgx_region` VALUES (217, '429000', '省直辖县级行政区划', 18, 2, 0, 'shengzhixiaxianjixingzhengquhua', '2', '1-18-217', '湖北省-省直辖县级行政区划', 1);
INSERT INTO `qmgx_region` VALUES (218, '430100', '长沙市', 19, 2, 0, 'Changsha Shi', 'CSX', '1-19-218', '湖南省-长沙市', 1);
INSERT INTO `qmgx_region` VALUES (219, '430200', '株洲市', 19, 2, 0, 'Zhuzhou Shi', 'ZZS', '1-19-219', '湖南省-株洲市', 1);
INSERT INTO `qmgx_region` VALUES (220, '430300', '湘潭市', 19, 2, 0, 'Xiangtan Shi', 'XGT', '1-19-220', '湖南省-湘潭市', 1);
INSERT INTO `qmgx_region` VALUES (221, '430400', '衡阳市', 19, 2, 0, 'Hengyang Shi', 'HNY', '1-19-221', '湖南省-衡阳市', 1);
INSERT INTO `qmgx_region` VALUES (222, '430500', '邵阳市', 19, 2, 0, 'Shaoyang Shi', 'SYR', '1-19-222', '湖南省-邵阳市', 1);
INSERT INTO `qmgx_region` VALUES (223, '430600', '岳阳市', 19, 2, 0, 'Yueyang Shi', 'YYG', '1-19-223', '湖南省-岳阳市', 1);
INSERT INTO `qmgx_region` VALUES (224, '430700', '常德市', 19, 2, 0, 'Changde Shi', 'CDE', '1-19-224', '湖南省-常德市', 1);
INSERT INTO `qmgx_region` VALUES (225, '430800', '张家界市', 19, 2, 0, 'Zhangjiajie Shi', 'ZJJ', '1-19-225', '湖南省-张家界市', 1);
INSERT INTO `qmgx_region` VALUES (226, '430900', '益阳市', 19, 2, 0, 'Yiyang Shi', 'YYS', '1-19-226', '湖南省-益阳市', 1);
INSERT INTO `qmgx_region` VALUES (227, '431000', '郴州市', 19, 2, 0, 'Chenzhou Shi', 'CNZ', '1-19-227', '湖南省-郴州市', 1);
INSERT INTO `qmgx_region` VALUES (228, '431100', '永州市', 19, 2, 0, 'Yongzhou Shi', 'YZS', '1-19-228', '湖南省-永州市', 1);
INSERT INTO `qmgx_region` VALUES (229, '431200', '怀化市', 19, 2, 0, 'Huaihua Shi', 'HHS', '1-19-229', '湖南省-怀化市', 1);
INSERT INTO `qmgx_region` VALUES (230, '431300', '娄底市', 19, 2, 0, 'Loudi Shi', '2', '1-19-230', '湖南省-娄底市', 1);
INSERT INTO `qmgx_region` VALUES (231, '433100', '湘西土家族苗族自治州', 19, 2, 0, 'Xiangxi Tujiazu Miaozu Zizhizhou ', 'XXZ', '1-19-231', '湖南省-湘西土家族苗族自治州', 1);
INSERT INTO `qmgx_region` VALUES (232, '440100', '广州市', 20, 2, 0, 'Guangzhou Shi', 'CAN', '1-20-232', '广东省-广州市', 1);
INSERT INTO `qmgx_region` VALUES (233, '440200', '韶关市', 20, 2, 0, 'Shaoguan Shi', 'HSC', '1-20-233', '广东省-韶关市', 1);
INSERT INTO `qmgx_region` VALUES (234, '440300', '深圳市', 20, 2, 0, 'Shenzhen Shi', 'SZX', '1-20-234', '广东省-深圳市', 1);
INSERT INTO `qmgx_region` VALUES (235, '440400', '珠海市', 20, 2, 0, 'Zhuhai Shi', 'ZUH', '1-20-235', '广东省-珠海市', 1);
INSERT INTO `qmgx_region` VALUES (236, '440500', '汕头市', 20, 2, 0, 'Shantou Shi', 'SWA', '1-20-236', '广东省-汕头市', 1);
INSERT INTO `qmgx_region` VALUES (237, '440600', '佛山市', 20, 2, 0, 'Foshan Shi', 'FOS', '1-20-237', '广东省-佛山市', 1);
INSERT INTO `qmgx_region` VALUES (238, '440700', '江门市', 20, 2, 0, 'Jiangmen Shi', 'JMN', '1-20-238', '广东省-江门市', 1);
INSERT INTO `qmgx_region` VALUES (239, '440800', '湛江市', 20, 2, 0, 'Zhanjiang Shi', 'ZHA', '1-20-239', '广东省-湛江市', 1);
INSERT INTO `qmgx_region` VALUES (240, '440900', '茂名市', 20, 2, 0, 'Maoming Shi', 'MMI', '1-20-240', '广东省-茂名市', 1);
INSERT INTO `qmgx_region` VALUES (241, '441200', '肇庆市', 20, 2, 0, 'Zhaoqing Shi', 'ZQG', '1-20-241', '广东省-肇庆市', 1);
INSERT INTO `qmgx_region` VALUES (242, '441300', '惠州市', 20, 2, 0, 'Huizhou Shi', 'HUI', '1-20-242', '广东省-惠州市', 1);
INSERT INTO `qmgx_region` VALUES (243, '441400', '梅州市', 20, 2, 0, 'Meizhou Shi', 'MXZ', '1-20-243', '广东省-梅州市', 1);
INSERT INTO `qmgx_region` VALUES (244, '441500', '汕尾市', 20, 2, 0, 'Shanwei Shi', 'SWE', '1-20-244', '广东省-汕尾市', 1);
INSERT INTO `qmgx_region` VALUES (245, '441600', '河源市', 20, 2, 0, 'Heyuan Shi', 'HEY', '1-20-245', '广东省-河源市', 1);
INSERT INTO `qmgx_region` VALUES (246, '441700', '阳江市', 20, 2, 0, 'Yangjiang Shi', 'YJI', '1-20-246', '广东省-阳江市', 1);
INSERT INTO `qmgx_region` VALUES (247, '441800', '清远市', 20, 2, 0, 'Qingyuan Shi', 'QYN', '1-20-247', '广东省-清远市', 1);
INSERT INTO `qmgx_region` VALUES (248, '441900', '东莞市', 20, 2, 0, 'Dongguan Shi', 'DGG', '1-20-248', '广东省-东莞市', 1);
INSERT INTO `qmgx_region` VALUES (249, '442000', '中山市', 20, 2, 0, 'Zhongshan Shi', 'ZSN', '1-20-249', '广东省-中山市', 1);
INSERT INTO `qmgx_region` VALUES (250, '445100', '潮州市', 20, 2, 0, 'Chaozhou Shi', 'CZY', '1-20-250', '广东省-潮州市', 1);
INSERT INTO `qmgx_region` VALUES (251, '445200', '揭阳市', 20, 2, 0, 'Jieyang Shi', 'JIY', '1-20-251', '广东省-揭阳市', 1);
INSERT INTO `qmgx_region` VALUES (252, '445300', '云浮市', 20, 2, 0, 'Yunfu Shi', 'YFS', '1-20-252', '广东省-云浮市', 1);
INSERT INTO `qmgx_region` VALUES (253, '450100', '南宁市', 21, 2, 0, 'Nanning Shi', 'NNG', '1-21-253', '广西壮族自治区-南宁市', 1);
INSERT INTO `qmgx_region` VALUES (254, '450200', '柳州市', 21, 2, 0, 'Liuzhou Shi', 'LZH', '1-21-254', '广西壮族自治区-柳州市', 1);
INSERT INTO `qmgx_region` VALUES (255, '450300', '桂林市', 21, 2, 0, 'Guilin Shi', 'KWL', '1-21-255', '广西壮族自治区-桂林市', 1);
INSERT INTO `qmgx_region` VALUES (256, '450400', '梧州市', 21, 2, 0, 'Wuzhou Shi', 'WUZ', '1-21-256', '广西壮族自治区-梧州市', 1);
INSERT INTO `qmgx_region` VALUES (257, '450500', '北海市', 21, 2, 0, 'Beihai Shi', 'BHY', '1-21-257', '广西壮族自治区-北海市', 1);
INSERT INTO `qmgx_region` VALUES (258, '450600', '防城港市', 21, 2, 0, 'Fangchenggang Shi', 'FAN', '1-21-258', '广西壮族自治区-防城港市', 1);
INSERT INTO `qmgx_region` VALUES (259, '450700', '钦州市', 21, 2, 0, 'Qinzhou Shi', 'QZH', '1-21-259', '广西壮族自治区-钦州市', 1);
INSERT INTO `qmgx_region` VALUES (260, '450800', '贵港市', 21, 2, 0, 'Guigang Shi', 'GUG', '1-21-260', '广西壮族自治区-贵港市', 1);
INSERT INTO `qmgx_region` VALUES (261, '450900', '玉林市', 21, 2, 0, 'Yulin Shi', 'YUL', '1-21-261', '广西壮族自治区-玉林市', 1);
INSERT INTO `qmgx_region` VALUES (262, '451000', '百色市', 21, 2, 0, 'Baise Shi', '2', '1-21-262', '广西壮族自治区-百色市', 1);
INSERT INTO `qmgx_region` VALUES (263, '451100', '贺州市', 21, 2, 0, 'Hezhou Shi', '2', '1-21-263', '广西壮族自治区-贺州市', 1);
INSERT INTO `qmgx_region` VALUES (264, '451200', '河池市', 21, 2, 0, 'Hechi Shi', '2', '1-21-264', '广西壮族自治区-河池市', 1);
INSERT INTO `qmgx_region` VALUES (265, '451300', '来宾市', 21, 2, 0, 'Laibin Shi', '2', '1-21-265', '广西壮族自治区-来宾市', 1);
INSERT INTO `qmgx_region` VALUES (266, '451400', '崇左市', 21, 2, 0, 'Chongzuo Shi', '2', '1-21-266', '广西壮族自治区-崇左市', 1);
INSERT INTO `qmgx_region` VALUES (267, '460100', '海口市', 22, 2, 0, 'Haikou Shi', 'HAK', '1-22-267', '海南省-海口市', 1);
INSERT INTO `qmgx_region` VALUES (268, '460200', '三亚市', 22, 2, 0, 'Sanya Shi', 'SYX', '1-22-268', '海南省-三亚市', 1);
INSERT INTO `qmgx_region` VALUES (269, '469000', '省直辖县级行政区划', 22, 2, 0, 'shengzhixiaxianjixingzhengquhua', '2', '1-22-269', '海南省-省直辖县级行政区划', 1);
INSERT INTO `qmgx_region` VALUES (270, '500100', '重庆市', 23, 2, 0, 'ChongQing', '2', '1-23-270', '重庆市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (273, '510100', '成都市', 24, 2, 0, 'Chengdu Shi', 'CTU', '1-24-273', '四川省-成都市', 1);
INSERT INTO `qmgx_region` VALUES (274, '510300', '自贡市', 24, 2, 0, 'Zigong Shi', 'ZGS', '1-24-274', '四川省-自贡市', 1);
INSERT INTO `qmgx_region` VALUES (275, '510400', '攀枝花市', 24, 2, 0, 'Panzhihua Shi', 'PZH', '1-24-275', '四川省-攀枝花市', 1);
INSERT INTO `qmgx_region` VALUES (276, '510500', '泸州市', 24, 2, 0, 'Luzhou Shi', 'LUZ', '1-24-276', '四川省-泸州市', 1);
INSERT INTO `qmgx_region` VALUES (277, '510600', '德阳市', 24, 2, 0, 'Deyang Shi', 'DEY', '1-24-277', '四川省-德阳市', 1);
INSERT INTO `qmgx_region` VALUES (278, '510700', '绵阳市', 24, 2, 0, 'Mianyang Shi', 'MYG', '1-24-278', '四川省-绵阳市', 1);
INSERT INTO `qmgx_region` VALUES (279, '510800', '广元市', 24, 2, 0, 'Guangyuan Shi', 'GYC', '1-24-279', '四川省-广元市', 1);
INSERT INTO `qmgx_region` VALUES (280, '510900', '遂宁市', 24, 2, 0, 'Suining Shi', 'SNS', '1-24-280', '四川省-遂宁市', 1);
INSERT INTO `qmgx_region` VALUES (281, '511000', '内江市', 24, 2, 0, 'Neijiang Shi', 'NJS', '1-24-281', '四川省-内江市', 1);
INSERT INTO `qmgx_region` VALUES (282, '511100', '乐山市', 24, 2, 0, 'Leshan Shi', 'LES', '1-24-282', '四川省-乐山市', 1);
INSERT INTO `qmgx_region` VALUES (283, '511300', '南充市', 24, 2, 0, 'Nanchong Shi', 'NCO', '1-24-283', '四川省-南充市', 1);
INSERT INTO `qmgx_region` VALUES (284, '511400', '眉山市', 24, 2, 0, 'Meishan Shi', '2', '1-24-284', '四川省-眉山市', 1);
INSERT INTO `qmgx_region` VALUES (285, '511500', '宜宾市', 24, 2, 0, 'Yibin Shi', 'YBS', '1-24-285', '四川省-宜宾市', 1);
INSERT INTO `qmgx_region` VALUES (286, '511600', '广安市', 24, 2, 0, 'Guang,an Shi', 'GAC', '1-24-286', '四川省-广安市', 1);
INSERT INTO `qmgx_region` VALUES (287, '511700', '达州市', 24, 2, 0, 'Dazhou Shi', '2', '1-24-287', '四川省-达州市', 1);
INSERT INTO `qmgx_region` VALUES (288, '511800', '雅安市', 24, 2, 0, 'Ya,an Shi', '2', '1-24-288', '四川省-雅安市', 1);
INSERT INTO `qmgx_region` VALUES (289, '511900', '巴中市', 24, 2, 0, 'Bazhong Shi', '2', '1-24-289', '四川省-巴中市', 1);
INSERT INTO `qmgx_region` VALUES (290, '512000', '资阳市', 24, 2, 0, 'Ziyang Shi', '2', '1-24-290', '四川省-资阳市', 1);
INSERT INTO `qmgx_region` VALUES (291, '513200', '阿坝藏族羌族自治州', 24, 2, 0, 'Aba(Ngawa) Zangzu Qiangzu Zizhizhou', 'ABA', '1-24-291', '四川省-阿坝藏族羌族自治州', 1);
INSERT INTO `qmgx_region` VALUES (292, '513300', '甘孜藏族自治州', 24, 2, 0, 'Garze Zangzu Zizhizhou', 'GAZ', '1-24-292', '四川省-甘孜藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (293, '513400', '凉山彝族自治州', 24, 2, 0, 'Liangshan Yizu Zizhizhou', 'LSY', '1-24-293', '四川省-凉山彝族自治州', 1);
INSERT INTO `qmgx_region` VALUES (294, '520100', '贵阳市', 25, 2, 0, 'Guiyang Shi', 'KWE', '1-25-294', '贵州省-贵阳市', 1);
INSERT INTO `qmgx_region` VALUES (295, '520200', '六盘水市', 25, 2, 0, 'Liupanshui Shi', 'LPS', '1-25-295', '贵州省-六盘水市', 1);
INSERT INTO `qmgx_region` VALUES (296, '520300', '遵义市', 25, 2, 0, 'Zunyi Shi', 'ZNY', '1-25-296', '贵州省-遵义市', 1);
INSERT INTO `qmgx_region` VALUES (297, '520400', '安顺市', 25, 2, 0, 'Anshun Xian', '2', '1-25-297', '贵州省-安顺市', 1);
INSERT INTO `qmgx_region` VALUES (298, '522200', '铜仁地区', 25, 2, 0, 'Tongren Diqu', 'TRD', '1-25-298', '贵州省-铜仁地区', 1);
INSERT INTO `qmgx_region` VALUES (299, '522300', '黔西南布依族苗族自治州', 25, 2, 0, 'Qianxinan Buyeizu Zizhizhou', 'QXZ', '1-25-299', '贵州省-黔西南布依族苗族自治州', 1);
INSERT INTO `qmgx_region` VALUES (300, '522400', '毕节地区', 25, 2, 0, 'Bijie Diqu', 'BJD', '1-25-300', '贵州省-毕节地区', 1);
INSERT INTO `qmgx_region` VALUES (301, '522600', '黔东南苗族侗族自治州', 25, 2, 0, 'Qiandongnan Miaozu Dongzu Zizhizhou', 'QND', '1-25-301', '贵州省-黔东南苗族侗族自治州', 1);
INSERT INTO `qmgx_region` VALUES (302, '522700', '黔南布依族苗族自治州', 25, 2, 0, 'Qiannan Buyeizu Miaozu Zizhizhou', 'QNZ', '1-25-302', '贵州省-黔南布依族苗族自治州', 1);
INSERT INTO `qmgx_region` VALUES (303, '530100', '昆明市', 26, 2, 0, 'Kunming Shi', 'KMG', '1-26-303', '云南省-昆明市', 1);
INSERT INTO `qmgx_region` VALUES (304, '530300', '曲靖市', 26, 2, 0, 'Qujing Shi', 'QJS', '1-26-304', '云南省-曲靖市', 1);
INSERT INTO `qmgx_region` VALUES (305, '530400', '玉溪市', 26, 2, 0, 'Yuxi Shi', 'YXS', '1-26-305', '云南省-玉溪市', 1);
INSERT INTO `qmgx_region` VALUES (306, '530500', '保山市', 26, 2, 0, 'Baoshan Shi', '2', '1-26-306', '云南省-保山市', 1);
INSERT INTO `qmgx_region` VALUES (307, '530600', '昭通市', 26, 2, 0, 'Zhaotong Shi', '2', '1-26-307', '云南省-昭通市', 1);
INSERT INTO `qmgx_region` VALUES (308, '530700', '丽江市', 26, 2, 0, 'Lijiang Shi', '2', '1-26-308', '云南省-丽江市', 1);
INSERT INTO `qmgx_region` VALUES (309, '530800', '普洱市', 26, 2, 0, 'Simao Shi', '2', '1-26-309', '云南省-普洱市', 1);
INSERT INTO `qmgx_region` VALUES (310, '530900', '临沧市', 26, 2, 0, 'Lincang Shi', '2', '1-26-310', '云南省-临沧市', 1);
INSERT INTO `qmgx_region` VALUES (311, '532300', '楚雄彝族自治州', 26, 2, 0, 'Chuxiong Yizu Zizhizhou', 'CXD', '1-26-311', '云南省-楚雄彝族自治州', 1);
INSERT INTO `qmgx_region` VALUES (312, '532500', '红河哈尼族彝族自治州', 26, 2, 0, 'Honghe Hanizu Yizu Zizhizhou', 'HHZ', '1-26-312', '云南省-红河哈尼族彝族自治州', 1);
INSERT INTO `qmgx_region` VALUES (313, '532600', '文山壮族苗族自治州', 26, 2, 0, 'Wenshan Zhuangzu Miaozu Zizhizhou', 'WSZ', '1-26-313', '云南省-文山壮族苗族自治州', 1);
INSERT INTO `qmgx_region` VALUES (314, '532800', '西双版纳傣族自治州', 26, 2, 0, 'Xishuangbanna Daizu Zizhizhou', 'XSB', '1-26-314', '云南省-西双版纳傣族自治州', 1);
INSERT INTO `qmgx_region` VALUES (315, '532900', '大理白族自治州', 26, 2, 0, 'Dali Baizu Zizhizhou', 'DLZ', '1-26-315', '云南省-大理白族自治州', 1);
INSERT INTO `qmgx_region` VALUES (316, '533100', '德宏傣族景颇族自治州', 26, 2, 0, 'Dehong Daizu Jingpozu Zizhizhou', 'DHG', '1-26-316', '云南省-德宏傣族景颇族自治州', 1);
INSERT INTO `qmgx_region` VALUES (317, '533300', '怒江傈僳族自治州', 26, 2, 0, 'Nujiang Lisuzu Zizhizhou', 'NUJ', '1-26-317', '云南省-怒江傈僳族自治州', 1);
INSERT INTO `qmgx_region` VALUES (318, '533400', '迪庆藏族自治州', 26, 2, 0, 'Deqen Zangzu Zizhizhou', 'DEZ', '1-26-318', '云南省-迪庆藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (319, '540100', '拉萨市', 27, 2, 0, 'Lhasa Shi', 'LXA', '1-27-319', '西藏自治区-拉萨市', 1);
INSERT INTO `qmgx_region` VALUES (320, '542100', '昌都地区', 27, 2, 0, 'Qamdo Diqu', 'QAD', '1-27-320', '西藏自治区-昌都地区', 1);
INSERT INTO `qmgx_region` VALUES (321, '542200', '山南地区', 27, 2, 0, 'Shannan Diqu', 'SND', '1-27-321', '西藏自治区-山南地区', 1);
INSERT INTO `qmgx_region` VALUES (322, '542300', '日喀则地区', 27, 2, 0, 'Xigaze Diqu', 'XID', '1-27-322', '西藏自治区-日喀则地区', 1);
INSERT INTO `qmgx_region` VALUES (323, '542400', '那曲地区', 27, 2, 0, 'Nagqu Diqu', 'NAD', '1-27-323', '西藏自治区-那曲地区', 1);
INSERT INTO `qmgx_region` VALUES (324, '542500', '阿里地区', 27, 2, 0, 'Ngari Diqu', 'NGD', '1-27-324', '西藏自治区-阿里地区', 1);
INSERT INTO `qmgx_region` VALUES (325, '542600', '林芝地区', 27, 2, 0, 'Nyingchi Diqu', 'NYD', '1-27-325', '西藏自治区-林芝地区', 1);
INSERT INTO `qmgx_region` VALUES (326, '610100', '西安市', 28, 2, 0, 'Xi,an Shi', 'SIA', '1-28-326', '陕西省-西安市', 1);
INSERT INTO `qmgx_region` VALUES (327, '610200', '铜川市', 28, 2, 0, 'Tongchuan Shi', 'TCN', '1-28-327', '陕西省-铜川市', 1);
INSERT INTO `qmgx_region` VALUES (328, '610300', '宝鸡市', 28, 2, 0, 'Baoji Shi', 'BJI', '1-28-328', '陕西省-宝鸡市', 1);
INSERT INTO `qmgx_region` VALUES (329, '610400', '咸阳市', 28, 2, 0, 'Xianyang Shi', 'XYS', '1-28-329', '陕西省-咸阳市', 1);
INSERT INTO `qmgx_region` VALUES (330, '610500', '渭南市', 28, 2, 0, 'Weinan Shi', 'WNA', '1-28-330', '陕西省-渭南市', 1);
INSERT INTO `qmgx_region` VALUES (331, '610600', '延安市', 28, 2, 0, 'Yan,an Shi', 'YNA', '1-28-331', '陕西省-延安市', 1);
INSERT INTO `qmgx_region` VALUES (332, '610700', '汉中市', 28, 2, 0, 'Hanzhong Shi', 'HZJ', '1-28-332', '陕西省-汉中市', 1);
INSERT INTO `qmgx_region` VALUES (333, '610800', '榆林市', 28, 2, 0, 'Yulin Shi', '2', '1-28-333', '陕西省-榆林市', 1);
INSERT INTO `qmgx_region` VALUES (334, '610900', '安康市', 28, 2, 0, 'Ankang Shi', '2', '1-28-334', '陕西省-安康市', 1);
INSERT INTO `qmgx_region` VALUES (335, '611000', '商洛市', 28, 2, 0, 'Shangluo Shi', '2', '1-28-335', '陕西省-商洛市', 1);
INSERT INTO `qmgx_region` VALUES (336, '620100', '兰州市', 29, 2, 0, 'Lanzhou Shi', 'LHW', '1-29-336', '甘肃省-兰州市', 1);
INSERT INTO `qmgx_region` VALUES (337, '620200', '嘉峪关市', 29, 2, 0, 'Jiayuguan Shi', 'JYG', '1-29-337', '甘肃省-嘉峪关市', 1);
INSERT INTO `qmgx_region` VALUES (338, '620300', '金昌市', 29, 2, 0, 'Jinchang Shi', 'JCS', '1-29-338', '甘肃省-金昌市', 1);
INSERT INTO `qmgx_region` VALUES (339, '620400', '白银市', 29, 2, 0, 'Baiyin Shi', 'BYS', '1-29-339', '甘肃省-白银市', 1);
INSERT INTO `qmgx_region` VALUES (340, '620500', '天水市', 29, 2, 0, 'Tianshui Shi', 'TSU', '1-29-340', '甘肃省-天水市', 1);
INSERT INTO `qmgx_region` VALUES (341, '620600', '武威市', 29, 2, 0, 'Wuwei Shi', '2', '1-29-341', '甘肃省-武威市', 1);
INSERT INTO `qmgx_region` VALUES (342, '620700', '张掖市', 29, 2, 0, 'Zhangye Shi', '2', '1-29-342', '甘肃省-张掖市', 1);
INSERT INTO `qmgx_region` VALUES (343, '620800', '平凉市', 29, 2, 0, 'Pingliang Shi', '2', '1-29-343', '甘肃省-平凉市', 1);
INSERT INTO `qmgx_region` VALUES (344, '620900', '酒泉市', 29, 2, 0, 'Jiuquan Shi', '2', '1-29-344', '甘肃省-酒泉市', 1);
INSERT INTO `qmgx_region` VALUES (345, '621000', '庆阳市', 29, 2, 0, 'Qingyang Shi', '2', '1-29-345', '甘肃省-庆阳市', 1);
INSERT INTO `qmgx_region` VALUES (346, '621100', '定西市', 29, 2, 0, 'Dingxi Shi', '2', '1-29-346', '甘肃省-定西市', 1);
INSERT INTO `qmgx_region` VALUES (347, '621200', '陇南市', 29, 2, 0, 'Longnan Shi', '2', '1-29-347', '甘肃省-陇南市', 1);
INSERT INTO `qmgx_region` VALUES (348, '622900', '临夏回族自治州', 29, 2, 0, 'Linxia Huizu Zizhizhou ', 'LXH', '1-29-348', '甘肃省-临夏回族自治州', 1);
INSERT INTO `qmgx_region` VALUES (349, '623000', '甘南藏族自治州', 29, 2, 0, 'Gannan Zangzu Zizhizhou', 'GNZ', '1-29-349', '甘肃省-甘南藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (350, '630100', '西宁市', 30, 2, 0, 'Xining Shi', 'XNN', '1-30-350', '青海省-西宁市', 1);
INSERT INTO `qmgx_region` VALUES (351, '632100', '海东地区', 30, 2, 0, 'Haidong Diqu', 'HDD', '1-30-351', '青海省-海东地区', 1);
INSERT INTO `qmgx_region` VALUES (352, '632200', '海北藏族自治州', 30, 2, 0, 'Haibei Zangzu Zizhizhou', 'HBZ', '1-30-352', '青海省-海北藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (353, '632300', '黄南藏族自治州', 30, 2, 0, 'Huangnan Zangzu Zizhizhou', 'HNZ', '1-30-353', '青海省-黄南藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (354, '632500', '海南藏族自治州', 30, 2, 0, 'Hainan Zangzu Zizhizhou', 'HNN', '1-30-354', '青海省-海南藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (355, '632600', '果洛藏族自治州', 30, 2, 0, 'Golog Zangzu Zizhizhou', 'GOL', '1-30-355', '青海省-果洛藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (356, '632700', '玉树藏族自治州', 30, 2, 0, 'Yushu Zangzu Zizhizhou', 'YSZ', '1-30-356', '青海省-玉树藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (357, '632800', '海西蒙古族藏族自治州', 30, 2, 0, 'Haixi Mongolzu Zangzu Zizhizhou', 'HXZ', '1-30-357', '青海省-海西蒙古族藏族自治州', 1);
INSERT INTO `qmgx_region` VALUES (358, '640100', '银川市', 31, 2, 0, 'Yinchuan Shi', 'INC', '1-31-358', '宁夏回族自治区-银川市', 1);
INSERT INTO `qmgx_region` VALUES (359, '640200', '石嘴山市', 31, 2, 0, 'Shizuishan Shi', 'SZS', '1-31-359', '宁夏回族自治区-石嘴山市', 1);
INSERT INTO `qmgx_region` VALUES (360, '640300', '吴忠市', 31, 2, 0, 'Wuzhong Shi', 'WZS', '1-31-360', '宁夏回族自治区-吴忠市', 1);
INSERT INTO `qmgx_region` VALUES (361, '640400', '固原市', 31, 2, 0, 'Guyuan Shi', '2', '1-31-361', '宁夏回族自治区-固原市', 1);
INSERT INTO `qmgx_region` VALUES (362, '640500', '中卫市', 31, 2, 0, 'Zhongwei Shi', '2', '1-31-362', '宁夏回族自治区-中卫市', 1);
INSERT INTO `qmgx_region` VALUES (363, '650100', '乌鲁木齐市', 32, 2, 0, 'Urumqi Shi', 'URC', '1-32-363', '新疆维吾尔自治区-乌鲁木齐市', 1);
INSERT INTO `qmgx_region` VALUES (364, '650200', '克拉玛依市', 32, 2, 0, 'Karamay Shi', 'KAR', '1-32-364', '新疆维吾尔自治区-克拉玛依市', 1);
INSERT INTO `qmgx_region` VALUES (365, '652100', '吐鲁番地区', 32, 2, 0, 'Turpan Diqu', 'TUD', '1-32-365', '新疆维吾尔自治区-吐鲁番地区', 1);
INSERT INTO `qmgx_region` VALUES (366, '652200', '哈密地区', 32, 2, 0, 'Hami(kumul) Diqu', 'HMD', '1-32-366', '新疆维吾尔自治区-哈密地区', 1);
INSERT INTO `qmgx_region` VALUES (367, '652300', '昌吉回族自治州', 32, 2, 0, 'Changji Huizu Zizhizhou', 'CJZ', '1-32-367', '新疆维吾尔自治区-昌吉回族自治州', 1);
INSERT INTO `qmgx_region` VALUES (368, '652700', '博尔塔拉蒙古自治州', 32, 2, 0, 'Bortala Monglo Zizhizhou', 'BOR', '1-32-368', '新疆维吾尔自治区-博尔塔拉蒙古自治州', 1);
INSERT INTO `qmgx_region` VALUES (369, '652800', '巴音郭楞蒙古自治州', 32, 2, 0, 'bayinguolengmengguzizhizhou', '2', '1-32-369', '新疆维吾尔自治区-巴音郭楞蒙古自治州', 1);
INSERT INTO `qmgx_region` VALUES (370, '652900', '阿克苏地区', 32, 2, 0, 'Aksu Diqu', 'AKD', '1-32-370', '新疆维吾尔自治区-阿克苏地区', 1);
INSERT INTO `qmgx_region` VALUES (371, '653000', '克孜勒苏柯尔克孜自治州', 32, 2, 0, 'Kizilsu Kirgiz Zizhizhou', 'KIZ', '1-32-371', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州', 1);
INSERT INTO `qmgx_region` VALUES (372, '653100', '喀什地区', 32, 2, 0, 'Kashi(Kaxgar) Diqu', 'KSI', '1-32-372', '新疆维吾尔自治区-喀什地区', 1);
INSERT INTO `qmgx_region` VALUES (373, '653200', '和田地区', 32, 2, 0, 'Hotan Diqu', 'HOD', '1-32-373', '新疆维吾尔自治区-和田地区', 1);
INSERT INTO `qmgx_region` VALUES (374, '654000', '伊犁哈萨克自治州', 32, 2, 0, 'Ili Kazak Zizhizhou', 'ILD', '1-32-374', '新疆维吾尔自治区-伊犁哈萨克自治州', 1);
INSERT INTO `qmgx_region` VALUES (375, '654200', '塔城地区', 32, 2, 0, 'Tacheng(Qoqek) Diqu', 'TCD', '1-32-375', '新疆维吾尔自治区-塔城地区', 1);
INSERT INTO `qmgx_region` VALUES (376, '654300', '阿勒泰地区', 32, 2, 0, 'Altay Diqu', 'ALD', '1-32-376', '新疆维吾尔自治区-阿勒泰地区', 1);
INSERT INTO `qmgx_region` VALUES (377, '659000', '自治区直辖县级行政区划', 32, 2, 0, 'zizhiquzhixiaxianjixingzhengquhua', '2', '1-32-377', '新疆维吾尔自治区-自治区直辖县级行政区划', 1);
INSERT INTO `qmgx_region` VALUES (378, '110101', '东城区', 33, 3, 0, 'Dongcheng Qu', 'DCQ', '1-2-33-378', '北京市-市辖区-东城区', 1);
INSERT INTO `qmgx_region` VALUES (379, '110102', '西城区', 33, 3, 0, 'Xicheng Qu', 'XCQ', '1-2-33-379', '北京市-市辖区-西城区', 1);
INSERT INTO `qmgx_region` VALUES (382, '110105', '朝阳区', 33, 3, 0, 'Chaoyang Qu', 'CYQ', '1-2-33-382', '北京市-市辖区-朝阳区', 1);
INSERT INTO `qmgx_region` VALUES (383, '110106', '丰台区', 33, 3, 0, 'Fengtai Qu', 'FTQ', '1-2-33-383', '北京市-市辖区-丰台区', 1);
INSERT INTO `qmgx_region` VALUES (384, '110107', '石景山区', 33, 3, 0, 'Shijingshan Qu', 'SJS', '1-2-33-384', '北京市-市辖区-石景山区', 1);
INSERT INTO `qmgx_region` VALUES (385, '110108', '海淀区', 33, 3, 0, 'Haidian Qu', 'HDN', '1-2-33-385', '北京市-市辖区-海淀区', 1);
INSERT INTO `qmgx_region` VALUES (386, '110109', '门头沟区', 33, 3, 0, 'Mentougou Qu', 'MTG', '1-2-33-386', '北京市-市辖区-门头沟区', 1);
INSERT INTO `qmgx_region` VALUES (387, '110111', '房山区', 33, 3, 0, 'Fangshan Qu', 'FSQ', '1-2-33-387', '北京市-市辖区-房山区', 1);
INSERT INTO `qmgx_region` VALUES (388, '110112', '通州区', 33, 3, 0, 'Tongzhou Qu', 'TZQ', '1-2-33-388', '北京市-市辖区-通州区', 1);
INSERT INTO `qmgx_region` VALUES (389, '110113', '顺义区', 33, 3, 0, 'Shunyi Qu', 'SYI', '1-2-33-389', '北京市-市辖区-顺义区', 1);
INSERT INTO `qmgx_region` VALUES (390, '110114', '昌平区', 33, 3, 0, 'Changping Qu', 'CP Q', '1-2-33-390', '北京市-市辖区-昌平区', 1);
INSERT INTO `qmgx_region` VALUES (391, '110115', '大兴区', 33, 3, 0, 'Daxing Qu', 'DX Q', '1-2-33-391', '北京市-市辖区-大兴区', 1);
INSERT INTO `qmgx_region` VALUES (392, '110116', '怀柔区', 33, 3, 0, 'Huairou Qu', 'HR Q', '1-2-33-392', '北京市-市辖区-怀柔区', 1);
INSERT INTO `qmgx_region` VALUES (393, '110117', '平谷区', 33, 3, 0, 'Pinggu Qu', 'PG Q', '1-2-33-393', '北京市-市辖区-平谷区', 1);
INSERT INTO `qmgx_region` VALUES (394, '110228', '密云县', 33, 3, 0, 'Miyun Xian ', 'MYN', '1-2-33-394', '北京市-北京-密云县', 1);
INSERT INTO `qmgx_region` VALUES (395, '110229', '延庆县', 33, 3, 0, 'Yanqing Xian', 'YQX', '1-2-33-395', '北京市-北京-延庆县', 1);
INSERT INTO `qmgx_region` VALUES (396, '120101', '和平区', 35, 3, 0, 'Heping Qu', 'HPG', '1-3-35-396', '天津市-市辖区-和平区', 1);
INSERT INTO `qmgx_region` VALUES (397, '120102', '河东区', 35, 3, 0, 'Hedong Qu', 'HDQ', '1-3-35-397', '天津市-市辖区-河东区', 1);
INSERT INTO `qmgx_region` VALUES (398, '120103', '河西区', 35, 3, 0, 'Hexi Qu', 'HXQ', '1-3-35-398', '天津市-市辖区-河西区', 1);
INSERT INTO `qmgx_region` VALUES (399, '120104', '南开区', 35, 3, 0, 'Nankai Qu', 'NKQ', '1-3-35-399', '天津市-市辖区-南开区', 1);
INSERT INTO `qmgx_region` VALUES (400, '120105', '河北区', 35, 3, 0, 'Hebei Qu', 'HBQ', '1-3-35-400', '天津市-市辖区-河北区', 1);
INSERT INTO `qmgx_region` VALUES (401, '120106', '红桥区', 35, 3, 0, 'Hongqiao Qu', 'HQO', '1-3-35-401', '天津市-市辖区-红桥区', 1);
INSERT INTO `qmgx_region` VALUES (404, '120116', '滨海新区', 35, 3, 0, 'Dagang Qu', '2', '1-3-35-404', '天津市-市辖区-滨海新区', 1);
INSERT INTO `qmgx_region` VALUES (405, '120110', '东丽区', 35, 3, 0, 'Dongli Qu', 'DLI', '1-3-35-405', '天津市-市辖区-东丽区', 1);
INSERT INTO `qmgx_region` VALUES (406, '120111', '西青区', 35, 3, 0, 'Xiqing Qu', 'XQG', '1-3-35-406', '天津市-市辖区-西青区', 1);
INSERT INTO `qmgx_region` VALUES (407, '120112', '津南区', 35, 3, 0, 'Jinnan Qu', 'JNQ', '1-3-35-407', '天津市-市辖区-津南区', 1);
INSERT INTO `qmgx_region` VALUES (408, '120113', '北辰区', 35, 3, 0, 'Beichen Qu', 'BCQ', '1-3-35-408', '天津市-市辖区-北辰区', 1);
INSERT INTO `qmgx_region` VALUES (409, '120114', '武清区', 35, 3, 0, 'Wuqing Qu', 'WQ Q', '1-3-35-409', '天津市-市辖区-武清区', 1);
INSERT INTO `qmgx_region` VALUES (410, '120115', '宝坻区', 35, 3, 0, 'Baodi Qu', 'BDI', '1-3-35-410', '天津市-市辖区-宝坻区', 1);
INSERT INTO `qmgx_region` VALUES (411, '120221', '宁河县', 35, 3, 0, 'Ninghe Xian', 'NHE', '1-3-35-411', '天津市-天津-宁河县', 1);
INSERT INTO `qmgx_region` VALUES (412, '120223', '静海县', 35, 3, 0, 'Jinghai Xian', 'JHT', '1-3-35-412', '天津市-天津-静海县', 1);
INSERT INTO `qmgx_region` VALUES (413, '120225', '蓟县', 35, 3, 0, 'Ji Xian', 'JIT', '1-3-35-413', '天津市-天津-蓟县', 1);
INSERT INTO `qmgx_region` VALUES (414, '130101', '市辖区', 37, 3, 0, 'Shixiaqu', '2', '1-4-37-414', '河北省-石家庄市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (415, '130102', '长安区', 37, 3, 0, 'Chang,an Qu', 'CAQ', '1-4-37-415', '河北省-石家庄市-长安区', 1);
INSERT INTO `qmgx_region` VALUES (416, '130103', '桥东区', 37, 3, 0, 'Qiaodong Qu', 'QDQ', '1-4-37-416', '河北省-石家庄市-桥东区', 1);
INSERT INTO `qmgx_region` VALUES (417, '130104', '桥西区', 37, 3, 0, 'Qiaoxi Qu', 'QXQ', '1-4-37-417', '河北省-石家庄市-桥西区', 1);
INSERT INTO `qmgx_region` VALUES (418, '130105', '新华区', 37, 3, 0, 'Xinhua Qu', 'XHK', '1-4-37-418', '河北省-石家庄市-新华区', 1);
INSERT INTO `qmgx_region` VALUES (419, '130107', '井陉矿区', 37, 3, 0, 'Jingxing Kuangqu', 'JXK', '1-4-37-419', '河北省-石家庄市-井陉矿区', 1);
INSERT INTO `qmgx_region` VALUES (420, '130108', '裕华区', 37, 3, 0, 'Yuhua Qu', '2', '1-4-37-420', '河北省-石家庄市-裕华区', 1);
INSERT INTO `qmgx_region` VALUES (421, '130121', '井陉县', 37, 3, 0, 'Jingxing Xian', 'JXJ', '1-4-37-421', '河北省-石家庄市-井陉县', 1);
INSERT INTO `qmgx_region` VALUES (422, '130123', '正定县', 37, 3, 0, 'Zhengding Xian', 'ZDJ', '1-4-37-422', '河北省-石家庄市-正定县', 1);
INSERT INTO `qmgx_region` VALUES (423, '130124', '栾城县', 37, 3, 0, 'Luancheng Xian', 'LCG', '1-4-37-423', '河北省-石家庄市-栾城县', 1);
INSERT INTO `qmgx_region` VALUES (424, '130125', '行唐县', 37, 3, 0, 'Xingtang Xian', 'XTG', '1-4-37-424', '河北省-石家庄市-行唐县', 1);
INSERT INTO `qmgx_region` VALUES (425, '130126', '灵寿县', 37, 3, 0, 'Lingshou Xian ', 'LSO', '1-4-37-425', '河北省-石家庄市-灵寿县', 1);
INSERT INTO `qmgx_region` VALUES (426, '130127', '高邑县', 37, 3, 0, 'Gaoyi Xian', 'GYJ', '1-4-37-426', '河北省-石家庄市-高邑县', 1);
INSERT INTO `qmgx_region` VALUES (427, '130128', '深泽县', 37, 3, 0, 'Shenze Xian', '2', '1-4-37-427', '河北省-石家庄市-深泽县', 1);
INSERT INTO `qmgx_region` VALUES (428, '130129', '赞皇县', 37, 3, 0, 'Zanhuang Xian', 'ZHG', '1-4-37-428', '河北省-石家庄市-赞皇县', 1);
INSERT INTO `qmgx_region` VALUES (429, '130130', '无极县', 37, 3, 0, 'Wuji Xian', 'WJI', '1-4-37-429', '河北省-石家庄市-无极县', 1);
INSERT INTO `qmgx_region` VALUES (430, '130131', '平山县', 37, 3, 0, 'Pingshan Xian', 'PSH', '1-4-37-430', '河北省-石家庄市-平山县', 1);
INSERT INTO `qmgx_region` VALUES (431, '130132', '元氏县', 37, 3, 0, 'Yuanshi Xian', 'YSI', '1-4-37-431', '河北省-石家庄市-元氏县', 1);
INSERT INTO `qmgx_region` VALUES (432, '130133', '赵县', 37, 3, 0, 'Zhao Xian', 'ZAO', '1-4-37-432', '河北省-石家庄市-赵县', 1);
INSERT INTO `qmgx_region` VALUES (433, '130181', '辛集市', 37, 3, 0, 'Xinji Shi', 'XJS', '1-4-37-433', '河北省-石家庄市-辛集市', 1);
INSERT INTO `qmgx_region` VALUES (434, '130182', '藁城市', 37, 3, 0, 'Gaocheng Shi', 'GCS', '1-4-37-434', '河北省-石家庄市-藁城市', 1);
INSERT INTO `qmgx_region` VALUES (435, '130183', '晋州市', 37, 3, 0, 'Jinzhou Shi', 'JZJ', '1-4-37-435', '河北省-石家庄市-晋州市', 1);
INSERT INTO `qmgx_region` VALUES (436, '130184', '新乐市', 37, 3, 0, 'Xinle Shi', 'XLE', '1-4-37-436', '河北省-石家庄市-新乐市', 1);
INSERT INTO `qmgx_region` VALUES (437, '130185', '鹿泉市', 37, 3, 0, 'Luquan Shi', 'LUQ', '1-4-37-437', '河北省-石家庄市-鹿泉市', 1);
INSERT INTO `qmgx_region` VALUES (438, '130201', '市辖区', 38, 3, 0, 'Shixiaqu', '2', '1-4-38-438', '河北省-唐山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (439, '130202', '路南区', 38, 3, 0, 'Lunan Qu', 'LNB', '1-4-38-439', '河北省-唐山市-路南区', 1);
INSERT INTO `qmgx_region` VALUES (440, '130203', '路北区', 38, 3, 0, 'Lubei Qu', 'LBQ', '1-4-38-440', '河北省-唐山市-路北区', 1);
INSERT INTO `qmgx_region` VALUES (441, '130204', '古冶区', 38, 3, 0, 'Guye Qu', 'GYE', '1-4-38-441', '河北省-唐山市-古冶区', 1);
INSERT INTO `qmgx_region` VALUES (442, '130205', '开平区', 38, 3, 0, 'Kaiping Qu', 'KPQ', '1-4-38-442', '河北省-唐山市-开平区', 1);
INSERT INTO `qmgx_region` VALUES (443, '130207', '丰南区', 38, 3, 0, 'Fengnan Qu', '2', '1-4-38-443', '河北省-唐山市-丰南区', 1);
INSERT INTO `qmgx_region` VALUES (444, '130208', '丰润区', 38, 3, 0, 'Fengrun Qu', '2', '1-4-38-444', '河北省-唐山市-丰润区', 1);
INSERT INTO `qmgx_region` VALUES (445, '130223', '滦县', 38, 3, 0, 'Luan Xian', 'LUA', '1-4-38-445', '河北省-唐山市-滦县', 1);
INSERT INTO `qmgx_region` VALUES (446, '130224', '滦南县', 38, 3, 0, 'Luannan Xian', 'LNJ', '1-4-38-446', '河北省-唐山市-滦南县', 1);
INSERT INTO `qmgx_region` VALUES (447, '130225', '乐亭县', 38, 3, 0, 'Leting Xian', 'LTJ', '1-4-38-447', '河北省-唐山市-乐亭县', 1);
INSERT INTO `qmgx_region` VALUES (448, '130227', '迁西县', 38, 3, 0, 'Qianxi Xian', 'QXX', '1-4-38-448', '河北省-唐山市-迁西县', 1);
INSERT INTO `qmgx_region` VALUES (449, '130229', '玉田县', 38, 3, 0, 'Yutian Xian', 'YTJ', '1-4-38-449', '河北省-唐山市-玉田县', 1);
INSERT INTO `qmgx_region` VALUES (450, '130230', '唐海县', 38, 3, 0, 'Tanghai Xian ', 'THA', '1-4-38-450', '河北省-唐山市-唐海县', 1);
INSERT INTO `qmgx_region` VALUES (451, '130281', '遵化市', 38, 3, 0, 'Zunhua Shi', 'ZNH', '1-4-38-451', '河北省-唐山市-遵化市', 1);
INSERT INTO `qmgx_region` VALUES (452, '130283', '迁安市', 38, 3, 0, 'Qian,an Shi', 'QAS', '1-4-38-452', '河北省-唐山市-迁安市', 1);
INSERT INTO `qmgx_region` VALUES (453, '130301', '市辖区', 39, 3, 0, 'Shixiaqu', '2', '1-4-39-453', '河北省-秦皇岛市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (454, '130302', '海港区', 39, 3, 0, 'Haigang Qu', 'HGG', '1-4-39-454', '河北省-秦皇岛市-海港区', 1);
INSERT INTO `qmgx_region` VALUES (455, '130303', '山海关区', 39, 3, 0, 'Shanhaiguan Qu', 'SHG', '1-4-39-455', '河北省-秦皇岛市-山海关区', 1);
INSERT INTO `qmgx_region` VALUES (456, '130304', '北戴河区', 39, 3, 0, 'Beidaihe Qu', 'BDH', '1-4-39-456', '河北省-秦皇岛市-北戴河区', 1);
INSERT INTO `qmgx_region` VALUES (457, '130321', '青龙满族自治县', 39, 3, 0, 'Qinglong Manzu Zizhixian', 'QLM', '1-4-39-457', '河北省-秦皇岛市-青龙满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (458, '130322', '昌黎县', 39, 3, 0, 'Changli Xian', 'CGL', '1-4-39-458', '河北省-秦皇岛市-昌黎县', 1);
INSERT INTO `qmgx_region` VALUES (459, '130323', '抚宁县', 39, 3, 0, 'Funing Xian ', 'FUN', '1-4-39-459', '河北省-秦皇岛市-抚宁县', 1);
INSERT INTO `qmgx_region` VALUES (460, '130324', '卢龙县', 39, 3, 0, 'Lulong Xian', 'LLG', '1-4-39-460', '河北省-秦皇岛市-卢龙县', 1);
INSERT INTO `qmgx_region` VALUES (461, '130401', '市辖区', 40, 3, 0, 'Shixiaqu', '2', '1-4-40-461', '河北省-邯郸市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (462, '130402', '邯山区', 40, 3, 0, 'Hanshan Qu', 'HHD', '1-4-40-462', '河北省-邯郸市-邯山区', 1);
INSERT INTO `qmgx_region` VALUES (463, '130403', '丛台区', 40, 3, 0, 'Congtai Qu', 'CTQ', '1-4-40-463', '河北省-邯郸市-丛台区', 1);
INSERT INTO `qmgx_region` VALUES (464, '130404', '复兴区', 40, 3, 0, 'Fuxing Qu', 'FXQ', '1-4-40-464', '河北省-邯郸市-复兴区', 1);
INSERT INTO `qmgx_region` VALUES (465, '130406', '峰峰矿区', 40, 3, 0, 'Fengfeng Kuangqu', 'FFK', '1-4-40-465', '河北省-邯郸市-峰峰矿区', 1);
INSERT INTO `qmgx_region` VALUES (466, '130421', '邯郸县', 40, 3, 0, 'Handan Xian ', 'HDX', '1-4-40-466', '河北省-邯郸市-邯郸县', 1);
INSERT INTO `qmgx_region` VALUES (467, '130423', '临漳县', 40, 3, 0, 'Linzhang Xian ', 'LNZ', '1-4-40-467', '河北省-邯郸市-临漳县', 1);
INSERT INTO `qmgx_region` VALUES (468, '130424', '成安县', 40, 3, 0, 'Cheng,an Xian', 'CAJ', '1-4-40-468', '河北省-邯郸市-成安县', 1);
INSERT INTO `qmgx_region` VALUES (469, '130425', '大名县', 40, 3, 0, 'Daming Xian', 'DMX', '1-4-40-469', '河北省-邯郸市-大名县', 1);
INSERT INTO `qmgx_region` VALUES (470, '130426', '涉县', 40, 3, 0, 'She Xian', 'SEJ', '1-4-40-470', '河北省-邯郸市-涉县', 1);
INSERT INTO `qmgx_region` VALUES (471, '130427', '磁县', 40, 3, 0, 'Ci Xian', 'CIX', '1-4-40-471', '河北省-邯郸市-磁县', 1);
INSERT INTO `qmgx_region` VALUES (472, '130428', '肥乡县', 40, 3, 0, 'Feixiang Xian', 'FXJ', '1-4-40-472', '河北省-邯郸市-肥乡县', 1);
INSERT INTO `qmgx_region` VALUES (473, '130429', '永年县', 40, 3, 0, 'Yongnian Xian', 'YON', '1-4-40-473', '河北省-邯郸市-永年县', 1);
INSERT INTO `qmgx_region` VALUES (474, '130430', '邱县', 40, 3, 0, 'Qiu Xian', 'QIU', '1-4-40-474', '河北省-邯郸市-邱县', 1);
INSERT INTO `qmgx_region` VALUES (475, '130431', '鸡泽县', 40, 3, 0, 'Jize Xian', 'JZE', '1-4-40-475', '河北省-邯郸市-鸡泽县', 1);
INSERT INTO `qmgx_region` VALUES (476, '130432', '广平县', 40, 3, 0, 'Guangping Xian ', 'GPX', '1-4-40-476', '河北省-邯郸市-广平县', 1);
INSERT INTO `qmgx_region` VALUES (477, '130433', '馆陶县', 40, 3, 0, 'Guantao Xian', 'GTO', '1-4-40-477', '河北省-邯郸市-馆陶县', 1);
INSERT INTO `qmgx_region` VALUES (478, '130434', '魏县', 40, 3, 0, 'Wei Xian ', 'WEI', '1-4-40-478', '河北省-邯郸市-魏县', 1);
INSERT INTO `qmgx_region` VALUES (479, '130435', '曲周县', 40, 3, 0, 'Quzhou Xian ', 'QZX', '1-4-40-479', '河北省-邯郸市-曲周县', 1);
INSERT INTO `qmgx_region` VALUES (480, '130481', '武安市', 40, 3, 0, 'Wu,an Shi', 'WUA', '1-4-40-480', '河北省-邯郸市-武安市', 1);
INSERT INTO `qmgx_region` VALUES (481, '130501', '市辖区', 41, 3, 0, 'Shixiaqu', '2', '1-4-41-481', '河北省-邢台市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (482, '130502', '桥东区', 41, 3, 0, 'Qiaodong Qu', 'QDG', '1-4-41-482', '河北省-邢台市-桥东区', 1);
INSERT INTO `qmgx_region` VALUES (483, '130503', '桥西区', 41, 3, 0, 'Qiaoxi Qu', 'QXT', '1-4-41-483', '河北省-邢台市-桥西区', 1);
INSERT INTO `qmgx_region` VALUES (484, '130521', '邢台县', 41, 3, 0, 'Xingtai Xian', 'XTJ', '1-4-41-484', '河北省-邢台市-邢台县', 1);
INSERT INTO `qmgx_region` VALUES (485, '130522', '临城县', 41, 3, 0, 'Lincheng Xian ', 'LNC', '1-4-41-485', '河北省-邢台市-临城县', 1);
INSERT INTO `qmgx_region` VALUES (486, '130523', '内丘县', 41, 3, 0, 'Neiqiu Xian ', 'NQU', '1-4-41-486', '河北省-邢台市-内丘县', 1);
INSERT INTO `qmgx_region` VALUES (487, '130524', '柏乡县', 41, 3, 0, 'Baixiang Xian', 'BXG', '1-4-41-487', '河北省-邢台市-柏乡县', 1);
INSERT INTO `qmgx_region` VALUES (488, '130525', '隆尧县', 41, 3, 0, 'Longyao Xian', 'LYO', '1-4-41-488', '河北省-邢台市-隆尧县', 1);
INSERT INTO `qmgx_region` VALUES (489, '130526', '任县', 41, 3, 0, 'Ren Xian', 'REN', '1-4-41-489', '河北省-邢台市-任县', 1);
INSERT INTO `qmgx_region` VALUES (490, '130527', '南和县', 41, 3, 0, 'Nanhe Xian', 'NHX', '1-4-41-490', '河北省-邢台市-南和县', 1);
INSERT INTO `qmgx_region` VALUES (491, '130528', '宁晋县', 41, 3, 0, 'Ningjin Xian', 'NJN', '1-4-41-491', '河北省-邢台市-宁晋县', 1);
INSERT INTO `qmgx_region` VALUES (492, '130529', '巨鹿县', 41, 3, 0, 'Julu Xian', 'JLU', '1-4-41-492', '河北省-邢台市-巨鹿县', 1);
INSERT INTO `qmgx_region` VALUES (493, '130530', '新河县', 41, 3, 0, 'Xinhe Xian ', 'XHJ', '1-4-41-493', '河北省-邢台市-新河县', 1);
INSERT INTO `qmgx_region` VALUES (494, '130531', '广宗县', 41, 3, 0, 'Guangzong Xian ', 'GZJ', '1-4-41-494', '河北省-邢台市-广宗县', 1);
INSERT INTO `qmgx_region` VALUES (495, '130532', '平乡县', 41, 3, 0, 'Pingxiang Xian', 'PXX', '1-4-41-495', '河北省-邢台市-平乡县', 1);
INSERT INTO `qmgx_region` VALUES (496, '130533', '威县', 41, 3, 0, 'Wei Xian ', 'WEX', '1-4-41-496', '河北省-邢台市-威县', 1);
INSERT INTO `qmgx_region` VALUES (497, '130534', '清河县', 41, 3, 0, 'Qinghe Xian', 'QHE', '1-4-41-497', '河北省-邢台市-清河县', 1);
INSERT INTO `qmgx_region` VALUES (498, '130535', '临西县', 41, 3, 0, 'Linxi Xian', 'LXI', '1-4-41-498', '河北省-邢台市-临西县', 1);
INSERT INTO `qmgx_region` VALUES (499, '130581', '南宫市', 41, 3, 0, 'Nangong Shi', 'NGO', '1-4-41-499', '河北省-邢台市-南宫市', 1);
INSERT INTO `qmgx_region` VALUES (500, '130582', '沙河市', 41, 3, 0, 'Shahe Shi', 'SHS', '1-4-41-500', '河北省-邢台市-沙河市', 1);
INSERT INTO `qmgx_region` VALUES (501, '130601', '市辖区', 42, 3, 0, 'Shixiaqu', '2', '1-4-42-501', '河北省-保定市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (502, '130600', '新市区', 42, 3, 0, 'Xinshi Qu', '2', '1-4-42-502', '河北省-保定市-新市区', 1);
INSERT INTO `qmgx_region` VALUES (503, '130603', '北市区', 42, 3, 0, 'Beishi Qu', 'BSI', '1-4-42-503', '河北省-保定市-北市区', 1);
INSERT INTO `qmgx_region` VALUES (504, '130604', '南市区', 42, 3, 0, 'Nanshi Qu', 'NSB', '1-4-42-504', '河北省-保定市-南市区', 1);
INSERT INTO `qmgx_region` VALUES (505, '130621', '满城县', 42, 3, 0, 'Mancheng Xian ', 'MCE', '1-4-42-505', '河北省-保定市-满城县', 1);
INSERT INTO `qmgx_region` VALUES (506, '130622', '清苑县', 42, 3, 0, 'Qingyuan Xian', 'QYJ', '1-4-42-506', '河北省-保定市-清苑县', 1);
INSERT INTO `qmgx_region` VALUES (507, '130623', '涞水县', 42, 3, 0, 'Laishui Xian', 'LSM', '1-4-42-507', '河北省-保定市-涞水县', 1);
INSERT INTO `qmgx_region` VALUES (508, '130624', '阜平县', 42, 3, 0, 'Fuping Xian ', 'FUP', '1-4-42-508', '河北省-保定市-阜平县', 1);
INSERT INTO `qmgx_region` VALUES (509, '130625', '徐水县', 42, 3, 0, 'Xushui Xian ', 'XSJ', '1-4-42-509', '河北省-保定市-徐水县', 1);
INSERT INTO `qmgx_region` VALUES (510, '130626', '定兴县', 42, 3, 0, 'Dingxing Xian ', 'DXG', '1-4-42-510', '河北省-保定市-定兴县', 1);
INSERT INTO `qmgx_region` VALUES (511, '130627', '唐县', 42, 3, 0, 'Tang Xian ', 'TAG', '1-4-42-511', '河北省-保定市-唐县', 1);
INSERT INTO `qmgx_region` VALUES (512, '130628', '高阳县', 42, 3, 0, 'Gaoyang Xian ', 'GAY', '1-4-42-512', '河北省-保定市-高阳县', 1);
INSERT INTO `qmgx_region` VALUES (513, '130629', '容城县', 42, 3, 0, 'Rongcheng Xian ', 'RCX', '1-4-42-513', '河北省-保定市-容城县', 1);
INSERT INTO `qmgx_region` VALUES (514, '130630', '涞源县', 42, 3, 0, 'Laiyuan Xian ', 'LIY', '1-4-42-514', '河北省-保定市-涞源县', 1);
INSERT INTO `qmgx_region` VALUES (515, '130631', '望都县', 42, 3, 0, 'Wangdu Xian ', 'WDU', '1-4-42-515', '河北省-保定市-望都县', 1);
INSERT INTO `qmgx_region` VALUES (516, '130632', '安新县', 42, 3, 0, 'Anxin Xian ', 'AXX', '1-4-42-516', '河北省-保定市-安新县', 1);
INSERT INTO `qmgx_region` VALUES (517, '130633', '易县', 42, 3, 0, 'Yi Xian', 'YII', '1-4-42-517', '河北省-保定市-易县', 1);
INSERT INTO `qmgx_region` VALUES (518, '130634', '曲阳县', 42, 3, 0, 'Quyang Xian ', 'QUY', '1-4-42-518', '河北省-保定市-曲阳县', 1);
INSERT INTO `qmgx_region` VALUES (519, '130635', '蠡县', 42, 3, 0, 'Li Xian', 'LXJ', '1-4-42-519', '河北省-保定市-蠡县', 1);
INSERT INTO `qmgx_region` VALUES (520, '130636', '顺平县', 42, 3, 0, 'Shunping Xian ', 'SPI', '1-4-42-520', '河北省-保定市-顺平县', 1);
INSERT INTO `qmgx_region` VALUES (521, '130637', '博野县', 42, 3, 0, 'Boye Xian ', 'BYE', '1-4-42-521', '河北省-保定市-博野县', 1);
INSERT INTO `qmgx_region` VALUES (522, '130638', '雄县', 42, 3, 0, 'Xiong Xian', 'XOX', '1-4-42-522', '河北省-保定市-雄县', 1);
INSERT INTO `qmgx_region` VALUES (523, '130681', '涿州市', 42, 3, 0, 'Zhuozhou Shi', 'ZZO', '1-4-42-523', '河北省-保定市-涿州市', 1);
INSERT INTO `qmgx_region` VALUES (524, '130682', '定州市', 42, 3, 0, 'Dingzhou Shi ', 'DZO', '1-4-42-524', '河北省-保定市-定州市', 1);
INSERT INTO `qmgx_region` VALUES (525, '130683', '安国市', 42, 3, 0, 'Anguo Shi ', 'AGO', '1-4-42-525', '河北省-保定市-安国市', 1);
INSERT INTO `qmgx_region` VALUES (526, '130684', '高碑店市', 42, 3, 0, 'Gaobeidian Shi', 'GBD', '1-4-42-526', '河北省-保定市-高碑店市', 1);
INSERT INTO `qmgx_region` VALUES (527, '130701', '市辖区', 43, 3, 0, 'Shixiaqu', '2', '1-4-43-527', '河北省-张家口市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (528, '130702', '桥东区', 43, 3, 0, 'Qiaodong Qu', 'QDZ', '1-4-43-528', '河北省-张家口市-桥东区', 1);
INSERT INTO `qmgx_region` VALUES (529, '130703', '桥西区', 43, 3, 0, 'Qiaoxi Qu', 'QXI', '1-4-43-529', '河北省-张家口市-桥西区', 1);
INSERT INTO `qmgx_region` VALUES (530, '130705', '宣化区', 43, 3, 0, 'Xuanhua Qu', 'XHZ', '1-4-43-530', '河北省-张家口市-宣化区', 1);
INSERT INTO `qmgx_region` VALUES (531, '130706', '下花园区', 43, 3, 0, 'Xiahuayuan Qu ', 'XHY', '1-4-43-531', '河北省-张家口市-下花园区', 1);
INSERT INTO `qmgx_region` VALUES (532, '130721', '宣化县', 43, 3, 0, 'Xuanhua Xian ', 'XHX', '1-4-43-532', '河北省-张家口市-宣化县', 1);
INSERT INTO `qmgx_region` VALUES (533, '130722', '张北县', 43, 3, 0, 'Zhangbei Xian ', 'ZGB', '1-4-43-533', '河北省-张家口市-张北县', 1);
INSERT INTO `qmgx_region` VALUES (534, '130723', '康保县', 43, 3, 0, 'Kangbao Xian', 'KBO', '1-4-43-534', '河北省-张家口市-康保县', 1);
INSERT INTO `qmgx_region` VALUES (535, '130724', '沽源县', 43, 3, 0, 'Guyuan Xian', '2', '1-4-43-535', '河北省-张家口市-沽源县', 1);
INSERT INTO `qmgx_region` VALUES (536, '130725', '尚义县', 43, 3, 0, 'Shangyi Xian', 'SYK', '1-4-43-536', '河北省-张家口市-尚义县', 1);
INSERT INTO `qmgx_region` VALUES (537, '130726', '蔚县', 43, 3, 0, 'Yu Xian', 'YXJ', '1-4-43-537', '河北省-张家口市-蔚县', 1);
INSERT INTO `qmgx_region` VALUES (538, '130727', '阳原县', 43, 3, 0, 'Yangyuan Xian', 'YYN', '1-4-43-538', '河北省-张家口市-阳原县', 1);
INSERT INTO `qmgx_region` VALUES (539, '130728', '怀安县', 43, 3, 0, 'Huai,an Xian', 'HAX', '1-4-43-539', '河北省-张家口市-怀安县', 1);
INSERT INTO `qmgx_region` VALUES (540, '130729', '万全县', 43, 3, 0, 'Wanquan Xian ', 'WQN', '1-4-43-540', '河北省-张家口市-万全县', 1);
INSERT INTO `qmgx_region` VALUES (541, '130730', '怀来县', 43, 3, 0, 'Huailai Xian', 'HLA', '1-4-43-541', '河北省-张家口市-怀来县', 1);
INSERT INTO `qmgx_region` VALUES (542, '130731', '涿鹿县', 43, 3, 0, 'Zhuolu Xian ', 'ZLU', '1-4-43-542', '河北省-张家口市-涿鹿县', 1);
INSERT INTO `qmgx_region` VALUES (543, '130732', '赤城县', 43, 3, 0, 'Chicheng Xian', 'CCX', '1-4-43-543', '河北省-张家口市-赤城县', 1);
INSERT INTO `qmgx_region` VALUES (544, '130733', '崇礼县', 43, 3, 0, 'Chongli Xian', 'COL', '1-4-43-544', '河北省-张家口市-崇礼县', 1);
INSERT INTO `qmgx_region` VALUES (545, '130801', '市辖区', 44, 3, 0, 'Shixiaqu', '2', '1-4-44-545', '河北省-承德市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (546, '130802', '双桥区', 44, 3, 0, 'Shuangqiao Qu ', 'SQO', '1-4-44-546', '河北省-承德市-双桥区', 1);
INSERT INTO `qmgx_region` VALUES (547, '130803', '双滦区', 44, 3, 0, 'Shuangluan Qu', 'SLQ', '1-4-44-547', '河北省-承德市-双滦区', 1);
INSERT INTO `qmgx_region` VALUES (548, '130804', '鹰手营子矿区', 44, 3, 0, 'Yingshouyingzi Kuangqu', 'YSY', '1-4-44-548', '河北省-承德市-鹰手营子矿区', 1);
INSERT INTO `qmgx_region` VALUES (549, '130821', '承德县', 44, 3, 0, 'Chengde Xian', 'CDX', '1-4-44-549', '河北省-承德市-承德县', 1);
INSERT INTO `qmgx_region` VALUES (550, '130822', '兴隆县', 44, 3, 0, 'Xinglong Xian', 'XLJ', '1-4-44-550', '河北省-承德市-兴隆县', 1);
INSERT INTO `qmgx_region` VALUES (551, '130823', '平泉县', 44, 3, 0, 'Pingquan Xian', 'PQN', '1-4-44-551', '河北省-承德市-平泉县', 1);
INSERT INTO `qmgx_region` VALUES (552, '130824', '滦平县', 44, 3, 0, 'Luanping Xian ', 'LUP', '1-4-44-552', '河北省-承德市-滦平县', 1);
INSERT INTO `qmgx_region` VALUES (553, '130825', '隆化县', 44, 3, 0, 'Longhua Xian', 'LHJ', '1-4-44-553', '河北省-承德市-隆化县', 1);
INSERT INTO `qmgx_region` VALUES (554, '130826', '丰宁满族自治县', 44, 3, 0, 'Fengning Manzu Zizhixian', 'FNJ', '1-4-44-554', '河北省-承德市-丰宁满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (555, '130827', '宽城满族自治县', 44, 3, 0, 'Kuancheng Manzu Zizhixian', 'KCX', '1-4-44-555', '河北省-承德市-宽城满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (556, '130828', '围场满族蒙古族自治县', 44, 3, 0, 'Weichang Manzu Menggolzu Zizhixian', 'WCJ', '1-4-44-556', '河北省-承德市-围场满族蒙古族自治县', 1);
INSERT INTO `qmgx_region` VALUES (557, '130901', '市辖区', 45, 3, 0, 'Shixiaqu', '2', '1-4-45-557', '河北省-沧州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (558, '130902', '新华区', 45, 3, 0, 'Xinhua Qu', 'XHF', '1-4-45-558', '河北省-沧州市-新华区', 1);
INSERT INTO `qmgx_region` VALUES (559, '130903', '运河区', 45, 3, 0, 'Yunhe Qu', 'YHC', '1-4-45-559', '河北省-沧州市-运河区', 1);
INSERT INTO `qmgx_region` VALUES (560, '130921', '沧县', 45, 3, 0, 'Cang Xian', 'CAG', '1-4-45-560', '河北省-沧州市-沧县', 1);
INSERT INTO `qmgx_region` VALUES (561, '130922', '青县', 45, 3, 0, 'Qing Xian', 'QIG', '1-4-45-561', '河北省-沧州市-青县', 1);
INSERT INTO `qmgx_region` VALUES (562, '130923', '东光县', 45, 3, 0, 'Dongguang Xian ', 'DGU', '1-4-45-562', '河北省-沧州市-东光县', 1);
INSERT INTO `qmgx_region` VALUES (563, '130924', '海兴县', 45, 3, 0, 'Haixing Xian', 'HXG', '1-4-45-563', '河北省-沧州市-海兴县', 1);
INSERT INTO `qmgx_region` VALUES (564, '130925', '盐山县', 45, 3, 0, 'Yanshan Xian', 'YNS', '1-4-45-564', '河北省-沧州市-盐山县', 1);
INSERT INTO `qmgx_region` VALUES (565, '130926', '肃宁县', 45, 3, 0, 'Suning Xian ', 'SNG', '1-4-45-565', '河北省-沧州市-肃宁县', 1);
INSERT INTO `qmgx_region` VALUES (566, '130927', '南皮县', 45, 3, 0, 'Nanpi Xian', 'NPI', '1-4-45-566', '河北省-沧州市-南皮县', 1);
INSERT INTO `qmgx_region` VALUES (567, '130928', '吴桥县', 45, 3, 0, 'Wuqiao Xian ', 'WUQ', '1-4-45-567', '河北省-沧州市-吴桥县', 1);
INSERT INTO `qmgx_region` VALUES (568, '130929', '献县', 45, 3, 0, 'Xian Xian ', 'XXN', '1-4-45-568', '河北省-沧州市-献县', 1);
INSERT INTO `qmgx_region` VALUES (569, '130930', '孟村回族自治县', 45, 3, 0, 'Mengcun Huizu Zizhixian', 'MCN', '1-4-45-569', '河北省-沧州市-孟村回族自治县', 1);
INSERT INTO `qmgx_region` VALUES (570, '130981', '泊头市', 45, 3, 0, 'Botou Shi ', 'BOT', '1-4-45-570', '河北省-沧州市-泊头市', 1);
INSERT INTO `qmgx_region` VALUES (571, '130982', '任丘市', 45, 3, 0, 'Renqiu Shi', 'RQS', '1-4-45-571', '河北省-沧州市-任丘市', 1);
INSERT INTO `qmgx_region` VALUES (572, '130983', '黄骅市', 45, 3, 0, 'Huanghua Shi', 'HHJ', '1-4-45-572', '河北省-沧州市-黄骅市', 1);
INSERT INTO `qmgx_region` VALUES (573, '130984', '河间市', 45, 3, 0, 'Hejian Shi', 'HJN', '1-4-45-573', '河北省-沧州市-河间市', 1);
INSERT INTO `qmgx_region` VALUES (574, '131001', '市辖区', 46, 3, 0, 'Shixiaqu', '2', '1-4-46-574', '河北省-廊坊市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (575, '131002', '安次区', 46, 3, 0, 'Anci Qu', 'ACI', '1-4-46-575', '河北省-廊坊市-安次区', 1);
INSERT INTO `qmgx_region` VALUES (576, '131003', '广阳区', 46, 3, 0, 'Guangyang Qu', '2', '1-4-46-576', '河北省-廊坊市-广阳区', 1);
INSERT INTO `qmgx_region` VALUES (577, '131022', '固安县', 46, 3, 0, 'Gu,an Xian', 'GUA', '1-4-46-577', '河北省-廊坊市-固安县', 1);
INSERT INTO `qmgx_region` VALUES (578, '131023', '永清县', 46, 3, 0, 'Yongqing Xian ', 'YQG', '1-4-46-578', '河北省-廊坊市-永清县', 1);
INSERT INTO `qmgx_region` VALUES (579, '131024', '香河县', 46, 3, 0, 'Xianghe Xian', 'XGH', '1-4-46-579', '河北省-廊坊市-香河县', 1);
INSERT INTO `qmgx_region` VALUES (580, '131025', '大城县', 46, 3, 0, 'Dacheng Xian', 'DCJ', '1-4-46-580', '河北省-廊坊市-大城县', 1);
INSERT INTO `qmgx_region` VALUES (581, '131026', '文安县', 46, 3, 0, 'Wen,an Xian', 'WEA', '1-4-46-581', '河北省-廊坊市-文安县', 1);
INSERT INTO `qmgx_region` VALUES (582, '131028', '大厂回族自治县', 46, 3, 0, 'Dachang Huizu Zizhixian', 'DCG', '1-4-46-582', '河北省-廊坊市-大厂回族自治县', 1);
INSERT INTO `qmgx_region` VALUES (583, '131081', '霸州市', 46, 3, 0, 'Bazhou Shi', 'BZO', '1-4-46-583', '河北省-廊坊市-霸州市', 1);
INSERT INTO `qmgx_region` VALUES (584, '131082', '三河市', 46, 3, 0, 'Sanhe Shi', 'SNH', '1-4-46-584', '河北省-廊坊市-三河市', 1);
INSERT INTO `qmgx_region` VALUES (585, '131101', '市辖区', 47, 3, 0, 'Shixiaqu', '2', '1-4-47-585', '河北省-衡水市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (586, '131102', '桃城区', 47, 3, 0, 'Taocheng Qu', 'TOC', '1-4-47-586', '河北省-衡水市-桃城区', 1);
INSERT INTO `qmgx_region` VALUES (587, '131121', '枣强县', 47, 3, 0, 'Zaoqiang Xian ', 'ZQJ', '1-4-47-587', '河北省-衡水市-枣强县', 1);
INSERT INTO `qmgx_region` VALUES (588, '131122', '武邑县', 47, 3, 0, 'Wuyi Xian', 'WYI', '1-4-47-588', '河北省-衡水市-武邑县', 1);
INSERT INTO `qmgx_region` VALUES (589, '131123', '武强县', 47, 3, 0, 'Wuqiang Xian ', 'WQG', '1-4-47-589', '河北省-衡水市-武强县', 1);
INSERT INTO `qmgx_region` VALUES (590, '131124', '饶阳县', 47, 3, 0, 'Raoyang Xian', 'RYG', '1-4-47-590', '河北省-衡水市-饶阳县', 1);
INSERT INTO `qmgx_region` VALUES (591, '131125', '安平县', 47, 3, 0, 'Anping Xian', 'APG', '1-4-47-591', '河北省-衡水市-安平县', 1);
INSERT INTO `qmgx_region` VALUES (592, '131126', '故城县', 47, 3, 0, 'Gucheng Xian', 'GCE', '1-4-47-592', '河北省-衡水市-故城县', 1);
INSERT INTO `qmgx_region` VALUES (593, '131127', '景县', 47, 3, 0, 'Jing Xian ', 'JIG', '1-4-47-593', '河北省-衡水市-景县', 1);
INSERT INTO `qmgx_region` VALUES (594, '131128', '阜城县', 47, 3, 0, 'Fucheng Xian ', 'FCE', '1-4-47-594', '河北省-衡水市-阜城县', 1);
INSERT INTO `qmgx_region` VALUES (595, '131181', '冀州市', 47, 3, 0, 'Jizhou Shi ', 'JIZ', '1-4-47-595', '河北省-衡水市-冀州市', 1);
INSERT INTO `qmgx_region` VALUES (596, '131182', '深州市', 47, 3, 0, 'Shenzhou Shi', 'SNZ', '1-4-47-596', '河北省-衡水市-深州市', 1);
INSERT INTO `qmgx_region` VALUES (597, '140101', '市辖区', 48, 3, 0, 'Shixiaqu', '2', '1-5-48-597', '山西省-太原市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (598, '140105', '小店区', 48, 3, 0, 'Xiaodian Qu', 'XDQ', '1-5-48-598', '山西省-太原市-小店区', 1);
INSERT INTO `qmgx_region` VALUES (599, '140106', '迎泽区', 48, 3, 0, 'Yingze Qu', 'YZT', '1-5-48-599', '山西省-太原市-迎泽区', 1);
INSERT INTO `qmgx_region` VALUES (600, '140107', '杏花岭区', 48, 3, 0, 'Xinghualing Qu', 'XHL', '1-5-48-600', '山西省-太原市-杏花岭区', 1);
INSERT INTO `qmgx_region` VALUES (601, '140108', '尖草坪区', 48, 3, 0, 'Jiancaoping Qu', 'JCP', '1-5-48-601', '山西省-太原市-尖草坪区', 1);
INSERT INTO `qmgx_region` VALUES (602, '140109', '万柏林区', 48, 3, 0, 'Wanbailin Qu', 'WBL', '1-5-48-602', '山西省-太原市-万柏林区', 1);
INSERT INTO `qmgx_region` VALUES (603, '140110', '晋源区', 48, 3, 0, 'Jinyuan Qu', 'JYM', '1-5-48-603', '山西省-太原市-晋源区', 1);
INSERT INTO `qmgx_region` VALUES (604, '140121', '清徐县', 48, 3, 0, 'Qingxu Xian ', 'QXU', '1-5-48-604', '山西省-太原市-清徐县', 1);
INSERT INTO `qmgx_region` VALUES (605, '140122', '阳曲县', 48, 3, 0, 'Yangqu Xian ', 'YGQ', '1-5-48-605', '山西省-太原市-阳曲县', 1);
INSERT INTO `qmgx_region` VALUES (606, '140123', '娄烦县', 48, 3, 0, 'Loufan Xian', 'LFA', '1-5-48-606', '山西省-太原市-娄烦县', 1);
INSERT INTO `qmgx_region` VALUES (607, '140181', '古交市', 48, 3, 0, 'Gujiao Shi', 'GUJ', '1-5-48-607', '山西省-太原市-古交市', 1);
INSERT INTO `qmgx_region` VALUES (608, '140201', '市辖区', 49, 3, 0, 'Shixiaqu', '2', '1-5-49-608', '山西省-大同市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (609, '140202', '城区', 49, 3, 0, 'Chengqu', 'CQD', '1-5-49-609', '山西省-大同市-城区', 1);
INSERT INTO `qmgx_region` VALUES (610, '140203', '矿区', 49, 3, 0, 'Kuangqu', 'KQD', '1-5-49-610', '山西省-大同市-矿区', 1);
INSERT INTO `qmgx_region` VALUES (611, '140211', '南郊区', 49, 3, 0, 'Nanjiao Qu', 'NJQ', '1-5-49-611', '山西省-大同市-南郊区', 1);
INSERT INTO `qmgx_region` VALUES (612, '140212', '新荣区', 49, 3, 0, 'Xinrong Qu', 'XRQ', '1-5-49-612', '山西省-大同市-新荣区', 1);
INSERT INTO `qmgx_region` VALUES (613, '140221', '阳高县', 49, 3, 0, 'Yanggao Xian ', 'YGO', '1-5-49-613', '山西省-大同市-阳高县', 1);
INSERT INTO `qmgx_region` VALUES (614, '140222', '天镇县', 49, 3, 0, 'Tianzhen Xian ', 'TZE', '1-5-49-614', '山西省-大同市-天镇县', 1);
INSERT INTO `qmgx_region` VALUES (615, '140223', '广灵县', 49, 3, 0, 'Guangling Xian ', 'GLJ', '1-5-49-615', '山西省-大同市-广灵县', 1);
INSERT INTO `qmgx_region` VALUES (616, '140224', '灵丘县', 49, 3, 0, 'Lingqiu Xian ', 'LQX', '1-5-49-616', '山西省-大同市-灵丘县', 1);
INSERT INTO `qmgx_region` VALUES (617, '140225', '浑源县', 49, 3, 0, 'Hunyuan Xian', 'HYM', '1-5-49-617', '山西省-大同市-浑源县', 1);
INSERT INTO `qmgx_region` VALUES (618, '140226', '左云县', 49, 3, 0, 'Zuoyun Xian', 'ZUY', '1-5-49-618', '山西省-大同市-左云县', 1);
INSERT INTO `qmgx_region` VALUES (619, '140227', '大同县', 49, 3, 0, 'Datong Xian ', 'DTX', '1-5-49-619', '山西省-大同市-大同县', 1);
INSERT INTO `qmgx_region` VALUES (620, '140301', '市辖区', 50, 3, 0, 'Shixiaqu', '2', '1-5-50-620', '山西省-阳泉市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (621, '140302', '城区', 50, 3, 0, 'Chengqu', 'CQU', '1-5-50-621', '山西省-阳泉市-城区', 1);
INSERT INTO `qmgx_region` VALUES (622, '140303', '矿区', 50, 3, 0, 'Kuangqu', 'KQY', '1-5-50-622', '山西省-阳泉市-矿区', 1);
INSERT INTO `qmgx_region` VALUES (623, '140311', '郊区', 50, 3, 0, 'Jiaoqu', 'JQY', '1-5-50-623', '山西省-阳泉市-郊区', 1);
INSERT INTO `qmgx_region` VALUES (624, '140321', '平定县', 50, 3, 0, 'Pingding Xian', 'PDG', '1-5-50-624', '山西省-阳泉市-平定县', 1);
INSERT INTO `qmgx_region` VALUES (625, '140322', '盂县', 50, 3, 0, 'Yu Xian', 'YUX', '1-5-50-625', '山西省-阳泉市-盂县', 1);
INSERT INTO `qmgx_region` VALUES (626, '140401', '市辖区', 51, 3, 0, 'Shixiaqu', '2', '1-5-51-626', '山西省-长治市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (627, '140402', '城区', 51, 3, 0, 'Chengqu ', 'CQC', '1-5-51-627', '山西省-长治市-城区', 1);
INSERT INTO `qmgx_region` VALUES (628, '140411', '郊区', 51, 3, 0, 'Jiaoqu', 'JQZ', '1-5-51-628', '山西省-长治市-郊区', 1);
INSERT INTO `qmgx_region` VALUES (629, '140421', '长治县', 51, 3, 0, 'Changzhi Xian', 'CZI', '1-5-51-629', '山西省-长治市-长治县', 1);
INSERT INTO `qmgx_region` VALUES (630, '140423', '襄垣县', 51, 3, 0, 'Xiangyuan Xian', 'XYJ', '1-5-51-630', '山西省-长治市-襄垣县', 1);
INSERT INTO `qmgx_region` VALUES (631, '140424', '屯留县', 51, 3, 0, 'Tunliu Xian', 'TNL', '1-5-51-631', '山西省-长治市-屯留县', 1);
INSERT INTO `qmgx_region` VALUES (632, '140425', '平顺县', 51, 3, 0, 'Pingshun Xian', 'PSX', '1-5-51-632', '山西省-长治市-平顺县', 1);
INSERT INTO `qmgx_region` VALUES (633, '140426', '黎城县', 51, 3, 0, 'Licheng Xian', 'LIC', '1-5-51-633', '山西省-长治市-黎城县', 1);
INSERT INTO `qmgx_region` VALUES (634, '140427', '壶关县', 51, 3, 0, 'Huguan Xian', 'HGN', '1-5-51-634', '山西省-长治市-壶关县', 1);
INSERT INTO `qmgx_region` VALUES (635, '140428', '长子县', 51, 3, 0, 'Zhangzi Xian ', 'ZHZ', '1-5-51-635', '山西省-长治市-长子县', 1);
INSERT INTO `qmgx_region` VALUES (636, '140429', '武乡县', 51, 3, 0, 'Wuxiang Xian', 'WXG', '1-5-51-636', '山西省-长治市-武乡县', 1);
INSERT INTO `qmgx_region` VALUES (637, '140430', '沁县', 51, 3, 0, 'Qin Xian', 'QIN', '1-5-51-637', '山西省-长治市-沁县', 1);
INSERT INTO `qmgx_region` VALUES (638, '140431', '沁源县', 51, 3, 0, 'Qinyuan Xian ', 'QYU', '1-5-51-638', '山西省-长治市-沁源县', 1);
INSERT INTO `qmgx_region` VALUES (639, '140481', '潞城市', 51, 3, 0, 'Lucheng Shi', 'LCS', '1-5-51-639', '山西省-长治市-潞城市', 1);
INSERT INTO `qmgx_region` VALUES (640, '140501', '市辖区', 52, 3, 0, 'Shixiaqu', '2', '1-5-52-640', '山西省-晋城市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (641, '140502', '城区', 52, 3, 0, 'Chengqu ', 'CQJ', '1-5-52-641', '山西省-晋城市-城区', 1);
INSERT INTO `qmgx_region` VALUES (642, '140521', '沁水县', 52, 3, 0, 'Qinshui Xian', 'QSI', '1-5-52-642', '山西省-晋城市-沁水县', 1);
INSERT INTO `qmgx_region` VALUES (643, '140522', '阳城县', 52, 3, 0, 'Yangcheng Xian ', 'YGC', '1-5-52-643', '山西省-晋城市-阳城县', 1);
INSERT INTO `qmgx_region` VALUES (644, '140524', '陵川县', 52, 3, 0, 'Lingchuan Xian', 'LGC', '1-5-52-644', '山西省-晋城市-陵川县', 1);
INSERT INTO `qmgx_region` VALUES (645, '140525', '泽州县', 52, 3, 0, 'Zezhou Xian', 'ZEZ', '1-5-52-645', '山西省-晋城市-泽州县', 1);
INSERT INTO `qmgx_region` VALUES (646, '140581', '高平市', 52, 3, 0, 'Gaoping Shi ', 'GPG', '1-5-52-646', '山西省-晋城市-高平市', 1);
INSERT INTO `qmgx_region` VALUES (647, '140601', '市辖区', 53, 3, 0, 'Shixiaqu', '2', '1-5-53-647', '山西省-朔州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (648, '140602', '朔城区', 53, 3, 0, 'Shuocheng Qu', 'SCH', '1-5-53-648', '山西省-朔州市-朔城区', 1);
INSERT INTO `qmgx_region` VALUES (649, '140603', '平鲁区', 53, 3, 0, 'Pinglu Qu', 'PLU', '1-5-53-649', '山西省-朔州市-平鲁区', 1);
INSERT INTO `qmgx_region` VALUES (650, '140621', '山阴县', 53, 3, 0, 'Shanyin Xian', 'SYP', '1-5-53-650', '山西省-朔州市-山阴县', 1);
INSERT INTO `qmgx_region` VALUES (651, '140622', '应县', 53, 3, 0, 'Ying Xian', 'YIG', '1-5-53-651', '山西省-朔州市-应县', 1);
INSERT INTO `qmgx_region` VALUES (652, '140623', '右玉县', 53, 3, 0, 'Youyu Xian ', 'YOY', '1-5-53-652', '山西省-朔州市-右玉县', 1);
INSERT INTO `qmgx_region` VALUES (653, '140624', '怀仁县', 53, 3, 0, 'Huairen Xian', 'HRN', '1-5-53-653', '山西省-朔州市-怀仁县', 1);
INSERT INTO `qmgx_region` VALUES (654, '140701', '市辖区', 54, 3, 0, '1', '2', '1-5-54-654', '山西省-晋中市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (655, '140702', '榆次区', 54, 3, 0, 'Yuci Qu', '2', '1-5-54-655', '山西省-晋中市-榆次区', 1);
INSERT INTO `qmgx_region` VALUES (656, '140721', '榆社县', 54, 3, 0, 'Yushe Xian', '2', '1-5-54-656', '山西省-晋中市-榆社县', 1);
INSERT INTO `qmgx_region` VALUES (657, '140722', '左权县', 54, 3, 0, 'Zuoquan Xian', '2', '1-5-54-657', '山西省-晋中市-左权县', 1);
INSERT INTO `qmgx_region` VALUES (658, '140723', '和顺县', 54, 3, 0, 'Heshun Xian', '2', '1-5-54-658', '山西省-晋中市-和顺县', 1);
INSERT INTO `qmgx_region` VALUES (659, '140724', '昔阳县', 54, 3, 0, 'Xiyang Xian', '2', '1-5-54-659', '山西省-晋中市-昔阳县', 1);
INSERT INTO `qmgx_region` VALUES (660, '140725', '寿阳县', 54, 3, 0, 'Shouyang Xian', '2', '1-5-54-660', '山西省-晋中市-寿阳县', 1);
INSERT INTO `qmgx_region` VALUES (661, '140726', '太谷县', 54, 3, 0, 'Taigu Xian', '2', '1-5-54-661', '山西省-晋中市-太谷县', 1);
INSERT INTO `qmgx_region` VALUES (662, '140727', '祁县', 54, 3, 0, 'Qi Xian', '2', '1-5-54-662', '山西省-晋中市-祁县', 1);
INSERT INTO `qmgx_region` VALUES (663, '140728', '平遥县', 54, 3, 0, 'Pingyao Xian', '2', '1-5-54-663', '山西省-晋中市-平遥县', 1);
INSERT INTO `qmgx_region` VALUES (664, '140729', '灵石县', 54, 3, 0, 'Lingshi Xian', '2', '1-5-54-664', '山西省-晋中市-灵石县', 1);
INSERT INTO `qmgx_region` VALUES (665, '140781', '介休市', 54, 3, 0, 'Jiexiu Shi', '2', '1-5-54-665', '山西省-晋中市-介休市', 1);
INSERT INTO `qmgx_region` VALUES (666, '140801', '市辖区', 55, 3, 0, '1', '2', '1-5-55-666', '山西省-运城市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (667, '140802', '盐湖区', 55, 3, 0, 'Yanhu Qu', '2', '1-5-55-667', '山西省-运城市-盐湖区', 1);
INSERT INTO `qmgx_region` VALUES (668, '140821', '临猗县', 55, 3, 0, 'Linyi Xian', '2', '1-5-55-668', '山西省-运城市-临猗县', 1);
INSERT INTO `qmgx_region` VALUES (669, '140822', '万荣县', 55, 3, 0, 'Wanrong Xian', '2', '1-5-55-669', '山西省-运城市-万荣县', 1);
INSERT INTO `qmgx_region` VALUES (670, '140823', '闻喜县', 55, 3, 0, 'Wenxi Xian', '2', '1-5-55-670', '山西省-运城市-闻喜县', 1);
INSERT INTO `qmgx_region` VALUES (671, '140824', '稷山县', 55, 3, 0, 'Jishan Xian', '2', '1-5-55-671', '山西省-运城市-稷山县', 1);
INSERT INTO `qmgx_region` VALUES (672, '140825', '新绛县', 55, 3, 0, 'Xinjiang Xian', '2', '1-5-55-672', '山西省-运城市-新绛县', 1);
INSERT INTO `qmgx_region` VALUES (673, '140826', '绛县', 55, 3, 0, 'Jiang Xian', '2', '1-5-55-673', '山西省-运城市-绛县', 1);
INSERT INTO `qmgx_region` VALUES (674, '140827', '垣曲县', 55, 3, 0, 'Yuanqu Xian', '2', '1-5-55-674', '山西省-运城市-垣曲县', 1);
INSERT INTO `qmgx_region` VALUES (675, '140828', '夏县', 55, 3, 0, 'Xia Xian ', '2', '1-5-55-675', '山西省-运城市-夏县', 1);
INSERT INTO `qmgx_region` VALUES (676, '140829', '平陆县', 55, 3, 0, 'Pinglu Xian', '2', '1-5-55-676', '山西省-运城市-平陆县', 1);
INSERT INTO `qmgx_region` VALUES (677, '140830', '芮城县', 55, 3, 0, 'Ruicheng Xian', '2', '1-5-55-677', '山西省-运城市-芮城县', 1);
INSERT INTO `qmgx_region` VALUES (678, '140881', '永济市', 55, 3, 0, 'Yongji Shi ', '2', '1-5-55-678', '山西省-运城市-永济市', 1);
INSERT INTO `qmgx_region` VALUES (679, '140882', '河津市', 55, 3, 0, 'Hejin Shi', '2', '1-5-55-679', '山西省-运城市-河津市', 1);
INSERT INTO `qmgx_region` VALUES (680, '140901', '市辖区', 56, 3, 0, '1', '2', '1-5-56-680', '山西省-忻州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (681, '140902', '忻府区', 56, 3, 0, 'Xinfu Qu', '2', '1-5-56-681', '山西省-忻州市-忻府区', 1);
INSERT INTO `qmgx_region` VALUES (682, '140921', '定襄县', 56, 3, 0, 'Dingxiang Xian', '2', '1-5-56-682', '山西省-忻州市-定襄县', 1);
INSERT INTO `qmgx_region` VALUES (683, '140922', '五台县', 56, 3, 0, 'Wutai Xian', '2', '1-5-56-683', '山西省-忻州市-五台县', 1);
INSERT INTO `qmgx_region` VALUES (684, '140923', '代县', 56, 3, 0, 'Dai Xian', '2', '1-5-56-684', '山西省-忻州市-代县', 1);
INSERT INTO `qmgx_region` VALUES (685, '140924', '繁峙县', 56, 3, 0, 'Fanshi Xian', '2', '1-5-56-685', '山西省-忻州市-繁峙县', 1);
INSERT INTO `qmgx_region` VALUES (686, '140925', '宁武县', 56, 3, 0, 'Ningwu Xian', '2', '1-5-56-686', '山西省-忻州市-宁武县', 1);
INSERT INTO `qmgx_region` VALUES (687, '140926', '静乐县', 56, 3, 0, 'Jingle Xian', '2', '1-5-56-687', '山西省-忻州市-静乐县', 1);
INSERT INTO `qmgx_region` VALUES (688, '140927', '神池县', 56, 3, 0, 'Shenchi Xian', '2', '1-5-56-688', '山西省-忻州市-神池县', 1);
INSERT INTO `qmgx_region` VALUES (689, '140928', '五寨县', 56, 3, 0, 'Wuzhai Xian', '2', '1-5-56-689', '山西省-忻州市-五寨县', 1);
INSERT INTO `qmgx_region` VALUES (690, '140929', '岢岚县', 56, 3, 0, 'Kelan Xian', '2', '1-5-56-690', '山西省-忻州市-岢岚县', 1);
INSERT INTO `qmgx_region` VALUES (691, '140930', '河曲县', 56, 3, 0, 'Hequ Xian ', '2', '1-5-56-691', '山西省-忻州市-河曲县', 1);
INSERT INTO `qmgx_region` VALUES (692, '140931', '保德县', 56, 3, 0, 'Baode Xian', '2', '1-5-56-692', '山西省-忻州市-保德县', 1);
INSERT INTO `qmgx_region` VALUES (693, '140932', '偏关县', 56, 3, 0, 'Pianguan Xian', '2', '1-5-56-693', '山西省-忻州市-偏关县', 1);
INSERT INTO `qmgx_region` VALUES (694, '140981', '原平市', 56, 3, 0, 'Yuanping Shi', '2', '1-5-56-694', '山西省-忻州市-原平市', 1);
INSERT INTO `qmgx_region` VALUES (695, '141001', '市辖区', 57, 3, 0, '1', '2', '1-5-57-695', '山西省-临汾市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (696, '141002', '尧都区', 57, 3, 0, 'Yaodu Qu', '2', '1-5-57-696', '山西省-临汾市-尧都区', 1);
INSERT INTO `qmgx_region` VALUES (697, '141021', '曲沃县', 57, 3, 0, 'Quwo Xian ', '2', '1-5-57-697', '山西省-临汾市-曲沃县', 1);
INSERT INTO `qmgx_region` VALUES (698, '141022', '翼城县', 57, 3, 0, 'Yicheng Xian', '2', '1-5-57-698', '山西省-临汾市-翼城县', 1);
INSERT INTO `qmgx_region` VALUES (699, '141023', '襄汾县', 57, 3, 0, 'Xiangfen Xian', '2', '1-5-57-699', '山西省-临汾市-襄汾县', 1);
INSERT INTO `qmgx_region` VALUES (700, '141024', '洪洞县', 57, 3, 0, 'Hongtong Xian', '2', '1-5-57-700', '山西省-临汾市-洪洞县', 1);
INSERT INTO `qmgx_region` VALUES (701, '141025', '古县', 57, 3, 0, 'Gu Xian', '2', '1-5-57-701', '山西省-临汾市-古县', 1);
INSERT INTO `qmgx_region` VALUES (702, '141026', '安泽县', 57, 3, 0, 'Anze Xian', '2', '1-5-57-702', '山西省-临汾市-安泽县', 1);
INSERT INTO `qmgx_region` VALUES (703, '141027', '浮山县', 57, 3, 0, 'Fushan Xian ', '2', '1-5-57-703', '山西省-临汾市-浮山县', 1);
INSERT INTO `qmgx_region` VALUES (704, '141028', '吉县', 57, 3, 0, 'Ji Xian', '2', '1-5-57-704', '山西省-临汾市-吉县', 1);
INSERT INTO `qmgx_region` VALUES (705, '141029', '乡宁县', 57, 3, 0, 'Xiangning Xian', '2', '1-5-57-705', '山西省-临汾市-乡宁县', 1);
INSERT INTO `qmgx_region` VALUES (706, '141030', '大宁县', 57, 3, 0, 'Daning Xian', '2', '1-5-57-706', '山西省-临汾市-大宁县', 1);
INSERT INTO `qmgx_region` VALUES (707, '141031', '隰县', 57, 3, 0, 'Xi Xian', '2', '1-5-57-707', '山西省-临汾市-隰县', 1);
INSERT INTO `qmgx_region` VALUES (708, '141032', '永和县', 57, 3, 0, 'Yonghe Xian', '2', '1-5-57-708', '山西省-临汾市-永和县', 1);
INSERT INTO `qmgx_region` VALUES (709, '141033', '蒲县', 57, 3, 0, 'Pu Xian', '2', '1-5-57-709', '山西省-临汾市-蒲县', 1);
INSERT INTO `qmgx_region` VALUES (710, '141034', '汾西县', 57, 3, 0, 'Fenxi Xian', '2', '1-5-57-710', '山西省-临汾市-汾西县', 1);
INSERT INTO `qmgx_region` VALUES (711, '141081', '侯马市', 57, 3, 0, 'Houma Shi ', '2', '1-5-57-711', '山西省-临汾市-侯马市', 1);
INSERT INTO `qmgx_region` VALUES (712, '141082', '霍州市', 57, 3, 0, 'Huozhou Shi ', '2', '1-5-57-712', '山西省-临汾市-霍州市', 1);
INSERT INTO `qmgx_region` VALUES (713, '141101', '市辖区', 58, 3, 0, '1', '2', '1-5-58-713', '山西省-吕梁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (714, '141102', '离石区', 58, 3, 0, 'Lishi Qu', '2', '1-5-58-714', '山西省-吕梁市-离石区', 1);
INSERT INTO `qmgx_region` VALUES (715, '141121', '文水县', 58, 3, 0, 'Wenshui Xian', '2', '1-5-58-715', '山西省-吕梁市-文水县', 1);
INSERT INTO `qmgx_region` VALUES (716, '141122', '交城县', 58, 3, 0, 'Jiaocheng Xian', '2', '1-5-58-716', '山西省-吕梁市-交城县', 1);
INSERT INTO `qmgx_region` VALUES (717, '141123', '兴县', 58, 3, 0, 'Xing Xian', '2', '1-5-58-717', '山西省-吕梁市-兴县', 1);
INSERT INTO `qmgx_region` VALUES (718, '141124', '临县', 58, 3, 0, 'Lin Xian ', '2', '1-5-58-718', '山西省-吕梁市-临县', 1);
INSERT INTO `qmgx_region` VALUES (719, '141125', '柳林县', 58, 3, 0, 'Liulin Xian', '2', '1-5-58-719', '山西省-吕梁市-柳林县', 1);
INSERT INTO `qmgx_region` VALUES (720, '141126', '石楼县', 58, 3, 0, 'Shilou Xian', '2', '1-5-58-720', '山西省-吕梁市-石楼县', 1);
INSERT INTO `qmgx_region` VALUES (721, '141127', '岚县', 58, 3, 0, 'Lan Xian', '2', '1-5-58-721', '山西省-吕梁市-岚县', 1);
INSERT INTO `qmgx_region` VALUES (722, '141128', '方山县', 58, 3, 0, 'Fangshan Xian', '2', '1-5-58-722', '山西省-吕梁市-方山县', 1);
INSERT INTO `qmgx_region` VALUES (723, '141129', '中阳县', 58, 3, 0, 'Zhongyang Xian', '2', '1-5-58-723', '山西省-吕梁市-中阳县', 1);
INSERT INTO `qmgx_region` VALUES (724, '141130', '交口县', 58, 3, 0, 'Jiaokou Xian', '2', '1-5-58-724', '山西省-吕梁市-交口县', 1);
INSERT INTO `qmgx_region` VALUES (725, '141181', '孝义市', 58, 3, 0, 'Xiaoyi Shi', '2', '1-5-58-725', '山西省-吕梁市-孝义市', 1);
INSERT INTO `qmgx_region` VALUES (726, '141182', '汾阳市', 58, 3, 0, 'Fenyang Shi', '2', '1-5-58-726', '山西省-吕梁市-汾阳市', 1);
INSERT INTO `qmgx_region` VALUES (727, '150101', '市辖区', 59, 3, 0, 'Shixiaqu', '2', '1-6-59-727', '内蒙古自治区-呼和浩特市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (728, '150102', '新城区', 59, 3, 0, 'Xincheng Qu', 'XCN', '1-6-59-728', '内蒙古自治区-呼和浩特市-新城区', 1);
INSERT INTO `qmgx_region` VALUES (729, '150103', '回民区', 59, 3, 0, 'Huimin Qu', 'HMQ', '1-6-59-729', '内蒙古自治区-呼和浩特市-回民区', 1);
INSERT INTO `qmgx_region` VALUES (730, '150104', '玉泉区', 59, 3, 0, 'Yuquan Qu', 'YQN', '1-6-59-730', '内蒙古自治区-呼和浩特市-玉泉区', 1);
INSERT INTO `qmgx_region` VALUES (731, '150105', '赛罕区', 59, 3, 0, 'Saihan Qu', '2', '1-6-59-731', '内蒙古自治区-呼和浩特市-赛罕区', 1);
INSERT INTO `qmgx_region` VALUES (732, '150121', '土默特左旗', 59, 3, 0, 'Tumd Zuoqi', 'TUZ', '1-6-59-732', '内蒙古自治区-呼和浩特市-土默特左旗', 1);
INSERT INTO `qmgx_region` VALUES (733, '150122', '托克托县', 59, 3, 0, 'Togtoh Xian', 'TOG', '1-6-59-733', '内蒙古自治区-呼和浩特市-托克托县', 1);
INSERT INTO `qmgx_region` VALUES (734, '150123', '和林格尔县', 59, 3, 0, 'Horinger Xian', 'HOR', '1-6-59-734', '内蒙古自治区-呼和浩特市-和林格尔县', 1);
INSERT INTO `qmgx_region` VALUES (735, '150124', '清水河县', 59, 3, 0, 'Qingshuihe Xian', 'QSH', '1-6-59-735', '内蒙古自治区-呼和浩特市-清水河县', 1);
INSERT INTO `qmgx_region` VALUES (736, '150125', '武川县', 59, 3, 0, 'Wuchuan Xian', 'WCX', '1-6-59-736', '内蒙古自治区-呼和浩特市-武川县', 1);
INSERT INTO `qmgx_region` VALUES (737, '150201', '市辖区', 60, 3, 0, 'Shixiaqu', '2', '1-6-60-737', '内蒙古自治区-包头市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (738, '150202', '东河区', 60, 3, 0, 'Donghe Qu', 'DHE', '1-6-60-738', '内蒙古自治区-包头市-东河区', 1);
INSERT INTO `qmgx_region` VALUES (739, '150203', '昆都仑区', 60, 3, 0, 'Kundulun Qu', '2', '1-6-60-739', '内蒙古自治区-包头市-昆都仑区', 1);
INSERT INTO `qmgx_region` VALUES (740, '150204', '青山区', 60, 3, 0, 'Qingshan Qu', 'QSB', '1-6-60-740', '内蒙古自治区-包头市-青山区', 1);
INSERT INTO `qmgx_region` VALUES (741, '150205', '石拐区', 60, 3, 0, 'Shiguai Qu', '2', '1-6-60-741', '内蒙古自治区-包头市-石拐区', 1);
INSERT INTO `qmgx_region` VALUES (742, '150206', '白云鄂博矿区', 60, 3, 0, 'Baiyun Kuangqu', '2', '1-6-60-742', '内蒙古自治区-包头市-白云鄂博矿区', 1);
INSERT INTO `qmgx_region` VALUES (743, '150207', '九原区', 60, 3, 0, 'Jiuyuan Qu', '2', '1-6-60-743', '内蒙古自治区-包头市-九原区', 1);
INSERT INTO `qmgx_region` VALUES (744, '150221', '土默特右旗', 60, 3, 0, 'Tumd Youqi', 'TUY', '1-6-60-744', '内蒙古自治区-包头市-土默特右旗', 1);
INSERT INTO `qmgx_region` VALUES (745, '150222', '固阳县', 60, 3, 0, 'Guyang Xian', 'GYM', '1-6-60-745', '内蒙古自治区-包头市-固阳县', 1);
INSERT INTO `qmgx_region` VALUES (746, '150223', '达尔罕茂明安联合旗', 60, 3, 0, 'Darhan Muminggan Lianheqi', 'DML', '1-6-60-746', '内蒙古自治区-包头市-达尔罕茂明安联合旗', 1);
INSERT INTO `qmgx_region` VALUES (747, '150301', '市辖区', 61, 3, 0, 'Shixiaqu', '2', '1-6-61-747', '内蒙古自治区-乌海市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (748, '150302', '海勃湾区', 61, 3, 0, 'Haibowan Qu', 'HBW', '1-6-61-748', '内蒙古自治区-乌海市-海勃湾区', 1);
INSERT INTO `qmgx_region` VALUES (749, '150303', '海南区', 61, 3, 0, 'Hainan Qu', 'HNU', '1-6-61-749', '内蒙古自治区-乌海市-海南区', 1);
INSERT INTO `qmgx_region` VALUES (750, '150304', '乌达区', 61, 3, 0, 'Ud Qu', 'UDQ', '1-6-61-750', '内蒙古自治区-乌海市-乌达区', 1);
INSERT INTO `qmgx_region` VALUES (751, '150401', '市辖区', 62, 3, 0, 'Shixiaqu', '2', '1-6-62-751', '内蒙古自治区-赤峰市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (752, '150402', '红山区', 62, 3, 0, 'Hongshan Qu', 'HSZ', '1-6-62-752', '内蒙古自治区-赤峰市-红山区', 1);
INSERT INTO `qmgx_region` VALUES (753, '150403', '元宝山区', 62, 3, 0, 'Yuanbaoshan Qu', 'YBO', '1-6-62-753', '内蒙古自治区-赤峰市-元宝山区', 1);
INSERT INTO `qmgx_region` VALUES (754, '150404', '松山区', 62, 3, 0, 'Songshan Qu', 'SSQ', '1-6-62-754', '内蒙古自治区-赤峰市-松山区', 1);
INSERT INTO `qmgx_region` VALUES (755, '150421', '阿鲁科尔沁旗', 62, 3, 0, 'Ar Horqin Qi', 'AHO', '1-6-62-755', '内蒙古自治区-赤峰市-阿鲁科尔沁旗', 1);
INSERT INTO `qmgx_region` VALUES (756, '150422', '巴林左旗', 62, 3, 0, 'Bairin Zuoqi', 'BAZ', '1-6-62-756', '内蒙古自治区-赤峰市-巴林左旗', 1);
INSERT INTO `qmgx_region` VALUES (757, '150423', '巴林右旗', 62, 3, 0, 'Bairin Youqi', 'BAY', '1-6-62-757', '内蒙古自治区-赤峰市-巴林右旗', 1);
INSERT INTO `qmgx_region` VALUES (758, '150424', '林西县', 62, 3, 0, 'Linxi Xian', 'LXM', '1-6-62-758', '内蒙古自治区-赤峰市-林西县', 1);
INSERT INTO `qmgx_region` VALUES (759, '150425', '克什克腾旗', 62, 3, 0, 'Hexigten Qi', 'HXT', '1-6-62-759', '内蒙古自治区-赤峰市-克什克腾旗', 1);
INSERT INTO `qmgx_region` VALUES (760, '150426', '翁牛特旗', 62, 3, 0, 'Ongniud Qi', 'ONG', '1-6-62-760', '内蒙古自治区-赤峰市-翁牛特旗', 1);
INSERT INTO `qmgx_region` VALUES (761, '150428', '喀喇沁旗', 62, 3, 0, 'Harqin Qi', 'HAR', '1-6-62-761', '内蒙古自治区-赤峰市-喀喇沁旗', 1);
INSERT INTO `qmgx_region` VALUES (762, '150429', '宁城县', 62, 3, 0, 'Ningcheng Xian', 'NCH', '1-6-62-762', '内蒙古自治区-赤峰市-宁城县', 1);
INSERT INTO `qmgx_region` VALUES (763, '150430', '敖汉旗', 62, 3, 0, 'Aohan Qi', 'AHN', '1-6-62-763', '内蒙古自治区-赤峰市-敖汉旗', 1);
INSERT INTO `qmgx_region` VALUES (764, '150501', '市辖区', 63, 3, 0, '1', '2', '1-6-63-764', '内蒙古自治区-通辽市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (765, '150502', '科尔沁区', 63, 3, 0, 'Keermi Qu', '2', '1-6-63-765', '内蒙古自治区-通辽市-科尔沁区', 1);
INSERT INTO `qmgx_region` VALUES (766, '150521', '科尔沁左翼中旗', 63, 3, 0, 'Horqin Zuoyi Zhongqi', '2', '1-6-63-766', '内蒙古自治区-通辽市-科尔沁左翼中旗', 1);
INSERT INTO `qmgx_region` VALUES (767, '150522', '科尔沁左翼后旗', 63, 3, 0, 'Horqin Zuoyi Houqi', '2', '1-6-63-767', '内蒙古自治区-通辽市-科尔沁左翼后旗', 1);
INSERT INTO `qmgx_region` VALUES (768, '150523', '开鲁县', 63, 3, 0, 'Kailu Xian', '2', '1-6-63-768', '内蒙古自治区-通辽市-开鲁县', 1);
INSERT INTO `qmgx_region` VALUES (769, '150524', '库伦旗', 63, 3, 0, 'Hure Qi', '2', '1-6-63-769', '内蒙古自治区-通辽市-库伦旗', 1);
INSERT INTO `qmgx_region` VALUES (770, '150525', '奈曼旗', 63, 3, 0, 'Naiman Qi', '2', '1-6-63-770', '内蒙古自治区-通辽市-奈曼旗', 1);
INSERT INTO `qmgx_region` VALUES (771, '150526', '扎鲁特旗', 63, 3, 0, 'Jarud Qi', '2', '1-6-63-771', '内蒙古自治区-通辽市-扎鲁特旗', 1);
INSERT INTO `qmgx_region` VALUES (772, '150581', '霍林郭勒市', 63, 3, 0, 'Holingol Shi', '2', '1-6-63-772', '内蒙古自治区-通辽市-霍林郭勒市', 1);
INSERT INTO `qmgx_region` VALUES (773, '150602', '东胜区', 64, 3, 0, 'Dongsheng Qu', '2', '1-6-64-773', '内蒙古自治区-鄂尔多斯市-东胜区', 1);
INSERT INTO `qmgx_region` VALUES (774, '150621', '达拉特旗', 64, 3, 0, 'Dalad Qi', '2', '1-6-64-774', '内蒙古自治区-鄂尔多斯市-达拉特旗', 1);
INSERT INTO `qmgx_region` VALUES (775, '150622', '准格尔旗', 64, 3, 0, 'Jungar Qi', '2', '1-6-64-775', '内蒙古自治区-鄂尔多斯市-准格尔旗', 1);
INSERT INTO `qmgx_region` VALUES (776, '150623', '鄂托克前旗', 64, 3, 0, 'Otog Qianqi', '2', '1-6-64-776', '内蒙古自治区-鄂尔多斯市-鄂托克前旗', 1);
INSERT INTO `qmgx_region` VALUES (777, '150624', '鄂托克旗', 64, 3, 0, 'Otog Qi', '2', '1-6-64-777', '内蒙古自治区-鄂尔多斯市-鄂托克旗', 1);
INSERT INTO `qmgx_region` VALUES (778, '150625', '杭锦旗', 64, 3, 0, 'Hanggin Qi', '2', '1-6-64-778', '内蒙古自治区-鄂尔多斯市-杭锦旗', 1);
INSERT INTO `qmgx_region` VALUES (779, '150626', '乌审旗', 64, 3, 0, 'Uxin Qi', '2', '1-6-64-779', '内蒙古自治区-鄂尔多斯市-乌审旗', 1);
INSERT INTO `qmgx_region` VALUES (780, '150627', '伊金霍洛旗', 64, 3, 0, 'Ejin Horo Qi', '2', '1-6-64-780', '内蒙古自治区-鄂尔多斯市-伊金霍洛旗', 1);
INSERT INTO `qmgx_region` VALUES (781, '150701', '市辖区', 65, 3, 0, '1', '2', '1-6-65-781', '内蒙古自治区-呼伦贝尔市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (782, '150702', '海拉尔区', 65, 3, 0, 'Hailaer Qu', '2', '1-6-65-782', '内蒙古自治区-呼伦贝尔市-海拉尔区', 1);
INSERT INTO `qmgx_region` VALUES (783, '150721', '阿荣旗', 65, 3, 0, 'Arun Qi', '2', '1-6-65-783', '内蒙古自治区-呼伦贝尔市-阿荣旗', 1);
INSERT INTO `qmgx_region` VALUES (784, '150722', '莫力达瓦达斡尔族自治旗', 65, 3, 0, 'Morin Dawa Daurzu Zizhiqi', '2', '1-6-65-784', '内蒙古自治区-呼伦贝尔市-莫力达瓦达斡尔族自治旗', 1);
INSERT INTO `qmgx_region` VALUES (785, '150723', '鄂伦春自治旗', 65, 3, 0, 'Oroqen Zizhiqi', '2', '1-6-65-785', '内蒙古自治区-呼伦贝尔市-鄂伦春自治旗', 1);
INSERT INTO `qmgx_region` VALUES (786, '150724', '鄂温克族自治旗', 65, 3, 0, 'Ewenkizu Zizhiqi', '2', '1-6-65-786', '内蒙古自治区-呼伦贝尔市-鄂温克族自治旗', 1);
INSERT INTO `qmgx_region` VALUES (787, '150725', '陈巴尔虎旗', 65, 3, 0, 'Chen Barag Qi', '2', '1-6-65-787', '内蒙古自治区-呼伦贝尔市-陈巴尔虎旗', 1);
INSERT INTO `qmgx_region` VALUES (788, '150726', '新巴尔虎左旗', 65, 3, 0, 'Xin Barag Zuoqi', '2', '1-6-65-788', '内蒙古自治区-呼伦贝尔市-新巴尔虎左旗', 1);
INSERT INTO `qmgx_region` VALUES (789, '150727', '新巴尔虎右旗', 65, 3, 0, 'Xin Barag Youqi', '2', '1-6-65-789', '内蒙古自治区-呼伦贝尔市-新巴尔虎右旗', 1);
INSERT INTO `qmgx_region` VALUES (790, '150781', '满洲里市', 65, 3, 0, 'Manzhouli Shi', '2', '1-6-65-790', '内蒙古自治区-呼伦贝尔市-满洲里市', 1);
INSERT INTO `qmgx_region` VALUES (791, '150782', '牙克石市', 65, 3, 0, 'Yakeshi Shi', '2', '1-6-65-791', '内蒙古自治区-呼伦贝尔市-牙克石市', 1);
INSERT INTO `qmgx_region` VALUES (792, '150783', '扎兰屯市', 65, 3, 0, 'Zalantun Shi', '2', '1-6-65-792', '内蒙古自治区-呼伦贝尔市-扎兰屯市', 1);
INSERT INTO `qmgx_region` VALUES (793, '150784', '额尔古纳市', 65, 3, 0, 'Ergun Shi', '2', '1-6-65-793', '内蒙古自治区-呼伦贝尔市-额尔古纳市', 1);
INSERT INTO `qmgx_region` VALUES (794, '150785', '根河市', 65, 3, 0, 'Genhe Shi', '2', '1-6-65-794', '内蒙古自治区-呼伦贝尔市-根河市', 1);
INSERT INTO `qmgx_region` VALUES (795, '150801', '市辖区', 66, 3, 0, '1', '2', '1-6-66-795', '内蒙古自治区-巴彦淖尔市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (796, '150802', '临河区', 66, 3, 0, 'Linhe Qu', '2', '1-6-66-796', '内蒙古自治区-巴彦淖尔市-临河区', 1);
INSERT INTO `qmgx_region` VALUES (797, '150821', '五原县', 66, 3, 0, 'Wuyuan Xian', '2', '1-6-66-797', '内蒙古自治区-巴彦淖尔市-五原县', 1);
INSERT INTO `qmgx_region` VALUES (798, '150822', '磴口县', 66, 3, 0, 'Dengkou Xian', '2', '1-6-66-798', '内蒙古自治区-巴彦淖尔市-磴口县', 1);
INSERT INTO `qmgx_region` VALUES (799, '150823', '乌拉特前旗', 66, 3, 0, 'Urad Qianqi', '2', '1-6-66-799', '内蒙古自治区-巴彦淖尔市-乌拉特前旗', 1);
INSERT INTO `qmgx_region` VALUES (800, '150824', '乌拉特中旗', 66, 3, 0, 'Urad Zhongqi', '2', '1-6-66-800', '内蒙古自治区-巴彦淖尔市-乌拉特中旗', 1);
INSERT INTO `qmgx_region` VALUES (801, '150825', '乌拉特后旗', 66, 3, 0, 'Urad Houqi', '2', '1-6-66-801', '内蒙古自治区-巴彦淖尔市-乌拉特后旗', 1);
INSERT INTO `qmgx_region` VALUES (802, '150826', '杭锦后旗', 66, 3, 0, 'Hanggin Houqi', '2', '1-6-66-802', '内蒙古自治区-巴彦淖尔市-杭锦后旗', 1);
INSERT INTO `qmgx_region` VALUES (803, '150901', '市辖区', 67, 3, 0, '1', '2', '1-6-67-803', '内蒙古自治区-乌兰察布市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (804, '150902', '集宁区', 67, 3, 0, 'Jining Qu', '2', '1-6-67-804', '内蒙古自治区-乌兰察布市-集宁区', 1);
INSERT INTO `qmgx_region` VALUES (805, '150921', '卓资县', 67, 3, 0, 'Zhuozi Xian', '2', '1-6-67-805', '内蒙古自治区-乌兰察布市-卓资县', 1);
INSERT INTO `qmgx_region` VALUES (806, '150922', '化德县', 67, 3, 0, 'Huade Xian', '2', '1-6-67-806', '内蒙古自治区-乌兰察布市-化德县', 1);
INSERT INTO `qmgx_region` VALUES (807, '150923', '商都县', 67, 3, 0, 'Shangdu Xian', '2', '1-6-67-807', '内蒙古自治区-乌兰察布市-商都县', 1);
INSERT INTO `qmgx_region` VALUES (808, '150924', '兴和县', 67, 3, 0, 'Xinghe Xian', '2', '1-6-67-808', '内蒙古自治区-乌兰察布市-兴和县', 1);
INSERT INTO `qmgx_region` VALUES (809, '150925', '凉城县', 67, 3, 0, 'Liangcheng Xian', '2', '1-6-67-809', '内蒙古自治区-乌兰察布市-凉城县', 1);
INSERT INTO `qmgx_region` VALUES (810, '150926', '察哈尔右翼前旗', 67, 3, 0, 'Qahar Youyi Qianqi', '2', '1-6-67-810', '内蒙古自治区-乌兰察布市-察哈尔右翼前旗', 1);
INSERT INTO `qmgx_region` VALUES (811, '150927', '察哈尔右翼中旗', 67, 3, 0, 'Qahar Youyi Zhongqi', '2', '1-6-67-811', '内蒙古自治区-乌兰察布市-察哈尔右翼中旗', 1);
INSERT INTO `qmgx_region` VALUES (812, '150928', '察哈尔右翼后旗', 67, 3, 0, 'Qahar Youyi Houqi', '2', '1-6-67-812', '内蒙古自治区-乌兰察布市-察哈尔右翼后旗', 1);
INSERT INTO `qmgx_region` VALUES (813, '150929', '四子王旗', 67, 3, 0, 'Dorbod Qi', '2', '1-6-67-813', '内蒙古自治区-乌兰察布市-四子王旗', 1);
INSERT INTO `qmgx_region` VALUES (814, '150981', '丰镇市', 67, 3, 0, 'Fengzhen Shi', '2', '1-6-67-814', '内蒙古自治区-乌兰察布市-丰镇市', 1);
INSERT INTO `qmgx_region` VALUES (815, '152201', '乌兰浩特市', 68, 3, 0, 'Ulan Hot Shi', 'ULO', '1-6-68-815', '内蒙古自治区-兴安盟-乌兰浩特市', 1);
INSERT INTO `qmgx_region` VALUES (816, '152202', '阿尔山市', 68, 3, 0, 'Arxan Shi', 'ARS', '1-6-68-816', '内蒙古自治区-兴安盟-阿尔山市', 1);
INSERT INTO `qmgx_region` VALUES (817, '152221', '科尔沁右翼前旗', 68, 3, 0, 'Horqin Youyi Qianqi', 'HYQ', '1-6-68-817', '内蒙古自治区-兴安盟-科尔沁右翼前旗', 1);
INSERT INTO `qmgx_region` VALUES (818, '152222', '科尔沁右翼中旗', 68, 3, 0, 'Horqin Youyi Zhongqi', 'HYZ', '1-6-68-818', '内蒙古自治区-兴安盟-科尔沁右翼中旗', 1);
INSERT INTO `qmgx_region` VALUES (819, '152223', '扎赉特旗', 68, 3, 0, 'Jalaid Qi', 'JAL', '1-6-68-819', '内蒙古自治区-兴安盟-扎赉特旗', 1);
INSERT INTO `qmgx_region` VALUES (820, '152224', '突泉县', 68, 3, 0, 'Tuquan Xian', 'TUQ', '1-6-68-820', '内蒙古自治区-兴安盟-突泉县', 1);
INSERT INTO `qmgx_region` VALUES (821, '152501', '二连浩特市', 69, 3, 0, 'Erenhot Shi', 'ERC', '1-6-69-821', '内蒙古自治区-锡林郭勒盟-二连浩特市', 1);
INSERT INTO `qmgx_region` VALUES (822, '152502', '锡林浩特市', 69, 3, 0, 'Xilinhot Shi', 'XLI', '1-6-69-822', '内蒙古自治区-锡林郭勒盟-锡林浩特市', 1);
INSERT INTO `qmgx_region` VALUES (823, '152522', '阿巴嘎旗', 69, 3, 0, 'Abag Qi', 'ABG', '1-6-69-823', '内蒙古自治区-锡林郭勒盟-阿巴嘎旗', 1);
INSERT INTO `qmgx_region` VALUES (824, '152523', '苏尼特左旗', 69, 3, 0, 'Sonid Zuoqi', 'SOZ', '1-6-69-824', '内蒙古自治区-锡林郭勒盟-苏尼特左旗', 1);
INSERT INTO `qmgx_region` VALUES (825, '152524', '苏尼特右旗', 69, 3, 0, 'Sonid Youqi', 'SOY', '1-6-69-825', '内蒙古自治区-锡林郭勒盟-苏尼特右旗', 1);
INSERT INTO `qmgx_region` VALUES (826, '152525', '东乌珠穆沁旗', 69, 3, 0, 'Dong Ujimqin Qi', 'DUJ', '1-6-69-826', '内蒙古自治区-锡林郭勒盟-东乌珠穆沁旗', 1);
INSERT INTO `qmgx_region` VALUES (827, '152526', '西乌珠穆沁旗', 69, 3, 0, 'Xi Ujimqin Qi', 'XUJ', '1-6-69-827', '内蒙古自治区-锡林郭勒盟-西乌珠穆沁旗', 1);
INSERT INTO `qmgx_region` VALUES (828, '152527', '太仆寺旗', 69, 3, 0, 'Taibus Qi', 'TAB', '1-6-69-828', '内蒙古自治区-锡林郭勒盟-太仆寺旗', 1);
INSERT INTO `qmgx_region` VALUES (829, '152528', '镶黄旗', 69, 3, 0, 'Xianghuang(Hobot Xar) Qi', 'XHG', '1-6-69-829', '内蒙古自治区-锡林郭勒盟-镶黄旗', 1);
INSERT INTO `qmgx_region` VALUES (830, '152529', '正镶白旗', 69, 3, 0, 'Zhengxiangbai(Xulun Hobot Qagan)Qi', 'ZXB', '1-6-69-830', '内蒙古自治区-锡林郭勒盟-正镶白旗', 1);
INSERT INTO `qmgx_region` VALUES (831, '152530', '正蓝旗', 69, 3, 0, 'Zhenglan(Xulun Hoh)Qi', 'ZLM', '1-6-69-831', '内蒙古自治区-锡林郭勒盟-正蓝旗', 1);
INSERT INTO `qmgx_region` VALUES (832, '152531', '多伦县', 69, 3, 0, 'Duolun (Dolonnur)Xian', 'DLM', '1-6-69-832', '内蒙古自治区-锡林郭勒盟-多伦县', 1);
INSERT INTO `qmgx_region` VALUES (833, '152921', '阿拉善左旗', 70, 3, 0, 'Alxa Zuoqi', 'ALZ', '1-6-70-833', '内蒙古自治区-阿拉善盟-阿拉善左旗', 1);
INSERT INTO `qmgx_region` VALUES (834, '152922', '阿拉善右旗', 70, 3, 0, 'Alxa Youqi', 'ALY', '1-6-70-834', '内蒙古自治区-阿拉善盟-阿拉善右旗', 1);
INSERT INTO `qmgx_region` VALUES (835, '152923', '额济纳旗', 70, 3, 0, 'Ejin Qi', 'EJI', '1-6-70-835', '内蒙古自治区-阿拉善盟-额济纳旗', 1);
INSERT INTO `qmgx_region` VALUES (836, '210101', '市辖区', 71, 3, 0, 'Shixiaqu', '2', '1-7-71-836', '辽宁省-沈阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (837, '210102', '和平区', 71, 3, 0, 'Heping Qu', 'HEP', '1-7-71-837', '辽宁省-沈阳市-和平区', 1);
INSERT INTO `qmgx_region` VALUES (838, '210103', '沈河区', 71, 3, 0, 'Shenhe Qu ', 'SHQ', '1-7-71-838', '辽宁省-沈阳市-沈河区', 1);
INSERT INTO `qmgx_region` VALUES (839, '210104', '大东区', 71, 3, 0, 'Dadong Qu ', 'DDQ', '1-7-71-839', '辽宁省-沈阳市-大东区', 1);
INSERT INTO `qmgx_region` VALUES (840, '210105', '皇姑区', 71, 3, 0, 'Huanggu Qu', 'HGU', '1-7-71-840', '辽宁省-沈阳市-皇姑区', 1);
INSERT INTO `qmgx_region` VALUES (841, '210106', '铁西区', 71, 3, 0, 'Tiexi Qu', 'TXI', '1-7-71-841', '辽宁省-沈阳市-铁西区', 1);
INSERT INTO `qmgx_region` VALUES (842, '210111', '苏家屯区', 71, 3, 0, 'Sujiatun Qu', 'SJT', '1-7-71-842', '辽宁省-沈阳市-苏家屯区', 1);
INSERT INTO `qmgx_region` VALUES (843, '210112', '东陵区', 71, 3, 0, 'Dongling Qu ', 'DLQ', '1-7-71-843', '辽宁省-沈阳市-东陵区', 1);
INSERT INTO `qmgx_region` VALUES (844, '210113', '沈北新区', 71, 3, 0, 'Xinchengzi Qu', '2', '1-7-71-844', '辽宁省-沈阳市-沈北新区', 1);
INSERT INTO `qmgx_region` VALUES (845, '210114', '于洪区', 71, 3, 0, 'Yuhong Qu ', 'YHQ', '1-7-71-845', '辽宁省-沈阳市-于洪区', 1);
INSERT INTO `qmgx_region` VALUES (846, '210122', '辽中县', 71, 3, 0, 'Liaozhong Xian', 'LZL', '1-7-71-846', '辽宁省-沈阳市-辽中县', 1);
INSERT INTO `qmgx_region` VALUES (847, '210123', '康平县', 71, 3, 0, 'Kangping Xian', 'KPG', '1-7-71-847', '辽宁省-沈阳市-康平县', 1);
INSERT INTO `qmgx_region` VALUES (848, '210124', '法库县', 71, 3, 0, 'Faku Xian', 'FKU', '1-7-71-848', '辽宁省-沈阳市-法库县', 1);
INSERT INTO `qmgx_region` VALUES (849, '210181', '新民市', 71, 3, 0, 'Xinmin Shi', 'XMS', '1-7-71-849', '辽宁省-沈阳市-新民市', 1);
INSERT INTO `qmgx_region` VALUES (850, '210201', '市辖区', 72, 3, 0, 'Shixiaqu', '2', '1-7-72-850', '辽宁省-大连市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (851, '210202', '中山区', 72, 3, 0, 'Zhongshan Qu', 'ZSD', '1-7-72-851', '辽宁省-大连市-中山区', 1);
INSERT INTO `qmgx_region` VALUES (852, '210203', '西岗区', 72, 3, 0, 'Xigang Qu', 'XGD', '1-7-72-852', '辽宁省-大连市-西岗区', 1);
INSERT INTO `qmgx_region` VALUES (853, '210204', '沙河口区', 72, 3, 0, 'Shahekou Qu', 'SHK', '1-7-72-853', '辽宁省-大连市-沙河口区', 1);
INSERT INTO `qmgx_region` VALUES (854, '210211', '甘井子区', 72, 3, 0, 'Ganjingzi Qu', 'GJZ', '1-7-72-854', '辽宁省-大连市-甘井子区', 1);
INSERT INTO `qmgx_region` VALUES (855, '210212', '旅顺口区', 72, 3, 0, 'Lvshunkou Qu ', 'LSK', '1-7-72-855', '辽宁省-大连市-旅顺口区', 1);
INSERT INTO `qmgx_region` VALUES (856, '210213', '金州区', 72, 3, 0, 'Jinzhou Qu', 'JZH', '1-7-72-856', '辽宁省-大连市-金州区', 1);
INSERT INTO `qmgx_region` VALUES (857, '210224', '长海县', 72, 3, 0, 'Changhai Xian', 'CHX', '1-7-72-857', '辽宁省-大连市-长海县', 1);
INSERT INTO `qmgx_region` VALUES (858, '210281', '瓦房店市', 72, 3, 0, 'Wafangdian Shi', 'WFD', '1-7-72-858', '辽宁省-大连市-瓦房店市', 1);
INSERT INTO `qmgx_region` VALUES (859, '210282', '普兰店市', 72, 3, 0, 'Pulandian Shi', 'PLD', '1-7-72-859', '辽宁省-大连市-普兰店市', 1);
INSERT INTO `qmgx_region` VALUES (860, '210283', '庄河市', 72, 3, 0, 'Zhuanghe Shi', 'ZHH', '1-7-72-860', '辽宁省-大连市-庄河市', 1);
INSERT INTO `qmgx_region` VALUES (861, '210301', '市辖区', 73, 3, 0, 'Shixiaqu', '2', '1-7-73-861', '辽宁省-鞍山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (862, '210302', '铁东区', 73, 3, 0, 'Tiedong Qu ', 'TED', '1-7-73-862', '辽宁省-鞍山市-铁东区', 1);
INSERT INTO `qmgx_region` VALUES (863, '210303', '铁西区', 73, 3, 0, 'Tiexi Qu', 'TXL', '1-7-73-863', '辽宁省-鞍山市-铁西区', 1);
INSERT INTO `qmgx_region` VALUES (864, '210304', '立山区', 73, 3, 0, 'Lishan Qu', 'LAS', '1-7-73-864', '辽宁省-鞍山市-立山区', 1);
INSERT INTO `qmgx_region` VALUES (865, '210311', '千山区', 73, 3, 0, 'Qianshan Qu ', 'QSQ', '1-7-73-865', '辽宁省-鞍山市-千山区', 1);
INSERT INTO `qmgx_region` VALUES (866, '210321', '台安县', 73, 3, 0, 'Tai,an Xian', 'TAX', '1-7-73-866', '辽宁省-鞍山市-台安县', 1);
INSERT INTO `qmgx_region` VALUES (867, '210323', '岫岩满族自治县', 73, 3, 0, 'Xiuyan Manzu Zizhixian', 'XYL', '1-7-73-867', '辽宁省-鞍山市-岫岩满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (868, '210381', '海城市', 73, 3, 0, 'Haicheng Shi', 'HCL', '1-7-73-868', '辽宁省-鞍山市-海城市', 1);
INSERT INTO `qmgx_region` VALUES (869, '210401', '市辖区', 74, 3, 0, 'Shixiaqu', '2', '1-7-74-869', '辽宁省-抚顺市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (870, '210402', '新抚区', 74, 3, 0, 'Xinfu Qu', 'XFU', '1-7-74-870', '辽宁省-抚顺市-新抚区', 1);
INSERT INTO `qmgx_region` VALUES (871, '210403', '东洲区', 74, 3, 0, 'Dongzhou Qu', '2', '1-7-74-871', '辽宁省-抚顺市-东洲区', 1);
INSERT INTO `qmgx_region` VALUES (872, '210404', '望花区', 74, 3, 0, 'Wanghua Qu', 'WHF', '1-7-74-872', '辽宁省-抚顺市-望花区', 1);
INSERT INTO `qmgx_region` VALUES (873, '210411', '顺城区', 74, 3, 0, 'Shuncheng Qu', 'SCF', '1-7-74-873', '辽宁省-抚顺市-顺城区', 1);
INSERT INTO `qmgx_region` VALUES (874, '210421', '抚顺县', 74, 3, 0, 'Fushun Xian', 'FSX', '1-7-74-874', '辽宁省-抚顺市-抚顺县', 1);
INSERT INTO `qmgx_region` VALUES (875, '210422', '新宾满族自治县', 74, 3, 0, 'Xinbinmanzuzizhi Xian', '2', '1-7-74-875', '辽宁省-抚顺市-新宾满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (876, '210423', '清原满族自治县', 74, 3, 0, 'Qingyuanmanzuzizhi Xian', '2', '1-7-74-876', '辽宁省-抚顺市-清原满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (877, '210501', '市辖区', 75, 3, 0, 'Shixiaqu', '2', '1-7-75-877', '辽宁省-本溪市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (878, '210502', '平山区', 75, 3, 0, 'Pingshan Qu', 'PSN', '1-7-75-878', '辽宁省-本溪市-平山区', 1);
INSERT INTO `qmgx_region` VALUES (879, '210503', '溪湖区', 75, 3, 0, 'Xihu Qu ', 'XHB', '1-7-75-879', '辽宁省-本溪市-溪湖区', 1);
INSERT INTO `qmgx_region` VALUES (880, '210504', '明山区', 75, 3, 0, 'Mingshan Qu', 'MSB', '1-7-75-880', '辽宁省-本溪市-明山区', 1);
INSERT INTO `qmgx_region` VALUES (881, '210505', '南芬区', 75, 3, 0, 'Nanfen Qu', 'NFQ', '1-7-75-881', '辽宁省-本溪市-南芬区', 1);
INSERT INTO `qmgx_region` VALUES (882, '210521', '本溪满族自治县', 75, 3, 0, 'Benxi Manzu Zizhixian', 'BXX', '1-7-75-882', '辽宁省-本溪市-本溪满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (883, '210522', '桓仁满族自治县', 75, 3, 0, 'Huanren Manzu Zizhixian', 'HRL', '1-7-75-883', '辽宁省-本溪市-桓仁满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (884, '210601', '市辖区', 76, 3, 0, 'Shixiaqu', '2', '1-7-76-884', '辽宁省-丹东市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (885, '210602', '元宝区', 76, 3, 0, 'Yuanbao Qu', 'YBD', '1-7-76-885', '辽宁省-丹东市-元宝区', 1);
INSERT INTO `qmgx_region` VALUES (886, '210603', '振兴区', 76, 3, 0, 'Zhenxing Qu ', 'ZXQ', '1-7-76-886', '辽宁省-丹东市-振兴区', 1);
INSERT INTO `qmgx_region` VALUES (887, '210604', '振安区', 76, 3, 0, 'Zhen,an Qu', 'ZAQ', '1-7-76-887', '辽宁省-丹东市-振安区', 1);
INSERT INTO `qmgx_region` VALUES (888, '210624', '宽甸满族自治县', 76, 3, 0, 'Kuandian Manzu Zizhixian', 'KDN', '1-7-76-888', '辽宁省-丹东市-宽甸满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (889, '210681', '东港市', 76, 3, 0, 'Donggang Shi', 'DGS', '1-7-76-889', '辽宁省-丹东市-东港市', 1);
INSERT INTO `qmgx_region` VALUES (890, '210682', '凤城市', 76, 3, 0, 'Fengcheng Shi', 'FCL', '1-7-76-890', '辽宁省-丹东市-凤城市', 1);
INSERT INTO `qmgx_region` VALUES (891, '210701', '市辖区', 77, 3, 0, 'Shixiaqu', '2', '1-7-77-891', '辽宁省-锦州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (892, '210702', '古塔区', 77, 3, 0, 'Guta Qu', 'GTQ', '1-7-77-892', '辽宁省-锦州市-古塔区', 1);
INSERT INTO `qmgx_region` VALUES (893, '210703', '凌河区', 77, 3, 0, 'Linghe Qu', 'LHF', '1-7-77-893', '辽宁省-锦州市-凌河区', 1);
INSERT INTO `qmgx_region` VALUES (894, '210711', '太和区', 77, 3, 0, 'Taihe Qu', '2', '1-7-77-894', '辽宁省-锦州市-太和区', 1);
INSERT INTO `qmgx_region` VALUES (895, '210726', '黑山县', 77, 3, 0, 'Heishan Xian', 'HSL', '1-7-77-895', '辽宁省-锦州市-黑山县', 1);
INSERT INTO `qmgx_region` VALUES (896, '210727', '义县', 77, 3, 0, 'Yi Xian', 'YXL', '1-7-77-896', '辽宁省-锦州市-义县', 1);
INSERT INTO `qmgx_region` VALUES (897, '210781', '凌海市', 77, 3, 0, 'Linghai Shi ', 'LHL', '1-7-77-897', '辽宁省-锦州市-凌海市', 1);
INSERT INTO `qmgx_region` VALUES (898, '210782', '北镇市', 77, 3, 0, 'Beining Shi', '2', '1-7-77-898', '辽宁省-锦州市-北镇市', 1);
INSERT INTO `qmgx_region` VALUES (899, '210801', '市辖区', 78, 3, 0, 'Shixiaqu', '2', '1-7-78-899', '辽宁省-营口市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (900, '210802', '站前区', 78, 3, 0, 'Zhanqian Qu', 'ZQQ', '1-7-78-900', '辽宁省-营口市-站前区', 1);
INSERT INTO `qmgx_region` VALUES (901, '210803', '西市区', 78, 3, 0, 'Xishi Qu', 'XII', '1-7-78-901', '辽宁省-营口市-西市区', 1);
INSERT INTO `qmgx_region` VALUES (902, '210804', '鲅鱼圈区', 78, 3, 0, 'Bayuquan Qu', 'BYQ', '1-7-78-902', '辽宁省-营口市-鲅鱼圈区', 1);
INSERT INTO `qmgx_region` VALUES (903, '210811', '老边区', 78, 3, 0, 'Laobian Qu', 'LOB', '1-7-78-903', '辽宁省-营口市-老边区', 1);
INSERT INTO `qmgx_region` VALUES (904, '210881', '盖州市', 78, 3, 0, 'Gaizhou Shi', 'GZU', '1-7-78-904', '辽宁省-营口市-盖州市', 1);
INSERT INTO `qmgx_region` VALUES (905, '210882', '大石桥市', 78, 3, 0, 'Dashiqiao Shi', 'DSQ', '1-7-78-905', '辽宁省-营口市-大石桥市', 1);
INSERT INTO `qmgx_region` VALUES (906, '210901', '市辖区', 79, 3, 0, 'Shixiaqu', '2', '1-7-79-906', '辽宁省-阜新市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (907, '210902', '海州区', 79, 3, 0, 'Haizhou Qu', 'HZF', '1-7-79-907', '辽宁省-阜新市-海州区', 1);
INSERT INTO `qmgx_region` VALUES (908, '210903', '新邱区', 79, 3, 0, 'Xinqiu Qu', 'XQF', '1-7-79-908', '辽宁省-阜新市-新邱区', 1);
INSERT INTO `qmgx_region` VALUES (909, '210904', '太平区', 79, 3, 0, 'Taiping Qu', 'TPG', '1-7-79-909', '辽宁省-阜新市-太平区', 1);
INSERT INTO `qmgx_region` VALUES (910, '210905', '清河门区', 79, 3, 0, 'Qinghemen Qu', 'QHM', '1-7-79-910', '辽宁省-阜新市-清河门区', 1);
INSERT INTO `qmgx_region` VALUES (911, '210911', '细河区', 79, 3, 0, 'Xihe Qu', 'XHO', '1-7-79-911', '辽宁省-阜新市-细河区', 1);
INSERT INTO `qmgx_region` VALUES (912, '210921', '阜新蒙古族自治县', 79, 3, 0, 'Fuxin Mongolzu Zizhixian', 'FXX', '1-7-79-912', '辽宁省-阜新市-阜新蒙古族自治县', 1);
INSERT INTO `qmgx_region` VALUES (913, '210922', '彰武县', 79, 3, 0, 'Zhangwu Xian', 'ZWU', '1-7-79-913', '辽宁省-阜新市-彰武县', 1);
INSERT INTO `qmgx_region` VALUES (914, '211001', '市辖区', 80, 3, 0, 'Shixiaqu', '2', '1-7-80-914', '辽宁省-辽阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (915, '211002', '白塔区', 80, 3, 0, 'Baita Qu', 'BTL', '1-7-80-915', '辽宁省-辽阳市-白塔区', 1);
INSERT INTO `qmgx_region` VALUES (916, '211003', '文圣区', 80, 3, 0, 'Wensheng Qu', 'WST', '1-7-80-916', '辽宁省-辽阳市-文圣区', 1);
INSERT INTO `qmgx_region` VALUES (917, '211004', '宏伟区', 80, 3, 0, 'Hongwei Qu', 'HWQ', '1-7-80-917', '辽宁省-辽阳市-宏伟区', 1);
INSERT INTO `qmgx_region` VALUES (918, '211005', '弓长岭区', 80, 3, 0, 'Gongchangling Qu', 'GCL', '1-7-80-918', '辽宁省-辽阳市-弓长岭区', 1);
INSERT INTO `qmgx_region` VALUES (919, '211011', '太子河区', 80, 3, 0, 'Taizihe Qu', 'TZH', '1-7-80-919', '辽宁省-辽阳市-太子河区', 1);
INSERT INTO `qmgx_region` VALUES (920, '211021', '辽阳县', 80, 3, 0, 'Liaoyang Xian', 'LYX', '1-7-80-920', '辽宁省-辽阳市-辽阳县', 1);
INSERT INTO `qmgx_region` VALUES (921, '211081', '灯塔市', 80, 3, 0, 'Dengta Shi', 'DTL', '1-7-80-921', '辽宁省-辽阳市-灯塔市', 1);
INSERT INTO `qmgx_region` VALUES (922, '211101', '市辖区', 81, 3, 0, 'Shixiaqu', '2', '1-7-81-922', '辽宁省-盘锦市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (923, '211102', '双台子区', 81, 3, 0, 'Shuangtaizi Qu', 'STZ', '1-7-81-923', '辽宁省-盘锦市-双台子区', 1);
INSERT INTO `qmgx_region` VALUES (924, '211103', '兴隆台区', 81, 3, 0, 'Xinglongtai Qu', 'XLT', '1-7-81-924', '辽宁省-盘锦市-兴隆台区', 1);
INSERT INTO `qmgx_region` VALUES (925, '211121', '大洼县', 81, 3, 0, 'Dawa Xian', 'DWA', '1-7-81-925', '辽宁省-盘锦市-大洼县', 1);
INSERT INTO `qmgx_region` VALUES (926, '211122', '盘山县', 81, 3, 0, 'Panshan Xian', 'PNS', '1-7-81-926', '辽宁省-盘锦市-盘山县', 1);
INSERT INTO `qmgx_region` VALUES (927, '211201', '市辖区', 82, 3, 0, 'Shixiaqu', '2', '1-7-82-927', '辽宁省-铁岭市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (928, '211202', '银州区', 82, 3, 0, 'Yinzhou Qu', 'YZU', '1-7-82-928', '辽宁省-铁岭市-银州区', 1);
INSERT INTO `qmgx_region` VALUES (929, '211204', '清河区', 82, 3, 0, 'Qinghe Qu', 'QHQ', '1-7-82-929', '辽宁省-铁岭市-清河区', 1);
INSERT INTO `qmgx_region` VALUES (930, '211221', '铁岭县', 82, 3, 0, 'Tieling Xian', 'TLG', '1-7-82-930', '辽宁省-铁岭市-铁岭县', 1);
INSERT INTO `qmgx_region` VALUES (931, '211223', '西丰县', 82, 3, 0, 'Xifeng Xian', 'XIF', '1-7-82-931', '辽宁省-铁岭市-西丰县', 1);
INSERT INTO `qmgx_region` VALUES (932, '211224', '昌图县', 82, 3, 0, 'Changtu Xian', 'CTX', '1-7-82-932', '辽宁省-铁岭市-昌图县', 1);
INSERT INTO `qmgx_region` VALUES (933, '211281', '调兵山市', 82, 3, 0, 'Diaobingshan Shi', '2', '1-7-82-933', '辽宁省-铁岭市-调兵山市', 1);
INSERT INTO `qmgx_region` VALUES (934, '211282', '开原市', 82, 3, 0, 'Kaiyuan Shi', 'KYS', '1-7-82-934', '辽宁省-铁岭市-开原市', 1);
INSERT INTO `qmgx_region` VALUES (935, '211301', '市辖区', 83, 3, 0, 'Shixiaqu', '2', '1-7-83-935', '辽宁省-朝阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (936, '211302', '双塔区', 83, 3, 0, 'Shuangta Qu', 'STQ', '1-7-83-936', '辽宁省-朝阳市-双塔区', 1);
INSERT INTO `qmgx_region` VALUES (937, '211303', '龙城区', 83, 3, 0, 'Longcheng Qu', 'LCL', '1-7-83-937', '辽宁省-朝阳市-龙城区', 1);
INSERT INTO `qmgx_region` VALUES (938, '211321', '朝阳县', 83, 3, 0, 'Chaoyang Xian', 'CYG', '1-7-83-938', '辽宁省-朝阳市-朝阳县', 1);
INSERT INTO `qmgx_region` VALUES (939, '211322', '建平县', 83, 3, 0, 'Jianping Xian', 'JPG', '1-7-83-939', '辽宁省-朝阳市-建平县', 1);
INSERT INTO `qmgx_region` VALUES (940, '211324', '喀喇沁左翼蒙古族自治县', 83, 3, 0, 'Harqin Zuoyi Mongolzu Zizhixian', 'HAZ', '1-7-83-940', '辽宁省-朝阳市-喀喇沁左翼蒙古族自治县', 1);
INSERT INTO `qmgx_region` VALUES (941, '211381', '北票市', 83, 3, 0, 'Beipiao Shi', 'BPO', '1-7-83-941', '辽宁省-朝阳市-北票市', 1);
INSERT INTO `qmgx_region` VALUES (942, '211382', '凌源市', 83, 3, 0, 'Lingyuan Shi', 'LYK', '1-7-83-942', '辽宁省-朝阳市-凌源市', 1);
INSERT INTO `qmgx_region` VALUES (943, '211401', '市辖区', 84, 3, 0, 'Shixiaqu', '2', '1-7-84-943', '辽宁省-葫芦岛市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (944, '211402', '连山区', 84, 3, 0, 'Lianshan Qu', 'LSQ', '1-7-84-944', '辽宁省-葫芦岛市-连山区', 1);
INSERT INTO `qmgx_region` VALUES (945, '211403', '龙港区', 84, 3, 0, 'Longgang Qu', 'LGD', '1-7-84-945', '辽宁省-葫芦岛市-龙港区', 1);
INSERT INTO `qmgx_region` VALUES (946, '211404', '南票区', 84, 3, 0, 'Nanpiao Qu', 'NPQ', '1-7-84-946', '辽宁省-葫芦岛市-南票区', 1);
INSERT INTO `qmgx_region` VALUES (947, '211421', '绥中县', 84, 3, 0, 'Suizhong Xian', 'SZL', '1-7-84-947', '辽宁省-葫芦岛市-绥中县', 1);
INSERT INTO `qmgx_region` VALUES (948, '211422', '建昌县', 84, 3, 0, 'Jianchang Xian', 'JCL', '1-7-84-948', '辽宁省-葫芦岛市-建昌县', 1);
INSERT INTO `qmgx_region` VALUES (949, '211481', '兴城市', 84, 3, 0, 'Xingcheng Shi', 'XCL', '1-7-84-949', '辽宁省-葫芦岛市-兴城市', 1);
INSERT INTO `qmgx_region` VALUES (950, '220101', '市辖区', 85, 3, 0, 'Shixiaqu', '2', '1-8-85-950', '吉林省-长春市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (951, '220102', '南关区', 85, 3, 0, 'Nanguan Qu', 'NGN', '1-8-85-951', '吉林省-长春市-南关区', 1);
INSERT INTO `qmgx_region` VALUES (952, '220103', '宽城区', 85, 3, 0, 'Kuancheng Qu', 'KCQ', '1-8-85-952', '吉林省-长春市-宽城区', 1);
INSERT INTO `qmgx_region` VALUES (953, '220104', '朝阳区', 85, 3, 0, 'Chaoyang Qu ', 'CYC', '1-8-85-953', '吉林省-长春市-朝阳区', 1);
INSERT INTO `qmgx_region` VALUES (954, '220105', '二道区', 85, 3, 0, 'Erdao Qu', 'EDQ', '1-8-85-954', '吉林省-长春市-二道区', 1);
INSERT INTO `qmgx_region` VALUES (955, '220106', '绿园区', 85, 3, 0, 'Lvyuan Qu', 'LYQ', '1-8-85-955', '吉林省-长春市-绿园区', 1);
INSERT INTO `qmgx_region` VALUES (956, '220112', '双阳区', 85, 3, 0, 'Shuangyang Qu', 'SYQ', '1-8-85-956', '吉林省-长春市-双阳区', 1);
INSERT INTO `qmgx_region` VALUES (957, '220122', '农安县', 85, 3, 0, 'Nong,an Xian ', 'NAJ', '1-8-85-957', '吉林省-长春市-农安县', 1);
INSERT INTO `qmgx_region` VALUES (958, '220181', '九台市', 85, 3, 0, 'Jiutai Shi', '2', '1-8-85-958', '吉林省-长春市-九台市', 1);
INSERT INTO `qmgx_region` VALUES (959, '220182', '榆树市', 85, 3, 0, 'Yushu Shi', 'YSS', '1-8-85-959', '吉林省-长春市-榆树市', 1);
INSERT INTO `qmgx_region` VALUES (960, '220183', '德惠市', 85, 3, 0, 'Dehui Shi', 'DEH', '1-8-85-960', '吉林省-长春市-德惠市', 1);
INSERT INTO `qmgx_region` VALUES (961, '220201', '市辖区', 86, 3, 0, 'Shixiaqu', '2', '1-8-86-961', '吉林省-吉林市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (962, '220202', '昌邑区', 86, 3, 0, 'Changyi Qu', 'CYI', '1-8-86-962', '吉林省-吉林市-昌邑区', 1);
INSERT INTO `qmgx_region` VALUES (963, '220203', '龙潭区', 86, 3, 0, 'Longtan Qu', 'LTQ', '1-8-86-963', '吉林省-吉林市-龙潭区', 1);
INSERT INTO `qmgx_region` VALUES (964, '220204', '船营区', 86, 3, 0, 'Chuanying Qu', 'CYJ', '1-8-86-964', '吉林省-吉林市-船营区', 1);
INSERT INTO `qmgx_region` VALUES (965, '220211', '丰满区', 86, 3, 0, 'Fengman Qu', 'FMQ', '1-8-86-965', '吉林省-吉林市-丰满区', 1);
INSERT INTO `qmgx_region` VALUES (966, '220221', '永吉县', 86, 3, 0, 'Yongji Xian', 'YOJ', '1-8-86-966', '吉林省-吉林市-永吉县', 1);
INSERT INTO `qmgx_region` VALUES (967, '220281', '蛟河市', 86, 3, 0, 'Jiaohe Shi', 'JHJ', '1-8-86-967', '吉林省-吉林市-蛟河市', 1);
INSERT INTO `qmgx_region` VALUES (968, '220282', '桦甸市', 86, 3, 0, 'Huadian Shi', 'HDJ', '1-8-86-968', '吉林省-吉林市-桦甸市', 1);
INSERT INTO `qmgx_region` VALUES (969, '220283', '舒兰市', 86, 3, 0, 'Shulan Shi', 'SLN', '1-8-86-969', '吉林省-吉林市-舒兰市', 1);
INSERT INTO `qmgx_region` VALUES (970, '220284', '磐石市', 86, 3, 0, 'Panshi Shi', 'PSI', '1-8-86-970', '吉林省-吉林市-磐石市', 1);
INSERT INTO `qmgx_region` VALUES (971, '220301', '市辖区', 87, 3, 0, 'Shixiaqu', '2', '1-8-87-971', '吉林省-四平市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (972, '220302', '铁西区', 87, 3, 0, 'Tiexi Qu', 'TXS', '1-8-87-972', '吉林省-四平市-铁西区', 1);
INSERT INTO `qmgx_region` VALUES (973, '220303', '铁东区', 87, 3, 0, 'Tiedong Qu ', 'TDQ', '1-8-87-973', '吉林省-四平市-铁东区', 1);
INSERT INTO `qmgx_region` VALUES (974, '220322', '梨树县', 87, 3, 0, 'Lishu Xian', 'LSU', '1-8-87-974', '吉林省-四平市-梨树县', 1);
INSERT INTO `qmgx_region` VALUES (975, '220323', '伊通满族自治县', 87, 3, 0, 'Yitong Manzu Zizhixian', 'YTO', '1-8-87-975', '吉林省-四平市-伊通满族自治县', 1);
INSERT INTO `qmgx_region` VALUES (976, '220381', '公主岭市', 87, 3, 0, 'Gongzhuling Shi', 'GZL', '1-8-87-976', '吉林省-四平市-公主岭市', 1);
INSERT INTO `qmgx_region` VALUES (977, '220382', '双辽市', 87, 3, 0, 'Shuangliao Shi', 'SLS', '1-8-87-977', '吉林省-四平市-双辽市', 1);
INSERT INTO `qmgx_region` VALUES (978, '220401', '市辖区', 88, 3, 0, 'Shixiaqu', '2', '1-8-88-978', '吉林省-辽源市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (979, '220402', '龙山区', 88, 3, 0, 'Longshan Qu', 'LGS', '1-8-88-979', '吉林省-辽源市-龙山区', 1);
INSERT INTO `qmgx_region` VALUES (980, '220403', '西安区', 88, 3, 0, 'Xi,an Qu', 'XAA', '1-8-88-980', '吉林省-辽源市-西安区', 1);
INSERT INTO `qmgx_region` VALUES (981, '220421', '东丰县', 88, 3, 0, 'Dongfeng Xian', 'DGF', '1-8-88-981', '吉林省-辽源市-东丰县', 1);
INSERT INTO `qmgx_region` VALUES (982, '220422', '东辽县', 88, 3, 0, 'Dongliao Xian ', 'DLX', '1-8-88-982', '吉林省-辽源市-东辽县', 1);
INSERT INTO `qmgx_region` VALUES (983, '220501', '市辖区', 89, 3, 0, 'Shixiaqu', '2', '1-8-89-983', '吉林省-通化市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (984, '220502', '东昌区', 89, 3, 0, 'Dongchang Qu', 'DCT', '1-8-89-984', '吉林省-通化市-东昌区', 1);
INSERT INTO `qmgx_region` VALUES (985, '220503', '二道江区', 89, 3, 0, 'Erdaojiang Qu', 'EDJ', '1-8-89-985', '吉林省-通化市-二道江区', 1);
INSERT INTO `qmgx_region` VALUES (986, '220521', '通化县', 89, 3, 0, 'Tonghua Xian ', 'THX', '1-8-89-986', '吉林省-通化市-通化县', 1);
INSERT INTO `qmgx_region` VALUES (987, '220523', '辉南县', 89, 3, 0, 'Huinan Xian ', 'HNA', '1-8-89-987', '吉林省-通化市-辉南县', 1);
INSERT INTO `qmgx_region` VALUES (988, '220524', '柳河县', 89, 3, 0, 'Liuhe Xian ', 'LHC', '1-8-89-988', '吉林省-通化市-柳河县', 1);
INSERT INTO `qmgx_region` VALUES (989, '220581', '梅河口市', 89, 3, 0, 'Meihekou Shi', 'MHK', '1-8-89-989', '吉林省-通化市-梅河口市', 1);
INSERT INTO `qmgx_region` VALUES (990, '220582', '集安市', 89, 3, 0, 'Ji,an Shi', 'KNC', '1-8-89-990', '吉林省-通化市-集安市', 1);
INSERT INTO `qmgx_region` VALUES (991, '220601', '市辖区', 90, 3, 0, 'Shixiaqu', '2', '1-8-90-991', '吉林省-白山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (992, '220602', '八道江区', 90, 3, 0, 'Badaojiang Qu', 'BDJ', '1-8-90-992', '吉林省-白山市-八道江区', 1);
INSERT INTO `qmgx_region` VALUES (993, '220621', '抚松县', 90, 3, 0, 'Fusong Xian', 'FSG', '1-8-90-993', '吉林省-白山市-抚松县', 1);
INSERT INTO `qmgx_region` VALUES (994, '220622', '靖宇县', 90, 3, 0, 'Jingyu Xian', 'JYJ', '1-8-90-994', '吉林省-白山市-靖宇县', 1);
INSERT INTO `qmgx_region` VALUES (995, '220623', '长白朝鲜族自治县', 90, 3, 0, 'Changbaichaoxianzuzizhi Xian', '2', '1-8-90-995', '吉林省-白山市-长白朝鲜族自治县', 1);
INSERT INTO `qmgx_region` VALUES (996, '220605', '江源区', 90, 3, 0, 'Jiangyuan Xian', '2', '1-8-90-996', '吉林省-白山市-江源区', 1);
INSERT INTO `qmgx_region` VALUES (997, '220681', '临江市', 90, 3, 0, 'Linjiang Shi', 'LIN', '1-8-90-997', '吉林省-白山市-临江市', 1);
INSERT INTO `qmgx_region` VALUES (998, '220701', '市辖区', 91, 3, 0, 'Shixiaqu', '2', '1-8-91-998', '吉林省-松原市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (999, '220702', '宁江区', 91, 3, 0, 'Ningjiang Qu', 'NJA', '1-8-91-999', '吉林省-松原市-宁江区', 1);
INSERT INTO `qmgx_region` VALUES (1000, '220721', '前郭尔罗斯蒙古族自治县', 91, 3, 0, 'Qian Gorlos Mongolzu Zizhixian', 'QGO', '1-8-91-1000', '吉林省-松原市-前郭尔罗斯蒙古族自治县', 1);
INSERT INTO `qmgx_region` VALUES (1001, '220722', '长岭县', 91, 3, 0, 'Changling Xian', 'CLG', '1-8-91-1001', '吉林省-松原市-长岭县', 1);
INSERT INTO `qmgx_region` VALUES (1002, '220723', '乾安县', 91, 3, 0, 'Qian,an Xian', 'QAJ', '1-8-91-1002', '吉林省-松原市-乾安县', 1);
INSERT INTO `qmgx_region` VALUES (1003, '220724', '扶余县', 91, 3, 0, 'Fuyu Xian', 'FYU', '1-8-91-1003', '吉林省-松原市-扶余县', 1);
INSERT INTO `qmgx_region` VALUES (1004, '220801', '市辖区', 92, 3, 0, 'Shixiaqu', '2', '1-8-92-1004', '吉林省-白城市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1005, '220802', '洮北区', 92, 3, 0, 'Taobei Qu', 'TBQ', '1-8-92-1005', '吉林省-白城市-洮北区', 1);
INSERT INTO `qmgx_region` VALUES (1006, '220821', '镇赉县', 92, 3, 0, 'Zhenlai Xian', 'ZLA', '1-8-92-1006', '吉林省-白城市-镇赉县', 1);
INSERT INTO `qmgx_region` VALUES (1007, '220822', '通榆县', 92, 3, 0, 'Tongyu Xian', 'TGY', '1-8-92-1007', '吉林省-白城市-通榆县', 1);
INSERT INTO `qmgx_region` VALUES (1008, '220881', '洮南市', 92, 3, 0, 'Taonan Shi', 'TNS', '1-8-92-1008', '吉林省-白城市-洮南市', 1);
INSERT INTO `qmgx_region` VALUES (1009, '220882', '大安市', 92, 3, 0, 'Da,an Shi', 'DNA', '1-8-92-1009', '吉林省-白城市-大安市', 1);
INSERT INTO `qmgx_region` VALUES (1010, '222401', '延吉市', 93, 3, 0, 'Yanji Shi', 'YNJ', '1-8-93-1010', '吉林省-延边朝鲜族自治州-延吉市', 1);
INSERT INTO `qmgx_region` VALUES (1011, '222402', '图们市', 93, 3, 0, 'Tumen Shi', 'TME', '1-8-93-1011', '吉林省-延边朝鲜族自治州-图们市', 1);
INSERT INTO `qmgx_region` VALUES (1012, '222403', '敦化市', 93, 3, 0, 'Dunhua Shi', 'DHS', '1-8-93-1012', '吉林省-延边朝鲜族自治州-敦化市', 1);
INSERT INTO `qmgx_region` VALUES (1013, '222404', '珲春市', 93, 3, 0, 'Hunchun Shi', 'HUC', '1-8-93-1013', '吉林省-延边朝鲜族自治州-珲春市', 1);
INSERT INTO `qmgx_region` VALUES (1014, '222405', '龙井市', 93, 3, 0, 'Longjing Shi', 'LJJ', '1-8-93-1014', '吉林省-延边朝鲜族自治州-龙井市', 1);
INSERT INTO `qmgx_region` VALUES (1015, '222406', '和龙市', 93, 3, 0, 'Helong Shi', 'HEL', '1-8-93-1015', '吉林省-延边朝鲜族自治州-和龙市', 1);
INSERT INTO `qmgx_region` VALUES (1016, '222424', '汪清县', 93, 3, 0, 'Wangqing Xian', 'WGQ', '1-8-93-1016', '吉林省-延边朝鲜族自治州-汪清县', 1);
INSERT INTO `qmgx_region` VALUES (1017, '222426', '安图县', 93, 3, 0, 'Antu Xian', 'ATU', '1-8-93-1017', '吉林省-延边朝鲜族自治州-安图县', 1);
INSERT INTO `qmgx_region` VALUES (1018, '230101', '市辖区', 94, 3, 0, 'Shixiaqu', '2', '1-9-94-1018', '黑龙江省-哈尔滨市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1019, '230102', '道里区', 94, 3, 0, 'Daoli Qu', 'DLH', '1-9-94-1019', '黑龙江省-哈尔滨市-道里区', 1);
INSERT INTO `qmgx_region` VALUES (1020, '230103', '南岗区', 94, 3, 0, 'Nangang Qu', 'NGQ', '1-9-94-1020', '黑龙江省-哈尔滨市-南岗区', 1);
INSERT INTO `qmgx_region` VALUES (1021, '230104', '道外区', 94, 3, 0, 'Daowai Qu', 'DWQ', '1-9-94-1021', '黑龙江省-哈尔滨市-道外区', 1);
INSERT INTO `qmgx_region` VALUES (1022, '230110', '香坊区', 94, 3, 0, 'Xiangfang Qu', '2', '1-9-94-1022', '黑龙江省-哈尔滨市-香坊区', 1);
INSERT INTO `qmgx_region` VALUES (1024, '230108', '平房区', 94, 3, 0, 'Pingfang Qu', 'PFQ', '1-9-94-1024', '黑龙江省-哈尔滨市-平房区', 1);
INSERT INTO `qmgx_region` VALUES (1025, '230109', '松北区', 94, 3, 0, 'Songbei Qu', '2', '1-9-94-1025', '黑龙江省-哈尔滨市-松北区', 1);
INSERT INTO `qmgx_region` VALUES (1026, '230111', '呼兰区', 94, 3, 0, 'Hulan Qu', '2', '1-9-94-1026', '黑龙江省-哈尔滨市-呼兰区', 1);
INSERT INTO `qmgx_region` VALUES (1027, '230123', '依兰县', 94, 3, 0, 'Yilan Xian', 'YLH', '1-9-94-1027', '黑龙江省-哈尔滨市-依兰县', 1);
INSERT INTO `qmgx_region` VALUES (1028, '230124', '方正县', 94, 3, 0, 'Fangzheng Xian', 'FZH', '1-9-94-1028', '黑龙江省-哈尔滨市-方正县', 1);
INSERT INTO `qmgx_region` VALUES (1029, '230125', '宾县', 94, 3, 0, 'Bin Xian', 'BNX', '1-9-94-1029', '黑龙江省-哈尔滨市-宾县', 1);
INSERT INTO `qmgx_region` VALUES (1030, '230126', '巴彦县', 94, 3, 0, 'Bayan Xian', 'BYH', '1-9-94-1030', '黑龙江省-哈尔滨市-巴彦县', 1);
INSERT INTO `qmgx_region` VALUES (1031, '230127', '木兰县', 94, 3, 0, 'Mulan Xian ', 'MUL', '1-9-94-1031', '黑龙江省-哈尔滨市-木兰县', 1);
INSERT INTO `qmgx_region` VALUES (1032, '230128', '通河县', 94, 3, 0, 'Tonghe Xian', 'TOH', '1-9-94-1032', '黑龙江省-哈尔滨市-通河县', 1);
INSERT INTO `qmgx_region` VALUES (1033, '230129', '延寿县', 94, 3, 0, 'Yanshou Xian', 'YSU', '1-9-94-1033', '黑龙江省-哈尔滨市-延寿县', 1);
INSERT INTO `qmgx_region` VALUES (1034, '230112', '阿城区', 94, 3, 0, 'Acheng Shi', '2', '1-9-94-1034', '黑龙江省-哈尔滨市-阿城区', 1);
INSERT INTO `qmgx_region` VALUES (1035, '230182', '双城市', 94, 3, 0, 'Shuangcheng Shi', 'SCS', '1-9-94-1035', '黑龙江省-哈尔滨市-双城市', 1);
INSERT INTO `qmgx_region` VALUES (1036, '230183', '尚志市', 94, 3, 0, 'Shangzhi Shi', 'SZI', '1-9-94-1036', '黑龙江省-哈尔滨市-尚志市', 1);
INSERT INTO `qmgx_region` VALUES (1037, '230184', '五常市', 94, 3, 0, 'Wuchang Shi', 'WCA', '1-9-94-1037', '黑龙江省-哈尔滨市-五常市', 1);
INSERT INTO `qmgx_region` VALUES (1038, '230201', '市辖区', 95, 3, 0, 'Shixiaqu', '2', '1-9-95-1038', '黑龙江省-齐齐哈尔市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1039, '230202', '龙沙区', 95, 3, 0, 'Longsha Qu', 'LQQ', '1-9-95-1039', '黑龙江省-齐齐哈尔市-龙沙区', 1);
INSERT INTO `qmgx_region` VALUES (1040, '230203', '建华区', 95, 3, 0, 'Jianhua Qu', 'JHQ', '1-9-95-1040', '黑龙江省-齐齐哈尔市-建华区', 1);
INSERT INTO `qmgx_region` VALUES (1041, '230204', '铁锋区', 95, 3, 0, 'Tiefeng Qu', '2', '1-9-95-1041', '黑龙江省-齐齐哈尔市-铁锋区', 1);
INSERT INTO `qmgx_region` VALUES (1042, '230205', '昂昂溪区', 95, 3, 0, 'Ang,angxi Qu', 'AAX', '1-9-95-1042', '黑龙江省-齐齐哈尔市-昂昂溪区', 1);
INSERT INTO `qmgx_region` VALUES (1043, '230206', '富拉尔基区', 95, 3, 0, 'Hulan Ergi Qu', 'HUE', '1-9-95-1043', '黑龙江省-齐齐哈尔市-富拉尔基区', 1);
INSERT INTO `qmgx_region` VALUES (1044, '230207', '碾子山区', 95, 3, 0, 'Nianzishan Qu', 'NZS', '1-9-95-1044', '黑龙江省-齐齐哈尔市-碾子山区', 1);
INSERT INTO `qmgx_region` VALUES (1045, '230208', '梅里斯达斡尔族区', 95, 3, 0, 'Meilisidawoerzu Qu', '2', '1-9-95-1045', '黑龙江省-齐齐哈尔市-梅里斯达斡尔族区', 1);
INSERT INTO `qmgx_region` VALUES (1046, '230221', '龙江县', 95, 3, 0, 'Longjiang Xian', 'LGJ', '1-9-95-1046', '黑龙江省-齐齐哈尔市-龙江县', 1);
INSERT INTO `qmgx_region` VALUES (1047, '230223', '依安县', 95, 3, 0, 'Yi,an Xian', 'YAN', '1-9-95-1047', '黑龙江省-齐齐哈尔市-依安县', 1);
INSERT INTO `qmgx_region` VALUES (1048, '230224', '泰来县', 95, 3, 0, 'Tailai Xian', 'TLA', '1-9-95-1048', '黑龙江省-齐齐哈尔市-泰来县', 1);
INSERT INTO `qmgx_region` VALUES (1049, '230225', '甘南县', 95, 3, 0, 'Gannan Xian', 'GNX', '1-9-95-1049', '黑龙江省-齐齐哈尔市-甘南县', 1);
INSERT INTO `qmgx_region` VALUES (1050, '230227', '富裕县', 95, 3, 0, 'Fuyu Xian', 'FYX', '1-9-95-1050', '黑龙江省-齐齐哈尔市-富裕县', 1);
INSERT INTO `qmgx_region` VALUES (1051, '230229', '克山县', 95, 3, 0, 'Keshan Xian', 'KSN', '1-9-95-1051', '黑龙江省-齐齐哈尔市-克山县', 1);
INSERT INTO `qmgx_region` VALUES (1052, '230230', '克东县', 95, 3, 0, 'Kedong Xian', 'KDO', '1-9-95-1052', '黑龙江省-齐齐哈尔市-克东县', 1);
INSERT INTO `qmgx_region` VALUES (1053, '230231', '拜泉县', 95, 3, 0, 'Baiquan Xian', 'BQN', '1-9-95-1053', '黑龙江省-齐齐哈尔市-拜泉县', 1);
INSERT INTO `qmgx_region` VALUES (1054, '230281', '讷河市', 95, 3, 0, 'Nehe Shi', 'NEH', '1-9-95-1054', '黑龙江省-齐齐哈尔市-讷河市', 1);
INSERT INTO `qmgx_region` VALUES (1055, '230301', '市辖区', 96, 3, 0, 'Shixiaqu', '2', '1-9-96-1055', '黑龙江省-鸡西市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1056, '230302', '鸡冠区', 96, 3, 0, 'Jiguan Qu', 'JGU', '1-9-96-1056', '黑龙江省-鸡西市-鸡冠区', 1);
INSERT INTO `qmgx_region` VALUES (1057, '230303', '恒山区', 96, 3, 0, 'Hengshan Qu', 'HSD', '1-9-96-1057', '黑龙江省-鸡西市-恒山区', 1);
INSERT INTO `qmgx_region` VALUES (1058, '230304', '滴道区', 96, 3, 0, 'Didao Qu', 'DDO', '1-9-96-1058', '黑龙江省-鸡西市-滴道区', 1);
INSERT INTO `qmgx_region` VALUES (1059, '230305', '梨树区', 96, 3, 0, 'Lishu Qu', 'LJX', '1-9-96-1059', '黑龙江省-鸡西市-梨树区', 1);
INSERT INTO `qmgx_region` VALUES (1060, '230306', '城子河区', 96, 3, 0, 'Chengzihe Qu', 'CZH', '1-9-96-1060', '黑龙江省-鸡西市-城子河区', 1);
INSERT INTO `qmgx_region` VALUES (1061, '230307', '麻山区', 96, 3, 0, 'Mashan Qu', 'MSN', '1-9-96-1061', '黑龙江省-鸡西市-麻山区', 1);
INSERT INTO `qmgx_region` VALUES (1062, '230321', '鸡东县', 96, 3, 0, 'Jidong Xian', 'JID', '1-9-96-1062', '黑龙江省-鸡西市-鸡东县', 1);
INSERT INTO `qmgx_region` VALUES (1063, '230381', '虎林市', 96, 3, 0, 'Hulin Shi', 'HUL', '1-9-96-1063', '黑龙江省-鸡西市-虎林市', 1);
INSERT INTO `qmgx_region` VALUES (1064, '230382', '密山市', 96, 3, 0, 'Mishan Shi', 'MIS', '1-9-96-1064', '黑龙江省-鸡西市-密山市', 1);
INSERT INTO `qmgx_region` VALUES (1065, '230401', '市辖区', 97, 3, 0, 'Shixiaqu', '2', '1-9-97-1065', '黑龙江省-鹤岗市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1066, '230402', '向阳区', 97, 3, 0, 'Xiangyang  Qu ', 'XYZ', '1-9-97-1066', '黑龙江省-鹤岗市-向阳区', 1);
INSERT INTO `qmgx_region` VALUES (1067, '230403', '工农区', 97, 3, 0, 'Gongnong Qu', 'GNH', '1-9-97-1067', '黑龙江省-鹤岗市-工农区', 1);
INSERT INTO `qmgx_region` VALUES (1068, '230404', '南山区', 97, 3, 0, 'Nanshan Qu', 'NSQ', '1-9-97-1068', '黑龙江省-鹤岗市-南山区', 1);
INSERT INTO `qmgx_region` VALUES (1069, '230405', '兴安区', 97, 3, 0, 'Xing,an Qu', 'XAH', '1-9-97-1069', '黑龙江省-鹤岗市-兴安区', 1);
INSERT INTO `qmgx_region` VALUES (1070, '230406', '东山区', 97, 3, 0, 'Dongshan Qu', 'DSA', '1-9-97-1070', '黑龙江省-鹤岗市-东山区', 1);
INSERT INTO `qmgx_region` VALUES (1071, '230407', '兴山区', 97, 3, 0, 'Xingshan Qu', 'XSQ', '1-9-97-1071', '黑龙江省-鹤岗市-兴山区', 1);
INSERT INTO `qmgx_region` VALUES (1072, '230421', '萝北县', 97, 3, 0, 'Luobei Xian', 'LUB', '1-9-97-1072', '黑龙江省-鹤岗市-萝北县', 1);
INSERT INTO `qmgx_region` VALUES (1073, '230422', '绥滨县', 97, 3, 0, 'Suibin Xian', '2', '1-9-97-1073', '黑龙江省-鹤岗市-绥滨县', 1);
INSERT INTO `qmgx_region` VALUES (1074, '230501', '市辖区', 98, 3, 0, 'Shixiaqu', '2', '1-9-98-1074', '黑龙江省-双鸭山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1075, '230502', '尖山区', 98, 3, 0, 'Jianshan Qu', 'JSQ', '1-9-98-1075', '黑龙江省-双鸭山市-尖山区', 1);
INSERT INTO `qmgx_region` VALUES (1076, '230503', '岭东区', 98, 3, 0, 'Lingdong Qu', 'LDQ', '1-9-98-1076', '黑龙江省-双鸭山市-岭东区', 1);
INSERT INTO `qmgx_region` VALUES (1077, '230505', '四方台区', 98, 3, 0, 'Sifangtai Qu', 'SFT', '1-9-98-1077', '黑龙江省-双鸭山市-四方台区', 1);
INSERT INTO `qmgx_region` VALUES (1078, '230506', '宝山区', 98, 3, 0, 'Baoshan Qu', 'BSQ', '1-9-98-1078', '黑龙江省-双鸭山市-宝山区', 1);
INSERT INTO `qmgx_region` VALUES (1079, '230521', '集贤县', 98, 3, 0, 'Jixian Xian', 'JXH', '1-9-98-1079', '黑龙江省-双鸭山市-集贤县', 1);
INSERT INTO `qmgx_region` VALUES (1080, '230522', '友谊县', 98, 3, 0, 'Youyi Xian', 'YYI', '1-9-98-1080', '黑龙江省-双鸭山市-友谊县', 1);
INSERT INTO `qmgx_region` VALUES (1081, '230523', '宝清县', 98, 3, 0, 'Baoqing Xian', 'BQG', '1-9-98-1081', '黑龙江省-双鸭山市-宝清县', 1);
INSERT INTO `qmgx_region` VALUES (1082, '230524', '饶河县', 98, 3, 0, 'Raohe Xian ', 'ROH', '1-9-98-1082', '黑龙江省-双鸭山市-饶河县', 1);
INSERT INTO `qmgx_region` VALUES (1083, '230601', '市辖区', 99, 3, 0, 'Shixiaqu', '2', '1-9-99-1083', '黑龙江省-大庆市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1084, '230602', '萨尔图区', 99, 3, 0, 'Sairt Qu', 'SAI', '1-9-99-1084', '黑龙江省-大庆市-萨尔图区', 1);
INSERT INTO `qmgx_region` VALUES (1085, '230603', '龙凤区', 99, 3, 0, 'Longfeng Qu', 'LFQ', '1-9-99-1085', '黑龙江省-大庆市-龙凤区', 1);
INSERT INTO `qmgx_region` VALUES (1086, '230604', '让胡路区', 99, 3, 0, 'Ranghulu Qu', 'RHL', '1-9-99-1086', '黑龙江省-大庆市-让胡路区', 1);
INSERT INTO `qmgx_region` VALUES (1087, '230605', '红岗区', 99, 3, 0, 'Honggang Qu', 'HGD', '1-9-99-1087', '黑龙江省-大庆市-红岗区', 1);
INSERT INTO `qmgx_region` VALUES (1088, '230606', '大同区', 99, 3, 0, 'Datong Qu', 'DHD', '1-9-99-1088', '黑龙江省-大庆市-大同区', 1);
INSERT INTO `qmgx_region` VALUES (1089, '230621', '肇州县', 99, 3, 0, 'Zhaozhou Xian', 'ZAZ', '1-9-99-1089', '黑龙江省-大庆市-肇州县', 1);
INSERT INTO `qmgx_region` VALUES (1090, '230622', '肇源县', 99, 3, 0, 'Zhaoyuan Xian', 'ZYH', '1-9-99-1090', '黑龙江省-大庆市-肇源县', 1);
INSERT INTO `qmgx_region` VALUES (1091, '230623', '林甸县', 99, 3, 0, 'Lindian Xian ', 'LDN', '1-9-99-1091', '黑龙江省-大庆市-林甸县', 1);
INSERT INTO `qmgx_region` VALUES (1092, '230624', '杜尔伯特蒙古族自治县', 99, 3, 0, 'Dorbod Mongolzu Zizhixian', 'DOM', '1-9-99-1092', '黑龙江省-大庆市-杜尔伯特蒙古族自治县', 1);
INSERT INTO `qmgx_region` VALUES (1093, '230701', '市辖区', 100, 3, 0, 'Shixiaqu', '2', '1-9-100-1093', '黑龙江省-伊春市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1094, '230702', '伊春区', 100, 3, 0, 'Yichun Qu', 'YYC', '1-9-100-1094', '黑龙江省-伊春市-伊春区', 1);
INSERT INTO `qmgx_region` VALUES (1095, '230703', '南岔区', 100, 3, 0, 'Nancha Qu', 'NCQ', '1-9-100-1095', '黑龙江省-伊春市-南岔区', 1);
INSERT INTO `qmgx_region` VALUES (1096, '230704', '友好区', 100, 3, 0, 'Youhao Qu', 'YOH', '1-9-100-1096', '黑龙江省-伊春市-友好区', 1);
INSERT INTO `qmgx_region` VALUES (1097, '230705', '西林区', 100, 3, 0, 'Xilin Qu', 'XIL', '1-9-100-1097', '黑龙江省-伊春市-西林区', 1);
INSERT INTO `qmgx_region` VALUES (1098, '230706', '翠峦区', 100, 3, 0, 'Cuiluan Qu', 'CLN', '1-9-100-1098', '黑龙江省-伊春市-翠峦区', 1);
INSERT INTO `qmgx_region` VALUES (1099, '230707', '新青区', 100, 3, 0, 'Xinqing Qu', 'XQQ', '1-9-100-1099', '黑龙江省-伊春市-新青区', 1);
INSERT INTO `qmgx_region` VALUES (1100, '230708', '美溪区', 100, 3, 0, 'Meixi Qu', 'MXQ', '1-9-100-1100', '黑龙江省-伊春市-美溪区', 1);
INSERT INTO `qmgx_region` VALUES (1101, '230709', '金山屯区', 100, 3, 0, 'Jinshantun Qu', 'JST', '1-9-100-1101', '黑龙江省-伊春市-金山屯区', 1);
INSERT INTO `qmgx_region` VALUES (1102, '230710', '五营区', 100, 3, 0, 'Wuying Qu', 'WYQ', '1-9-100-1102', '黑龙江省-伊春市-五营区', 1);
INSERT INTO `qmgx_region` VALUES (1103, '230711', '乌马河区', 100, 3, 0, 'Wumahe Qu', 'WMH', '1-9-100-1103', '黑龙江省-伊春市-乌马河区', 1);
INSERT INTO `qmgx_region` VALUES (1104, '230712', '汤旺河区', 100, 3, 0, 'Tangwanghe Qu', 'TWH', '1-9-100-1104', '黑龙江省-伊春市-汤旺河区', 1);
INSERT INTO `qmgx_region` VALUES (1105, '230713', '带岭区', 100, 3, 0, 'Dailing Qu', 'DLY', '1-9-100-1105', '黑龙江省-伊春市-带岭区', 1);
INSERT INTO `qmgx_region` VALUES (1106, '230714', '乌伊岭区', 100, 3, 0, 'Wuyiling Qu', 'WYL', '1-9-100-1106', '黑龙江省-伊春市-乌伊岭区', 1);
INSERT INTO `qmgx_region` VALUES (1107, '230715', '红星区', 100, 3, 0, 'Hongxing Qu', 'HGX', '1-9-100-1107', '黑龙江省-伊春市-红星区', 1);
INSERT INTO `qmgx_region` VALUES (1108, '230716', '上甘岭区', 100, 3, 0, 'Shangganling Qu', 'SGL', '1-9-100-1108', '黑龙江省-伊春市-上甘岭区', 1);
INSERT INTO `qmgx_region` VALUES (1109, '230722', '嘉荫县', 100, 3, 0, 'Jiayin Xian', '2', '1-9-100-1109', '黑龙江省-伊春市-嘉荫县', 1);
INSERT INTO `qmgx_region` VALUES (1110, '230781', '铁力市', 100, 3, 0, 'Tieli Shi', 'TEL', '1-9-100-1110', '黑龙江省-伊春市-铁力市', 1);
INSERT INTO `qmgx_region` VALUES (1111, '230801', '市辖区', 101, 3, 0, 'Shixiaqu', '2', '1-9-101-1111', '黑龙江省-佳木斯市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1113, '230803', '向阳区', 101, 3, 0, 'Xiangyang  Qu ', 'XYQ', '1-9-101-1113', '黑龙江省-佳木斯市-向阳区', 1);
INSERT INTO `qmgx_region` VALUES (1114, '230804', '前进区', 101, 3, 0, 'Qianjin Qu', 'QJQ', '1-9-101-1114', '黑龙江省-佳木斯市-前进区', 1);
INSERT INTO `qmgx_region` VALUES (1115, '230805', '东风区', 101, 3, 0, 'Dongfeng Qu', 'DFQ', '1-9-101-1115', '黑龙江省-佳木斯市-东风区', 1);
INSERT INTO `qmgx_region` VALUES (1116, '230811', '郊区', 101, 3, 0, 'Jiaoqu', 'JQJ', '1-9-101-1116', '黑龙江省-佳木斯市-郊区', 1);
INSERT INTO `qmgx_region` VALUES (1117, '230822', '桦南县', 101, 3, 0, 'Huanan Xian', 'HNH', '1-9-101-1117', '黑龙江省-佳木斯市-桦南县', 1);
INSERT INTO `qmgx_region` VALUES (1118, '230826', '桦川县', 101, 3, 0, 'Huachuan Xian', 'HCN', '1-9-101-1118', '黑龙江省-佳木斯市-桦川县', 1);
INSERT INTO `qmgx_region` VALUES (1119, '230828', '汤原县', 101, 3, 0, 'Tangyuan Xian', 'TYX', '1-9-101-1119', '黑龙江省-佳木斯市-汤原县', 1);
INSERT INTO `qmgx_region` VALUES (1120, '230833', '抚远县', 101, 3, 0, 'Fuyuan Xian', 'FUY', '1-9-101-1120', '黑龙江省-佳木斯市-抚远县', 1);
INSERT INTO `qmgx_region` VALUES (1121, '230881', '同江市', 101, 3, 0, 'Tongjiang Shi', 'TOJ', '1-9-101-1121', '黑龙江省-佳木斯市-同江市', 1);
INSERT INTO `qmgx_region` VALUES (1122, '230882', '富锦市', 101, 3, 0, 'Fujin Shi', 'FUJ', '1-9-101-1122', '黑龙江省-佳木斯市-富锦市', 1);
INSERT INTO `qmgx_region` VALUES (1123, '230901', '市辖区', 102, 3, 0, 'Shixiaqu', '2', '1-9-102-1123', '黑龙江省-七台河市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1124, '230902', '新兴区', 102, 3, 0, 'Xinxing Qu', 'XXQ', '1-9-102-1124', '黑龙江省-七台河市-新兴区', 1);
INSERT INTO `qmgx_region` VALUES (1125, '230903', '桃山区', 102, 3, 0, 'Taoshan Qu', 'TSC', '1-9-102-1125', '黑龙江省-七台河市-桃山区', 1);
INSERT INTO `qmgx_region` VALUES (1126, '230904', '茄子河区', 102, 3, 0, 'Qiezihe Qu', 'QZI', '1-9-102-1126', '黑龙江省-七台河市-茄子河区', 1);
INSERT INTO `qmgx_region` VALUES (1127, '230921', '勃利县', 102, 3, 0, 'Boli Xian', 'BLI', '1-9-102-1127', '黑龙江省-七台河市-勃利县', 1);
INSERT INTO `qmgx_region` VALUES (1128, '231001', '市辖区', 103, 3, 0, 'Shixiaqu', '2', '1-9-103-1128', '黑龙江省-牡丹江市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1129, '231002', '东安区', 103, 3, 0, 'Dong,an Qu', 'DGA', '1-9-103-1129', '黑龙江省-牡丹江市-东安区', 1);
INSERT INTO `qmgx_region` VALUES (1130, '231003', '阳明区', 103, 3, 0, 'Yangming Qu', 'YMQ', '1-9-103-1130', '黑龙江省-牡丹江市-阳明区', 1);
INSERT INTO `qmgx_region` VALUES (1131, '231004', '爱民区', 103, 3, 0, 'Aimin Qu', 'AMQ', '1-9-103-1131', '黑龙江省-牡丹江市-爱民区', 1);
INSERT INTO `qmgx_region` VALUES (1132, '231005', '西安区', 103, 3, 0, 'Xi,an Qu', 'XAQ', '1-9-103-1132', '黑龙江省-牡丹江市-西安区', 1);
INSERT INTO `qmgx_region` VALUES (1133, '231024', '东宁县', 103, 3, 0, 'Dongning Xian', 'DON', '1-9-103-1133', '黑龙江省-牡丹江市-东宁县', 1);
INSERT INTO `qmgx_region` VALUES (1134, '231025', '林口县', 103, 3, 0, 'Linkou Xian', 'LKO', '1-9-103-1134', '黑龙江省-牡丹江市-林口县', 1);
INSERT INTO `qmgx_region` VALUES (1135, '231081', '绥芬河市', 103, 3, 0, 'Suifenhe Shi', 'SFE', '1-9-103-1135', '黑龙江省-牡丹江市-绥芬河市', 1);
INSERT INTO `qmgx_region` VALUES (1136, '231083', '海林市', 103, 3, 0, 'Hailin Shi', 'HLS', '1-9-103-1136', '黑龙江省-牡丹江市-海林市', 1);
INSERT INTO `qmgx_region` VALUES (1137, '231084', '宁安市', 103, 3, 0, 'Ning,an Shi', 'NAI', '1-9-103-1137', '黑龙江省-牡丹江市-宁安市', 1);
INSERT INTO `qmgx_region` VALUES (1138, '231085', '穆棱市', 103, 3, 0, 'Muling Shi', 'MLG', '1-9-103-1138', '黑龙江省-牡丹江市-穆棱市', 1);
INSERT INTO `qmgx_region` VALUES (1139, '231101', '市辖区', 104, 3, 0, 'Shixiaqu', '2', '1-9-104-1139', '黑龙江省-黑河市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1140, '231102', '爱辉区', 104, 3, 0, 'Aihui Qu', 'AHQ', '1-9-104-1140', '黑龙江省-黑河市-爱辉区', 1);
INSERT INTO `qmgx_region` VALUES (1141, '231121', '嫩江县', 104, 3, 0, 'Nenjiang Xian', 'NJH', '1-9-104-1141', '黑龙江省-黑河市-嫩江县', 1);
INSERT INTO `qmgx_region` VALUES (1142, '231123', '逊克县', 104, 3, 0, 'Xunke Xian', 'XUK', '1-9-104-1142', '黑龙江省-黑河市-逊克县', 1);
INSERT INTO `qmgx_region` VALUES (1143, '231124', '孙吴县', 104, 3, 0, 'Sunwu Xian', 'SUW', '1-9-104-1143', '黑龙江省-黑河市-孙吴县', 1);
INSERT INTO `qmgx_region` VALUES (1144, '231181', '北安市', 104, 3, 0, 'Bei,an Shi', 'BAS', '1-9-104-1144', '黑龙江省-黑河市-北安市', 1);
INSERT INTO `qmgx_region` VALUES (1145, '231182', '五大连池市', 104, 3, 0, 'Wudalianchi Shi', 'WDL', '1-9-104-1145', '黑龙江省-黑河市-五大连池市', 1);
INSERT INTO `qmgx_region` VALUES (1146, '231201', '市辖区', 105, 3, 0, '1', '2', '1-9-105-1146', '黑龙江省-绥化市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1147, '231202', '北林区', 105, 3, 0, 'Beilin Qu', '2', '1-9-105-1147', '黑龙江省-绥化市-北林区', 1);
INSERT INTO `qmgx_region` VALUES (1148, '231221', '望奎县', 105, 3, 0, 'Wangkui Xian', '2', '1-9-105-1148', '黑龙江省-绥化市-望奎县', 1);
INSERT INTO `qmgx_region` VALUES (1149, '231222', '兰西县', 105, 3, 0, 'Lanxi Xian', '2', '1-9-105-1149', '黑龙江省-绥化市-兰西县', 1);
INSERT INTO `qmgx_region` VALUES (1150, '231223', '青冈县', 105, 3, 0, 'Qinggang Xian', '2', '1-9-105-1150', '黑龙江省-绥化市-青冈县', 1);
INSERT INTO `qmgx_region` VALUES (1151, '231224', '庆安县', 105, 3, 0, 'Qing,an Xian', '2', '1-9-105-1151', '黑龙江省-绥化市-庆安县', 1);
INSERT INTO `qmgx_region` VALUES (1152, '231225', '明水县', 105, 3, 0, 'Mingshui Xian', '2', '1-9-105-1152', '黑龙江省-绥化市-明水县', 1);
INSERT INTO `qmgx_region` VALUES (1153, '231226', '绥棱县', 105, 3, 0, 'Suileng Xian', '2', '1-9-105-1153', '黑龙江省-绥化市-绥棱县', 1);
INSERT INTO `qmgx_region` VALUES (1154, '231281', '安达市', 105, 3, 0, 'Anda Shi', '2', '1-9-105-1154', '黑龙江省-绥化市-安达市', 1);
INSERT INTO `qmgx_region` VALUES (1155, '231282', '肇东市', 105, 3, 0, 'Zhaodong Shi', '2', '1-9-105-1155', '黑龙江省-绥化市-肇东市', 1);
INSERT INTO `qmgx_region` VALUES (1156, '231283', '海伦市', 105, 3, 0, 'Hailun Shi', '2', '1-9-105-1156', '黑龙江省-绥化市-海伦市', 1);
INSERT INTO `qmgx_region` VALUES (1157, '232721', '呼玛县', 106, 3, 0, 'Huma Xian', 'HUM', '1-9-106-1157', '黑龙江省-大兴安岭地区-呼玛县', 1);
INSERT INTO `qmgx_region` VALUES (1158, '232722', '塔河县', 106, 3, 0, 'Tahe Xian', 'TAH', '1-9-106-1158', '黑龙江省-大兴安岭地区-塔河县', 1);
INSERT INTO `qmgx_region` VALUES (1159, '232723', '漠河县', 106, 3, 0, 'Mohe Xian', 'MOH', '1-9-106-1159', '黑龙江省-大兴安岭地区-漠河县', 1);
INSERT INTO `qmgx_region` VALUES (1160, '310101', '黄浦区', 107, 3, 0, 'Huangpu Qu', 'HGP', '1-10-107-1160', '上海市-市辖区-黄浦区', 1);
INSERT INTO `qmgx_region` VALUES (1161, '310103', '卢湾区', 107, 3, 0, 'Luwan Qu', 'LWN', '1-10-107-1161', '上海市-市辖区-卢湾区', 1);
INSERT INTO `qmgx_region` VALUES (1162, '310104', '徐汇区', 107, 3, 0, 'Xuhui Qu', 'XHI', '1-10-107-1162', '上海市-市辖区-徐汇区', 1);
INSERT INTO `qmgx_region` VALUES (1163, '310105', '长宁区', 107, 3, 0, 'Changning Qu', 'CNQ', '1-10-107-1163', '上海市-市辖区-长宁区', 1);
INSERT INTO `qmgx_region` VALUES (1164, '310106', '静安区', 107, 3, 0, 'Jing,an Qu', 'JAQ', '1-10-107-1164', '上海市-市辖区-静安区', 1);
INSERT INTO `qmgx_region` VALUES (1165, '310107', '普陀区', 107, 3, 0, 'Putuo Qu', 'PTQ', '1-10-107-1165', '上海市-市辖区-普陀区', 1);
INSERT INTO `qmgx_region` VALUES (1166, '310108', '闸北区', 107, 3, 0, 'Zhabei Qu', 'ZBE', '1-10-107-1166', '上海市-市辖区-闸北区', 1);
INSERT INTO `qmgx_region` VALUES (1167, '310109', '虹口区', 107, 3, 0, 'Hongkou Qu', 'HKQ', '1-10-107-1167', '上海市-市辖区-虹口区', 1);
INSERT INTO `qmgx_region` VALUES (1168, '310110', '杨浦区', 107, 3, 0, 'Yangpu Qu', 'YPU', '1-10-107-1168', '上海市-市辖区-杨浦区', 1);
INSERT INTO `qmgx_region` VALUES (1169, '310112', '闵行区', 107, 3, 0, 'Minhang Qu', 'MHQ', '1-10-107-1169', '上海市-市辖区-闵行区', 1);
INSERT INTO `qmgx_region` VALUES (1170, '310113', '宝山区', 107, 3, 0, 'Baoshan Qu', 'BAO', '1-10-107-1170', '上海市-市辖区-宝山区', 1);
INSERT INTO `qmgx_region` VALUES (1171, '310114', '嘉定区', 107, 3, 0, 'Jiading Qu', 'JDG', '1-10-107-1171', '上海市-市辖区-嘉定区', 1);
INSERT INTO `qmgx_region` VALUES (1172, '310115', '浦东新区', 107, 3, 0, 'Pudong Xinqu', 'PDX', '1-10-107-1172', '上海市-市辖区-浦东新区', 1);
INSERT INTO `qmgx_region` VALUES (1173, '310116', '金山区', 107, 3, 0, 'Jinshan Qu', 'JSH', '1-10-107-1173', '上海市-市辖区-金山区', 1);
INSERT INTO `qmgx_region` VALUES (1174, '310117', '松江区', 107, 3, 0, 'Songjiang Qu', 'SOJ', '1-10-107-1174', '上海市-市辖区-松江区', 1);
INSERT INTO `qmgx_region` VALUES (1175, '310118', '青浦区', 107, 3, 0, 'Qingpu  Qu', 'QPU', '1-10-107-1175', '上海市-市辖区-青浦区', 1);
INSERT INTO `qmgx_region` VALUES (1177, '310120', '奉贤区', 107, 3, 0, 'Fengxian Qu', 'FXI', '1-10-107-1177', '上海市-市辖区-奉贤区', 1);
INSERT INTO `qmgx_region` VALUES (1178, '310230', '崇明县', 107, 3, 0, 'Chongming Xian', 'CMI', '1-10-107-1178', '上海市-县-崇明县', 1);
INSERT INTO `qmgx_region` VALUES (1179, '320101', '市辖区', 109, 3, 0, 'Shixiaqu', '2', '1-11-109-1179', '江苏省-南京市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1180, '320102', '玄武区', 109, 3, 0, 'Xuanwu Qu', 'XWU', '1-11-109-1180', '江苏省-南京市-玄武区', 1);
INSERT INTO `qmgx_region` VALUES (1181, '320103', '白下区', 109, 3, 0, 'Baixia Qu', 'BXQ', '1-11-109-1181', '江苏省-南京市-白下区', 1);
INSERT INTO `qmgx_region` VALUES (1182, '320104', '秦淮区', 109, 3, 0, 'Qinhuai Qu', 'QHU', '1-11-109-1182', '江苏省-南京市-秦淮区', 1);
INSERT INTO `qmgx_region` VALUES (1183, '320105', '建邺区', 109, 3, 0, 'Jianye Qu', 'JYQ', '1-11-109-1183', '江苏省-南京市-建邺区', 1);
INSERT INTO `qmgx_region` VALUES (1184, '320106', '鼓楼区', 109, 3, 0, 'Gulou Qu', 'GLQ', '1-11-109-1184', '江苏省-南京市-鼓楼区', 1);
INSERT INTO `qmgx_region` VALUES (1185, '320107', '下关区', 109, 3, 0, 'Xiaguan Qu', 'XGQ', '1-11-109-1185', '江苏省-南京市-下关区', 1);
INSERT INTO `qmgx_region` VALUES (1186, '320111', '浦口区', 109, 3, 0, 'Pukou Qu', 'PKO', '1-11-109-1186', '江苏省-南京市-浦口区', 1);
INSERT INTO `qmgx_region` VALUES (1187, '320113', '栖霞区', 109, 3, 0, 'Qixia Qu', 'QAX', '1-11-109-1187', '江苏省-南京市-栖霞区', 1);
INSERT INTO `qmgx_region` VALUES (1188, '320114', '雨花台区', 109, 3, 0, 'Yuhuatai Qu', 'YHT', '1-11-109-1188', '江苏省-南京市-雨花台区', 1);
INSERT INTO `qmgx_region` VALUES (1189, '320115', '江宁区', 109, 3, 0, 'Jiangning Qu', '2', '1-11-109-1189', '江苏省-南京市-江宁区', 1);
INSERT INTO `qmgx_region` VALUES (1190, '320116', '六合区', 109, 3, 0, 'Liuhe Qu', '2', '1-11-109-1190', '江苏省-南京市-六合区', 1);
INSERT INTO `qmgx_region` VALUES (1191, '320124', '溧水县', 109, 3, 0, 'Lishui Xian', 'LIS', '1-11-109-1191', '江苏省-南京市-溧水县', 1);
INSERT INTO `qmgx_region` VALUES (1192, '320125', '高淳县', 109, 3, 0, 'Gaochun Xian', 'GCN', '1-11-109-1192', '江苏省-南京市-高淳县', 1);
INSERT INTO `qmgx_region` VALUES (1193, '320201', '市辖区', 110, 3, 0, 'Shixiaqu', '2', '1-11-110-1193', '江苏省-无锡市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1194, '320202', '崇安区', 110, 3, 0, 'Chong,an Qu', 'CGA', '1-11-110-1194', '江苏省-无锡市-崇安区', 1);
INSERT INTO `qmgx_region` VALUES (1195, '320203', '南长区', 110, 3, 0, 'Nanchang Qu', 'NCG', '1-11-110-1195', '江苏省-无锡市-南长区', 1);
INSERT INTO `qmgx_region` VALUES (1196, '320204', '北塘区', 110, 3, 0, 'Beitang Qu', 'BTQ', '1-11-110-1196', '江苏省-无锡市-北塘区', 1);
INSERT INTO `qmgx_region` VALUES (1197, '320205', '锡山区', 110, 3, 0, 'Xishan Qu', '2', '1-11-110-1197', '江苏省-无锡市-锡山区', 1);
INSERT INTO `qmgx_region` VALUES (1198, '320206', '惠山区', 110, 3, 0, 'Huishan Qu', '2', '1-11-110-1198', '江苏省-无锡市-惠山区', 1);
INSERT INTO `qmgx_region` VALUES (1199, '320211', '滨湖区', 110, 3, 0, 'Binhu Qu', '2', '1-11-110-1199', '江苏省-无锡市-滨湖区', 1);
INSERT INTO `qmgx_region` VALUES (1200, '320281', '江阴市', 110, 3, 0, 'Jiangyin Shi', 'JIA', '1-11-110-1200', '江苏省-无锡市-江阴市', 1);
INSERT INTO `qmgx_region` VALUES (1201, '320282', '宜兴市', 110, 3, 0, 'Yixing Shi', 'YIX', '1-11-110-1201', '江苏省-无锡市-宜兴市', 1);
INSERT INTO `qmgx_region` VALUES (1202, '320301', '市辖区', 111, 3, 0, 'Shixiaqu', '2', '1-11-111-1202', '江苏省-徐州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1203, '320302', '鼓楼区', 111, 3, 0, 'Gulou Qu', 'GLU', '1-11-111-1203', '江苏省-徐州市-鼓楼区', 1);
INSERT INTO `qmgx_region` VALUES (1204, '320303', '云龙区', 111, 3, 0, 'Yunlong Qu', 'YLF', '1-11-111-1204', '江苏省-徐州市-云龙区', 1);
INSERT INTO `qmgx_region` VALUES (1206, '320305', '贾汪区', 111, 3, 0, 'Jiawang Qu', 'JWQ', '1-11-111-1206', '江苏省-徐州市-贾汪区', 1);
INSERT INTO `qmgx_region` VALUES (1207, '320311', '泉山区', 111, 3, 0, 'Quanshan Qu', 'QSX', '1-11-111-1207', '江苏省-徐州市-泉山区', 1);
INSERT INTO `qmgx_region` VALUES (1208, '320321', '丰县', 111, 3, 0, 'Feng Xian', 'FXN', '1-11-111-1208', '江苏省-徐州市-丰县', 1);
INSERT INTO `qmgx_region` VALUES (1209, '320322', '沛县', 111, 3, 0, 'Pei Xian', 'PEI', '1-11-111-1209', '江苏省-徐州市-沛县', 1);
INSERT INTO `qmgx_region` VALUES (1210, '320312', '铜山区', 111, 3, 0, 'Tongshan Xian', '2', '1-11-111-1210', '江苏省-徐州市-铜山区', 1);
INSERT INTO `qmgx_region` VALUES (1211, '320324', '睢宁县', 111, 3, 0, 'Suining Xian', 'SNI', '1-11-111-1211', '江苏省-徐州市-睢宁县', 1);
INSERT INTO `qmgx_region` VALUES (1212, '320381', '新沂市', 111, 3, 0, 'Xinyi Shi', 'XYW', '1-11-111-1212', '江苏省-徐州市-新沂市', 1);
INSERT INTO `qmgx_region` VALUES (1213, '320382', '邳州市', 111, 3, 0, 'Pizhou Shi', 'PZO', '1-11-111-1213', '江苏省-徐州市-邳州市', 1);
INSERT INTO `qmgx_region` VALUES (1214, '320401', '市辖区', 112, 3, 0, 'Shixiaqu', '2', '1-11-112-1214', '江苏省-常州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1215, '320402', '天宁区', 112, 3, 0, 'Tianning Qu', 'TNQ', '1-11-112-1215', '江苏省-常州市-天宁区', 1);
INSERT INTO `qmgx_region` VALUES (1216, '320404', '钟楼区', 112, 3, 0, 'Zhonglou Qu', 'ZLQ', '1-11-112-1216', '江苏省-常州市-钟楼区', 1);
INSERT INTO `qmgx_region` VALUES (1217, '320405', '戚墅堰区', 112, 3, 0, 'Qishuyan Qu', 'QSY', '1-11-112-1217', '江苏省-常州市-戚墅堰区', 1);
INSERT INTO `qmgx_region` VALUES (1218, '320411', '新北区', 112, 3, 0, 'Xinbei Qu', '2', '1-11-112-1218', '江苏省-常州市-新北区', 1);
INSERT INTO `qmgx_region` VALUES (1219, '320412', '武进区', 112, 3, 0, 'Wujin Qu', '2', '1-11-112-1219', '江苏省-常州市-武进区', 1);
INSERT INTO `qmgx_region` VALUES (1220, '320481', '溧阳市', 112, 3, 0, 'Liyang Shi', 'LYR', '1-11-112-1220', '江苏省-常州市-溧阳市', 1);
INSERT INTO `qmgx_region` VALUES (1221, '320482', '金坛市', 112, 3, 0, 'Jintan Shi', 'JTS', '1-11-112-1221', '江苏省-常州市-金坛市', 1);
INSERT INTO `qmgx_region` VALUES (1222, '320501', '市辖区', 113, 3, 0, 'Shixiaqu', '2', '1-11-113-1222', '江苏省-苏州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1223, '320502', '沧浪区', 113, 3, 0, 'Canglang Qu', 'CLQ', '1-11-113-1223', '江苏省-苏州市-沧浪区', 1);
INSERT INTO `qmgx_region` VALUES (1224, '320503', '平江区', 113, 3, 0, 'Pingjiang Qu', 'PJQ', '1-11-113-1224', '江苏省-苏州市-平江区', 1);
INSERT INTO `qmgx_region` VALUES (1225, '320504', '金阊区', 113, 3, 0, 'Jinchang Qu', 'JCA', '1-11-113-1225', '江苏省-苏州市-金阊区', 1);
INSERT INTO `qmgx_region` VALUES (1226, '320505', '虎丘区', 113, 3, 0, 'Huqiu Qu', '2', '1-11-113-1226', '江苏省-苏州市-虎丘区', 1);
INSERT INTO `qmgx_region` VALUES (1227, '320506', '吴中区', 113, 3, 0, 'Wuzhong Qu', '2', '1-11-113-1227', '江苏省-苏州市-吴中区', 1);
INSERT INTO `qmgx_region` VALUES (1228, '320507', '相城区', 113, 3, 0, 'Xiangcheng Qu', '2', '1-11-113-1228', '江苏省-苏州市-相城区', 1);
INSERT INTO `qmgx_region` VALUES (1229, '320581', '常熟市', 113, 3, 0, 'Changshu Shi', 'CGS', '1-11-113-1229', '江苏省-苏州市-常熟市', 1);
INSERT INTO `qmgx_region` VALUES (1230, '320582', '张家港市', 113, 3, 0, 'Zhangjiagang Shi ', 'ZJG', '1-11-113-1230', '江苏省-苏州市-张家港市', 1);
INSERT INTO `qmgx_region` VALUES (1231, '320583', '昆山市', 113, 3, 0, 'Kunshan Shi', 'KUS', '1-11-113-1231', '江苏省-苏州市-昆山市', 1);
INSERT INTO `qmgx_region` VALUES (1232, '320584', '吴江市', 113, 3, 0, 'Wujiang Shi', 'WUJ', '1-11-113-1232', '江苏省-苏州市-吴江市', 1);
INSERT INTO `qmgx_region` VALUES (1233, '320585', '太仓市', 113, 3, 0, 'Taicang Shi', 'TAC', '1-11-113-1233', '江苏省-苏州市-太仓市', 1);
INSERT INTO `qmgx_region` VALUES (1234, '320601', '市辖区', 114, 3, 0, 'Shixiaqu', '2', '1-11-114-1234', '江苏省-南通市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1235, '320602', '崇川区', 114, 3, 0, 'Chongchuan Qu', 'CCQ', '1-11-114-1235', '江苏省-南通市-崇川区', 1);
INSERT INTO `qmgx_region` VALUES (1236, '320611', '港闸区', 114, 3, 0, 'Gangzha Qu', 'GZQ', '1-11-114-1236', '江苏省-南通市-港闸区', 1);
INSERT INTO `qmgx_region` VALUES (1237, '320621', '海安县', 114, 3, 0, 'Hai,an Xian', 'HIA', '1-11-114-1237', '江苏省-南通市-海安县', 1);
INSERT INTO `qmgx_region` VALUES (1238, '320623', '如东县', 114, 3, 0, 'Rudong Xian', 'RDG', '1-11-114-1238', '江苏省-南通市-如东县', 1);
INSERT INTO `qmgx_region` VALUES (1239, '320681', '启东市', 114, 3, 0, 'Qidong Shi', 'QID', '1-11-114-1239', '江苏省-南通市-启东市', 1);
INSERT INTO `qmgx_region` VALUES (1240, '320682', '如皋市', 114, 3, 0, 'Rugao Shi', 'RGO', '1-11-114-1240', '江苏省-南通市-如皋市', 1);
INSERT INTO `qmgx_region` VALUES (1241, '320612', '通州区', 114, 3, 0, 'Tongzhou Shi', '2', '1-11-114-1241', '江苏省-南通市-通州区', 1);
INSERT INTO `qmgx_region` VALUES (1242, '320684', '海门市', 114, 3, 0, 'Haimen Shi', 'HME', '1-11-114-1242', '江苏省-南通市-海门市', 1);
INSERT INTO `qmgx_region` VALUES (1243, '320701', '市辖区', 115, 3, 0, 'Shixiaqu', '2', '1-11-115-1243', '江苏省-连云港市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1244, '320703', '连云区', 115, 3, 0, 'Lianyun Qu', 'LYB', '1-11-115-1244', '江苏省-连云港市-连云区', 1);
INSERT INTO `qmgx_region` VALUES (1245, '320705', '新浦区', 115, 3, 0, 'Xinpu Qu', 'XPQ', '1-11-115-1245', '江苏省-连云港市-新浦区', 1);
INSERT INTO `qmgx_region` VALUES (1246, '320706', '海州区', 115, 3, 0, 'Haizhou Qu', 'HIZ', '1-11-115-1246', '江苏省-连云港市-海州区', 1);
INSERT INTO `qmgx_region` VALUES (1247, '320721', '赣榆县', 115, 3, 0, 'Ganyu Xian', 'GYU', '1-11-115-1247', '江苏省-连云港市-赣榆县', 1);
INSERT INTO `qmgx_region` VALUES (1248, '320722', '东海县', 115, 3, 0, 'Donghai Xian', 'DHX', '1-11-115-1248', '江苏省-连云港市-东海县', 1);
INSERT INTO `qmgx_region` VALUES (1249, '320723', '灌云县', 115, 3, 0, 'Guanyun Xian', 'GYS', '1-11-115-1249', '江苏省-连云港市-灌云县', 1);
INSERT INTO `qmgx_region` VALUES (1250, '320724', '灌南县', 115, 3, 0, 'Guannan Xian', 'GUN', '1-11-115-1250', '江苏省-连云港市-灌南县', 1);
INSERT INTO `qmgx_region` VALUES (1251, '320801', '市辖区', 116, 3, 0, 'Shixiaqu', '2', '1-11-116-1251', '江苏省-淮安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1252, '320802', '清河区', 116, 3, 0, 'Qinghe Qu', 'QHH', '1-11-116-1252', '江苏省-淮安市-清河区', 1);
INSERT INTO `qmgx_region` VALUES (1253, '320803', '楚州区', 116, 3, 0, 'Chuzhou Qu', '2', '1-11-116-1253', '江苏省-淮安市-楚州区', 1);
INSERT INTO `qmgx_region` VALUES (1254, '320804', '淮阴区', 116, 3, 0, 'Huaiyin Qu', '2', '1-11-116-1254', '江苏省-淮安市-淮阴区', 1);
INSERT INTO `qmgx_region` VALUES (1255, '320811', '清浦区', 116, 3, 0, 'Qingpu Qu', 'QPQ', '1-11-116-1255', '江苏省-淮安市-清浦区', 1);
INSERT INTO `qmgx_region` VALUES (1256, '320826', '涟水县', 116, 3, 0, 'Lianshui Xian', 'LSI', '1-11-116-1256', '江苏省-淮安市-涟水县', 1);
INSERT INTO `qmgx_region` VALUES (1257, '320829', '洪泽县', 116, 3, 0, 'Hongze Xian', 'HGZ', '1-11-116-1257', '江苏省-淮安市-洪泽县', 1);
INSERT INTO `qmgx_region` VALUES (1258, '320830', '盱眙县', 116, 3, 0, 'Xuyi Xian', 'XUY', '1-11-116-1258', '江苏省-淮安市-盱眙县', 1);
INSERT INTO `qmgx_region` VALUES (1259, '320831', '金湖县', 116, 3, 0, 'Jinhu Xian', 'JHU', '1-11-116-1259', '江苏省-淮安市-金湖县', 1);
INSERT INTO `qmgx_region` VALUES (1260, '320901', '市辖区', 117, 3, 0, 'Shixiaqu', '2', '1-11-117-1260', '江苏省-盐城市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1261, '320902', '亭湖区', 117, 3, 0, 'Tinghu Qu', '2', '1-11-117-1261', '江苏省-盐城市-亭湖区', 1);
INSERT INTO `qmgx_region` VALUES (1262, '320903', '盐都区', 117, 3, 0, 'Yandu Qu', '2', '1-11-117-1262', '江苏省-盐城市-盐都区', 1);
INSERT INTO `qmgx_region` VALUES (1263, '320921', '响水县', 117, 3, 0, 'Xiangshui Xian', 'XSH', '1-11-117-1263', '江苏省-盐城市-响水县', 1);
INSERT INTO `qmgx_region` VALUES (1264, '320922', '滨海县', 117, 3, 0, 'Binhai Xian', 'BHI', '1-11-117-1264', '江苏省-盐城市-滨海县', 1);
INSERT INTO `qmgx_region` VALUES (1265, '320923', '阜宁县', 117, 3, 0, 'Funing Xian', 'FNG', '1-11-117-1265', '江苏省-盐城市-阜宁县', 1);
INSERT INTO `qmgx_region` VALUES (1266, '320924', '射阳县', 117, 3, 0, 'Sheyang Xian', 'SEY', '1-11-117-1266', '江苏省-盐城市-射阳县', 1);
INSERT INTO `qmgx_region` VALUES (1267, '320925', '建湖县', 117, 3, 0, 'Jianhu Xian', 'JIH', '1-11-117-1267', '江苏省-盐城市-建湖县', 1);
INSERT INTO `qmgx_region` VALUES (1268, '320981', '东台市', 117, 3, 0, 'Dongtai Shi', 'DTS', '1-11-117-1268', '江苏省-盐城市-东台市', 1);
INSERT INTO `qmgx_region` VALUES (1269, '320982', '大丰市', 117, 3, 0, 'Dafeng Shi', 'DFS', '1-11-117-1269', '江苏省-盐城市-大丰市', 1);
INSERT INTO `qmgx_region` VALUES (1270, '321001', '市辖区', 118, 3, 0, 'Shixiaqu', '2', '1-11-118-1270', '江苏省-扬州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1271, '321002', '广陵区', 118, 3, 0, 'Guangling Qu', 'GGL', '1-11-118-1271', '江苏省-扬州市-广陵区', 1);
INSERT INTO `qmgx_region` VALUES (1272, '321003', '邗江区', 118, 3, 0, 'Hanjiang Qu', '2', '1-11-118-1272', '江苏省-扬州市-邗江区', 1);
INSERT INTO `qmgx_region` VALUES (1273, '321011', '维扬区', 118, 3, 0, 'Weiyang Qu', '2', '1-11-118-1273', '江苏省-扬州市-维扬区', 1);
INSERT INTO `qmgx_region` VALUES (1274, '321023', '宝应县', 118, 3, 0, 'Baoying Xian ', 'BYI', '1-11-118-1274', '江苏省-扬州市-宝应县', 1);
INSERT INTO `qmgx_region` VALUES (1275, '321081', '仪征市', 118, 3, 0, 'Yizheng Shi', 'YZE', '1-11-118-1275', '江苏省-扬州市-仪征市', 1);
INSERT INTO `qmgx_region` VALUES (1276, '321084', '高邮市', 118, 3, 0, 'Gaoyou Shi', 'GYO', '1-11-118-1276', '江苏省-扬州市-高邮市', 1);
INSERT INTO `qmgx_region` VALUES (1277, '321088', '江都市', 118, 3, 0, 'Jiangdu Shi', 'JDU', '1-11-118-1277', '江苏省-扬州市-江都市', 1);
INSERT INTO `qmgx_region` VALUES (1278, '321101', '市辖区', 119, 3, 0, 'Shixiaqu', '2', '1-11-119-1278', '江苏省-镇江市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1279, '321102', '京口区', 119, 3, 0, 'Jingkou Qu', '2', '1-11-119-1279', '江苏省-镇江市-京口区', 1);
INSERT INTO `qmgx_region` VALUES (1280, '321111', '润州区', 119, 3, 0, 'Runzhou Qu', 'RZQ', '1-11-119-1280', '江苏省-镇江市-润州区', 1);
INSERT INTO `qmgx_region` VALUES (1281, '321112', '丹徒区', 119, 3, 0, 'Dantu Qu', '2', '1-11-119-1281', '江苏省-镇江市-丹徒区', 1);
INSERT INTO `qmgx_region` VALUES (1282, '321181', '丹阳市', 119, 3, 0, 'Danyang Xian', 'DNY', '1-11-119-1282', '江苏省-镇江市-丹阳市', 1);
INSERT INTO `qmgx_region` VALUES (1283, '321182', '扬中市', 119, 3, 0, 'Yangzhong Shi', 'YZG', '1-11-119-1283', '江苏省-镇江市-扬中市', 1);
INSERT INTO `qmgx_region` VALUES (1284, '321183', '句容市', 119, 3, 0, 'Jurong Shi', 'JRG', '1-11-119-1284', '江苏省-镇江市-句容市', 1);
INSERT INTO `qmgx_region` VALUES (1285, '321201', '市辖区', 120, 3, 0, 'Shixiaqu', '2', '1-11-120-1285', '江苏省-泰州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1286, '321202', '海陵区', 120, 3, 0, 'Hailing Qu', 'HIL', '1-11-120-1286', '江苏省-泰州市-海陵区', 1);
INSERT INTO `qmgx_region` VALUES (1287, '321203', '高港区', 120, 3, 0, 'Gaogang Qu', 'GGQ', '1-11-120-1287', '江苏省-泰州市-高港区', 1);
INSERT INTO `qmgx_region` VALUES (1288, '321281', '兴化市', 120, 3, 0, 'Xinghua Shi', 'XHS', '1-11-120-1288', '江苏省-泰州市-兴化市', 1);
INSERT INTO `qmgx_region` VALUES (1289, '321282', '靖江市', 120, 3, 0, 'Jingjiang Shi', 'JGJ', '1-11-120-1289', '江苏省-泰州市-靖江市', 1);
INSERT INTO `qmgx_region` VALUES (1290, '321283', '泰兴市', 120, 3, 0, 'Taixing Shi', 'TXG', '1-11-120-1290', '江苏省-泰州市-泰兴市', 1);
INSERT INTO `qmgx_region` VALUES (1291, '321284', '姜堰市', 120, 3, 0, 'Jiangyan Shi', 'JYS', '1-11-120-1291', '江苏省-泰州市-姜堰市', 1);
INSERT INTO `qmgx_region` VALUES (1292, '321301', '市辖区', 121, 3, 0, 'Shixiaqu', '2', '1-11-121-1292', '江苏省-宿迁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1293, '321302', '宿城区', 121, 3, 0, 'Sucheng Qu', 'SCE', '1-11-121-1293', '江苏省-宿迁市-宿城区', 1);
INSERT INTO `qmgx_region` VALUES (1294, '321311', '宿豫区', 121, 3, 0, 'Suyu Qu', '2', '1-11-121-1294', '江苏省-宿迁市-宿豫区', 1);
INSERT INTO `qmgx_region` VALUES (1295, '321322', '沭阳县', 121, 3, 0, 'Shuyang Xian', 'SYD', '1-11-121-1295', '江苏省-宿迁市-沭阳县', 1);
INSERT INTO `qmgx_region` VALUES (1296, '321323', '泗阳县', 121, 3, 0, 'Siyang Xian ', 'SIY', '1-11-121-1296', '江苏省-宿迁市-泗阳县', 1);
INSERT INTO `qmgx_region` VALUES (1297, '321324', '泗洪县', 121, 3, 0, 'Sihong Xian', 'SIH', '1-11-121-1297', '江苏省-宿迁市-泗洪县', 1);
INSERT INTO `qmgx_region` VALUES (1298, '330101', '市辖区', 122, 3, 0, 'Shixiaqu', '2', '1-12-122-1298', '浙江省-杭州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1299, '330102', '上城区', 122, 3, 0, 'Shangcheng Qu', 'SCQ', '1-12-122-1299', '浙江省-杭州市-上城区', 1);
INSERT INTO `qmgx_region` VALUES (1300, '330103', '下城区', 122, 3, 0, 'Xiacheng Qu', 'XCG', '1-12-122-1300', '浙江省-杭州市-下城区', 1);
INSERT INTO `qmgx_region` VALUES (1301, '330104', '江干区', 122, 3, 0, 'Jianggan Qu', 'JGQ', '1-12-122-1301', '浙江省-杭州市-江干区', 1);
INSERT INTO `qmgx_region` VALUES (1302, '330105', '拱墅区', 122, 3, 0, 'Gongshu Qu', 'GSQ', '1-12-122-1302', '浙江省-杭州市-拱墅区', 1);
INSERT INTO `qmgx_region` VALUES (1303, '330106', '西湖区', 122, 3, 0, 'Xihu Qu ', 'XHU', '1-12-122-1303', '浙江省-杭州市-西湖区', 1);
INSERT INTO `qmgx_region` VALUES (1304, '330108', '滨江区', 122, 3, 0, 'Binjiang Qu', 'BJQ', '1-12-122-1304', '浙江省-杭州市-滨江区', 1);
INSERT INTO `qmgx_region` VALUES (1305, '330109', '萧山区', 122, 3, 0, 'Xiaoshan Qu', '2', '1-12-122-1305', '浙江省-杭州市-萧山区', 1);
INSERT INTO `qmgx_region` VALUES (1306, '330110', '余杭区', 122, 3, 0, 'Yuhang Qu', '2', '1-12-122-1306', '浙江省-杭州市-余杭区', 1);
INSERT INTO `qmgx_region` VALUES (1307, '330122', '桐庐县', 122, 3, 0, 'Tonglu Xian', 'TLU', '1-12-122-1307', '浙江省-杭州市-桐庐县', 1);
INSERT INTO `qmgx_region` VALUES (1308, '330127', '淳安县', 122, 3, 0, 'Chun,an Xian', 'CAZ', '1-12-122-1308', '浙江省-杭州市-淳安县', 1);
INSERT INTO `qmgx_region` VALUES (1309, '330182', '建德市', 122, 3, 0, 'Jiande Shi', 'JDS', '1-12-122-1309', '浙江省-杭州市-建德市', 1);
INSERT INTO `qmgx_region` VALUES (1310, '330183', '富阳市', 122, 3, 0, 'Fuyang Shi', 'FYZ', '1-12-122-1310', '浙江省-杭州市-富阳市', 1);
INSERT INTO `qmgx_region` VALUES (1311, '330185', '临安市', 122, 3, 0, 'Lin,an Shi', 'LNA', '1-12-122-1311', '浙江省-杭州市-临安市', 1);
INSERT INTO `qmgx_region` VALUES (1312, '330201', '市辖区', 123, 3, 0, 'Shixiaqu', '2', '1-12-123-1312', '浙江省-宁波市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1313, '330203', '海曙区', 123, 3, 0, 'Haishu Qu', 'HNB', '1-12-123-1313', '浙江省-宁波市-海曙区', 1);
INSERT INTO `qmgx_region` VALUES (1314, '330204', '江东区', 123, 3, 0, 'Jiangdong Qu', 'JDO', '1-12-123-1314', '浙江省-宁波市-江东区', 1);
INSERT INTO `qmgx_region` VALUES (1315, '330205', '江北区', 123, 3, 0, 'Jiangbei Qu', 'JBQ', '1-12-123-1315', '浙江省-宁波市-江北区', 1);
INSERT INTO `qmgx_region` VALUES (1316, '330206', '北仑区', 123, 3, 0, 'Beilun Qu', 'BLN', '1-12-123-1316', '浙江省-宁波市-北仑区', 1);
INSERT INTO `qmgx_region` VALUES (1317, '330211', '镇海区', 123, 3, 0, 'Zhenhai Qu', 'ZHF', '1-12-123-1317', '浙江省-宁波市-镇海区', 1);
INSERT INTO `qmgx_region` VALUES (1318, '330212', '鄞州区', 123, 3, 0, 'Yinzhou Qu', '2', '1-12-123-1318', '浙江省-宁波市-鄞州区', 1);
INSERT INTO `qmgx_region` VALUES (1319, '330225', '象山县', 123, 3, 0, 'Xiangshan Xian', 'YSZ', '1-12-123-1319', '浙江省-宁波市-象山县', 1);
INSERT INTO `qmgx_region` VALUES (1320, '330226', '宁海县', 123, 3, 0, 'Ninghai Xian', 'NHI', '1-12-123-1320', '浙江省-宁波市-宁海县', 1);
INSERT INTO `qmgx_region` VALUES (1321, '330281', '余姚市', 123, 3, 0, 'Yuyao Shi', 'YYO', '1-12-123-1321', '浙江省-宁波市-余姚市', 1);
INSERT INTO `qmgx_region` VALUES (1322, '330282', '慈溪市', 123, 3, 0, 'Cixi Shi', 'CXI', '1-12-123-1322', '浙江省-宁波市-慈溪市', 1);
INSERT INTO `qmgx_region` VALUES (1323, '330283', '奉化市', 123, 3, 0, 'Fenghua Shi', 'FHU', '1-12-123-1323', '浙江省-宁波市-奉化市', 1);
INSERT INTO `qmgx_region` VALUES (1324, '330301', '市辖区', 124, 3, 0, 'Shixiaqu', '2', '1-12-124-1324', '浙江省-温州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1325, '330302', '鹿城区', 124, 3, 0, 'Lucheng Qu', 'LUW', '1-12-124-1325', '浙江省-温州市-鹿城区', 1);
INSERT INTO `qmgx_region` VALUES (1326, '330303', '龙湾区', 124, 3, 0, 'Longwan Qu', 'LWW', '1-12-124-1326', '浙江省-温州市-龙湾区', 1);
INSERT INTO `qmgx_region` VALUES (1327, '330304', '瓯海区', 124, 3, 0, 'Ouhai Qu', 'OHQ', '1-12-124-1327', '浙江省-温州市-瓯海区', 1);
INSERT INTO `qmgx_region` VALUES (1328, '330322', '洞头县', 124, 3, 0, 'Dongtou Xian', 'DTO', '1-12-124-1328', '浙江省-温州市-洞头县', 1);
INSERT INTO `qmgx_region` VALUES (1329, '330324', '永嘉县', 124, 3, 0, 'Yongjia Xian', 'YJX', '1-12-124-1329', '浙江省-温州市-永嘉县', 1);
INSERT INTO `qmgx_region` VALUES (1330, '330326', '平阳县', 124, 3, 0, 'Pingyang Xian', 'PYG', '1-12-124-1330', '浙江省-温州市-平阳县', 1);
INSERT INTO `qmgx_region` VALUES (1331, '330327', '苍南县', 124, 3, 0, 'Cangnan Xian', 'CAN', '1-12-124-1331', '浙江省-温州市-苍南县', 1);
INSERT INTO `qmgx_region` VALUES (1332, '330328', '文成县', 124, 3, 0, 'Wencheng Xian', 'WCZ', '1-12-124-1332', '浙江省-温州市-文成县', 1);
INSERT INTO `qmgx_region` VALUES (1333, '330329', '泰顺县', 124, 3, 0, 'Taishun Xian', 'TSZ', '1-12-124-1333', '浙江省-温州市-泰顺县', 1);
INSERT INTO `qmgx_region` VALUES (1334, '330381', '瑞安市', 124, 3, 0, 'Rui,an Xian', 'RAS', '1-12-124-1334', '浙江省-温州市-瑞安市', 1);
INSERT INTO `qmgx_region` VALUES (1335, '330382', '乐清市', 124, 3, 0, 'Yueqing Shi', 'YQZ', '1-12-124-1335', '浙江省-温州市-乐清市', 1);
INSERT INTO `qmgx_region` VALUES (1336, '330401', '市辖区', 125, 3, 0, 'Shixiaqu', '2', '1-12-125-1336', '浙江省-嘉兴市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1338, '330411', '秀洲区', 125, 3, 0, 'Xiuzhou Qu', '2', '1-12-125-1338', '浙江省-嘉兴市-秀洲区', 1);
INSERT INTO `qmgx_region` VALUES (1339, '330421', '嘉善县', 125, 3, 0, 'Jiashan Xian', 'JSK', '1-12-125-1339', '浙江省-嘉兴市-嘉善县', 1);
INSERT INTO `qmgx_region` VALUES (1340, '330424', '海盐县', 125, 3, 0, 'Haiyan Xian', 'HYN', '1-12-125-1340', '浙江省-嘉兴市-海盐县', 1);
INSERT INTO `qmgx_region` VALUES (1341, '330481', '海宁市', 125, 3, 0, 'Haining Shi', 'HNG', '1-12-125-1341', '浙江省-嘉兴市-海宁市', 1);
INSERT INTO `qmgx_region` VALUES (1342, '330482', '平湖市', 125, 3, 0, 'Pinghu Shi', 'PHU', '1-12-125-1342', '浙江省-嘉兴市-平湖市', 1);
INSERT INTO `qmgx_region` VALUES (1343, '330483', '桐乡市', 125, 3, 0, 'Tongxiang Shi', 'TXZ', '1-12-125-1343', '浙江省-嘉兴市-桐乡市', 1);
INSERT INTO `qmgx_region` VALUES (1344, '330501', '市辖区', 126, 3, 0, 'Shixiaqu', '2', '1-12-126-1344', '浙江省-湖州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1345, '330502', '吴兴区', 126, 3, 0, 'Wuxing Qu', '2', '1-12-126-1345', '浙江省-湖州市-吴兴区', 1);
INSERT INTO `qmgx_region` VALUES (1346, '330503', '南浔区', 126, 3, 0, 'Nanxun Qu', '2', '1-12-126-1346', '浙江省-湖州市-南浔区', 1);
INSERT INTO `qmgx_region` VALUES (1347, '330521', '德清县', 126, 3, 0, 'Deqing Xian', 'DQX', '1-12-126-1347', '浙江省-湖州市-德清县', 1);
INSERT INTO `qmgx_region` VALUES (1348, '330522', '长兴县', 126, 3, 0, 'Changxing Xian', 'CXG', '1-12-126-1348', '浙江省-湖州市-长兴县', 1);
INSERT INTO `qmgx_region` VALUES (1349, '330523', '安吉县', 126, 3, 0, 'Anji Xian', 'AJI', '1-12-126-1349', '浙江省-湖州市-安吉县', 1);
INSERT INTO `qmgx_region` VALUES (1350, '330601', '市辖区', 127, 3, 0, 'Shixiaqu', '2', '1-12-127-1350', '浙江省-绍兴市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1351, '330602', '越城区', 127, 3, 0, 'Yuecheng Qu', 'YSX', '1-12-127-1351', '浙江省-绍兴市-越城区', 1);
INSERT INTO `qmgx_region` VALUES (1352, '330621', '绍兴县', 127, 3, 0, 'Shaoxing Xian', 'SXZ', '1-12-127-1352', '浙江省-绍兴市-绍兴县', 1);
INSERT INTO `qmgx_region` VALUES (1353, '330624', '新昌县', 127, 3, 0, 'Xinchang Xian', 'XCX', '1-12-127-1353', '浙江省-绍兴市-新昌县', 1);
INSERT INTO `qmgx_region` VALUES (1354, '330681', '诸暨市', 127, 3, 0, 'Zhuji Shi', 'ZHJ', '1-12-127-1354', '浙江省-绍兴市-诸暨市', 1);
INSERT INTO `qmgx_region` VALUES (1355, '330682', '上虞市', 127, 3, 0, 'Shangyu Shi', 'SYZ', '1-12-127-1355', '浙江省-绍兴市-上虞市', 1);
INSERT INTO `qmgx_region` VALUES (1356, '330683', '嵊州市', 127, 3, 0, 'Shengzhou Shi', 'SGZ', '1-12-127-1356', '浙江省-绍兴市-嵊州市', 1);
INSERT INTO `qmgx_region` VALUES (1357, '330701', '市辖区', 128, 3, 0, 'Shixiaqu', '2', '1-12-128-1357', '浙江省-金华市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1358, '330702', '婺城区', 128, 3, 0, 'Wucheng Qu', 'WCF', '1-12-128-1358', '浙江省-金华市-婺城区', 1);
INSERT INTO `qmgx_region` VALUES (1359, '330703', '金东区', 128, 3, 0, 'Jindong Qu', '2', '1-12-128-1359', '浙江省-金华市-金东区', 1);
INSERT INTO `qmgx_region` VALUES (1360, '330723', '武义县', 128, 3, 0, 'Wuyi Xian', 'WYX', '1-12-128-1360', '浙江省-金华市-武义县', 1);
INSERT INTO `qmgx_region` VALUES (1361, '330726', '浦江县', 128, 3, 0, 'Pujiang Xian ', 'PJG', '1-12-128-1361', '浙江省-金华市-浦江县', 1);
INSERT INTO `qmgx_region` VALUES (1362, '330727', '磐安县', 128, 3, 0, 'Pan,an Xian', 'PAX', '1-12-128-1362', '浙江省-金华市-磐安县', 1);
INSERT INTO `qmgx_region` VALUES (1363, '330781', '兰溪市', 128, 3, 0, 'Lanxi Shi', 'LXZ', '1-12-128-1363', '浙江省-金华市-兰溪市', 1);
INSERT INTO `qmgx_region` VALUES (1364, '330782', '义乌市', 128, 3, 0, 'Yiwu Shi', 'YWS', '1-12-128-1364', '浙江省-金华市-义乌市', 1);
INSERT INTO `qmgx_region` VALUES (1365, '330783', '东阳市', 128, 3, 0, 'Dongyang Shi', 'DGY', '1-12-128-1365', '浙江省-金华市-东阳市', 1);
INSERT INTO `qmgx_region` VALUES (1366, '330784', '永康市', 128, 3, 0, 'Yongkang Shi', 'YKG', '1-12-128-1366', '浙江省-金华市-永康市', 1);
INSERT INTO `qmgx_region` VALUES (1367, '330801', '市辖区', 129, 3, 0, 'Shixiaqu', '2', '1-12-129-1367', '浙江省-衢州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1368, '330802', '柯城区', 129, 3, 0, 'Kecheng Qu', 'KEC', '1-12-129-1368', '浙江省-衢州市-柯城区', 1);
INSERT INTO `qmgx_region` VALUES (1369, '330803', '衢江区', 129, 3, 0, 'Qujiang Qu', '2', '1-12-129-1369', '浙江省-衢州市-衢江区', 1);
INSERT INTO `qmgx_region` VALUES (1370, '330822', '常山县', 129, 3, 0, 'Changshan Xian', 'CSN', '1-12-129-1370', '浙江省-衢州市-常山县', 1);
INSERT INTO `qmgx_region` VALUES (1371, '330824', '开化县', 129, 3, 0, 'Kaihua Xian', 'KHU', '1-12-129-1371', '浙江省-衢州市-开化县', 1);
INSERT INTO `qmgx_region` VALUES (1372, '330825', '龙游县', 129, 3, 0, 'Longyou Xian ', 'LGY', '1-12-129-1372', '浙江省-衢州市-龙游县', 1);
INSERT INTO `qmgx_region` VALUES (1373, '330881', '江山市', 129, 3, 0, 'Jiangshan Shi', 'JIS', '1-12-129-1373', '浙江省-衢州市-江山市', 1);
INSERT INTO `qmgx_region` VALUES (1374, '330901', '市辖区', 130, 3, 0, 'Shixiaqu', '2', '1-12-130-1374', '浙江省-舟山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1375, '330902', '定海区', 130, 3, 0, 'Dinghai Qu', 'DHQ', '1-12-130-1375', '浙江省-舟山市-定海区', 1);
INSERT INTO `qmgx_region` VALUES (1376, '330903', '普陀区', 130, 3, 0, 'Putuo Qu', 'PTO', '1-12-130-1376', '浙江省-舟山市-普陀区', 1);
INSERT INTO `qmgx_region` VALUES (1377, '330921', '岱山县', 130, 3, 0, 'Daishan Xian', 'DSH', '1-12-130-1377', '浙江省-舟山市-岱山县', 1);
INSERT INTO `qmgx_region` VALUES (1378, '330922', '嵊泗县', 130, 3, 0, 'Shengsi Xian', 'SSZ', '1-12-130-1378', '浙江省-舟山市-嵊泗县', 1);
INSERT INTO `qmgx_region` VALUES (1379, '331001', '市辖区', 131, 3, 0, 'Shixiaqu', '2', '1-12-131-1379', '浙江省-台州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1380, '331002', '椒江区', 131, 3, 0, 'Jiaojiang Qu', 'JJT', '1-12-131-1380', '浙江省-台州市-椒江区', 1);
INSERT INTO `qmgx_region` VALUES (1381, '331003', '黄岩区', 131, 3, 0, 'Huangyan Qu', 'HYT', '1-12-131-1381', '浙江省-台州市-黄岩区', 1);
INSERT INTO `qmgx_region` VALUES (1382, '331004', '路桥区', 131, 3, 0, 'Luqiao Qu', 'LQT', '1-12-131-1382', '浙江省-台州市-路桥区', 1);
INSERT INTO `qmgx_region` VALUES (1383, '331021', '玉环县', 131, 3, 0, 'Yuhuan Xian', 'YHN', '1-12-131-1383', '浙江省-台州市-玉环县', 1);
INSERT INTO `qmgx_region` VALUES (1384, '331022', '三门县', 131, 3, 0, 'Sanmen Xian', 'SMN', '1-12-131-1384', '浙江省-台州市-三门县', 1);
INSERT INTO `qmgx_region` VALUES (1385, '331023', '天台县', 131, 3, 0, 'Tiantai Xian', 'TTA', '1-12-131-1385', '浙江省-台州市-天台县', 1);
INSERT INTO `qmgx_region` VALUES (1386, '331024', '仙居县', 131, 3, 0, 'Xianju Xian', 'XJU', '1-12-131-1386', '浙江省-台州市-仙居县', 1);
INSERT INTO `qmgx_region` VALUES (1387, '331081', '温岭市', 131, 3, 0, 'Wenling Shi', 'WLS', '1-12-131-1387', '浙江省-台州市-温岭市', 1);
INSERT INTO `qmgx_region` VALUES (1388, '331082', '临海市', 131, 3, 0, 'Linhai Shi', 'LHI', '1-12-131-1388', '浙江省-台州市-临海市', 1);
INSERT INTO `qmgx_region` VALUES (1389, '331101', '市辖区', 132, 3, 0, '1', '2', '1-12-132-1389', '浙江省-丽水市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1390, '331102', '莲都区', 132, 3, 0, 'Liandu Qu', '2', '1-12-132-1390', '浙江省-丽水市-莲都区', 1);
INSERT INTO `qmgx_region` VALUES (1391, '331121', '青田县', 132, 3, 0, 'Qingtian Xian', '2', '1-12-132-1391', '浙江省-丽水市-青田县', 1);
INSERT INTO `qmgx_region` VALUES (1392, '331122', '缙云县', 132, 3, 0, 'Jinyun Xian', '2', '1-12-132-1392', '浙江省-丽水市-缙云县', 1);
INSERT INTO `qmgx_region` VALUES (1393, '331123', '遂昌县', 132, 3, 0, 'Suichang Xian', '2', '1-12-132-1393', '浙江省-丽水市-遂昌县', 1);
INSERT INTO `qmgx_region` VALUES (1394, '331124', '松阳县', 132, 3, 0, 'Songyang Xian', '2', '1-12-132-1394', '浙江省-丽水市-松阳县', 1);
INSERT INTO `qmgx_region` VALUES (1395, '331125', '云和县', 132, 3, 0, 'Yunhe Xian', '2', '1-12-132-1395', '浙江省-丽水市-云和县', 1);
INSERT INTO `qmgx_region` VALUES (1396, '331126', '庆元县', 132, 3, 0, 'Qingyuan Xian', '2', '1-12-132-1396', '浙江省-丽水市-庆元县', 1);
INSERT INTO `qmgx_region` VALUES (1397, '331127', '景宁畲族自治县', 132, 3, 0, 'Jingning Shezu Zizhixian', '2', '1-12-132-1397', '浙江省-丽水市-景宁畲族自治县', 1);
INSERT INTO `qmgx_region` VALUES (1398, '331181', '龙泉市', 132, 3, 0, 'Longquan Shi', '2', '1-12-132-1398', '浙江省-丽水市-龙泉市', 1);
INSERT INTO `qmgx_region` VALUES (1399, '340101', '市辖区', 133, 3, 0, 'Shixiaqu', '2', '1-13-133-1399', '安徽省-合肥市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1400, '340102', '瑶海区', 133, 3, 0, 'Yaohai Qu', '2', '1-13-133-1400', '安徽省-合肥市-瑶海区', 1);
INSERT INTO `qmgx_region` VALUES (1401, '340103', '庐阳区', 133, 3, 0, 'Luyang Qu', '2', '1-13-133-1401', '安徽省-合肥市-庐阳区', 1);
INSERT INTO `qmgx_region` VALUES (1402, '340104', '蜀山区', 133, 3, 0, 'Shushan Qu', '2', '1-13-133-1402', '安徽省-合肥市-蜀山区', 1);
INSERT INTO `qmgx_region` VALUES (1403, '340111', '包河区', 133, 3, 0, 'Baohe Qu', '2', '1-13-133-1403', '安徽省-合肥市-包河区', 1);
INSERT INTO `qmgx_region` VALUES (1404, '340121', '长丰县', 133, 3, 0, 'Changfeng Xian', 'CFG', '1-13-133-1404', '安徽省-合肥市-长丰县', 1);
INSERT INTO `qmgx_region` VALUES (1405, '340122', '肥东县', 133, 3, 0, 'Feidong Xian', 'FDO', '1-13-133-1405', '安徽省-合肥市-肥东县', 1);
INSERT INTO `qmgx_region` VALUES (1406, '340123', '肥西县', 133, 3, 0, 'Feixi Xian', 'FIX', '1-13-133-1406', '安徽省-合肥市-肥西县', 1);
INSERT INTO `qmgx_region` VALUES (1407, '340201', '市辖区', 1412, 3, 0, 'Shixiaqu', '2', '-1407', '-芜湖市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1408, '340202', '镜湖区', 1412, 3, 0, 'Jinghu Qu', 'JHW', '-1408', '-芜湖市-镜湖区', 1);
INSERT INTO `qmgx_region` VALUES (1409, '340208', '三山区', 1412, 3, 0, 'Matang Qu', '2', '-1409', '-芜湖市-三山区', 1);
INSERT INTO `qmgx_region` VALUES (1410, '340203', '弋江区', 1412, 3, 0, 'Xinwu Qu', '2', '-1410', '-芜湖市-弋江区', 1);
INSERT INTO `qmgx_region` VALUES (1411, '340207', '鸠江区', 1412, 3, 0, 'Jiujiang Qu', 'JJW', '-1411', '-芜湖市-鸠江区', 1);
INSERT INTO `qmgx_region` VALUES (1412, '340200', '芜湖市', 134, 3, 0, 'Wuhu Shi', 'WHI', '1-13-134-1412', '安徽省-芜湖市-芜湖市', 1);
INSERT INTO `qmgx_region` VALUES (1413, '340222', '繁昌县', 1412, 3, 0, 'Fanchang Xian', 'FCH', '-1413', '-芜湖市-繁昌县', 1);
INSERT INTO `qmgx_region` VALUES (1414, '340223', '南陵县', 1412, 3, 0, 'Nanling Xian', 'NLX', '-1414', '-芜湖市-南陵县', 1);
INSERT INTO `qmgx_region` VALUES (1415, '340301', '市辖区', 135, 3, 0, 'Shixiaqu', '2', '1-13-135-1415', '安徽省-蚌埠市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1416, '340302', '龙子湖区', 135, 3, 0, 'Longzihu Qu', '2', '1-13-135-1416', '安徽省-蚌埠市-龙子湖区', 1);
INSERT INTO `qmgx_region` VALUES (1417, '340303', '蚌山区', 135, 3, 0, 'Bangshan Qu', '2', '1-13-135-1417', '安徽省-蚌埠市-蚌山区', 1);
INSERT INTO `qmgx_region` VALUES (1418, '340304', '禹会区', 135, 3, 0, 'Yuhui Qu', '2', '1-13-135-1418', '安徽省-蚌埠市-禹会区', 1);
INSERT INTO `qmgx_region` VALUES (1419, '340311', '淮上区', 135, 3, 0, 'Huaishang Qu', '2', '1-13-135-1419', '安徽省-蚌埠市-淮上区', 1);
INSERT INTO `qmgx_region` VALUES (1420, '340321', '怀远县', 135, 3, 0, 'Huaiyuan Qu', 'HYW', '1-13-135-1420', '安徽省-蚌埠市-怀远县', 1);
INSERT INTO `qmgx_region` VALUES (1421, '340322', '五河县', 135, 3, 0, 'Wuhe Xian', 'WHE', '1-13-135-1421', '安徽省-蚌埠市-五河县', 1);
INSERT INTO `qmgx_region` VALUES (1422, '340323', '固镇县', 135, 3, 0, 'Guzhen Xian', 'GZX', '1-13-135-1422', '安徽省-蚌埠市-固镇县', 1);
INSERT INTO `qmgx_region` VALUES (1423, '340401', '市辖区', 136, 3, 0, 'Shixiaqu', '2', '1-13-136-1423', '安徽省-淮南市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1424, '340402', '大通区', 136, 3, 0, 'Datong Qu', 'DTQ', '1-13-136-1424', '安徽省-淮南市-大通区', 1);
INSERT INTO `qmgx_region` VALUES (1425, '340403', '田家庵区', 136, 3, 0, 'Tianjia,an Qu', 'TJA', '1-13-136-1425', '安徽省-淮南市-田家庵区', 1);
INSERT INTO `qmgx_region` VALUES (1426, '340404', '谢家集区', 136, 3, 0, 'Xiejiaji Qu', 'XJJ', '1-13-136-1426', '安徽省-淮南市-谢家集区', 1);
INSERT INTO `qmgx_region` VALUES (1427, '340405', '八公山区', 136, 3, 0, 'Bagongshan Qu', 'BGS', '1-13-136-1427', '安徽省-淮南市-八公山区', 1);
INSERT INTO `qmgx_region` VALUES (1428, '340406', '潘集区', 136, 3, 0, 'Panji Qu', 'PJI', '1-13-136-1428', '安徽省-淮南市-潘集区', 1);
INSERT INTO `qmgx_region` VALUES (1429, '340421', '凤台县', 136, 3, 0, 'Fengtai Xian', '2', '1-13-136-1429', '安徽省-淮南市-凤台县', 1);
INSERT INTO `qmgx_region` VALUES (1430, '340501', '市辖区', 137, 3, 0, 'Shixiaqu', '2', '1-13-137-1430', '安徽省-马鞍山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1431, '340502', '金家庄区', 137, 3, 0, 'Jinjiazhuang Qu', 'JJZ', '1-13-137-1431', '安徽省-马鞍山市-金家庄区', 1);
INSERT INTO `qmgx_region` VALUES (1432, '340503', '花山区', 137, 3, 0, 'Huashan Qu', 'HSM', '1-13-137-1432', '安徽省-马鞍山市-花山区', 1);
INSERT INTO `qmgx_region` VALUES (1433, '340504', '雨山区', 137, 3, 0, 'Yushan Qu', 'YSQ', '1-13-137-1433', '安徽省-马鞍山市-雨山区', 1);
INSERT INTO `qmgx_region` VALUES (1434, '340521', '当涂县', 137, 3, 0, 'Dangtu Xian', 'DTU', '1-13-137-1434', '安徽省-马鞍山市-当涂县', 1);
INSERT INTO `qmgx_region` VALUES (1435, '340601', '市辖区', 138, 3, 0, 'Shixiaqu', '2', '1-13-138-1435', '安徽省-淮北市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1436, '340602', '杜集区', 138, 3, 0, 'Duji Qu', 'DJQ', '1-13-138-1436', '安徽省-淮北市-杜集区', 1);
INSERT INTO `qmgx_region` VALUES (1437, '340603', '相山区', 138, 3, 0, 'Xiangshan Qu', 'XSA', '1-13-138-1437', '安徽省-淮北市-相山区', 1);
INSERT INTO `qmgx_region` VALUES (1438, '340604', '烈山区', 138, 3, 0, 'Lieshan Qu', 'LHB', '1-13-138-1438', '安徽省-淮北市-烈山区', 1);
INSERT INTO `qmgx_region` VALUES (1439, '340621', '濉溪县', 138, 3, 0, 'Suixi Xian', 'SXW', '1-13-138-1439', '安徽省-淮北市-濉溪县', 1);
INSERT INTO `qmgx_region` VALUES (1440, '340701', '市辖区', 139, 3, 0, 'Shixiaqu', '2', '1-13-139-1440', '安徽省-铜陵市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1441, '340702', '铜官山区', 139, 3, 0, 'Tongguanshan Qu', 'TGQ', '1-13-139-1441', '安徽省-铜陵市-铜官山区', 1);
INSERT INTO `qmgx_region` VALUES (1442, '340703', '狮子山区', 139, 3, 0, 'Shizishan Qu', 'SZN', '1-13-139-1442', '安徽省-铜陵市-狮子山区', 1);
INSERT INTO `qmgx_region` VALUES (1443, '340711', '郊区', 139, 3, 0, 'Jiaoqu', 'JTL', '1-13-139-1443', '安徽省-铜陵市-郊区', 1);
INSERT INTO `qmgx_region` VALUES (1444, '340721', '铜陵县', 139, 3, 0, 'Tongling Xian', 'TLX', '1-13-139-1444', '安徽省-铜陵市-铜陵县', 1);
INSERT INTO `qmgx_region` VALUES (1445, '340801', '市辖区', 140, 3, 0, 'Shixiaqu', '2', '1-13-140-1445', '安徽省-安庆市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1446, '340802', '迎江区', 140, 3, 0, 'Yingjiang Qu', 'YJQ', '1-13-140-1446', '安徽省-安庆市-迎江区', 1);
INSERT INTO `qmgx_region` VALUES (1447, '340803', '大观区', 140, 3, 0, 'Daguan Qu', 'DGQ', '1-13-140-1447', '安徽省-安庆市-大观区', 1);
INSERT INTO `qmgx_region` VALUES (1448, '340811', '宜秀区', 140, 3, 0, 'Yixiu Qu', '2', '1-13-140-1448', '安徽省-安庆市-宜秀区', 1);
INSERT INTO `qmgx_region` VALUES (1449, '340822', '怀宁县', 140, 3, 0, 'Huaining Xian', 'HNW', '1-13-140-1449', '安徽省-安庆市-怀宁县', 1);
INSERT INTO `qmgx_region` VALUES (1450, '340823', '枞阳县', 140, 3, 0, 'Zongyang Xian', 'ZYW', '1-13-140-1450', '安徽省-安庆市-枞阳县', 1);
INSERT INTO `qmgx_region` VALUES (1451, '340824', '潜山县', 140, 3, 0, 'Qianshan Xian', 'QSW', '1-13-140-1451', '安徽省-安庆市-潜山县', 1);
INSERT INTO `qmgx_region` VALUES (1452, '340825', '太湖县', 140, 3, 0, 'Taihu Xian', 'THU', '1-13-140-1452', '安徽省-安庆市-太湖县', 1);
INSERT INTO `qmgx_region` VALUES (1453, '340826', '宿松县', 140, 3, 0, 'Susong Xian', 'SUS', '1-13-140-1453', '安徽省-安庆市-宿松县', 1);
INSERT INTO `qmgx_region` VALUES (1454, '340827', '望江县', 140, 3, 0, 'Wangjiang Xian', 'WJX', '1-13-140-1454', '安徽省-安庆市-望江县', 1);
INSERT INTO `qmgx_region` VALUES (1455, '340828', '岳西县', 140, 3, 0, 'Yuexi Xian', 'YXW', '1-13-140-1455', '安徽省-安庆市-岳西县', 1);
INSERT INTO `qmgx_region` VALUES (1456, '340881', '桐城市', 140, 3, 0, 'Tongcheng Shi', 'TCW', '1-13-140-1456', '安徽省-安庆市-桐城市', 1);
INSERT INTO `qmgx_region` VALUES (1457, '341001', '市辖区', 141, 3, 0, 'Shixiaqu', '2', '1-13-141-1457', '安徽省-黄山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1458, '341002', '屯溪区', 141, 3, 0, 'Tunxi Qu', 'TXN', '1-13-141-1458', '安徽省-黄山市-屯溪区', 1);
INSERT INTO `qmgx_region` VALUES (1459, '341003', '黄山区', 141, 3, 0, 'Huangshan Qu', 'HSK', '1-13-141-1459', '安徽省-黄山市-黄山区', 1);
INSERT INTO `qmgx_region` VALUES (1460, '341004', '徽州区', 141, 3, 0, 'Huizhou Qu', 'HZQ', '1-13-141-1460', '安徽省-黄山市-徽州区', 1);
INSERT INTO `qmgx_region` VALUES (1461, '341021', '歙县', 141, 3, 0, 'She Xian', 'SEX', '1-13-141-1461', '安徽省-黄山市-歙县', 1);
INSERT INTO `qmgx_region` VALUES (1462, '341022', '休宁县', 141, 3, 0, 'Xiuning Xian', 'XUN', '1-13-141-1462', '安徽省-黄山市-休宁县', 1);
INSERT INTO `qmgx_region` VALUES (1463, '341023', '黟县', 141, 3, 0, 'Yi Xian', 'YIW', '1-13-141-1463', '安徽省-黄山市-黟县', 1);
INSERT INTO `qmgx_region` VALUES (1464, '341024', '祁门县', 141, 3, 0, 'Qimen Xian', 'QMN', '1-13-141-1464', '安徽省-黄山市-祁门县', 1);
INSERT INTO `qmgx_region` VALUES (1465, '341101', '市辖区', 142, 3, 0, 'Shixiaqu', '2', '1-13-142-1465', '安徽省-滁州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1466, '341102', '琅琊区', 142, 3, 0, 'Langya Qu', 'LYU', '1-13-142-1466', '安徽省-滁州市-琅琊区', 1);
INSERT INTO `qmgx_region` VALUES (1467, '341103', '南谯区', 142, 3, 0, 'Nanqiao Qu', 'NQQ', '1-13-142-1467', '安徽省-滁州市-南谯区', 1);
INSERT INTO `qmgx_region` VALUES (1468, '341122', '来安县', 142, 3, 0, 'Lai,an Xian', 'LAX', '1-13-142-1468', '安徽省-滁州市-来安县', 1);
INSERT INTO `qmgx_region` VALUES (1469, '341124', '全椒县', 142, 3, 0, 'Quanjiao Xian', 'QJO', '1-13-142-1469', '安徽省-滁州市-全椒县', 1);
INSERT INTO `qmgx_region` VALUES (1470, '341125', '定远县', 142, 3, 0, 'Dingyuan Xian', 'DYW', '1-13-142-1470', '安徽省-滁州市-定远县', 1);
INSERT INTO `qmgx_region` VALUES (1471, '341126', '凤阳县', 142, 3, 0, 'Fengyang Xian', 'FYG', '1-13-142-1471', '安徽省-滁州市-凤阳县', 1);
INSERT INTO `qmgx_region` VALUES (1472, '341181', '天长市', 142, 3, 0, 'Tianchang Shi', 'TNC', '1-13-142-1472', '安徽省-滁州市-天长市', 1);
INSERT INTO `qmgx_region` VALUES (1473, '341182', '明光市', 142, 3, 0, 'Mingguang Shi', 'MGG', '1-13-142-1473', '安徽省-滁州市-明光市', 1);
INSERT INTO `qmgx_region` VALUES (1474, '341201', '市辖区', 143, 3, 0, 'Shixiaqu', '2', '1-13-143-1474', '安徽省-阜阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1475, '341202', '颍州区', 143, 3, 0, 'Yingzhou Qu', '2', '1-13-143-1475', '安徽省-阜阳市-颍州区', 1);
INSERT INTO `qmgx_region` VALUES (1476, '341203', '颍东区', 143, 3, 0, 'Yingdong Qu', '2', '1-13-143-1476', '安徽省-阜阳市-颍东区', 1);
INSERT INTO `qmgx_region` VALUES (1477, '341204', '颍泉区', 143, 3, 0, 'Yingquan Qu', '2', '1-13-143-1477', '安徽省-阜阳市-颍泉区', 1);
INSERT INTO `qmgx_region` VALUES (1478, '341221', '临泉县', 143, 3, 0, 'Linquan Xian', 'LQN', '1-13-143-1478', '安徽省-阜阳市-临泉县', 1);
INSERT INTO `qmgx_region` VALUES (1479, '341222', '太和县', 143, 3, 0, 'Taihe Xian', 'TIH', '1-13-143-1479', '安徽省-阜阳市-太和县', 1);
INSERT INTO `qmgx_region` VALUES (1480, '341225', '阜南县', 143, 3, 0, 'Funan Xian', 'FNX', '1-13-143-1480', '安徽省-阜阳市-阜南县', 1);
INSERT INTO `qmgx_region` VALUES (1481, '341226', '颍上县', 143, 3, 0, 'Yingshang Xian', '2', '1-13-143-1481', '安徽省-阜阳市-颍上县', 1);
INSERT INTO `qmgx_region` VALUES (1482, '341282', '界首市', 143, 3, 0, 'Jieshou Shi', 'JSW', '1-13-143-1482', '安徽省-阜阳市-界首市', 1);
INSERT INTO `qmgx_region` VALUES (1483, '341301', '市辖区', 144, 3, 0, 'Shixiaqu', '2', '1-13-144-1483', '安徽省-宿州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1484, '341302', '埇桥区', 144, 3, 0, 'Yongqiao Qu', '2', '1-13-144-1484', '安徽省-宿州市-埇桥区', 1);
INSERT INTO `qmgx_region` VALUES (1485, '341321', '砀山县', 144, 3, 0, 'Dangshan Xian', 'DSW', '1-13-144-1485', '安徽省-宿州市-砀山县', 1);
INSERT INTO `qmgx_region` VALUES (1486, '341322', '萧县', 144, 3, 0, 'Xiao Xian', 'XIO', '1-13-144-1486', '安徽省-宿州市-萧县', 1);
INSERT INTO `qmgx_region` VALUES (1487, '341323', '灵璧县', 144, 3, 0, 'Lingbi Xian', 'LBI', '1-13-144-1487', '安徽省-宿州市-灵璧县', 1);
INSERT INTO `qmgx_region` VALUES (1488, '341324', '泗县', 144, 3, 0, 'Si Xian ', 'SIX', '1-13-144-1488', '安徽省-宿州市-泗县', 1);
INSERT INTO `qmgx_region` VALUES (1489, '341401', '市辖区', 145, 3, 0, '1', '2', '1-13-145-1489', '安徽省-巢湖市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1490, '341402', '居巢区', 145, 3, 0, 'Juchao Qu', '2', '1-13-145-1490', '安徽省-巢湖市-居巢区', 1);
INSERT INTO `qmgx_region` VALUES (1491, '341421', '庐江县', 145, 3, 0, 'Lujiang Xian', '2', '1-13-145-1491', '安徽省-巢湖市-庐江县', 1);
INSERT INTO `qmgx_region` VALUES (1492, '341422', '无为县', 145, 3, 0, 'Wuwei Xian', '2', '1-13-145-1492', '安徽省-巢湖市-无为县', 1);
INSERT INTO `qmgx_region` VALUES (1493, '341423', '含山县', 145, 3, 0, 'Hanshan Xian', '2', '1-13-145-1493', '安徽省-巢湖市-含山县', 1);
INSERT INTO `qmgx_region` VALUES (1494, '341424', '和县', 145, 3, 0, 'He Xian ', '2', '1-13-145-1494', '安徽省-巢湖市-和县', 1);
INSERT INTO `qmgx_region` VALUES (1495, '341501', '市辖区', 146, 3, 0, '1', '2', '1-13-146-1495', '安徽省-六安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1496, '341502', '金安区', 146, 3, 0, 'Jinan Qu', '2', '1-13-146-1496', '安徽省-六安市-金安区', 1);
INSERT INTO `qmgx_region` VALUES (1497, '341503', '裕安区', 146, 3, 0, 'Yuan Qu', '2', '1-13-146-1497', '安徽省-六安市-裕安区', 1);
INSERT INTO `qmgx_region` VALUES (1498, '341521', '寿县', 146, 3, 0, 'Shou Xian', '2', '1-13-146-1498', '安徽省-六安市-寿县', 1);
INSERT INTO `qmgx_region` VALUES (1499, '341522', '霍邱县', 146, 3, 0, 'Huoqiu Xian', '2', '1-13-146-1499', '安徽省-六安市-霍邱县', 1);
INSERT INTO `qmgx_region` VALUES (1500, '341523', '舒城县', 146, 3, 0, 'Shucheng Xian', '2', '1-13-146-1500', '安徽省-六安市-舒城县', 1);
INSERT INTO `qmgx_region` VALUES (1501, '341524', '金寨县', 146, 3, 0, 'Jingzhai Xian', '2', '1-13-146-1501', '安徽省-六安市-金寨县', 1);
INSERT INTO `qmgx_region` VALUES (1502, '341525', '霍山县', 146, 3, 0, 'Huoshan Xian', '2', '1-13-146-1502', '安徽省-六安市-霍山县', 1);
INSERT INTO `qmgx_region` VALUES (1503, '341601', '市辖区', 147, 3, 0, '1', '2', '1-13-147-1503', '安徽省-亳州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1504, '341602', '谯城区', 147, 3, 0, 'Qiaocheng Qu', '2', '1-13-147-1504', '安徽省-亳州市-谯城区', 1);
INSERT INTO `qmgx_region` VALUES (1505, '341621', '涡阳县', 147, 3, 0, 'Guoyang Xian', '2', '1-13-147-1505', '安徽省-亳州市-涡阳县', 1);
INSERT INTO `qmgx_region` VALUES (1506, '341622', '蒙城县', 147, 3, 0, 'Mengcheng Xian', '2', '1-13-147-1506', '安徽省-亳州市-蒙城县', 1);
INSERT INTO `qmgx_region` VALUES (1507, '341623', '利辛县', 147, 3, 0, 'Lixin Xian', '2', '1-13-147-1507', '安徽省-亳州市-利辛县', 1);
INSERT INTO `qmgx_region` VALUES (1508, '341701', '市辖区', 148, 3, 0, '1', '2', '1-13-148-1508', '安徽省-池州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1509, '341702', '贵池区', 148, 3, 0, 'Guichi Qu', '2', '1-13-148-1509', '安徽省-池州市-贵池区', 1);
INSERT INTO `qmgx_region` VALUES (1510, '341721', '东至县', 148, 3, 0, 'Dongzhi Xian', '2', '1-13-148-1510', '安徽省-池州市-东至县', 1);
INSERT INTO `qmgx_region` VALUES (1511, '341722', '石台县', 148, 3, 0, 'Shitai Xian', '2', '1-13-148-1511', '安徽省-池州市-石台县', 1);
INSERT INTO `qmgx_region` VALUES (1512, '341723', '青阳县', 148, 3, 0, 'Qingyang Xian', '2', '1-13-148-1512', '安徽省-池州市-青阳县', 1);
INSERT INTO `qmgx_region` VALUES (1513, '341801', '市辖区', 149, 3, 0, '1', '2', '1-13-149-1513', '安徽省-宣城市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1514, '341802', '宣州区', 149, 3, 0, 'Xuanzhou Qu', '2', '1-13-149-1514', '安徽省-宣城市-宣州区', 1);
INSERT INTO `qmgx_region` VALUES (1515, '341821', '郎溪县', 149, 3, 0, 'Langxi Xian', '2', '1-13-149-1515', '安徽省-宣城市-郎溪县', 1);
INSERT INTO `qmgx_region` VALUES (1516, '341822', '广德县', 149, 3, 0, 'Guangde Xian', '2', '1-13-149-1516', '安徽省-宣城市-广德县', 1);
INSERT INTO `qmgx_region` VALUES (1517, '341823', '泾县', 149, 3, 0, 'Jing Xian', '2', '1-13-149-1517', '安徽省-宣城市-泾县', 1);
INSERT INTO `qmgx_region` VALUES (1518, '341824', '绩溪县', 149, 3, 0, 'Jixi Xian', '2', '1-13-149-1518', '安徽省-宣城市-绩溪县', 1);
INSERT INTO `qmgx_region` VALUES (1519, '341825', '旌德县', 149, 3, 0, 'Jingde Xian', '2', '1-13-149-1519', '安徽省-宣城市-旌德县', 1);
INSERT INTO `qmgx_region` VALUES (1520, '341881', '宁国市', 149, 3, 0, 'Ningguo Shi', '2', '1-13-149-1520', '安徽省-宣城市-宁国市', 1);
INSERT INTO `qmgx_region` VALUES (1521, '350101', '市辖区', 150, 3, 0, 'Shixiaqu', '2', '1-14-150-1521', '福建省-福州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1522, '350102', '鼓楼区', 150, 3, 0, 'Gulou Qu', 'GLR', '1-14-150-1522', '福建省-福州市-鼓楼区', 1);
INSERT INTO `qmgx_region` VALUES (1523, '350103', '台江区', 150, 3, 0, 'Taijiang Qu', 'TJQ', '1-14-150-1523', '福建省-福州市-台江区', 1);
INSERT INTO `qmgx_region` VALUES (1524, '350104', '仓山区', 150, 3, 0, 'Cangshan Qu', 'CSQ', '1-14-150-1524', '福建省-福州市-仓山区', 1);
INSERT INTO `qmgx_region` VALUES (1525, '350105', '马尾区', 150, 3, 0, 'Mawei Qu', 'MWQ', '1-14-150-1525', '福建省-福州市-马尾区', 1);
INSERT INTO `qmgx_region` VALUES (1526, '350111', '晋安区', 150, 3, 0, 'Jin,an Qu', 'JAF', '1-14-150-1526', '福建省-福州市-晋安区', 1);
INSERT INTO `qmgx_region` VALUES (1527, '350121', '闽侯县', 150, 3, 0, 'Minhou Qu', 'MHO', '1-14-150-1527', '福建省-福州市-闽侯县', 1);
INSERT INTO `qmgx_region` VALUES (1528, '350122', '连江县', 150, 3, 0, 'Lianjiang Xian', 'LJF', '1-14-150-1528', '福建省-福州市-连江县', 1);
INSERT INTO `qmgx_region` VALUES (1529, '350123', '罗源县', 150, 3, 0, 'Luoyuan Xian', 'LOY', '1-14-150-1529', '福建省-福州市-罗源县', 1);
INSERT INTO `qmgx_region` VALUES (1530, '350124', '闽清县', 150, 3, 0, 'Minqing Xian', 'MQG', '1-14-150-1530', '福建省-福州市-闽清县', 1);
INSERT INTO `qmgx_region` VALUES (1531, '350125', '永泰县', 150, 3, 0, 'Yongtai Xian', 'YTX', '1-14-150-1531', '福建省-福州市-永泰县', 1);
INSERT INTO `qmgx_region` VALUES (1532, '350128', '平潭县', 150, 3, 0, 'Pingtan Xian', 'PTN', '1-14-150-1532', '福建省-福州市-平潭县', 1);
INSERT INTO `qmgx_region` VALUES (1533, '350181', '福清市', 150, 3, 0, 'Fuqing Shi', 'FQS', '1-14-150-1533', '福建省-福州市-福清市', 1);
INSERT INTO `qmgx_region` VALUES (1534, '350182', '长乐市', 150, 3, 0, 'Changle Shi', 'CLS', '1-14-150-1534', '福建省-福州市-长乐市', 1);
INSERT INTO `qmgx_region` VALUES (1535, '350201', '市辖区', 151, 3, 0, 'Shixiaqu', '2', '1-14-151-1535', '福建省-厦门市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1536, '350203', '思明区', 151, 3, 0, 'Siming Qu', 'SMQ', '1-14-151-1536', '福建省-厦门市-思明区', 1);
INSERT INTO `qmgx_region` VALUES (1537, '350205', '海沧区', 151, 3, 0, 'Haicang Qu', '2', '1-14-151-1537', '福建省-厦门市-海沧区', 1);
INSERT INTO `qmgx_region` VALUES (1538, '350206', '湖里区', 151, 3, 0, 'Huli Qu', 'HLQ', '1-14-151-1538', '福建省-厦门市-湖里区', 1);
INSERT INTO `qmgx_region` VALUES (1539, '350211', '集美区', 151, 3, 0, 'Jimei Qu', 'JMQ', '1-14-151-1539', '福建省-厦门市-集美区', 1);
INSERT INTO `qmgx_region` VALUES (1540, '350212', '同安区', 151, 3, 0, 'Tong,an Qu', 'TAQ', '1-14-151-1540', '福建省-厦门市-同安区', 1);
INSERT INTO `qmgx_region` VALUES (1541, '350213', '翔安区', 151, 3, 0, 'Xiangan Qu', '2', '1-14-151-1541', '福建省-厦门市-翔安区', 1);
INSERT INTO `qmgx_region` VALUES (1542, '350301', '市辖区', 152, 3, 0, 'Shixiaqu', '2', '1-14-152-1542', '福建省-莆田市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1543, '350302', '城厢区', 152, 3, 0, 'Chengxiang Qu', 'CXP', '1-14-152-1543', '福建省-莆田市-城厢区', 1);
INSERT INTO `qmgx_region` VALUES (1544, '350303', '涵江区', 152, 3, 0, 'Hanjiang Qu', 'HJQ', '1-14-152-1544', '福建省-莆田市-涵江区', 1);
INSERT INTO `qmgx_region` VALUES (1545, '350304', '荔城区', 152, 3, 0, 'Licheng Qu', '2', '1-14-152-1545', '福建省-莆田市-荔城区', 1);
INSERT INTO `qmgx_region` VALUES (1546, '350305', '秀屿区', 152, 3, 0, 'Xiuyu Qu', '2', '1-14-152-1546', '福建省-莆田市-秀屿区', 1);
INSERT INTO `qmgx_region` VALUES (1547, '350322', '仙游县', 152, 3, 0, 'Xianyou Xian', 'XYF', '1-14-152-1547', '福建省-莆田市-仙游县', 1);
INSERT INTO `qmgx_region` VALUES (1548, '350401', '市辖区', 153, 3, 0, 'Shixiaqu', '2', '1-14-153-1548', '福建省-三明市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1549, '350402', '梅列区', 153, 3, 0, 'Meilie Qu', 'MLQ', '1-14-153-1549', '福建省-三明市-梅列区', 1);
INSERT INTO `qmgx_region` VALUES (1550, '350403', '三元区', 153, 3, 0, 'Sanyuan Qu', 'SYB', '1-14-153-1550', '福建省-三明市-三元区', 1);
INSERT INTO `qmgx_region` VALUES (1551, '350421', '明溪县', 153, 3, 0, 'Mingxi Xian', 'MXI', '1-14-153-1551', '福建省-三明市-明溪县', 1);
INSERT INTO `qmgx_region` VALUES (1552, '350423', '清流县', 153, 3, 0, 'Qingliu Xian', 'QLX', '1-14-153-1552', '福建省-三明市-清流县', 1);
INSERT INTO `qmgx_region` VALUES (1553, '350424', '宁化县', 153, 3, 0, 'Ninghua Xian', 'NGH', '1-14-153-1553', '福建省-三明市-宁化县', 1);
INSERT INTO `qmgx_region` VALUES (1554, '350425', '大田县', 153, 3, 0, 'Datian Xian', 'DTM', '1-14-153-1554', '福建省-三明市-大田县', 1);
INSERT INTO `qmgx_region` VALUES (1555, '350426', '尤溪县', 153, 3, 0, 'Youxi Xian', 'YXF', '1-14-153-1555', '福建省-三明市-尤溪县', 1);
INSERT INTO `qmgx_region` VALUES (1556, '350427', '沙县', 153, 3, 0, 'Sha Xian', 'SAX', '1-14-153-1556', '福建省-三明市-沙县', 1);
INSERT INTO `qmgx_region` VALUES (1557, '350428', '将乐县', 153, 3, 0, 'Jiangle Xian', 'JLE', '1-14-153-1557', '福建省-三明市-将乐县', 1);
INSERT INTO `qmgx_region` VALUES (1558, '350429', '泰宁县', 153, 3, 0, 'Taining Xian', 'TNG', '1-14-153-1558', '福建省-三明市-泰宁县', 1);
INSERT INTO `qmgx_region` VALUES (1559, '350430', '建宁县', 153, 3, 0, 'Jianning Xian', 'JNF', '1-14-153-1559', '福建省-三明市-建宁县', 1);
INSERT INTO `qmgx_region` VALUES (1560, '350481', '永安市', 153, 3, 0, 'Yong,an Shi', 'YAF', '1-14-153-1560', '福建省-三明市-永安市', 1);
INSERT INTO `qmgx_region` VALUES (1561, '350501', '市辖区', 154, 3, 0, 'Shixiaqu', '2', '1-14-154-1561', '福建省-泉州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1562, '350502', '鲤城区', 154, 3, 0, 'Licheng Qu', 'LCQ', '1-14-154-1562', '福建省-泉州市-鲤城区', 1);
INSERT INTO `qmgx_region` VALUES (1563, '350503', '丰泽区', 154, 3, 0, 'Fengze Qu', 'FZE', '1-14-154-1563', '福建省-泉州市-丰泽区', 1);
INSERT INTO `qmgx_region` VALUES (1564, '350504', '洛江区', 154, 3, 0, 'Luojiang Qu', 'LJQ', '1-14-154-1564', '福建省-泉州市-洛江区', 1);
INSERT INTO `qmgx_region` VALUES (1565, '350505', '泉港区', 154, 3, 0, 'Quangang Qu', '2', '1-14-154-1565', '福建省-泉州市-泉港区', 1);
INSERT INTO `qmgx_region` VALUES (1566, '350521', '惠安县', 154, 3, 0, 'Hui,an Xian', 'HAF', '1-14-154-1566', '福建省-泉州市-惠安县', 1);
INSERT INTO `qmgx_region` VALUES (1567, '350524', '安溪县', 154, 3, 0, 'Anxi Xian', 'ANX', '1-14-154-1567', '福建省-泉州市-安溪县', 1);
INSERT INTO `qmgx_region` VALUES (1568, '350525', '永春县', 154, 3, 0, 'Yongchun Xian', 'YCM', '1-14-154-1568', '福建省-泉州市-永春县', 1);
INSERT INTO `qmgx_region` VALUES (1569, '350526', '德化县', 154, 3, 0, 'Dehua Xian', 'DHA', '1-14-154-1569', '福建省-泉州市-德化县', 1);
INSERT INTO `qmgx_region` VALUES (1570, '350527', '金门县', 154, 3, 0, 'Jinmen Xian', 'JME', '1-14-154-1570', '福建省-泉州市-金门县', 1);
INSERT INTO `qmgx_region` VALUES (1571, '350581', '石狮市', 154, 3, 0, 'Shishi Shi', 'SHH', '1-14-154-1571', '福建省-泉州市-石狮市', 1);
INSERT INTO `qmgx_region` VALUES (1572, '350582', '晋江市', 154, 3, 0, 'Jinjiang Shi', 'JJG', '1-14-154-1572', '福建省-泉州市-晋江市', 1);
INSERT INTO `qmgx_region` VALUES (1573, '350583', '南安市', 154, 3, 0, 'Nan,an Shi', 'NAS', '1-14-154-1573', '福建省-泉州市-南安市', 1);
INSERT INTO `qmgx_region` VALUES (1574, '350601', '市辖区', 155, 3, 0, 'Shixiaqu', '2', '1-14-155-1574', '福建省-漳州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1575, '350602', '芗城区', 155, 3, 0, 'Xiangcheng Qu', 'XZZ', '1-14-155-1575', '福建省-漳州市-芗城区', 1);
INSERT INTO `qmgx_region` VALUES (1576, '350603', '龙文区', 155, 3, 0, 'Longwen Qu', 'LWZ', '1-14-155-1576', '福建省-漳州市-龙文区', 1);
INSERT INTO `qmgx_region` VALUES (1577, '350622', '云霄县', 155, 3, 0, 'Yunxiao Xian', 'YXO', '1-14-155-1577', '福建省-漳州市-云霄县', 1);
INSERT INTO `qmgx_region` VALUES (1578, '350623', '漳浦县', 155, 3, 0, 'Zhangpu Xian', 'ZPU', '1-14-155-1578', '福建省-漳州市-漳浦县', 1);
INSERT INTO `qmgx_region` VALUES (1579, '350624', '诏安县', 155, 3, 0, 'Zhao,an Xian', 'ZAF', '1-14-155-1579', '福建省-漳州市-诏安县', 1);
INSERT INTO `qmgx_region` VALUES (1580, '350625', '长泰县', 155, 3, 0, 'Changtai Xian', 'CTA', '1-14-155-1580', '福建省-漳州市-长泰县', 1);
INSERT INTO `qmgx_region` VALUES (1581, '350626', '东山县', 155, 3, 0, 'Dongshan Xian', 'DSN', '1-14-155-1581', '福建省-漳州市-东山县', 1);
INSERT INTO `qmgx_region` VALUES (1582, '350627', '南靖县', 155, 3, 0, 'Nanjing Xian', 'NJX', '1-14-155-1582', '福建省-漳州市-南靖县', 1);
INSERT INTO `qmgx_region` VALUES (1583, '350628', '平和县', 155, 3, 0, 'Pinghe Xian', 'PHE', '1-14-155-1583', '福建省-漳州市-平和县', 1);
INSERT INTO `qmgx_region` VALUES (1584, '350629', '华安县', 155, 3, 0, 'Hua,an Xian', 'HAN', '1-14-155-1584', '福建省-漳州市-华安县', 1);
INSERT INTO `qmgx_region` VALUES (1585, '350681', '龙海市', 155, 3, 0, 'Longhai Shi', 'LHM', '1-14-155-1585', '福建省-漳州市-龙海市', 1);
INSERT INTO `qmgx_region` VALUES (1586, '350701', '市辖区', 156, 3, 0, 'Shixiaqu', '2', '1-14-156-1586', '福建省-南平市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1587, '350702', '延平区', 156, 3, 0, 'Yanping Qu', 'YPQ', '1-14-156-1587', '福建省-南平市-延平区', 1);
INSERT INTO `qmgx_region` VALUES (1588, '350721', '顺昌县', 156, 3, 0, 'Shunchang Xian', 'SCG', '1-14-156-1588', '福建省-南平市-顺昌县', 1);
INSERT INTO `qmgx_region` VALUES (1589, '350722', '浦城县', 156, 3, 0, 'Pucheng Xian', 'PCX', '1-14-156-1589', '福建省-南平市-浦城县', 1);
INSERT INTO `qmgx_region` VALUES (1590, '350723', '光泽县', 156, 3, 0, 'Guangze Xian', 'GZE', '1-14-156-1590', '福建省-南平市-光泽县', 1);
INSERT INTO `qmgx_region` VALUES (1591, '350724', '松溪县', 156, 3, 0, 'Songxi Xian', 'SOX', '1-14-156-1591', '福建省-南平市-松溪县', 1);
INSERT INTO `qmgx_region` VALUES (1592, '350725', '政和县', 156, 3, 0, 'Zhenghe Xian', 'ZGH', '1-14-156-1592', '福建省-南平市-政和县', 1);
INSERT INTO `qmgx_region` VALUES (1593, '350781', '邵武市', 156, 3, 0, 'Shaowu Shi', 'SWU', '1-14-156-1593', '福建省-南平市-邵武市', 1);
INSERT INTO `qmgx_region` VALUES (1594, '350782', '武夷山市', 156, 3, 0, 'Wuyishan Shi', 'WUS', '1-14-156-1594', '福建省-南平市-武夷山市', 1);
INSERT INTO `qmgx_region` VALUES (1595, '350783', '建瓯市', 156, 3, 0, 'Jian,ou Shi', 'JOU', '1-14-156-1595', '福建省-南平市-建瓯市', 1);
INSERT INTO `qmgx_region` VALUES (1596, '350784', '建阳市', 156, 3, 0, 'Jianyang Shi', 'JNY', '1-14-156-1596', '福建省-南平市-建阳市', 1);
INSERT INTO `qmgx_region` VALUES (1597, '350801', '市辖区', 157, 3, 0, 'Shixiaqu', '2', '1-14-157-1597', '福建省-龙岩市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1598, '350802', '新罗区', 157, 3, 0, 'Xinluo Qu', 'XNL', '1-14-157-1598', '福建省-龙岩市-新罗区', 1);
INSERT INTO `qmgx_region` VALUES (1599, '350821', '长汀县', 157, 3, 0, 'Changting Xian', 'CTG', '1-14-157-1599', '福建省-龙岩市-长汀县', 1);
INSERT INTO `qmgx_region` VALUES (1600, '350822', '永定县', 157, 3, 0, 'Yongding Xian', 'YDI', '1-14-157-1600', '福建省-龙岩市-永定县', 1);
INSERT INTO `qmgx_region` VALUES (1601, '350823', '上杭县', 157, 3, 0, 'Shanghang Xian', 'SHF', '1-14-157-1601', '福建省-龙岩市-上杭县', 1);
INSERT INTO `qmgx_region` VALUES (1602, '350824', '武平县', 157, 3, 0, 'Wuping Xian', 'WPG', '1-14-157-1602', '福建省-龙岩市-武平县', 1);
INSERT INTO `qmgx_region` VALUES (1603, '350825', '连城县', 157, 3, 0, 'Liancheng Xian', 'LCF', '1-14-157-1603', '福建省-龙岩市-连城县', 1);
INSERT INTO `qmgx_region` VALUES (1604, '350881', '漳平市', 157, 3, 0, 'Zhangping Xian', 'ZGP', '1-14-157-1604', '福建省-龙岩市-漳平市', 1);
INSERT INTO `qmgx_region` VALUES (1605, '350901', '市辖区', 158, 3, 0, '1', '2', '1-14-158-1605', '福建省-宁德市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1606, '350902', '蕉城区', 158, 3, 0, 'Jiaocheng Qu', '2', '1-14-158-1606', '福建省-宁德市-蕉城区', 1);
INSERT INTO `qmgx_region` VALUES (1607, '350921', '霞浦县', 158, 3, 0, 'Xiapu Xian', '2', '1-14-158-1607', '福建省-宁德市-霞浦县', 1);
INSERT INTO `qmgx_region` VALUES (1608, '350922', '古田县', 158, 3, 0, 'Gutian Xian', '2', '1-14-158-1608', '福建省-宁德市-古田县', 1);
INSERT INTO `qmgx_region` VALUES (1609, '350923', '屏南县', 158, 3, 0, 'Pingnan Xian', '2', '1-14-158-1609', '福建省-宁德市-屏南县', 1);
INSERT INTO `qmgx_region` VALUES (1610, '350924', '寿宁县', 158, 3, 0, 'Shouning Xian', '2', '1-14-158-1610', '福建省-宁德市-寿宁县', 1);
INSERT INTO `qmgx_region` VALUES (1611, '350925', '周宁县', 158, 3, 0, 'Zhouning Xian', '2', '1-14-158-1611', '福建省-宁德市-周宁县', 1);
INSERT INTO `qmgx_region` VALUES (1612, '350926', '柘荣县', 158, 3, 0, 'Zherong Xian', '2', '1-14-158-1612', '福建省-宁德市-柘荣县', 1);
INSERT INTO `qmgx_region` VALUES (1613, '350981', '福安市', 158, 3, 0, 'Fu,an Shi', '2', '1-14-158-1613', '福建省-宁德市-福安市', 1);
INSERT INTO `qmgx_region` VALUES (1614, '350982', '福鼎市', 158, 3, 0, 'Fuding Shi', '2', '1-14-158-1614', '福建省-宁德市-福鼎市', 1);
INSERT INTO `qmgx_region` VALUES (1615, '360101', '市辖区', 159, 3, 0, 'Shixiaqu', '2', '1-15-159-1615', '江西省-南昌市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1616, '360102', '东湖区', 159, 3, 0, 'Donghu Qu', 'DHU', '1-15-159-1616', '江西省-南昌市-东湖区', 1);
INSERT INTO `qmgx_region` VALUES (1617, '360103', '西湖区', 159, 3, 0, 'Xihu Qu ', 'XHQ', '1-15-159-1617', '江西省-南昌市-西湖区', 1);
INSERT INTO `qmgx_region` VALUES (1618, '360104', '青云谱区', 159, 3, 0, 'Qingyunpu Qu', 'QYP', '1-15-159-1618', '江西省-南昌市-青云谱区', 1);
INSERT INTO `qmgx_region` VALUES (1619, '360105', '湾里区', 159, 3, 0, 'Wanli Qu', 'WLI', '1-15-159-1619', '江西省-南昌市-湾里区', 1);
INSERT INTO `qmgx_region` VALUES (1620, '360111', '青山湖区', 159, 3, 0, 'Qingshanhu Qu', '2', '1-15-159-1620', '江西省-南昌市-青山湖区', 1);
INSERT INTO `qmgx_region` VALUES (1621, '360121', '南昌县', 159, 3, 0, 'Nanchang Xian', 'NCA', '1-15-159-1621', '江西省-南昌市-南昌县', 1);
INSERT INTO `qmgx_region` VALUES (1622, '360122', '新建县', 159, 3, 0, 'Xinjian Xian', 'XJN', '1-15-159-1622', '江西省-南昌市-新建县', 1);
INSERT INTO `qmgx_region` VALUES (1623, '360123', '安义县', 159, 3, 0, 'Anyi Xian', 'AYI', '1-15-159-1623', '江西省-南昌市-安义县', 1);
INSERT INTO `qmgx_region` VALUES (1624, '360124', '进贤县', 159, 3, 0, 'Jinxian Xian', 'JXX', '1-15-159-1624', '江西省-南昌市-进贤县', 1);
INSERT INTO `qmgx_region` VALUES (1625, '360201', '市辖区', 160, 3, 0, 'Shixiaqu', '2', '1-15-160-1625', '江西省-景德镇市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1626, '360202', '昌江区', 160, 3, 0, 'Changjiang Qu', 'CJG', '1-15-160-1626', '江西省-景德镇市-昌江区', 1);
INSERT INTO `qmgx_region` VALUES (1627, '360203', '珠山区', 160, 3, 0, 'Zhushan Qu', 'ZSJ', '1-15-160-1627', '江西省-景德镇市-珠山区', 1);
INSERT INTO `qmgx_region` VALUES (1628, '360222', '浮梁县', 160, 3, 0, 'Fuliang Xian', 'FLX', '1-15-160-1628', '江西省-景德镇市-浮梁县', 1);
INSERT INTO `qmgx_region` VALUES (1629, '360281', '乐平市', 160, 3, 0, 'Leping Shi', 'LEP', '1-15-160-1629', '江西省-景德镇市-乐平市', 1);
INSERT INTO `qmgx_region` VALUES (1630, '360301', '市辖区', 161, 3, 0, 'Shixiaqu', '2', '1-15-161-1630', '江西省-萍乡市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1631, '360302', '安源区', 161, 3, 0, 'Anyuan Qu', 'AYQ', '1-15-161-1631', '江西省-萍乡市-安源区', 1);
INSERT INTO `qmgx_region` VALUES (1632, '360313', '湘东区', 161, 3, 0, 'Xiangdong Qu', 'XDG', '1-15-161-1632', '江西省-萍乡市-湘东区', 1);
INSERT INTO `qmgx_region` VALUES (1633, '360321', '莲花县', 161, 3, 0, 'Lianhua Xian', 'LHG', '1-15-161-1633', '江西省-萍乡市-莲花县', 1);
INSERT INTO `qmgx_region` VALUES (1634, '360322', '上栗县', 161, 3, 0, 'Shangli Xian', 'SLI', '1-15-161-1634', '江西省-萍乡市-上栗县', 1);
INSERT INTO `qmgx_region` VALUES (1635, '360323', '芦溪县', 161, 3, 0, 'Lixi Xian', 'LXP', '1-15-161-1635', '江西省-萍乡市-芦溪县', 1);
INSERT INTO `qmgx_region` VALUES (1636, '360401', '市辖区', 162, 3, 0, 'Shixiaqu', '2', '1-15-162-1636', '江西省-九江市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1637, '360402', '庐山区', 162, 3, 0, 'Lushan Qu', 'LSV', '1-15-162-1637', '江西省-九江市-庐山区', 1);
INSERT INTO `qmgx_region` VALUES (1638, '360403', '浔阳区', 162, 3, 0, 'Xunyang Qu', 'XYC', '1-15-162-1638', '江西省-九江市-浔阳区', 1);
INSERT INTO `qmgx_region` VALUES (1639, '360421', '九江县', 162, 3, 0, 'Jiujiang Xian', 'JUJ', '1-15-162-1639', '江西省-九江市-九江县', 1);
INSERT INTO `qmgx_region` VALUES (1640, '360423', '武宁县', 162, 3, 0, 'Wuning Xian', 'WUN', '1-15-162-1640', '江西省-九江市-武宁县', 1);
INSERT INTO `qmgx_region` VALUES (1641, '360424', '修水县', 162, 3, 0, 'Xiushui Xian', 'XSG', '1-15-162-1641', '江西省-九江市-修水县', 1);
INSERT INTO `qmgx_region` VALUES (1642, '360425', '永修县', 162, 3, 0, 'Yongxiu Xian', 'YOX', '1-15-162-1642', '江西省-九江市-永修县', 1);
INSERT INTO `qmgx_region` VALUES (1643, '360426', '德安县', 162, 3, 0, 'De,an Xian', 'DEA', '1-15-162-1643', '江西省-九江市-德安县', 1);
INSERT INTO `qmgx_region` VALUES (1644, '360427', '星子县', 162, 3, 0, 'Xingzi Xian', 'XZI', '1-15-162-1644', '江西省-九江市-星子县', 1);
INSERT INTO `qmgx_region` VALUES (1645, '360428', '都昌县', 162, 3, 0, 'Duchang Xian', 'DUC', '1-15-162-1645', '江西省-九江市-都昌县', 1);
INSERT INTO `qmgx_region` VALUES (1646, '360429', '湖口县', 162, 3, 0, 'Hukou Xian', 'HUK', '1-15-162-1646', '江西省-九江市-湖口县', 1);
INSERT INTO `qmgx_region` VALUES (1647, '360430', '彭泽县', 162, 3, 0, 'Pengze Xian', 'PZE', '1-15-162-1647', '江西省-九江市-彭泽县', 1);
INSERT INTO `qmgx_region` VALUES (1648, '360481', '瑞昌市', 162, 3, 0, 'Ruichang Shi', 'RCG', '1-15-162-1648', '江西省-九江市-瑞昌市', 1);
INSERT INTO `qmgx_region` VALUES (1649, '360501', '市辖区', 163, 3, 0, 'Shixiaqu', '2', '1-15-163-1649', '江西省-新余市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1650, '360502', '渝水区', 163, 3, 0, 'Yushui Qu', 'YSR', '1-15-163-1650', '江西省-新余市-渝水区', 1);
INSERT INTO `qmgx_region` VALUES (1651, '360521', '分宜县', 163, 3, 0, 'Fenyi Xian', 'FYI', '1-15-163-1651', '江西省-新余市-分宜县', 1);
INSERT INTO `qmgx_region` VALUES (1652, '360601', '市辖区', 164, 3, 0, 'Shixiaqu', '2', '1-15-164-1652', '江西省-鹰潭市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1653, '360602', '月湖区', 164, 3, 0, 'Yuehu Qu', 'YHY', '1-15-164-1653', '江西省-鹰潭市-月湖区', 1);
INSERT INTO `qmgx_region` VALUES (1654, '360622', '余江县', 164, 3, 0, 'Yujiang Xian', 'YUJ', '1-15-164-1654', '江西省-鹰潭市-余江县', 1);
INSERT INTO `qmgx_region` VALUES (1655, '360681', '贵溪市', 164, 3, 0, 'Guixi Shi', 'GXS', '1-15-164-1655', '江西省-鹰潭市-贵溪市', 1);
INSERT INTO `qmgx_region` VALUES (1656, '360701', '市辖区', 165, 3, 0, 'Shixiaqu', '2', '1-15-165-1656', '江西省-赣州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1657, '360702', '章贡区', 165, 3, 0, 'Zhanggong Qu', 'ZGG', '1-15-165-1657', '江西省-赣州市-章贡区', 1);
INSERT INTO `qmgx_region` VALUES (1658, '360721', '赣县', 165, 3, 0, 'Gan Xian', 'GXN', '1-15-165-1658', '江西省-赣州市-赣县', 1);
INSERT INTO `qmgx_region` VALUES (1659, '360722', '信丰县', 165, 3, 0, 'Xinfeng Xian ', 'XNF', '1-15-165-1659', '江西省-赣州市-信丰县', 1);
INSERT INTO `qmgx_region` VALUES (1660, '360723', '大余县', 165, 3, 0, 'Dayu Xian', 'DYX', '1-15-165-1660', '江西省-赣州市-大余县', 1);
INSERT INTO `qmgx_region` VALUES (1661, '360724', '上犹县', 165, 3, 0, 'Shangyou Xian', 'SYO', '1-15-165-1661', '江西省-赣州市-上犹县', 1);
INSERT INTO `qmgx_region` VALUES (1662, '360725', '崇义县', 165, 3, 0, 'Chongyi Xian', 'CYX', '1-15-165-1662', '江西省-赣州市-崇义县', 1);
INSERT INTO `qmgx_region` VALUES (1663, '360726', '安远县', 165, 3, 0, 'Anyuan Xian', 'AYN', '1-15-165-1663', '江西省-赣州市-安远县', 1);
INSERT INTO `qmgx_region` VALUES (1664, '360727', '龙南县', 165, 3, 0, 'Longnan Xian', 'LNX', '1-15-165-1664', '江西省-赣州市-龙南县', 1);
INSERT INTO `qmgx_region` VALUES (1665, '360728', '定南县', 165, 3, 0, 'Dingnan Xian', 'DNN', '1-15-165-1665', '江西省-赣州市-定南县', 1);
INSERT INTO `qmgx_region` VALUES (1666, '360729', '全南县', 165, 3, 0, 'Quannan Xian', 'QNN', '1-15-165-1666', '江西省-赣州市-全南县', 1);
INSERT INTO `qmgx_region` VALUES (1667, '360730', '宁都县', 165, 3, 0, 'Ningdu Xian', 'NDU', '1-15-165-1667', '江西省-赣州市-宁都县', 1);
INSERT INTO `qmgx_region` VALUES (1668, '360731', '于都县', 165, 3, 0, 'Yudu Xian', 'YUD', '1-15-165-1668', '江西省-赣州市-于都县', 1);
INSERT INTO `qmgx_region` VALUES (1669, '360732', '兴国县', 165, 3, 0, 'Xingguo Xian', 'XGG', '1-15-165-1669', '江西省-赣州市-兴国县', 1);
INSERT INTO `qmgx_region` VALUES (1670, '360733', '会昌县', 165, 3, 0, 'Huichang Xian', 'HIC', '1-15-165-1670', '江西省-赣州市-会昌县', 1);
INSERT INTO `qmgx_region` VALUES (1671, '360734', '寻乌县', 165, 3, 0, 'Xunwu Xian', 'XNW', '1-15-165-1671', '江西省-赣州市-寻乌县', 1);
INSERT INTO `qmgx_region` VALUES (1672, '360735', '石城县', 165, 3, 0, 'Shicheng Xian', 'SIC', '1-15-165-1672', '江西省-赣州市-石城县', 1);
INSERT INTO `qmgx_region` VALUES (1673, '360781', '瑞金市', 165, 3, 0, 'Ruijin Shi', 'RJS', '1-15-165-1673', '江西省-赣州市-瑞金市', 1);
INSERT INTO `qmgx_region` VALUES (1674, '360782', '南康市', 165, 3, 0, 'Nankang Shi', 'NNK', '1-15-165-1674', '江西省-赣州市-南康市', 1);
INSERT INTO `qmgx_region` VALUES (1675, '360801', '市辖区', 166, 3, 0, '1', '2', '1-15-166-1675', '江西省-吉安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1676, '360802', '吉州区', 166, 3, 0, 'Jizhou Qu', '2', '1-15-166-1676', '江西省-吉安市-吉州区', 1);
INSERT INTO `qmgx_region` VALUES (1677, '360803', '青原区', 166, 3, 0, 'Qingyuan Qu', '2', '1-15-166-1677', '江西省-吉安市-青原区', 1);
INSERT INTO `qmgx_region` VALUES (1678, '360821', '吉安县', 166, 3, 0, 'Ji,an Xian', '2', '1-15-166-1678', '江西省-吉安市-吉安县', 1);
INSERT INTO `qmgx_region` VALUES (1679, '360822', '吉水县', 166, 3, 0, 'Jishui Xian', '2', '1-15-166-1679', '江西省-吉安市-吉水县', 1);
INSERT INTO `qmgx_region` VALUES (1680, '360823', '峡江县', 166, 3, 0, 'Xiajiang Xian', '2', '1-15-166-1680', '江西省-吉安市-峡江县', 1);
INSERT INTO `qmgx_region` VALUES (1681, '360824', '新干县', 166, 3, 0, 'Xingan Xian', '2', '1-15-166-1681', '江西省-吉安市-新干县', 1);
INSERT INTO `qmgx_region` VALUES (1682, '360825', '永丰县', 166, 3, 0, 'Yongfeng Xian', '2', '1-15-166-1682', '江西省-吉安市-永丰县', 1);
INSERT INTO `qmgx_region` VALUES (1683, '360826', '泰和县', 166, 3, 0, 'Taihe Xian', '2', '1-15-166-1683', '江西省-吉安市-泰和县', 1);
INSERT INTO `qmgx_region` VALUES (1684, '360827', '遂川县', 166, 3, 0, 'Suichuan Xian', '2', '1-15-166-1684', '江西省-吉安市-遂川县', 1);
INSERT INTO `qmgx_region` VALUES (1685, '360828', '万安县', 166, 3, 0, 'Wan,an Xian', '2', '1-15-166-1685', '江西省-吉安市-万安县', 1);
INSERT INTO `qmgx_region` VALUES (1686, '360829', '安福县', 166, 3, 0, 'Anfu Xian', '2', '1-15-166-1686', '江西省-吉安市-安福县', 1);
INSERT INTO `qmgx_region` VALUES (1687, '360830', '永新县', 166, 3, 0, 'Yongxin Xian ', '2', '1-15-166-1687', '江西省-吉安市-永新县', 1);
INSERT INTO `qmgx_region` VALUES (1688, '360881', '井冈山市', 166, 3, 0, 'Jinggangshan Shi', '2', '1-15-166-1688', '江西省-吉安市-井冈山市', 1);
INSERT INTO `qmgx_region` VALUES (1689, '360901', '市辖区', 167, 3, 0, '1', '2', '1-15-167-1689', '江西省-宜春市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1690, '360902', '袁州区', 167, 3, 0, 'Yuanzhou Qu', '2', '1-15-167-1690', '江西省-宜春市-袁州区', 1);
INSERT INTO `qmgx_region` VALUES (1691, '360921', '奉新县', 167, 3, 0, 'Fengxin Xian', '2', '1-15-167-1691', '江西省-宜春市-奉新县', 1);
INSERT INTO `qmgx_region` VALUES (1692, '360922', '万载县', 167, 3, 0, 'Wanzai Xian', '2', '1-15-167-1692', '江西省-宜春市-万载县', 1);
INSERT INTO `qmgx_region` VALUES (1693, '360923', '上高县', 167, 3, 0, 'Shanggao Xian', '2', '1-15-167-1693', '江西省-宜春市-上高县', 1);
INSERT INTO `qmgx_region` VALUES (1694, '360924', '宜丰县', 167, 3, 0, 'Yifeng Xian', '2', '1-15-167-1694', '江西省-宜春市-宜丰县', 1);
INSERT INTO `qmgx_region` VALUES (1695, '360925', '靖安县', 167, 3, 0, 'Jing,an Xian', '2', '1-15-167-1695', '江西省-宜春市-靖安县', 1);
INSERT INTO `qmgx_region` VALUES (1696, '360926', '铜鼓县', 167, 3, 0, 'Tonggu Xian', '2', '1-15-167-1696', '江西省-宜春市-铜鼓县', 1);
INSERT INTO `qmgx_region` VALUES (1697, '360981', '丰城市', 167, 3, 0, 'Fengcheng Shi', '2', '1-15-167-1697', '江西省-宜春市-丰城市', 1);
INSERT INTO `qmgx_region` VALUES (1698, '360982', '樟树市', 167, 3, 0, 'Zhangshu Shi', '2', '1-15-167-1698', '江西省-宜春市-樟树市', 1);
INSERT INTO `qmgx_region` VALUES (1699, '360983', '高安市', 167, 3, 0, 'Gao,an Shi', '2', '1-15-167-1699', '江西省-宜春市-高安市', 1);
INSERT INTO `qmgx_region` VALUES (1700, '361001', '市辖区', 168, 3, 0, '1', '2', '1-15-168-1700', '江西省-抚州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1701, '361002', '临川区', 168, 3, 0, 'Linchuan Qu', '2', '1-15-168-1701', '江西省-抚州市-临川区', 1);
INSERT INTO `qmgx_region` VALUES (1702, '361021', '南城县', 168, 3, 0, 'Nancheng Xian', '2', '1-15-168-1702', '江西省-抚州市-南城县', 1);
INSERT INTO `qmgx_region` VALUES (1703, '361022', '黎川县', 168, 3, 0, 'Lichuan Xian', '2', '1-15-168-1703', '江西省-抚州市-黎川县', 1);
INSERT INTO `qmgx_region` VALUES (1704, '361023', '南丰县', 168, 3, 0, 'Nanfeng Xian', '2', '1-15-168-1704', '江西省-抚州市-南丰县', 1);
INSERT INTO `qmgx_region` VALUES (1705, '361024', '崇仁县', 168, 3, 0, 'Chongren Xian', '2', '1-15-168-1705', '江西省-抚州市-崇仁县', 1);
INSERT INTO `qmgx_region` VALUES (1706, '361025', '乐安县', 168, 3, 0, 'Le,an Xian', '2', '1-15-168-1706', '江西省-抚州市-乐安县', 1);
INSERT INTO `qmgx_region` VALUES (1707, '361026', '宜黄县', 168, 3, 0, 'Yihuang Xian', '2', '1-15-168-1707', '江西省-抚州市-宜黄县', 1);
INSERT INTO `qmgx_region` VALUES (1708, '361027', '金溪县', 168, 3, 0, 'Jinxi Xian', '2', '1-15-168-1708', '江西省-抚州市-金溪县', 1);
INSERT INTO `qmgx_region` VALUES (1709, '361028', '资溪县', 168, 3, 0, 'Zixi Xian', '2', '1-15-168-1709', '江西省-抚州市-资溪县', 1);
INSERT INTO `qmgx_region` VALUES (1710, '361029', '东乡县', 168, 3, 0, 'Dongxiang Xian', '2', '1-15-168-1710', '江西省-抚州市-东乡县', 1);
INSERT INTO `qmgx_region` VALUES (1711, '361030', '广昌县', 168, 3, 0, 'Guangchang Xian', '2', '1-15-168-1711', '江西省-抚州市-广昌县', 1);
INSERT INTO `qmgx_region` VALUES (1712, '361101', '市辖区', 169, 3, 0, '1', '2', '1-15-169-1712', '江西省-上饶市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1713, '361102', '信州区', 169, 3, 0, 'Xinzhou Qu', 'XZQ', '1-15-169-1713', '江西省-上饶市-信州区', 1);
INSERT INTO `qmgx_region` VALUES (1714, '361121', '上饶县', 169, 3, 0, 'Shangrao Xian ', '2', '1-15-169-1714', '江西省-上饶市-上饶县', 1);
INSERT INTO `qmgx_region` VALUES (1715, '361122', '广丰县', 169, 3, 0, 'Guangfeng Xian', '2', '1-15-169-1715', '江西省-上饶市-广丰县', 1);
INSERT INTO `qmgx_region` VALUES (1716, '361123', '玉山县', 169, 3, 0, 'Yushan Xian', '2', '1-15-169-1716', '江西省-上饶市-玉山县', 1);
INSERT INTO `qmgx_region` VALUES (1717, '361124', '铅山县', 169, 3, 0, 'Qianshan Xian', '2', '1-15-169-1717', '江西省-上饶市-铅山县', 1);
INSERT INTO `qmgx_region` VALUES (1718, '361125', '横峰县', 169, 3, 0, 'Hengfeng Xian', '2', '1-15-169-1718', '江西省-上饶市-横峰县', 1);
INSERT INTO `qmgx_region` VALUES (1719, '361126', '弋阳县', 169, 3, 0, 'Yiyang Xian', '2', '1-15-169-1719', '江西省-上饶市-弋阳县', 1);
INSERT INTO `qmgx_region` VALUES (1720, '361127', '余干县', 169, 3, 0, 'Yugan Xian', '2', '1-15-169-1720', '江西省-上饶市-余干县', 1);
INSERT INTO `qmgx_region` VALUES (1721, '361128', '鄱阳县', 169, 3, 0, 'Poyang Xian', 'PYX', '1-15-169-1721', '江西省-上饶市-鄱阳县', 1);
INSERT INTO `qmgx_region` VALUES (1722, '361129', '万年县', 169, 3, 0, 'Wannian Xian', '2', '1-15-169-1722', '江西省-上饶市-万年县', 1);
INSERT INTO `qmgx_region` VALUES (1723, '361130', '婺源县', 169, 3, 0, 'Wuyuan Xian', '2', '1-15-169-1723', '江西省-上饶市-婺源县', 1);
INSERT INTO `qmgx_region` VALUES (1724, '361181', '德兴市', 169, 3, 0, 'Dexing Shi', '2', '1-15-169-1724', '江西省-上饶市-德兴市', 1);
INSERT INTO `qmgx_region` VALUES (1725, '370101', '市辖区', 170, 3, 0, 'Shixiaqu', '2', '1-16-170-1725', '山东省-济南市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1726, '370102', '历下区', 170, 3, 0, 'Lixia Qu', 'LXQ', '1-16-170-1726', '山东省-济南市-历下区', 1);
INSERT INTO `qmgx_region` VALUES (1727, '370101', '市中区', 170, 3, 0, 'Shizhong Qu', 'SZQ', '1-16-170-1727', '山东省-济南市-市中区', 1);
INSERT INTO `qmgx_region` VALUES (1728, '370104', '槐荫区', 170, 3, 0, 'Huaiyin Qu', 'HYF', '1-16-170-1728', '山东省-济南市-槐荫区', 1);
INSERT INTO `qmgx_region` VALUES (1729, '370105', '天桥区', 170, 3, 0, 'Tianqiao Qu', 'TQQ', '1-16-170-1729', '山东省-济南市-天桥区', 1);
INSERT INTO `qmgx_region` VALUES (1730, '370112', '历城区', 170, 3, 0, 'Licheng Qu', 'LCZ', '1-16-170-1730', '山东省-济南市-历城区', 1);
INSERT INTO `qmgx_region` VALUES (1731, '370113', '长清区', 170, 3, 0, 'Changqing Qu', '2', '1-16-170-1731', '山东省-济南市-长清区', 1);
INSERT INTO `qmgx_region` VALUES (1732, '370124', '平阴县', 170, 3, 0, 'Pingyin Xian', 'PYL', '1-16-170-1732', '山东省-济南市-平阴县', 1);
INSERT INTO `qmgx_region` VALUES (1733, '370125', '济阳县', 170, 3, 0, 'Jiyang Xian', 'JYL', '1-16-170-1733', '山东省-济南市-济阳县', 1);
INSERT INTO `qmgx_region` VALUES (1734, '370126', '商河县', 170, 3, 0, 'Shanghe Xian', 'SGH', '1-16-170-1734', '山东省-济南市-商河县', 1);
INSERT INTO `qmgx_region` VALUES (1735, '370181', '章丘市', 170, 3, 0, 'Zhangqiu Shi', 'ZQS', '1-16-170-1735', '山东省-济南市-章丘市', 1);
INSERT INTO `qmgx_region` VALUES (1736, '370201', '市辖区', 171, 3, 0, 'Shixiaqu', '2', '1-16-171-1736', '山东省-青岛市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1737, '370202', '市南区', 171, 3, 0, 'Shinan Qu', 'SNQ', '1-16-171-1737', '山东省-青岛市-市南区', 1);
INSERT INTO `qmgx_region` VALUES (1738, '370203', '市北区', 171, 3, 0, 'Shibei Qu', 'SBQ', '1-16-171-1738', '山东省-青岛市-市北区', 1);
INSERT INTO `qmgx_region` VALUES (1739, '370205', '四方区', 171, 3, 0, 'Sifang Qu', 'SFQ', '1-16-171-1739', '山东省-青岛市-四方区', 1);
INSERT INTO `qmgx_region` VALUES (1740, '370211', '黄岛区', 171, 3, 0, 'Huangdao Qu', 'HDO', '1-16-171-1740', '山东省-青岛市-黄岛区', 1);
INSERT INTO `qmgx_region` VALUES (1741, '370212', '崂山区', 171, 3, 0, 'Laoshan Qu', 'LQD', '1-16-171-1741', '山东省-青岛市-崂山区', 1);
INSERT INTO `qmgx_region` VALUES (1742, '370213', '李沧区', 171, 3, 0, 'Licang Qu', 'LCT', '1-16-171-1742', '山东省-青岛市-李沧区', 1);
INSERT INTO `qmgx_region` VALUES (1743, '370214', '城阳区', 171, 3, 0, 'Chengyang Qu', 'CEY', '1-16-171-1743', '山东省-青岛市-城阳区', 1);
INSERT INTO `qmgx_region` VALUES (1744, '370281', '胶州市', 171, 3, 0, 'Jiaozhou Shi', 'JZS', '1-16-171-1744', '山东省-青岛市-胶州市', 1);
INSERT INTO `qmgx_region` VALUES (1745, '370282', '即墨市', 171, 3, 0, 'Jimo Shi', 'JMO', '1-16-171-1745', '山东省-青岛市-即墨市', 1);
INSERT INTO `qmgx_region` VALUES (1746, '370283', '平度市', 171, 3, 0, 'Pingdu Shi', 'PDU', '1-16-171-1746', '山东省-青岛市-平度市', 1);
INSERT INTO `qmgx_region` VALUES (1747, '370284', '胶南市', 171, 3, 0, 'Jiaonan Shi', 'JNS', '1-16-171-1747', '山东省-青岛市-胶南市', 1);
INSERT INTO `qmgx_region` VALUES (1748, '370285', '莱西市', 171, 3, 0, 'Laixi Shi', 'LXE', '1-16-171-1748', '山东省-青岛市-莱西市', 1);
INSERT INTO `qmgx_region` VALUES (1749, '370301', '市辖区', 172, 3, 0, 'Shixiaqu', '2', '1-16-172-1749', '山东省-淄博市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1750, '370302', '淄川区', 172, 3, 0, 'Zichuan Qu', 'ZCQ', '1-16-172-1750', '山东省-淄博市-淄川区', 1);
INSERT INTO `qmgx_region` VALUES (1751, '370303', '张店区', 172, 3, 0, 'Zhangdian Qu', 'ZDQ', '1-16-172-1751', '山东省-淄博市-张店区', 1);
INSERT INTO `qmgx_region` VALUES (1752, '370304', '博山区', 172, 3, 0, 'Boshan Qu', 'BSZ', '1-16-172-1752', '山东省-淄博市-博山区', 1);
INSERT INTO `qmgx_region` VALUES (1753, '370305', '临淄区', 172, 3, 0, 'Linzi Qu', 'LZQ', '1-16-172-1753', '山东省-淄博市-临淄区', 1);
INSERT INTO `qmgx_region` VALUES (1754, '370306', '周村区', 172, 3, 0, 'Zhoucun Qu', 'ZCN', '1-16-172-1754', '山东省-淄博市-周村区', 1);
INSERT INTO `qmgx_region` VALUES (1755, '370321', '桓台县', 172, 3, 0, 'Huantai Xian', 'HTL', '1-16-172-1755', '山东省-淄博市-桓台县', 1);
INSERT INTO `qmgx_region` VALUES (1756, '370322', '高青县', 172, 3, 0, 'Gaoqing Xian', 'GQG', '1-16-172-1756', '山东省-淄博市-高青县', 1);
INSERT INTO `qmgx_region` VALUES (1757, '370323', '沂源县', 172, 3, 0, 'Yiyuan Xian', 'YIY', '1-16-172-1757', '山东省-淄博市-沂源县', 1);
INSERT INTO `qmgx_region` VALUES (1758, '370401', '市辖区', 173, 3, 0, 'Shixiaqu', '2', '1-16-173-1758', '山东省-枣庄市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1759, '370402', '市中区', 173, 3, 0, 'Shizhong Qu', 'SZZ', '1-16-173-1759', '山东省-枣庄市-市中区', 1);
INSERT INTO `qmgx_region` VALUES (1760, '370403', '薛城区', 173, 3, 0, 'Xuecheng Qu', 'XEC', '1-16-173-1760', '山东省-枣庄市-薛城区', 1);
INSERT INTO `qmgx_region` VALUES (1761, '370404', '峄城区', 173, 3, 0, 'Yicheng Qu', 'YZZ', '1-16-173-1761', '山东省-枣庄市-峄城区', 1);
INSERT INTO `qmgx_region` VALUES (1762, '370405', '台儿庄区', 173, 3, 0, 'Tai,erzhuang Qu', 'TEZ', '1-16-173-1762', '山东省-枣庄市-台儿庄区', 1);
INSERT INTO `qmgx_region` VALUES (1763, '370406', '山亭区', 173, 3, 0, 'Shanting Qu', 'STG', '1-16-173-1763', '山东省-枣庄市-山亭区', 1);
INSERT INTO `qmgx_region` VALUES (1764, '370481', '滕州市', 173, 3, 0, 'Tengzhou Shi', 'TZO', '1-16-173-1764', '山东省-枣庄市-滕州市', 1);
INSERT INTO `qmgx_region` VALUES (1765, '370501', '市辖区', 174, 3, 0, 'Shixiaqu', '2', '1-16-174-1765', '山东省-东营市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1766, '370502', '东营区', 174, 3, 0, 'Dongying Qu', 'DYQ', '1-16-174-1766', '山东省-东营市-东营区', 1);
INSERT INTO `qmgx_region` VALUES (1767, '370503', '河口区', 174, 3, 0, 'Hekou Qu', 'HKO', '1-16-174-1767', '山东省-东营市-河口区', 1);
INSERT INTO `qmgx_region` VALUES (1768, '370521', '垦利县', 174, 3, 0, 'Kenli Xian', 'KLI', '1-16-174-1768', '山东省-东营市-垦利县', 1);
INSERT INTO `qmgx_region` VALUES (1769, '370522', '利津县', 174, 3, 0, 'Lijin Xian', 'LJN', '1-16-174-1769', '山东省-东营市-利津县', 1);
INSERT INTO `qmgx_region` VALUES (1770, '370523', '广饶县', 174, 3, 0, 'Guangrao Xian ', 'GRO', '1-16-174-1770', '山东省-东营市-广饶县', 1);
INSERT INTO `qmgx_region` VALUES (1771, '370601', '市辖区', 175, 3, 0, 'Shixiaqu', '2', '1-16-175-1771', '山东省-烟台市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1772, '370602', '芝罘区', 175, 3, 0, 'Zhifu Qu', 'ZFQ', '1-16-175-1772', '山东省-烟台市-芝罘区', 1);
INSERT INTO `qmgx_region` VALUES (1773, '370611', '福山区', 175, 3, 0, 'Fushan Qu', 'FUS', '1-16-175-1773', '山东省-烟台市-福山区', 1);
INSERT INTO `qmgx_region` VALUES (1774, '370612', '牟平区', 175, 3, 0, 'Muping Qu', 'MPQ', '1-16-175-1774', '山东省-烟台市-牟平区', 1);
INSERT INTO `qmgx_region` VALUES (1775, '370613', '莱山区', 175, 3, 0, 'Laishan Qu', 'LYT', '1-16-175-1775', '山东省-烟台市-莱山区', 1);
INSERT INTO `qmgx_region` VALUES (1776, '370634', '长岛县', 175, 3, 0, 'Changdao Xian', 'CDO', '1-16-175-1776', '山东省-烟台市-长岛县', 1);
INSERT INTO `qmgx_region` VALUES (1777, '370681', '龙口市', 175, 3, 0, 'Longkou Shi', 'LKU', '1-16-175-1777', '山东省-烟台市-龙口市', 1);
INSERT INTO `qmgx_region` VALUES (1778, '370682', '莱阳市', 175, 3, 0, 'Laiyang Shi', 'LYD', '1-16-175-1778', '山东省-烟台市-莱阳市', 1);
INSERT INTO `qmgx_region` VALUES (1779, '370683', '莱州市', 175, 3, 0, 'Laizhou Shi', 'LZG', '1-16-175-1779', '山东省-烟台市-莱州市', 1);
INSERT INTO `qmgx_region` VALUES (1780, '370684', '蓬莱市', 175, 3, 0, 'Penglai Shi', 'PLI', '1-16-175-1780', '山东省-烟台市-蓬莱市', 1);
INSERT INTO `qmgx_region` VALUES (1781, '370685', '招远市', 175, 3, 0, 'Zhaoyuan Shi', 'ZYL', '1-16-175-1781', '山东省-烟台市-招远市', 1);
INSERT INTO `qmgx_region` VALUES (1782, '370686', '栖霞市', 175, 3, 0, 'Qixia Shi', 'QXS', '1-16-175-1782', '山东省-烟台市-栖霞市', 1);
INSERT INTO `qmgx_region` VALUES (1783, '370687', '海阳市', 175, 3, 0, 'Haiyang Shi', 'HYL', '1-16-175-1783', '山东省-烟台市-海阳市', 1);
INSERT INTO `qmgx_region` VALUES (1784, '370701', '市辖区', 176, 3, 0, 'Shixiaqu', '2', '1-16-176-1784', '山东省-潍坊市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1785, '370702', '潍城区', 176, 3, 0, 'Weicheng Qu', 'WCG', '1-16-176-1785', '山东省-潍坊市-潍城区', 1);
INSERT INTO `qmgx_region` VALUES (1786, '370703', '寒亭区', 176, 3, 0, 'Hanting Qu', 'HNT', '1-16-176-1786', '山东省-潍坊市-寒亭区', 1);
INSERT INTO `qmgx_region` VALUES (1787, '370704', '坊子区', 176, 3, 0, 'Fangzi Qu', 'FZQ', '1-16-176-1787', '山东省-潍坊市-坊子区', 1);
INSERT INTO `qmgx_region` VALUES (1788, '370705', '奎文区', 176, 3, 0, 'Kuiwen Qu', 'KWN', '1-16-176-1788', '山东省-潍坊市-奎文区', 1);
INSERT INTO `qmgx_region` VALUES (1789, '370724', '临朐县', 176, 3, 0, 'Linqu Xian', 'LNQ', '1-16-176-1789', '山东省-潍坊市-临朐县', 1);
INSERT INTO `qmgx_region` VALUES (1790, '370725', '昌乐县', 176, 3, 0, 'Changle Xian', 'CLX', '1-16-176-1790', '山东省-潍坊市-昌乐县', 1);
INSERT INTO `qmgx_region` VALUES (1791, '370781', '青州市', 176, 3, 0, 'Qingzhou Shi', 'QGZ', '1-16-176-1791', '山东省-潍坊市-青州市', 1);
INSERT INTO `qmgx_region` VALUES (1792, '370782', '诸城市', 176, 3, 0, 'Zhucheng Shi', 'ZCL', '1-16-176-1792', '山东省-潍坊市-诸城市', 1);
INSERT INTO `qmgx_region` VALUES (1793, '370783', '寿光市', 176, 3, 0, 'Shouguang Shi', 'SGG', '1-16-176-1793', '山东省-潍坊市-寿光市', 1);
INSERT INTO `qmgx_region` VALUES (1794, '370784', '安丘市', 176, 3, 0, 'Anqiu Shi', 'AQU', '1-16-176-1794', '山东省-潍坊市-安丘市', 1);
INSERT INTO `qmgx_region` VALUES (1795, '370785', '高密市', 176, 3, 0, 'Gaomi Shi', 'GMI', '1-16-176-1795', '山东省-潍坊市-高密市', 1);
INSERT INTO `qmgx_region` VALUES (1796, '370786', '昌邑市', 176, 3, 0, 'Changyi Shi', 'CYL', '1-16-176-1796', '山东省-潍坊市-昌邑市', 1);
INSERT INTO `qmgx_region` VALUES (1797, '370801', '市辖区', 177, 3, 0, 'Shixiaqu', '2', '1-16-177-1797', '山东省-济宁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1798, '370802', '市中区', 177, 3, 0, 'Shizhong Qu', 'SZU', '1-16-177-1798', '山东省-济宁市-市中区', 1);
INSERT INTO `qmgx_region` VALUES (1799, '370811', '任城区', 177, 3, 0, 'Rencheng Qu', 'RCQ', '1-16-177-1799', '山东省-济宁市-任城区', 1);
INSERT INTO `qmgx_region` VALUES (1800, '370826', '微山县', 177, 3, 0, 'Weishan Xian', 'WSA', '1-16-177-1800', '山东省-济宁市-微山县', 1);
INSERT INTO `qmgx_region` VALUES (1801, '370827', '鱼台县', 177, 3, 0, 'Yutai Xian', 'YTL', '1-16-177-1801', '山东省-济宁市-鱼台县', 1);
INSERT INTO `qmgx_region` VALUES (1802, '370828', '金乡县', 177, 3, 0, 'Jinxiang Xian', 'JXG', '1-16-177-1802', '山东省-济宁市-金乡县', 1);
INSERT INTO `qmgx_region` VALUES (1803, '370829', '嘉祥县', 177, 3, 0, 'Jiaxiang Xian', 'JXP', '1-16-177-1803', '山东省-济宁市-嘉祥县', 1);
INSERT INTO `qmgx_region` VALUES (1804, '370830', '汶上县', 177, 3, 0, 'Wenshang Xian', 'WNS', '1-16-177-1804', '山东省-济宁市-汶上县', 1);
INSERT INTO `qmgx_region` VALUES (1805, '370831', '泗水县', 177, 3, 0, 'Sishui Xian', 'SSH', '1-16-177-1805', '山东省-济宁市-泗水县', 1);
INSERT INTO `qmgx_region` VALUES (1806, '370832', '梁山县', 177, 3, 0, 'Liangshan Xian', 'LSN', '1-16-177-1806', '山东省-济宁市-梁山县', 1);
INSERT INTO `qmgx_region` VALUES (1807, '370881', '曲阜市', 177, 3, 0, 'Qufu Shi', 'QFU', '1-16-177-1807', '山东省-济宁市-曲阜市', 1);
INSERT INTO `qmgx_region` VALUES (1808, '370882', '兖州市', 177, 3, 0, 'Yanzhou Shi', 'YZL', '1-16-177-1808', '山东省-济宁市-兖州市', 1);
INSERT INTO `qmgx_region` VALUES (1809, '370883', '邹城市', 177, 3, 0, 'Zoucheng Shi', 'ZCG', '1-16-177-1809', '山东省-济宁市-邹城市', 1);
INSERT INTO `qmgx_region` VALUES (1810, '370901', '市辖区', 178, 3, 0, 'Shixiaqu', '2', '1-16-178-1810', '山东省-泰安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1811, '370902', '泰山区', 178, 3, 0, 'Taishan Qu', 'TSQ', '1-16-178-1811', '山东省-泰安市-泰山区', 1);
INSERT INTO `qmgx_region` VALUES (1812, '370911', '岱岳区', 178, 3, 0, 'Daiyue Qu', '2', '1-16-178-1812', '山东省-泰安市-岱岳区', 1);
INSERT INTO `qmgx_region` VALUES (1813, '370921', '宁阳县', 178, 3, 0, 'Ningyang Xian', 'NGY', '1-16-178-1813', '山东省-泰安市-宁阳县', 1);
INSERT INTO `qmgx_region` VALUES (1814, '370923', '东平县', 178, 3, 0, 'Dongping Xian', 'DPG', '1-16-178-1814', '山东省-泰安市-东平县', 1);
INSERT INTO `qmgx_region` VALUES (1815, '370982', '新泰市', 178, 3, 0, 'Xintai Shi', 'XTA', '1-16-178-1815', '山东省-泰安市-新泰市', 1);
INSERT INTO `qmgx_region` VALUES (1816, '370983', '肥城市', 178, 3, 0, 'Feicheng Shi', 'FEC', '1-16-178-1816', '山东省-泰安市-肥城市', 1);
INSERT INTO `qmgx_region` VALUES (1817, '371001', '市辖区', 179, 3, 0, 'Shixiaqu', '2', '1-16-179-1817', '山东省-威海市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1818, '371002', '环翠区', 179, 3, 0, 'Huancui Qu', 'HNC', '1-16-179-1818', '山东省-威海市-环翠区', 1);
INSERT INTO `qmgx_region` VALUES (1819, '371081', '文登市', 179, 3, 0, 'Wendeng Shi', 'WDS', '1-16-179-1819', '山东省-威海市-文登市', 1);
INSERT INTO `qmgx_region` VALUES (1820, '371082', '荣成市', 179, 3, 0, 'Rongcheng Shi', '2', '1-16-179-1820', '山东省-威海市-荣成市', 1);
INSERT INTO `qmgx_region` VALUES (1821, '371083', '乳山市', 179, 3, 0, 'Rushan Shi', 'RSN', '1-16-179-1821', '山东省-威海市-乳山市', 1);
INSERT INTO `qmgx_region` VALUES (1822, '371101', '市辖区', 180, 3, 0, 'Shixiaqu', '2', '1-16-180-1822', '山东省-日照市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1823, '371102', '东港区', 180, 3, 0, 'Donggang Qu', 'DGR', '1-16-180-1823', '山东省-日照市-东港区', 1);
INSERT INTO `qmgx_region` VALUES (1824, '371103', '岚山区', 180, 3, 0, 'Lanshan Qu', '2', '1-16-180-1824', '山东省-日照市-岚山区', 1);
INSERT INTO `qmgx_region` VALUES (1825, '371121', '五莲县', 180, 3, 0, 'Wulian Xian', 'WLN', '1-16-180-1825', '山东省-日照市-五莲县', 1);
INSERT INTO `qmgx_region` VALUES (1826, '371122', '莒县', 180, 3, 0, 'Ju Xian', 'JUX', '1-16-180-1826', '山东省-日照市-莒县', 1);
INSERT INTO `qmgx_region` VALUES (1827, '371201', '市辖区', 181, 3, 0, 'Shixiaqu', '2', '1-16-181-1827', '山东省-莱芜市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1828, '371202', '莱城区', 181, 3, 0, 'Laicheng Qu', 'LAC', '1-16-181-1828', '山东省-莱芜市-莱城区', 1);
INSERT INTO `qmgx_region` VALUES (1829, '371203', '钢城区', 181, 3, 0, 'Gangcheng Qu', 'GCQ', '1-16-181-1829', '山东省-莱芜市-钢城区', 1);
INSERT INTO `qmgx_region` VALUES (1830, '371301', '市辖区', 182, 3, 0, 'Shixiaqu', '2', '1-16-182-1830', '山东省-临沂市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1831, '371302', '兰山区', 182, 3, 0, 'Lanshan Qu', 'LLS', '1-16-182-1831', '山东省-临沂市-兰山区', 1);
INSERT INTO `qmgx_region` VALUES (1832, '371311', '罗庄区', 182, 3, 0, 'Luozhuang Qu', 'LZU', '1-16-182-1832', '山东省-临沂市-罗庄区', 1);
INSERT INTO `qmgx_region` VALUES (1833, '371301', '河东区', 182, 3, 0, 'Hedong Qu', '2', '1-16-182-1833', '山东省-临沂市-河东区', 1);
INSERT INTO `qmgx_region` VALUES (1834, '371321', '沂南县', 182, 3, 0, 'Yinan Xian', 'YNN', '1-16-182-1834', '山东省-临沂市-沂南县', 1);
INSERT INTO `qmgx_region` VALUES (1835, '371322', '郯城县', 182, 3, 0, 'Tancheng Xian', 'TCE', '1-16-182-1835', '山东省-临沂市-郯城县', 1);
INSERT INTO `qmgx_region` VALUES (1836, '371323', '沂水县', 182, 3, 0, 'Yishui Xian', 'YIS', '1-16-182-1836', '山东省-临沂市-沂水县', 1);
INSERT INTO `qmgx_region` VALUES (1837, '371324', '苍山县', 182, 3, 0, 'Cangshan Xian', 'CSH', '1-16-182-1837', '山东省-临沂市-苍山县', 1);
INSERT INTO `qmgx_region` VALUES (1838, '371325', '费县', 182, 3, 0, 'Fei Xian', 'FEI', '1-16-182-1838', '山东省-临沂市-费县', 1);
INSERT INTO `qmgx_region` VALUES (1839, '371326', '平邑县', 182, 3, 0, 'Pingyi Xian', 'PYI', '1-16-182-1839', '山东省-临沂市-平邑县', 1);
INSERT INTO `qmgx_region` VALUES (1840, '371327', '莒南县', 182, 3, 0, 'Junan Xian', 'JNB', '1-16-182-1840', '山东省-临沂市-莒南县', 1);
INSERT INTO `qmgx_region` VALUES (1841, '371328', '蒙阴县', 182, 3, 0, 'Mengyin Xian', 'MYL', '1-16-182-1841', '山东省-临沂市-蒙阴县', 1);
INSERT INTO `qmgx_region` VALUES (1842, '371329', '临沭县', 182, 3, 0, 'Linshu Xian', 'LSP', '1-16-182-1842', '山东省-临沂市-临沭县', 1);
INSERT INTO `qmgx_region` VALUES (1843, '371401', '市辖区', 183, 3, 0, 'Shixiaqu', '2', '1-16-183-1843', '山东省-德州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1844, '371402', '德城区', 183, 3, 0, 'Decheng Qu', 'DCD', '1-16-183-1844', '山东省-德州市-德城区', 1);
INSERT INTO `qmgx_region` VALUES (1845, '371421', '陵县', 183, 3, 0, 'Ling Xian', 'LXL', '1-16-183-1845', '山东省-德州市-陵县', 1);
INSERT INTO `qmgx_region` VALUES (1846, '371422', '宁津县', 183, 3, 0, 'Ningjin Xian', 'NGJ', '1-16-183-1846', '山东省-德州市-宁津县', 1);
INSERT INTO `qmgx_region` VALUES (1847, '371423', '庆云县', 183, 3, 0, 'Qingyun Xian', 'QYL', '1-16-183-1847', '山东省-德州市-庆云县', 1);
INSERT INTO `qmgx_region` VALUES (1848, '371424', '临邑县', 183, 3, 0, 'Linyi xian', 'LYM', '1-16-183-1848', '山东省-德州市-临邑县', 1);
INSERT INTO `qmgx_region` VALUES (1849, '371425', '齐河县', 183, 3, 0, 'Qihe Xian', 'QIH', '1-16-183-1849', '山东省-德州市-齐河县', 1);
INSERT INTO `qmgx_region` VALUES (1850, '371426', '平原县', 183, 3, 0, 'Pingyuan Xian', 'PYN', '1-16-183-1850', '山东省-德州市-平原县', 1);
INSERT INTO `qmgx_region` VALUES (1851, '371427', '夏津县', 183, 3, 0, 'Xiajin Xian', 'XAJ', '1-16-183-1851', '山东省-德州市-夏津县', 1);
INSERT INTO `qmgx_region` VALUES (1852, '371428', '武城县', 183, 3, 0, 'Wucheng Xian', 'WUC', '1-16-183-1852', '山东省-德州市-武城县', 1);
INSERT INTO `qmgx_region` VALUES (1853, '371481', '乐陵市', 183, 3, 0, 'Leling Shi', 'LEL', '1-16-183-1853', '山东省-德州市-乐陵市', 1);
INSERT INTO `qmgx_region` VALUES (1854, '371482', '禹城市', 183, 3, 0, 'Yucheng Shi', 'YCL', '1-16-183-1854', '山东省-德州市-禹城市', 1);
INSERT INTO `qmgx_region` VALUES (1855, '371501', '市辖区', 184, 3, 0, 'Shixiaqu', '2', '1-16-184-1855', '山东省-聊城市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1856, '371502', '东昌府区', 184, 3, 0, 'Dongchangfu Qu', 'DCF', '1-16-184-1856', '山东省-聊城市-东昌府区', 1);
INSERT INTO `qmgx_region` VALUES (1857, '371521', '阳谷县', 184, 3, 0, 'Yanggu Xian ', 'YGU', '1-16-184-1857', '山东省-聊城市-阳谷县', 1);
INSERT INTO `qmgx_region` VALUES (1858, '371522', '莘县', 184, 3, 0, 'Shen Xian', 'SHN', '1-16-184-1858', '山东省-聊城市-莘县', 1);
INSERT INTO `qmgx_region` VALUES (1859, '371523', '茌平县', 184, 3, 0, 'Chiping Xian ', 'CPG', '1-16-184-1859', '山东省-聊城市-茌平县', 1);
INSERT INTO `qmgx_region` VALUES (1860, '371524', '东阿县', 184, 3, 0, 'Dong,e Xian', 'DGE', '1-16-184-1860', '山东省-聊城市-东阿县', 1);
INSERT INTO `qmgx_region` VALUES (1861, '371525', '冠县', 184, 3, 0, 'Guan Xian', 'GXL', '1-16-184-1861', '山东省-聊城市-冠县', 1);
INSERT INTO `qmgx_region` VALUES (1862, '371526', '高唐县', 184, 3, 0, 'Gaotang Xian', 'GTG', '1-16-184-1862', '山东省-聊城市-高唐县', 1);
INSERT INTO `qmgx_region` VALUES (1863, '371581', '临清市', 184, 3, 0, 'Linqing Xian', 'LQS', '1-16-184-1863', '山东省-聊城市-临清市', 1);
INSERT INTO `qmgx_region` VALUES (1864, '371601', '市辖区', 185, 3, 0, '1', '2', '1-16-185-1864', '山东省-滨州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1865, '371602', '滨城区', 185, 3, 0, 'Bincheng Qu', '2', '1-16-185-1865', '山东省-滨州市-滨城区', 1);
INSERT INTO `qmgx_region` VALUES (1866, '371621', '惠民县', 185, 3, 0, 'Huimin Xian', '2', '1-16-185-1866', '山东省-滨州市-惠民县', 1);
INSERT INTO `qmgx_region` VALUES (1867, '371622', '阳信县', 185, 3, 0, 'Yangxin Xian', '2', '1-16-185-1867', '山东省-滨州市-阳信县', 1);
INSERT INTO `qmgx_region` VALUES (1868, '371623', '无棣县', 185, 3, 0, 'Wudi Xian', '2', '1-16-185-1868', '山东省-滨州市-无棣县', 1);
INSERT INTO `qmgx_region` VALUES (1869, '371624', '沾化县', 185, 3, 0, 'Zhanhua Xian', '2', '1-16-185-1869', '山东省-滨州市-沾化县', 1);
INSERT INTO `qmgx_region` VALUES (1870, '371625', '博兴县', 185, 3, 0, 'Boxing Xian', '2', '1-16-185-1870', '山东省-滨州市-博兴县', 1);
INSERT INTO `qmgx_region` VALUES (1871, '371626', '邹平县', 185, 3, 0, 'Zouping Xian', '2', '1-16-185-1871', '山东省-滨州市-邹平县', 1);
INSERT INTO `qmgx_region` VALUES (1873, '371702', '牡丹区', 186, 3, 0, 'Mudan Qu', '2', '1-16-186-1873', '山东省-菏泽市-牡丹区', 1);
INSERT INTO `qmgx_region` VALUES (1874, '371721', '曹县', 186, 3, 0, 'Cao Xian', '2', '1-16-186-1874', '山东省-菏泽市-曹县', 1);
INSERT INTO `qmgx_region` VALUES (1875, '371722', '单县', 186, 3, 0, 'Shan Xian', '2', '1-16-186-1875', '山东省-菏泽市-单县', 1);
INSERT INTO `qmgx_region` VALUES (1876, '371723', '成武县', 186, 3, 0, 'Chengwu Xian', '2', '1-16-186-1876', '山东省-菏泽市-成武县', 1);
INSERT INTO `qmgx_region` VALUES (1877, '371724', '巨野县', 186, 3, 0, 'Juye Xian', '2', '1-16-186-1877', '山东省-菏泽市-巨野县', 1);
INSERT INTO `qmgx_region` VALUES (1878, '371725', '郓城县', 186, 3, 0, 'Yuncheng Xian', '2', '1-16-186-1878', '山东省-菏泽市-郓城县', 1);
INSERT INTO `qmgx_region` VALUES (1879, '371726', '鄄城县', 186, 3, 0, 'Juancheng Xian', '2', '1-16-186-1879', '山东省-菏泽市-鄄城县', 1);
INSERT INTO `qmgx_region` VALUES (1880, '371727', '定陶县', 186, 3, 0, 'Dingtao Xian', '2', '1-16-186-1880', '山东省-菏泽市-定陶县', 1);
INSERT INTO `qmgx_region` VALUES (1881, '371728', '东明县', 186, 3, 0, 'Dongming Xian', '2', '1-16-186-1881', '山东省-菏泽市-东明县', 1);
INSERT INTO `qmgx_region` VALUES (1882, '410101', '市辖区', 187, 3, 0, 'Shixiaqu', '2', '1-17-187-1882', '河南省-郑州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1883, '410102', '中原区', 187, 3, 0, 'Zhongyuan Qu', 'ZYQ', '1-17-187-1883', '河南省-郑州市-中原区', 1);
INSERT INTO `qmgx_region` VALUES (1884, '410103', '二七区', 187, 3, 0, 'Erqi Qu', 'EQQ', '1-17-187-1884', '河南省-郑州市-二七区', 1);
INSERT INTO `qmgx_region` VALUES (1885, '410104', '管城回族区', 187, 3, 0, 'Guancheng Huizu Qu', 'GCH', '1-17-187-1885', '河南省-郑州市-管城回族区', 1);
INSERT INTO `qmgx_region` VALUES (1886, '410105', '金水区', 187, 3, 0, 'Jinshui Qu', 'JSU', '1-17-187-1886', '河南省-郑州市-金水区', 1);
INSERT INTO `qmgx_region` VALUES (1887, '410106', '上街区', 187, 3, 0, 'Shangjie Qu', 'SJE', '1-17-187-1887', '河南省-郑州市-上街区', 1);
INSERT INTO `qmgx_region` VALUES (1888, '410108', '惠济区', 187, 3, 0, 'Mangshan Qu', '2', '1-17-187-1888', '河南省-郑州市-惠济区', 1);
INSERT INTO `qmgx_region` VALUES (1889, '410122', '中牟县', 187, 3, 0, 'Zhongmou Xian', 'ZMO', '1-17-187-1889', '河南省-郑州市-中牟县', 1);
INSERT INTO `qmgx_region` VALUES (1890, '410181', '巩义市', 187, 3, 0, 'Gongyi Shi', 'GYI', '1-17-187-1890', '河南省-郑州市-巩义市', 1);
INSERT INTO `qmgx_region` VALUES (1891, '410182', '荥阳市', 187, 3, 0, 'Xingyang Shi', 'XYK', '1-17-187-1891', '河南省-郑州市-荥阳市', 1);
INSERT INTO `qmgx_region` VALUES (1892, '410183', '新密市', 187, 3, 0, 'Xinmi Shi', 'XMI', '1-17-187-1892', '河南省-郑州市-新密市', 1);
INSERT INTO `qmgx_region` VALUES (1893, '410184', '新郑市', 187, 3, 0, 'Xinzheng Shi', 'XZG', '1-17-187-1893', '河南省-郑州市-新郑市', 1);
INSERT INTO `qmgx_region` VALUES (1894, '410185', '登封市', 187, 3, 0, 'Dengfeng Shi', '2', '1-17-187-1894', '河南省-郑州市-登封市', 1);
INSERT INTO `qmgx_region` VALUES (1895, '410201', '市辖区', 188, 3, 0, 'Shixiaqu', '2', '1-17-188-1895', '河南省-开封市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1896, '410202', '龙亭区', 188, 3, 0, 'Longting Qu', 'LTK', '1-17-188-1896', '河南省-开封市-龙亭区', 1);
INSERT INTO `qmgx_region` VALUES (1897, '410203', '顺河回族区', 188, 3, 0, 'Shunhe Huizu Qu', 'SHR', '1-17-188-1897', '河南省-开封市-顺河回族区', 1);
INSERT INTO `qmgx_region` VALUES (1898, '410204', '鼓楼区', 188, 3, 0, 'Gulou Qu', 'GLK', '1-17-188-1898', '河南省-开封市-鼓楼区', 1);
INSERT INTO `qmgx_region` VALUES (1899, '410205', '禹王台区', 188, 3, 0, 'Yuwangtai Qu', '2', '1-17-188-1899', '河南省-开封市-禹王台区', 1);
INSERT INTO `qmgx_region` VALUES (1900, '410211', '金明区', 188, 3, 0, 'Jinming Qu', '2', '1-17-188-1900', '河南省-开封市-金明区', 1);
INSERT INTO `qmgx_region` VALUES (1901, '410221', '杞县', 188, 3, 0, 'Qi Xian', 'QIX', '1-17-188-1901', '河南省-开封市-杞县', 1);
INSERT INTO `qmgx_region` VALUES (1902, '410222', '通许县', 188, 3, 0, 'Tongxu Xian', 'TXY', '1-17-188-1902', '河南省-开封市-通许县', 1);
INSERT INTO `qmgx_region` VALUES (1903, '410223', '尉氏县', 188, 3, 0, 'Weishi Xian', 'WSI', '1-17-188-1903', '河南省-开封市-尉氏县', 1);
INSERT INTO `qmgx_region` VALUES (1904, '410224', '开封县', 188, 3, 0, 'Kaifeng Xian', 'KFX', '1-17-188-1904', '河南省-开封市-开封县', 1);
INSERT INTO `qmgx_region` VALUES (1905, '410225', '兰考县', 188, 3, 0, 'Lankao Xian', 'LKA', '1-17-188-1905', '河南省-开封市-兰考县', 1);
INSERT INTO `qmgx_region` VALUES (1906, '410301', '市辖区', 189, 3, 0, 'Shixiaqu', '2', '1-17-189-1906', '河南省-洛阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1907, '410302', '老城区', 189, 3, 0, 'Laocheng Qu', 'LLY', '1-17-189-1907', '河南省-洛阳市-老城区', 1);
INSERT INTO `qmgx_region` VALUES (1908, '410303', '西工区', 189, 3, 0, 'Xigong Qu', 'XGL', '1-17-189-1908', '河南省-洛阳市-西工区', 1);
INSERT INTO `qmgx_region` VALUES (1909, '410304', '瀍河回族区', 189, 3, 0, 'Chanhehuizu Qu', '2', '1-17-189-1909', '河南省-洛阳市-瀍河回族区', 1);
INSERT INTO `qmgx_region` VALUES (1910, '410305', '涧西区', 189, 3, 0, 'Jianxi Qu', 'JXL', '1-17-189-1910', '河南省-洛阳市-涧西区', 1);
INSERT INTO `qmgx_region` VALUES (1911, '410306', '吉利区', 189, 3, 0, 'Jili Qu', 'JLL', '1-17-189-1911', '河南省-洛阳市-吉利区', 1);
INSERT INTO `qmgx_region` VALUES (1912, '410311', '洛龙区', 189, 3, 0, 'Luolong Qu', '2', '1-17-189-1912', '河南省-洛阳市-洛龙区', 1);
INSERT INTO `qmgx_region` VALUES (1913, '410322', '孟津县', 189, 3, 0, 'Mengjin Xian', 'MGJ', '1-17-189-1913', '河南省-洛阳市-孟津县', 1);
INSERT INTO `qmgx_region` VALUES (1914, '410323', '新安县', 189, 3, 0, 'Xin,an Xian', 'XAX', '1-17-189-1914', '河南省-洛阳市-新安县', 1);
INSERT INTO `qmgx_region` VALUES (1915, '410324', '栾川县', 189, 3, 0, 'Luanchuan Xian', 'LCK', '1-17-189-1915', '河南省-洛阳市-栾川县', 1);
INSERT INTO `qmgx_region` VALUES (1916, '410325', '嵩县', 189, 3, 0, 'Song Xian', 'SON', '1-17-189-1916', '河南省-洛阳市-嵩县', 1);
INSERT INTO `qmgx_region` VALUES (1917, '410326', '汝阳县', 189, 3, 0, 'Ruyang Xian', 'RUY', '1-17-189-1917', '河南省-洛阳市-汝阳县', 1);
INSERT INTO `qmgx_region` VALUES (1918, '410327', '宜阳县', 189, 3, 0, 'Yiyang Xian', 'YYY', '1-17-189-1918', '河南省-洛阳市-宜阳县', 1);
INSERT INTO `qmgx_region` VALUES (1919, '410328', '洛宁县', 189, 3, 0, 'Luoning Xian', 'LNI', '1-17-189-1919', '河南省-洛阳市-洛宁县', 1);
INSERT INTO `qmgx_region` VALUES (1920, '410329', '伊川县', 189, 3, 0, 'Yichuan Xian', 'YCZ', '1-17-189-1920', '河南省-洛阳市-伊川县', 1);
INSERT INTO `qmgx_region` VALUES (1921, '410381', '偃师市', 189, 3, 0, 'Yanshi Shi', 'YST', '1-17-189-1921', '河南省-洛阳市-偃师市', 1);
INSERT INTO `qmgx_region` VALUES (1922, '410401', '市辖区', 190, 3, 0, 'Shixiaqu', '2', '1-17-190-1922', '河南省-平顶山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1923, '410402', '新华区', 190, 3, 0, 'Xinhua Qu', 'XHP', '1-17-190-1923', '河南省-平顶山市-新华区', 1);
INSERT INTO `qmgx_region` VALUES (1924, '410403', '卫东区', 190, 3, 0, 'Weidong Qu', 'WDG', '1-17-190-1924', '河南省-平顶山市-卫东区', 1);
INSERT INTO `qmgx_region` VALUES (1925, '410404', '石龙区', 190, 3, 0, 'Shilong Qu', 'SIL', '1-17-190-1925', '河南省-平顶山市-石龙区', 1);
INSERT INTO `qmgx_region` VALUES (1926, '410411', '湛河区', 190, 3, 0, 'Zhanhe Qu', 'ZHQ', '1-17-190-1926', '河南省-平顶山市-湛河区', 1);
INSERT INTO `qmgx_region` VALUES (1927, '410421', '宝丰县', 190, 3, 0, 'Baofeng Xian', 'BFG', '1-17-190-1927', '河南省-平顶山市-宝丰县', 1);
INSERT INTO `qmgx_region` VALUES (1928, '410422', '叶县', 190, 3, 0, 'Ye Xian', 'YEX', '1-17-190-1928', '河南省-平顶山市-叶县', 1);
INSERT INTO `qmgx_region` VALUES (1929, '410423', '鲁山县', 190, 3, 0, 'Lushan Xian', 'LUS', '1-17-190-1929', '河南省-平顶山市-鲁山县', 1);
INSERT INTO `qmgx_region` VALUES (1930, '410425', '郏县', 190, 3, 0, 'Jia Xian', 'JXY', '1-17-190-1930', '河南省-平顶山市-郏县', 1);
INSERT INTO `qmgx_region` VALUES (1931, '410481', '舞钢市', 190, 3, 0, 'Wugang Shi', 'WGY', '1-17-190-1931', '河南省-平顶山市-舞钢市', 1);
INSERT INTO `qmgx_region` VALUES (1932, '410482', '汝州市', 190, 3, 0, 'Ruzhou Shi', 'RZO', '1-17-190-1932', '河南省-平顶山市-汝州市', 1);
INSERT INTO `qmgx_region` VALUES (1933, '410501', '市辖区', 191, 3, 0, 'Shixiaqu', '2', '1-17-191-1933', '河南省-安阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1934, '410502', '文峰区', 191, 3, 0, 'Wenfeng Qu', 'WFQ', '1-17-191-1934', '河南省-安阳市-文峰区', 1);
INSERT INTO `qmgx_region` VALUES (1935, '410503', '北关区', 191, 3, 0, 'Beiguan Qu', 'BGQ', '1-17-191-1935', '河南省-安阳市-北关区', 1);
INSERT INTO `qmgx_region` VALUES (1936, '410505', '殷都区', 191, 3, 0, 'Yindu Qu', '2', '1-17-191-1936', '河南省-安阳市-殷都区', 1);
INSERT INTO `qmgx_region` VALUES (1937, '410506', '龙安区', 191, 3, 0, 'Longan Qu', '2', '1-17-191-1937', '河南省-安阳市-龙安区', 1);
INSERT INTO `qmgx_region` VALUES (1938, '410522', '安阳县', 191, 3, 0, 'Anyang Xian', 'AYX', '1-17-191-1938', '河南省-安阳市-安阳县', 1);
INSERT INTO `qmgx_region` VALUES (1939, '410523', '汤阴县', 191, 3, 0, 'Tangyin Xian', 'TYI', '1-17-191-1939', '河南省-安阳市-汤阴县', 1);
INSERT INTO `qmgx_region` VALUES (1940, '410526', '滑县', 191, 3, 0, 'Hua Xian', 'HUA', '1-17-191-1940', '河南省-安阳市-滑县', 1);
INSERT INTO `qmgx_region` VALUES (1941, '410527', '内黄县', 191, 3, 0, 'Neihuang Xian', 'NHG', '1-17-191-1941', '河南省-安阳市-内黄县', 1);
INSERT INTO `qmgx_region` VALUES (1942, '410581', '林州市', 191, 3, 0, 'Linzhou Shi', 'LZY', '1-17-191-1942', '河南省-安阳市-林州市', 1);
INSERT INTO `qmgx_region` VALUES (1943, '410601', '市辖区', 192, 3, 0, 'Shixiaqu', '2', '1-17-192-1943', '河南省-鹤壁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1944, '410602', '鹤山区', 192, 3, 0, 'Heshan Qu', 'HSF', '1-17-192-1944', '河南省-鹤壁市-鹤山区', 1);
INSERT INTO `qmgx_region` VALUES (1945, '410603', '山城区', 192, 3, 0, 'Shancheng Qu', 'SCB', '1-17-192-1945', '河南省-鹤壁市-山城区', 1);
INSERT INTO `qmgx_region` VALUES (1946, '410611', '淇滨区', 192, 3, 0, 'Qibin Qu', '2', '1-17-192-1946', '河南省-鹤壁市-淇滨区', 1);
INSERT INTO `qmgx_region` VALUES (1947, '410621', '浚县', 192, 3, 0, 'Xun Xian', 'XUX', '1-17-192-1947', '河南省-鹤壁市-浚县', 1);
INSERT INTO `qmgx_region` VALUES (1948, '410622', '淇县', 192, 3, 0, 'Qi Xian', 'QXY', '1-17-192-1948', '河南省-鹤壁市-淇县', 1);
INSERT INTO `qmgx_region` VALUES (1949, '410701', '市辖区', 193, 3, 0, 'Shixiaqu', '2', '1-17-193-1949', '河南省-新乡市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1950, '410702', '红旗区', 193, 3, 0, 'Hongqi Qu', 'HQQ', '1-17-193-1950', '河南省-新乡市-红旗区', 1);
INSERT INTO `qmgx_region` VALUES (1951, '410703', '卫滨区', 193, 3, 0, 'Weibin Qu', '2', '1-17-193-1951', '河南省-新乡市-卫滨区', 1);
INSERT INTO `qmgx_region` VALUES (1952, '410704', '凤泉区', 193, 3, 0, 'Fengquan Qu', '2', '1-17-193-1952', '河南省-新乡市-凤泉区', 1);
INSERT INTO `qmgx_region` VALUES (1953, '410711', '牧野区', 193, 3, 0, 'Muye Qu', '2', '1-17-193-1953', '河南省-新乡市-牧野区', 1);
INSERT INTO `qmgx_region` VALUES (1954, '410721', '新乡县', 193, 3, 0, 'Xinxiang Xian', 'XXX', '1-17-193-1954', '河南省-新乡市-新乡县', 1);
INSERT INTO `qmgx_region` VALUES (1955, '410724', '获嘉县', 193, 3, 0, 'Huojia Xian', 'HOJ', '1-17-193-1955', '河南省-新乡市-获嘉县', 1);
INSERT INTO `qmgx_region` VALUES (1956, '410725', '原阳县', 193, 3, 0, 'Yuanyang Xian', 'YYA', '1-17-193-1956', '河南省-新乡市-原阳县', 1);
INSERT INTO `qmgx_region` VALUES (1957, '410726', '延津县', 193, 3, 0, 'Yanjin Xian', 'YJN', '1-17-193-1957', '河南省-新乡市-延津县', 1);
INSERT INTO `qmgx_region` VALUES (1958, '410727', '封丘县', 193, 3, 0, 'Fengqiu Xian', 'FQU', '1-17-193-1958', '河南省-新乡市-封丘县', 1);
INSERT INTO `qmgx_region` VALUES (1959, '410728', '长垣县', 193, 3, 0, 'Changyuan Xian', 'CYU', '1-17-193-1959', '河南省-新乡市-长垣县', 1);
INSERT INTO `qmgx_region` VALUES (1960, '410781', '卫辉市', 193, 3, 0, 'Weihui Shi', 'WHS', '1-17-193-1960', '河南省-新乡市-卫辉市', 1);
INSERT INTO `qmgx_region` VALUES (1961, '410782', '辉县市', 193, 3, 0, 'Huixian Shi', 'HXS', '1-17-193-1961', '河南省-新乡市-辉县市', 1);
INSERT INTO `qmgx_region` VALUES (1962, '410801', '市辖区', 194, 3, 0, 'Shixiaqu', '2', '1-17-194-1962', '河南省-焦作市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1963, '410802', '解放区', 194, 3, 0, 'Jiefang Qu', 'JFQ', '1-17-194-1963', '河南省-焦作市-解放区', 1);
INSERT INTO `qmgx_region` VALUES (1964, '410803', '中站区', 194, 3, 0, 'Zhongzhan Qu', 'ZZQ', '1-17-194-1964', '河南省-焦作市-中站区', 1);
INSERT INTO `qmgx_region` VALUES (1965, '410804', '马村区', 194, 3, 0, 'Macun Qu', 'MCQ', '1-17-194-1965', '河南省-焦作市-马村区', 1);
INSERT INTO `qmgx_region` VALUES (1966, '410811', '山阳区', 194, 3, 0, 'Shanyang Qu', 'SYC', '1-17-194-1966', '河南省-焦作市-山阳区', 1);
INSERT INTO `qmgx_region` VALUES (1967, '410821', '修武县', 194, 3, 0, 'Xiuwu Xian', 'XUW', '1-17-194-1967', '河南省-焦作市-修武县', 1);
INSERT INTO `qmgx_region` VALUES (1968, '410822', '博爱县', 194, 3, 0, 'Bo,ai Xian', 'BOA', '1-17-194-1968', '河南省-焦作市-博爱县', 1);
INSERT INTO `qmgx_region` VALUES (1969, '410823', '武陟县', 194, 3, 0, 'Wuzhi Xian', 'WZI', '1-17-194-1969', '河南省-焦作市-武陟县', 1);
INSERT INTO `qmgx_region` VALUES (1970, '410825', '温县', 194, 3, 0, 'Wen Xian', 'WEN', '1-17-194-1970', '河南省-焦作市-温县', 1);
INSERT INTO `qmgx_region` VALUES (1971, '419001', '济源市', 194, 3, 0, 'Jiyuan Shi', '2', '1-17-194-1971', '河南省-焦作市-济源市', 1);
INSERT INTO `qmgx_region` VALUES (1972, '410882', '沁阳市', 194, 3, 0, 'Qinyang Shi', 'QYS', '1-17-194-1972', '河南省-焦作市-沁阳市', 1);
INSERT INTO `qmgx_region` VALUES (1973, '410883', '孟州市', 194, 3, 0, 'Mengzhou Shi', 'MZO', '1-17-194-1973', '河南省-焦作市-孟州市', 1);
INSERT INTO `qmgx_region` VALUES (1974, '410901', '市辖区', 195, 3, 0, 'Shixiaqu', '2', '1-17-195-1974', '河南省-濮阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1975, '410902', '华龙区', 195, 3, 0, 'Hualong Qu', '2', '1-17-195-1975', '河南省-濮阳市-华龙区', 1);
INSERT INTO `qmgx_region` VALUES (1976, '410922', '清丰县', 195, 3, 0, 'Qingfeng Xian', 'QFG', '1-17-195-1976', '河南省-濮阳市-清丰县', 1);
INSERT INTO `qmgx_region` VALUES (1977, '410923', '南乐县', 195, 3, 0, 'Nanle Xian', 'NLE', '1-17-195-1977', '河南省-濮阳市-南乐县', 1);
INSERT INTO `qmgx_region` VALUES (1978, '410926', '范县', 195, 3, 0, 'Fan Xian', 'FAX', '1-17-195-1978', '河南省-濮阳市-范县', 1);
INSERT INTO `qmgx_region` VALUES (1979, '410927', '台前县', 195, 3, 0, 'Taiqian Xian', 'TQN', '1-17-195-1979', '河南省-濮阳市-台前县', 1);
INSERT INTO `qmgx_region` VALUES (1980, '410928', '濮阳县', 195, 3, 0, 'Puyang Xian', 'PUY', '1-17-195-1980', '河南省-濮阳市-濮阳县', 1);
INSERT INTO `qmgx_region` VALUES (1981, '411001', '市辖区', 196, 3, 0, 'Shixiaqu', '2', '1-17-196-1981', '河南省-许昌市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1982, '411002', '魏都区', 196, 3, 0, 'Weidu Qu', 'WED', '1-17-196-1982', '河南省-许昌市-魏都区', 1);
INSERT INTO `qmgx_region` VALUES (1983, '411023', '许昌县', 196, 3, 0, 'Xuchang Xian', 'XUC', '1-17-196-1983', '河南省-许昌市-许昌县', 1);
INSERT INTO `qmgx_region` VALUES (1984, '411024', '鄢陵县', 196, 3, 0, 'Yanling Xian', 'YLY', '1-17-196-1984', '河南省-许昌市-鄢陵县', 1);
INSERT INTO `qmgx_region` VALUES (1985, '411025', '襄城县', 196, 3, 0, 'Xiangcheng Xian', 'XAC', '1-17-196-1985', '河南省-许昌市-襄城县', 1);
INSERT INTO `qmgx_region` VALUES (1986, '411081', '禹州市', 196, 3, 0, 'Yuzhou Shi', 'YUZ', '1-17-196-1986', '河南省-许昌市-禹州市', 1);
INSERT INTO `qmgx_region` VALUES (1987, '411082', '长葛市', 196, 3, 0, 'Changge Shi', 'CGE', '1-17-196-1987', '河南省-许昌市-长葛市', 1);
INSERT INTO `qmgx_region` VALUES (1988, '411101', '市辖区', 197, 3, 0, 'Shixiaqu', '2', '1-17-197-1988', '河南省-漯河市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1989, '411102', '源汇区', 197, 3, 0, 'Yuanhui Qu', 'YHI', '1-17-197-1989', '河南省-漯河市-源汇区', 1);
INSERT INTO `qmgx_region` VALUES (1990, '411103', '郾城区', 197, 3, 0, 'Yancheng Qu', '2', '1-17-197-1990', '河南省-漯河市-郾城区', 1);
INSERT INTO `qmgx_region` VALUES (1991, '411104', '召陵区', 197, 3, 0, 'Zhaoling Qu', '2', '1-17-197-1991', '河南省-漯河市-召陵区', 1);
INSERT INTO `qmgx_region` VALUES (1992, '411121', '舞阳县', 197, 3, 0, 'Wuyang Xian', 'WYG', '1-17-197-1992', '河南省-漯河市-舞阳县', 1);
INSERT INTO `qmgx_region` VALUES (1993, '411122', '临颍县', 197, 3, 0, 'Linying Xian', 'LNY', '1-17-197-1993', '河南省-漯河市-临颍县', 1);
INSERT INTO `qmgx_region` VALUES (1994, '411201', '市辖区', 198, 3, 0, 'Shixiaqu', '2', '1-17-198-1994', '河南省-三门峡市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (1995, '411202', '湖滨区', 198, 3, 0, 'Hubin Qu', 'HBI', '1-17-198-1995', '河南省-三门峡市-湖滨区', 1);
INSERT INTO `qmgx_region` VALUES (1996, '411221', '渑池县', 198, 3, 0, 'Mianchi Xian', 'MCI', '1-17-198-1996', '河南省-三门峡市-渑池县', 1);
INSERT INTO `qmgx_region` VALUES (1997, '411222', '陕县', 198, 3, 0, 'Shan Xian', 'SHX', '1-17-198-1997', '河南省-三门峡市-陕县', 1);
INSERT INTO `qmgx_region` VALUES (1998, '411224', '卢氏县', 198, 3, 0, 'Lushi Xian', 'LUU', '1-17-198-1998', '河南省-三门峡市-卢氏县', 1);
INSERT INTO `qmgx_region` VALUES (1999, '411281', '义马市', 198, 3, 0, 'Yima Shi', 'YMA', '1-17-198-1999', '河南省-三门峡市-义马市', 1);
INSERT INTO `qmgx_region` VALUES (2000, '411282', '灵宝市', 198, 3, 0, 'Lingbao Shi', 'LBS', '1-17-198-2000', '河南省-三门峡市-灵宝市', 1);
INSERT INTO `qmgx_region` VALUES (2001, '411301', '市辖区', 199, 3, 0, 'Shixiaqu', '2', '1-17-199-2001', '河南省-南阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2002, '411302', '宛城区', 199, 3, 0, 'Wancheng Qu', 'WCN', '1-17-199-2002', '河南省-南阳市-宛城区', 1);
INSERT INTO `qmgx_region` VALUES (2003, '411303', '卧龙区', 199, 3, 0, 'Wolong Qu', 'WOL', '1-17-199-2003', '河南省-南阳市-卧龙区', 1);
INSERT INTO `qmgx_region` VALUES (2004, '411321', '南召县', 199, 3, 0, 'Nanzhao Xian', 'NZO', '1-17-199-2004', '河南省-南阳市-南召县', 1);
INSERT INTO `qmgx_region` VALUES (2005, '411322', '方城县', 199, 3, 0, 'Fangcheng Xian', 'FCX', '1-17-199-2005', '河南省-南阳市-方城县', 1);
INSERT INTO `qmgx_region` VALUES (2006, '411323', '西峡县', 199, 3, 0, 'Xixia Xian', 'XXY', '1-17-199-2006', '河南省-南阳市-西峡县', 1);
INSERT INTO `qmgx_region` VALUES (2007, '411324', '镇平县', 199, 3, 0, 'Zhenping Xian', 'ZPX', '1-17-199-2007', '河南省-南阳市-镇平县', 1);
INSERT INTO `qmgx_region` VALUES (2008, '411325', '内乡县', 199, 3, 0, 'Neixiang Xian', 'NXG', '1-17-199-2008', '河南省-南阳市-内乡县', 1);
INSERT INTO `qmgx_region` VALUES (2009, '411326', '淅川县', 199, 3, 0, 'Xichuan Xian', 'XCY', '1-17-199-2009', '河南省-南阳市-淅川县', 1);
INSERT INTO `qmgx_region` VALUES (2010, '411327', '社旗县', 199, 3, 0, 'Sheqi Xian', 'SEQ', '1-17-199-2010', '河南省-南阳市-社旗县', 1);
INSERT INTO `qmgx_region` VALUES (2011, '411328', '唐河县', 199, 3, 0, 'Tanghe Xian', 'TGH', '1-17-199-2011', '河南省-南阳市-唐河县', 1);
INSERT INTO `qmgx_region` VALUES (2012, '411329', '新野县', 199, 3, 0, 'Xinye Xian', 'XYE', '1-17-199-2012', '河南省-南阳市-新野县', 1);
INSERT INTO `qmgx_region` VALUES (2013, '411330', '桐柏县', 199, 3, 0, 'Tongbai Xian', 'TBX', '1-17-199-2013', '河南省-南阳市-桐柏县', 1);
INSERT INTO `qmgx_region` VALUES (2014, '411381', '邓州市', 199, 3, 0, 'Dengzhou Shi', 'DGZ', '1-17-199-2014', '河南省-南阳市-邓州市', 1);
INSERT INTO `qmgx_region` VALUES (2015, '411401', '市辖区', 200, 3, 0, 'Shixiaqu', '2', '1-17-200-2015', '河南省-商丘市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2016, '411402', '梁园区', 200, 3, 0, 'Liangyuan Qu', 'LYY', '1-17-200-2016', '河南省-商丘市-梁园区', 1);
INSERT INTO `qmgx_region` VALUES (2017, '411403', '睢阳区', 200, 3, 0, 'Suiyang Qu', 'SYA', '1-17-200-2017', '河南省-商丘市-睢阳区', 1);
INSERT INTO `qmgx_region` VALUES (2018, '411421', '民权县', 200, 3, 0, 'Minquan Xian', 'MQY', '1-17-200-2018', '河南省-商丘市-民权县', 1);
INSERT INTO `qmgx_region` VALUES (2019, '411422', '睢县', 200, 3, 0, 'Sui Xian', 'SUI', '1-17-200-2019', '河南省-商丘市-睢县', 1);
INSERT INTO `qmgx_region` VALUES (2020, '411423', '宁陵县', 200, 3, 0, 'Ningling Xian', 'NGL', '1-17-200-2020', '河南省-商丘市-宁陵县', 1);
INSERT INTO `qmgx_region` VALUES (2021, '411424', '柘城县', 200, 3, 0, 'Zhecheng Xian', 'ZHC', '1-17-200-2021', '河南省-商丘市-柘城县', 1);
INSERT INTO `qmgx_region` VALUES (2022, '411425', '虞城县', 200, 3, 0, 'Yucheng Xian', 'YUC', '1-17-200-2022', '河南省-商丘市-虞城县', 1);
INSERT INTO `qmgx_region` VALUES (2023, '411426', '夏邑县', 200, 3, 0, 'Xiayi Xian', 'XAY', '1-17-200-2023', '河南省-商丘市-夏邑县', 1);
INSERT INTO `qmgx_region` VALUES (2024, '411481', '永城市', 200, 3, 0, 'Yongcheng Shi', 'YOC', '1-17-200-2024', '河南省-商丘市-永城市', 1);
INSERT INTO `qmgx_region` VALUES (2025, '411501', '市辖区', 201, 3, 0, 'Shixiaqu', '2', '1-17-201-2025', '河南省-信阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2026, '411502', '浉河区', 201, 3, 0, 'Shihe Qu', 'SHU', '1-17-201-2026', '河南省-信阳市-浉河区', 1);
INSERT INTO `qmgx_region` VALUES (2027, '411503', '平桥区', 201, 3, 0, 'Pingqiao Qu', 'PQQ', '1-17-201-2027', '河南省-信阳市-平桥区', 1);
INSERT INTO `qmgx_region` VALUES (2028, '411521', '罗山县', 201, 3, 0, 'Luoshan Xian', 'LSE', '1-17-201-2028', '河南省-信阳市-罗山县', 1);
INSERT INTO `qmgx_region` VALUES (2029, '411522', '光山县', 201, 3, 0, 'Guangshan Xian', 'GSX', '1-17-201-2029', '河南省-信阳市-光山县', 1);
INSERT INTO `qmgx_region` VALUES (2030, '411523', '新县', 201, 3, 0, 'Xin Xian', 'XXI', '1-17-201-2030', '河南省-信阳市-新县', 1);
INSERT INTO `qmgx_region` VALUES (2031, '411524', '商城县', 201, 3, 0, 'Shangcheng Xian', 'SCX', '1-17-201-2031', '河南省-信阳市-商城县', 1);
INSERT INTO `qmgx_region` VALUES (2032, '411525', '固始县', 201, 3, 0, 'Gushi Xian', 'GSI', '1-17-201-2032', '河南省-信阳市-固始县', 1);
INSERT INTO `qmgx_region` VALUES (2033, '411526', '潢川县', 201, 3, 0, 'Huangchuan Xian', 'HCU', '1-17-201-2033', '河南省-信阳市-潢川县', 1);
INSERT INTO `qmgx_region` VALUES (2034, '411527', '淮滨县', 201, 3, 0, 'Huaibin Xian', 'HBN', '1-17-201-2034', '河南省-信阳市-淮滨县', 1);
INSERT INTO `qmgx_region` VALUES (2035, '411528', '息县', 201, 3, 0, 'Xi Xian', 'XIX', '1-17-201-2035', '河南省-信阳市-息县', 1);
INSERT INTO `qmgx_region` VALUES (2036, '411601', '市辖区', 202, 3, 0, '1', '2', '1-17-202-2036', '河南省-周口市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2037, '411602', '川汇区', 202, 3, 0, 'Chuanhui Qu', '2', '1-17-202-2037', '河南省-周口市-川汇区', 1);
INSERT INTO `qmgx_region` VALUES (2038, '411621', '扶沟县', 202, 3, 0, 'Fugou Xian', '2', '1-17-202-2038', '河南省-周口市-扶沟县', 1);
INSERT INTO `qmgx_region` VALUES (2039, '411622', '西华县', 202, 3, 0, 'Xihua Xian', '2', '1-17-202-2039', '河南省-周口市-西华县', 1);
INSERT INTO `qmgx_region` VALUES (2040, '411623', '商水县', 202, 3, 0, 'Shangshui Xian', '2', '1-17-202-2040', '河南省-周口市-商水县', 1);
INSERT INTO `qmgx_region` VALUES (2041, '411624', '沈丘县', 202, 3, 0, 'Shenqiu Xian', '2', '1-17-202-2041', '河南省-周口市-沈丘县', 1);
INSERT INTO `qmgx_region` VALUES (2042, '411625', '郸城县', 202, 3, 0, 'Dancheng Xian', '2', '1-17-202-2042', '河南省-周口市-郸城县', 1);
INSERT INTO `qmgx_region` VALUES (2043, '411626', '淮阳县', 202, 3, 0, 'Huaiyang Xian', '2', '1-17-202-2043', '河南省-周口市-淮阳县', 1);
INSERT INTO `qmgx_region` VALUES (2044, '411627', '太康县', 202, 3, 0, 'Taikang Xian', '2', '1-17-202-2044', '河南省-周口市-太康县', 1);
INSERT INTO `qmgx_region` VALUES (2045, '411628', '鹿邑县', 202, 3, 0, 'Luyi Xian', '2', '1-17-202-2045', '河南省-周口市-鹿邑县', 1);
INSERT INTO `qmgx_region` VALUES (2046, '411681', '项城市', 202, 3, 0, 'Xiangcheng Shi', '2', '1-17-202-2046', '河南省-周口市-项城市', 1);
INSERT INTO `qmgx_region` VALUES (2047, '411701', '市辖区', 203, 3, 0, '1', '2', '1-17-203-2047', '河南省-驻马店市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2048, '411702', '驿城区', 203, 3, 0, 'Yicheng Qu', '2', '1-17-203-2048', '河南省-驻马店市-驿城区', 1);
INSERT INTO `qmgx_region` VALUES (2049, '411721', '西平县', 203, 3, 0, 'Xiping Xian', '2', '1-17-203-2049', '河南省-驻马店市-西平县', 1);
INSERT INTO `qmgx_region` VALUES (2050, '411722', '上蔡县', 203, 3, 0, 'Shangcai Xian', '2', '1-17-203-2050', '河南省-驻马店市-上蔡县', 1);
INSERT INTO `qmgx_region` VALUES (2051, '411723', '平舆县', 203, 3, 0, 'Pingyu Xian', '2', '1-17-203-2051', '河南省-驻马店市-平舆县', 1);
INSERT INTO `qmgx_region` VALUES (2052, '411724', '正阳县', 203, 3, 0, 'Zhengyang Xian', '2', '1-17-203-2052', '河南省-驻马店市-正阳县', 1);
INSERT INTO `qmgx_region` VALUES (2053, '411725', '确山县', 203, 3, 0, 'Queshan Xian', '2', '1-17-203-2053', '河南省-驻马店市-确山县', 1);
INSERT INTO `qmgx_region` VALUES (2054, '411726', '泌阳县', 203, 3, 0, 'Biyang Xian', '2', '1-17-203-2054', '河南省-驻马店市-泌阳县', 1);
INSERT INTO `qmgx_region` VALUES (2055, '411727', '汝南县', 203, 3, 0, 'Runan Xian', '2', '1-17-203-2055', '河南省-驻马店市-汝南县', 1);
INSERT INTO `qmgx_region` VALUES (2056, '411728', '遂平县', 203, 3, 0, 'Suiping Xian', '2', '1-17-203-2056', '河南省-驻马店市-遂平县', 1);
INSERT INTO `qmgx_region` VALUES (2057, '411729', '新蔡县', 203, 3, 0, 'Xincai Xian', '2', '1-17-203-2057', '河南省-驻马店市-新蔡县', 1);
INSERT INTO `qmgx_region` VALUES (2058, '420101', '市辖区', 204, 3, 0, 'Shixiaqu', '2', '1-18-204-2058', '湖北省-武汉市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2059, '420102', '江岸区', 204, 3, 0, 'Jiang,an Qu', 'JAA', '1-18-204-2059', '湖北省-武汉市-江岸区', 1);
INSERT INTO `qmgx_region` VALUES (2060, '420103', '江汉区', 204, 3, 0, 'Jianghan Qu', 'JHN', '1-18-204-2060', '湖北省-武汉市-江汉区', 1);
INSERT INTO `qmgx_region` VALUES (2061, '420104', '硚口区', 204, 3, 0, 'Qiaokou Qu', 'QKQ', '1-18-204-2061', '湖北省-武汉市-硚口区', 1);
INSERT INTO `qmgx_region` VALUES (2062, '420105', '汉阳区', 204, 3, 0, 'Hanyang Qu', 'HYA', '1-18-204-2062', '湖北省-武汉市-汉阳区', 1);
INSERT INTO `qmgx_region` VALUES (2063, '420106', '武昌区', 204, 3, 0, 'Wuchang Qu', 'WCQ', '1-18-204-2063', '湖北省-武汉市-武昌区', 1);
INSERT INTO `qmgx_region` VALUES (2064, '420107', '青山区', 204, 3, 0, 'Qingshan Qu', 'QSN', '1-18-204-2064', '湖北省-武汉市-青山区', 1);
INSERT INTO `qmgx_region` VALUES (2065, '420111', '洪山区', 204, 3, 0, 'Hongshan Qu', 'HSQ', '1-18-204-2065', '湖北省-武汉市-洪山区', 1);
INSERT INTO `qmgx_region` VALUES (2066, '420112', '东西湖区', 204, 3, 0, 'Dongxihu Qu', 'DXH', '1-18-204-2066', '湖北省-武汉市-东西湖区', 1);
INSERT INTO `qmgx_region` VALUES (2067, '420113', '汉南区', 204, 3, 0, 'Hannan Qu', 'HNQ', '1-18-204-2067', '湖北省-武汉市-汉南区', 1);
INSERT INTO `qmgx_region` VALUES (2068, '420114', '蔡甸区', 204, 3, 0, 'Caidian Qu', 'CDN', '1-18-204-2068', '湖北省-武汉市-蔡甸区', 1);
INSERT INTO `qmgx_region` VALUES (2069, '420115', '江夏区', 204, 3, 0, 'Jiangxia Qu', 'JXQ', '1-18-204-2069', '湖北省-武汉市-江夏区', 1);
INSERT INTO `qmgx_region` VALUES (2070, '420116', '黄陂区', 204, 3, 0, 'Huangpi Qu', 'HPI', '1-18-204-2070', '湖北省-武汉市-黄陂区', 1);
INSERT INTO `qmgx_region` VALUES (2071, '420117', '新洲区', 204, 3, 0, 'Xinzhou Qu', 'XNZ', '1-18-204-2071', '湖北省-武汉市-新洲区', 1);
INSERT INTO `qmgx_region` VALUES (2072, '420201', '市辖区', 205, 3, 0, 'Shixiaqu', '2', '1-18-205-2072', '湖北省-黄石市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2073, '420202', '黄石港区', 205, 3, 0, 'Huangshigang Qu', 'HSG', '1-18-205-2073', '湖北省-黄石市-黄石港区', 1);
INSERT INTO `qmgx_region` VALUES (2074, '420203', '西塞山区', 205, 3, 0, 'Xisaishan Qu', '2', '1-18-205-2074', '湖北省-黄石市-西塞山区', 1);
INSERT INTO `qmgx_region` VALUES (2075, '420204', '下陆区', 205, 3, 0, 'Xialu Qu', 'XAL', '1-18-205-2075', '湖北省-黄石市-下陆区', 1);
INSERT INTO `qmgx_region` VALUES (2076, '420205', '铁山区', 205, 3, 0, 'Tieshan Qu', 'TSH', '1-18-205-2076', '湖北省-黄石市-铁山区', 1);
INSERT INTO `qmgx_region` VALUES (2077, '420222', '阳新县', 205, 3, 0, 'Yangxin Xian', 'YXE', '1-18-205-2077', '湖北省-黄石市-阳新县', 1);
INSERT INTO `qmgx_region` VALUES (2078, '420281', '大冶市', 205, 3, 0, 'Daye Shi', 'DYE', '1-18-205-2078', '湖北省-黄石市-大冶市', 1);
INSERT INTO `qmgx_region` VALUES (2079, '420301', '市辖区', 206, 3, 0, 'Shixiaqu', '2', '1-18-206-2079', '湖北省-十堰市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2080, '420302', '茅箭区', 206, 3, 0, 'Maojian Qu', 'MJN', '1-18-206-2080', '湖北省-十堰市-茅箭区', 1);
INSERT INTO `qmgx_region` VALUES (2081, '420303', '张湾区', 206, 3, 0, 'Zhangwan Qu', 'ZWQ', '1-18-206-2081', '湖北省-十堰市-张湾区', 1);
INSERT INTO `qmgx_region` VALUES (2082, '420321', '郧县', 206, 3, 0, 'Yun Xian', 'YUN', '1-18-206-2082', '湖北省-十堰市-郧县', 1);
INSERT INTO `qmgx_region` VALUES (2083, '420322', '郧西县', 206, 3, 0, 'Yunxi Xian', 'YNX', '1-18-206-2083', '湖北省-十堰市-郧西县', 1);
INSERT INTO `qmgx_region` VALUES (2084, '420323', '竹山县', 206, 3, 0, 'Zhushan Xian', 'ZHS', '1-18-206-2084', '湖北省-十堰市-竹山县', 1);
INSERT INTO `qmgx_region` VALUES (2085, '420324', '竹溪县', 206, 3, 0, 'Zhuxi Xian', 'ZXX', '1-18-206-2085', '湖北省-十堰市-竹溪县', 1);
INSERT INTO `qmgx_region` VALUES (2086, '420325', '房县', 206, 3, 0, 'Fang Xian', 'FAG', '1-18-206-2086', '湖北省-十堰市-房县', 1);
INSERT INTO `qmgx_region` VALUES (2087, '420381', '丹江口市', 206, 3, 0, 'Danjiangkou Shi', 'DJK', '1-18-206-2087', '湖北省-十堰市-丹江口市', 1);
INSERT INTO `qmgx_region` VALUES (2088, '420501', '市辖区', 207, 3, 0, 'Shixiaqu', '2', '1-18-207-2088', '湖北省-宜昌市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2089, '420502', '西陵区', 207, 3, 0, 'Xiling Qu', 'XLQ', '1-18-207-2089', '湖北省-宜昌市-西陵区', 1);
INSERT INTO `qmgx_region` VALUES (2090, '420503', '伍家岗区', 207, 3, 0, 'Wujiagang Qu', 'WJG', '1-18-207-2090', '湖北省-宜昌市-伍家岗区', 1);
INSERT INTO `qmgx_region` VALUES (2091, '420504', '点军区', 207, 3, 0, 'Dianjun Qu', 'DJN', '1-18-207-2091', '湖北省-宜昌市-点军区', 1);
INSERT INTO `qmgx_region` VALUES (2092, '420505', '猇亭区', 207, 3, 0, 'Xiaoting Qu', 'XTQ', '1-18-207-2092', '湖北省-宜昌市-猇亭区', 1);
INSERT INTO `qmgx_region` VALUES (2093, '420506', '夷陵区', 207, 3, 0, 'Yiling Qu', '2', '1-18-207-2093', '湖北省-宜昌市-夷陵区', 1);
INSERT INTO `qmgx_region` VALUES (2094, '420525', '远安县', 207, 3, 0, 'Yuan,an Xian', 'YAX', '1-18-207-2094', '湖北省-宜昌市-远安县', 1);
INSERT INTO `qmgx_region` VALUES (2095, '420526', '兴山县', 207, 3, 0, 'Xingshan Xian', 'XSX', '1-18-207-2095', '湖北省-宜昌市-兴山县', 1);
INSERT INTO `qmgx_region` VALUES (2096, '420527', '秭归县', 207, 3, 0, 'Zigui Xian', 'ZGI', '1-18-207-2096', '湖北省-宜昌市-秭归县', 1);
INSERT INTO `qmgx_region` VALUES (2097, '420528', '长阳土家族自治县', 207, 3, 0, 'Changyang Tujiazu Zizhixian', 'CYA', '1-18-207-2097', '湖北省-宜昌市-长阳土家族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2098, '420529', '五峰土家族自治县', 207, 3, 0, 'Wufeng Tujiazu Zizhixian', 'WFG', '1-18-207-2098', '湖北省-宜昌市-五峰土家族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2099, '420581', '宜都市', 207, 3, 0, 'Yidu Shi', 'YID', '1-18-207-2099', '湖北省-宜昌市-宜都市', 1);
INSERT INTO `qmgx_region` VALUES (2100, '420582', '当阳市', 207, 3, 0, 'Dangyang Shi', 'DYS', '1-18-207-2100', '湖北省-宜昌市-当阳市', 1);
INSERT INTO `qmgx_region` VALUES (2101, '420583', '枝江市', 207, 3, 0, 'Zhijiang Shi', 'ZIJ', '1-18-207-2101', '湖北省-宜昌市-枝江市', 1);
INSERT INTO `qmgx_region` VALUES (2102, '420601', '市辖区', 208, 3, 0, 'Shixiaqu', '2', '1-18-208-2102', '湖北省-襄樊市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2103, '420602', '襄城区', 208, 3, 0, 'Xiangcheng Qu', 'XXF', '1-18-208-2103', '湖北省-襄樊市-襄城区', 1);
INSERT INTO `qmgx_region` VALUES (2104, '420606', '樊城区', 208, 3, 0, 'Fancheng Qu', 'FNC', '1-18-208-2104', '湖北省-襄樊市-樊城区', 1);
INSERT INTO `qmgx_region` VALUES (2105, '420607', '襄阳区', 208, 3, 0, 'Xiangyang Qu', '2', '1-18-208-2105', '湖北省-襄樊市-襄阳区', 1);
INSERT INTO `qmgx_region` VALUES (2106, '420624', '南漳县', 208, 3, 0, 'Nanzhang Xian', 'NZH', '1-18-208-2106', '湖北省-襄樊市-南漳县', 1);
INSERT INTO `qmgx_region` VALUES (2107, '420625', '谷城县', 208, 3, 0, 'Gucheng Xian', 'GUC', '1-18-208-2107', '湖北省-襄樊市-谷城县', 1);
INSERT INTO `qmgx_region` VALUES (2108, '420626', '保康县', 208, 3, 0, 'Baokang Xian', 'BKG', '1-18-208-2108', '湖北省-襄樊市-保康县', 1);
INSERT INTO `qmgx_region` VALUES (2109, '420682', '老河口市', 208, 3, 0, 'Laohekou Shi', 'LHK', '1-18-208-2109', '湖北省-襄樊市-老河口市', 1);
INSERT INTO `qmgx_region` VALUES (2110, '420683', '枣阳市', 208, 3, 0, 'Zaoyang Shi', 'ZOY', '1-18-208-2110', '湖北省-襄樊市-枣阳市', 1);
INSERT INTO `qmgx_region` VALUES (2111, '420684', '宜城市', 208, 3, 0, 'Yicheng Shi', 'YCW', '1-18-208-2111', '湖北省-襄樊市-宜城市', 1);
INSERT INTO `qmgx_region` VALUES (2112, '420701', '市辖区', 209, 3, 0, 'Shixiaqu', '2', '1-18-209-2112', '湖北省-鄂州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2113, '420702', '梁子湖区', 209, 3, 0, 'Liangzihu Qu', 'LZI', '1-18-209-2113', '湖北省-鄂州市-梁子湖区', 1);
INSERT INTO `qmgx_region` VALUES (2114, '420703', '华容区', 209, 3, 0, 'Huarong Qu', 'HRQ', '1-18-209-2114', '湖北省-鄂州市-华容区', 1);
INSERT INTO `qmgx_region` VALUES (2115, '420704', '鄂城区', 209, 3, 0, 'Echeng Qu', 'ECQ', '1-18-209-2115', '湖北省-鄂州市-鄂城区', 1);
INSERT INTO `qmgx_region` VALUES (2116, '420801', '市辖区', 210, 3, 0, 'Shixiaqu', '2', '1-18-210-2116', '湖北省-荆门市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2117, '420802', '东宝区', 210, 3, 0, 'Dongbao Qu', 'DBQ', '1-18-210-2117', '湖北省-荆门市-东宝区', 1);
INSERT INTO `qmgx_region` VALUES (2118, '420804', '掇刀区', 210, 3, 0, 'Duodao Qu', '2', '1-18-210-2118', '湖北省-荆门市-掇刀区', 1);
INSERT INTO `qmgx_region` VALUES (2119, '420821', '京山县', 210, 3, 0, 'Jingshan Xian', 'JSA', '1-18-210-2119', '湖北省-荆门市-京山县', 1);
INSERT INTO `qmgx_region` VALUES (2120, '420822', '沙洋县', 210, 3, 0, 'Shayang Xian', 'SYF', '1-18-210-2120', '湖北省-荆门市-沙洋县', 1);
INSERT INTO `qmgx_region` VALUES (2121, '420881', '钟祥市', 210, 3, 0, 'Zhongxiang Shi', '2', '1-18-210-2121', '湖北省-荆门市-钟祥市', 1);
INSERT INTO `qmgx_region` VALUES (2122, '420901', '市辖区', 211, 3, 0, 'Shixiaqu', '2', '1-18-211-2122', '湖北省-孝感市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2123, '420902', '孝南区', 211, 3, 0, 'Xiaonan Qu', 'XNA', '1-18-211-2123', '湖北省-孝感市-孝南区', 1);
INSERT INTO `qmgx_region` VALUES (2124, '420921', '孝昌县', 211, 3, 0, 'Xiaochang Xian', 'XOC', '1-18-211-2124', '湖北省-孝感市-孝昌县', 1);
INSERT INTO `qmgx_region` VALUES (2125, '420922', '大悟县', 211, 3, 0, 'Dawu Xian', 'DWU', '1-18-211-2125', '湖北省-孝感市-大悟县', 1);
INSERT INTO `qmgx_region` VALUES (2126, '420923', '云梦县', 211, 3, 0, 'Yunmeng Xian', 'YMX', '1-18-211-2126', '湖北省-孝感市-云梦县', 1);
INSERT INTO `qmgx_region` VALUES (2127, '420981', '应城市', 211, 3, 0, 'Yingcheng Shi', 'YCG', '1-18-211-2127', '湖北省-孝感市-应城市', 1);
INSERT INTO `qmgx_region` VALUES (2128, '420982', '安陆市', 211, 3, 0, 'Anlu Shi', 'ALU', '1-18-211-2128', '湖北省-孝感市-安陆市', 1);
INSERT INTO `qmgx_region` VALUES (2129, '420984', '汉川市', 211, 3, 0, 'Hanchuan Shi', 'HCH', '1-18-211-2129', '湖北省-孝感市-汉川市', 1);
INSERT INTO `qmgx_region` VALUES (2130, '421001', '市辖区', 212, 3, 0, 'Shixiaqu', '2', '1-18-212-2130', '湖北省-荆州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2131, '421002', '沙市区', 212, 3, 0, 'Shashi Qu', 'SSJ', '1-18-212-2131', '湖北省-荆州市-沙市区', 1);
INSERT INTO `qmgx_region` VALUES (2132, '421003', '荆州区', 212, 3, 0, 'Jingzhou Qu', 'JZQ', '1-18-212-2132', '湖北省-荆州市-荆州区', 1);
INSERT INTO `qmgx_region` VALUES (2133, '421022', '公安县', 212, 3, 0, 'Gong,an Xian', 'GGA', '1-18-212-2133', '湖北省-荆州市-公安县', 1);
INSERT INTO `qmgx_region` VALUES (2134, '421023', '监利县', 212, 3, 0, 'Jianli Xian', 'JLI', '1-18-212-2134', '湖北省-荆州市-监利县', 1);
INSERT INTO `qmgx_region` VALUES (2135, '421024', '江陵县', 212, 3, 0, 'Jiangling Xian', 'JLX', '1-18-212-2135', '湖北省-荆州市-江陵县', 1);
INSERT INTO `qmgx_region` VALUES (2136, '421081', '石首市', 212, 3, 0, 'Shishou Shi', 'SSO', '1-18-212-2136', '湖北省-荆州市-石首市', 1);
INSERT INTO `qmgx_region` VALUES (2137, '421083', '洪湖市', 212, 3, 0, 'Honghu Shi', 'HHU', '1-18-212-2137', '湖北省-荆州市-洪湖市', 1);
INSERT INTO `qmgx_region` VALUES (2138, '421087', '松滋市', 212, 3, 0, 'Songzi Shi', 'SZF', '1-18-212-2138', '湖北省-荆州市-松滋市', 1);
INSERT INTO `qmgx_region` VALUES (2139, '421101', '市辖区', 213, 3, 0, 'Shixiaqu', '2', '1-18-213-2139', '湖北省-黄冈市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2140, '421102', '黄州区', 213, 3, 0, 'Huangzhou Qu', 'HZC', '1-18-213-2140', '湖北省-黄冈市-黄州区', 1);
INSERT INTO `qmgx_region` VALUES (2141, '421121', '团风县', 213, 3, 0, 'Tuanfeng Xian', 'TFG', '1-18-213-2141', '湖北省-黄冈市-团风县', 1);
INSERT INTO `qmgx_region` VALUES (2142, '421122', '红安县', 213, 3, 0, 'Hong,an Xian', 'HGA', '1-18-213-2142', '湖北省-黄冈市-红安县', 1);
INSERT INTO `qmgx_region` VALUES (2143, '421123', '罗田县', 213, 3, 0, 'Luotian Xian', 'LTE', '1-18-213-2143', '湖北省-黄冈市-罗田县', 1);
INSERT INTO `qmgx_region` VALUES (2144, '421124', '英山县', 213, 3, 0, 'Yingshan Xian', 'YSE', '1-18-213-2144', '湖北省-黄冈市-英山县', 1);
INSERT INTO `qmgx_region` VALUES (2145, '421125', '浠水县', 213, 3, 0, 'Xishui Xian', 'XSE', '1-18-213-2145', '湖北省-黄冈市-浠水县', 1);
INSERT INTO `qmgx_region` VALUES (2146, '421126', '蕲春县', 213, 3, 0, 'Qichun Xian', 'QCN', '1-18-213-2146', '湖北省-黄冈市-蕲春县', 1);
INSERT INTO `qmgx_region` VALUES (2147, '421127', '黄梅县', 213, 3, 0, 'Huangmei Xian', 'HGM', '1-18-213-2147', '湖北省-黄冈市-黄梅县', 1);
INSERT INTO `qmgx_region` VALUES (2148, '421181', '麻城市', 213, 3, 0, 'Macheng Shi', 'MCS', '1-18-213-2148', '湖北省-黄冈市-麻城市', 1);
INSERT INTO `qmgx_region` VALUES (2149, '421182', '武穴市', 213, 3, 0, 'Wuxue Shi', 'WXE', '1-18-213-2149', '湖北省-黄冈市-武穴市', 1);
INSERT INTO `qmgx_region` VALUES (2150, '421201', '市辖区', 214, 3, 0, 'Shixiaqu', '2', '1-18-214-2150', '湖北省-咸宁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2151, '421202', '咸安区', 214, 3, 0, 'Xian,an Qu', 'XAN', '1-18-214-2151', '湖北省-咸宁市-咸安区', 1);
INSERT INTO `qmgx_region` VALUES (2152, '421221', '嘉鱼县', 214, 3, 0, 'Jiayu Xian', 'JYX', '1-18-214-2152', '湖北省-咸宁市-嘉鱼县', 1);
INSERT INTO `qmgx_region` VALUES (2153, '421222', '通城县', 214, 3, 0, 'Tongcheng Xian', 'TCX', '1-18-214-2153', '湖北省-咸宁市-通城县', 1);
INSERT INTO `qmgx_region` VALUES (2154, '421223', '崇阳县', 214, 3, 0, 'Chongyang Xian', 'CGY', '1-18-214-2154', '湖北省-咸宁市-崇阳县', 1);
INSERT INTO `qmgx_region` VALUES (2155, '421224', '通山县', 214, 3, 0, 'Tongshan Xian', 'TSA', '1-18-214-2155', '湖北省-咸宁市-通山县', 1);
INSERT INTO `qmgx_region` VALUES (2156, '421281', '赤壁市', 214, 3, 0, 'Chibi Shi', 'CBI', '1-18-214-2156', '湖北省-咸宁市-赤壁市', 1);
INSERT INTO `qmgx_region` VALUES (2157, '421301', '市辖区', 215, 3, 0, '1', '2', '1-18-215-2157', '湖北省-随州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2158, '421303', '曾都区', 215, 3, 0, 'Zengdu Qu', '2', '1-18-215-2158', '湖北省-随州市-曾都区', 1);
INSERT INTO `qmgx_region` VALUES (2159, '421381', '广水市', 215, 3, 0, 'Guangshui Shi', '2', '1-18-215-2159', '湖北省-随州市-广水市', 1);
INSERT INTO `qmgx_region` VALUES (2160, '422801', '恩施市', 216, 3, 0, 'Enshi Shi', 'ESS', '1-18-216-2160', '湖北省-恩施土家族苗族自治州-恩施市', 1);
INSERT INTO `qmgx_region` VALUES (2161, '422802', '利川市', 216, 3, 0, 'Lichuan Shi', 'LCE', '1-18-216-2161', '湖北省-恩施土家族苗族自治州-利川市', 1);
INSERT INTO `qmgx_region` VALUES (2162, '422822', '建始县', 216, 3, 0, 'Jianshi Xian', 'JSE', '1-18-216-2162', '湖北省-恩施土家族苗族自治州-建始县', 1);
INSERT INTO `qmgx_region` VALUES (2163, '422823', '巴东县', 216, 3, 0, 'Badong Xian', 'BDG', '1-18-216-2163', '湖北省-恩施土家族苗族自治州-巴东县', 1);
INSERT INTO `qmgx_region` VALUES (2164, '422825', '宣恩县', 216, 3, 0, 'Xuan,en Xian', 'XEN', '1-18-216-2164', '湖北省-恩施土家族苗族自治州-宣恩县', 1);
INSERT INTO `qmgx_region` VALUES (2165, '422826', '咸丰县', 216, 3, 0, 'Xianfeng Xian', 'XFG', '1-18-216-2165', '湖北省-恩施土家族苗族自治州-咸丰县', 1);
INSERT INTO `qmgx_region` VALUES (2166, '422827', '来凤县', 216, 3, 0, 'Laifeng Xian', 'LFG', '1-18-216-2166', '湖北省-恩施土家族苗族自治州-来凤县', 1);
INSERT INTO `qmgx_region` VALUES (2167, '422828', '鹤峰县', 216, 3, 0, 'Hefeng Xian', 'HEF', '1-18-216-2167', '湖北省-恩施土家族苗族自治州-鹤峰县', 1);
INSERT INTO `qmgx_region` VALUES (2168, '429004', '仙桃市', 217, 3, 0, 'Xiantao Shi', 'XNT', '1-18-217-2168', '湖北省-省直辖县级行政区划-仙桃市', 1);
INSERT INTO `qmgx_region` VALUES (2169, '429005', '潜江市', 217, 3, 0, 'Qianjiang Shi', 'QNJ', '1-18-217-2169', '湖北省-省直辖县级行政区划-潜江市', 1);
INSERT INTO `qmgx_region` VALUES (2170, '429006', '天门市', 217, 3, 0, 'Tianmen Shi', 'TMS', '1-18-217-2170', '湖北省-省直辖县级行政区划-天门市', 1);
INSERT INTO `qmgx_region` VALUES (2171, '429021', '神农架林区', 217, 3, 0, 'Shennongjia Linqu', 'SNJ', '1-18-217-2171', '湖北省-省直辖县级行政区划-神农架林区', 1);
INSERT INTO `qmgx_region` VALUES (2172, '430101', '市辖区', 218, 3, 0, 'Shixiaqu', '2', '1-19-218-2172', '湖南省-长沙市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2173, '430102', '芙蓉区', 218, 3, 0, 'Furong Qu', 'FRQ', '1-19-218-2173', '湖南省-长沙市-芙蓉区', 1);
INSERT INTO `qmgx_region` VALUES (2174, '430103', '天心区', 218, 3, 0, 'Tianxin Qu', 'TXQ', '1-19-218-2174', '湖南省-长沙市-天心区', 1);
INSERT INTO `qmgx_region` VALUES (2175, '430104', '岳麓区', 218, 3, 0, 'Yuelu Qu', 'YLU', '1-19-218-2175', '湖南省-长沙市-岳麓区', 1);
INSERT INTO `qmgx_region` VALUES (2176, '430105', '开福区', 218, 3, 0, 'Kaifu Qu', 'KFQ', '1-19-218-2176', '湖南省-长沙市-开福区', 1);
INSERT INTO `qmgx_region` VALUES (2177, '430111', '雨花区', 218, 3, 0, 'Yuhua Qu', 'YHA', '1-19-218-2177', '湖南省-长沙市-雨花区', 1);
INSERT INTO `qmgx_region` VALUES (2178, '430121', '长沙县', 218, 3, 0, 'Changsha Xian', 'CSA', '1-19-218-2178', '湖南省-长沙市-长沙县', 1);
INSERT INTO `qmgx_region` VALUES (2179, '430122', '望城县', 218, 3, 0, 'Wangcheng Xian', 'WCH', '1-19-218-2179', '湖南省-长沙市-望城县', 1);
INSERT INTO `qmgx_region` VALUES (2180, '430124', '宁乡县', 218, 3, 0, 'Ningxiang Xian', 'NXX', '1-19-218-2180', '湖南省-长沙市-宁乡县', 1);
INSERT INTO `qmgx_region` VALUES (2181, '430181', '浏阳市', 218, 3, 0, 'Liuyang Shi', 'LYS', '1-19-218-2181', '湖南省-长沙市-浏阳市', 1);
INSERT INTO `qmgx_region` VALUES (2182, '430201', '市辖区', 219, 3, 0, 'Shixiaqu', '2', '1-19-219-2182', '湖南省-株洲市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2183, '430202', '荷塘区', 219, 3, 0, 'Hetang Qu', 'HTZ', '1-19-219-2183', '湖南省-株洲市-荷塘区', 1);
INSERT INTO `qmgx_region` VALUES (2184, '430203', '芦淞区', 219, 3, 0, 'Lusong Qu', 'LZZ', '1-19-219-2184', '湖南省-株洲市-芦淞区', 1);
INSERT INTO `qmgx_region` VALUES (2185, '430204', '石峰区', 219, 3, 0, 'Shifeng Qu', 'SFG', '1-19-219-2185', '湖南省-株洲市-石峰区', 1);
INSERT INTO `qmgx_region` VALUES (2186, '430211', '天元区', 219, 3, 0, 'Tianyuan Qu', 'TYQ', '1-19-219-2186', '湖南省-株洲市-天元区', 1);
INSERT INTO `qmgx_region` VALUES (2187, '430221', '株洲县', 219, 3, 0, 'Zhuzhou Xian', 'ZZX', '1-19-219-2187', '湖南省-株洲市-株洲县', 1);
INSERT INTO `qmgx_region` VALUES (2188, '430223', '攸县', 219, 3, 0, 'You Xian', 'YOU', '1-19-219-2188', '湖南省-株洲市-攸县', 1);
INSERT INTO `qmgx_region` VALUES (2189, '430224', '茶陵县', 219, 3, 0, 'Chaling Xian', 'CAL', '1-19-219-2189', '湖南省-株洲市-茶陵县', 1);
INSERT INTO `qmgx_region` VALUES (2190, '430225', '炎陵县', 219, 3, 0, 'Yanling Xian', 'YLX', '1-19-219-2190', '湖南省-株洲市-炎陵县', 1);
INSERT INTO `qmgx_region` VALUES (2191, '430281', '醴陵市', 219, 3, 0, 'Liling Shi', 'LIL', '1-19-219-2191', '湖南省-株洲市-醴陵市', 1);
INSERT INTO `qmgx_region` VALUES (2192, '430301', '市辖区', 220, 3, 0, 'Shixiaqu', '2', '1-19-220-2192', '湖南省-湘潭市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2193, '430302', '雨湖区', 220, 3, 0, 'Yuhu Qu', 'YHU', '1-19-220-2193', '湖南省-湘潭市-雨湖区', 1);
INSERT INTO `qmgx_region` VALUES (2194, '430304', '岳塘区', 220, 3, 0, 'Yuetang Qu', 'YTG', '1-19-220-2194', '湖南省-湘潭市-岳塘区', 1);
INSERT INTO `qmgx_region` VALUES (2195, '430321', '湘潭县', 220, 3, 0, 'Xiangtan Qu', 'XTX', '1-19-220-2195', '湖南省-湘潭市-湘潭县', 1);
INSERT INTO `qmgx_region` VALUES (2196, '430381', '湘乡市', 220, 3, 0, 'Xiangxiang Shi', 'XXG', '1-19-220-2196', '湖南省-湘潭市-湘乡市', 1);
INSERT INTO `qmgx_region` VALUES (2197, '430382', '韶山市', 220, 3, 0, 'Shaoshan Shi', 'SSN', '1-19-220-2197', '湖南省-湘潭市-韶山市', 1);
INSERT INTO `qmgx_region` VALUES (2198, '430401', '市辖区', 221, 3, 0, 'Shixiaqu', '2', '1-19-221-2198', '湖南省-衡阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2199, '430405', '珠晖区', 221, 3, 0, 'Zhuhui Qu', '2', '1-19-221-2199', '湖南省-衡阳市-珠晖区', 1);
INSERT INTO `qmgx_region` VALUES (2200, '430406', '雁峰区', 221, 3, 0, 'Yanfeng Qu', '2', '1-19-221-2200', '湖南省-衡阳市-雁峰区', 1);
INSERT INTO `qmgx_region` VALUES (2201, '430407', '石鼓区', 221, 3, 0, 'Shigu Qu', '2', '1-19-221-2201', '湖南省-衡阳市-石鼓区', 1);
INSERT INTO `qmgx_region` VALUES (2202, '430408', '蒸湘区', 221, 3, 0, 'Zhengxiang Qu', '2', '1-19-221-2202', '湖南省-衡阳市-蒸湘区', 1);
INSERT INTO `qmgx_region` VALUES (2203, '430412', '南岳区', 221, 3, 0, 'Nanyue Qu', 'NYQ', '1-19-221-2203', '湖南省-衡阳市-南岳区', 1);
INSERT INTO `qmgx_region` VALUES (2204, '430421', '衡阳县', 221, 3, 0, 'Hengyang Xian', 'HYO', '1-19-221-2204', '湖南省-衡阳市-衡阳县', 1);
INSERT INTO `qmgx_region` VALUES (2205, '430422', '衡南县', 221, 3, 0, 'Hengnan Xian', 'HNX', '1-19-221-2205', '湖南省-衡阳市-衡南县', 1);
INSERT INTO `qmgx_region` VALUES (2206, '430423', '衡山县', 221, 3, 0, 'Hengshan Xian', 'HSH', '1-19-221-2206', '湖南省-衡阳市-衡山县', 1);
INSERT INTO `qmgx_region` VALUES (2207, '430424', '衡东县', 221, 3, 0, 'Hengdong Xian', 'HED', '1-19-221-2207', '湖南省-衡阳市-衡东县', 1);
INSERT INTO `qmgx_region` VALUES (2208, '430426', '祁东县', 221, 3, 0, 'Qidong Xian', 'QDX', '1-19-221-2208', '湖南省-衡阳市-祁东县', 1);
INSERT INTO `qmgx_region` VALUES (2209, '430481', '耒阳市', 221, 3, 0, 'Leiyang Shi', 'LEY', '1-19-221-2209', '湖南省-衡阳市-耒阳市', 1);
INSERT INTO `qmgx_region` VALUES (2210, '430482', '常宁市', 221, 3, 0, 'Changning Shi', 'CNS', '1-19-221-2210', '湖南省-衡阳市-常宁市', 1);
INSERT INTO `qmgx_region` VALUES (2211, '430501', '市辖区', 222, 3, 0, 'Shixiaqu', '2', '1-19-222-2211', '湖南省-邵阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2212, '430502', '双清区', 222, 3, 0, 'Shuangqing Qu', 'SGQ', '1-19-222-2212', '湖南省-邵阳市-双清区', 1);
INSERT INTO `qmgx_region` VALUES (2213, '430503', '大祥区', 222, 3, 0, 'Daxiang Qu', 'DXS', '1-19-222-2213', '湖南省-邵阳市-大祥区', 1);
INSERT INTO `qmgx_region` VALUES (2214, '430511', '北塔区', 222, 3, 0, 'Beita Qu', 'BET', '1-19-222-2214', '湖南省-邵阳市-北塔区', 1);
INSERT INTO `qmgx_region` VALUES (2215, '430521', '邵东县', 222, 3, 0, 'Shaodong Xian', 'SDG', '1-19-222-2215', '湖南省-邵阳市-邵东县', 1);
INSERT INTO `qmgx_region` VALUES (2216, '430522', '新邵县', 222, 3, 0, 'Xinshao Xian', 'XSO', '1-19-222-2216', '湖南省-邵阳市-新邵县', 1);
INSERT INTO `qmgx_region` VALUES (2217, '430523', '邵阳县', 222, 3, 0, 'Shaoyang Xian', 'SYW', '1-19-222-2217', '湖南省-邵阳市-邵阳县', 1);
INSERT INTO `qmgx_region` VALUES (2218, '430524', '隆回县', 222, 3, 0, 'Longhui Xian', 'LGH', '1-19-222-2218', '湖南省-邵阳市-隆回县', 1);
INSERT INTO `qmgx_region` VALUES (2219, '430525', '洞口县', 222, 3, 0, 'Dongkou Xian', 'DGK', '1-19-222-2219', '湖南省-邵阳市-洞口县', 1);
INSERT INTO `qmgx_region` VALUES (2220, '430527', '绥宁县', 222, 3, 0, 'Suining Xian', 'SNX', '1-19-222-2220', '湖南省-邵阳市-绥宁县', 1);
INSERT INTO `qmgx_region` VALUES (2221, '430528', '新宁县', 222, 3, 0, 'Xinning Xian', 'XNI', '1-19-222-2221', '湖南省-邵阳市-新宁县', 1);
INSERT INTO `qmgx_region` VALUES (2222, '430529', '城步苗族自治县', 222, 3, 0, 'Chengbu Miaozu Zizhixian', 'CBU', '1-19-222-2222', '湖南省-邵阳市-城步苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2223, '430581', '武冈市', 222, 3, 0, 'Wugang Shi', 'WGS', '1-19-222-2223', '湖南省-邵阳市-武冈市', 1);
INSERT INTO `qmgx_region` VALUES (2224, '430601', '市辖区', 223, 3, 0, 'Shixiaqu', '2', '1-19-223-2224', '湖南省-岳阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2225, '430602', '岳阳楼区', 223, 3, 0, 'Yueyanglou Qu', 'YYL', '1-19-223-2225', '湖南省-岳阳市-岳阳楼区', 1);
INSERT INTO `qmgx_region` VALUES (2226, '430603', '云溪区', 223, 3, 0, 'Yunxi Qu', 'YXI', '1-19-223-2226', '湖南省-岳阳市-云溪区', 1);
INSERT INTO `qmgx_region` VALUES (2227, '430611', '君山区', 223, 3, 0, 'Junshan Qu', 'JUS', '1-19-223-2227', '湖南省-岳阳市-君山区', 1);
INSERT INTO `qmgx_region` VALUES (2228, '430621', '岳阳县', 223, 3, 0, 'Yueyang Xian', 'YYX', '1-19-223-2228', '湖南省-岳阳市-岳阳县', 1);
INSERT INTO `qmgx_region` VALUES (2229, '430623', '华容县', 223, 3, 0, 'Huarong Xian', 'HRG', '1-19-223-2229', '湖南省-岳阳市-华容县', 1);
INSERT INTO `qmgx_region` VALUES (2230, '430624', '湘阴县', 223, 3, 0, 'Xiangyin Xian', 'XYN', '1-19-223-2230', '湖南省-岳阳市-湘阴县', 1);
INSERT INTO `qmgx_region` VALUES (2231, '430626', '平江县', 223, 3, 0, 'Pingjiang Xian', 'PJH', '1-19-223-2231', '湖南省-岳阳市-平江县', 1);
INSERT INTO `qmgx_region` VALUES (2232, '430681', '汨罗市', 223, 3, 0, 'Miluo Shi', 'MLU', '1-19-223-2232', '湖南省-岳阳市-汨罗市', 1);
INSERT INTO `qmgx_region` VALUES (2233, '430682', '临湘市', 223, 3, 0, 'Linxiang Shi', 'LXY', '1-19-223-2233', '湖南省-岳阳市-临湘市', 1);
INSERT INTO `qmgx_region` VALUES (2234, '430701', '市辖区', 224, 3, 0, 'Shixiaqu', '2', '1-19-224-2234', '湖南省-常德市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2235, '430702', '武陵区', 224, 3, 0, 'Wuling Qu', 'WLQ', '1-19-224-2235', '湖南省-常德市-武陵区', 1);
INSERT INTO `qmgx_region` VALUES (2236, '430703', '鼎城区', 224, 3, 0, 'Dingcheng Qu', 'DCE', '1-19-224-2236', '湖南省-常德市-鼎城区', 1);
INSERT INTO `qmgx_region` VALUES (2237, '430721', '安乡县', 224, 3, 0, 'Anxiang Xian', 'AXG', '1-19-224-2237', '湖南省-常德市-安乡县', 1);
INSERT INTO `qmgx_region` VALUES (2238, '430722', '汉寿县', 224, 3, 0, 'Hanshou Xian', 'HSO', '1-19-224-2238', '湖南省-常德市-汉寿县', 1);
INSERT INTO `qmgx_region` VALUES (2239, '430723', '澧县', 224, 3, 0, 'Li Xian', 'LXX', '1-19-224-2239', '湖南省-常德市-澧县', 1);
INSERT INTO `qmgx_region` VALUES (2240, '430724', '临澧县', 224, 3, 0, 'Linli Xian', 'LNL', '1-19-224-2240', '湖南省-常德市-临澧县', 1);
INSERT INTO `qmgx_region` VALUES (2241, '430725', '桃源县', 224, 3, 0, 'Taoyuan Xian', 'TOY', '1-19-224-2241', '湖南省-常德市-桃源县', 1);
INSERT INTO `qmgx_region` VALUES (2242, '430726', '石门县', 224, 3, 0, 'Shimen Xian', 'SHM', '1-19-224-2242', '湖南省-常德市-石门县', 1);
INSERT INTO `qmgx_region` VALUES (2243, '430781', '津市市', 224, 3, 0, 'Jinshi Shi', 'JSS', '1-19-224-2243', '湖南省-常德市-津市市', 1);
INSERT INTO `qmgx_region` VALUES (2244, '430801', '市辖区', 225, 3, 0, 'Shixiaqu', '2', '1-19-225-2244', '湖南省-张家界市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2245, '430802', '永定区', 225, 3, 0, 'Yongding Qu', 'YDQ', '1-19-225-2245', '湖南省-张家界市-永定区', 1);
INSERT INTO `qmgx_region` VALUES (2246, '430811', '武陵源区', 225, 3, 0, 'Wulingyuan Qu', 'WLY', '1-19-225-2246', '湖南省-张家界市-武陵源区', 1);
INSERT INTO `qmgx_region` VALUES (2247, '430821', '慈利县', 225, 3, 0, 'Cili Xian', 'CLI', '1-19-225-2247', '湖南省-张家界市-慈利县', 1);
INSERT INTO `qmgx_region` VALUES (2248, '430822', '桑植县', 225, 3, 0, 'Sangzhi Xian', 'SZT', '1-19-225-2248', '湖南省-张家界市-桑植县', 1);
INSERT INTO `qmgx_region` VALUES (2249, '430901', '市辖区', 226, 3, 0, 'Shixiaqu', '2', '1-19-226-2249', '湖南省-益阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2250, '430902', '资阳区', 226, 3, 0, 'Ziyang Qu', 'ZYC', '1-19-226-2250', '湖南省-益阳市-资阳区', 1);
INSERT INTO `qmgx_region` VALUES (2251, '430903', '赫山区', 226, 3, 0, 'Heshan Qu', 'HSY', '1-19-226-2251', '湖南省-益阳市-赫山区', 1);
INSERT INTO `qmgx_region` VALUES (2252, '430921', '南县', 226, 3, 0, 'Nan Xian', 'NXN', '1-19-226-2252', '湖南省-益阳市-南县', 1);
INSERT INTO `qmgx_region` VALUES (2253, '430922', '桃江县', 226, 3, 0, 'Taojiang Xian', 'TJG', '1-19-226-2253', '湖南省-益阳市-桃江县', 1);
INSERT INTO `qmgx_region` VALUES (2254, '430923', '安化县', 226, 3, 0, 'Anhua Xian', 'ANH', '1-19-226-2254', '湖南省-益阳市-安化县', 1);
INSERT INTO `qmgx_region` VALUES (2255, '430981', '沅江市', 226, 3, 0, 'Yuanjiang Shi', 'YJS', '1-19-226-2255', '湖南省-益阳市-沅江市', 1);
INSERT INTO `qmgx_region` VALUES (2256, '431001', '市辖区', 227, 3, 0, 'Shixiaqu', '2', '1-19-227-2256', '湖南省-郴州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2257, '431002', '北湖区', 227, 3, 0, 'Beihu Qu', 'BHQ', '1-19-227-2257', '湖南省-郴州市-北湖区', 1);
INSERT INTO `qmgx_region` VALUES (2258, '431003', '苏仙区', 227, 3, 0, 'Suxian Qu', '2', '1-19-227-2258', '湖南省-郴州市-苏仙区', 1);
INSERT INTO `qmgx_region` VALUES (2259, '431021', '桂阳县', 227, 3, 0, 'Guiyang Xian', 'GYX', '1-19-227-2259', '湖南省-郴州市-桂阳县', 1);
INSERT INTO `qmgx_region` VALUES (2260, '431022', '宜章县', 227, 3, 0, 'yizhang Xian', 'YZA', '1-19-227-2260', '湖南省-郴州市-宜章县', 1);
INSERT INTO `qmgx_region` VALUES (2261, '431023', '永兴县', 227, 3, 0, 'Yongxing Xian', 'YXX', '1-19-227-2261', '湖南省-郴州市-永兴县', 1);
INSERT INTO `qmgx_region` VALUES (2262, '431024', '嘉禾县', 227, 3, 0, 'Jiahe Xian', 'JAH', '1-19-227-2262', '湖南省-郴州市-嘉禾县', 1);
INSERT INTO `qmgx_region` VALUES (2263, '431025', '临武县', 227, 3, 0, 'Linwu Xian', 'LWX', '1-19-227-2263', '湖南省-郴州市-临武县', 1);
INSERT INTO `qmgx_region` VALUES (2264, '431026', '汝城县', 227, 3, 0, 'Rucheng Xian', 'RCE', '1-19-227-2264', '湖南省-郴州市-汝城县', 1);
INSERT INTO `qmgx_region` VALUES (2265, '431027', '桂东县', 227, 3, 0, 'Guidong Xian', 'GDO', '1-19-227-2265', '湖南省-郴州市-桂东县', 1);
INSERT INTO `qmgx_region` VALUES (2266, '431028', '安仁县', 227, 3, 0, 'Anren Xian', 'ARN', '1-19-227-2266', '湖南省-郴州市-安仁县', 1);
INSERT INTO `qmgx_region` VALUES (2267, '431081', '资兴市', 227, 3, 0, 'Zixing Shi', 'ZXG', '1-19-227-2267', '湖南省-郴州市-资兴市', 1);
INSERT INTO `qmgx_region` VALUES (2268, '431101', '市辖区', 228, 3, 0, 'Shixiaqu', '2', '1-19-228-2268', '湖南省-永州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2270, '431103', '冷水滩区', 228, 3, 0, 'Lengshuitan Qu', 'LST', '1-19-228-2270', '湖南省-永州市-冷水滩区', 1);
INSERT INTO `qmgx_region` VALUES (2271, '431121', '祁阳县', 228, 3, 0, 'Qiyang Xian', 'QJY', '1-19-228-2271', '湖南省-永州市-祁阳县', 1);
INSERT INTO `qmgx_region` VALUES (2272, '431122', '东安县', 228, 3, 0, 'Dong,an Xian', 'DOA', '1-19-228-2272', '湖南省-永州市-东安县', 1);
INSERT INTO `qmgx_region` VALUES (2273, '431123', '双牌县', 228, 3, 0, 'Shuangpai Xian', 'SPA', '1-19-228-2273', '湖南省-永州市-双牌县', 1);
INSERT INTO `qmgx_region` VALUES (2274, '431124', '道县', 228, 3, 0, 'Dao Xian', 'DAO', '1-19-228-2274', '湖南省-永州市-道县', 1);
INSERT INTO `qmgx_region` VALUES (2275, '431125', '江永县', 228, 3, 0, 'Jiangyong Xian', 'JYD', '1-19-228-2275', '湖南省-永州市-江永县', 1);
INSERT INTO `qmgx_region` VALUES (2276, '431126', '宁远县', 228, 3, 0, 'Ningyuan Xian', 'NYN', '1-19-228-2276', '湖南省-永州市-宁远县', 1);
INSERT INTO `qmgx_region` VALUES (2277, '431127', '蓝山县', 228, 3, 0, 'Lanshan Xian', 'LNS', '1-19-228-2277', '湖南省-永州市-蓝山县', 1);
INSERT INTO `qmgx_region` VALUES (2278, '431128', '新田县', 228, 3, 0, 'Xintian Xian', 'XTN', '1-19-228-2278', '湖南省-永州市-新田县', 1);
INSERT INTO `qmgx_region` VALUES (2279, '431129', '江华瑶族自治县', 228, 3, 0, 'Jianghua Yaozu Zizhixian', 'JHX', '1-19-228-2279', '湖南省-永州市-江华瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2280, '431201', '市辖区', 229, 3, 0, 'Shixiaqu', '2', '1-19-229-2280', '湖南省-怀化市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2281, '431202', '鹤城区', 229, 3, 0, 'Hecheng Qu', 'HCG', '1-19-229-2281', '湖南省-怀化市-鹤城区', 1);
INSERT INTO `qmgx_region` VALUES (2282, '431221', '中方县', 229, 3, 0, 'Zhongfang Xian', 'ZFX', '1-19-229-2282', '湖南省-怀化市-中方县', 1);
INSERT INTO `qmgx_region` VALUES (2283, '431222', '沅陵县', 229, 3, 0, 'Yuanling Xian', 'YNL', '1-19-229-2283', '湖南省-怀化市-沅陵县', 1);
INSERT INTO `qmgx_region` VALUES (2284, '431223', '辰溪县', 229, 3, 0, 'Chenxi Xian', 'CXX', '1-19-229-2284', '湖南省-怀化市-辰溪县', 1);
INSERT INTO `qmgx_region` VALUES (2285, '431224', '溆浦县', 229, 3, 0, 'Xupu Xian', 'XUP', '1-19-229-2285', '湖南省-怀化市-溆浦县', 1);
INSERT INTO `qmgx_region` VALUES (2286, '431225', '会同县', 229, 3, 0, 'Huitong Xian', 'HTG', '1-19-229-2286', '湖南省-怀化市-会同县', 1);
INSERT INTO `qmgx_region` VALUES (2287, '431226', '麻阳苗族自治县', 229, 3, 0, 'Mayang Miaozu Zizhixian', 'MYX', '1-19-229-2287', '湖南省-怀化市-麻阳苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2288, '431227', '新晃侗族自治县', 229, 3, 0, 'Xinhuang Dongzu Zizhixian', 'XHD', '1-19-229-2288', '湖南省-怀化市-新晃侗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2289, '431228', '芷江侗族自治县', 229, 3, 0, 'Zhijiang Dongzu Zizhixian', 'ZJX', '1-19-229-2289', '湖南省-怀化市-芷江侗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2290, '431229', '靖州苗族侗族自治县', 229, 3, 0, 'Jingzhou Miaozu Dongzu Zizhixian', 'JZO', '1-19-229-2290', '湖南省-怀化市-靖州苗族侗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2291, '431230', '通道侗族自治县', 229, 3, 0, 'Tongdao Dongzu Zizhixian', 'TDD', '1-19-229-2291', '湖南省-怀化市-通道侗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2292, '431281', '洪江市', 229, 3, 0, 'Hongjiang Shi', 'HGJ', '1-19-229-2292', '湖南省-怀化市-洪江市', 1);
INSERT INTO `qmgx_region` VALUES (2293, '431301', '市辖区', 230, 3, 0, '1', '2', '1-19-230-2293', '湖南省-娄底市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2294, '431302', '娄星区', 230, 3, 0, 'Louxing Qu', '2', '1-19-230-2294', '湖南省-娄底市-娄星区', 1);
INSERT INTO `qmgx_region` VALUES (2295, '431321', '双峰县', 230, 3, 0, 'Shuangfeng Xian', '2', '1-19-230-2295', '湖南省-娄底市-双峰县', 1);
INSERT INTO `qmgx_region` VALUES (2296, '431322', '新化县', 230, 3, 0, 'Xinhua Xian', '2', '1-19-230-2296', '湖南省-娄底市-新化县', 1);
INSERT INTO `qmgx_region` VALUES (2297, '431381', '冷水江市', 230, 3, 0, 'Lengshuijiang Shi', '2', '1-19-230-2297', '湖南省-娄底市-冷水江市', 1);
INSERT INTO `qmgx_region` VALUES (2298, '431382', '涟源市', 230, 3, 0, 'Lianyuan Shi', '2', '1-19-230-2298', '湖南省-娄底市-涟源市', 1);
INSERT INTO `qmgx_region` VALUES (2299, '433101', '吉首市', 231, 3, 0, 'Jishou Shi', 'JSO', '1-19-231-2299', '湖南省-湘西土家族苗族自治州-吉首市', 1);
INSERT INTO `qmgx_region` VALUES (2300, '433122', '泸溪县', 231, 3, 0, 'Luxi Xian', 'LXW', '1-19-231-2300', '湖南省-湘西土家族苗族自治州-泸溪县', 1);
INSERT INTO `qmgx_region` VALUES (2301, '433123', '凤凰县', 231, 3, 0, 'Fenghuang Xian', 'FHX', '1-19-231-2301', '湖南省-湘西土家族苗族自治州-凤凰县', 1);
INSERT INTO `qmgx_region` VALUES (2302, '433124', '花垣县', 231, 3, 0, 'Huayuan Xian', 'HYH', '1-19-231-2302', '湖南省-湘西土家族苗族自治州-花垣县', 1);
INSERT INTO `qmgx_region` VALUES (2303, '433125', '保靖县', 231, 3, 0, 'Baojing Xian', 'BJG', '1-19-231-2303', '湖南省-湘西土家族苗族自治州-保靖县', 1);
INSERT INTO `qmgx_region` VALUES (2304, '433126', '古丈县', 231, 3, 0, 'Guzhang Xian', 'GZG', '1-19-231-2304', '湖南省-湘西土家族苗族自治州-古丈县', 1);
INSERT INTO `qmgx_region` VALUES (2305, '433127', '永顺县', 231, 3, 0, 'Yongshun Xian', 'YSF', '1-19-231-2305', '湖南省-湘西土家族苗族自治州-永顺县', 1);
INSERT INTO `qmgx_region` VALUES (2306, '433130', '龙山县', 231, 3, 0, 'Longshan Xian', 'LSR', '1-19-231-2306', '湖南省-湘西土家族苗族自治州-龙山县', 1);
INSERT INTO `qmgx_region` VALUES (2307, '440101', '市辖区', 232, 3, 0, 'Shixiaqu', '2', '1-20-232-2307', '广东省-广州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2308, '440115', '南沙区', 232, 3, 0, 'Nansha Qu', '2', '1-20-232-2308', '广东省-广州市-南沙区', 1);
INSERT INTO `qmgx_region` VALUES (2309, '440103', '荔湾区', 232, 3, 0, 'Liwan Qu', 'LWQ', '1-20-232-2309', '广东省-广州市-荔湾区', 1);
INSERT INTO `qmgx_region` VALUES (2310, '440104', '越秀区', 232, 3, 0, 'Yuexiu Qu', 'YXU', '1-20-232-2310', '广东省-广州市-越秀区', 1);
INSERT INTO `qmgx_region` VALUES (2311, '440105', '海珠区', 232, 3, 0, 'Haizhu Qu', 'HZU', '1-20-232-2311', '广东省-广州市-海珠区', 1);
INSERT INTO `qmgx_region` VALUES (2312, '440106', '天河区', 232, 3, 0, 'Tianhe Qu', 'THQ', '1-20-232-2312', '广东省-广州市-天河区', 1);
INSERT INTO `qmgx_region` VALUES (2313, '440116', '萝岗区', 232, 3, 0, 'Luogang Qu', '2', '1-20-232-2313', '广东省-广州市-萝岗区', 1);
INSERT INTO `qmgx_region` VALUES (2314, '440111', '白云区', 232, 3, 0, 'Baiyun Qu', 'BYN', '1-20-232-2314', '广东省-广州市-白云区', 1);
INSERT INTO `qmgx_region` VALUES (2315, '440112', '黄埔区', 232, 3, 0, 'Huangpu Qu', 'HPU', '1-20-232-2315', '广东省-广州市-黄埔区', 1);
INSERT INTO `qmgx_region` VALUES (2316, '440113', '番禺区', 232, 3, 0, 'Panyu Qu', 'PNY', '1-20-232-2316', '广东省-广州市-番禺区', 1);
INSERT INTO `qmgx_region` VALUES (2317, '440114', '花都区', 232, 3, 0, 'Huadu Qu', 'HDU', '1-20-232-2317', '广东省-广州市-花都区', 1);
INSERT INTO `qmgx_region` VALUES (2318, '440183', '增城市', 232, 3, 0, 'Zengcheng Shi', 'ZEC', '1-20-232-2318', '广东省-广州市-增城市', 1);
INSERT INTO `qmgx_region` VALUES (2319, '440184', '从化市', 232, 3, 0, 'Conghua Shi', 'CNH', '1-20-232-2319', '广东省-广州市-从化市', 1);
INSERT INTO `qmgx_region` VALUES (2320, '440201', '市辖区', 233, 3, 0, 'Shixiaqu', '2', '1-20-233-2320', '广东省-韶关市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2321, '440203', '武江区', 233, 3, 0, 'Wujiang Qu', 'WJQ', '1-20-233-2321', '广东省-韶关市-武江区', 1);
INSERT INTO `qmgx_region` VALUES (2322, '440204', '浈江区', 233, 3, 0, 'Zhenjiang Qu', 'ZJQ', '1-20-233-2322', '广东省-韶关市-浈江区', 1);
INSERT INTO `qmgx_region` VALUES (2323, '440205', '曲江区', 233, 3, 0, 'Qujiang Qu', '2', '1-20-233-2323', '广东省-韶关市-曲江区', 1);
INSERT INTO `qmgx_region` VALUES (2324, '440222', '始兴县', 233, 3, 0, 'Shixing Xian', 'SXX', '1-20-233-2324', '广东省-韶关市-始兴县', 1);
INSERT INTO `qmgx_region` VALUES (2325, '440224', '仁化县', 233, 3, 0, 'Renhua Xian', 'RHA', '1-20-233-2325', '广东省-韶关市-仁化县', 1);
INSERT INTO `qmgx_region` VALUES (2326, '440229', '翁源县', 233, 3, 0, 'Wengyuan Xian', 'WYN', '1-20-233-2326', '广东省-韶关市-翁源县', 1);
INSERT INTO `qmgx_region` VALUES (2327, '440232', '乳源瑶族自治县', 233, 3, 0, 'Ruyuan Yaozu Zizhixian', 'RYN', '1-20-233-2327', '广东省-韶关市-乳源瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2328, '440233', '新丰县', 233, 3, 0, 'Xinfeng Xian', 'XFY', '1-20-233-2328', '广东省-韶关市-新丰县', 1);
INSERT INTO `qmgx_region` VALUES (2329, '440281', '乐昌市', 233, 3, 0, 'Lechang Shi', 'LEC', '1-20-233-2329', '广东省-韶关市-乐昌市', 1);
INSERT INTO `qmgx_region` VALUES (2330, '440282', '南雄市', 233, 3, 0, 'Nanxiong Shi', 'NXS', '1-20-233-2330', '广东省-韶关市-南雄市', 1);
INSERT INTO `qmgx_region` VALUES (2331, '440301', '市辖区', 234, 3, 0, 'Shixiaqu', '2', '1-20-234-2331', '广东省-深圳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2332, '440303', '罗湖区', 234, 3, 0, 'Luohu Qu', 'LHQ', '1-20-234-2332', '广东省-深圳市-罗湖区', 1);
INSERT INTO `qmgx_region` VALUES (2333, '440304', '福田区', 234, 3, 0, 'Futian Qu', 'FTN', '1-20-234-2333', '广东省-深圳市-福田区', 1);
INSERT INTO `qmgx_region` VALUES (2334, '440305', '南山区', 234, 3, 0, 'Nanshan Qu', 'NSN', '1-20-234-2334', '广东省-深圳市-南山区', 1);
INSERT INTO `qmgx_region` VALUES (2335, '440306', '宝安区', 234, 3, 0, 'Bao,an Qu', 'BAQ', '1-20-234-2335', '广东省-深圳市-宝安区', 1);
INSERT INTO `qmgx_region` VALUES (2336, '440307', '龙岗区', 234, 3, 0, 'Longgang Qu', 'LGG', '1-20-234-2336', '广东省-深圳市-龙岗区', 1);
INSERT INTO `qmgx_region` VALUES (2337, '440308', '盐田区', 234, 3, 0, 'Yan Tian Qu', 'YTQ', '1-20-234-2337', '广东省-深圳市-盐田区', 1);
INSERT INTO `qmgx_region` VALUES (2338, '440401', '市辖区', 235, 3, 0, 'Shixiaqu', '2', '1-20-235-2338', '广东省-珠海市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2339, '440402', '香洲区', 235, 3, 0, 'Xiangzhou Qu', 'XZQ', '1-20-235-2339', '广东省-珠海市-香洲区', 1);
INSERT INTO `qmgx_region` VALUES (2340, '440403', '斗门区', 235, 3, 0, 'Doumen Qu', 'DOU', '1-20-235-2340', '广东省-珠海市-斗门区', 1);
INSERT INTO `qmgx_region` VALUES (2341, '440404', '金湾区', 235, 3, 0, 'Jinwan Qu', 'JW Q', '1-20-235-2341', '广东省-珠海市-金湾区', 1);
INSERT INTO `qmgx_region` VALUES (2342, '440501', '市辖区', 236, 3, 0, 'Shixiaqu', '2', '1-20-236-2342', '广东省-汕头市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2343, '440507', '龙湖区', 236, 3, 0, 'Longhu Qu', 'LHH', '1-20-236-2343', '广东省-汕头市-龙湖区', 1);
INSERT INTO `qmgx_region` VALUES (2344, '440511', '金平区', 236, 3, 0, 'Jinping Qu', 'JPQ', '1-20-236-2344', '广东省-汕头市-金平区', 1);
INSERT INTO `qmgx_region` VALUES (2345, '440512', '濠江区', 236, 3, 0, 'Haojiang Qu', 'HJ Q', '1-20-236-2345', '广东省-汕头市-濠江区', 1);
INSERT INTO `qmgx_region` VALUES (2346, '440513', '潮阳区', 236, 3, 0, 'Chaoyang  Qu', 'CHY', '1-20-236-2346', '广东省-汕头市-潮阳区', 1);
INSERT INTO `qmgx_region` VALUES (2347, '440514', '潮南区', 236, 3, 0, 'Chaonan Qu', 'CN Q', '1-20-236-2347', '广东省-汕头市-潮南区', 1);
INSERT INTO `qmgx_region` VALUES (2348, '440515', '澄海区', 236, 3, 0, 'Chenghai QU', 'CHS', '1-20-236-2348', '广东省-汕头市-澄海区', 1);
INSERT INTO `qmgx_region` VALUES (2349, '440523', '南澳县', 236, 3, 0, 'Nan,ao Xian', 'NAN', '1-20-236-2349', '广东省-汕头市-南澳县', 1);
INSERT INTO `qmgx_region` VALUES (2350, '440601', '市辖区', 237, 3, 0, 'Shixiaqu', '2', '1-20-237-2350', '广东省-佛山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2351, '440604', '禅城区', 237, 3, 0, 'Chancheng Qu', 'CC Q', '1-20-237-2351', '广东省-佛山市-禅城区', 1);
INSERT INTO `qmgx_region` VALUES (2352, '440605', '南海区', 237, 3, 0, 'Nanhai Shi', 'NAH', '1-20-237-2352', '广东省-佛山市-南海区', 1);
INSERT INTO `qmgx_region` VALUES (2353, '440606', '顺德区', 237, 3, 0, 'Shunde Shi', 'SUD', '1-20-237-2353', '广东省-佛山市-顺德区', 1);
INSERT INTO `qmgx_region` VALUES (2354, '440607', '三水区', 237, 3, 0, 'Sanshui Shi', 'SJQ', '1-20-237-2354', '广东省-佛山市-三水区', 1);
INSERT INTO `qmgx_region` VALUES (2355, '440608', '高明区', 237, 3, 0, 'Gaoming Shi', 'GOM', '1-20-237-2355', '广东省-佛山市-高明区', 1);
INSERT INTO `qmgx_region` VALUES (2356, '440701', '市辖区', 238, 3, 0, 'Shixiaqu', '2', '1-20-238-2356', '广东省-江门市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2357, '440703', '蓬江区', 238, 3, 0, 'Pengjiang Qu', 'PJJ', '1-20-238-2357', '广东省-江门市-蓬江区', 1);
INSERT INTO `qmgx_region` VALUES (2358, '440704', '江海区', 238, 3, 0, 'Jianghai Qu', 'JHI', '1-20-238-2358', '广东省-江门市-江海区', 1);
INSERT INTO `qmgx_region` VALUES (2359, '440705', '新会区', 238, 3, 0, 'Xinhui Shi', 'XIN', '1-20-238-2359', '广东省-江门市-新会区', 1);
INSERT INTO `qmgx_region` VALUES (2360, '440781', '台山市', 238, 3, 0, 'Taishan Shi', 'TSS', '1-20-238-2360', '广东省-江门市-台山市', 1);
INSERT INTO `qmgx_region` VALUES (2361, '440783', '开平市', 238, 3, 0, 'Kaiping Shi', 'KPS', '1-20-238-2361', '广东省-江门市-开平市', 1);
INSERT INTO `qmgx_region` VALUES (2362, '440784', '鹤山市', 238, 3, 0, 'Heshan Shi', 'HES', '1-20-238-2362', '广东省-江门市-鹤山市', 1);
INSERT INTO `qmgx_region` VALUES (2363, '440785', '恩平市', 238, 3, 0, 'Enping Shi', 'ENP', '1-20-238-2363', '广东省-江门市-恩平市', 1);
INSERT INTO `qmgx_region` VALUES (2364, '440801', '市辖区', 239, 3, 0, 'Shixiaqu', '2', '1-20-239-2364', '广东省-湛江市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2365, '440802', '赤坎区', 239, 3, 0, 'Chikan Qu', 'CKQ', '1-20-239-2365', '广东省-湛江市-赤坎区', 1);
INSERT INTO `qmgx_region` VALUES (2366, '440803', '霞山区', 239, 3, 0, 'Xiashan Qu', 'XAS', '1-20-239-2366', '广东省-湛江市-霞山区', 1);
INSERT INTO `qmgx_region` VALUES (2367, '440804', '坡头区', 239, 3, 0, 'Potou Qu', 'PTU', '1-20-239-2367', '广东省-湛江市-坡头区', 1);
INSERT INTO `qmgx_region` VALUES (2368, '440811', '麻章区', 239, 3, 0, 'Mazhang Qu', 'MZQ', '1-20-239-2368', '广东省-湛江市-麻章区', 1);
INSERT INTO `qmgx_region` VALUES (2369, '440823', '遂溪县', 239, 3, 0, 'Suixi Xian', 'SXI', '1-20-239-2369', '广东省-湛江市-遂溪县', 1);
INSERT INTO `qmgx_region` VALUES (2370, '440825', '徐闻县', 239, 3, 0, 'Xuwen Xian', 'XWN', '1-20-239-2370', '广东省-湛江市-徐闻县', 1);
INSERT INTO `qmgx_region` VALUES (2371, '440881', '廉江市', 239, 3, 0, 'Lianjiang Shi', 'LJS', '1-20-239-2371', '广东省-湛江市-廉江市', 1);
INSERT INTO `qmgx_region` VALUES (2372, '440882', '雷州市', 239, 3, 0, 'Leizhou Shi', 'LEZ', '1-20-239-2372', '广东省-湛江市-雷州市', 1);
INSERT INTO `qmgx_region` VALUES (2373, '440883', '吴川市', 239, 3, 0, 'Wuchuan Shi', 'WCS', '1-20-239-2373', '广东省-湛江市-吴川市', 1);
INSERT INTO `qmgx_region` VALUES (2374, '440901', '市辖区', 240, 3, 0, 'Shixiaqu', '2', '1-20-240-2374', '广东省-茂名市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2375, '440902', '茂南区', 240, 3, 0, 'Maonan Qu', 'MNQ', '1-20-240-2375', '广东省-茂名市-茂南区', 1);
INSERT INTO `qmgx_region` VALUES (2376, '440903', '茂港区', 240, 3, 0, 'Maogang Qu', 'MGQ', '1-20-240-2376', '广东省-茂名市-茂港区', 1);
INSERT INTO `qmgx_region` VALUES (2377, '440923', '电白县', 240, 3, 0, 'Dianbai Xian', 'DBI', '1-20-240-2377', '广东省-茂名市-电白县', 1);
INSERT INTO `qmgx_region` VALUES (2378, '440981', '高州市', 240, 3, 0, 'Gaozhou Shi', 'GZO', '1-20-240-2378', '广东省-茂名市-高州市', 1);
INSERT INTO `qmgx_region` VALUES (2379, '440982', '化州市', 240, 3, 0, 'Huazhou Shi', 'HZY', '1-20-240-2379', '广东省-茂名市-化州市', 1);
INSERT INTO `qmgx_region` VALUES (2380, '440983', '信宜市', 240, 3, 0, 'Xinyi Shi', 'XYY', '1-20-240-2380', '广东省-茂名市-信宜市', 1);
INSERT INTO `qmgx_region` VALUES (2381, '441201', '市辖区', 241, 3, 0, 'Shixiaqu', '2', '1-20-241-2381', '广东省-肇庆市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2382, '441202', '端州区', 241, 3, 0, 'Duanzhou Qu', 'DZQ', '1-20-241-2382', '广东省-肇庆市-端州区', 1);
INSERT INTO `qmgx_region` VALUES (2383, '441203', '鼎湖区', 241, 3, 0, 'Dinghu Qu', 'DGH', '1-20-241-2383', '广东省-肇庆市-鼎湖区', 1);
INSERT INTO `qmgx_region` VALUES (2384, '441223', '广宁县', 241, 3, 0, 'Guangning Xian', 'GNG', '1-20-241-2384', '广东省-肇庆市-广宁县', 1);
INSERT INTO `qmgx_region` VALUES (2385, '441224', '怀集县', 241, 3, 0, 'Huaiji Xian', 'HJX', '1-20-241-2385', '广东省-肇庆市-怀集县', 1);
INSERT INTO `qmgx_region` VALUES (2386, '441225', '封开县', 241, 3, 0, 'Fengkai Xian', 'FKX', '1-20-241-2386', '广东省-肇庆市-封开县', 1);
INSERT INTO `qmgx_region` VALUES (2387, '441226', '德庆县', 241, 3, 0, 'Deqing Xian', 'DQY', '1-20-241-2387', '广东省-肇庆市-德庆县', 1);
INSERT INTO `qmgx_region` VALUES (2388, '441283', '高要市', 241, 3, 0, 'Gaoyao Xian', 'GYY', '1-20-241-2388', '广东省-肇庆市-高要市', 1);
INSERT INTO `qmgx_region` VALUES (2389, '441284', '四会市', 241, 3, 0, 'Sihui Shi', 'SHI', '1-20-241-2389', '广东省-肇庆市-四会市', 1);
INSERT INTO `qmgx_region` VALUES (2390, '441301', '市辖区', 242, 3, 0, 'Shixiaqu', '2', '1-20-242-2390', '广东省-惠州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2391, '441302', '惠城区', 242, 3, 0, 'Huicheng Qu', 'HCQ', '1-20-242-2391', '广东省-惠州市-惠城区', 1);
INSERT INTO `qmgx_region` VALUES (2392, '441303', '惠阳区', 242, 3, 0, 'Huiyang Shi', 'HUY', '1-20-242-2392', '广东省-惠州市-惠阳区', 1);
INSERT INTO `qmgx_region` VALUES (2393, '441322', '博罗县', 242, 3, 0, 'Boluo Xian', 'BOL', '1-20-242-2393', '广东省-惠州市-博罗县', 1);
INSERT INTO `qmgx_region` VALUES (2394, '441323', '惠东县', 242, 3, 0, 'Huidong Xian', 'HID', '1-20-242-2394', '广东省-惠州市-惠东县', 1);
INSERT INTO `qmgx_region` VALUES (2395, '441324', '龙门县', 242, 3, 0, 'Longmen Xian', 'LMN', '1-20-242-2395', '广东省-惠州市-龙门县', 1);
INSERT INTO `qmgx_region` VALUES (2396, '441401', '市辖区', 243, 3, 0, 'Shixiaqu', '2', '1-20-243-2396', '广东省-梅州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2397, '441402', '梅江区', 243, 3, 0, 'Meijiang Qu', 'MJQ', '1-20-243-2397', '广东省-梅州市-梅江区', 1);
INSERT INTO `qmgx_region` VALUES (2398, '441421', '梅县', 243, 3, 0, 'Mei Xian', 'MEX', '1-20-243-2398', '广东省-梅州市-梅县', 1);
INSERT INTO `qmgx_region` VALUES (2399, '441422', '大埔县', 243, 3, 0, 'Dabu Xian', 'DBX', '1-20-243-2399', '广东省-梅州市-大埔县', 1);
INSERT INTO `qmgx_region` VALUES (2400, '441423', '丰顺县', 243, 3, 0, 'Fengshun Xian', 'FES', '1-20-243-2400', '广东省-梅州市-丰顺县', 1);
INSERT INTO `qmgx_region` VALUES (2401, '441424', '五华县', 243, 3, 0, 'Wuhua Xian', 'WHY', '1-20-243-2401', '广东省-梅州市-五华县', 1);
INSERT INTO `qmgx_region` VALUES (2402, '441426', '平远县', 243, 3, 0, 'Pingyuan Xian', 'PYY', '1-20-243-2402', '广东省-梅州市-平远县', 1);
INSERT INTO `qmgx_region` VALUES (2403, '441427', '蕉岭县', 243, 3, 0, 'Jiaoling Xian', 'JOL', '1-20-243-2403', '广东省-梅州市-蕉岭县', 1);
INSERT INTO `qmgx_region` VALUES (2404, '441481', '兴宁市', 243, 3, 0, 'Xingning Shi', 'XNG', '1-20-243-2404', '广东省-梅州市-兴宁市', 1);
INSERT INTO `qmgx_region` VALUES (2405, '441501', '市辖区', 244, 3, 0, 'Shixiaqu', '2', '1-20-244-2405', '广东省-汕尾市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2406, '441502', '城区', 244, 3, 0, 'Chengqu', 'CQS', '1-20-244-2406', '广东省-汕尾市-城区', 1);
INSERT INTO `qmgx_region` VALUES (2407, '441521', '海丰县', 244, 3, 0, 'Haifeng Xian', 'HIF', '1-20-244-2407', '广东省-汕尾市-海丰县', 1);
INSERT INTO `qmgx_region` VALUES (2408, '441523', '陆河县', 244, 3, 0, 'Luhe Xian', 'LHY', '1-20-244-2408', '广东省-汕尾市-陆河县', 1);
INSERT INTO `qmgx_region` VALUES (2409, '441581', '陆丰市', 244, 3, 0, 'Lufeng Shi', 'LUF', '1-20-244-2409', '广东省-汕尾市-陆丰市', 1);
INSERT INTO `qmgx_region` VALUES (2410, '441601', '市辖区', 245, 3, 0, 'Shixiaqu', '2', '1-20-245-2410', '广东省-河源市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2411, '441602', '源城区', 245, 3, 0, 'Yuancheng Qu', 'YCQ', '1-20-245-2411', '广东省-河源市-源城区', 1);
INSERT INTO `qmgx_region` VALUES (2412, '441621', '紫金县', 245, 3, 0, 'Zijin Xian', 'ZJY', '1-20-245-2412', '广东省-河源市-紫金县', 1);
INSERT INTO `qmgx_region` VALUES (2413, '441622', '龙川县', 245, 3, 0, 'Longchuan Xian', 'LCY', '1-20-245-2413', '广东省-河源市-龙川县', 1);
INSERT INTO `qmgx_region` VALUES (2414, '441623', '连平县', 245, 3, 0, 'Lianping Xian', 'LNP', '1-20-245-2414', '广东省-河源市-连平县', 1);
INSERT INTO `qmgx_region` VALUES (2415, '441624', '和平县', 245, 3, 0, 'Heping Xian', 'HPY', '1-20-245-2415', '广东省-河源市-和平县', 1);
INSERT INTO `qmgx_region` VALUES (2416, '441625', '东源县', 245, 3, 0, 'Dongyuan Xian', 'DYN', '1-20-245-2416', '广东省-河源市-东源县', 1);
INSERT INTO `qmgx_region` VALUES (2417, '441701', '市辖区', 246, 3, 0, 'Shixiaqu', '2', '1-20-246-2417', '广东省-阳江市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2418, '441702', '江城区', 246, 3, 0, 'Jiangcheng Qu', 'JCQ', '1-20-246-2418', '广东省-阳江市-江城区', 1);
INSERT INTO `qmgx_region` VALUES (2419, '441721', '阳西县', 246, 3, 0, 'Yangxi Xian', 'YXY', '1-20-246-2419', '广东省-阳江市-阳西县', 1);
INSERT INTO `qmgx_region` VALUES (2420, '441723', '阳东县', 246, 3, 0, 'Yangdong Xian', 'YGD', '1-20-246-2420', '广东省-阳江市-阳东县', 1);
INSERT INTO `qmgx_region` VALUES (2421, '441781', '阳春市', 246, 3, 0, 'Yangchun Shi', 'YCU', '1-20-246-2421', '广东省-阳江市-阳春市', 1);
INSERT INTO `qmgx_region` VALUES (2422, '441801', '市辖区', 247, 3, 0, 'Shixiaqu', '2', '1-20-247-2422', '广东省-清远市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2423, '441802', '清城区', 247, 3, 0, 'Qingcheng Qu', 'QCQ', '1-20-247-2423', '广东省-清远市-清城区', 1);
INSERT INTO `qmgx_region` VALUES (2424, '441821', '佛冈县', 247, 3, 0, 'Fogang Xian', 'FGY', '1-20-247-2424', '广东省-清远市-佛冈县', 1);
INSERT INTO `qmgx_region` VALUES (2425, '441823', '阳山县', 247, 3, 0, 'Yangshan Xian', 'YSN', '1-20-247-2425', '广东省-清远市-阳山县', 1);
INSERT INTO `qmgx_region` VALUES (2426, '441825', '连山壮族瑶族自治县', 247, 3, 0, 'Lianshan Zhuangzu Yaozu Zizhixian', 'LSZ', '1-20-247-2426', '广东省-清远市-连山壮族瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2427, '441826', '连南瑶族自治县', 247, 3, 0, 'Liannanyaozuzizhi Qu', '2', '1-20-247-2427', '广东省-清远市-连南瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2428, '441827', '清新县', 247, 3, 0, 'Qingxin Xian', 'QGX', '1-20-247-2428', '广东省-清远市-清新县', 1);
INSERT INTO `qmgx_region` VALUES (2429, '441881', '英德市', 247, 3, 0, 'Yingde Shi', 'YDS', '1-20-247-2429', '广东省-清远市-英德市', 1);
INSERT INTO `qmgx_region` VALUES (2430, '441882', '连州市', 247, 3, 0, 'Lianzhou Shi', 'LZO', '1-20-247-2430', '广东省-清远市-连州市', 1);
INSERT INTO `qmgx_region` VALUES (2431, '445101', '市辖区', 250, 3, 0, 'Shixiaqu', '2', '1-20-250-2431', '广东省-潮州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2432, '445102', '湘桥区', 250, 3, 0, 'Xiangqiao Qu', 'XQO', '1-20-250-2432', '广东省-潮州市-湘桥区', 1);
INSERT INTO `qmgx_region` VALUES (2433, '445121', '潮安县', 250, 3, 0, 'Chao,an Xian', 'CAY', '1-20-250-2433', '广东省-潮州市-潮安县', 1);
INSERT INTO `qmgx_region` VALUES (2434, '445122', '饶平县', 250, 3, 0, 'Raoping Xian', 'RPG', '1-20-250-2434', '广东省-潮州市-饶平县', 1);
INSERT INTO `qmgx_region` VALUES (2435, '445201', '市辖区', 251, 3, 0, 'Shixiaqu', '2', '1-20-251-2435', '广东省-揭阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2436, '445202', '榕城区', 251, 3, 0, 'Rongcheng Qu', 'RCH', '1-20-251-2436', '广东省-揭阳市-榕城区', 1);
INSERT INTO `qmgx_region` VALUES (2437, '445221', '揭东县', 251, 3, 0, 'Jiedong Xian', 'JDX', '1-20-251-2437', '广东省-揭阳市-揭东县', 1);
INSERT INTO `qmgx_region` VALUES (2438, '445222', '揭西县', 251, 3, 0, 'Jiexi Xian', 'JEX', '1-20-251-2438', '广东省-揭阳市-揭西县', 1);
INSERT INTO `qmgx_region` VALUES (2439, '445224', '惠来县', 251, 3, 0, 'Huilai Xian', 'HLY', '1-20-251-2439', '广东省-揭阳市-惠来县', 1);
INSERT INTO `qmgx_region` VALUES (2440, '445281', '普宁市', 251, 3, 0, 'Puning Shi', 'PNG', '1-20-251-2440', '广东省-揭阳市-普宁市', 1);
INSERT INTO `qmgx_region` VALUES (2441, '445301', '市辖区', 252, 3, 0, 'Shixiaqu', '2', '1-20-252-2441', '广东省-云浮市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2442, '445302', '云城区', 252, 3, 0, 'Yuncheng Qu', 'YYF', '1-20-252-2442', '广东省-云浮市-云城区', 1);
INSERT INTO `qmgx_region` VALUES (2443, '445321', '新兴县', 252, 3, 0, 'Xinxing Xian', 'XNX', '1-20-252-2443', '广东省-云浮市-新兴县', 1);
INSERT INTO `qmgx_region` VALUES (2444, '445322', '郁南县', 252, 3, 0, 'Yunan Xian', 'YNK', '1-20-252-2444', '广东省-云浮市-郁南县', 1);
INSERT INTO `qmgx_region` VALUES (2445, '445323', '云安县', 252, 3, 0, 'Yun,an Xian', 'YUA', '1-20-252-2445', '广东省-云浮市-云安县', 1);
INSERT INTO `qmgx_region` VALUES (2446, '445381', '罗定市', 252, 3, 0, 'Luoding Shi', 'LUO', '1-20-252-2446', '广东省-云浮市-罗定市', 1);
INSERT INTO `qmgx_region` VALUES (2447, '450101', '市辖区', 253, 3, 0, 'Shixiaqu', '2', '1-21-253-2447', '广西壮族自治区-南宁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2448, '450102', '兴宁区', 253, 3, 0, 'Xingning Qu', 'XNE', '1-21-253-2448', '广西壮族自治区-南宁市-兴宁区', 1);
INSERT INTO `qmgx_region` VALUES (2449, '450103', '青秀区', 253, 3, 0, 'Qingxiu Qu', '2', '1-21-253-2449', '广西壮族自治区-南宁市-青秀区', 1);
INSERT INTO `qmgx_region` VALUES (2450, '450105', '江南区', 253, 3, 0, 'Jiangnan Qu', 'JNA', '1-21-253-2450', '广西壮族自治区-南宁市-江南区', 1);
INSERT INTO `qmgx_region` VALUES (2451, '450107', '西乡塘区', 253, 3, 0, 'Xixiangtang Qu', '2', '1-21-253-2451', '广西壮族自治区-南宁市-西乡塘区', 1);
INSERT INTO `qmgx_region` VALUES (2452, '450108', '良庆区', 253, 3, 0, 'Liangqing Qu', '2', '1-21-253-2452', '广西壮族自治区-南宁市-良庆区', 1);
INSERT INTO `qmgx_region` VALUES (2453, '450109', '邕宁区', 253, 3, 0, 'Yongning Qu', '2', '1-21-253-2453', '广西壮族自治区-南宁市-邕宁区', 1);
INSERT INTO `qmgx_region` VALUES (2454, '450122', '武鸣县', 253, 3, 0, 'Wuming Xian', 'WMG', '1-21-253-2454', '广西壮族自治区-南宁市-武鸣县', 1);
INSERT INTO `qmgx_region` VALUES (2455, '450123', '隆安县', 253, 3, 0, 'Long,an Xian', '2', '1-21-253-2455', '广西壮族自治区-南宁市-隆安县', 1);
INSERT INTO `qmgx_region` VALUES (2456, '450124', '马山县', 253, 3, 0, 'Mashan Xian', '2', '1-21-253-2456', '广西壮族自治区-南宁市-马山县', 1);
INSERT INTO `qmgx_region` VALUES (2457, '450125', '上林县', 253, 3, 0, 'Shanglin Xian', '2', '1-21-253-2457', '广西壮族自治区-南宁市-上林县', 1);
INSERT INTO `qmgx_region` VALUES (2458, '450126', '宾阳县', 253, 3, 0, 'Binyang Xian', '2', '1-21-253-2458', '广西壮族自治区-南宁市-宾阳县', 1);
INSERT INTO `qmgx_region` VALUES (2459, '450127', '横县', 253, 3, 0, 'Heng Xian', '2', '1-21-253-2459', '广西壮族自治区-南宁市-横县', 1);
INSERT INTO `qmgx_region` VALUES (2460, '450201', '市辖区', 254, 3, 0, 'Shixiaqu', '2', '1-21-254-2460', '广西壮族自治区-柳州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2461, '450202', '城中区', 254, 3, 0, 'Chengzhong Qu', 'CZG', '1-21-254-2461', '广西壮族自治区-柳州市-城中区', 1);
INSERT INTO `qmgx_region` VALUES (2462, '450203', '鱼峰区', 254, 3, 0, 'Yufeng Qu', 'YFQ', '1-21-254-2462', '广西壮族自治区-柳州市-鱼峰区', 1);
INSERT INTO `qmgx_region` VALUES (2463, '450204', '柳南区', 254, 3, 0, 'Liunan Qu', 'LNU', '1-21-254-2463', '广西壮族自治区-柳州市-柳南区', 1);
INSERT INTO `qmgx_region` VALUES (2464, '450205', '柳北区', 254, 3, 0, 'Liubei Qu', 'LBE', '1-21-254-2464', '广西壮族自治区-柳州市-柳北区', 1);
INSERT INTO `qmgx_region` VALUES (2465, '450221', '柳江县', 254, 3, 0, 'Liujiang Xian', 'LUJ', '1-21-254-2465', '广西壮族自治区-柳州市-柳江县', 1);
INSERT INTO `qmgx_region` VALUES (2466, '450222', '柳城县', 254, 3, 0, 'Liucheng Xian', 'LCB', '1-21-254-2466', '广西壮族自治区-柳州市-柳城县', 1);
INSERT INTO `qmgx_region` VALUES (2467, '450223', '鹿寨县', 254, 3, 0, 'Luzhai Xian', '2', '1-21-254-2467', '广西壮族自治区-柳州市-鹿寨县', 1);
INSERT INTO `qmgx_region` VALUES (2468, '450224', '融安县', 254, 3, 0, 'Rong,an Xian', '2', '1-21-254-2468', '广西壮族自治区-柳州市-融安县', 1);
INSERT INTO `qmgx_region` VALUES (2469, '450225', '融水苗族自治县', 254, 3, 0, 'Rongshui Miaozu Zizhixian', '2', '1-21-254-2469', '广西壮族自治区-柳州市-融水苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2470, '450226', '三江侗族自治县', 254, 3, 0, 'Sanjiang Dongzu Zizhixian', '2', '1-21-254-2470', '广西壮族自治区-柳州市-三江侗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2471, '450301', '市辖区', 255, 3, 0, 'Shixiaqu', '2', '1-21-255-2471', '广西壮族自治区-桂林市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2472, '450302', '秀峰区', 255, 3, 0, 'Xiufeng Qu', 'XUF', '1-21-255-2472', '广西壮族自治区-桂林市-秀峰区', 1);
INSERT INTO `qmgx_region` VALUES (2473, '450303', '叠彩区', 255, 3, 0, 'Diecai Qu', 'DCA', '1-21-255-2473', '广西壮族自治区-桂林市-叠彩区', 1);
INSERT INTO `qmgx_region` VALUES (2474, '450304', '象山区', 255, 3, 0, 'Xiangshan Qu', 'XSK', '1-21-255-2474', '广西壮族自治区-桂林市-象山区', 1);
INSERT INTO `qmgx_region` VALUES (2475, '450305', '七星区', 255, 3, 0, 'Qixing Qu', 'QXG', '1-21-255-2475', '广西壮族自治区-桂林市-七星区', 1);
INSERT INTO `qmgx_region` VALUES (2476, '450311', '雁山区', 255, 3, 0, 'Yanshan Qu', 'YSA', '1-21-255-2476', '广西壮族自治区-桂林市-雁山区', 1);
INSERT INTO `qmgx_region` VALUES (2477, '450321', '阳朔县', 255, 3, 0, 'Yangshuo Xian', 'YSO', '1-21-255-2477', '广西壮族自治区-桂林市-阳朔县', 1);
INSERT INTO `qmgx_region` VALUES (2478, '450322', '临桂县', 255, 3, 0, 'Lingui Xian', 'LGI', '1-21-255-2478', '广西壮族自治区-桂林市-临桂县', 1);
INSERT INTO `qmgx_region` VALUES (2479, '450323', '灵川县', 255, 3, 0, 'Lingchuan Xian', 'LCU', '1-21-255-2479', '广西壮族自治区-桂林市-灵川县', 1);
INSERT INTO `qmgx_region` VALUES (2480, '450324', '全州县', 255, 3, 0, 'Quanzhou Xian', 'QZO', '1-21-255-2480', '广西壮族自治区-桂林市-全州县', 1);
INSERT INTO `qmgx_region` VALUES (2481, '450325', '兴安县', 255, 3, 0, 'Xing,an Xian', 'XAG', '1-21-255-2481', '广西壮族自治区-桂林市-兴安县', 1);
INSERT INTO `qmgx_region` VALUES (2482, '450326', '永福县', 255, 3, 0, 'Yongfu Xian', 'YFU', '1-21-255-2482', '广西壮族自治区-桂林市-永福县', 1);
INSERT INTO `qmgx_region` VALUES (2483, '450327', '灌阳县', 255, 3, 0, 'Guanyang Xian', 'GNY', '1-21-255-2483', '广西壮族自治区-桂林市-灌阳县', 1);
INSERT INTO `qmgx_region` VALUES (2484, '450328', '龙胜各族自治县', 255, 3, 0, 'Longsheng Gezu Zizhixian', 'LSG', '1-21-255-2484', '广西壮族自治区-桂林市-龙胜各族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2485, '450329', '资源县', 255, 3, 0, 'Ziyuan Xian', 'ZYU', '1-21-255-2485', '广西壮族自治区-桂林市-资源县', 1);
INSERT INTO `qmgx_region` VALUES (2486, '450330', '平乐县', 255, 3, 0, 'Pingle Xian', 'PLE', '1-21-255-2486', '广西壮族自治区-桂林市-平乐县', 1);
INSERT INTO `qmgx_region` VALUES (2487, '450331', '荔蒲县', 255, 3, 0, 'Lipu Xian', '2', '1-21-255-2487', '广西壮族自治区-桂林市-荔蒲县', 1);
INSERT INTO `qmgx_region` VALUES (2488, '450332', '恭城瑶族自治县', 255, 3, 0, 'Gongcheng Yaozu Zizhixian', 'GGC', '1-21-255-2488', '广西壮族自治区-桂林市-恭城瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2489, '450401', '市辖区', 256, 3, 0, 'Shixiaqu', '2', '1-21-256-2489', '广西壮族自治区-梧州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2490, '450403', '万秀区', 256, 3, 0, 'Wanxiu Qu', 'WXQ', '1-21-256-2490', '广西壮族自治区-梧州市-万秀区', 1);
INSERT INTO `qmgx_region` VALUES (2491, '450404', '蝶山区', 256, 3, 0, 'Dieshan Qu', 'DES', '1-21-256-2491', '广西壮族自治区-梧州市-蝶山区', 1);
INSERT INTO `qmgx_region` VALUES (2492, '450405', '长洲区', 256, 3, 0, 'Changzhou Qu', '2', '1-21-256-2492', '广西壮族自治区-梧州市-长洲区', 1);
INSERT INTO `qmgx_region` VALUES (2493, '450421', '苍梧县', 256, 3, 0, 'Cangwu Xian', 'CAW', '1-21-256-2493', '广西壮族自治区-梧州市-苍梧县', 1);
INSERT INTO `qmgx_region` VALUES (2494, '450422', '藤县', 256, 3, 0, 'Teng Xian', '2', '1-21-256-2494', '广西壮族自治区-梧州市-藤县', 1);
INSERT INTO `qmgx_region` VALUES (2495, '450423', '蒙山县', 256, 3, 0, 'Mengshan Xian', 'MSA', '1-21-256-2495', '广西壮族自治区-梧州市-蒙山县', 1);
INSERT INTO `qmgx_region` VALUES (2496, '450481', '岑溪市', 256, 3, 0, 'Cenxi Shi', 'CEX', '1-21-256-2496', '广西壮族自治区-梧州市-岑溪市', 1);
INSERT INTO `qmgx_region` VALUES (2497, '450501', '市辖区', 257, 3, 0, 'Shixiaqu', '2', '1-21-257-2497', '广西壮族自治区-北海市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2498, '450502', '海城区', 257, 3, 0, 'Haicheng Qu', 'HCB', '1-21-257-2498', '广西壮族自治区-北海市-海城区', 1);
INSERT INTO `qmgx_region` VALUES (2499, '450503', '银海区', 257, 3, 0, 'Yinhai Qu', 'YHB', '1-21-257-2499', '广西壮族自治区-北海市-银海区', 1);
INSERT INTO `qmgx_region` VALUES (2500, '450512', '铁山港区', 257, 3, 0, 'Tieshangangqu ', 'TSG', '1-21-257-2500', '广西壮族自治区-北海市-铁山港区', 1);
INSERT INTO `qmgx_region` VALUES (2501, '450521', '合浦县', 257, 3, 0, 'Hepu Xian', 'HPX', '1-21-257-2501', '广西壮族自治区-北海市-合浦县', 1);
INSERT INTO `qmgx_region` VALUES (2502, '450601', '市辖区', 258, 3, 0, 'Shixiaqu', '2', '1-21-258-2502', '广西壮族自治区-防城港市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2503, '450602', '港口区', 258, 3, 0, 'Gangkou Qu', 'GKQ', '1-21-258-2503', '广西壮族自治区-防城港市-港口区', 1);
INSERT INTO `qmgx_region` VALUES (2504, '450603', '防城区', 258, 3, 0, 'Fangcheng Qu', 'FCQ', '1-21-258-2504', '广西壮族自治区-防城港市-防城区', 1);
INSERT INTO `qmgx_region` VALUES (2505, '450621', '上思县', 258, 3, 0, 'Shangsi Xian', 'SGS', '1-21-258-2505', '广西壮族自治区-防城港市-上思县', 1);
INSERT INTO `qmgx_region` VALUES (2506, '450681', '东兴市', 258, 3, 0, 'Dongxing Shi', 'DOX', '1-21-258-2506', '广西壮族自治区-防城港市-东兴市', 1);
INSERT INTO `qmgx_region` VALUES (2507, '450701', '市辖区', 259, 3, 0, 'Shixiaqu', '2', '1-21-259-2507', '广西壮族自治区-钦州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2508, '450702', '钦南区', 259, 3, 0, 'Qinnan Qu', 'QNQ', '1-21-259-2508', '广西壮族自治区-钦州市-钦南区', 1);
INSERT INTO `qmgx_region` VALUES (2509, '450703', '钦北区', 259, 3, 0, 'Qinbei Qu', 'QBQ', '1-21-259-2509', '广西壮族自治区-钦州市-钦北区', 1);
INSERT INTO `qmgx_region` VALUES (2510, '450721', '灵山县', 259, 3, 0, 'Lingshan Xian', 'LSB', '1-21-259-2510', '广西壮族自治区-钦州市-灵山县', 1);
INSERT INTO `qmgx_region` VALUES (2511, '450722', '浦北县', 259, 3, 0, 'Pubei Xian', 'PBE', '1-21-259-2511', '广西壮族自治区-钦州市-浦北县', 1);
INSERT INTO `qmgx_region` VALUES (2512, '450801', '市辖区', 260, 3, 0, 'Shixiaqu', '2', '1-21-260-2512', '广西壮族自治区-贵港市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2513, '450802', '港北区', 260, 3, 0, 'Gangbei Qu', 'GBE', '1-21-260-2513', '广西壮族自治区-贵港市-港北区', 1);
INSERT INTO `qmgx_region` VALUES (2514, '450803', '港南区', 260, 3, 0, 'Gangnan Qu', 'GNQ', '1-21-260-2514', '广西壮族自治区-贵港市-港南区', 1);
INSERT INTO `qmgx_region` VALUES (2515, '450804', '覃塘区', 260, 3, 0, 'Tantang Qu', '2', '1-21-260-2515', '广西壮族自治区-贵港市-覃塘区', 1);
INSERT INTO `qmgx_region` VALUES (2516, '450821', '平南县', 260, 3, 0, 'Pingnan Xian', 'PNN', '1-21-260-2516', '广西壮族自治区-贵港市-平南县', 1);
INSERT INTO `qmgx_region` VALUES (2517, '450881', '桂平市', 260, 3, 0, 'Guiping Shi', 'GPS', '1-21-260-2517', '广西壮族自治区-贵港市-桂平市', 1);
INSERT INTO `qmgx_region` VALUES (2518, '450901', '市辖区', 261, 3, 0, 'Shixiaqu', '2', '1-21-261-2518', '广西壮族自治区-玉林市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2519, '450902', '玉州区', 261, 3, 0, 'Yuzhou Qu', 'YZO', '1-21-261-2519', '广西壮族自治区-玉林市-玉州区', 1);
INSERT INTO `qmgx_region` VALUES (2520, '450921', '容县', 261, 3, 0, 'Rong Xian', 'ROG', '1-21-261-2520', '广西壮族自治区-玉林市-容县', 1);
INSERT INTO `qmgx_region` VALUES (2521, '450922', '陆川县', 261, 3, 0, 'Luchuan Xian', 'LCJ', '1-21-261-2521', '广西壮族自治区-玉林市-陆川县', 1);
INSERT INTO `qmgx_region` VALUES (2522, '450923', '博白县', 261, 3, 0, 'Bobai Xian', 'BBA', '1-21-261-2522', '广西壮族自治区-玉林市-博白县', 1);
INSERT INTO `qmgx_region` VALUES (2523, '450924', '兴业县', 261, 3, 0, 'Xingye Xian', 'XGY', '1-21-261-2523', '广西壮族自治区-玉林市-兴业县', 1);
INSERT INTO `qmgx_region` VALUES (2524, '450981', '北流市', 261, 3, 0, 'Beiliu Shi', 'BLS', '1-21-261-2524', '广西壮族自治区-玉林市-北流市', 1);
INSERT INTO `qmgx_region` VALUES (2525, '451001', '市辖区', 262, 3, 0, '1', '2', '1-21-262-2525', '广西壮族自治区-百色市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2526, '451002', '右江区', 262, 3, 0, 'Youjiang Qu', '2', '1-21-262-2526', '广西壮族自治区-百色市-右江区', 1);
INSERT INTO `qmgx_region` VALUES (2527, '451021', '田阳县', 262, 3, 0, 'Tianyang Xian', '2', '1-21-262-2527', '广西壮族自治区-百色市-田阳县', 1);
INSERT INTO `qmgx_region` VALUES (2528, '451022', '田东县', 262, 3, 0, 'Tiandong Xian', '2', '1-21-262-2528', '广西壮族自治区-百色市-田东县', 1);
INSERT INTO `qmgx_region` VALUES (2529, '451023', '平果县', 262, 3, 0, 'Pingguo Xian', '2', '1-21-262-2529', '广西壮族自治区-百色市-平果县', 1);
INSERT INTO `qmgx_region` VALUES (2530, '451024', '德保县', 262, 3, 0, 'Debao Xian', '2', '1-21-262-2530', '广西壮族自治区-百色市-德保县', 1);
INSERT INTO `qmgx_region` VALUES (2531, '451025', '靖西县', 262, 3, 0, 'Jingxi Xian', '2', '1-21-262-2531', '广西壮族自治区-百色市-靖西县', 1);
INSERT INTO `qmgx_region` VALUES (2532, '451026', '那坡县', 262, 3, 0, 'Napo Xian', '2', '1-21-262-2532', '广西壮族自治区-百色市-那坡县', 1);
INSERT INTO `qmgx_region` VALUES (2533, '451027', '凌云县', 262, 3, 0, 'Lingyun Xian', '2', '1-21-262-2533', '广西壮族自治区-百色市-凌云县', 1);
INSERT INTO `qmgx_region` VALUES (2534, '451028', '乐业县', 262, 3, 0, 'Leye Xian', '2', '1-21-262-2534', '广西壮族自治区-百色市-乐业县', 1);
INSERT INTO `qmgx_region` VALUES (2535, '451029', '田林县', 262, 3, 0, 'Tianlin Xian', '2', '1-21-262-2535', '广西壮族自治区-百色市-田林县', 1);
INSERT INTO `qmgx_region` VALUES (2536, '451030', '西林县', 262, 3, 0, 'Xilin Xian', '2', '1-21-262-2536', '广西壮族自治区-百色市-西林县', 1);
INSERT INTO `qmgx_region` VALUES (2537, '451031', '隆林各族自治县', 262, 3, 0, 'Longlin Gezu Zizhixian', '2', '1-21-262-2537', '广西壮族自治区-百色市-隆林各族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2538, '451101', '市辖区', 263, 3, 0, '1', '2', '1-21-263-2538', '广西壮族自治区-贺州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2539, '451102', '八步区', 263, 3, 0, 'Babu Qu', '2', '1-21-263-2539', '广西壮族自治区-贺州市-八步区', 1);
INSERT INTO `qmgx_region` VALUES (2540, '451121', '昭平县', 263, 3, 0, 'Zhaoping Xian', '2', '1-21-263-2540', '广西壮族自治区-贺州市-昭平县', 1);
INSERT INTO `qmgx_region` VALUES (2541, '451122', '钟山县', 263, 3, 0, 'Zhongshan Xian', '2', '1-21-263-2541', '广西壮族自治区-贺州市-钟山县', 1);
INSERT INTO `qmgx_region` VALUES (2542, '451123', '富川瑶族自治县', 263, 3, 0, 'Fuchuan Yaozu Zizhixian', '2', '1-21-263-2542', '广西壮族自治区-贺州市-富川瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2543, '451201', '市辖区', 264, 3, 0, '1', '2', '1-21-264-2543', '广西壮族自治区-河池市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2544, '451202', '金城江区', 264, 3, 0, 'Jinchengjiang Qu', '2', '1-21-264-2544', '广西壮族自治区-河池市-金城江区', 1);
INSERT INTO `qmgx_region` VALUES (2545, '451221', '南丹县', 264, 3, 0, 'Nandan Xian', '2', '1-21-264-2545', '广西壮族自治区-河池市-南丹县', 1);
INSERT INTO `qmgx_region` VALUES (2546, '451222', '天峨县', 264, 3, 0, 'Tian,e Xian', '2', '1-21-264-2546', '广西壮族自治区-河池市-天峨县', 1);
INSERT INTO `qmgx_region` VALUES (2547, '451223', '凤山县', 264, 3, 0, 'Fengshan Xian', '2', '1-21-264-2547', '广西壮族自治区-河池市-凤山县', 1);
INSERT INTO `qmgx_region` VALUES (2548, '451224', '东兰县', 264, 3, 0, 'Donglan Xian', '2', '1-21-264-2548', '广西壮族自治区-河池市-东兰县', 1);
INSERT INTO `qmgx_region` VALUES (2549, '451225', '罗城仫佬族自治县', 264, 3, 0, 'Luocheng Mulaozu Zizhixian', '2', '1-21-264-2549', '广西壮族自治区-河池市-罗城仫佬族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2550, '451226', '环江毛南族自治县', 264, 3, 0, 'Huanjiang Maonanzu Zizhixian', '2', '1-21-264-2550', '广西壮族自治区-河池市-环江毛南族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2551, '451227', '巴马瑶族自治县', 264, 3, 0, 'Bama Yaozu Zizhixian', '2', '1-21-264-2551', '广西壮族自治区-河池市-巴马瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2552, '451228', '都安瑶族自治县', 264, 3, 0, 'Du,an Yaozu Zizhixian', '2', '1-21-264-2552', '广西壮族自治区-河池市-都安瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2553, '451229', '大化瑶族自治县', 264, 3, 0, 'Dahua Yaozu Zizhixian', '2', '1-21-264-2553', '广西壮族自治区-河池市-大化瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2554, '451281', '宜州市', 264, 3, 0, 'Yizhou Shi', '2', '1-21-264-2554', '广西壮族自治区-河池市-宜州市', 1);
INSERT INTO `qmgx_region` VALUES (2555, '451301', '市辖区', 265, 3, 0, '1', '2', '1-21-265-2555', '广西壮族自治区-来宾市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2556, '451302', '兴宾区', 265, 3, 0, 'Xingbin Qu', '2', '1-21-265-2556', '广西壮族自治区-来宾市-兴宾区', 1);
INSERT INTO `qmgx_region` VALUES (2557, '451321', '忻城县', 265, 3, 0, 'Xincheng Xian', '2', '1-21-265-2557', '广西壮族自治区-来宾市-忻城县', 1);
INSERT INTO `qmgx_region` VALUES (2558, '451322', '象州县', 265, 3, 0, 'Xiangzhou Xian', '2', '1-21-265-2558', '广西壮族自治区-来宾市-象州县', 1);
INSERT INTO `qmgx_region` VALUES (2559, '451323', '武宣县', 265, 3, 0, 'Wuxuan Xian', '2', '1-21-265-2559', '广西壮族自治区-来宾市-武宣县', 1);
INSERT INTO `qmgx_region` VALUES (2560, '451324', '金秀瑶族自治县', 265, 3, 0, 'Jinxiu Yaozu Zizhixian', '2', '1-21-265-2560', '广西壮族自治区-来宾市-金秀瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2561, '451381', '合山市', 265, 3, 0, 'Heshan Shi', '2', '1-21-265-2561', '广西壮族自治区-来宾市-合山市', 1);
INSERT INTO `qmgx_region` VALUES (2562, '451401', '市辖区', 266, 3, 0, '1', '2', '1-21-266-2562', '广西壮族自治区-崇左市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2563, '451402', '江洲区', 266, 3, 0, 'Jiangzhou Qu', '2', '1-21-266-2563', '广西壮族自治区-崇左市-江洲区', 1);
INSERT INTO `qmgx_region` VALUES (2564, '451421', '扶绥县', 266, 3, 0, 'Fusui Xian', '2', '1-21-266-2564', '广西壮族自治区-崇左市-扶绥县', 1);
INSERT INTO `qmgx_region` VALUES (2565, '451422', '宁明县', 266, 3, 0, 'Ningming Xian', '2', '1-21-266-2565', '广西壮族自治区-崇左市-宁明县', 1);
INSERT INTO `qmgx_region` VALUES (2566, '451423', '龙州县', 266, 3, 0, 'Longzhou Xian', '2', '1-21-266-2566', '广西壮族自治区-崇左市-龙州县', 1);
INSERT INTO `qmgx_region` VALUES (2567, '451424', '大新县', 266, 3, 0, 'Daxin Xian', '2', '1-21-266-2567', '广西壮族自治区-崇左市-大新县', 1);
INSERT INTO `qmgx_region` VALUES (2568, '451425', '天等县', 266, 3, 0, 'Tiandeng Xian', '2', '1-21-266-2568', '广西壮族自治区-崇左市-天等县', 1);
INSERT INTO `qmgx_region` VALUES (2569, '451481', '凭祥市', 266, 3, 0, 'Pingxiang Shi', '2', '1-21-266-2569', '广西壮族自治区-崇左市-凭祥市', 1);
INSERT INTO `qmgx_region` VALUES (2570, '460101', '市辖区', 267, 3, 0, 'Shixiaqu', '2', '1-22-267-2570', '海南省-海口市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2571, '460105', '秀英区', 267, 3, 0, 'Xiuying Qu', 'XYH', '1-22-267-2571', '海南省-海口市-秀英区', 1);
INSERT INTO `qmgx_region` VALUES (2572, '460106', '龙华区', 267, 3, 0, 'LongHua Qu', 'LH', '1-22-267-2572', '海南省-海口市-龙华区', 1);
INSERT INTO `qmgx_region` VALUES (2573, '460107', '琼山区', 267, 3, 0, 'QiongShan Qu', 'QS', '1-22-267-2573', '海南省-海口市-琼山区', 1);
INSERT INTO `qmgx_region` VALUES (2574, '460108', '美兰区', 267, 3, 0, 'MeiLan Qu', 'ML', '1-22-267-2574', '海南省-海口市-美兰区', 1);
INSERT INTO `qmgx_region` VALUES (2575, '460201', '市辖区', 268, 3, 0, 'Shixiaqu', '2', '1-22-268-2575', '海南省-三亚市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2576, '469001', '五指山市', 269, 3, 0, 'Wuzhishan Qu', '2', '1-22-269-2576', '海南省-省直辖县级行政区划-五指山市', 1);
INSERT INTO `qmgx_region` VALUES (2577, '469002', '琼海市', 269, 3, 0, 'Qionghai Shi', '2', '1-22-269-2577', '海南省-省直辖县级行政区划-琼海市', 1);
INSERT INTO `qmgx_region` VALUES (2578, '469003', '儋州市', 269, 3, 0, 'Danzhou Shi', '2', '1-22-269-2578', '海南省-省直辖县级行政区划-儋州市', 1);
INSERT INTO `qmgx_region` VALUES (2579, '469005', '文昌市', 269, 3, 0, 'Wenchang Shi', '2', '1-22-269-2579', '海南省-省直辖县级行政区划-文昌市', 1);
INSERT INTO `qmgx_region` VALUES (2580, '469006', '万宁市', 269, 3, 0, 'Wanning Shi', '2', '1-22-269-2580', '海南省-省直辖县级行政区划-万宁市', 1);
INSERT INTO `qmgx_region` VALUES (2581, '469007', '东方市', 269, 3, 0, 'Dongfang Shi', '2', '1-22-269-2581', '海南省-省直辖县级行政区划-东方市', 1);
INSERT INTO `qmgx_region` VALUES (2582, '469021', '定安县', 269, 3, 0, 'Ding,an Xian', '2', '1-22-269-2582', '海南省-省直辖县级行政区划-定安县', 1);
INSERT INTO `qmgx_region` VALUES (2583, '469022', '屯昌县', 269, 3, 0, 'Tunchang Xian', '2', '1-22-269-2583', '海南省-省直辖县级行政区划-屯昌县', 1);
INSERT INTO `qmgx_region` VALUES (2584, '469023', '澄迈县', 269, 3, 0, 'Chengmai Xian', '2', '1-22-269-2584', '海南省-省直辖县级行政区划-澄迈县', 1);
INSERT INTO `qmgx_region` VALUES (2585, '469024', '临高县', 269, 3, 0, 'Lingao Xian', '2', '1-22-269-2585', '海南省-省直辖县级行政区划-临高县', 1);
INSERT INTO `qmgx_region` VALUES (2586, '469025', '白沙黎族自治县', 269, 3, 0, 'Baisha Lizu Zizhixian', '2', '1-22-269-2586', '海南省-省直辖县级行政区划-白沙黎族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2587, '469026', '昌江黎族自治县', 269, 3, 0, 'Changjiang Lizu Zizhixian', '2', '1-22-269-2587', '海南省-省直辖县级行政区划-昌江黎族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2588, '469027', '乐东黎族自治县', 269, 3, 0, 'Ledong Lizu Zizhixian', '2', '1-22-269-2588', '海南省-省直辖县级行政区划-乐东黎族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2589, '469028', '陵水黎族自治县', 269, 3, 0, 'Lingshui Lizu Zizhixian', '2', '1-22-269-2589', '海南省-省直辖县级行政区划-陵水黎族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2590, '469029', '保亭黎族苗族自治县', 269, 3, 0, 'Baoting Lizu Miaozu Zizhixian', '2', '1-22-269-2590', '海南省-省直辖县级行政区划-保亭黎族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2591, '469030', '琼中黎族苗族自治县', 269, 3, 0, 'Qiongzhong Lizu Miaozu Zizhixian', '2', '1-22-269-2591', '海南省-省直辖县级行政区划-琼中黎族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2592, '469031', '西沙群岛', 269, 3, 0, 'Xisha Qundao', '2', '1-22-269-2592', '海南省-省直辖县级行政区划-西沙群岛', 1);
INSERT INTO `qmgx_region` VALUES (2593, '469032', '南沙群岛', 269, 3, 0, 'Nansha Qundao', '2', '1-22-269-2593', '海南省-省直辖县级行政区划-南沙群岛', 1);
INSERT INTO `qmgx_region` VALUES (2594, '469033', '中沙群岛的岛礁及其海域', 269, 3, 0, 'Zhongsha Qundao de Daojiao Jiqi Haiyu', '2', '1-22-269-2594', '海南省-省直辖县级行政区划-中沙群岛的岛礁及其海域', 1);
INSERT INTO `qmgx_region` VALUES (2595, '500101', '万州区', 270, 3, 0, 'Wanzhou Qu', 'WZO ', '1-23-270-2595', '重庆市-市辖区-万州区', 1);
INSERT INTO `qmgx_region` VALUES (2596, '500102', '涪陵区', 270, 3, 0, 'Fuling Qu', 'FLG', '1-23-270-2596', '重庆市-市辖区-涪陵区', 1);
INSERT INTO `qmgx_region` VALUES (2597, '500103', '渝中区', 270, 3, 0, 'Yuzhong Qu', 'YZQ', '1-23-270-2597', '重庆市-市辖区-渝中区', 1);
INSERT INTO `qmgx_region` VALUES (2598, '500104', '大渡口区', 270, 3, 0, 'Dadukou Qu', 'DDK', '1-23-270-2598', '重庆市-市辖区-大渡口区', 1);
INSERT INTO `qmgx_region` VALUES (2599, '500105', '江北区', 270, 3, 0, 'Jiangbei Qu', 'JBE', '1-23-270-2599', '重庆市-市辖区-江北区', 1);
INSERT INTO `qmgx_region` VALUES (2600, '500106', '沙坪坝区', 270, 3, 0, 'Shapingba Qu', 'SPB', '1-23-270-2600', '重庆市-市辖区-沙坪坝区', 1);
INSERT INTO `qmgx_region` VALUES (2601, '500107', '九龙坡区', 270, 3, 0, 'Jiulongpo Qu', 'JLP', '1-23-270-2601', '重庆市-市辖区-九龙坡区', 1);
INSERT INTO `qmgx_region` VALUES (2602, '500108', '南岸区', 270, 3, 0, 'Nan,an Qu', 'NAQ', '1-23-270-2602', '重庆市-市辖区-南岸区', 1);
INSERT INTO `qmgx_region` VALUES (2603, '500109', '北碚区', 270, 3, 0, 'Beibei Qu', 'BBE', '1-23-270-2603', '重庆市-市辖区-北碚区', 1);
INSERT INTO `qmgx_region` VALUES (2604, '500110', '万盛区', 270, 3, 0, 'Wansheng Qu', 'WSQ', '1-23-270-2604', '重庆市-市辖区-万盛区', 1);
INSERT INTO `qmgx_region` VALUES (2605, '500111', '双桥区', 270, 3, 0, 'Shuangqiao Qu', 'SQQ', '1-23-270-2605', '重庆市-市辖区-双桥区', 1);
INSERT INTO `qmgx_region` VALUES (2606, '500112', '渝北区', 270, 3, 0, 'Yubei Qu', 'YBE', '1-23-270-2606', '重庆市-市辖区-渝北区', 1);
INSERT INTO `qmgx_region` VALUES (2607, '500113', '巴南区', 270, 3, 0, 'Banan Qu', 'BNN', '1-23-270-2607', '重庆市-市辖区-巴南区', 1);
INSERT INTO `qmgx_region` VALUES (2608, '500114', '黔江区', 270, 3, 0, 'Qianjiang Qu', '2', '1-23-270-2608', '重庆市-市辖区-黔江区', 1);
INSERT INTO `qmgx_region` VALUES (2609, '500115', '长寿区', 270, 3, 0, 'Changshou Qu', '2', '1-23-270-2609', '重庆市-市辖区-长寿区', 1);
INSERT INTO `qmgx_region` VALUES (2610, '500222', '綦江县', 270, 3, 0, 'Qijiang Xian', 'QJG', '1-23-270-2610', '重庆市-县-綦江县', 1);
INSERT INTO `qmgx_region` VALUES (2611, '500223', '潼南县', 270, 3, 0, 'Tongnan Xian', 'TNN', '1-23-270-2611', '重庆市-县-潼南县', 1);
INSERT INTO `qmgx_region` VALUES (2612, '500224', '铜梁县', 270, 3, 0, 'Tongliang Xian', 'TGL', '1-23-270-2612', '重庆市-县-铜梁县', 1);
INSERT INTO `qmgx_region` VALUES (2613, '500225', '大足县', 270, 3, 0, 'Dazu Xian', 'DZX', '1-23-270-2613', '重庆市-县-大足县', 1);
INSERT INTO `qmgx_region` VALUES (2614, '500226', '荣昌县', 270, 3, 0, 'Rongchang Xian', 'RGC', '1-23-270-2614', '重庆市-县-荣昌县', 1);
INSERT INTO `qmgx_region` VALUES (2615, '500227', '璧山县', 270, 3, 0, 'Bishan Xian', 'BSY', '1-23-270-2615', '重庆市-县-璧山县', 1);
INSERT INTO `qmgx_region` VALUES (2616, '500228', '梁平县', 270, 3, 0, 'Liangping Xian', 'LGP', '1-23-270-2616', '重庆市-县-梁平县', 1);
INSERT INTO `qmgx_region` VALUES (2617, '500229', '城口县', 270, 3, 0, 'Chengkou Xian', 'CKO', '1-23-270-2617', '重庆市-县-城口县', 1);
INSERT INTO `qmgx_region` VALUES (2618, '500230', '丰都县', 270, 3, 0, 'Fengdu Xian', 'FDU', '1-23-270-2618', '重庆市-县-丰都县', 1);
INSERT INTO `qmgx_region` VALUES (2619, '500231', '垫江县', 270, 3, 0, 'Dianjiang Xian', 'DJG', '1-23-270-2619', '重庆市-县-垫江县', 1);
INSERT INTO `qmgx_region` VALUES (2620, '500232', '武隆县', 270, 3, 0, 'Wulong Xian', 'WLG', '1-23-270-2620', '重庆市-县-武隆县', 1);
INSERT INTO `qmgx_region` VALUES (2621, '500233', '忠县', 270, 3, 0, 'Zhong Xian', 'ZHX', '1-23-270-2621', '重庆市-县-忠县', 1);
INSERT INTO `qmgx_region` VALUES (2622, '500234', '开县', 270, 3, 0, 'Kai Xian', 'KAI', '1-23-270-2622', '重庆市-县-开县', 1);
INSERT INTO `qmgx_region` VALUES (2623, '500235', '云阳县', 270, 3, 0, 'Yunyang Xian', 'YNY', '1-23-270-2623', '重庆市-县-云阳县', 1);
INSERT INTO `qmgx_region` VALUES (2624, '500236', '奉节县', 270, 3, 0, 'Fengjie Xian', 'FJE', '1-23-270-2624', '重庆市-县-奉节县', 1);
INSERT INTO `qmgx_region` VALUES (2625, '500237', '巫山县', 270, 3, 0, 'Wushan Xian', 'WSN', '1-23-270-2625', '重庆市-县-巫山县', 1);
INSERT INTO `qmgx_region` VALUES (2626, '500238', '巫溪县', 270, 3, 0, 'Wuxi Xian', 'WXX', '1-23-270-2626', '重庆市-县-巫溪县', 1);
INSERT INTO `qmgx_region` VALUES (2627, '500240', '石柱土家族自治县', 270, 3, 0, 'Shizhu Tujiazu Zizhixian', 'SZY', '1-23-270-2627', '重庆市-县-石柱土家族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2628, '500241', '秀山土家族苗族自治县', 270, 3, 0, 'Xiushan Tujiazu Miaozu Zizhixian', 'XUS', '1-23-270-2628', '重庆市-县-秀山土家族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2629, '500242', '酉阳土家族苗族自治县', 270, 3, 0, 'Youyang Tujiazu Miaozu Zizhixian', 'YUY', '1-23-270-2629', '重庆市-县-酉阳土家族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2630, '500243', '彭水苗族土家族自治县', 270, 3, 0, 'Pengshui Miaozu Tujiazu Zizhixian', 'PSU', '1-23-270-2630', '重庆市-县-彭水苗族土家族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2631, '500116', '江津区', 272, 3, 0, 'Jiangjin Shi', '2', '', '', 1);
INSERT INTO `qmgx_region` VALUES (2632, '500117', '合川区', 272, 3, 0, 'Hechuan Shi', '2', '', '', 1);
INSERT INTO `qmgx_region` VALUES (2633, '500118', '永川区', 272, 3, 0, 'Yongchuan Shi', '2', '', '', 1);
INSERT INTO `qmgx_region` VALUES (2634, '500119', '南川区', 272, 3, 0, 'Nanchuan Shi', '2', '', '', 1);
INSERT INTO `qmgx_region` VALUES (2635, '510101', '市辖区', 273, 3, 0, 'Shixiaqu', '2', '1-24-273-2635', '四川省-成都市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2636, '510104', '锦江区', 273, 3, 0, 'Jinjiang Qu', 'JJQ', '1-24-273-2636', '四川省-成都市-锦江区', 1);
INSERT INTO `qmgx_region` VALUES (2637, '510105', '青羊区', 273, 3, 0, 'Qingyang Qu', 'QYQ', '1-24-273-2637', '四川省-成都市-青羊区', 1);
INSERT INTO `qmgx_region` VALUES (2638, '510106', '金牛区', 273, 3, 0, 'Jinniu Qu', 'JNU', '1-24-273-2638', '四川省-成都市-金牛区', 1);
INSERT INTO `qmgx_region` VALUES (2639, '510107', '武侯区', 273, 3, 0, 'Wuhou Qu', 'WHQ', '1-24-273-2639', '四川省-成都市-武侯区', 1);
INSERT INTO `qmgx_region` VALUES (2640, '510108', '成华区', 273, 3, 0, 'Chenghua Qu', 'CHQ', '1-24-273-2640', '四川省-成都市-成华区', 1);
INSERT INTO `qmgx_region` VALUES (2641, '510112', '龙泉驿区', 273, 3, 0, 'Longquanyi Qu', 'LQY', '1-24-273-2641', '四川省-成都市-龙泉驿区', 1);
INSERT INTO `qmgx_region` VALUES (2642, '510113', '青白江区', 273, 3, 0, 'Qingbaijiang Qu', 'QBJ', '1-24-273-2642', '四川省-成都市-青白江区', 1);
INSERT INTO `qmgx_region` VALUES (2643, '510114', '新都区', 273, 3, 0, 'Xindu Qu', '2', '1-24-273-2643', '四川省-成都市-新都区', 1);
INSERT INTO `qmgx_region` VALUES (2644, '510115', '温江区', 273, 3, 0, 'Wenjiang Qu', '2', '1-24-273-2644', '四川省-成都市-温江区', 1);
INSERT INTO `qmgx_region` VALUES (2645, '510121', '金堂县', 273, 3, 0, 'Jintang Xian', 'JNT', '1-24-273-2645', '四川省-成都市-金堂县', 1);
INSERT INTO `qmgx_region` VALUES (2646, '510122', '双流县', 273, 3, 0, 'Shuangliu Xian', 'SLU', '1-24-273-2646', '四川省-成都市-双流县', 1);
INSERT INTO `qmgx_region` VALUES (2647, '510124', '郫县', 273, 3, 0, 'Pi Xian', 'PIX', '1-24-273-2647', '四川省-成都市-郫县', 1);
INSERT INTO `qmgx_region` VALUES (2648, '510129', '大邑县', 273, 3, 0, 'Dayi Xian', 'DYI', '1-24-273-2648', '四川省-成都市-大邑县', 1);
INSERT INTO `qmgx_region` VALUES (2649, '510131', '蒲江县', 273, 3, 0, 'Pujiang Xian', 'PJX', '1-24-273-2649', '四川省-成都市-蒲江县', 1);
INSERT INTO `qmgx_region` VALUES (2650, '510132', '新津县', 273, 3, 0, 'Xinjin Xian', 'XJC', '1-24-273-2650', '四川省-成都市-新津县', 1);
INSERT INTO `qmgx_region` VALUES (2651, '510181', '都江堰市', 273, 3, 0, 'Dujiangyan Shi', 'DJY', '1-24-273-2651', '四川省-成都市-都江堰市', 1);
INSERT INTO `qmgx_region` VALUES (2652, '510182', '彭州市', 273, 3, 0, 'Pengzhou Shi', 'PZS', '1-24-273-2652', '四川省-成都市-彭州市', 1);
INSERT INTO `qmgx_region` VALUES (2653, '510183', '邛崃市', 273, 3, 0, 'Qionglai Shi', 'QLA', '1-24-273-2653', '四川省-成都市-邛崃市', 1);
INSERT INTO `qmgx_region` VALUES (2654, '510184', '崇州市', 273, 3, 0, 'Chongzhou Shi', 'CZO', '1-24-273-2654', '四川省-成都市-崇州市', 1);
INSERT INTO `qmgx_region` VALUES (2655, '510301', '市辖区', 274, 3, 0, 'Shixiaqu', '2', '1-24-274-2655', '四川省-自贡市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2656, '510302', '自流井区', 274, 3, 0, 'Ziliujing Qu', 'ZLJ', '1-24-274-2656', '四川省-自贡市-自流井区', 1);
INSERT INTO `qmgx_region` VALUES (2657, '510303', '贡井区', 274, 3, 0, 'Gongjing Qu', '2', '1-24-274-2657', '四川省-自贡市-贡井区', 1);
INSERT INTO `qmgx_region` VALUES (2658, '510304', '大安区', 274, 3, 0, 'Da,an Qu', 'DAQ', '1-24-274-2658', '四川省-自贡市-大安区', 1);
INSERT INTO `qmgx_region` VALUES (2659, '510311', '沿滩区', 274, 3, 0, 'Yantan Qu', 'YTN', '1-24-274-2659', '四川省-自贡市-沿滩区', 1);
INSERT INTO `qmgx_region` VALUES (2660, '510321', '荣县', 274, 3, 0, 'Rong Xian', 'RGX', '1-24-274-2660', '四川省-自贡市-荣县', 1);
INSERT INTO `qmgx_region` VALUES (2661, '510322', '富顺县', 274, 3, 0, 'Fushun Xian', 'FSH', '1-24-274-2661', '四川省-自贡市-富顺县', 1);
INSERT INTO `qmgx_region` VALUES (2662, '510401', '市辖区', 275, 3, 0, 'Shixiaqu', '2', '1-24-275-2662', '四川省-攀枝花市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2663, '510402', '东区', 275, 3, 0, 'Dong Qu', 'DQP', '1-24-275-2663', '四川省-攀枝花市-东区', 1);
INSERT INTO `qmgx_region` VALUES (2664, '510403', '西区', 275, 3, 0, 'Xi Qu', 'XIQ', '1-24-275-2664', '四川省-攀枝花市-西区', 1);
INSERT INTO `qmgx_region` VALUES (2665, '510411', '仁和区', 275, 3, 0, 'Renhe Qu', 'RHQ', '1-24-275-2665', '四川省-攀枝花市-仁和区', 1);
INSERT INTO `qmgx_region` VALUES (2666, '510421', '米易县', 275, 3, 0, 'Miyi Xian', 'MIY', '1-24-275-2666', '四川省-攀枝花市-米易县', 1);
INSERT INTO `qmgx_region` VALUES (2667, '510422', '盐边县', 275, 3, 0, 'Yanbian Xian', 'YBN', '1-24-275-2667', '四川省-攀枝花市-盐边县', 1);
INSERT INTO `qmgx_region` VALUES (2668, '510501', '市辖区', 276, 3, 0, 'Shixiaqu', '2', '1-24-276-2668', '四川省-泸州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2669, '510502', '江阳区', 276, 3, 0, 'Jiangyang Qu', 'JYB', '1-24-276-2669', '四川省-泸州市-江阳区', 1);
INSERT INTO `qmgx_region` VALUES (2670, '510503', '纳溪区', 276, 3, 0, 'Naxi Qu', 'NXI', '1-24-276-2670', '四川省-泸州市-纳溪区', 1);
INSERT INTO `qmgx_region` VALUES (2671, '510504', '龙马潭区', 276, 3, 0, 'Longmatan Qu', 'LMT', '1-24-276-2671', '四川省-泸州市-龙马潭区', 1);
INSERT INTO `qmgx_region` VALUES (2672, '510521', '泸县', 276, 3, 0, 'Lu Xian', 'LUX', '1-24-276-2672', '四川省-泸州市-泸县', 1);
INSERT INTO `qmgx_region` VALUES (2673, '510522', '合江县', 276, 3, 0, 'Hejiang Xian', 'HEJ', '1-24-276-2673', '四川省-泸州市-合江县', 1);
INSERT INTO `qmgx_region` VALUES (2674, '510524', '叙永县', 276, 3, 0, 'Xuyong Xian', 'XYO', '1-24-276-2674', '四川省-泸州市-叙永县', 1);
INSERT INTO `qmgx_region` VALUES (2675, '510525', '古蔺县', 276, 3, 0, 'Gulin Xian', 'GUL', '1-24-276-2675', '四川省-泸州市-古蔺县', 1);
INSERT INTO `qmgx_region` VALUES (2676, '510601', '市辖区', 277, 3, 0, 'Shixiaqu', '2', '1-24-277-2676', '四川省-德阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2677, '510603', '旌阳区', 277, 3, 0, 'Jingyang Qu', 'JYF', '1-24-277-2677', '四川省-德阳市-旌阳区', 1);
INSERT INTO `qmgx_region` VALUES (2678, '510623', '中江县', 277, 3, 0, 'Zhongjiang Xian', 'ZGJ', '1-24-277-2678', '四川省-德阳市-中江县', 1);
INSERT INTO `qmgx_region` VALUES (2679, '510626', '罗江县', 277, 3, 0, 'Luojiang Xian', 'LOJ', '1-24-277-2679', '四川省-德阳市-罗江县', 1);
INSERT INTO `qmgx_region` VALUES (2680, '510681', '广汉市', 277, 3, 0, 'Guanghan Shi', 'GHN', '1-24-277-2680', '四川省-德阳市-广汉市', 1);
INSERT INTO `qmgx_region` VALUES (2681, '510682', '什邡市', 277, 3, 0, 'Shifang Shi', 'SFS', '1-24-277-2681', '四川省-德阳市-什邡市', 1);
INSERT INTO `qmgx_region` VALUES (2682, '510683', '绵竹市', 277, 3, 0, 'Jinzhou Shi', 'MZU', '1-24-277-2682', '四川省-德阳市-绵竹市', 1);
INSERT INTO `qmgx_region` VALUES (2683, '510701', '市辖区', 278, 3, 0, 'Shixiaqu', '2', '1-24-278-2683', '四川省-绵阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2684, '510703', '涪城区', 278, 3, 0, 'Fucheng Qu', 'FCM', '1-24-278-2684', '四川省-绵阳市-涪城区', 1);
INSERT INTO `qmgx_region` VALUES (2685, '510704', '游仙区', 278, 3, 0, 'Youxian Qu', 'YXM', '1-24-278-2685', '四川省-绵阳市-游仙区', 1);
INSERT INTO `qmgx_region` VALUES (2686, '510722', '三台县', 278, 3, 0, 'Santai Xian', 'SNT', '1-24-278-2686', '四川省-绵阳市-三台县', 1);
INSERT INTO `qmgx_region` VALUES (2687, '510723', '盐亭县', 278, 3, 0, 'Yanting Xian', 'YTC', '1-24-278-2687', '四川省-绵阳市-盐亭县', 1);
INSERT INTO `qmgx_region` VALUES (2688, '510724', '安县', 278, 3, 0, 'An Xian', 'AXN', '1-24-278-2688', '四川省-绵阳市-安县', 1);
INSERT INTO `qmgx_region` VALUES (2689, '510725', '梓潼县', 278, 3, 0, 'Zitong Xian', 'ZTG', '1-24-278-2689', '四川省-绵阳市-梓潼县', 1);
INSERT INTO `qmgx_region` VALUES (2690, '510726', '北川羌族自治县', 278, 3, 0, 'Beichuanqiangzuzizhi Qu', '2', '1-24-278-2690', '四川省-绵阳市-北川羌族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2691, '510727', '平武县', 278, 3, 0, 'Pingwu Xian', 'PWU', '1-24-278-2691', '四川省-绵阳市-平武县', 1);
INSERT INTO `qmgx_region` VALUES (2692, '510781', '江油市', 278, 3, 0, 'Jiangyou Shi', 'JYO', '1-24-278-2692', '四川省-绵阳市-江油市', 1);
INSERT INTO `qmgx_region` VALUES (2693, '510801', '市辖区', 279, 3, 0, 'Shixiaqu', '2', '1-24-279-2693', '四川省-广元市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2694, '511002', '市中区', 279, 3, 0, 'Shizhong Qu', 'SZM', '1-24-279-2694', '四川省-广元市-市中区', 1);
INSERT INTO `qmgx_region` VALUES (2695, '510811', '元坝区', 279, 3, 0, 'Yuanba Qu', 'YBQ', '1-24-279-2695', '四川省-广元市-元坝区', 1);
INSERT INTO `qmgx_region` VALUES (2696, '510812', '朝天区', 279, 3, 0, 'Chaotian Qu', 'CTN', '1-24-279-2696', '四川省-广元市-朝天区', 1);
INSERT INTO `qmgx_region` VALUES (2697, '510821', '旺苍县', 279, 3, 0, 'Wangcang Xian', 'WGC', '1-24-279-2697', '四川省-广元市-旺苍县', 1);
INSERT INTO `qmgx_region` VALUES (2698, '510822', '青川县', 279, 3, 0, 'Qingchuan Xian', 'QCX', '1-24-279-2698', '四川省-广元市-青川县', 1);
INSERT INTO `qmgx_region` VALUES (2699, '510823', '剑阁县', 279, 3, 0, 'Jiange Xian', 'JGE', '1-24-279-2699', '四川省-广元市-剑阁县', 1);
INSERT INTO `qmgx_region` VALUES (2700, '510824', '苍溪县', 279, 3, 0, 'Cangxi Xian', 'CXC', '1-24-279-2700', '四川省-广元市-苍溪县', 1);
INSERT INTO `qmgx_region` VALUES (2701, '510901', '市辖区', 280, 3, 0, 'Shixiaqu', '2', '1-24-280-2701', '四川省-遂宁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2702, '510903', '船山区', 280, 3, 0, 'Chuanshan Qu', '2', '1-24-280-2702', '四川省-遂宁市-船山区', 1);
INSERT INTO `qmgx_region` VALUES (2703, '510904', '安居区', 280, 3, 0, 'Anju Qu', '2', '1-24-280-2703', '四川省-遂宁市-安居区', 1);
INSERT INTO `qmgx_region` VALUES (2704, '510921', '蓬溪县', 280, 3, 0, 'Pengxi Xian', 'PXI', '1-24-280-2704', '四川省-遂宁市-蓬溪县', 1);
INSERT INTO `qmgx_region` VALUES (2705, '510922', '射洪县', 280, 3, 0, 'Shehong Xian', 'SHE', '1-24-280-2705', '四川省-遂宁市-射洪县', 1);
INSERT INTO `qmgx_region` VALUES (2706, '510923', '大英县', 280, 3, 0, 'Daying Xian', 'DAY', '1-24-280-2706', '四川省-遂宁市-大英县', 1);
INSERT INTO `qmgx_region` VALUES (2707, '511001', '市辖区', 281, 3, 0, 'Shixiaqu', '2', '1-24-281-2707', '四川省-内江市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2708, '511002', '市中区', 281, 3, 0, 'Shizhong Qu', 'SZM', '1-24-281-2708', '四川省-内江市-市中区', 1);
INSERT INTO `qmgx_region` VALUES (2709, '511011', '东兴区', 281, 3, 0, 'Dongxing Qu', 'DXQ', '1-24-281-2709', '四川省-内江市-东兴区', 1);
INSERT INTO `qmgx_region` VALUES (2710, '511024', '威远县', 281, 3, 0, 'Weiyuan Xian', 'WYU', '1-24-281-2710', '四川省-内江市-威远县', 1);
INSERT INTO `qmgx_region` VALUES (2711, '511025', '资中县', 281, 3, 0, 'Zizhong Xian', 'ZZC', '1-24-281-2711', '四川省-内江市-资中县', 1);
INSERT INTO `qmgx_region` VALUES (2712, '511028', '隆昌县', 281, 3, 0, 'Longchang Xian', 'LCC', '1-24-281-2712', '四川省-内江市-隆昌县', 1);
INSERT INTO `qmgx_region` VALUES (2713, '511101', '市辖区', 282, 3, 0, 'Shixiaqu', '2', '1-24-282-2713', '四川省-乐山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2714, '511102', '市中区', 282, 3, 0, 'Shizhong Qu', 'SZP', '1-24-282-2714', '四川省-乐山市-市中区', 1);
INSERT INTO `qmgx_region` VALUES (2715, '511111', '沙湾区', 282, 3, 0, 'Shawan Qu', 'SWN', '1-24-282-2715', '四川省-乐山市-沙湾区', 1);
INSERT INTO `qmgx_region` VALUES (2716, '511112', '五通桥区', 282, 3, 0, 'Wutongqiao Qu', 'WTQ', '1-24-282-2716', '四川省-乐山市-五通桥区', 1);
INSERT INTO `qmgx_region` VALUES (2717, '511113', '金口河区', 282, 3, 0, 'Jinkouhe Qu', 'JKH', '1-24-282-2717', '四川省-乐山市-金口河区', 1);
INSERT INTO `qmgx_region` VALUES (2718, '511123', '犍为县', 282, 3, 0, 'Qianwei Xian', 'QWE', '1-24-282-2718', '四川省-乐山市-犍为县', 1);
INSERT INTO `qmgx_region` VALUES (2719, '511124', '井研县', 282, 3, 0, 'Jingyan Xian', 'JYA', '1-24-282-2719', '四川省-乐山市-井研县', 1);
INSERT INTO `qmgx_region` VALUES (2720, '511126', '夹江县', 282, 3, 0, 'Jiajiang Xian', 'JJC', '1-24-282-2720', '四川省-乐山市-夹江县', 1);
INSERT INTO `qmgx_region` VALUES (2721, '511129', '沐川县', 282, 3, 0, 'Muchuan Xian', 'MCH', '1-24-282-2721', '四川省-乐山市-沐川县', 1);
INSERT INTO `qmgx_region` VALUES (2722, '511132', '峨边彝族自治县', 282, 3, 0, 'Ebian Yizu Zizhixian', 'EBN', '1-24-282-2722', '四川省-乐山市-峨边彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2723, '511133', '马边彝族自治县', 282, 3, 0, 'Mabian Yizu Zizhixian', 'MBN', '1-24-282-2723', '四川省-乐山市-马边彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2724, '511181', '峨眉山市', 282, 3, 0, 'Emeishan Shi', 'EMS', '1-24-282-2724', '四川省-乐山市-峨眉山市', 1);
INSERT INTO `qmgx_region` VALUES (2725, '511301', '市辖区', 283, 3, 0, 'Shixiaqu', '2', '1-24-283-2725', '四川省-南充市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2726, '511302', '顺庆区', 283, 3, 0, 'Shunqing Xian', 'SQG', '1-24-283-2726', '四川省-南充市-顺庆区', 1);
INSERT INTO `qmgx_region` VALUES (2727, '511303', '高坪区', 283, 3, 0, 'Gaoping Qu', 'GPQ', '1-24-283-2727', '四川省-南充市-高坪区', 1);
INSERT INTO `qmgx_region` VALUES (2728, '511304', '嘉陵区', 283, 3, 0, 'Jialing Qu', 'JLG', '1-24-283-2728', '四川省-南充市-嘉陵区', 1);
INSERT INTO `qmgx_region` VALUES (2729, '511321', '南部县', 283, 3, 0, 'Nanbu Xian', 'NBU', '1-24-283-2729', '四川省-南充市-南部县', 1);
INSERT INTO `qmgx_region` VALUES (2730, '511322', '营山县', 283, 3, 0, 'Yingshan Xian', 'YGS', '1-24-283-2730', '四川省-南充市-营山县', 1);
INSERT INTO `qmgx_region` VALUES (2731, '511323', '蓬安县', 283, 3, 0, 'Peng,an Xian', 'PGA', '1-24-283-2731', '四川省-南充市-蓬安县', 1);
INSERT INTO `qmgx_region` VALUES (2732, '511324', '仪陇县', 283, 3, 0, 'Yilong Xian', 'YLC', '1-24-283-2732', '四川省-南充市-仪陇县', 1);
INSERT INTO `qmgx_region` VALUES (2733, '511325', '西充县', 283, 3, 0, 'Xichong Xian', 'XCO', '1-24-283-2733', '四川省-南充市-西充县', 1);
INSERT INTO `qmgx_region` VALUES (2734, '511381', '阆中市', 283, 3, 0, 'Langzhong Shi', 'LZJ', '1-24-283-2734', '四川省-南充市-阆中市', 1);
INSERT INTO `qmgx_region` VALUES (2735, '511401', '市辖区', 284, 3, 0, '1', '2', '1-24-284-2735', '四川省-眉山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2736, '511402', '东坡区', 284, 3, 0, 'Dongpo Qu', '2', '1-24-284-2736', '四川省-眉山市-东坡区', 1);
INSERT INTO `qmgx_region` VALUES (2737, '511421', '仁寿县', 284, 3, 0, 'Renshou Xian', '2', '1-24-284-2737', '四川省-眉山市-仁寿县', 1);
INSERT INTO `qmgx_region` VALUES (2738, '511422', '彭山县', 284, 3, 0, 'Pengshan Xian', '2', '1-24-284-2738', '四川省-眉山市-彭山县', 1);
INSERT INTO `qmgx_region` VALUES (2739, '511423', '洪雅县', 284, 3, 0, 'Hongya Xian', '2', '1-24-284-2739', '四川省-眉山市-洪雅县', 1);
INSERT INTO `qmgx_region` VALUES (2740, '511424', '丹棱县', 284, 3, 0, 'Danling Xian', '2', '1-24-284-2740', '四川省-眉山市-丹棱县', 1);
INSERT INTO `qmgx_region` VALUES (2741, '511425', '青神县', 284, 3, 0, 'Qingshen Xian', '2', '1-24-284-2741', '四川省-眉山市-青神县', 1);
INSERT INTO `qmgx_region` VALUES (2742, '511501', '市辖区', 285, 3, 0, 'Shixiaqu', '2', '1-24-285-2742', '四川省-宜宾市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2743, '511502', '翠屏区', 285, 3, 0, 'Cuiping Qu', 'CPQ', '1-24-285-2743', '四川省-宜宾市-翠屏区', 1);
INSERT INTO `qmgx_region` VALUES (2744, '511521', '宜宾县', 285, 3, 0, 'Yibin Xian', 'YBX', '1-24-285-2744', '四川省-宜宾市-宜宾县', 1);
INSERT INTO `qmgx_region` VALUES (2745, '511522', '南溪县', 285, 3, 0, 'Nanxi Xian', 'NNX', '1-24-285-2745', '四川省-宜宾市-南溪县', 1);
INSERT INTO `qmgx_region` VALUES (2746, '511523', '江安县', 285, 3, 0, 'Jiang,an Xian', 'JAC', '1-24-285-2746', '四川省-宜宾市-江安县', 1);
INSERT INTO `qmgx_region` VALUES (2747, '511524', '长宁县', 285, 3, 0, 'Changning Xian', 'CNX', '1-24-285-2747', '四川省-宜宾市-长宁县', 1);
INSERT INTO `qmgx_region` VALUES (2748, '511525', '高县', 285, 3, 0, 'Gao Xian', 'GAO', '1-24-285-2748', '四川省-宜宾市-高县', 1);
INSERT INTO `qmgx_region` VALUES (2749, '511526', '珙县', 285, 3, 0, 'Gong Xian', 'GOG', '1-24-285-2749', '四川省-宜宾市-珙县', 1);
INSERT INTO `qmgx_region` VALUES (2750, '511527', '筠连县', 285, 3, 0, 'Junlian Xian', 'JNL', '1-24-285-2750', '四川省-宜宾市-筠连县', 1);
INSERT INTO `qmgx_region` VALUES (2751, '511528', '兴文县', 285, 3, 0, 'Xingwen Xian', 'XWC', '1-24-285-2751', '四川省-宜宾市-兴文县', 1);
INSERT INTO `qmgx_region` VALUES (2752, '511529', '屏山县', 285, 3, 0, 'Pingshan Xian', 'PSC', '1-24-285-2752', '四川省-宜宾市-屏山县', 1);
INSERT INTO `qmgx_region` VALUES (2753, '511601', '市辖区', 286, 3, 0, 'Shixiaqu', '2', '1-24-286-2753', '四川省-广安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2754, '511602', '广安区', 286, 3, 0, 'Guang,an Qu', 'GAQ', '1-24-286-2754', '四川省-广安市-广安区', 1);
INSERT INTO `qmgx_region` VALUES (2755, '511621', '岳池县', 286, 3, 0, 'Yuechi Xian', 'YCC', '1-24-286-2755', '四川省-广安市-岳池县', 1);
INSERT INTO `qmgx_region` VALUES (2756, '511622', '武胜县', 286, 3, 0, 'Wusheng Xian', 'WSG', '1-24-286-2756', '四川省-广安市-武胜县', 1);
INSERT INTO `qmgx_region` VALUES (2757, '511623', '邻水县', 286, 3, 0, 'Linshui Xian', 'LSH', '1-24-286-2757', '四川省-广安市-邻水县', 1);
INSERT INTO `qmgx_region` VALUES (2759, '511701', '市辖区', 287, 3, 0, '1', '2', '1-24-287-2759', '四川省-达州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2760, '511702', '通川区', 287, 3, 0, 'Tongchuan Qu', '2', '1-24-287-2760', '四川省-达州市-通川区', 1);
INSERT INTO `qmgx_region` VALUES (2761, '511721', '达县', 287, 3, 0, 'Da Xian', '2', '1-24-287-2761', '四川省-达州市-达县', 1);
INSERT INTO `qmgx_region` VALUES (2762, '511722', '宣汉县', 287, 3, 0, 'Xuanhan Xian', '2', '1-24-287-2762', '四川省-达州市-宣汉县', 1);
INSERT INTO `qmgx_region` VALUES (2763, '511723', '开江县', 287, 3, 0, 'Kaijiang Xian', '2', '1-24-287-2763', '四川省-达州市-开江县', 1);
INSERT INTO `qmgx_region` VALUES (2764, '511724', '大竹县', 287, 3, 0, 'Dazhu Xian', '2', '1-24-287-2764', '四川省-达州市-大竹县', 1);
INSERT INTO `qmgx_region` VALUES (2765, '511725', '渠县', 287, 3, 0, 'Qu Xian', '2', '1-24-287-2765', '四川省-达州市-渠县', 1);
INSERT INTO `qmgx_region` VALUES (2766, '511781', '万源市', 287, 3, 0, 'Wanyuan Shi', '2', '1-24-287-2766', '四川省-达州市-万源市', 1);
INSERT INTO `qmgx_region` VALUES (2767, '511801', '市辖区', 288, 3, 0, '1', '2', '1-24-288-2767', '四川省-雅安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2768, '511802', '雨城区', 288, 3, 0, 'Yucheg Qu', '2', '1-24-288-2768', '四川省-雅安市-雨城区', 1);
INSERT INTO `qmgx_region` VALUES (2769, '511821', '名山县', 288, 3, 0, 'Mingshan Xian', '2', '1-24-288-2769', '四川省-雅安市-名山县', 1);
INSERT INTO `qmgx_region` VALUES (2770, '511822', '荥经县', 288, 3, 0, 'Yingjing Xian', '2', '1-24-288-2770', '四川省-雅安市-荥经县', 1);
INSERT INTO `qmgx_region` VALUES (2771, '511823', '汉源县', 288, 3, 0, 'Hanyuan Xian', '2', '1-24-288-2771', '四川省-雅安市-汉源县', 1);
INSERT INTO `qmgx_region` VALUES (2772, '511824', '石棉县', 288, 3, 0, 'Shimian Xian', '2', '1-24-288-2772', '四川省-雅安市-石棉县', 1);
INSERT INTO `qmgx_region` VALUES (2773, '511825', '天全县', 288, 3, 0, 'Tianquan Xian', '2', '1-24-288-2773', '四川省-雅安市-天全县', 1);
INSERT INTO `qmgx_region` VALUES (2774, '511826', '芦山县', 288, 3, 0, 'Lushan Xian', '2', '1-24-288-2774', '四川省-雅安市-芦山县', 1);
INSERT INTO `qmgx_region` VALUES (2775, '511827', '宝兴县', 288, 3, 0, 'Baoxing Xian', '2', '1-24-288-2775', '四川省-雅安市-宝兴县', 1);
INSERT INTO `qmgx_region` VALUES (2776, '511901', '市辖区', 289, 3, 0, '1', '2', '1-24-289-2776', '四川省-巴中市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2777, '511902', '巴州区', 289, 3, 0, 'Bazhou Qu', '2', '1-24-289-2777', '四川省-巴中市-巴州区', 1);
INSERT INTO `qmgx_region` VALUES (2778, '511921', '通江县', 289, 3, 0, 'Tongjiang Xian', '2', '1-24-289-2778', '四川省-巴中市-通江县', 1);
INSERT INTO `qmgx_region` VALUES (2779, '511922', '南江县', 289, 3, 0, 'Nanjiang Xian', '2', '1-24-289-2779', '四川省-巴中市-南江县', 1);
INSERT INTO `qmgx_region` VALUES (2780, '511923', '平昌县', 289, 3, 0, 'Pingchang Xian', '2', '1-24-289-2780', '四川省-巴中市-平昌县', 1);
INSERT INTO `qmgx_region` VALUES (2781, '512001', '市辖区', 290, 3, 0, '1', '2', '1-24-290-2781', '四川省-资阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2782, '512002', '雁江区', 290, 3, 0, 'Yanjiang Qu', '2', '1-24-290-2782', '四川省-资阳市-雁江区', 1);
INSERT INTO `qmgx_region` VALUES (2783, '512021', '安岳县', 290, 3, 0, 'Anyue Xian', '2', '1-24-290-2783', '四川省-资阳市-安岳县', 1);
INSERT INTO `qmgx_region` VALUES (2784, '512022', '乐至县', 290, 3, 0, 'Lezhi Xian', '2', '1-24-290-2784', '四川省-资阳市-乐至县', 1);
INSERT INTO `qmgx_region` VALUES (2785, '512081', '简阳市', 290, 3, 0, 'Jianyang Shi', '2', '1-24-290-2785', '四川省-资阳市-简阳市', 1);
INSERT INTO `qmgx_region` VALUES (2786, '513221', '汶川县', 291, 3, 0, 'Wenchuan Xian', 'WNC', '1-24-291-2786', '四川省-阿坝藏族羌族自治州-汶川县', 1);
INSERT INTO `qmgx_region` VALUES (2787, '513222', '理县', 291, 3, 0, 'Li Xian', 'LXC', '1-24-291-2787', '四川省-阿坝藏族羌族自治州-理县', 1);
INSERT INTO `qmgx_region` VALUES (2788, '513223', '茂县', 291, 3, 0, 'Mao Xian', 'MAO', '1-24-291-2788', '四川省-阿坝藏族羌族自治州-茂县', 1);
INSERT INTO `qmgx_region` VALUES (2789, '513224', '松潘县', 291, 3, 0, 'Songpan Xian', 'SOP', '1-24-291-2789', '四川省-阿坝藏族羌族自治州-松潘县', 1);
INSERT INTO `qmgx_region` VALUES (2790, '513225', '九寨沟县', 291, 3, 0, 'Jiuzhaigou Xian', 'JZG', '1-24-291-2790', '四川省-阿坝藏族羌族自治州-九寨沟县', 1);
INSERT INTO `qmgx_region` VALUES (2791, '513226', '金川县', 291, 3, 0, 'Jinchuan Xian', 'JCH', '1-24-291-2791', '四川省-阿坝藏族羌族自治州-金川县', 1);
INSERT INTO `qmgx_region` VALUES (2792, '513227', '小金县', 291, 3, 0, 'Xiaojin Xian', 'XJX', '1-24-291-2792', '四川省-阿坝藏族羌族自治州-小金县', 1);
INSERT INTO `qmgx_region` VALUES (2793, '513228', '黑水县', 291, 3, 0, 'Heishui Xian', 'HIS', '1-24-291-2793', '四川省-阿坝藏族羌族自治州-黑水县', 1);
INSERT INTO `qmgx_region` VALUES (2794, '513229', '马尔康县', 291, 3, 0, 'Barkam Xian', 'BAK', '1-24-291-2794', '四川省-阿坝藏族羌族自治州-马尔康县', 1);
INSERT INTO `qmgx_region` VALUES (2795, '513230', '壤塘县', 291, 3, 0, 'Zamtang Xian', 'ZAM', '1-24-291-2795', '四川省-阿坝藏族羌族自治州-壤塘县', 1);
INSERT INTO `qmgx_region` VALUES (2796, '513231', '阿坝县', 291, 3, 0, 'Aba(Ngawa) Xian', 'ABX', '1-24-291-2796', '四川省-阿坝藏族羌族自治州-阿坝县', 1);
INSERT INTO `qmgx_region` VALUES (2797, '513232', '若尔盖县', 291, 3, 0, 'ZoigeXian', 'ZOI', '1-24-291-2797', '四川省-阿坝藏族羌族自治州-若尔盖县', 1);
INSERT INTO `qmgx_region` VALUES (2798, '513233', '红原县', 291, 3, 0, 'Hongyuan Xian', 'HOY', '1-24-291-2798', '四川省-阿坝藏族羌族自治州-红原县', 1);
INSERT INTO `qmgx_region` VALUES (2799, '513321', '康定县', 292, 3, 0, 'Kangding(Dardo) Xian', 'KDX', '1-24-292-2799', '四川省-甘孜藏族自治州-康定县', 1);
INSERT INTO `qmgx_region` VALUES (2800, '513322', '泸定县', 292, 3, 0, 'Luding(Jagsamka) Xian', 'LUD', '1-24-292-2800', '四川省-甘孜藏族自治州-泸定县', 1);
INSERT INTO `qmgx_region` VALUES (2801, '513323', '丹巴县', 292, 3, 0, 'Danba(Rongzhag) Xian', 'DBA', '1-24-292-2801', '四川省-甘孜藏族自治州-丹巴县', 1);
INSERT INTO `qmgx_region` VALUES (2802, '513324', '九龙县', 292, 3, 0, 'Jiulong(Gyaisi) Xian', 'JLC', '1-24-292-2802', '四川省-甘孜藏族自治州-九龙县', 1);
INSERT INTO `qmgx_region` VALUES (2803, '513325', '雅江县', 292, 3, 0, 'Yajiang(Nyagquka) Xian', 'YAJ', '1-24-292-2803', '四川省-甘孜藏族自治州-雅江县', 1);
INSERT INTO `qmgx_region` VALUES (2804, '513326', '道孚县', 292, 3, 0, 'Dawu Xian', 'DAW', '1-24-292-2804', '四川省-甘孜藏族自治州-道孚县', 1);
INSERT INTO `qmgx_region` VALUES (2805, '513327', '炉霍县', 292, 3, 0, 'Luhuo(Zhaggo) Xian', 'LUH', '1-24-292-2805', '四川省-甘孜藏族自治州-炉霍县', 1);
INSERT INTO `qmgx_region` VALUES (2806, '513328', '甘孜县', 292, 3, 0, 'Garze Xian', 'GRZ', '1-24-292-2806', '四川省-甘孜藏族自治州-甘孜县', 1);
INSERT INTO `qmgx_region` VALUES (2807, '513329', '新龙县', 292, 3, 0, 'Xinlong(Nyagrong) Xian', 'XLG', '1-24-292-2807', '四川省-甘孜藏族自治州-新龙县', 1);
INSERT INTO `qmgx_region` VALUES (2808, '513330', '德格县', 292, 3, 0, 'DegeXian', 'DEG', '1-24-292-2808', '四川省-甘孜藏族自治州-德格县', 1);
INSERT INTO `qmgx_region` VALUES (2809, '513331', '白玉县', 292, 3, 0, 'Baiyu Xian', 'BYC', '1-24-292-2809', '四川省-甘孜藏族自治州-白玉县', 1);
INSERT INTO `qmgx_region` VALUES (2810, '513332', '石渠县', 292, 3, 0, 'Serxv Xian', 'SER', '1-24-292-2810', '四川省-甘孜藏族自治州-石渠县', 1);
INSERT INTO `qmgx_region` VALUES (2811, '513333', '色达县', 292, 3, 0, 'Sertar Xian', 'STX', '1-24-292-2811', '四川省-甘孜藏族自治州-色达县', 1);
INSERT INTO `qmgx_region` VALUES (2812, '513334', '理塘县', 292, 3, 0, 'Litang Xian', 'LIT', '1-24-292-2812', '四川省-甘孜藏族自治州-理塘县', 1);
INSERT INTO `qmgx_region` VALUES (2813, '513335', '巴塘县', 292, 3, 0, 'Batang Xian', 'BTC', '1-24-292-2813', '四川省-甘孜藏族自治州-巴塘县', 1);
INSERT INTO `qmgx_region` VALUES (2814, '513336', '乡城县', 292, 3, 0, 'Xiangcheng(Qagcheng) Xian', 'XCC', '1-24-292-2814', '四川省-甘孜藏族自治州-乡城县', 1);
INSERT INTO `qmgx_region` VALUES (2815, '513337', '稻城县', 292, 3, 0, 'Daocheng(Dabba) Xian', 'DCX', '1-24-292-2815', '四川省-甘孜藏族自治州-稻城县', 1);
INSERT INTO `qmgx_region` VALUES (2816, '513338', '得荣县', 292, 3, 0, 'Derong Xian', 'DER', '1-24-292-2816', '四川省-甘孜藏族自治州-得荣县', 1);
INSERT INTO `qmgx_region` VALUES (2817, '513401', '西昌市', 293, 3, 0, 'Xichang Shi', 'XCA', '1-24-293-2817', '四川省-凉山彝族自治州-西昌市', 1);
INSERT INTO `qmgx_region` VALUES (2818, '513422', '木里藏族自治县', 293, 3, 0, 'Muli Zangzu Zizhixian', 'MLI', '1-24-293-2818', '四川省-凉山彝族自治州-木里藏族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2819, '513423', '盐源县', 293, 3, 0, 'Yanyuan Xian', 'YYU', '1-24-293-2819', '四川省-凉山彝族自治州-盐源县', 1);
INSERT INTO `qmgx_region` VALUES (2820, '513424', '德昌县', 293, 3, 0, 'Dechang Xian', 'DEC', '1-24-293-2820', '四川省-凉山彝族自治州-德昌县', 1);
INSERT INTO `qmgx_region` VALUES (2821, '513425', '会理县', 293, 3, 0, 'Huili Xian', 'HLI', '1-24-293-2821', '四川省-凉山彝族自治州-会理县', 1);
INSERT INTO `qmgx_region` VALUES (2822, '513426', '会东县', 293, 3, 0, 'Huidong Xian', 'HDG', '1-24-293-2822', '四川省-凉山彝族自治州-会东县', 1);
INSERT INTO `qmgx_region` VALUES (2823, '513427', '宁南县', 293, 3, 0, 'Ningnan Xian', 'NIN', '1-24-293-2823', '四川省-凉山彝族自治州-宁南县', 1);
INSERT INTO `qmgx_region` VALUES (2824, '513428', '普格县', 293, 3, 0, 'Puge Xian', 'PGE', '1-24-293-2824', '四川省-凉山彝族自治州-普格县', 1);
INSERT INTO `qmgx_region` VALUES (2825, '513429', '布拖县', 293, 3, 0, 'Butuo Xian', 'BTO', '1-24-293-2825', '四川省-凉山彝族自治州-布拖县', 1);
INSERT INTO `qmgx_region` VALUES (2826, '513430', '金阳县', 293, 3, 0, 'Jinyang Xian', 'JYW', '1-24-293-2826', '四川省-凉山彝族自治州-金阳县', 1);
INSERT INTO `qmgx_region` VALUES (2827, '513431', '昭觉县', 293, 3, 0, 'Zhaojue Xian', 'ZJE', '1-24-293-2827', '四川省-凉山彝族自治州-昭觉县', 1);
INSERT INTO `qmgx_region` VALUES (2828, '513432', '喜德县', 293, 3, 0, 'Xide Xian', 'XDE', '1-24-293-2828', '四川省-凉山彝族自治州-喜德县', 1);
INSERT INTO `qmgx_region` VALUES (2829, '513433', '冕宁县', 293, 3, 0, 'Mianning Xian', 'MNG', '1-24-293-2829', '四川省-凉山彝族自治州-冕宁县', 1);
INSERT INTO `qmgx_region` VALUES (2830, '513434', '越西县', 293, 3, 0, 'Yuexi Xian', 'YXC', '1-24-293-2830', '四川省-凉山彝族自治州-越西县', 1);
INSERT INTO `qmgx_region` VALUES (2831, '513435', '甘洛县', 293, 3, 0, 'Ganluo Xian', 'GLO', '1-24-293-2831', '四川省-凉山彝族自治州-甘洛县', 1);
INSERT INTO `qmgx_region` VALUES (2832, '513436', '美姑县', 293, 3, 0, 'Meigu Xian', 'MEG', '1-24-293-2832', '四川省-凉山彝族自治州-美姑县', 1);
INSERT INTO `qmgx_region` VALUES (2833, '513437', '雷波县', 293, 3, 0, 'Leibo Xian', 'LBX', '1-24-293-2833', '四川省-凉山彝族自治州-雷波县', 1);
INSERT INTO `qmgx_region` VALUES (2834, '520101', '市辖区', 294, 3, 0, 'Shixiaqu', '2', '1-25-294-2834', '贵州省-贵阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2835, '520102', '南明区', 294, 3, 0, 'Nanming Qu', 'NMQ', '1-25-294-2835', '贵州省-贵阳市-南明区', 1);
INSERT INTO `qmgx_region` VALUES (2836, '520103', '云岩区', 294, 3, 0, 'Yunyan Qu', 'YYQ', '1-25-294-2836', '贵州省-贵阳市-云岩区', 1);
INSERT INTO `qmgx_region` VALUES (2837, '520111', '花溪区', 294, 3, 0, 'Huaxi Qu', 'HXI', '1-25-294-2837', '贵州省-贵阳市-花溪区', 1);
INSERT INTO `qmgx_region` VALUES (2838, '520112', '乌当区', 294, 3, 0, 'Wudang Qu', 'WDQ', '1-25-294-2838', '贵州省-贵阳市-乌当区', 1);
INSERT INTO `qmgx_region` VALUES (2839, '520113', '白云区', 294, 3, 0, 'Baiyun Qu', 'BYU', '1-25-294-2839', '贵州省-贵阳市-白云区', 1);
INSERT INTO `qmgx_region` VALUES (2840, '520114', '小河区', 294, 3, 0, 'Xiaohe Qu', '2', '1-25-294-2840', '贵州省-贵阳市-小河区', 1);
INSERT INTO `qmgx_region` VALUES (2841, '520121', '开阳县', 294, 3, 0, 'Kaiyang Xian', 'KYG', '1-25-294-2841', '贵州省-贵阳市-开阳县', 1);
INSERT INTO `qmgx_region` VALUES (2842, '520122', '息烽县', 294, 3, 0, 'Xifeng Xian', 'XFX', '1-25-294-2842', '贵州省-贵阳市-息烽县', 1);
INSERT INTO `qmgx_region` VALUES (2843, '520123', '修文县', 294, 3, 0, 'Xiuwen Xian', 'XWX', '1-25-294-2843', '贵州省-贵阳市-修文县', 1);
INSERT INTO `qmgx_region` VALUES (2844, '520181', '清镇市', 294, 3, 0, 'Qingzhen Shi', 'QZN', '1-25-294-2844', '贵州省-贵阳市-清镇市', 1);
INSERT INTO `qmgx_region` VALUES (2845, '520201', '钟山区', 295, 3, 0, 'Zhongshan Qu', 'ZSQ', '1-25-295-2845', '贵州省-六盘水市-钟山区', 1);
INSERT INTO `qmgx_region` VALUES (2846, '520203', '六枝特区', 295, 3, 0, 'Liuzhi Tequ', 'LZT', '1-25-295-2846', '贵州省-六盘水市-六枝特区', 1);
INSERT INTO `qmgx_region` VALUES (2847, '520221', '水城县', 295, 3, 0, 'Shuicheng Xian', 'SUC', '1-25-295-2847', '贵州省-六盘水市-水城县', 1);
INSERT INTO `qmgx_region` VALUES (2848, '520222', '盘县', 295, 3, 0, 'Pan Xian', '2', '1-25-295-2848', '贵州省-六盘水市-盘县', 1);
INSERT INTO `qmgx_region` VALUES (2849, '520301', '市辖区', 296, 3, 0, 'Shixiaqu', '2', '1-25-296-2849', '贵州省-遵义市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2850, '520302', '红花岗区', 296, 3, 0, 'Honghuagang Qu', 'HHG', '1-25-296-2850', '贵州省-遵义市-红花岗区', 1);
INSERT INTO `qmgx_region` VALUES (2851, '520303', '汇川区', 296, 3, 0, 'Huichuan Qu', '2', '1-25-296-2851', '贵州省-遵义市-汇川区', 1);
INSERT INTO `qmgx_region` VALUES (2852, '520321', '遵义县', 296, 3, 0, 'Zunyi Xian', 'ZYI', '1-25-296-2852', '贵州省-遵义市-遵义县', 1);
INSERT INTO `qmgx_region` VALUES (2853, '520322', '桐梓县', 296, 3, 0, 'Tongzi Xian', 'TZI', '1-25-296-2853', '贵州省-遵义市-桐梓县', 1);
INSERT INTO `qmgx_region` VALUES (2854, '520323', '绥阳县', 296, 3, 0, 'Suiyang Xian', 'SUY', '1-25-296-2854', '贵州省-遵义市-绥阳县', 1);
INSERT INTO `qmgx_region` VALUES (2855, '520324', '正安县', 296, 3, 0, 'Zhengan Xan', '2', '1-25-296-2855', '贵州省-遵义市-正安县', 1);
INSERT INTO `qmgx_region` VALUES (2856, '520325', '道真仡佬族苗族自治县', 296, 3, 0, 'Daozhen Gelaozu Miaozu Zizhixian', 'DZN', '1-25-296-2856', '贵州省-遵义市-道真仡佬族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2857, '520326', '务川仡佬族苗族自治县', 296, 3, 0, 'Wuchuan Gelaozu Miaozu Zizhixian', 'WCU', '1-25-296-2857', '贵州省-遵义市-务川仡佬族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2858, '520327', '凤冈县', 296, 3, 0, 'Fenggang Xian', 'FGG', '1-25-296-2858', '贵州省-遵义市-凤冈县', 1);
INSERT INTO `qmgx_region` VALUES (2859, '520328', '湄潭县', 296, 3, 0, 'Meitan Xian', 'MTN', '1-25-296-2859', '贵州省-遵义市-湄潭县', 1);
INSERT INTO `qmgx_region` VALUES (2860, '520329', '余庆县', 296, 3, 0, 'Yuqing Xian', 'YUQ', '1-25-296-2860', '贵州省-遵义市-余庆县', 1);
INSERT INTO `qmgx_region` VALUES (2861, '520330', '习水县', 296, 3, 0, 'Xishui Xian', 'XSI', '1-25-296-2861', '贵州省-遵义市-习水县', 1);
INSERT INTO `qmgx_region` VALUES (2862, '520381', '赤水市', 296, 3, 0, 'Chishui Shi', 'CSS', '1-25-296-2862', '贵州省-遵义市-赤水市', 1);
INSERT INTO `qmgx_region` VALUES (2863, '520382', '仁怀市', 296, 3, 0, 'Renhuai Shi', 'RHS', '1-25-296-2863', '贵州省-遵义市-仁怀市', 1);
INSERT INTO `qmgx_region` VALUES (2864, '520401', '市辖区', 297, 3, 0, '1', '2', '1-25-297-2864', '贵州省-安顺市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2865, '520402', '西秀区', 297, 3, 0, 'Xixiu Qu', '2', '1-25-297-2865', '贵州省-安顺市-西秀区', 1);
INSERT INTO `qmgx_region` VALUES (2866, '520421', '平坝县', 297, 3, 0, 'Pingba Xian', '2', '1-25-297-2866', '贵州省-安顺市-平坝县', 1);
INSERT INTO `qmgx_region` VALUES (2867, '520422', '普定县', 297, 3, 0, 'Puding Xian', '2', '1-25-297-2867', '贵州省-安顺市-普定县', 1);
INSERT INTO `qmgx_region` VALUES (2868, '520423', '镇宁布依族苗族自治县', 297, 3, 0, 'Zhenning Buyeizu Miaozu Zizhixian', '2', '1-25-297-2868', '贵州省-安顺市-镇宁布依族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2869, '520424', '关岭布依族苗族自治县', 297, 3, 0, 'Guanling Buyeizu Miaozu Zizhixian', '2', '1-25-297-2869', '贵州省-安顺市-关岭布依族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2870, '520425', '紫云苗族布依族自治县', 297, 3, 0, 'Ziyun Miaozu Buyeizu Zizhixian', '2', '1-25-297-2870', '贵州省-安顺市-紫云苗族布依族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2871, '522201', '铜仁市', 298, 3, 0, 'Tongren Shi', 'TRS', '1-25-298-2871', '贵州省-铜仁地区-铜仁市', 1);
INSERT INTO `qmgx_region` VALUES (2872, '522222', '江口县', 298, 3, 0, 'Jiangkou Xian', 'JGK', '1-25-298-2872', '贵州省-铜仁地区-江口县', 1);
INSERT INTO `qmgx_region` VALUES (2873, '522223', '玉屏侗族自治县', 298, 3, 0, 'Yuping Dongzu Zizhixian', 'YPG', '1-25-298-2873', '贵州省-铜仁地区-玉屏侗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2874, '522224', '石阡县', 298, 3, 0, 'Shiqian Xian', 'SQI', '1-25-298-2874', '贵州省-铜仁地区-石阡县', 1);
INSERT INTO `qmgx_region` VALUES (2875, '522225', '思南县', 298, 3, 0, 'Sinan Xian', 'SNA', '1-25-298-2875', '贵州省-铜仁地区-思南县', 1);
INSERT INTO `qmgx_region` VALUES (2876, '522226', '印江土家族苗族自治县', 298, 3, 0, 'Yinjiang Tujiazu Miaozu Zizhixian', 'YJY', '1-25-298-2876', '贵州省-铜仁地区-印江土家族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2877, '522227', '德江县', 298, 3, 0, 'Dejiang Xian', 'DEJ', '1-25-298-2877', '贵州省-铜仁地区-德江县', 1);
INSERT INTO `qmgx_region` VALUES (2878, '522228', '沿河土家族自治县', 298, 3, 0, 'Yanhe Tujiazu Zizhixian', 'YHE', '1-25-298-2878', '贵州省-铜仁地区-沿河土家族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2879, '522229', '松桃苗族自治县', 298, 3, 0, 'Songtao Miaozu Zizhixian', 'STM', '1-25-298-2879', '贵州省-铜仁地区-松桃苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2880, '522230', '万山特区', 298, 3, 0, 'Wanshan Tequ', 'WAS', '1-25-298-2880', '贵州省-铜仁地区-万山特区', 1);
INSERT INTO `qmgx_region` VALUES (2881, '522301', '兴义市', 299, 3, 0, 'Xingyi Shi', 'XYI', '1-25-299-2881', '贵州省-黔西南布依族苗族自治州-兴义市', 1);
INSERT INTO `qmgx_region` VALUES (2882, '522322', '兴仁县', 299, 3, 0, 'Xingren Xian', 'XRN', '1-25-299-2882', '贵州省-黔西南布依族苗族自治州-兴仁县', 1);
INSERT INTO `qmgx_region` VALUES (2883, '522323', '普安县', 299, 3, 0, 'Pu,an Xian', 'PUA', '1-25-299-2883', '贵州省-黔西南布依族苗族自治州-普安县', 1);
INSERT INTO `qmgx_region` VALUES (2884, '522324', '晴隆县', 299, 3, 0, 'Qinglong Xian', 'QLG', '1-25-299-2884', '贵州省-黔西南布依族苗族自治州-晴隆县', 1);
INSERT INTO `qmgx_region` VALUES (2885, '522325', '贞丰县', 299, 3, 0, 'Zhenfeng Xian', 'ZFG', '1-25-299-2885', '贵州省-黔西南布依族苗族自治州-贞丰县', 1);
INSERT INTO `qmgx_region` VALUES (2886, '522326', '望谟县', 299, 3, 0, 'Wangmo Xian', 'WMO', '1-25-299-2886', '贵州省-黔西南布依族苗族自治州-望谟县', 1);
INSERT INTO `qmgx_region` VALUES (2887, '522327', '册亨县', 299, 3, 0, 'Ceheng Xian', 'CEH', '1-25-299-2887', '贵州省-黔西南布依族苗族自治州-册亨县', 1);
INSERT INTO `qmgx_region` VALUES (2888, '522328', '安龙县', 299, 3, 0, 'Anlong Xian', 'ALG', '1-25-299-2888', '贵州省-黔西南布依族苗族自治州-安龙县', 1);
INSERT INTO `qmgx_region` VALUES (2889, '522401', '毕节市', 300, 3, 0, 'Bijie Shi', 'BJE', '1-25-300-2889', '贵州省-毕节地区-毕节市', 1);
INSERT INTO `qmgx_region` VALUES (2890, '522422', '大方县', 300, 3, 0, 'Dafang Xian', 'DAF', '1-25-300-2890', '贵州省-毕节地区-大方县', 1);
INSERT INTO `qmgx_region` VALUES (2891, '522423', '黔西县', 300, 3, 0, 'Qianxi Xian', 'QNX', '1-25-300-2891', '贵州省-毕节地区-黔西县', 1);
INSERT INTO `qmgx_region` VALUES (2892, '522424', '金沙县', 300, 3, 0, 'Jinsha Xian', 'JSX', '1-25-300-2892', '贵州省-毕节地区-金沙县', 1);
INSERT INTO `qmgx_region` VALUES (2893, '522425', '织金县', 300, 3, 0, 'Zhijin Xian', 'ZJN', '1-25-300-2893', '贵州省-毕节地区-织金县', 1);
INSERT INTO `qmgx_region` VALUES (2894, '522426', '纳雍县', 300, 3, 0, 'Nayong Xian', 'NYG', '1-25-300-2894', '贵州省-毕节地区-纳雍县', 1);
INSERT INTO `qmgx_region` VALUES (2895, '522427', '威宁彝族回族苗族自治县', 300, 3, 0, 'Weining Yizu Huizu Miaozu Zizhixian', 'WNG', '1-25-300-2895', '贵州省-毕节地区-威宁彝族回族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2896, '522428', '赫章县', 300, 3, 0, 'Hezhang Xian', 'HZA', '1-25-300-2896', '贵州省-毕节地区-赫章县', 1);
INSERT INTO `qmgx_region` VALUES (2897, '522601', '凯里市', 301, 3, 0, 'Kaili Shi', 'KLS', '1-25-301-2897', '贵州省-黔东南苗族侗族自治州-凯里市', 1);
INSERT INTO `qmgx_region` VALUES (2898, '522622', '黄平县', 301, 3, 0, 'Huangping Xian', 'HPN', '1-25-301-2898', '贵州省-黔东南苗族侗族自治州-黄平县', 1);
INSERT INTO `qmgx_region` VALUES (2899, '522623', '施秉县', 301, 3, 0, 'Shibing Xian', 'SBG', '1-25-301-2899', '贵州省-黔东南苗族侗族自治州-施秉县', 1);
INSERT INTO `qmgx_region` VALUES (2900, '522624', '三穗县', 301, 3, 0, 'Sansui Xian', 'SAS', '1-25-301-2900', '贵州省-黔东南苗族侗族自治州-三穗县', 1);
INSERT INTO `qmgx_region` VALUES (2901, '522625', '镇远县', 301, 3, 0, 'Zhenyuan Xian', 'ZYX', '1-25-301-2901', '贵州省-黔东南苗族侗族自治州-镇远县', 1);
INSERT INTO `qmgx_region` VALUES (2902, '522626', '岑巩县', 301, 3, 0, 'Cengong Xian', 'CGX', '1-25-301-2902', '贵州省-黔东南苗族侗族自治州-岑巩县', 1);
INSERT INTO `qmgx_region` VALUES (2903, '522627', '天柱县', 301, 3, 0, 'Tianzhu Xian', 'TZU', '1-25-301-2903', '贵州省-黔东南苗族侗族自治州-天柱县', 1);
INSERT INTO `qmgx_region` VALUES (2904, '522628', '锦屏县', 301, 3, 0, 'Jinping Xian', 'JPX', '1-25-301-2904', '贵州省-黔东南苗族侗族自治州-锦屏县', 1);
INSERT INTO `qmgx_region` VALUES (2905, '522629', '剑河县', 301, 3, 0, 'Jianhe Xian', 'JHE', '1-25-301-2905', '贵州省-黔东南苗族侗族自治州-剑河县', 1);
INSERT INTO `qmgx_region` VALUES (2906, '522630', '台江县', 301, 3, 0, 'Taijiang Xian', 'TJX', '1-25-301-2906', '贵州省-黔东南苗族侗族自治州-台江县', 1);
INSERT INTO `qmgx_region` VALUES (2907, '522631', '黎平县', 301, 3, 0, 'Liping Xian', 'LIP', '1-25-301-2907', '贵州省-黔东南苗族侗族自治州-黎平县', 1);
INSERT INTO `qmgx_region` VALUES (2908, '522632', '榕江县', 301, 3, 0, 'Rongjiang Xian', 'RJG', '1-25-301-2908', '贵州省-黔东南苗族侗族自治州-榕江县', 1);
INSERT INTO `qmgx_region` VALUES (2909, '522633', '从江县', 301, 3, 0, 'Congjiang Xian', 'COJ', '1-25-301-2909', '贵州省-黔东南苗族侗族自治州-从江县', 1);
INSERT INTO `qmgx_region` VALUES (2910, '522634', '雷山县', 301, 3, 0, 'Leishan Xian', 'LSA', '1-25-301-2910', '贵州省-黔东南苗族侗族自治州-雷山县', 1);
INSERT INTO `qmgx_region` VALUES (2911, '522635', '麻江县', 301, 3, 0, 'Majiang Xian', 'MAJ', '1-25-301-2911', '贵州省-黔东南苗族侗族自治州-麻江县', 1);
INSERT INTO `qmgx_region` VALUES (2912, '522636', '丹寨县', 301, 3, 0, 'Danzhai Xian', 'DZH', '1-25-301-2912', '贵州省-黔东南苗族侗族自治州-丹寨县', 1);
INSERT INTO `qmgx_region` VALUES (2913, '522701', '都匀市', 302, 3, 0, 'Duyun Shi', 'DUY', '1-25-302-2913', '贵州省-黔南布依族苗族自治州-都匀市', 1);
INSERT INTO `qmgx_region` VALUES (2914, '522702', '福泉市', 302, 3, 0, 'Fuquan Shi', 'FQN', '1-25-302-2914', '贵州省-黔南布依族苗族自治州-福泉市', 1);
INSERT INTO `qmgx_region` VALUES (2915, '522722', '荔波县', 302, 3, 0, 'Libo Xian', 'LBO', '1-25-302-2915', '贵州省-黔南布依族苗族自治州-荔波县', 1);
INSERT INTO `qmgx_region` VALUES (2916, '522723', '贵定县', 302, 3, 0, 'Guiding Xian', 'GDG', '1-25-302-2916', '贵州省-黔南布依族苗族自治州-贵定县', 1);
INSERT INTO `qmgx_region` VALUES (2917, '522725', '瓮安县', 302, 3, 0, 'Weng,an Xian', 'WGA', '1-25-302-2917', '贵州省-黔南布依族苗族自治州-瓮安县', 1);
INSERT INTO `qmgx_region` VALUES (2918, '522726', '独山县', 302, 3, 0, 'Dushan Xian', 'DSX', '1-25-302-2918', '贵州省-黔南布依族苗族自治州-独山县', 1);
INSERT INTO `qmgx_region` VALUES (2919, '522727', '平塘县', 302, 3, 0, 'Pingtang Xian', 'PTG', '1-25-302-2919', '贵州省-黔南布依族苗族自治州-平塘县', 1);
INSERT INTO `qmgx_region` VALUES (2920, '522728', '罗甸县', 302, 3, 0, 'Luodian Xian', 'LOD', '1-25-302-2920', '贵州省-黔南布依族苗族自治州-罗甸县', 1);
INSERT INTO `qmgx_region` VALUES (2921, '522729', '长顺县', 302, 3, 0, 'Changshun Xian', 'CSU', '1-25-302-2921', '贵州省-黔南布依族苗族自治州-长顺县', 1);
INSERT INTO `qmgx_region` VALUES (2922, '522730', '龙里县', 302, 3, 0, 'Longli Xian', 'LLI', '1-25-302-2922', '贵州省-黔南布依族苗族自治州-龙里县', 1);
INSERT INTO `qmgx_region` VALUES (2923, '522731', '惠水县', 302, 3, 0, 'Huishui Xian', 'HUS', '1-25-302-2923', '贵州省-黔南布依族苗族自治州-惠水县', 1);
INSERT INTO `qmgx_region` VALUES (2924, '522732', '三都水族自治县', 302, 3, 0, 'Sandu Suizu Zizhixian', 'SDU', '1-25-302-2924', '贵州省-黔南布依族苗族自治州-三都水族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2925, '530101', '市辖区', 303, 3, 0, 'Shixiaqu', '2', '1-26-303-2925', '云南省-昆明市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2926, '530102', '五华区', 303, 3, 0, 'Wuhua Qu', 'WHA', '1-26-303-2926', '云南省-昆明市-五华区', 1);
INSERT INTO `qmgx_region` VALUES (2927, '530103', '盘龙区', 303, 3, 0, 'Panlong Qu', 'PLQ', '1-26-303-2927', '云南省-昆明市-盘龙区', 1);
INSERT INTO `qmgx_region` VALUES (2928, '530111', '官渡区', 303, 3, 0, 'Guandu Qu', 'GDU', '1-26-303-2928', '云南省-昆明市-官渡区', 1);
INSERT INTO `qmgx_region` VALUES (2929, '530112', '西山区', 303, 3, 0, 'Xishan Qu', 'XSN', '1-26-303-2929', '云南省-昆明市-西山区', 1);
INSERT INTO `qmgx_region` VALUES (2930, '530113', '东川区', 303, 3, 0, 'Dongchuan Qu', 'DCU', '1-26-303-2930', '云南省-昆明市-东川区', 1);
INSERT INTO `qmgx_region` VALUES (2931, '530121', '呈贡县', 303, 3, 0, 'Chenggong Xian', 'CGD', '1-26-303-2931', '云南省-昆明市-呈贡县', 1);
INSERT INTO `qmgx_region` VALUES (2932, '530122', '晋宁县', 303, 3, 0, 'Jinning Xian', 'JND', '1-26-303-2932', '云南省-昆明市-晋宁县', 1);
INSERT INTO `qmgx_region` VALUES (2933, '530124', '富民县', 303, 3, 0, 'Fumin Xian', 'FMN', '1-26-303-2933', '云南省-昆明市-富民县', 1);
INSERT INTO `qmgx_region` VALUES (2934, '530125', '宜良县', 303, 3, 0, 'Yiliang Xian', 'YIL', '1-26-303-2934', '云南省-昆明市-宜良县', 1);
INSERT INTO `qmgx_region` VALUES (2935, '530126', '石林彝族自治县', 303, 3, 0, 'Shilin Yizu Zizhixian', 'SLY', '1-26-303-2935', '云南省-昆明市-石林彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2936, '530127', '嵩明县', 303, 3, 0, 'Songming Xian', 'SMI', '1-26-303-2936', '云南省-昆明市-嵩明县', 1);
INSERT INTO `qmgx_region` VALUES (2937, '530128', '禄劝彝族苗族自治县', 303, 3, 0, 'Luchuan Yizu Miaozu Zizhixian', 'LUC', '1-26-303-2937', '云南省-昆明市-禄劝彝族苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2938, '530129', '寻甸回族彝族自治县', 303, 3, 0, 'Xundian Huizu Yizu Zizhixian', 'XDN', '1-26-303-2938', '云南省-昆明市-寻甸回族彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2939, '530181', '安宁市', 303, 3, 0, 'Anning Shi', 'ANG', '1-26-303-2939', '云南省-昆明市-安宁市', 1);
INSERT INTO `qmgx_region` VALUES (2940, '530301', '市辖区', 304, 3, 0, 'Shixiaqu', '2', '1-26-304-2940', '云南省-曲靖市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2941, '530302', '麒麟区', 304, 3, 0, 'Qilin Xian', 'QLQ', '1-26-304-2941', '云南省-曲靖市-麒麟区', 1);
INSERT INTO `qmgx_region` VALUES (2942, '530321', '马龙县', 304, 3, 0, 'Malong Xian', 'MLO', '1-26-304-2942', '云南省-曲靖市-马龙县', 1);
INSERT INTO `qmgx_region` VALUES (2943, '530322', '陆良县', 304, 3, 0, 'Luliang Xian', 'LLX', '1-26-304-2943', '云南省-曲靖市-陆良县', 1);
INSERT INTO `qmgx_region` VALUES (2944, '530323', '师宗县', 304, 3, 0, 'Shizong Xian', 'SZD', '1-26-304-2944', '云南省-曲靖市-师宗县', 1);
INSERT INTO `qmgx_region` VALUES (2945, '530324', '罗平县', 304, 3, 0, 'Luoping Xian', 'LPX', '1-26-304-2945', '云南省-曲靖市-罗平县', 1);
INSERT INTO `qmgx_region` VALUES (2946, '530325', '富源县', 304, 3, 0, 'Fuyuan Xian', 'FYD', '1-26-304-2946', '云南省-曲靖市-富源县', 1);
INSERT INTO `qmgx_region` VALUES (2947, '530326', '会泽县', 304, 3, 0, 'Huize Xian', 'HUZ', '1-26-304-2947', '云南省-曲靖市-会泽县', 1);
INSERT INTO `qmgx_region` VALUES (2948, '530328', '沾益县', 304, 3, 0, 'Zhanyi Xian', 'ZYD', '1-26-304-2948', '云南省-曲靖市-沾益县', 1);
INSERT INTO `qmgx_region` VALUES (2949, '530381', '宣威市', 304, 3, 0, 'Xuanwei Shi', 'XWS', '1-26-304-2949', '云南省-曲靖市-宣威市', 1);
INSERT INTO `qmgx_region` VALUES (2950, '530401', '市辖区', 305, 3, 0, 'Shixiaqu', '2', '1-26-305-2950', '云南省-玉溪市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2951, '530402', '红塔区', 305, 3, 0, 'Hongta Qu', 'HTA', '1-26-305-2951', '云南省-玉溪市-红塔区', 1);
INSERT INTO `qmgx_region` VALUES (2952, '530421', '江川县', 305, 3, 0, 'Jiangchuan Xian', 'JGC', '1-26-305-2952', '云南省-玉溪市-江川县', 1);
INSERT INTO `qmgx_region` VALUES (2953, '530422', '澄江县', 305, 3, 0, 'Chengjiang Xian', 'CGJ', '1-26-305-2953', '云南省-玉溪市-澄江县', 1);
INSERT INTO `qmgx_region` VALUES (2954, '530423', '通海县', 305, 3, 0, 'Tonghai Xian', 'THI', '1-26-305-2954', '云南省-玉溪市-通海县', 1);
INSERT INTO `qmgx_region` VALUES (2955, '530424', '华宁县', 305, 3, 0, 'Huaning Xian', 'HND', '1-26-305-2955', '云南省-玉溪市-华宁县', 1);
INSERT INTO `qmgx_region` VALUES (2956, '530425', '易门县', 305, 3, 0, 'Yimen Xian', 'YMD', '1-26-305-2956', '云南省-玉溪市-易门县', 1);
INSERT INTO `qmgx_region` VALUES (2957, '530426', '峨山彝族自治县', 305, 3, 0, 'Eshan Yizu Zizhixian', 'ESN', '1-26-305-2957', '云南省-玉溪市-峨山彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2958, '530427', '新平彝族傣族自治县', 305, 3, 0, 'Xinping Yizu Daizu Zizhixian', 'XNP', '1-26-305-2958', '云南省-玉溪市-新平彝族傣族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2959, '530428', '元江哈尼族彝族傣族自治县', 305, 3, 0, 'Yuanjiang Hanizu Yizu Daizu Zizhixian', 'YJA', '1-26-305-2959', '云南省-玉溪市-元江哈尼族彝族傣族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2960, '530501', '市辖区', 306, 3, 0, '1', '2', '1-26-306-2960', '云南省-保山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2961, '530502', '隆阳区', 306, 3, 0, 'Longyang Qu', '2', '1-26-306-2961', '云南省-保山市-隆阳区', 1);
INSERT INTO `qmgx_region` VALUES (2962, '530521', '施甸县', 306, 3, 0, 'Shidian Xian', '2', '1-26-306-2962', '云南省-保山市-施甸县', 1);
INSERT INTO `qmgx_region` VALUES (2963, '530522', '腾冲县', 306, 3, 0, 'Tengchong Xian', '2', '1-26-306-2963', '云南省-保山市-腾冲县', 1);
INSERT INTO `qmgx_region` VALUES (2964, '530523', '龙陵县', 306, 3, 0, 'Longling Xian', '2', '1-26-306-2964', '云南省-保山市-龙陵县', 1);
INSERT INTO `qmgx_region` VALUES (2965, '530524', '昌宁县', 306, 3, 0, 'Changning Xian', '2', '1-26-306-2965', '云南省-保山市-昌宁县', 1);
INSERT INTO `qmgx_region` VALUES (2966, '530601', '市辖区', 307, 3, 0, '1', '2', '1-26-307-2966', '云南省-昭通市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2967, '530602', '昭阳区', 307, 3, 0, 'Zhaoyang Qu', '2', '1-26-307-2967', '云南省-昭通市-昭阳区', 1);
INSERT INTO `qmgx_region` VALUES (2968, '530621', '鲁甸县', 307, 3, 0, 'Ludian Xian', '2', '1-26-307-2968', '云南省-昭通市-鲁甸县', 1);
INSERT INTO `qmgx_region` VALUES (2969, '530622', '巧家县', 307, 3, 0, 'Qiaojia Xian', '2', '1-26-307-2969', '云南省-昭通市-巧家县', 1);
INSERT INTO `qmgx_region` VALUES (2970, '530623', '盐津县', 307, 3, 0, 'Yanjin Xian', '2', '1-26-307-2970', '云南省-昭通市-盐津县', 1);
INSERT INTO `qmgx_region` VALUES (2971, '530624', '大关县', 307, 3, 0, 'Daguan Xian', '2', '1-26-307-2971', '云南省-昭通市-大关县', 1);
INSERT INTO `qmgx_region` VALUES (2972, '530625', '永善县', 307, 3, 0, 'Yongshan Xian', '2', '1-26-307-2972', '云南省-昭通市-永善县', 1);
INSERT INTO `qmgx_region` VALUES (2973, '530626', '绥江县', 307, 3, 0, 'Suijiang Xian', '2', '1-26-307-2973', '云南省-昭通市-绥江县', 1);
INSERT INTO `qmgx_region` VALUES (2974, '530627', '镇雄县', 307, 3, 0, 'Zhenxiong Xian', '2', '1-26-307-2974', '云南省-昭通市-镇雄县', 1);
INSERT INTO `qmgx_region` VALUES (2975, '530628', '彝良县', 307, 3, 0, 'Yiliang Xian', '2', '1-26-307-2975', '云南省-昭通市-彝良县', 1);
INSERT INTO `qmgx_region` VALUES (2976, '530629', '威信县', 307, 3, 0, 'Weixin Xian', '2', '1-26-307-2976', '云南省-昭通市-威信县', 1);
INSERT INTO `qmgx_region` VALUES (2977, '530630', '水富县', 307, 3, 0, 'Shuifu Xian ', '2', '1-26-307-2977', '云南省-昭通市-水富县', 1);
INSERT INTO `qmgx_region` VALUES (2978, '530701', '市辖区', 308, 3, 0, '1', '2', '1-26-308-2978', '云南省-丽江市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2979, '530702', '古城区', 308, 3, 0, 'Gucheng Qu', '2', '1-26-308-2979', '云南省-丽江市-古城区', 1);
INSERT INTO `qmgx_region` VALUES (2980, '530721', '玉龙纳西族自治县', 308, 3, 0, 'Yulongnaxizuzizhi Xian', '2', '1-26-308-2980', '云南省-丽江市-玉龙纳西族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2981, '530722', '永胜县', 308, 3, 0, 'Yongsheng Xian', '2', '1-26-308-2981', '云南省-丽江市-永胜县', 1);
INSERT INTO `qmgx_region` VALUES (2982, '530723', '华坪县', 308, 3, 0, 'Huaping Xian', '2', '1-26-308-2982', '云南省-丽江市-华坪县', 1);
INSERT INTO `qmgx_region` VALUES (2983, '530724', '宁蒗彝族自治县', 308, 3, 0, 'Ninglang Yizu Zizhixian', '2', '1-26-308-2983', '云南省-丽江市-宁蒗彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2984, '530801', '市辖区', 309, 3, 0, '1', '2', '1-26-309-2984', '云南省-普洱市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2985, '530802', '思茅区', 309, 3, 0, 'Simao Qu', '2', '1-26-309-2985', '云南省-普洱市-思茅区', 1);
INSERT INTO `qmgx_region` VALUES (2986, '530821', '宁洱哈尼族彝族自治县', 309, 3, 0, 'Pu,er Hanizu Yizu Zizhixian', '2', '1-26-309-2986', '云南省-普洱市-宁洱哈尼族彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2987, '530822', '墨江哈尼族自治县', 309, 3, 0, 'Mojiang Hanizu Zizhixian', '2', '1-26-309-2987', '云南省-普洱市-墨江哈尼族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2988, '530823', '景东彝族自治县', 309, 3, 0, 'Jingdong Yizu Zizhixian', '2', '1-26-309-2988', '云南省-普洱市-景东彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2989, '530824', '景谷傣族彝族自治县', 309, 3, 0, 'Jinggu Daizu Yizu Zizhixian', '2', '1-26-309-2989', '云南省-普洱市-景谷傣族彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2990, '530825', '镇沅彝族哈尼族拉祜族自治县', 309, 3, 0, 'Zhenyuan Yizu Hanizu Lahuzu Zizhixian', '2', '1-26-309-2990', '云南省-普洱市-镇沅彝族哈尼族拉祜族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2991, '530826', '江城哈尼族彝族自治县', 309, 3, 0, 'Jiangcheng Hanizu Yizu Zizhixian', '2', '1-26-309-2991', '云南省-普洱市-江城哈尼族彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2992, '530827', '孟连傣族拉祜族佤族自治县', 309, 3, 0, 'Menglian Daizu Lahuzu Vazu Zizixian', '2', '1-26-309-2992', '云南省-普洱市-孟连傣族拉祜族佤族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2993, '530828', '澜沧拉祜族自治县', 309, 3, 0, 'Lancang Lahuzu Zizhixian', '2', '1-26-309-2993', '云南省-普洱市-澜沧拉祜族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2994, '530829', '西盟佤族自治县', 309, 3, 0, 'Ximeng Vazu Zizhixian', '2', '1-26-309-2994', '云南省-普洱市-西盟佤族自治县', 1);
INSERT INTO `qmgx_region` VALUES (2995, '530901', '市辖区', 310, 3, 0, '1', '2', '1-26-310-2995', '云南省-临沧市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (2996, '530902', '临翔区', 310, 3, 0, 'Linxiang Qu', '2', '1-26-310-2996', '云南省-临沧市-临翔区', 1);
INSERT INTO `qmgx_region` VALUES (2997, '530921', '凤庆县', 310, 3, 0, 'Fengqing Xian', '2', '1-26-310-2997', '云南省-临沧市-凤庆县', 1);
INSERT INTO `qmgx_region` VALUES (2998, '530922', '云县', 310, 3, 0, 'Yun Xian', '2', '1-26-310-2998', '云南省-临沧市-云县', 1);
INSERT INTO `qmgx_region` VALUES (2999, '530923', '永德县', 310, 3, 0, 'Yongde Xian', '2', '1-26-310-2999', '云南省-临沧市-永德县', 1);
INSERT INTO `qmgx_region` VALUES (3000, '530924', '镇康县', 310, 3, 0, 'Zhenkang Xian', '2', '1-26-310-3000', '云南省-临沧市-镇康县', 1);
INSERT INTO `qmgx_region` VALUES (3001, '530925', '双江拉祜族佤族布朗族傣族自治县', 310, 3, 0, 'Shuangjiang Lahuzu Vazu Bulangzu Daizu Zizhixian', '2', '1-26-310-3001', '云南省-临沧市-双江拉祜族佤族布朗族傣族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3002, '530926', '耿马傣族佤族自治县', 310, 3, 0, 'Gengma Daizu Vazu Zizhixian', '2', '1-26-310-3002', '云南省-临沧市-耿马傣族佤族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3003, '530927', '沧源佤族自治县', 310, 3, 0, 'Cangyuan Vazu Zizhixian', '2', '1-26-310-3003', '云南省-临沧市-沧源佤族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3004, '532301', '楚雄市', 311, 3, 0, 'Chuxiong Shi', 'CXS', '1-26-311-3004', '云南省-楚雄彝族自治州-楚雄市', 1);
INSERT INTO `qmgx_region` VALUES (3005, '532322', '双柏县', 311, 3, 0, 'Shuangbai Xian', 'SBA', '1-26-311-3005', '云南省-楚雄彝族自治州-双柏县', 1);
INSERT INTO `qmgx_region` VALUES (3006, '532323', '牟定县', 311, 3, 0, 'Mouding Xian', 'MDI', '1-26-311-3006', '云南省-楚雄彝族自治州-牟定县', 1);
INSERT INTO `qmgx_region` VALUES (3007, '532324', '南华县', 311, 3, 0, 'Nanhua Xian', 'NHA', '1-26-311-3007', '云南省-楚雄彝族自治州-南华县', 1);
INSERT INTO `qmgx_region` VALUES (3008, '532325', '姚安县', 311, 3, 0, 'Yao,an Xian', 'YOA', '1-26-311-3008', '云南省-楚雄彝族自治州-姚安县', 1);
INSERT INTO `qmgx_region` VALUES (3009, '532326', '大姚县', 311, 3, 0, 'Dayao Xian', 'DYO', '1-26-311-3009', '云南省-楚雄彝族自治州-大姚县', 1);
INSERT INTO `qmgx_region` VALUES (3010, '532327', '永仁县', 311, 3, 0, 'Yongren Xian', 'YRN', '1-26-311-3010', '云南省-楚雄彝族自治州-永仁县', 1);
INSERT INTO `qmgx_region` VALUES (3011, '532328', '元谋县', 311, 3, 0, 'Yuanmou Xian', 'YMO', '1-26-311-3011', '云南省-楚雄彝族自治州-元谋县', 1);
INSERT INTO `qmgx_region` VALUES (3012, '532329', '武定县', 311, 3, 0, 'Wuding Xian', 'WDX', '1-26-311-3012', '云南省-楚雄彝族自治州-武定县', 1);
INSERT INTO `qmgx_region` VALUES (3013, '532331', '禄丰县', 311, 3, 0, 'Lufeng Xian', 'LFX', '1-26-311-3013', '云南省-楚雄彝族自治州-禄丰县', 1);
INSERT INTO `qmgx_region` VALUES (3014, '532501', '个旧市', 312, 3, 0, 'Gejiu Shi', 'GJU', '1-26-312-3014', '云南省-红河哈尼族彝族自治州-个旧市', 1);
INSERT INTO `qmgx_region` VALUES (3015, '532502', '开远市', 312, 3, 0, 'Kaiyuan Shi', 'KYD', '1-26-312-3015', '云南省-红河哈尼族彝族自治州-开远市', 1);
INSERT INTO `qmgx_region` VALUES (3016, '532503', '蒙自市', 312, 3, 0, 'Mengzi Xian', '2', '1-26-312-3016', '云南省-红河哈尼族彝族自治州-蒙自市', 1);
INSERT INTO `qmgx_region` VALUES (3017, '532523', '屏边苗族自治县', 312, 3, 0, 'Pingbian Miaozu Zizhixian', 'PBN', '1-26-312-3017', '云南省-红河哈尼族彝族自治州-屏边苗族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3018, '532524', '建水县', 312, 3, 0, 'Jianshui Xian', 'JSD', '1-26-312-3018', '云南省-红河哈尼族彝族自治州-建水县', 1);
INSERT INTO `qmgx_region` VALUES (3019, '532525', '石屏县', 312, 3, 0, 'Shiping Xian', 'SPG', '1-26-312-3019', '云南省-红河哈尼族彝族自治州-石屏县', 1);
INSERT INTO `qmgx_region` VALUES (3020, '532526', '弥勒县', 312, 3, 0, 'Mile Xian', 'MIL', '1-26-312-3020', '云南省-红河哈尼族彝族自治州-弥勒县', 1);
INSERT INTO `qmgx_region` VALUES (3021, '532527', '泸西县', 312, 3, 0, 'Luxi Xian', 'LXD', '1-26-312-3021', '云南省-红河哈尼族彝族自治州-泸西县', 1);
INSERT INTO `qmgx_region` VALUES (3022, '532528', '元阳县', 312, 3, 0, 'Yuanyang Xian', 'YYD', '1-26-312-3022', '云南省-红河哈尼族彝族自治州-元阳县', 1);
INSERT INTO `qmgx_region` VALUES (3023, '532529', '红河县', 312, 3, 0, 'Honghe Xian', 'HHX', '1-26-312-3023', '云南省-红河哈尼族彝族自治州-红河县', 1);
INSERT INTO `qmgx_region` VALUES (3024, '532530', '金平苗族瑶族傣族自治县', 312, 3, 0, 'Jinping Miaozu Yaozu Daizu Zizhixian', 'JNP', '1-26-312-3024', '云南省-红河哈尼族彝族自治州-金平苗族瑶族傣族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3025, '532531', '绿春县', 312, 3, 0, 'Lvchun Xian', 'LCX', '1-26-312-3025', '云南省-红河哈尼族彝族自治州-绿春县', 1);
INSERT INTO `qmgx_region` VALUES (3026, '532532', '河口瑶族自治县', 312, 3, 0, 'Hekou Yaozu Zizhixian', 'HKM', '1-26-312-3026', '云南省-红河哈尼族彝族自治州-河口瑶族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3027, '532621', '文山县', 313, 3, 0, 'Wenshan Xian', 'WES', '1-26-313-3027', '云南省-文山壮族苗族自治州-文山县', 1);
INSERT INTO `qmgx_region` VALUES (3028, '532622', '砚山县', 313, 3, 0, 'Yanshan Xian', 'YSD', '1-26-313-3028', '云南省-文山壮族苗族自治州-砚山县', 1);
INSERT INTO `qmgx_region` VALUES (3029, '532623', '西畴县', 313, 3, 0, 'Xichou Xian', 'XIC', '1-26-313-3029', '云南省-文山壮族苗族自治州-西畴县', 1);
INSERT INTO `qmgx_region` VALUES (3030, '532624', '麻栗坡县', 313, 3, 0, 'Malipo Xian', 'MLP', '1-26-313-3030', '云南省-文山壮族苗族自治州-麻栗坡县', 1);
INSERT INTO `qmgx_region` VALUES (3031, '532625', '马关县', 313, 3, 0, 'Maguan Xian', 'MGN', '1-26-313-3031', '云南省-文山壮族苗族自治州-马关县', 1);
INSERT INTO `qmgx_region` VALUES (3032, '532626', '丘北县', 313, 3, 0, 'Qiubei Xian', 'QBE', '1-26-313-3032', '云南省-文山壮族苗族自治州-丘北县', 1);
INSERT INTO `qmgx_region` VALUES (3033, '532627', '广南县', 313, 3, 0, 'Guangnan Xian', 'GGN', '1-26-313-3033', '云南省-文山壮族苗族自治州-广南县', 1);
INSERT INTO `qmgx_region` VALUES (3034, '532628', '富宁县', 313, 3, 0, 'Funing Xian', 'FND', '1-26-313-3034', '云南省-文山壮族苗族自治州-富宁县', 1);
INSERT INTO `qmgx_region` VALUES (3035, '532801', '景洪市', 314, 3, 0, 'Jinghong Shi', 'JHG', '1-26-314-3035', '云南省-西双版纳傣族自治州-景洪市', 1);
INSERT INTO `qmgx_region` VALUES (3036, '532822', '勐海县', 314, 3, 0, 'Menghai Xian', 'MHI', '1-26-314-3036', '云南省-西双版纳傣族自治州-勐海县', 1);
INSERT INTO `qmgx_region` VALUES (3037, '532823', '勐腊县', 314, 3, 0, 'Mengla Xian', 'MLA', '1-26-314-3037', '云南省-西双版纳傣族自治州-勐腊县', 1);
INSERT INTO `qmgx_region` VALUES (3038, '532901', '大理市', 315, 3, 0, 'Dali Shi', 'DLS', '1-26-315-3038', '云南省-大理白族自治州-大理市', 1);
INSERT INTO `qmgx_region` VALUES (3039, '532922', '漾濞彝族自治县', 315, 3, 0, 'Yangbi Yizu Zizhixian', 'YGB', '1-26-315-3039', '云南省-大理白族自治州-漾濞彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3040, '532923', '祥云县', 315, 3, 0, 'Xiangyun Xian', 'XYD', '1-26-315-3040', '云南省-大理白族自治州-祥云县', 1);
INSERT INTO `qmgx_region` VALUES (3041, '532924', '宾川县', 315, 3, 0, 'Binchuan Xian', 'BCD', '1-26-315-3041', '云南省-大理白族自治州-宾川县', 1);
INSERT INTO `qmgx_region` VALUES (3042, '532925', '弥渡县', 315, 3, 0, 'Midu Xian', 'MDU', '1-26-315-3042', '云南省-大理白族自治州-弥渡县', 1);
INSERT INTO `qmgx_region` VALUES (3043, '532926', '南涧彝族自治县', 315, 3, 0, 'Nanjian Yizu Zizhixian', 'NNJ', '1-26-315-3043', '云南省-大理白族自治州-南涧彝族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3044, '532927', '巍山彝族回族自治县', 315, 3, 0, 'Weishan Yizu Huizu Zizhixian', 'WSY', '1-26-315-3044', '云南省-大理白族自治州-巍山彝族回族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3045, '532928', '永平县', 315, 3, 0, 'Yongping Xian', 'YPX', '1-26-315-3045', '云南省-大理白族自治州-永平县', 1);
INSERT INTO `qmgx_region` VALUES (3046, '532929', '云龙县', 315, 3, 0, 'Yunlong Xian', 'YLO', '1-26-315-3046', '云南省-大理白族自治州-云龙县', 1);
INSERT INTO `qmgx_region` VALUES (3047, '532930', '洱源县', 315, 3, 0, 'Eryuan Xian', 'EYN', '1-26-315-3047', '云南省-大理白族自治州-洱源县', 1);
INSERT INTO `qmgx_region` VALUES (3048, '532931', '剑川县', 315, 3, 0, 'Jianchuan Xian', 'JIC', '1-26-315-3048', '云南省-大理白族自治州-剑川县', 1);
INSERT INTO `qmgx_region` VALUES (3049, '532932', '鹤庆县', 315, 3, 0, 'Heqing Xian', 'HQG', '1-26-315-3049', '云南省-大理白族自治州-鹤庆县', 1);
INSERT INTO `qmgx_region` VALUES (3050, '533102', '瑞丽市', 316, 3, 0, 'Ruili Shi', 'RUI', '1-26-316-3050', '云南省-德宏傣族景颇族自治州-瑞丽市', 1);
INSERT INTO `qmgx_region` VALUES (3051, '533103', '芒市', 316, 3, 0, 'Luxi Shi', '2', '1-26-316-3051', '云南省-德宏傣族景颇族自治州-芒市', 1);
INSERT INTO `qmgx_region` VALUES (3052, '533122', '梁河县', 316, 3, 0, 'Lianghe Xian', 'LHD', '1-26-316-3052', '云南省-德宏傣族景颇族自治州-梁河县', 1);
INSERT INTO `qmgx_region` VALUES (3053, '533123', '盈江县', 316, 3, 0, 'Yingjiang Xian', 'YGJ', '1-26-316-3053', '云南省-德宏傣族景颇族自治州-盈江县', 1);
INSERT INTO `qmgx_region` VALUES (3054, '533124', '陇川县', 316, 3, 0, 'Longchuan Xian', 'LCN', '1-26-316-3054', '云南省-德宏傣族景颇族自治州-陇川县', 1);
INSERT INTO `qmgx_region` VALUES (3055, '533321', '泸水县', 317, 3, 0, 'Lushui Xian', 'LSX', '1-26-317-3055', '云南省-怒江傈僳族自治州-泸水县', 1);
INSERT INTO `qmgx_region` VALUES (3056, '533323', '福贡县', 317, 3, 0, 'Fugong Xian', 'FGO', '1-26-317-3056', '云南省-怒江傈僳族自治州-福贡县', 1);
INSERT INTO `qmgx_region` VALUES (3057, '533324', '贡山独龙族怒族自治县', 317, 3, 0, 'Gongshan Dulongzu Nuzu Zizhixian', 'GSN', '1-26-317-3057', '云南省-怒江傈僳族自治州-贡山独龙族怒族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3058, '533325', '兰坪白族普米族自治县', 317, 3, 0, 'Lanping Baizu Pumizu Zizhixian', 'LPG', '1-26-317-3058', '云南省-怒江傈僳族自治州-兰坪白族普米族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3059, '533421', '香格里拉县', 318, 3, 0, 'Xianggelila Xian', '2', '1-26-318-3059', '云南省-迪庆藏族自治州-香格里拉县', 1);
INSERT INTO `qmgx_region` VALUES (3060, '533422', '德钦县', 318, 3, 0, 'Deqen Xian', 'DQN', '1-26-318-3060', '云南省-迪庆藏族自治州-德钦县', 1);
INSERT INTO `qmgx_region` VALUES (3061, '533423', '维西傈僳族自治县', 318, 3, 0, 'Weixi Lisuzu Zizhixian', 'WXI', '1-26-318-3061', '云南省-迪庆藏族自治州-维西傈僳族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3062, '540101', '市辖区', 319, 3, 0, 'Shixiaqu', '2', '1-27-319-3062', '西藏自治区-拉萨市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3063, '540102', '城关区', 319, 3, 0, 'Chengguang Qu', 'CGN', '1-27-319-3063', '西藏自治区-拉萨市-城关区', 1);
INSERT INTO `qmgx_region` VALUES (3064, '540121', '林周县', 319, 3, 0, 'Lhvnzhub Xian', 'LZB', '1-27-319-3064', '西藏自治区-拉萨市-林周县', 1);
INSERT INTO `qmgx_region` VALUES (3065, '540122', '当雄县', 319, 3, 0, 'Damxung Xian', 'DAM', '1-27-319-3065', '西藏自治区-拉萨市-当雄县', 1);
INSERT INTO `qmgx_region` VALUES (3066, '540123', '尼木县', 319, 3, 0, 'Nyemo Xian', 'NYE', '1-27-319-3066', '西藏自治区-拉萨市-尼木县', 1);
INSERT INTO `qmgx_region` VALUES (3067, '540124', '曲水县', 319, 3, 0, 'Qvxv Xian', 'QUX', '1-27-319-3067', '西藏自治区-拉萨市-曲水县', 1);
INSERT INTO `qmgx_region` VALUES (3068, '540125', '堆龙德庆县', 319, 3, 0, 'Doilungdeqen Xian', 'DOI', '1-27-319-3068', '西藏自治区-拉萨市-堆龙德庆县', 1);
INSERT INTO `qmgx_region` VALUES (3069, '540126', '达孜县', 319, 3, 0, 'Dagze Xian', 'DAG', '1-27-319-3069', '西藏自治区-拉萨市-达孜县', 1);
INSERT INTO `qmgx_region` VALUES (3070, '540127', '墨竹工卡县', 319, 3, 0, 'Maizhokunggar Xian', 'MAI', '1-27-319-3070', '西藏自治区-拉萨市-墨竹工卡县', 1);
INSERT INTO `qmgx_region` VALUES (3071, '542121', '昌都县', 320, 3, 0, 'Qamdo Xian', 'QAX', '1-27-320-3071', '西藏自治区-昌都地区-昌都县', 1);
INSERT INTO `qmgx_region` VALUES (3072, '542122', '江达县', 320, 3, 0, 'Jomda Xian', 'JOM', '1-27-320-3072', '西藏自治区-昌都地区-江达县', 1);
INSERT INTO `qmgx_region` VALUES (3073, '542123', '贡觉县', 320, 3, 0, 'Konjo Xian', 'KON', '1-27-320-3073', '西藏自治区-昌都地区-贡觉县', 1);
INSERT INTO `qmgx_region` VALUES (3074, '542124', '类乌齐县', 320, 3, 0, 'Riwoqe Xian', 'RIW', '1-27-320-3074', '西藏自治区-昌都地区-类乌齐县', 1);
INSERT INTO `qmgx_region` VALUES (3075, '542125', '丁青县', 320, 3, 0, 'Dengqen Xian', 'DEN', '1-27-320-3075', '西藏自治区-昌都地区-丁青县', 1);
INSERT INTO `qmgx_region` VALUES (3076, '542126', '察雅县', 320, 3, 0, 'Chagyab Xian', 'CHA', '1-27-320-3076', '西藏自治区-昌都地区-察雅县', 1);
INSERT INTO `qmgx_region` VALUES (3077, '542127', '八宿县', 320, 3, 0, 'Baxoi Xian', 'BAX', '1-27-320-3077', '西藏自治区-昌都地区-八宿县', 1);
INSERT INTO `qmgx_region` VALUES (3078, '542128', '左贡县', 320, 3, 0, 'Zogang Xian', 'ZOX', '1-27-320-3078', '西藏自治区-昌都地区-左贡县', 1);
INSERT INTO `qmgx_region` VALUES (3079, '542129', '芒康县', 320, 3, 0, 'Mangkam Xian', 'MAN', '1-27-320-3079', '西藏自治区-昌都地区-芒康县', 1);
INSERT INTO `qmgx_region` VALUES (3080, '542132', '洛隆县', 320, 3, 0, 'Lhorong Xian', 'LHO', '1-27-320-3080', '西藏自治区-昌都地区-洛隆县', 1);
INSERT INTO `qmgx_region` VALUES (3081, '542133', '边坝县', 320, 3, 0, 'Banbar Xian', 'BAN', '1-27-320-3081', '西藏自治区-昌都地区-边坝县', 1);
INSERT INTO `qmgx_region` VALUES (3082, '542221', '乃东县', 321, 3, 0, 'Nedong Xian', 'NED', '1-27-321-3082', '西藏自治区-山南地区-乃东县', 1);
INSERT INTO `qmgx_region` VALUES (3083, '542222', '扎囊县', 321, 3, 0, 'Chanang(Chatang) Xian', 'CNG', '1-27-321-3083', '西藏自治区-山南地区-扎囊县', 1);
INSERT INTO `qmgx_region` VALUES (3084, '542223', '贡嘎县', 321, 3, 0, 'Gonggar Xian', 'GON', '1-27-321-3084', '西藏自治区-山南地区-贡嘎县', 1);
INSERT INTO `qmgx_region` VALUES (3085, '542224', '桑日县', 321, 3, 0, 'Sangri Xian', 'SRI', '1-27-321-3085', '西藏自治区-山南地区-桑日县', 1);
INSERT INTO `qmgx_region` VALUES (3086, '542225', '琼结县', 321, 3, 0, 'Qonggyai Xian', 'QON', '1-27-321-3086', '西藏自治区-山南地区-琼结县', 1);
INSERT INTO `qmgx_region` VALUES (3087, '542226', '曲松县', 321, 3, 0, 'Qusum Xian', 'QUS', '1-27-321-3087', '西藏自治区-山南地区-曲松县', 1);
INSERT INTO `qmgx_region` VALUES (3088, '542227', '措美县', 321, 3, 0, 'Comai Xian', 'COM', '1-27-321-3088', '西藏自治区-山南地区-措美县', 1);
INSERT INTO `qmgx_region` VALUES (3089, '542228', '洛扎县', 321, 3, 0, 'Lhozhag Xian', 'LHX', '1-27-321-3089', '西藏自治区-山南地区-洛扎县', 1);
INSERT INTO `qmgx_region` VALUES (3090, '542229', '加查县', 321, 3, 0, 'Gyaca Xian', 'GYA', '1-27-321-3090', '西藏自治区-山南地区-加查县', 1);
INSERT INTO `qmgx_region` VALUES (3091, '542231', '隆子县', 321, 3, 0, 'Lhvnze Xian', 'LHZ', '1-27-321-3091', '西藏自治区-山南地区-隆子县', 1);
INSERT INTO `qmgx_region` VALUES (3092, '542232', '错那县', 321, 3, 0, 'Cona Xian', 'CON', '1-27-321-3092', '西藏自治区-山南地区-错那县', 1);
INSERT INTO `qmgx_region` VALUES (3093, '542233', '浪卡子县', 321, 3, 0, 'Nagarze Xian', 'NAX', '1-27-321-3093', '西藏自治区-山南地区-浪卡子县', 1);
INSERT INTO `qmgx_region` VALUES (3094, '542301', '日喀则市', 322, 3, 0, 'Xigaze Shi', 'XIG', '1-27-322-3094', '西藏自治区-日喀则地区-日喀则市', 1);
INSERT INTO `qmgx_region` VALUES (3095, '542322', '南木林县', 322, 3, 0, 'Namling Xian', 'NAM', '1-27-322-3095', '西藏自治区-日喀则地区-南木林县', 1);
INSERT INTO `qmgx_region` VALUES (3096, '542323', '江孜县', 322, 3, 0, 'Gyangze Xian', 'GYZ', '1-27-322-3096', '西藏自治区-日喀则地区-江孜县', 1);
INSERT INTO `qmgx_region` VALUES (3097, '542324', '定日县', 322, 3, 0, 'Tingri Xian', 'TIN', '1-27-322-3097', '西藏自治区-日喀则地区-定日县', 1);
INSERT INTO `qmgx_region` VALUES (3098, '542325', '萨迦县', 322, 3, 0, 'Sa,gya Xian', 'SGX', '1-27-322-3098', '西藏自治区-日喀则地区-萨迦县', 1);
INSERT INTO `qmgx_region` VALUES (3099, '542326', '拉孜县', 322, 3, 0, 'Lhaze Xian', 'LAZ', '1-27-322-3099', '西藏自治区-日喀则地区-拉孜县', 1);
INSERT INTO `qmgx_region` VALUES (3100, '542327', '昂仁县', 322, 3, 0, 'Ngamring Xian', 'NGA', '1-27-322-3100', '西藏自治区-日喀则地区-昂仁县', 1);
INSERT INTO `qmgx_region` VALUES (3101, '542328', '谢通门县', 322, 3, 0, 'Xaitongmoin Xian', 'XTM', '1-27-322-3101', '西藏自治区-日喀则地区-谢通门县', 1);
INSERT INTO `qmgx_region` VALUES (3102, '542329', '白朗县', 322, 3, 0, 'Bainang Xian', 'BAI', '1-27-322-3102', '西藏自治区-日喀则地区-白朗县', 1);
INSERT INTO `qmgx_region` VALUES (3103, '542330', '仁布县', 322, 3, 0, 'Rinbung Xian', 'RIN', '1-27-322-3103', '西藏自治区-日喀则地区-仁布县', 1);
INSERT INTO `qmgx_region` VALUES (3104, '542331', '康马县', 322, 3, 0, 'Kangmar Xian', 'KAN', '1-27-322-3104', '西藏自治区-日喀则地区-康马县', 1);
INSERT INTO `qmgx_region` VALUES (3105, '542332', '定结县', 322, 3, 0, 'Dinggye Xian', 'DIN', '1-27-322-3105', '西藏自治区-日喀则地区-定结县', 1);
INSERT INTO `qmgx_region` VALUES (3106, '542333', '仲巴县', 322, 3, 0, 'Zhongba Xian', 'ZHB', '1-27-322-3106', '西藏自治区-日喀则地区-仲巴县', 1);
INSERT INTO `qmgx_region` VALUES (3107, '542334', '亚东县', 322, 3, 0, 'Yadong(Chomo) Xian', 'YDZ', '1-27-322-3107', '西藏自治区-日喀则地区-亚东县', 1);
INSERT INTO `qmgx_region` VALUES (3108, '542335', '吉隆县', 322, 3, 0, 'Gyirong Xian', 'GIR', '1-27-322-3108', '西藏自治区-日喀则地区-吉隆县', 1);
INSERT INTO `qmgx_region` VALUES (3109, '542336', '聂拉木县', 322, 3, 0, 'Nyalam Xian', 'NYA', '1-27-322-3109', '西藏自治区-日喀则地区-聂拉木县', 1);
INSERT INTO `qmgx_region` VALUES (3110, '542337', '萨嘎县', 322, 3, 0, 'Saga Xian', 'SAG', '1-27-322-3110', '西藏自治区-日喀则地区-萨嘎县', 1);
INSERT INTO `qmgx_region` VALUES (3111, '542338', '岗巴县', 322, 3, 0, 'Gamba Xian', 'GAM', '1-27-322-3111', '西藏自治区-日喀则地区-岗巴县', 1);
INSERT INTO `qmgx_region` VALUES (3112, '542421', '那曲县', 323, 3, 0, 'Nagqu Xian', 'NAG', '1-27-323-3112', '西藏自治区-那曲地区-那曲县', 1);
INSERT INTO `qmgx_region` VALUES (3113, '542422', '嘉黎县', 323, 3, 0, 'Lhari Xian', 'LHR', '1-27-323-3113', '西藏自治区-那曲地区-嘉黎县', 1);
INSERT INTO `qmgx_region` VALUES (3114, '542423', '比如县', 323, 3, 0, 'Biru Xian', 'BRU', '1-27-323-3114', '西藏自治区-那曲地区-比如县', 1);
INSERT INTO `qmgx_region` VALUES (3115, '542424', '聂荣县', 323, 3, 0, 'Nyainrong Xian', 'NRO', '1-27-323-3115', '西藏自治区-那曲地区-聂荣县', 1);
INSERT INTO `qmgx_region` VALUES (3116, '542425', '安多县', 323, 3, 0, 'Amdo Xian', 'AMD', '1-27-323-3116', '西藏自治区-那曲地区-安多县', 1);
INSERT INTO `qmgx_region` VALUES (3117, '542426', '申扎县', 323, 3, 0, 'Xainza Xian', 'XZX', '1-27-323-3117', '西藏自治区-那曲地区-申扎县', 1);
INSERT INTO `qmgx_region` VALUES (3118, '542427', '索县', 323, 3, 0, 'Sog Xian', 'SOG', '1-27-323-3118', '西藏自治区-那曲地区-索县', 1);
INSERT INTO `qmgx_region` VALUES (3119, '542428', '班戈县', 323, 3, 0, 'Bangoin Xian', 'BGX', '1-27-323-3119', '西藏自治区-那曲地区-班戈县', 1);
INSERT INTO `qmgx_region` VALUES (3120, '542429', '巴青县', 323, 3, 0, 'Baqen Xian', 'BQE', '1-27-323-3120', '西藏自治区-那曲地区-巴青县', 1);
INSERT INTO `qmgx_region` VALUES (3121, '542430', '尼玛县', 323, 3, 0, 'Nyima Xian', 'NYX', '1-27-323-3121', '西藏自治区-那曲地区-尼玛县', 1);
INSERT INTO `qmgx_region` VALUES (3122, '542521', '普兰县', 324, 3, 0, 'Burang Xian', 'BUR', '1-27-324-3122', '西藏自治区-阿里地区-普兰县', 1);
INSERT INTO `qmgx_region` VALUES (3123, '542522', '札达县', 324, 3, 0, 'Zanda Xian', 'ZAN', '1-27-324-3123', '西藏自治区-阿里地区-札达县', 1);
INSERT INTO `qmgx_region` VALUES (3124, '542523', '噶尔县', 324, 3, 0, 'Gar Xian', 'GAR', '1-27-324-3124', '西藏自治区-阿里地区-噶尔县', 1);
INSERT INTO `qmgx_region` VALUES (3125, '542524', '日土县', 324, 3, 0, 'Rutog Xian', 'RUT', '1-27-324-3125', '西藏自治区-阿里地区-日土县', 1);
INSERT INTO `qmgx_region` VALUES (3126, '542525', '革吉县', 324, 3, 0, 'Ge,gyai Xian', 'GEG', '1-27-324-3126', '西藏自治区-阿里地区-革吉县', 1);
INSERT INTO `qmgx_region` VALUES (3127, '542526', '改则县', 324, 3, 0, 'Gerze Xian', 'GER', '1-27-324-3127', '西藏自治区-阿里地区-改则县', 1);
INSERT INTO `qmgx_region` VALUES (3128, '542527', '措勤县', 324, 3, 0, 'Coqen Xian', 'COQ', '1-27-324-3128', '西藏自治区-阿里地区-措勤县', 1);
INSERT INTO `qmgx_region` VALUES (3129, '542621', '林芝县', 325, 3, 0, 'Nyingchi Xian', 'NYI', '1-27-325-3129', '西藏自治区-林芝地区-林芝县', 1);
INSERT INTO `qmgx_region` VALUES (3130, '542622', '工布江达县', 325, 3, 0, 'Gongbo,gyamda Xian', 'GOX', '1-27-325-3130', '西藏自治区-林芝地区-工布江达县', 1);
INSERT INTO `qmgx_region` VALUES (3131, '542623', '米林县', 325, 3, 0, 'Mainling Xian', 'MAX', '1-27-325-3131', '西藏自治区-林芝地区-米林县', 1);
INSERT INTO `qmgx_region` VALUES (3132, '542624', '墨脱县', 325, 3, 0, 'Metog Xian', 'MET', '1-27-325-3132', '西藏自治区-林芝地区-墨脱县', 1);
INSERT INTO `qmgx_region` VALUES (3133, '542625', '波密县', 325, 3, 0, 'Bomi(Bowo) Xian', 'BMI', '1-27-325-3133', '西藏自治区-林芝地区-波密县', 1);
INSERT INTO `qmgx_region` VALUES (3134, '542626', '察隅县', 325, 3, 0, 'Zayv Xian', 'ZAY', '1-27-325-3134', '西藏自治区-林芝地区-察隅县', 1);
INSERT INTO `qmgx_region` VALUES (3135, '542627', '朗县', 325, 3, 0, 'Nang Xian', 'NGX', '1-27-325-3135', '西藏自治区-林芝地区-朗县', 1);
INSERT INTO `qmgx_region` VALUES (3136, '610101', '市辖区', 326, 3, 0, 'Shixiaqu', '2', '1-28-326-3136', '陕西省-西安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3137, '610102', '新城区', 326, 3, 0, 'Xincheng Qu', 'XCK', '1-28-326-3137', '陕西省-西安市-新城区', 1);
INSERT INTO `qmgx_region` VALUES (3138, '610103', '碑林区', 326, 3, 0, 'Beilin Qu', 'BLQ', '1-28-326-3138', '陕西省-西安市-碑林区', 1);
INSERT INTO `qmgx_region` VALUES (3139, '610104', '莲湖区', 326, 3, 0, 'Lianhu Qu', 'LHU', '1-28-326-3139', '陕西省-西安市-莲湖区', 1);
INSERT INTO `qmgx_region` VALUES (3140, '610111', '灞桥区', 326, 3, 0, 'Baqiao Qu', 'BQQ', '1-28-326-3140', '陕西省-西安市-灞桥区', 1);
INSERT INTO `qmgx_region` VALUES (3141, '610112', '未央区', 326, 3, 0, 'Weiyang Qu', '2', '1-28-326-3141', '陕西省-西安市-未央区', 1);
INSERT INTO `qmgx_region` VALUES (3142, '610113', '雁塔区', 326, 3, 0, 'Yanta Qu', 'YTA', '1-28-326-3142', '陕西省-西安市-雁塔区', 1);
INSERT INTO `qmgx_region` VALUES (3143, '610114', '阎良区', 326, 3, 0, 'Yanliang Qu', 'YLQ', '1-28-326-3143', '陕西省-西安市-阎良区', 1);
INSERT INTO `qmgx_region` VALUES (3144, '610115', '临潼区', 326, 3, 0, 'Lintong Qu', 'LTG', '1-28-326-3144', '陕西省-西安市-临潼区', 1);
INSERT INTO `qmgx_region` VALUES (3145, '610116', '长安区', 326, 3, 0, 'Changan Qu', '2', '1-28-326-3145', '陕西省-西安市-长安区', 1);
INSERT INTO `qmgx_region` VALUES (3146, '610122', '蓝田县', 326, 3, 0, 'Lantian Xian', 'LNT', '1-28-326-3146', '陕西省-西安市-蓝田县', 1);
INSERT INTO `qmgx_region` VALUES (3147, '610124', '周至县', 326, 3, 0, 'Zhouzhi Xian', 'ZOZ', '1-28-326-3147', '陕西省-西安市-周至县', 1);
INSERT INTO `qmgx_region` VALUES (3148, '610125', '户县', 326, 3, 0, 'Hu Xian', 'HUX', '1-28-326-3148', '陕西省-西安市-户县', 1);
INSERT INTO `qmgx_region` VALUES (3149, '610126', '高陵县', 326, 3, 0, 'Gaoling Xian', 'GLS', '1-28-326-3149', '陕西省-西安市-高陵县', 1);
INSERT INTO `qmgx_region` VALUES (3150, '610201', '市辖区', 327, 3, 0, 'Shixiaqu', '2', '1-28-327-3150', '陕西省-铜川市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3151, '610202', '王益区', 327, 3, 0, 'Wangyi Qu', '2', '1-28-327-3151', '陕西省-铜川市-王益区', 1);
INSERT INTO `qmgx_region` VALUES (3152, '610203', '印台区', 327, 3, 0, 'Yintai Qu', '2', '1-28-327-3152', '陕西省-铜川市-印台区', 1);
INSERT INTO `qmgx_region` VALUES (3153, '610204', '耀州区', 327, 3, 0, 'Yaozhou Qu', '2', '1-28-327-3153', '陕西省-铜川市-耀州区', 1);
INSERT INTO `qmgx_region` VALUES (3154, '610222', '宜君县', 327, 3, 0, 'Yijun Xian', 'YJU', '1-28-327-3154', '陕西省-铜川市-宜君县', 1);
INSERT INTO `qmgx_region` VALUES (3155, '610301', '市辖区', 328, 3, 0, 'Shixiaqu', '2', '1-28-328-3155', '陕西省-宝鸡市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3156, '610302', '渭滨区', 328, 3, 0, 'Weibin Qu', 'WBQ', '1-28-328-3156', '陕西省-宝鸡市-渭滨区', 1);
INSERT INTO `qmgx_region` VALUES (3157, '610303', '金台区', 328, 3, 0, 'Jintai Qu', 'JTQ', '1-28-328-3157', '陕西省-宝鸡市-金台区', 1);
INSERT INTO `qmgx_region` VALUES (3158, '610304', '陈仓区', 328, 3, 0, 'Chencang Qu', '2', '1-28-328-3158', '陕西省-宝鸡市-陈仓区', 1);
INSERT INTO `qmgx_region` VALUES (3159, '610322', '凤翔县', 328, 3, 0, 'Fengxiang Xian', 'FXG', '1-28-328-3159', '陕西省-宝鸡市-凤翔县', 1);
INSERT INTO `qmgx_region` VALUES (3160, '610323', '岐山县', 328, 3, 0, 'Qishan Xian', 'QIS', '1-28-328-3160', '陕西省-宝鸡市-岐山县', 1);
INSERT INTO `qmgx_region` VALUES (3161, '610324', '扶风县', 328, 3, 0, 'Fufeng Xian', 'FFG', '1-28-328-3161', '陕西省-宝鸡市-扶风县', 1);
INSERT INTO `qmgx_region` VALUES (3162, '610326', '眉县', 328, 3, 0, 'Mei Xian', 'MEI', '1-28-328-3162', '陕西省-宝鸡市-眉县', 1);
INSERT INTO `qmgx_region` VALUES (3163, '610327', '陇县', 328, 3, 0, 'Long Xian', 'LON', '1-28-328-3163', '陕西省-宝鸡市-陇县', 1);
INSERT INTO `qmgx_region` VALUES (3164, '610328', '千阳县', 328, 3, 0, 'Qianyang Xian', 'QNY', '1-28-328-3164', '陕西省-宝鸡市-千阳县', 1);
INSERT INTO `qmgx_region` VALUES (3165, '610329', '麟游县', 328, 3, 0, 'Linyou Xian', 'LYP', '1-28-328-3165', '陕西省-宝鸡市-麟游县', 1);
INSERT INTO `qmgx_region` VALUES (3166, '610330', '凤县', 328, 3, 0, 'Feng Xian', 'FEG', '1-28-328-3166', '陕西省-宝鸡市-凤县', 1);
INSERT INTO `qmgx_region` VALUES (3167, '610331', '太白县', 328, 3, 0, 'Taibai Xian', 'TBA', '1-28-328-3167', '陕西省-宝鸡市-太白县', 1);
INSERT INTO `qmgx_region` VALUES (3168, '610401', '市辖区', 329, 3, 0, 'Shixiaqu', '2', '1-28-329-3168', '陕西省-咸阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3169, '610402', '秦都区', 329, 3, 0, 'Qindu Qu', 'QDU', '1-28-329-3169', '陕西省-咸阳市-秦都区', 1);
INSERT INTO `qmgx_region` VALUES (3170, '610403', '杨陵区', 329, 3, 0, 'Yangling Qu', 'YGL', '1-28-329-3170', '陕西省-咸阳市-杨陵区', 1);
INSERT INTO `qmgx_region` VALUES (3171, '610404', '渭城区', 329, 3, 0, 'Weicheng Qu', 'WIC', '1-28-329-3171', '陕西省-咸阳市-渭城区', 1);
INSERT INTO `qmgx_region` VALUES (3172, '610422', '三原县', 329, 3, 0, 'Sanyuan Xian', 'SYN', '1-28-329-3172', '陕西省-咸阳市-三原县', 1);
INSERT INTO `qmgx_region` VALUES (3173, '610423', '泾阳县', 329, 3, 0, 'Jingyang Xian', 'JGY', '1-28-329-3173', '陕西省-咸阳市-泾阳县', 1);
INSERT INTO `qmgx_region` VALUES (3174, '610424', '乾县', 329, 3, 0, 'Qian Xian', 'QIA', '1-28-329-3174', '陕西省-咸阳市-乾县', 1);
INSERT INTO `qmgx_region` VALUES (3175, '610425', '礼泉县', 329, 3, 0, 'Liquan Xian', 'LIQ', '1-28-329-3175', '陕西省-咸阳市-礼泉县', 1);
INSERT INTO `qmgx_region` VALUES (3176, '610426', '永寿县', 329, 3, 0, 'Yongshou Xian', 'YSH', '1-28-329-3176', '陕西省-咸阳市-永寿县', 1);
INSERT INTO `qmgx_region` VALUES (3177, '610427', '彬县', 329, 3, 0, 'Bin Xian', 'BIX', '1-28-329-3177', '陕西省-咸阳市-彬县', 1);
INSERT INTO `qmgx_region` VALUES (3178, '610428', '长武县', 329, 3, 0, 'Changwu Xian', 'CWU', '1-28-329-3178', '陕西省-咸阳市-长武县', 1);
INSERT INTO `qmgx_region` VALUES (3179, '610429', '旬邑县', 329, 3, 0, 'Xunyi Xian', 'XNY', '1-28-329-3179', '陕西省-咸阳市-旬邑县', 1);
INSERT INTO `qmgx_region` VALUES (3180, '610430', '淳化县', 329, 3, 0, 'Chunhua Xian', 'CHU', '1-28-329-3180', '陕西省-咸阳市-淳化县', 1);
INSERT INTO `qmgx_region` VALUES (3181, '610431', '武功县', 329, 3, 0, 'Wugong Xian', 'WGG', '1-28-329-3181', '陕西省-咸阳市-武功县', 1);
INSERT INTO `qmgx_region` VALUES (3182, '610481', '兴平市', 329, 3, 0, 'Xingping Shi', 'XPG', '1-28-329-3182', '陕西省-咸阳市-兴平市', 1);
INSERT INTO `qmgx_region` VALUES (3183, '610501', '市辖区', 330, 3, 0, 'Shixiaqu', '2', '1-28-330-3183', '陕西省-渭南市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3184, '610502', '临渭区', 330, 3, 0, 'Linwei Qu', 'LWE', '1-28-330-3184', '陕西省-渭南市-临渭区', 1);
INSERT INTO `qmgx_region` VALUES (3185, '610521', '华县', 330, 3, 0, 'Hua Xian', 'HXN', '1-28-330-3185', '陕西省-渭南市-华县', 1);
INSERT INTO `qmgx_region` VALUES (3186, '610522', '潼关县', 330, 3, 0, 'Tongguan Xian', 'TGN', '1-28-330-3186', '陕西省-渭南市-潼关县', 1);
INSERT INTO `qmgx_region` VALUES (3187, '610523', '大荔县', 330, 3, 0, 'Dali Xian', 'DAL', '1-28-330-3187', '陕西省-渭南市-大荔县', 1);
INSERT INTO `qmgx_region` VALUES (3188, '610524', '合阳县', 330, 3, 0, 'Heyang Xian', 'HYK', '1-28-330-3188', '陕西省-渭南市-合阳县', 1);
INSERT INTO `qmgx_region` VALUES (3189, '610525', '澄城县', 330, 3, 0, 'Chengcheng Xian', 'CCG', '1-28-330-3189', '陕西省-渭南市-澄城县', 1);
INSERT INTO `qmgx_region` VALUES (3190, '610526', '蒲城县', 330, 3, 0, 'Pucheng Xian', 'PUC', '1-28-330-3190', '陕西省-渭南市-蒲城县', 1);
INSERT INTO `qmgx_region` VALUES (3191, '610527', '白水县', 330, 3, 0, 'Baishui Xian', 'BSU', '1-28-330-3191', '陕西省-渭南市-白水县', 1);
INSERT INTO `qmgx_region` VALUES (3192, '610528', '富平县', 330, 3, 0, 'Fuping Xian', 'FPX', '1-28-330-3192', '陕西省-渭南市-富平县', 1);
INSERT INTO `qmgx_region` VALUES (3193, '610581', '韩城市', 330, 3, 0, 'Hancheng Shi', 'HCE', '1-28-330-3193', '陕西省-渭南市-韩城市', 1);
INSERT INTO `qmgx_region` VALUES (3194, '610582', '华阴市', 330, 3, 0, 'Huayin Shi', 'HYI', '1-28-330-3194', '陕西省-渭南市-华阴市', 1);
INSERT INTO `qmgx_region` VALUES (3195, '610601', '市辖区', 331, 3, 0, 'Shixiaqu', '2', '1-28-331-3195', '陕西省-延安市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3196, '610602', '宝塔区', 331, 3, 0, 'Baota Qu', 'BTA', '1-28-331-3196', '陕西省-延安市-宝塔区', 1);
INSERT INTO `qmgx_region` VALUES (3197, '610621', '延长县', 331, 3, 0, 'Yanchang Xian', 'YCA', '1-28-331-3197', '陕西省-延安市-延长县', 1);
INSERT INTO `qmgx_region` VALUES (3198, '610622', '延川县', 331, 3, 0, 'Yanchuan Xian', 'YCT', '1-28-331-3198', '陕西省-延安市-延川县', 1);
INSERT INTO `qmgx_region` VALUES (3199, '610623', '子长县', 331, 3, 0, 'Zichang Xian', 'ZCA', '1-28-331-3199', '陕西省-延安市-子长县', 1);
INSERT INTO `qmgx_region` VALUES (3200, '610624', '安塞县', 331, 3, 0, 'Ansai Xian', 'ANS', '1-28-331-3200', '陕西省-延安市-安塞县', 1);
INSERT INTO `qmgx_region` VALUES (3201, '610625', '志丹县', 331, 3, 0, 'Zhidan Xian', 'ZDN', '1-28-331-3201', '陕西省-延安市-志丹县', 1);
INSERT INTO `qmgx_region` VALUES (3202, '610626', '吴起县', 331, 3, 0, 'Wuqi Xian', '2', '1-28-331-3202', '陕西省-延安市-吴起县', 1);
INSERT INTO `qmgx_region` VALUES (3203, '610627', '甘泉县', 331, 3, 0, 'Ganquan Xian', 'GQN', '1-28-331-3203', '陕西省-延安市-甘泉县', 1);
INSERT INTO `qmgx_region` VALUES (3204, '610628', '富县', 331, 3, 0, 'Fu Xian', 'FUX', '1-28-331-3204', '陕西省-延安市-富县', 1);
INSERT INTO `qmgx_region` VALUES (3205, '610629', '洛川县', 331, 3, 0, 'Luochuan Xian', 'LCW', '1-28-331-3205', '陕西省-延安市-洛川县', 1);
INSERT INTO `qmgx_region` VALUES (3206, '610630', '宜川县', 331, 3, 0, 'Yichuan Xian', 'YIC', '1-28-331-3206', '陕西省-延安市-宜川县', 1);
INSERT INTO `qmgx_region` VALUES (3207, '610631', '黄龙县', 331, 3, 0, 'Huanglong Xian', 'HGL', '1-28-331-3207', '陕西省-延安市-黄龙县', 1);
INSERT INTO `qmgx_region` VALUES (3208, '610632', '黄陵县', 331, 3, 0, 'Huangling Xian', 'HLG', '1-28-331-3208', '陕西省-延安市-黄陵县', 1);
INSERT INTO `qmgx_region` VALUES (3209, '610701', '市辖区', 332, 3, 0, 'Shixiaqu', '2', '1-28-332-3209', '陕西省-汉中市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3210, '610702', '汉台区', 332, 3, 0, 'Hantai Qu', 'HTQ', '1-28-332-3210', '陕西省-汉中市-汉台区', 1);
INSERT INTO `qmgx_region` VALUES (3211, '610721', '南郑县', 332, 3, 0, 'Nanzheng Xian', 'NZG', '1-28-332-3211', '陕西省-汉中市-南郑县', 1);
INSERT INTO `qmgx_region` VALUES (3212, '610722', '城固县', 332, 3, 0, 'Chenggu Xian', 'CGU', '1-28-332-3212', '陕西省-汉中市-城固县', 1);
INSERT INTO `qmgx_region` VALUES (3213, '610723', '洋县', 332, 3, 0, 'Yang Xian', 'YGX', '1-28-332-3213', '陕西省-汉中市-洋县', 1);
INSERT INTO `qmgx_region` VALUES (3214, '610724', '西乡县', 332, 3, 0, 'Xixiang Xian', 'XXA', '1-28-332-3214', '陕西省-汉中市-西乡县', 1);
INSERT INTO `qmgx_region` VALUES (3215, '610725', '勉县', 332, 3, 0, 'Mian Xian', 'MIA', '1-28-332-3215', '陕西省-汉中市-勉县', 1);
INSERT INTO `qmgx_region` VALUES (3216, '610726', '宁强县', 332, 3, 0, 'Ningqiang Xian', 'NQG', '1-28-332-3216', '陕西省-汉中市-宁强县', 1);
INSERT INTO `qmgx_region` VALUES (3217, '610727', '略阳县', 332, 3, 0, 'Lueyang Xian', 'LYC', '1-28-332-3217', '陕西省-汉中市-略阳县', 1);
INSERT INTO `qmgx_region` VALUES (3218, '610728', '镇巴县', 332, 3, 0, 'Zhenba Xian', 'ZBA', '1-28-332-3218', '陕西省-汉中市-镇巴县', 1);
INSERT INTO `qmgx_region` VALUES (3219, '610729', '留坝县', 332, 3, 0, 'Liuba Xian', 'LBA', '1-28-332-3219', '陕西省-汉中市-留坝县', 1);
INSERT INTO `qmgx_region` VALUES (3220, '610730', '佛坪县', 332, 3, 0, 'Foping Xian', 'FPG', '1-28-332-3220', '陕西省-汉中市-佛坪县', 1);
INSERT INTO `qmgx_region` VALUES (3221, '610801', '市辖区', 333, 3, 0, '1', '2', '1-28-333-3221', '陕西省-榆林市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3222, '610802', '榆阳区', 333, 3, 0, 'Yuyang Qu', '2', '1-28-333-3222', '陕西省-榆林市-榆阳区', 1);
INSERT INTO `qmgx_region` VALUES (3223, '610821', '神木县', 333, 3, 0, 'Shenmu Xian', '2', '1-28-333-3223', '陕西省-榆林市-神木县', 1);
INSERT INTO `qmgx_region` VALUES (3224, '610822', '府谷县', 333, 3, 0, 'Fugu Xian', '2', '1-28-333-3224', '陕西省-榆林市-府谷县', 1);
INSERT INTO `qmgx_region` VALUES (3225, '610823', '横山县', 333, 3, 0, 'Hengshan Xian', '2', '1-28-333-3225', '陕西省-榆林市-横山县', 1);
INSERT INTO `qmgx_region` VALUES (3226, '610824', '靖边县', 333, 3, 0, 'Jingbian Xian', '2', '1-28-333-3226', '陕西省-榆林市-靖边县', 1);
INSERT INTO `qmgx_region` VALUES (3227, '610825', '定边县', 333, 3, 0, 'Dingbian Xian', '2', '1-28-333-3227', '陕西省-榆林市-定边县', 1);
INSERT INTO `qmgx_region` VALUES (3228, '610826', '绥德县', 333, 3, 0, 'Suide Xian', '2', '1-28-333-3228', '陕西省-榆林市-绥德县', 1);
INSERT INTO `qmgx_region` VALUES (3229, '610827', '米脂县', 333, 3, 0, 'Mizhi Xian', '2', '1-28-333-3229', '陕西省-榆林市-米脂县', 1);
INSERT INTO `qmgx_region` VALUES (3230, '610828', '佳县', 333, 3, 0, 'Jia Xian', '2', '1-28-333-3230', '陕西省-榆林市-佳县', 1);
INSERT INTO `qmgx_region` VALUES (3231, '610829', '吴堡县', 333, 3, 0, 'Wubu Xian', '2', '1-28-333-3231', '陕西省-榆林市-吴堡县', 1);
INSERT INTO `qmgx_region` VALUES (3232, '610830', '清涧县', 333, 3, 0, 'Qingjian Xian', '2', '1-28-333-3232', '陕西省-榆林市-清涧县', 1);
INSERT INTO `qmgx_region` VALUES (3233, '610831', '子洲县', 333, 3, 0, 'Zizhou Xian', '2', '1-28-333-3233', '陕西省-榆林市-子洲县', 1);
INSERT INTO `qmgx_region` VALUES (3234, '610901', '市辖区', 334, 3, 0, '1', '2', '1-28-334-3234', '陕西省-安康市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3235, '610902', '汉滨区', 334, 3, 0, 'Hanbin Qu', '2', '1-28-334-3235', '陕西省-安康市-汉滨区', 1);
INSERT INTO `qmgx_region` VALUES (3236, '610921', '汉阴县', 334, 3, 0, 'Hanyin Xian', '2', '1-28-334-3236', '陕西省-安康市-汉阴县', 1);
INSERT INTO `qmgx_region` VALUES (3237, '610922', '石泉县', 334, 3, 0, 'Shiquan Xian', '2', '1-28-334-3237', '陕西省-安康市-石泉县', 1);
INSERT INTO `qmgx_region` VALUES (3238, '610923', '宁陕县', 334, 3, 0, 'Ningshan Xian', '2', '1-28-334-3238', '陕西省-安康市-宁陕县', 1);
INSERT INTO `qmgx_region` VALUES (3239, '610924', '紫阳县', 334, 3, 0, 'Ziyang Xian', '2', '1-28-334-3239', '陕西省-安康市-紫阳县', 1);
INSERT INTO `qmgx_region` VALUES (3240, '610925', '岚皋县', 334, 3, 0, 'Langao Xian', '2', '1-28-334-3240', '陕西省-安康市-岚皋县', 1);
INSERT INTO `qmgx_region` VALUES (3241, '610926', '平利县', 334, 3, 0, 'Pingli Xian', '2', '1-28-334-3241', '陕西省-安康市-平利县', 1);
INSERT INTO `qmgx_region` VALUES (3242, '610927', '镇坪县', 334, 3, 0, 'Zhenping Xian', '2', '1-28-334-3242', '陕西省-安康市-镇坪县', 1);
INSERT INTO `qmgx_region` VALUES (3243, '610928', '旬阳县', 334, 3, 0, 'Xunyang Xian', '2', '1-28-334-3243', '陕西省-安康市-旬阳县', 1);
INSERT INTO `qmgx_region` VALUES (3244, '610929', '白河县', 334, 3, 0, 'Baihe Xian', '2', '1-28-334-3244', '陕西省-安康市-白河县', 1);
INSERT INTO `qmgx_region` VALUES (3245, '611001', '市辖区', 335, 3, 0, '1', '2', '1-28-335-3245', '陕西省-商洛市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3246, '611002', '商州区', 335, 3, 0, 'Shangzhou Qu', '2', '1-28-335-3246', '陕西省-商洛市-商州区', 1);
INSERT INTO `qmgx_region` VALUES (3247, '611021', '洛南县', 335, 3, 0, 'Luonan Xian', '2', '1-28-335-3247', '陕西省-商洛市-洛南县', 1);
INSERT INTO `qmgx_region` VALUES (3248, '611022', '丹凤县', 335, 3, 0, 'Danfeng Xian', '2', '1-28-335-3248', '陕西省-商洛市-丹凤县', 1);
INSERT INTO `qmgx_region` VALUES (3249, '611023', '商南县', 335, 3, 0, 'Shangnan Xian', '2', '1-28-335-3249', '陕西省-商洛市-商南县', 1);
INSERT INTO `qmgx_region` VALUES (3250, '611024', '山阳县', 335, 3, 0, 'Shanyang Xian', '2', '1-28-335-3250', '陕西省-商洛市-山阳县', 1);
INSERT INTO `qmgx_region` VALUES (3251, '611025', '镇安县', 335, 3, 0, 'Zhen,an Xian', '2', '1-28-335-3251', '陕西省-商洛市-镇安县', 1);
INSERT INTO `qmgx_region` VALUES (3252, '611026', '柞水县', 335, 3, 0, 'Zhashui Xian', '2', '1-28-335-3252', '陕西省-商洛市-柞水县', 1);
INSERT INTO `qmgx_region` VALUES (3253, '620101', '市辖区', 336, 3, 0, 'Shixiaqu', '2', '1-29-336-3253', '甘肃省-兰州市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3254, '620102', '城关区', 336, 3, 0, 'Chengguan Qu', 'CLZ', '1-29-336-3254', '甘肃省-兰州市-城关区', 1);
INSERT INTO `qmgx_region` VALUES (3255, '620103', '七里河区', 336, 3, 0, 'Qilihe Qu', 'QLH', '1-29-336-3255', '甘肃省-兰州市-七里河区', 1);
INSERT INTO `qmgx_region` VALUES (3256, '620104', '西固区', 336, 3, 0, 'Xigu Qu', 'XGU', '1-29-336-3256', '甘肃省-兰州市-西固区', 1);
INSERT INTO `qmgx_region` VALUES (3257, '620105', '安宁区', 336, 3, 0, 'Anning Qu', 'ANQ', '1-29-336-3257', '甘肃省-兰州市-安宁区', 1);
INSERT INTO `qmgx_region` VALUES (3258, '620111', '红古区', 336, 3, 0, 'Honggu Qu', 'HOG', '1-29-336-3258', '甘肃省-兰州市-红古区', 1);
INSERT INTO `qmgx_region` VALUES (3259, '620121', '永登县', 336, 3, 0, 'Yongdeng Xian', 'YDG', '1-29-336-3259', '甘肃省-兰州市-永登县', 1);
INSERT INTO `qmgx_region` VALUES (3260, '620122', '皋兰县', 336, 3, 0, 'Gaolan Xian', 'GAL', '1-29-336-3260', '甘肃省-兰州市-皋兰县', 1);
INSERT INTO `qmgx_region` VALUES (3261, '620123', '榆中县', 336, 3, 0, 'Yuzhong Xian', 'YZX', '1-29-336-3261', '甘肃省-兰州市-榆中县', 1);
INSERT INTO `qmgx_region` VALUES (3262, '620201', '市辖区', 337, 3, 0, 'Shixiaqu', '2', '1-29-337-3262', '甘肃省-嘉峪关市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3263, '620301', '市辖区', 338, 3, 0, 'Shixiaqu', '2', '1-29-338-3263', '甘肃省-金昌市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3264, '620302', '金川区', 338, 3, 0, 'Jinchuan Qu', 'JCU', '1-29-338-3264', '甘肃省-金昌市-金川区', 1);
INSERT INTO `qmgx_region` VALUES (3265, '620321', '永昌县', 338, 3, 0, 'Yongchang Xian', 'YCF', '1-29-338-3265', '甘肃省-金昌市-永昌县', 1);
INSERT INTO `qmgx_region` VALUES (3266, '620401', '市辖区', 339, 3, 0, 'Shixiaqu', '2', '1-29-339-3266', '甘肃省-白银市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3267, '620402', '白银区', 339, 3, 0, 'Baiyin Qu', 'BYB', '1-29-339-3267', '甘肃省-白银市-白银区', 1);
INSERT INTO `qmgx_region` VALUES (3268, '620403', '平川区', 339, 3, 0, 'Pingchuan Qu', 'PCQ', '1-29-339-3268', '甘肃省-白银市-平川区', 1);
INSERT INTO `qmgx_region` VALUES (3269, '620421', '靖远县', 339, 3, 0, 'Jingyuan Xian', 'JYH', '1-29-339-3269', '甘肃省-白银市-靖远县', 1);
INSERT INTO `qmgx_region` VALUES (3270, '620422', '会宁县', 339, 3, 0, 'Huining xian', 'HNI', '1-29-339-3270', '甘肃省-白银市-会宁县', 1);
INSERT INTO `qmgx_region` VALUES (3271, '620423', '景泰县', 339, 3, 0, 'Jingtai Xian', 'JGT', '1-29-339-3271', '甘肃省-白银市-景泰县', 1);
INSERT INTO `qmgx_region` VALUES (3272, '620501', '市辖区', 340, 3, 0, 'Shixiaqu', '2', '1-29-340-3272', '甘肃省-天水市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3274, '620502', '秦州区', 340, 3, 0, 'Beidao Qu', '2', '1-29-340-3274', '甘肃省-天水市-秦州区', 1);
INSERT INTO `qmgx_region` VALUES (3275, '620521', '清水县', 340, 3, 0, 'Qingshui Xian', 'QSG', '1-29-340-3275', '甘肃省-天水市-清水县', 1);
INSERT INTO `qmgx_region` VALUES (3276, '620522', '秦安县', 340, 3, 0, 'Qin,an Xian', 'QNA', '1-29-340-3276', '甘肃省-天水市-秦安县', 1);
INSERT INTO `qmgx_region` VALUES (3277, '620523', '甘谷县', 340, 3, 0, 'Gangu Xian', 'GGU', '1-29-340-3277', '甘肃省-天水市-甘谷县', 1);
INSERT INTO `qmgx_region` VALUES (3278, '620524', '武山县', 340, 3, 0, 'Wushan Xian', 'WSX', '1-29-340-3278', '甘肃省-天水市-武山县', 1);
INSERT INTO `qmgx_region` VALUES (3279, '620525', '张家川回族自治县', 340, 3, 0, 'Zhangjiachuan Huizu Zizhixian', 'ZJC', '1-29-340-3279', '甘肃省-天水市-张家川回族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3280, '620601', '市辖区', 341, 3, 0, '1', '2', '1-29-341-3280', '甘肃省-武威市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3281, '620602', '凉州区', 341, 3, 0, 'Liangzhou Qu', '2', '1-29-341-3281', '甘肃省-武威市-凉州区', 1);
INSERT INTO `qmgx_region` VALUES (3282, '620621', '民勤县', 341, 3, 0, 'Minqin Xian', '2', '1-29-341-3282', '甘肃省-武威市-民勤县', 1);
INSERT INTO `qmgx_region` VALUES (3283, '620622', '古浪县', 341, 3, 0, 'Gulang Xian', '2', '1-29-341-3283', '甘肃省-武威市-古浪县', 1);
INSERT INTO `qmgx_region` VALUES (3284, '620623', '天祝藏族自治县', 341, 3, 0, 'Tianzhu Zangzu Zizhixian', '2', '1-29-341-3284', '甘肃省-武威市-天祝藏族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3285, '620701', '市辖区', 342, 3, 0, '1', '2', '1-29-342-3285', '甘肃省-张掖市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3286, '620702', '甘州区', 342, 3, 0, 'Ganzhou Qu', '2', '1-29-342-3286', '甘肃省-张掖市-甘州区', 1);
INSERT INTO `qmgx_region` VALUES (3287, '620721', '肃南裕固族自治县', 342, 3, 0, 'Sunan Yugurzu Zizhixian', '2', '1-29-342-3287', '甘肃省-张掖市-肃南裕固族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3288, '620722', '民乐县', 342, 3, 0, 'Minle Xian', '2', '1-29-342-3288', '甘肃省-张掖市-民乐县', 1);
INSERT INTO `qmgx_region` VALUES (3289, '620723', '临泽县', 342, 3, 0, 'Linze Xian', '2', '1-29-342-3289', '甘肃省-张掖市-临泽县', 1);
INSERT INTO `qmgx_region` VALUES (3290, '620724', '高台县', 342, 3, 0, 'Gaotai Xian', '2', '1-29-342-3290', '甘肃省-张掖市-高台县', 1);
INSERT INTO `qmgx_region` VALUES (3291, '620725', '山丹县', 342, 3, 0, 'Shandan Xian', '2', '1-29-342-3291', '甘肃省-张掖市-山丹县', 1);
INSERT INTO `qmgx_region` VALUES (3292, '620801', '市辖区', 343, 3, 0, '1', '2', '1-29-343-3292', '甘肃省-平凉市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3293, '620802', '崆峒区', 343, 3, 0, 'Kongdong Qu', '2', '1-29-343-3293', '甘肃省-平凉市-崆峒区', 1);
INSERT INTO `qmgx_region` VALUES (3294, '620821', '泾川县', 343, 3, 0, 'Jingchuan Xian', '2', '1-29-343-3294', '甘肃省-平凉市-泾川县', 1);
INSERT INTO `qmgx_region` VALUES (3295, '620822', '灵台县', 343, 3, 0, 'Lingtai Xian', '2', '1-29-343-3295', '甘肃省-平凉市-灵台县', 1);
INSERT INTO `qmgx_region` VALUES (3296, '620823', '崇信县', 343, 3, 0, 'Chongxin Xian', '2', '1-29-343-3296', '甘肃省-平凉市-崇信县', 1);
INSERT INTO `qmgx_region` VALUES (3297, '620824', '华亭县', 343, 3, 0, 'Huating Xian', '2', '1-29-343-3297', '甘肃省-平凉市-华亭县', 1);
INSERT INTO `qmgx_region` VALUES (3298, '620825', '庄浪县', 343, 3, 0, 'Zhuanglang Xian', '2', '1-29-343-3298', '甘肃省-平凉市-庄浪县', 1);
INSERT INTO `qmgx_region` VALUES (3299, '620826', '静宁县', 343, 3, 0, 'Jingning Xian', '2', '1-29-343-3299', '甘肃省-平凉市-静宁县', 1);
INSERT INTO `qmgx_region` VALUES (3300, '620901', '市辖区', 344, 3, 0, '1', '2', '1-29-344-3300', '甘肃省-酒泉市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3301, '620902', '肃州区', 344, 3, 0, 'Suzhou Qu', '2', '1-29-344-3301', '甘肃省-酒泉市-肃州区', 1);
INSERT INTO `qmgx_region` VALUES (3302, '620921', '金塔县', 344, 3, 0, 'Jinta Xian', '2', '1-29-344-3302', '甘肃省-酒泉市-金塔县', 1);
INSERT INTO `qmgx_region` VALUES (3304, '620923', '肃北蒙古族自治县', 344, 3, 0, 'Subei Monguzu Zizhixian', '2', '1-29-344-3304', '甘肃省-酒泉市-肃北蒙古族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3305, '620924', '阿克塞哈萨克族自治县', 344, 3, 0, 'Aksay Kazakzu Zizhixian', '2', '1-29-344-3305', '甘肃省-酒泉市-阿克塞哈萨克族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3306, '620981', '玉门市', 344, 3, 0, 'Yumen Shi', '2', '1-29-344-3306', '甘肃省-酒泉市-玉门市', 1);
INSERT INTO `qmgx_region` VALUES (3307, '620982', '敦煌市', 344, 3, 0, 'Dunhuang Shi', '2', '1-29-344-3307', '甘肃省-酒泉市-敦煌市', 1);
INSERT INTO `qmgx_region` VALUES (3308, '621001', '市辖区', 345, 3, 0, '1', '2', '1-29-345-3308', '甘肃省-庆阳市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3309, '621002', '西峰区', 345, 3, 0, 'Xifeng Qu', '2', '1-29-345-3309', '甘肃省-庆阳市-西峰区', 1);
INSERT INTO `qmgx_region` VALUES (3310, '621021', '庆城县', 345, 3, 0, 'Qingcheng Xian', '2', '1-29-345-3310', '甘肃省-庆阳市-庆城县', 1);
INSERT INTO `qmgx_region` VALUES (3311, '621022', '环县', 345, 3, 0, 'Huan Xian', '2', '1-29-345-3311', '甘肃省-庆阳市-环县', 1);
INSERT INTO `qmgx_region` VALUES (3312, '621023', '华池县', 345, 3, 0, 'Huachi Xian', '2', '1-29-345-3312', '甘肃省-庆阳市-华池县', 1);
INSERT INTO `qmgx_region` VALUES (3313, '621024', '合水县', 345, 3, 0, 'Heshui Xian', '2', '1-29-345-3313', '甘肃省-庆阳市-合水县', 1);
INSERT INTO `qmgx_region` VALUES (3314, '621025', '正宁县', 345, 3, 0, 'Zhengning Xian', '2', '1-29-345-3314', '甘肃省-庆阳市-正宁县', 1);
INSERT INTO `qmgx_region` VALUES (3315, '621026', '宁县', 345, 3, 0, 'Ning Xian', '2', '1-29-345-3315', '甘肃省-庆阳市-宁县', 1);
INSERT INTO `qmgx_region` VALUES (3316, '621027', '镇原县', 345, 3, 0, 'Zhenyuan Xian', '2', '1-29-345-3316', '甘肃省-庆阳市-镇原县', 1);
INSERT INTO `qmgx_region` VALUES (3317, '621101', '市辖区', 346, 3, 0, '1', '2', '1-29-346-3317', '甘肃省-定西市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3318, '621102', '安定区', 346, 3, 0, 'Anding Qu', '2', '1-29-346-3318', '甘肃省-定西市-安定区', 1);
INSERT INTO `qmgx_region` VALUES (3319, '621121', '通渭县', 346, 3, 0, 'Tongwei Xian', '2', '1-29-346-3319', '甘肃省-定西市-通渭县', 1);
INSERT INTO `qmgx_region` VALUES (3320, '621122', '陇西县', 346, 3, 0, 'Longxi Xian', '2', '1-29-346-3320', '甘肃省-定西市-陇西县', 1);
INSERT INTO `qmgx_region` VALUES (3321, '621123', '渭源县', 346, 3, 0, 'Weiyuan Xian', '2', '1-29-346-3321', '甘肃省-定西市-渭源县', 1);
INSERT INTO `qmgx_region` VALUES (3322, '621124', '临洮县', 346, 3, 0, 'Lintao Xian', '2', '1-29-346-3322', '甘肃省-定西市-临洮县', 1);
INSERT INTO `qmgx_region` VALUES (3323, '621125', '漳县', 346, 3, 0, 'Zhang Xian', '2', '1-29-346-3323', '甘肃省-定西市-漳县', 1);
INSERT INTO `qmgx_region` VALUES (3324, '621126', '岷县', 346, 3, 0, 'Min Xian', '2', '1-29-346-3324', '甘肃省-定西市-岷县', 1);
INSERT INTO `qmgx_region` VALUES (3325, '621201', '市辖区', 347, 3, 0, '1', '2', '1-29-347-3325', '甘肃省-陇南市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3326, '621202', '武都区', 347, 3, 0, 'Wudu Qu', '2', '1-29-347-3326', '甘肃省-陇南市-武都区', 1);
INSERT INTO `qmgx_region` VALUES (3327, '621221', '成县', 347, 3, 0, 'Cheng Xian', '2', '1-29-347-3327', '甘肃省-陇南市-成县', 1);
INSERT INTO `qmgx_region` VALUES (3328, '621222', '文县', 347, 3, 0, 'Wen Xian', '2', '1-29-347-3328', '甘肃省-陇南市-文县', 1);
INSERT INTO `qmgx_region` VALUES (3329, '621223', '宕昌县', 347, 3, 0, 'Dangchang Xian', '2', '1-29-347-3329', '甘肃省-陇南市-宕昌县', 1);
INSERT INTO `qmgx_region` VALUES (3330, '621224', '康县', 347, 3, 0, 'Kang Xian', '2', '1-29-347-3330', '甘肃省-陇南市-康县', 1);
INSERT INTO `qmgx_region` VALUES (3331, '621225', '西和县', 347, 3, 0, 'Xihe Xian', '2', '1-29-347-3331', '甘肃省-陇南市-西和县', 1);
INSERT INTO `qmgx_region` VALUES (3332, '621226', '礼县', 347, 3, 0, 'Li Xian', '2', '1-29-347-3332', '甘肃省-陇南市-礼县', 1);
INSERT INTO `qmgx_region` VALUES (3333, '621227', '徽县', 347, 3, 0, 'Hui Xian', '2', '1-29-347-3333', '甘肃省-陇南市-徽县', 1);
INSERT INTO `qmgx_region` VALUES (3334, '621228', '两当县', 347, 3, 0, 'Liangdang Xian', '2', '1-29-347-3334', '甘肃省-陇南市-两当县', 1);
INSERT INTO `qmgx_region` VALUES (3335, '622901', '临夏市', 348, 3, 0, 'Linxia Shi', 'LXR', '1-29-348-3335', '甘肃省-临夏回族自治州-临夏市', 1);
INSERT INTO `qmgx_region` VALUES (3336, '622921', '临夏县', 348, 3, 0, 'Linxia Xian', 'LXF', '1-29-348-3336', '甘肃省-临夏回族自治州-临夏县', 1);
INSERT INTO `qmgx_region` VALUES (3337, '622922', '康乐县', 348, 3, 0, 'Kangle Xian', 'KLE', '1-29-348-3337', '甘肃省-临夏回族自治州-康乐县', 1);
INSERT INTO `qmgx_region` VALUES (3338, '622923', '永靖县', 348, 3, 0, 'Yongjing Xian', 'YJG', '1-29-348-3338', '甘肃省-临夏回族自治州-永靖县', 1);
INSERT INTO `qmgx_region` VALUES (3339, '622924', '广河县', 348, 3, 0, 'Guanghe Xian', 'GHX', '1-29-348-3339', '甘肃省-临夏回族自治州-广河县', 1);
INSERT INTO `qmgx_region` VALUES (3340, '622925', '和政县', 348, 3, 0, 'Hezheng Xian', 'HZG', '1-29-348-3340', '甘肃省-临夏回族自治州-和政县', 1);
INSERT INTO `qmgx_region` VALUES (3341, '622926', '东乡族自治县', 348, 3, 0, 'Dongxiangzu Zizhixian', 'DXZ', '1-29-348-3341', '甘肃省-临夏回族自治州-东乡族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3342, '622927', '积石山保安族东乡族撒拉族自治县', 348, 3, 0, 'Jishishan Bonanzu Dongxiangzu Salarzu Zizhixian', 'JSN', '1-29-348-3342', '甘肃省-临夏回族自治州-积石山保安族东乡族撒拉族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3343, '623001', '合作市', 349, 3, 0, 'Hezuo Shi', 'HEZ', '1-29-349-3343', '甘肃省-甘南藏族自治州-合作市', 1);
INSERT INTO `qmgx_region` VALUES (3344, '623021', '临潭县', 349, 3, 0, 'Lintan Xian', 'LTN', '1-29-349-3344', '甘肃省-甘南藏族自治州-临潭县', 1);
INSERT INTO `qmgx_region` VALUES (3345, '623022', '卓尼县', 349, 3, 0, 'Jone', 'JON', '1-29-349-3345', '甘肃省-甘南藏族自治州-卓尼县', 1);
INSERT INTO `qmgx_region` VALUES (3346, '623023', '舟曲县', 349, 3, 0, 'Zhugqu Xian', 'ZQU', '1-29-349-3346', '甘肃省-甘南藏族自治州-舟曲县', 1);
INSERT INTO `qmgx_region` VALUES (3347, '623024', '迭部县', 349, 3, 0, 'Tewo Xian', 'TEW', '1-29-349-3347', '甘肃省-甘南藏族自治州-迭部县', 1);
INSERT INTO `qmgx_region` VALUES (3348, '623025', '玛曲县', 349, 3, 0, 'Maqu Xian', 'MQU', '1-29-349-3348', '甘肃省-甘南藏族自治州-玛曲县', 1);
INSERT INTO `qmgx_region` VALUES (3349, '623026', '碌曲县', 349, 3, 0, 'Luqu Xian', 'LQU', '1-29-349-3349', '甘肃省-甘南藏族自治州-碌曲县', 1);
INSERT INTO `qmgx_region` VALUES (3350, '623027', '夏河县', 349, 3, 0, 'Xiahe Xian', 'XHN', '1-29-349-3350', '甘肃省-甘南藏族自治州-夏河县', 1);
INSERT INTO `qmgx_region` VALUES (3351, '630101', '市辖区', 350, 3, 0, 'Shixiaqu', '2', '1-30-350-3351', '青海省-西宁市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3352, '630102', '城东区', 350, 3, 0, 'Chengdong Qu', 'CDQ', '1-30-350-3352', '青海省-西宁市-城东区', 1);
INSERT INTO `qmgx_region` VALUES (3353, '630103', '城中区', 350, 3, 0, 'Chengzhong Qu', 'CZQ', '1-30-350-3353', '青海省-西宁市-城中区', 1);
INSERT INTO `qmgx_region` VALUES (3354, '630104', '城西区', 350, 3, 0, 'Chengxi Qu', 'CXQ', '1-30-350-3354', '青海省-西宁市-城西区', 1);
INSERT INTO `qmgx_region` VALUES (3355, '630105', '城北区', 350, 3, 0, 'Chengbei Qu', 'CBE', '1-30-350-3355', '青海省-西宁市-城北区', 1);
INSERT INTO `qmgx_region` VALUES (3356, '630121', '大通回族土族自治县', 350, 3, 0, 'Datong Huizu Tuzu Zizhixian', 'DAT', '1-30-350-3356', '青海省-西宁市-大通回族土族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3357, '630122', '湟中县', 350, 3, 0, 'Huangzhong Xian', '2', '1-30-350-3357', '青海省-西宁市-湟中县', 1);
INSERT INTO `qmgx_region` VALUES (3358, '630123', '湟源县', 350, 3, 0, 'Huangyuan Xian', '2', '1-30-350-3358', '青海省-西宁市-湟源县', 1);
INSERT INTO `qmgx_region` VALUES (3359, '632121', '平安县', 351, 3, 0, 'Ping,an Xian', 'PAN', '1-30-351-3359', '青海省-海东地区-平安县', 1);
INSERT INTO `qmgx_region` VALUES (3360, '632122', '民和回族土族自治县', 351, 3, 0, 'Minhe Huizu Tuzu Zizhixian', 'MHE', '1-30-351-3360', '青海省-海东地区-民和回族土族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3361, '632123', '乐都县', 351, 3, 0, 'Ledu Xian', 'LDU', '1-30-351-3361', '青海省-海东地区-乐都县', 1);
INSERT INTO `qmgx_region` VALUES (3362, '632126', '互助土族自治县', 351, 3, 0, 'Huzhu Tuzu Zizhixian', 'HZT', '1-30-351-3362', '青海省-海东地区-互助土族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3363, '632127', '化隆回族自治县', 351, 3, 0, 'Hualong Huizu Zizhixian', 'HLO', '1-30-351-3363', '青海省-海东地区-化隆回族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3364, '632128', '循化撒拉族自治县', 351, 3, 0, 'Xunhua Salazu Zizhixian', 'XUH', '1-30-351-3364', '青海省-海东地区-循化撒拉族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3365, '632221', '门源回族自治县', 352, 3, 0, 'Menyuan Huizu Zizhixian', 'MYU', '1-30-352-3365', '青海省-海北藏族自治州-门源回族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3366, '632222', '祁连县', 352, 3, 0, 'Qilian Xian', 'QLN', '1-30-352-3366', '青海省-海北藏族自治州-祁连县', 1);
INSERT INTO `qmgx_region` VALUES (3367, '632223', '海晏县', 352, 3, 0, 'Haiyan Xian', 'HIY', '1-30-352-3367', '青海省-海北藏族自治州-海晏县', 1);
INSERT INTO `qmgx_region` VALUES (3368, '632224', '刚察县', 352, 3, 0, 'Gangca Xian', 'GAN', '1-30-352-3368', '青海省-海北藏族自治州-刚察县', 1);
INSERT INTO `qmgx_region` VALUES (3369, '632321', '同仁县', 353, 3, 0, 'Tongren Xian', 'TRN', '1-30-353-3369', '青海省-黄南藏族自治州-同仁县', 1);
INSERT INTO `qmgx_region` VALUES (3370, '632322', '尖扎县', 353, 3, 0, 'Jainca Xian', 'JAI', '1-30-353-3370', '青海省-黄南藏族自治州-尖扎县', 1);
INSERT INTO `qmgx_region` VALUES (3371, '632323', '泽库县', 353, 3, 0, 'Zekog Xian', 'ZEK', '1-30-353-3371', '青海省-黄南藏族自治州-泽库县', 1);
INSERT INTO `qmgx_region` VALUES (3372, '632324', '河南蒙古族自治县', 353, 3, 0, 'Henan Mongolzu Zizhixian', 'HNM', '1-30-353-3372', '青海省-黄南藏族自治州-河南蒙古族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3373, '632521', '共和县', 354, 3, 0, 'Gonghe Xian', 'GHE', '1-30-354-3373', '青海省-海南藏族自治州-共和县', 1);
INSERT INTO `qmgx_region` VALUES (3374, '632522', '同德县', 354, 3, 0, 'Tongde Xian', 'TDX', '1-30-354-3374', '青海省-海南藏族自治州-同德县', 1);
INSERT INTO `qmgx_region` VALUES (3375, '632523', '贵德县', 354, 3, 0, 'Guide Xian', 'GID', '1-30-354-3375', '青海省-海南藏族自治州-贵德县', 1);
INSERT INTO `qmgx_region` VALUES (3376, '632524', '兴海县', 354, 3, 0, 'Xinghai Xian', 'XHA', '1-30-354-3376', '青海省-海南藏族自治州-兴海县', 1);
INSERT INTO `qmgx_region` VALUES (3377, '632525', '贵南县', 354, 3, 0, 'Guinan Xian', 'GNN', '1-30-354-3377', '青海省-海南藏族自治州-贵南县', 1);
INSERT INTO `qmgx_region` VALUES (3378, '632621', '玛沁县', 355, 3, 0, 'Maqen Xian', 'MAQ', '1-30-355-3378', '青海省-果洛藏族自治州-玛沁县', 1);
INSERT INTO `qmgx_region` VALUES (3379, '632622', '班玛县', 355, 3, 0, 'Baima Xian', 'BMX', '1-30-355-3379', '青海省-果洛藏族自治州-班玛县', 1);
INSERT INTO `qmgx_region` VALUES (3380, '632623', '甘德县', 355, 3, 0, 'Gade Xian', 'GAD', '1-30-355-3380', '青海省-果洛藏族自治州-甘德县', 1);
INSERT INTO `qmgx_region` VALUES (3381, '632624', '达日县', 355, 3, 0, 'Tarlag Xian', 'TAR', '1-30-355-3381', '青海省-果洛藏族自治州-达日县', 1);
INSERT INTO `qmgx_region` VALUES (3382, '632625', '久治县', 355, 3, 0, 'Jigzhi Xian', 'JUZ', '1-30-355-3382', '青海省-果洛藏族自治州-久治县', 1);
INSERT INTO `qmgx_region` VALUES (3383, '632626', '玛多县', 355, 3, 0, 'Madoi Xian', 'MAD', '1-30-355-3383', '青海省-果洛藏族自治州-玛多县', 1);
INSERT INTO `qmgx_region` VALUES (3384, '632721', '玉树县', 356, 3, 0, 'Yushu Xian', 'YSK', '1-30-356-3384', '青海省-玉树藏族自治州-玉树县', 1);
INSERT INTO `qmgx_region` VALUES (3385, '632722', '杂多县', 356, 3, 0, 'Zadoi Xian', 'ZAD', '1-30-356-3385', '青海省-玉树藏族自治州-杂多县', 1);
INSERT INTO `qmgx_region` VALUES (3386, '632723', '称多县', 356, 3, 0, 'Chindu Xian', 'CHI', '1-30-356-3386', '青海省-玉树藏族自治州-称多县', 1);
INSERT INTO `qmgx_region` VALUES (3387, '632724', '治多县', 356, 3, 0, 'Zhidoi Xian', 'ZHI', '1-30-356-3387', '青海省-玉树藏族自治州-治多县', 1);
INSERT INTO `qmgx_region` VALUES (3388, '632725', '囊谦县', 356, 3, 0, 'Nangqen Xian', 'NQN', '1-30-356-3388', '青海省-玉树藏族自治州-囊谦县', 1);
INSERT INTO `qmgx_region` VALUES (3389, '632726', '曲麻莱县', 356, 3, 0, 'Qumarleb Xian', 'QUM', '1-30-356-3389', '青海省-玉树藏族自治州-曲麻莱县', 1);
INSERT INTO `qmgx_region` VALUES (3390, '632801', '格尔木市', 357, 3, 0, 'Golmud Shi', 'GOS', '1-30-357-3390', '青海省-海西蒙古族藏族自治州-格尔木市', 1);
INSERT INTO `qmgx_region` VALUES (3391, '632802', '德令哈市', 357, 3, 0, 'Delhi Shi', 'DEL', '1-30-357-3391', '青海省-海西蒙古族藏族自治州-德令哈市', 1);
INSERT INTO `qmgx_region` VALUES (3392, '632821', '乌兰县', 357, 3, 0, 'Ulan Xian', 'ULA', '1-30-357-3392', '青海省-海西蒙古族藏族自治州-乌兰县', 1);
INSERT INTO `qmgx_region` VALUES (3393, '632822', '都兰县', 357, 3, 0, 'Dulan Xian', 'DUL', '1-30-357-3393', '青海省-海西蒙古族藏族自治州-都兰县', 1);
INSERT INTO `qmgx_region` VALUES (3394, '632823', '天峻县', 357, 3, 0, 'Tianjun Xian', 'TJN', '1-30-357-3394', '青海省-海西蒙古族藏族自治州-天峻县', 1);
INSERT INTO `qmgx_region` VALUES (3395, '640101', '市辖区', 358, 3, 0, 'Shixiaqu', '2', '1-31-358-3395', '宁夏回族自治区-银川市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3396, '640104', '兴庆区', 358, 3, 0, 'Xingqing Qu', '2', '1-31-358-3396', '宁夏回族自治区-银川市-兴庆区', 1);
INSERT INTO `qmgx_region` VALUES (3397, '640105', '西夏区', 358, 3, 0, 'Xixia Qu', '2', '1-31-358-3397', '宁夏回族自治区-银川市-西夏区', 1);
INSERT INTO `qmgx_region` VALUES (3398, '640106', '金凤区', 358, 3, 0, 'Jinfeng Qu', '2', '1-31-358-3398', '宁夏回族自治区-银川市-金凤区', 1);
INSERT INTO `qmgx_region` VALUES (3399, '640121', '永宁县', 358, 3, 0, 'Yongning Xian', 'YGN', '1-31-358-3399', '宁夏回族自治区-银川市-永宁县', 1);
INSERT INTO `qmgx_region` VALUES (3400, '640122', '贺兰县', 358, 3, 0, 'Helan Xian', 'HLN', '1-31-358-3400', '宁夏回族自治区-银川市-贺兰县', 1);
INSERT INTO `qmgx_region` VALUES (3401, '640181', '灵武市', 358, 3, 0, 'Lingwu Shi', '2', '1-31-358-3401', '宁夏回族自治区-银川市-灵武市', 1);
INSERT INTO `qmgx_region` VALUES (3402, '640201', '市辖区', 359, 3, 0, 'Shixiaqu', '2', '1-31-359-3402', '宁夏回族自治区-石嘴山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3403, '640202', '大武口区', 359, 3, 0, 'Dawukou Qu', 'DWK', '1-31-359-3403', '宁夏回族自治区-石嘴山市-大武口区', 1);
INSERT INTO `qmgx_region` VALUES (3404, '640205', '惠农区', 359, 3, 0, 'Huinong Qu', '2', '1-31-359-3404', '宁夏回族自治区-石嘴山市-惠农区', 1);
INSERT INTO `qmgx_region` VALUES (3405, '640221', '平罗县', 359, 3, 0, 'Pingluo Xian', 'PLO', '1-31-359-3405', '宁夏回族自治区-石嘴山市-平罗县', 1);
INSERT INTO `qmgx_region` VALUES (3406, '640301', '市辖区', 360, 3, 0, 'Shixiaqu', '2', '1-31-360-3406', '宁夏回族自治区-吴忠市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3407, '640302', '利通区', 360, 3, 0, 'Litong Qu', 'LTW', '1-31-360-3407', '宁夏回族自治区-吴忠市-利通区', 1);
INSERT INTO `qmgx_region` VALUES (3408, '640323', '盐池县', 360, 3, 0, 'Yanchi Xian', 'YCY', '1-31-360-3408', '宁夏回族自治区-吴忠市-盐池县', 1);
INSERT INTO `qmgx_region` VALUES (3409, '640324', '同心县', 360, 3, 0, 'Tongxin Xian', 'TGX', '1-31-360-3409', '宁夏回族自治区-吴忠市-同心县', 1);
INSERT INTO `qmgx_region` VALUES (3410, '640381', '青铜峡市', 360, 3, 0, 'Qingtongxia Xian', 'QTX', '1-31-360-3410', '宁夏回族自治区-吴忠市-青铜峡市', 1);
INSERT INTO `qmgx_region` VALUES (3411, '640401', '市辖区', 361, 3, 0, '1', '2', '1-31-361-3411', '宁夏回族自治区-固原市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3412, '640402', '原州区', 361, 3, 0, 'Yuanzhou Qu', '2', '1-31-361-3412', '宁夏回族自治区-固原市-原州区', 1);
INSERT INTO `qmgx_region` VALUES (3413, '640422', '西吉县', 361, 3, 0, 'Xiji Xian', '2', '1-31-361-3413', '宁夏回族自治区-固原市-西吉县', 1);
INSERT INTO `qmgx_region` VALUES (3414, '640423', '隆德县', 361, 3, 0, 'Longde Xian', '2', '1-31-361-3414', '宁夏回族自治区-固原市-隆德县', 1);
INSERT INTO `qmgx_region` VALUES (3415, '640424', '泾源县', 361, 3, 0, 'Jingyuan Xian', '2', '1-31-361-3415', '宁夏回族自治区-固原市-泾源县', 1);
INSERT INTO `qmgx_region` VALUES (3416, '640425', '彭阳县', 361, 3, 0, 'Pengyang Xian', '2', '1-31-361-3416', '宁夏回族自治区-固原市-彭阳县', 1);
INSERT INTO `qmgx_region` VALUES (3417, '640501', '市辖区', 362, 3, 0, '1', '2', '1-31-362-3417', '宁夏回族自治区-中卫市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3418, '640502', '沙坡头区', 362, 3, 0, 'Shapotou Qu', '2', '1-31-362-3418', '宁夏回族自治区-中卫市-沙坡头区', 1);
INSERT INTO `qmgx_region` VALUES (3419, '640521', '中宁县', 362, 3, 0, 'Zhongning Xian', '2', '1-31-362-3419', '宁夏回族自治区-中卫市-中宁县', 1);
INSERT INTO `qmgx_region` VALUES (3420, '640522', '海原县', 362, 3, 0, 'Haiyuan Xian', '2', '1-31-362-3420', '宁夏回族自治区-中卫市-海原县', 1);
INSERT INTO `qmgx_region` VALUES (3421, '650101', '市辖区', 363, 3, 0, 'Shixiaqu', '2', '1-32-363-3421', '新疆维吾尔自治区-乌鲁木齐市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3422, '650102', '天山区', 363, 3, 0, 'Tianshan Qu', 'TSL', '1-32-363-3422', '新疆维吾尔自治区-乌鲁木齐市-天山区', 1);
INSERT INTO `qmgx_region` VALUES (3423, '650103', '沙依巴克区', 363, 3, 0, 'Saybag Qu', 'SAY', '1-32-363-3423', '新疆维吾尔自治区-乌鲁木齐市-沙依巴克区', 1);
INSERT INTO `qmgx_region` VALUES (3424, '650104', '新市区', 363, 3, 0, 'Xinshi Qu', 'XSU', '1-32-363-3424', '新疆维吾尔自治区-乌鲁木齐市-新市区', 1);
INSERT INTO `qmgx_region` VALUES (3425, '650105', '水磨沟区', 363, 3, 0, 'Shuimogou Qu', 'SMG', '1-32-363-3425', '新疆维吾尔自治区-乌鲁木齐市-水磨沟区', 1);
INSERT INTO `qmgx_region` VALUES (3426, '650106', '头屯河区', 363, 3, 0, 'Toutunhe Qu', 'TTH', '1-32-363-3426', '新疆维吾尔自治区-乌鲁木齐市-头屯河区', 1);
INSERT INTO `qmgx_region` VALUES (3427, '650107', '达坂城区', 363, 3, 0, 'Dabancheng Qu', '2', '1-32-363-3427', '新疆维吾尔自治区-乌鲁木齐市-达坂城区', 1);
INSERT INTO `qmgx_region` VALUES (3428, '650109', '米东区', 363, 3, 0, 'Midong Qu', '2', '1-32-363-3428', '新疆维吾尔自治区-乌鲁木齐市-米东区', 1);
INSERT INTO `qmgx_region` VALUES (3429, '650121', '乌鲁木齐县', 363, 3, 0, 'Urumqi Xian', 'URX', '1-32-363-3429', '新疆维吾尔自治区-乌鲁木齐市-乌鲁木齐县', 1);
INSERT INTO `qmgx_region` VALUES (3430, '650201', '市辖区', 364, 3, 0, 'Shixiaqu', '2', '1-32-364-3430', '新疆维吾尔自治区-克拉玛依市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (3431, '650202', '独山子区', 364, 3, 0, 'Dushanzi Qu', 'DSZ', '1-32-364-3431', '新疆维吾尔自治区-克拉玛依市-独山子区', 1);
INSERT INTO `qmgx_region` VALUES (3432, '650203', '克拉玛依区', 364, 3, 0, 'Karamay Qu', 'KRQ', '1-32-364-3432', '新疆维吾尔自治区-克拉玛依市-克拉玛依区', 1);
INSERT INTO `qmgx_region` VALUES (3433, '650204', '白碱滩区', 364, 3, 0, 'Baijiantan Qu', 'BJT', '1-32-364-3433', '新疆维吾尔自治区-克拉玛依市-白碱滩区', 1);
INSERT INTO `qmgx_region` VALUES (3434, '650205', '乌尔禾区', 364, 3, 0, 'Orku Qu', 'ORK', '1-32-364-3434', '新疆维吾尔自治区-克拉玛依市-乌尔禾区', 1);
INSERT INTO `qmgx_region` VALUES (3435, '652101', '吐鲁番市', 365, 3, 0, 'Turpan Shi', 'TUR', '1-32-365-3435', '新疆维吾尔自治区-吐鲁番地区-吐鲁番市', 1);
INSERT INTO `qmgx_region` VALUES (3436, '652122', '鄯善县', 365, 3, 0, 'Shanshan(piqan) Xian', 'SSX', '1-32-365-3436', '新疆维吾尔自治区-吐鲁番地区-鄯善县', 1);
INSERT INTO `qmgx_region` VALUES (3437, '652123', '托克逊县', 365, 3, 0, 'Toksun Xian', 'TOK', '1-32-365-3437', '新疆维吾尔自治区-吐鲁番地区-托克逊县', 1);
INSERT INTO `qmgx_region` VALUES (3438, '652201', '哈密市', 366, 3, 0, 'Hami(kumul) Shi', 'HAM', '1-32-366-3438', '新疆维吾尔自治区-哈密地区-哈密市', 1);
INSERT INTO `qmgx_region` VALUES (3439, '652222', '巴里坤哈萨克自治县', 366, 3, 0, 'Barkol Kazak Zizhixian', 'BAR', '1-32-366-3439', '新疆维吾尔自治区-哈密地区-巴里坤哈萨克自治县', 1);
INSERT INTO `qmgx_region` VALUES (3440, '652223', '伊吾县', 366, 3, 0, 'Yiwu(Araturuk) Xian', 'YWX', '1-32-366-3440', '新疆维吾尔自治区-哈密地区-伊吾县', 1);
INSERT INTO `qmgx_region` VALUES (3441, '652301', '昌吉市', 367, 3, 0, 'Changji Shi', 'CJS', '1-32-367-3441', '新疆维吾尔自治区-昌吉回族自治州-昌吉市', 1);
INSERT INTO `qmgx_region` VALUES (3442, '652302', '阜康市', 367, 3, 0, 'Fukang Shi', 'FKG', '1-32-367-3442', '新疆维吾尔自治区-昌吉回族自治州-阜康市', 1);
INSERT INTO `qmgx_region` VALUES (3444, '652323', '呼图壁县', 367, 3, 0, 'Hutubi Xian', 'HTB', '1-32-367-3444', '新疆维吾尔自治区-昌吉回族自治州-呼图壁县', 1);
INSERT INTO `qmgx_region` VALUES (3445, '652324', '玛纳斯县', 367, 3, 0, 'Manas Xian', 'MAS', '1-32-367-3445', '新疆维吾尔自治区-昌吉回族自治州-玛纳斯县', 1);
INSERT INTO `qmgx_region` VALUES (3446, '652325', '奇台县', 367, 3, 0, 'Qitai Xian', 'QTA', '1-32-367-3446', '新疆维吾尔自治区-昌吉回族自治州-奇台县', 1);
INSERT INTO `qmgx_region` VALUES (3447, '652327', '吉木萨尔县', 367, 3, 0, 'Jimsar Xian', 'JIM', '1-32-367-3447', '新疆维吾尔自治区-昌吉回族自治州-吉木萨尔县', 1);
INSERT INTO `qmgx_region` VALUES (3448, '652328', '木垒哈萨克自治县', 367, 3, 0, 'Mori Kazak Zizhixian', 'MOR', '1-32-367-3448', '新疆维吾尔自治区-昌吉回族自治州-木垒哈萨克自治县', 1);
INSERT INTO `qmgx_region` VALUES (3449, '652701', '博乐市', 368, 3, 0, 'Bole(Bortala) Shi', 'BLE', '1-32-368-3449', '新疆维吾尔自治区-博尔塔拉蒙古自治州-博乐市', 1);
INSERT INTO `qmgx_region` VALUES (3450, '652722', '精河县', 368, 3, 0, 'Jinghe(Jing) Xian', 'JGH', '1-32-368-3450', '新疆维吾尔自治区-博尔塔拉蒙古自治州-精河县', 1);
INSERT INTO `qmgx_region` VALUES (3451, '652723', '温泉县', 368, 3, 0, 'Wenquan(Arixang) Xian', 'WNQ', '1-32-368-3451', '新疆维吾尔自治区-博尔塔拉蒙古自治州-温泉县', 1);
INSERT INTO `qmgx_region` VALUES (3452, '652801', '库尔勒市', 369, 3, 0, 'Korla Shi', 'KOR', '1-32-369-3452', '新疆维吾尔自治区-巴音郭楞蒙古自治州-库尔勒市', 1);
INSERT INTO `qmgx_region` VALUES (3453, '652822', '轮台县', 369, 3, 0, 'Luntai(Bugur) Xian', 'LTX', '1-32-369-3453', '新疆维吾尔自治区-巴音郭楞蒙古自治州-轮台县', 1);
INSERT INTO `qmgx_region` VALUES (3454, '652823', '尉犁县', 369, 3, 0, 'Yuli(Lopnur) Xian', 'YLI', '1-32-369-3454', '新疆维吾尔自治区-巴音郭楞蒙古自治州-尉犁县', 1);
INSERT INTO `qmgx_region` VALUES (3455, '652824', '若羌县', 369, 3, 0, 'Ruoqiang(Qakilik) Xian', 'RQG', '1-32-369-3455', '新疆维吾尔自治区-巴音郭楞蒙古自治州-若羌县', 1);
INSERT INTO `qmgx_region` VALUES (3456, '652825', '且末县', 369, 3, 0, 'Qiemo(Qarqan) Xian', 'QMO', '1-32-369-3456', '新疆维吾尔自治区-巴音郭楞蒙古自治州-且末县', 1);
INSERT INTO `qmgx_region` VALUES (3457, '652826', '焉耆回族自治县', 369, 3, 0, 'Yanqi Huizu Zizhixian', 'YQI', '1-32-369-3457', '新疆维吾尔自治区-巴音郭楞蒙古自治州-焉耆回族自治县', 1);
INSERT INTO `qmgx_region` VALUES (3458, '652827', '和静县', 369, 3, 0, 'Hejing Xian', 'HJG', '1-32-369-3458', '新疆维吾尔自治区-巴音郭楞蒙古自治州-和静县', 1);
INSERT INTO `qmgx_region` VALUES (3459, '652828', '和硕县', 369, 3, 0, 'Hoxud Xian', 'HOX', '1-32-369-3459', '新疆维吾尔自治区-巴音郭楞蒙古自治州-和硕县', 1);
INSERT INTO `qmgx_region` VALUES (3460, '652829', '博湖县', 369, 3, 0, 'Bohu(Bagrax) Xian', 'BHU', '1-32-369-3460', '新疆维吾尔自治区-巴音郭楞蒙古自治州-博湖县', 1);
INSERT INTO `qmgx_region` VALUES (3461, '652901', '阿克苏市', 370, 3, 0, 'Aksu Shi', 'AKS', '1-32-370-3461', '新疆维吾尔自治区-阿克苏地区-阿克苏市', 1);
INSERT INTO `qmgx_region` VALUES (3462, '652922', '温宿县', 370, 3, 0, 'Wensu Xian', 'WSU', '1-32-370-3462', '新疆维吾尔自治区-阿克苏地区-温宿县', 1);
INSERT INTO `qmgx_region` VALUES (3463, '652923', '库车县', 370, 3, 0, 'Kuqa Xian', 'KUQ', '1-32-370-3463', '新疆维吾尔自治区-阿克苏地区-库车县', 1);
INSERT INTO `qmgx_region` VALUES (3464, '652924', '沙雅县', 370, 3, 0, 'Xayar Xian', 'XYR', '1-32-370-3464', '新疆维吾尔自治区-阿克苏地区-沙雅县', 1);
INSERT INTO `qmgx_region` VALUES (3465, '652925', '新和县', 370, 3, 0, 'Xinhe(Toksu) Xian', 'XHT', '1-32-370-3465', '新疆维吾尔自治区-阿克苏地区-新和县', 1);
INSERT INTO `qmgx_region` VALUES (3466, '652926', '拜城县', 370, 3, 0, 'Baicheng(Bay) Xian', 'BCG', '1-32-370-3466', '新疆维吾尔自治区-阿克苏地区-拜城县', 1);
INSERT INTO `qmgx_region` VALUES (3467, '652927', '乌什县', 370, 3, 0, 'Wushi(Uqturpan) Xian', 'WSH', '1-32-370-3467', '新疆维吾尔自治区-阿克苏地区-乌什县', 1);
INSERT INTO `qmgx_region` VALUES (3468, '652928', '阿瓦提县', 370, 3, 0, 'Awat Xian', 'AWA', '1-32-370-3468', '新疆维吾尔自治区-阿克苏地区-阿瓦提县', 1);
INSERT INTO `qmgx_region` VALUES (3469, '652929', '柯坪县', 370, 3, 0, 'Kalpin Xian', 'KAL', '1-32-370-3469', '新疆维吾尔自治区-阿克苏地区-柯坪县', 1);
INSERT INTO `qmgx_region` VALUES (3470, '653001', '阿图什市', 371, 3, 0, 'Artux Shi', 'ART', '1-32-371-3470', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-阿图什市', 1);
INSERT INTO `qmgx_region` VALUES (3471, '653022', '阿克陶县', 371, 3, 0, 'Akto Xian', 'AKT', '1-32-371-3471', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-阿克陶县', 1);
INSERT INTO `qmgx_region` VALUES (3472, '653023', '阿合奇县', 371, 3, 0, 'Akqi Xian', 'AKQ', '1-32-371-3472', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-阿合奇县', 1);
INSERT INTO `qmgx_region` VALUES (3473, '653024', '乌恰县', 371, 3, 0, 'Wuqia(Ulugqat) Xian', 'WQA', '1-32-371-3473', '新疆维吾尔自治区-克孜勒苏柯尔克孜自治州-乌恰县', 1);
INSERT INTO `qmgx_region` VALUES (3474, '653101', '喀什市', 372, 3, 0, 'Kashi (Kaxgar) Shi', 'KHG', '1-32-372-3474', '新疆维吾尔自治区-喀什地区-喀什市', 1);
INSERT INTO `qmgx_region` VALUES (3475, '653121', '疏附县', 372, 3, 0, 'Shufu Xian', 'SFU', '1-32-372-3475', '新疆维吾尔自治区-喀什地区-疏附县', 1);
INSERT INTO `qmgx_region` VALUES (3476, '653122', '疏勒县', 372, 3, 0, 'Shule Xian', 'SHL', '1-32-372-3476', '新疆维吾尔自治区-喀什地区-疏勒县', 1);
INSERT INTO `qmgx_region` VALUES (3477, '653123', '英吉沙县', 372, 3, 0, 'Yengisar Xian', 'YEN', '1-32-372-3477', '新疆维吾尔自治区-喀什地区-英吉沙县', 1);
INSERT INTO `qmgx_region` VALUES (3478, '653124', '泽普县', 372, 3, 0, 'Zepu(Poskam) Xian', 'ZEP', '1-32-372-3478', '新疆维吾尔自治区-喀什地区-泽普县', 1);
INSERT INTO `qmgx_region` VALUES (3479, '653125', '莎车县', 372, 3, 0, 'Shache(Yarkant) Xian', 'SHC', '1-32-372-3479', '新疆维吾尔自治区-喀什地区-莎车县', 1);
INSERT INTO `qmgx_region` VALUES (3480, '653126', '叶城县', 372, 3, 0, 'Yecheng(Kargilik) Xian', 'YEC', '1-32-372-3480', '新疆维吾尔自治区-喀什地区-叶城县', 1);
INSERT INTO `qmgx_region` VALUES (3481, '653127', '麦盖提县', 372, 3, 0, 'Markit Xian', 'MAR', '1-32-372-3481', '新疆维吾尔自治区-喀什地区-麦盖提县', 1);
INSERT INTO `qmgx_region` VALUES (3482, '653128', '岳普湖县', 372, 3, 0, 'Yopurga Xian', 'YOP', '1-32-372-3482', '新疆维吾尔自治区-喀什地区-岳普湖县', 1);
INSERT INTO `qmgx_region` VALUES (3483, '653129', '伽师县', 372, 3, 0, 'Jiashi(Payzawat) Xian', 'JSI', '1-32-372-3483', '新疆维吾尔自治区-喀什地区-伽师县', 1);
INSERT INTO `qmgx_region` VALUES (3484, '653130', '巴楚县', 372, 3, 0, 'Bachu(Maralbexi) Xian', 'BCX', '1-32-372-3484', '新疆维吾尔自治区-喀什地区-巴楚县', 1);
INSERT INTO `qmgx_region` VALUES (3485, '653131', '塔什库尔干塔吉克自治县', 372, 3, 0, 'Taxkorgan Tajik Zizhixian', 'TXK', '1-32-372-3485', '新疆维吾尔自治区-喀什地区-塔什库尔干塔吉克自治县', 1);
INSERT INTO `qmgx_region` VALUES (3486, '653201', '和田市', 373, 3, 0, 'Hotan Shi', 'HTS', '1-32-373-3486', '新疆维吾尔自治区-和田地区-和田市', 1);
INSERT INTO `qmgx_region` VALUES (3487, '653221', '和田县', 373, 3, 0, 'Hotan Xian', 'HOT', '1-32-373-3487', '新疆维吾尔自治区-和田地区-和田县', 1);
INSERT INTO `qmgx_region` VALUES (3488, '653222', '墨玉县', 373, 3, 0, 'Moyu(Karakax) Xian', 'MOY', '1-32-373-3488', '新疆维吾尔自治区-和田地区-墨玉县', 1);
INSERT INTO `qmgx_region` VALUES (3489, '653223', '皮山县', 373, 3, 0, 'Pishan(Guma) Xian', 'PSA', '1-32-373-3489', '新疆维吾尔自治区-和田地区-皮山县', 1);
INSERT INTO `qmgx_region` VALUES (3490, '653224', '洛浦县', 373, 3, 0, 'Lop Xian', 'LOP', '1-32-373-3490', '新疆维吾尔自治区-和田地区-洛浦县', 1);
INSERT INTO `qmgx_region` VALUES (3491, '653225', '策勒县', 373, 3, 0, 'Qira Xian', 'QIR', '1-32-373-3491', '新疆维吾尔自治区-和田地区-策勒县', 1);
INSERT INTO `qmgx_region` VALUES (3492, '653226', '于田县', 373, 3, 0, 'Yutian(Keriya) Xian', 'YUT', '1-32-373-3492', '新疆维吾尔自治区-和田地区-于田县', 1);
INSERT INTO `qmgx_region` VALUES (3493, '653227', '民丰县', 373, 3, 0, 'Minfeng(Niya) Xian', 'MFG', '1-32-373-3493', '新疆维吾尔自治区-和田地区-民丰县', 1);
INSERT INTO `qmgx_region` VALUES (3494, '654002', '伊宁市', 374, 3, 0, 'Yining(Gulja) Shi', '2', '1-32-374-3494', '新疆维吾尔自治区-伊犁哈萨克自治州-伊宁市', 1);
INSERT INTO `qmgx_region` VALUES (3495, '654003', '奎屯市', 374, 3, 0, 'Kuytun Shi', '2', '1-32-374-3495', '新疆维吾尔自治区-伊犁哈萨克自治州-奎屯市', 1);
INSERT INTO `qmgx_region` VALUES (3496, '654021', '伊宁县', 374, 3, 0, 'Yining(Gulja) Xian', '2', '1-32-374-3496', '新疆维吾尔自治区-伊犁哈萨克自治州-伊宁县', 1);
INSERT INTO `qmgx_region` VALUES (3497, '654022', '察布查尔锡伯自治县', 374, 3, 0, 'Qapqal Xibe Zizhixian', '2', '1-32-374-3497', '新疆维吾尔自治区-伊犁哈萨克自治州-察布查尔锡伯自治县', 1);
INSERT INTO `qmgx_region` VALUES (3498, '654023', '霍城县', 374, 3, 0, 'Huocheng Xin', '2', '1-32-374-3498', '新疆维吾尔自治区-伊犁哈萨克自治州-霍城县', 1);
INSERT INTO `qmgx_region` VALUES (3499, '654024', '巩留县', 374, 3, 0, 'Gongliu(Tokkuztara) Xian', '2', '1-32-374-3499', '新疆维吾尔自治区-伊犁哈萨克自治州-巩留县', 1);
INSERT INTO `qmgx_region` VALUES (3500, '654025', '新源县', 374, 3, 0, 'Xinyuan(Kunes) Xian', '2', '1-32-374-3500', '新疆维吾尔自治区-伊犁哈萨克自治州-新源县', 1);
INSERT INTO `qmgx_region` VALUES (3501, '654026', '昭苏县', 374, 3, 0, 'Zhaosu(Mongolkure) Xian', '2', '1-32-374-3501', '新疆维吾尔自治区-伊犁哈萨克自治州-昭苏县', 1);
INSERT INTO `qmgx_region` VALUES (3502, '654027', '特克斯县', 374, 3, 0, 'Tekes Xian', '2', '1-32-374-3502', '新疆维吾尔自治区-伊犁哈萨克自治州-特克斯县', 1);
INSERT INTO `qmgx_region` VALUES (3503, '654028', '尼勒克县', 374, 3, 0, 'Nilka Xian', '2', '1-32-374-3503', '新疆维吾尔自治区-伊犁哈萨克自治州-尼勒克县', 1);
INSERT INTO `qmgx_region` VALUES (3504, '654201', '塔城市', 375, 3, 0, 'Tacheng(Qoqek) Shi', 'TCS', '1-32-375-3504', '新疆维吾尔自治区-塔城地区-塔城市', 1);
INSERT INTO `qmgx_region` VALUES (3505, '654202', '乌苏市', 375, 3, 0, 'Usu Shi', 'USU', '1-32-375-3505', '新疆维吾尔自治区-塔城地区-乌苏市', 1);
INSERT INTO `qmgx_region` VALUES (3506, '654221', '额敏县', 375, 3, 0, 'Emin(Dorbiljin) Xian', 'EMN', '1-32-375-3506', '新疆维吾尔自治区-塔城地区-额敏县', 1);
INSERT INTO `qmgx_region` VALUES (3507, '654223', '沙湾县', 375, 3, 0, 'Shawan Xian', 'SWX', '1-32-375-3507', '新疆维吾尔自治区-塔城地区-沙湾县', 1);
INSERT INTO `qmgx_region` VALUES (3508, '654224', '托里县', 375, 3, 0, 'Toli Xian', 'TLI', '1-32-375-3508', '新疆维吾尔自治区-塔城地区-托里县', 1);
INSERT INTO `qmgx_region` VALUES (3509, '654225', '裕民县', 375, 3, 0, 'Yumin(Qagantokay) Xian', 'YMN', '1-32-375-3509', '新疆维吾尔自治区-塔城地区-裕民县', 1);
INSERT INTO `qmgx_region` VALUES (3510, '654226', '和布克赛尔蒙古自治县', 375, 3, 0, 'Hebukesaiermengguzizhi Xian', '2', '1-32-375-3510', '新疆维吾尔自治区-塔城地区-和布克赛尔蒙古自治县', 1);
INSERT INTO `qmgx_region` VALUES (3511, '654301', '阿勒泰市', 376, 3, 0, 'Altay Shi', 'ALT', '1-32-376-3511', '新疆维吾尔自治区-阿勒泰地区-阿勒泰市', 1);
INSERT INTO `qmgx_region` VALUES (3512, '654321', '布尔津县', 376, 3, 0, 'Burqin Xian', 'BUX', '1-32-376-3512', '新疆维吾尔自治区-阿勒泰地区-布尔津县', 1);
INSERT INTO `qmgx_region` VALUES (3513, '654322', '富蕴县', 376, 3, 0, 'Fuyun(Koktokay) Xian', 'FYN', '1-32-376-3513', '新疆维吾尔自治区-阿勒泰地区-富蕴县', 1);
INSERT INTO `qmgx_region` VALUES (3514, '654323', '福海县', 376, 3, 0, 'Fuhai(Burultokay) Xian', 'FHI', '1-32-376-3514', '新疆维吾尔自治区-阿勒泰地区-福海县', 1);
INSERT INTO `qmgx_region` VALUES (3515, '654324', '哈巴河县', 376, 3, 0, 'Habahe(Kaba) Xian', 'HBH', '1-32-376-3515', '新疆维吾尔自治区-阿勒泰地区-哈巴河县', 1);
INSERT INTO `qmgx_region` VALUES (3516, '654325', '青河县', 376, 3, 0, 'Qinghe(Qinggil) Xian', 'QHX', '1-32-376-3516', '新疆维吾尔自治区-阿勒泰地区-青河县', 1);
INSERT INTO `qmgx_region` VALUES (3517, '654326', '吉木乃县', 376, 3, 0, 'Jeminay Xian', 'JEM', '1-32-376-3517', '新疆维吾尔自治区-阿勒泰地区-吉木乃县', 1);
INSERT INTO `qmgx_region` VALUES (3518, '659001', '石河子市', 377, 3, 0, 'Shihezi Shi', 'SHZ', '1-32-377-3518', '新疆维吾尔自治区-自治区直辖县级行政区划-石河子市', 1);
INSERT INTO `qmgx_region` VALUES (3519, '659002', '阿拉尔市', 377, 3, 0, 'Alaer Shi', '2', '1-32-377-3519', '新疆维吾尔自治区-自治区直辖县级行政区划-阿拉尔市', 1);
INSERT INTO `qmgx_region` VALUES (3520, '659003', '图木舒克市', 377, 3, 0, 'Tumushuke Shi', '2', '1-32-377-3520', '新疆维吾尔自治区-自治区直辖县级行政区划-图木舒克市', 1);
INSERT INTO `qmgx_region` VALUES (3521, '659004', '五家渠市', 377, 3, 0, 'Wujiaqu Shi', '2', '1-32-377-3521', '新疆维吾尔自治区-自治区直辖县级行政区划-五家渠市', 1);
INSERT INTO `qmgx_region` VALUES (4000, '620503', '麦积区', 340, 3, 0, 'Maiji Qu', '2', '1-29-340-4000', '甘肃省-天水市-麦积区', 1);
INSERT INTO `qmgx_region` VALUES (4001, '500116', '江津区', 270, 3, 0, 'Jiangjin Qu', '2', '1-23-270-4001', '重庆市-市辖区-江津区', 1);
INSERT INTO `qmgx_region` VALUES (4002, '500117', '合川区', 270, 3, 0, 'Hechuan Qu', '2', '1-23-270-4002', '重庆市-市辖区-合川区', 1);
INSERT INTO `qmgx_region` VALUES (4003, '500118', '永川区', 270, 3, 0, 'Yongchuan Qu', '2', '1-23-270-4003', '重庆市-市辖区-永川区', 1);
INSERT INTO `qmgx_region` VALUES (4004, '500119', '南川区', 270, 3, 0, 'Nanchuan Qu', '2', '1-23-270-4004', '重庆市-市辖区-南川区', 1);
INSERT INTO `qmgx_region` VALUES (4006, '340221', '芜湖县', 1412, 3, 0, 'Wuhu Xian', 'WHX', '-4006', '-芜湖市-芜湖县', 1);
INSERT INTO `qmgx_region` VALUES (4100, '232701', '加格达奇区', 106, 3, 0, 'Jiagedaqi Qu', '2', '1-9-106-4100', '黑龙江省-大兴安岭地区-加格达奇区', 1);
INSERT INTO `qmgx_region` VALUES (4101, '232702', '松岭区', 106, 3, 0, 'Songling Qu', '2', '1-9-106-4101', '黑龙江省-大兴安岭地区-松岭区', 1);
INSERT INTO `qmgx_region` VALUES (4102, '232703', '新林区', 106, 3, 0, 'Xinlin Qu', '2', '1-9-106-4102', '黑龙江省-大兴安岭地区-新林区', 1);
INSERT INTO `qmgx_region` VALUES (4103, '232704', '呼中区', 106, 3, 0, 'Huzhong Qu', '2', '1-9-106-4103', '黑龙江省-大兴安岭地区-呼中区', 1);
INSERT INTO `qmgx_region` VALUES (4200, '330402', '南湖区', 125, 3, 0, 'Nanhu Qu', '2', '1-12-125-4200', '浙江省-嘉兴市-南湖区', 1);
INSERT INTO `qmgx_region` VALUES (4300, '360482', '共青城市', 162, 3, 0, 'Gongqingcheng Shi', '2', '1-15-162-4300', '江西省-九江市-共青城市', 1);
INSERT INTO `qmgx_region` VALUES (4400, '640303', '红寺堡区', 360, 3, 0, 'Hongsibao Qu', '2', '1-31-360-4400', '宁夏回族自治区-吴忠市-红寺堡区', 1);
INSERT INTO `qmgx_region` VALUES (4500, '620922', '瓜州县', 344, 3, 0, 'Guazhou Xian', '2', '1-29-344-4500', '甘肃省-酒泉市-瓜州县', 1);
INSERT INTO `qmgx_region` VALUES (4600, '421321', '随县', 215, 3, 0, 'Sui Xian', '2', '1-18-215-4600', '湖北省-随州市-随县', 1);
INSERT INTO `qmgx_region` VALUES (4700, '431102', '零陵区', 228, 3, 0, 'Lingling Qu', '2', '1-19-228-4700', '湖南省-永州市-零陵区', 1);
INSERT INTO `qmgx_region` VALUES (4800, '451119', '平桂管理区', 263, 3, 0, 'Pingguiguanli Qu', '2', '1-21-263-4800', '广西壮族自治区-贺州市-平桂管理区', 1);
INSERT INTO `qmgx_region` VALUES (4900, '510802', '利州区', 279, 3, 0, 'Lizhou Qu', '2', '1-24-279-4900', '四川省-广元市-利州区', 1);
INSERT INTO `qmgx_region` VALUES (5000, '511681', '华蓥市', 286, 3, 0, 'Huaying Shi', 'HYC', '1-24-286-5000', '四川省-广安市-华蓥市', 1);
INSERT INTO `qmgx_region` VALUES (5001, '', '市辖区', 248, 3, 0, 'Shixiaqu', '2', '1-20-248-5001', '广东省-东莞市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (5002, '', '市辖区', 249, 3, 0, 'Shixiaqu', '2', '1-20-248-5002', '广东省-中山市-市辖区', 1);
INSERT INTO `qmgx_region` VALUES (5003, '', '南城区', 248, 3, 0, '', '', '1-20-348-5003', '', 1);
INSERT INTO `qmgx_region` VALUES (5004, '', '东城区', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5005, '', '万江区', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5006, '', '莞城区', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5007, '', '石龙镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5008, '', '虎门镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5009, '', '麻涌镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5010, '', '道滘镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5011, '', '石碣镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5012, '', '沙田镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5013, '', '望牛墩镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5014, '', '洪梅镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5015, '', '茶山镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5016, '', '寮步镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5017, '', '大岭山镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5018, '', '大朗镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5019, '', '黄江镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5020, '', '樟木头镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5021, '', '凤岗镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5022, '', '塘厦镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5023, '', '谢岗镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5024, '', '厚街镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5025, '', '清溪镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5026, '', '常平镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5027, '', '桥头镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5028, '', '横沥镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5029, '', '东坑镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5030, '', '企石镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5031, '', '石排镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5032, '', '长安镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5033, '', '中堂镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5034, '', '高埗镇', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5035, '', '松山湖', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5036, '', '其他区', 248, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5037, '', '石岐区街道', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5038, '', '东区街道', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5039, '', '西区街道', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5040, '', '五桂山镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5041, '', '南区街道', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5042, '', '小榄镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5043, '', '黄圃镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5044, '', '民众镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5045, '', '东凤镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5046, '', '东升镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5047, '', '古镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5048, '', '沙溪镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5049, '', '坦洲镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5050, '', '港口镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5051, '', '三角镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5052, '', '横栏镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5053, '', '南头镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5054, '', '阜沙镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5055, '', '南朗镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5056, '', '三乡镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5057, '', '板芙镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5058, '', '大涌镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5059, '', '神湾镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5060, '', '沙琅镇', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5061, '', '火炬开发区', 249, 3, 0, '', '', '', '', 1);
INSERT INTO `qmgx_region` VALUES (5062, '', '其他区', 249, 3, 0, '', '', '', '', 1);

-- ----------------------------
-- Table structure for qmgx_sell_goods
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_goods`;
CREATE TABLE `qmgx_sell_goods`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id1` int(11) NOT NULL DEFAULT 0 COMMENT '一级分类id',
  `category_id2` int(11) NOT NULL DEFAULT 0 COMMENT '二级分类id',
  `category_id3` int(11) NOT NULL DEFAULT 0 COMMENT '三级分类id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `short_title` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短标题',
  `keyword` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键词|隔开',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `video_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '视频地址 未用',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '默认售价',
  `original_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '默认原价',
  `can_coin` decimal(11, 1) NOT NULL DEFAULT 0.1 COMMENT '购物积分最高抵扣',
  `is_recommend` tinyint(1) NOT NULL DEFAULT 2 COMMENT '是否首页推荐 1 是 2 不是',
  `send_coin` decimal(11, 1) NOT NULL DEFAULT 0.0 COMMENT '赠送购物积分比例 百分比 商品售价',
  `send_sugar` decimal(11, 1) NOT NULL DEFAULT 0.0 COMMENT '赠送消费积分比例 百分比 商品售价',
  `real_sales` int(11) NOT NULL DEFAULT 0 COMMENT '真实销量',
  `sales_nums` int(11) NOT NULL DEFAULT 0 COMMENT '显示销量',
  `view_nums` int(11) NOT NULL DEFAULT 0 COMMENT '浏览量',
  `stock` int(11) NOT NULL DEFAULT 0 COMMENT '默认库存 无规格时',
  `dispatch_type` int(3) NOT NULL DEFAULT 1 COMMENT '运费计算类型 1统一 多件只收一次 2按件计费 每件单算',
  `dispatch_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '运费',
  `status` tinyint(2) NOT NULL DEFAULT 0 COMMENT '上架状态 0下架 1上架',
  `maxbuy` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单次最多购买 0空不限',
  `minbuy` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单次最少购买 0空不限',
  `allbuy` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '总共最多购买 0空不限',
  `display_order` int(11) NOT NULL DEFAULT 0 COMMENT '平台排序',
  `detail_id` int(11) NOT NULL DEFAULT 0 COMMENT '详情id',
  `use_spec` tinyint(2) NOT NULL DEFAULT 0 COMMENT '启用规格 0不启用 1启用',
  `spec_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格标题',
  `create_time` int(10) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) NULL DEFAULT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `in_category_id1`(`category_id1`) USING BTREE,
  INDEX `in_category_id2`(`category_id2`) USING BTREE,
  INDEX `in_category_id3`(`category_id3`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_goods
-- ----------------------------
INSERT INTO `qmgx_sell_goods` VALUES (2, 1, 2, 0, '羽绒服男短款加厚保暖冬季棉衣棉服户外工装李现同款衣服情侣外套', '', '羽绒服', '/uploads/image/10/18/2019/6a981d93ebf32b9982018705ffe91281.jpg', '', 599.00, 699.00, 100.0, 1, 1.0, 2.0, 4, 10004, 326, 50, 1, 0.00, 1, 0, 0, 0, 1, 2, 1, '尺寸', 1571382351, 1599489793);
INSERT INTO `qmgx_sell_goods` VALUES (3, 1, 2, 0, 'MAC子弹头', '', '', 'http://img.yizitao.cn/10a5597a177eede1d59b02509b9d2b51.png', '', 0.01, 0.01, 0.1, 1, 0.0, 0.0, 2, 2, 36, 98, 1, 0.00, 1, 0, 0, 0, 0, 3, 0, '', 1599225831, 1599488072);

-- ----------------------------
-- Table structure for qmgx_sell_goods_car
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_goods_car`;
CREATE TABLE `qmgx_sell_goods_car`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '会员id',
  `goods_id` int(11) NOT NULL DEFAULT 0 COMMENT '商品id',
  `spec_id` int(11) NOT NULL DEFAULT 0 COMMENT '规格id',
  `num` int(11) NOT NULL DEFAULT 0 COMMENT '商品件数',
  `create_time` int(10) NOT NULL COMMENT '添加时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '购物车' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_goods_car
-- ----------------------------
INSERT INTO `qmgx_sell_goods_car` VALUES (1, 8, 2, 7, 1, 1572416537, 1572416537);
INSERT INTO `qmgx_sell_goods_car` VALUES (2, 1, 2, 6, 1, 1589360944, 1589360944);

-- ----------------------------
-- Table structure for qmgx_sell_goods_collection
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_goods_collection`;
CREATE TABLE `qmgx_sell_goods_collection`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `goods_id` int(11) NOT NULL DEFAULT 0 COMMENT '商品',
  `create_time` int(10) NOT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_uid`(`uid`) USING BTREE,
  INDEX `index_goods_id`(`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品收藏' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_goods_collection
-- ----------------------------
INSERT INTO `qmgx_sell_goods_collection` VALUES (2, 8, 2, 1572419220);

-- ----------------------------
-- Table structure for qmgx_sell_goods_detail
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_goods_detail`;
CREATE TABLE `qmgx_sell_goods_detail`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `detail` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品详情' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_goods_detail
-- ----------------------------
INSERT INTO `qmgx_sell_goods_detail` VALUES (2, '<p>羽绒服今冬新品</p><p><img src=\"/uploads/image/10/18/2019/ef0a175935e8a4fbe93708ce289b33d0.jpg\"/></p><p><img src=\"/uploads/image/10/18/2019/9c8c1ea0e8713adf3ff8cb9949e473b1.jpg\"/></p><p><br/></p>');
INSERT INTO `qmgx_sell_goods_detail` VALUES (3, '<p>fewf</p>');

-- ----------------------------
-- Table structure for qmgx_sell_goods_img
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_goods_img`;
CREATE TABLE `qmgx_sell_goods_img`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL DEFAULT 0,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_goods_id`(`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品图片' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_goods_img
-- ----------------------------
INSERT INTO `qmgx_sell_goods_img` VALUES (28, 2, '/uploads/image/10/18/2019/72b9d4165aebd2ffeb7435aecafce49f.jpg');
INSERT INTO `qmgx_sell_goods_img` VALUES (29, 2, '/uploads/image/10/18/2019/287cc0fb65c0ca0ad7f3da7be567ca16.jpg');
INSERT INTO `qmgx_sell_goods_img` VALUES (30, 2, '/uploads/image/10/18/2019/2cc99038c0047c50182b95772dd9ca08.jpg');
INSERT INTO `qmgx_sell_goods_img` VALUES (31, 3, 'http://img.yizitao.cn/497943fe2b6c3edd2a8c96996667f111.png');

-- ----------------------------
-- Table structure for qmgx_sell_goods_spec
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_goods_spec`;
CREATE TABLE `qmgx_sell_goods_spec`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '规格名称',
  `price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '售价',
  `original_price` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '原价',
  `stock` int(11) NOT NULL DEFAULT 0 COMMENT '库存',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片地址',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_goods_id`(`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品规格' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_goods_spec
-- ----------------------------
INSERT INTO `qmgx_sell_goods_spec` VALUES (5, 2, 'L', 598.00, 699.00, 10, '/uploads/image/10/18/2019/cc35d0d860616ac447c578353e91c89e.jpg');
INSERT INTO `qmgx_sell_goods_spec` VALUES (6, 2, 'XL', 597.00, 699.00, 20, '/uploads/image/10/18/2019/b9a95e2c7c2bc2272b8b8197d5fa5480.jpg');
INSERT INTO `qmgx_sell_goods_spec` VALUES (7, 2, 'XLL', 596.00, 699.00, 21, '/uploads/image/10/18/2019/911dfb6ea312ae2bdb84d9385c819f66.jpg');

-- ----------------------------
-- Table structure for qmgx_sell_order
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_order`;
CREATE TABLE `qmgx_sell_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '发起订单会员id',
  `order_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `status` tinyint(2) NOT NULL DEFAULT 0 COMMENT '状态 -1已取消 0待支付 ，1已支付，2已发货，3已收货',
  `num` int(11) NOT NULL DEFAULT 0 COMMENT '商品数量',
  `goods_price` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '商品总价',
  `dispatch_price` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '运费总价',
  `total_price` decimal(12, 2) NOT NULL DEFAULT 0.00 COMMENT '订单总价',
  `coin` decimal(13, 2) NOT NULL DEFAULT 0.00 COMMENT '购物积分抵扣',
  `money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '支付金额',
  `create_time` int(10) NULL DEFAULT 0 COMMENT '订单创建时间',
  `pay_type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '现金部分支付类型 0 无 有抵扣不需要支付的情况 1微信 2余额',
  `pay_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方交易单号',
  `pay_money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '真正支付金额 第三方交易会返回',
  `pay_time` int(10) NULL DEFAULT NULL COMMENT '支付时间',
  `send_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人姓名',
  `send_mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人联系方式',
  `send_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人地址',
  `send_company` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流或快递公司名称',
  `send_sn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物流单号 快递单号',
  `send_time` int(10) NULL DEFAULT 0 COMMENT '发货时间',
  `receive_time` int(10) NULL DEFAULT 0 COMMENT '收货时间',
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户留言',
  `is_valid` tinyint(2) NOT NULL DEFAULT 1 COMMENT '前台用户 删除订单后变为0不显示 1显示 只在前端有效',
  `is_vip` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否可升级VIP 0否 1是 ',
  `send_coin` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '赠送购物积分',
  `send_sugar` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '赠送消费积分',
  `proid` int(11) NOT NULL DEFAULT 0 COMMENT '省份id',
  `cityid` int(11) NOT NULL DEFAULT 0 COMMENT '城市id',
  `areaid` int(11) NOT NULL DEFAULT 0 COMMENT '县区id',
  `is_return` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否执行过返利',
  `up1_id` int(11) NOT NULL DEFAULT 0 COMMENT '一级上级',
  `up1_price` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '一级上级返利',
  `up2_id` int(11) NOT NULL DEFAULT 0 COMMENT '二级上级',
  `up2_price` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '二级上级 返利',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_uid`(`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_order
-- ----------------------------
INSERT INTO `qmgx_sell_order` VALUES (1, 272, '20200908143005068670', -1, 1, 0.01, 0.00, 0.01, 0.00, 0.01, 1599546605, 0, NULL, 0.00, NULL, '佩奇', '15554832010', '内蒙古自治区 呼和浩特市 市辖区 山东省临沂市兰山区应用科学城', NULL, NULL, 0, 0, '', 1, 0, 0.00, 0.00, 6, 59, 727, 0, 0, 0.00, 0, 0.00);
INSERT INTO `qmgx_sell_order` VALUES (2, 272, '20200908143839924800', 1, 1, 0.01, 0.00, 0.01, 0.00, 0.01, 1599547119, 2, NULL, 0.01, 1599552196, '佩奇', '15554832010', '内蒙古自治区 呼和浩特市 市辖区 山东省临沂市兰山区应用科学城', NULL, NULL, 0, 0, '', 1, 0, 0.00, 0.00, 6, 59, 727, 0, 0, 0.00, 0, 0.00);
INSERT INTO `qmgx_sell_order` VALUES (3, 272, '20200908144735468284', 3, 1, 0.01, 0.00, 0.01, 0.00, 0.01, 1599547655, 2, NULL, 0.01, 1599547749, '佩奇', '15554832010', '内蒙古自治区 呼和浩特市 市辖区 山东省临沂市兰山区应用科学城', '纷纷为', '2341211234', 1599553020, 1599553505, '', 1, 0, 0.00, 0.00, 6, 59, 727, 0, 0, 0.00, 0, 0.00);

-- ----------------------------
-- Table structure for qmgx_sell_order_goods
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sell_order_goods`;
CREATE TABLE `qmgx_sell_order_goods`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL DEFAULT 0 COMMENT '订单表的id 是id',
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '会员id',
  `goods_id` int(11) NOT NULL DEFAULT 0 COMMENT '商品id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品缩略图',
  `spec_id` int(11) NOT NULL DEFAULT 0 COMMENT '规格id',
  `spec_title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格名称',
  `num` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  `price` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '单价',
  `can_coin` decimal(11, 1) NOT NULL DEFAULT 0.0 COMMENT '可用购物积分比例',
  `is_vip` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否vip升级商品',
  `send_coin` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '赠送购物积分',
  `send_sugar` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '赠送消费积分',
  `is_comment` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否评价暂未用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单商品表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_sell_order_goods
-- ----------------------------
INSERT INTO `qmgx_sell_order_goods` VALUES (1, 1, 272, 3, 'MAC子弹头', 'http://img.yizitao.cn/10a5597a177eede1d59b02509b9d2b51.png', 0, '', 1, 0.01, 0.1, 0, 0.00, 0.00, 0);
INSERT INTO `qmgx_sell_order_goods` VALUES (2, 2, 272, 3, 'MAC子弹头', 'http://img.yizitao.cn/10a5597a177eede1d59b02509b9d2b51.png', 0, '', 1, 0.01, 0.1, 0, 0.00, 0.00, 0);
INSERT INTO `qmgx_sell_order_goods` VALUES (3, 3, 272, 3, 'MAC子弹头', 'http://img.yizitao.cn/10a5597a177eede1d59b02509b9d2b51.png', 0, '', 1, 0.01, 0.1, 0, 0.00, 0.00, 0);

-- ----------------------------
-- Table structure for qmgx_service
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_service`;
CREATE TABLE `qmgx_service`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '1 游戏规则',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务中心' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_service
-- ----------------------------
INSERT INTO `qmgx_service` VALUES (6, '客服', '<p style=\"text-align: center;\"><img src=\"http://img.yizitao.cn/72562e1f41edb128e231d1a5a19c0e2c.png\" title=\"\" alt=\"\"/></p><p style=\"text-align: center;\">客服服务时间:11:00-21:00</p><p><br/></p><p><br/></p>', 2);
INSERT INTO `qmgx_service` VALUES (3, '怎么联系我们', '<p>微信客服：XXX</p>', 1);

-- ----------------------------
-- Table structure for qmgx_shop_goods
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_shop_goods`;
CREATE TABLE `qmgx_shop_goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` int(11) NOT NULL COMMENT '领养价值开始范围',
  `daynum` int(11) NULL DEFAULT 0 COMMENT '收益周期',
  `dayearn` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '每日收益',
  `isshow` tinyint(3) NOT NULL DEFAULT 1 COMMENT '1显示2隐藏',
  `sort` int(11) NULL DEFAULT 0,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `buynum` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '每人可购买',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '矿机' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_shop_goods
-- ----------------------------
INSERT INTO `qmgx_shop_goods` VALUES (23, '小型矿机', 1000, 20, 75.00, 1, 70, '/static/home/images/v10_a16.png', 3);
INSERT INTO `qmgx_shop_goods` VALUES (24, '中型矿机', 3000, 20, 225.00, 1, 65, '/static/home/images/v10_a16.png', 2);
INSERT INTO `qmgx_shop_goods` VALUES (25, '大型矿机', 5000, 20, 375.00, 1, 60, '/static/home/images/v10_a16.png', 1);
INSERT INTO `qmgx_shop_goods` VALUES (26, '宇宙矿机', 8000, 20, 600.00, 1, 55, '/static/home/images/v10_a16.png', 1);
INSERT INTO `qmgx_shop_goods` VALUES (27, '神级矿机', 10000, 20, 750.00, 1, 50, '/static/home/images/v10_a16.png', 1);

-- ----------------------------
-- Table structure for qmgx_shopmy
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_shopmy`;
CREATE TABLE `qmgx_shopmy`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 1 COMMENT '购买人',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` int(11) NOT NULL COMMENT '领养价值开始范围',
  `daynum` int(11) NULL DEFAULT 0 COMMENT '收益周期',
  `dayearn` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '每日收益',
  `usedaynum` int(11) NULL DEFAULT 0 COMMENT '已发放天数',
  `useearn` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '共得到收益',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品图片',
  `buytime` int(11) NULL DEFAULT NULL COMMENT '购买时间',
  `proid` int(11) NULL DEFAULT 0 COMMENT '产品id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_shopmy
-- ----------------------------
INSERT INTO `qmgx_shopmy` VALUES (1, 10, '神级矿机', 10000, 20, 750.00, 21, 1500.00, '/static/home/images/v10_a16.png', 1596633891, 27);
INSERT INTO `qmgx_shopmy` VALUES (2, 4, '小型矿机', 1000, 20, 75.00, 0, 0.00, '/static/home/images/v10_a16.png', 1599369017, 23);

-- ----------------------------
-- Table structure for qmgx_shopmylist
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_shopmylist`;
CREATE TABLE `qmgx_shopmylist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopid` int(11) NULL DEFAULT NULL COMMENT '矿机id',
  `uid` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `sendtime` int(11) NULL DEFAULT NULL COMMENT '开始发放时间',
  `state` smallint(1) NULL DEFAULT NULL COMMENT '1未发放2已发放',
  `edittime` int(11) NULL DEFAULT NULL COMMENT '变更时间',
  `dayearn` decimal(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_shopmylist
-- ----------------------------
INSERT INTO `qmgx_shopmylist` VALUES (1, 1, 10, 1596643200, 2, 1596643201, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (2, 1, 10, 1596729600, 2, 1596729601, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (3, 1, 10, 1596816000, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (4, 1, 10, 1596902400, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (5, 1, 10, 1596988800, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (6, 1, 10, 1597075200, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (7, 1, 10, 1597161600, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (8, 1, 10, 1597248000, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (9, 1, 10, 1597334400, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (10, 1, 10, 1597420800, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (11, 1, 10, 1597507200, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (12, 1, 10, 1597593600, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (13, 1, 10, 1597680000, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (14, 1, 10, 1597766400, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (15, 1, 10, 1597852800, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (16, 1, 10, 1597939200, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (17, 1, 10, 1598025600, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (18, 1, 10, 1598112000, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (19, 1, 10, 1598198400, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (20, 1, 10, 1598284800, 1, NULL, 750.00);
INSERT INTO `qmgx_shopmylist` VALUES (21, 2, 4, 1599408000, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (22, 2, 4, 1599494400, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (23, 2, 4, 1599580800, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (24, 2, 4, 1599667200, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (25, 2, 4, 1599753600, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (26, 2, 4, 1599840000, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (27, 2, 4, 1599926400, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (28, 2, 4, 1600012800, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (29, 2, 4, 1600099200, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (30, 2, 4, 1600185600, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (31, 2, 4, 1600272000, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (32, 2, 4, 1600358400, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (33, 2, 4, 1600444800, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (34, 2, 4, 1600531200, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (35, 2, 4, 1600617600, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (36, 2, 4, 1600704000, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (37, 2, 4, 1600790400, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (38, 2, 4, 1600876800, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (39, 2, 4, 1600963200, 1, NULL, 75.00);
INSERT INTO `qmgx_shopmylist` VALUES (40, 2, 4, 1601049600, 1, NULL, 75.00);

-- ----------------------------
-- Table structure for qmgx_sms_log
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_sms_log`;
CREATE TABLE `qmgx_sms_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '验证码手机号',
  `vcode` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '短信验证码',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '验证码状态 0 未发送 1 未使用 2 已使用 3 已过期  -1 发送失败',
  `exp_time` int(12) NOT NULL COMMENT '过期时间',
  `type` int(2) NOT NULL COMMENT '验证码类型  1 注册  2 重置密码 3 登录 4 订单提醒',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '发送失败提示',
  `create_time` int(12) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `telephone、vcode`(`telephone`, `vcode`) USING BTREE,
  INDEX `telephone、type`(`telephone`, `type`, `status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 401 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '短信发送记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_sms_log
-- ----------------------------
INSERT INTO `qmgx_sms_log` VALUES (1, '15376068375', '420286', 2, 1596105862, 1, '', 1596105622);
INSERT INTO `qmgx_sms_log` VALUES (2, '15376068375', '654273', 2, 1596105935, 1, '', 1596105695);
INSERT INTO `qmgx_sms_log` VALUES (3, '18922336751', '0000', 1, 1596590501, 4, '', 1596589501);
INSERT INTO `qmgx_sms_log` VALUES (4, '18922336751', '0000', 1, 1596590501, 4, '', 1596589501);
INSERT INTO `qmgx_sms_log` VALUES (5, '17009125357', '0000', 1, 1596590502, 4, '', 1596589502);
INSERT INTO `qmgx_sms_log` VALUES (6, '16676238051', '0000', 1, 1596590502, 4, '', 1596589502);
INSERT INTO `qmgx_sms_log` VALUES (7, '18888888888', '0000', 1, 1596590503, 4, '', 1596589503);
INSERT INTO `qmgx_sms_log` VALUES (8, '17009125358', '0000', 1, 1596593982, 4, '', 1596592982);
INSERT INTO `qmgx_sms_log` VALUES (9, '17009125358', '0000', 1, 1596593982, 4, '', 1596592982);
INSERT INTO `qmgx_sms_log` VALUES (10, '17013226770', '0000', 1, 1596593982, 4, '', 1596592982);
INSERT INTO `qmgx_sms_log` VALUES (11, '17009125358', '0000', 1, 1596593983, 4, '', 1596592983);
INSERT INTO `qmgx_sms_log` VALUES (12, '17009125358', '0000', 1, 1596593983, 4, '', 1596592983);
INSERT INTO `qmgx_sms_log` VALUES (13, '17009125358', '0000', 1, 1596593984, 4, '', 1596592984);
INSERT INTO `qmgx_sms_log` VALUES (14, '18888888888', '0000', 1, 1596593984, 4, '', 1596592984);
INSERT INTO `qmgx_sms_log` VALUES (15, '17013226770', '0000', 1, 1596593984, 4, '', 1596592984);
INSERT INTO `qmgx_sms_log` VALUES (16, '17009125358', '0000', 1, 1596593984, 4, '', 1596592984);
INSERT INTO `qmgx_sms_log` VALUES (17, '17009125357', '0000', 1, 1596593985, 4, '', 1596592985);
INSERT INTO `qmgx_sms_log` VALUES (18, '17009125358', '0000', 1, 1596594402, 4, '', 1596593402);
INSERT INTO `qmgx_sms_log` VALUES (19, '17009125358', '0000', 1, 1596594407, 4, '', 1596593407);
INSERT INTO `qmgx_sms_log` VALUES (20, '17009125357', '0000', 1, 1596594408, 4, '', 1596593408);
INSERT INTO `qmgx_sms_log` VALUES (21, '18146613930', '212465', 2, 1596596992, 1, '', 1596596752);
INSERT INTO `qmgx_sms_log` VALUES (22, '18857970824', '665146', 2, 1596597305, 1, '', 1596597065);
INSERT INTO `qmgx_sms_log` VALUES (23, '18909943315', '846246', 2, 1596597315, 1, '', 1596597075);
INSERT INTO `qmgx_sms_log` VALUES (24, '15523518952', '658848', 2, 1596597434, 1, '', 1596597194);
INSERT INTO `qmgx_sms_log` VALUES (25, '15523518952', '682330', 2, 1596597496, 1, '', 1596597256);
INSERT INTO `qmgx_sms_log` VALUES (26, '15523518952', '685506', 2, 1596597558, 1, '', 1596597318);
INSERT INTO `qmgx_sms_log` VALUES (27, '17054497911', '266430', 2, 1596597891, 1, '', 1596597651);
INSERT INTO `qmgx_sms_log` VALUES (28, '17321049709', '288322', 1, 1596597985, 1, '', 1596597745);
INSERT INTO `qmgx_sms_log` VALUES (29, '18094444666', '754536', 2, 1596598124, 1, '', 1596597884);
INSERT INTO `qmgx_sms_log` VALUES (30, '17321049862', '638082', 2, 1596598151, 1, '', 1596597911);
INSERT INTO `qmgx_sms_log` VALUES (31, '18779238342', '421346', 2, 1596598297, 1, '', 1596598057);
INSERT INTO `qmgx_sms_log` VALUES (32, '15739624884', '562648', 2, 1596598449, 1, '', 1596598209);
INSERT INTO `qmgx_sms_log` VALUES (33, '15242536257', '654082', 3, 1596598452, 1, '', 1596598212);
INSERT INTO `qmgx_sms_log` VALUES (34, '15157863554', '954412', 2, 1596598636, 1, '', 1596598396);
INSERT INTO `qmgx_sms_log` VALUES (35, '18060172936', '855244', 2, 1596598636, 1, '', 1596598396);
INSERT INTO `qmgx_sms_log` VALUES (36, '18196160520', '282673', 2, 1596598702, 1, '', 1596598462);
INSERT INTO `qmgx_sms_log` VALUES (37, '18196160520', '872261', 2, 1596598786, 1, '', 1596598546);
INSERT INTO `qmgx_sms_log` VALUES (38, '17326099563', '092044', 2, 1596598826, 1, '', 1596598586);
INSERT INTO `qmgx_sms_log` VALUES (39, '15888738658', '240429', 2, 1596599091, 1, '', 1596598851);
INSERT INTO `qmgx_sms_log` VALUES (40, '15393265376', '744283', 2, 1596599234, 1, '', 1596598994);
INSERT INTO `qmgx_sms_log` VALUES (41, '13059385678', '416087', 2, 1596599245, 1, '', 1596599005);
INSERT INTO `qmgx_sms_log` VALUES (42, '19959850596', '522256', 2, 1596599458, 1, '', 1596599218);
INSERT INTO `qmgx_sms_log` VALUES (43, '18027195555', '862877', 2, 1596599856, 1, '', 1596599616);
INSERT INTO `qmgx_sms_log` VALUES (44, '15881879193', '769077', 2, 1596600061, 1, '', 1596599821);
INSERT INTO `qmgx_sms_log` VALUES (45, '13533576777', '749648', 2, 1596600327, 1, '', 1596600087);
INSERT INTO `qmgx_sms_log` VALUES (46, '13533576777', '280148', 2, 1596600390, 1, '', 1596600150);
INSERT INTO `qmgx_sms_log` VALUES (47, '13770135227', '244492', 2, 1596601073, 1, '', 1596600833);
INSERT INTO `qmgx_sms_log` VALUES (48, '18073923127', '468216', 2, 1596601785, 1, '', 1596601545);
INSERT INTO `qmgx_sms_log` VALUES (49, '13636966250', '586384', 2, 1596601786, 1, '', 1596601546);
INSERT INTO `qmgx_sms_log` VALUES (50, '18333779381', '488481', 2, 1596602016, 1, '', 1596601776);
INSERT INTO `qmgx_sms_log` VALUES (51, '13772145686', '814842', 2, 1596602219, 1, '', 1596601979);
INSERT INTO `qmgx_sms_log` VALUES (52, '13193741098', '662264', 2, 1596602621, 1, '', 1596602381);
INSERT INTO `qmgx_sms_log` VALUES (53, '18128134200', '029912', 2, 1596603413, 1, '', 1596603173);
INSERT INTO `qmgx_sms_log` VALUES (54, '15651536223', '968899', 1, 1596603547, 1, '', 1596603307);
INSERT INTO `qmgx_sms_log` VALUES (55, '18627259520', '006528', 2, 1596603707, 1, '', 1596603467);
INSERT INTO `qmgx_sms_log` VALUES (56, '15077060025', '486272', 2, 1596604785, 1, '', 1596604545);
INSERT INTO `qmgx_sms_log` VALUES (57, '17078198437', '442744', 1, 1596605073, 1, '', 1596604833);
INSERT INTO `qmgx_sms_log` VALUES (58, '17075011614', '841268', 2, 1596605073, 1, '', 1596604833);
INSERT INTO `qmgx_sms_log` VALUES (59, '17056491293', '854464', 2, 1596605073, 1, '', 1596604833);
INSERT INTO `qmgx_sms_log` VALUES (60, '17053874517', '888202', 2, 1596605073, 1, '', 1596604833);
INSERT INTO `qmgx_sms_log` VALUES (61, '16569715475', '440627', 2, 1596605073, 1, '', 1596604833);
INSERT INTO `qmgx_sms_log` VALUES (62, '18092011959', '840082', 2, 1596605250, 1, '', 1596605010);
INSERT INTO `qmgx_sms_log` VALUES (63, '17075011614', '632868', 2, 1596605254, 1, '', 1596605014);
INSERT INTO `qmgx_sms_log` VALUES (64, '19805055208', '114406', 2, 1596605267, 1, '', 1596605027);
INSERT INTO `qmgx_sms_log` VALUES (65, '17075011614', '022662', 2, 1596605330, 1, '', 1596605090);
INSERT INTO `qmgx_sms_log` VALUES (66, '16533283457', '489662', 2, 1596605584, 1, '', 1596605344);
INSERT INTO `qmgx_sms_log` VALUES (67, '17075011398', '472888', 2, 1596605790, 1, '', 1596605550);
INSERT INTO `qmgx_sms_log` VALUES (68, '16534063013', '779712', 2, 1596605854, 1, '', 1596605614);
INSERT INTO `qmgx_sms_log` VALUES (69, '16533420220', '662623', 2, 1596606108, 1, '', 1596605868);
INSERT INTO `qmgx_sms_log` VALUES (70, '19655756618', '288674', 1, 1596606493, 1, '', 1596606253);
INSERT INTO `qmgx_sms_log` VALUES (71, '18024168164', '016053', 2, 1596606508, 1, '', 1596606268);
INSERT INTO `qmgx_sms_log` VALUES (72, '16569711581', '750281', 2, 1596606553, 1, '', 1596606313);
INSERT INTO `qmgx_sms_log` VALUES (73, '18655756618', '446809', 2, 1596606557, 1, '', 1596606317);
INSERT INTO `qmgx_sms_log` VALUES (74, '17053660796', '283143', 2, 1596606646, 1, '', 1596606406);
INSERT INTO `qmgx_sms_log` VALUES (75, '16533420287', '536763', 2, 1596606692, 1, '', 1596606452);
INSERT INTO `qmgx_sms_log` VALUES (76, '16725453273', '225628', 2, 1596607172, 1, '', 1596606932);
INSERT INTO `qmgx_sms_log` VALUES (77, '15888738658', '0000', 1, 1596608201, 4, '', 1596607201);
INSERT INTO `qmgx_sms_log` VALUES (78, '15888738658', '0000', 1, 1596608202, 4, '', 1596607202);
INSERT INTO `qmgx_sms_log` VALUES (79, '19088962350', '0000', 1, 1596608202, 4, '', 1596607202);
INSERT INTO `qmgx_sms_log` VALUES (80, '13428874792', '201420', 2, 1596607504, 1, '', 1596607264);
INSERT INTO `qmgx_sms_log` VALUES (81, '13428874792', '664825', 2, 1596607567, 1, '', 1596607327);
INSERT INTO `qmgx_sms_log` VALUES (82, '15967293599', '986354', 2, 1596607867, 1, '', 1596607627);
INSERT INTO `qmgx_sms_log` VALUES (83, '16565707485', '549665', 2, 1596607978, 1, '', 1596607738);
INSERT INTO `qmgx_sms_log` VALUES (84, '17885933724', '660791', 2, 1596608091, 1, '', 1596607851);
INSERT INTO `qmgx_sms_log` VALUES (85, '13288838098', '480668', 1, 1596608130, 1, '', 1596607890);
INSERT INTO `qmgx_sms_log` VALUES (86, '16536921264', '168757', 2, 1596608200, 1, '', 1596607960);
INSERT INTO `qmgx_sms_log` VALUES (87, '18563272732', '542521', 2, 1596608445, 1, '', 1596608205);
INSERT INTO `qmgx_sms_log` VALUES (88, '19151002353', '486786', 2, 1596608538, 1, '', 1596608298);
INSERT INTO `qmgx_sms_log` VALUES (89, '16573055396', '229272', 2, 1596608664, 1, '', 1596608424);
INSERT INTO `qmgx_sms_log` VALUES (90, '17075011689', '224617', 2, 1596608721, 1, '', 1596608481);
INSERT INTO `qmgx_sms_log` VALUES (91, '17102905640', '518656', 2, 1596608749, 1, '', 1596608509);
INSERT INTO `qmgx_sms_log` VALUES (92, '13867704109', '768596', 2, 1596609100, 1, '', 1596608860);
INSERT INTO `qmgx_sms_log` VALUES (93, '13169119767', '953812', 2, 1596609233, 1, '', 1596608993);
INSERT INTO `qmgx_sms_log` VALUES (94, '13733434061', '841281', 2, 1596609426, 1, '', 1596609186);
INSERT INTO `qmgx_sms_log` VALUES (95, '13600774732', '562427', 2, 1596609734, 1, '', 1596609494);
INSERT INTO `qmgx_sms_log` VALUES (96, '18675765704', '724264', 2, 1596610251, 1, '', 1596610011);
INSERT INTO `qmgx_sms_log` VALUES (97, '15519841075', '643284', 2, 1596610680, 1, '', 1596610440);
INSERT INTO `qmgx_sms_log` VALUES (98, '15507474770', '184686', 2, 1596610797, 1, '', 1596610557);
INSERT INTO `qmgx_sms_log` VALUES (99, '13725608186', '454224', 2, 1596611238, 1, '', 1596610998);
INSERT INTO `qmgx_sms_log` VALUES (100, '15261185382', '358625', 2, 1596611256, 1, '', 1596611016);
INSERT INTO `qmgx_sms_log` VALUES (101, '13424519886', '616293', 2, 1596611425, 1, '', 1596611185);
INSERT INTO `qmgx_sms_log` VALUES (102, '13977488752', '653444', 2, 1596611489, 1, '', 1596611249);
INSERT INTO `qmgx_sms_log` VALUES (103, '15242536257', '672188', 2, 1596611614, 1, '', 1596611374);
INSERT INTO `qmgx_sms_log` VALUES (104, '13392298144', '348811', 2, 1596611616, 1, '', 1596611376);
INSERT INTO `qmgx_sms_log` VALUES (105, '13818287089', '028461', 2, 1596612038, 1, '', 1596611798);
INSERT INTO `qmgx_sms_log` VALUES (106, '16591806765', '316815', 1, 1596612203, 1, '', 1596611963);
INSERT INTO `qmgx_sms_log` VALUES (107, '13591806765', '613046', 2, 1596612267, 1, '', 1596612027);
INSERT INTO `qmgx_sms_log` VALUES (108, '16533280514', '224493', 2, 1596613013, 1, '', 1596612773);
INSERT INTO `qmgx_sms_log` VALUES (109, '18097883326', '0000', 1, 1596613901, 4, '', 1596612901);
INSERT INTO `qmgx_sms_log` VALUES (110, '18097883326', '0000', 1, 1596613902, 4, '', 1596612902);
INSERT INTO `qmgx_sms_log` VALUES (111, '17009125357', '0000', 1, 1596613902, 4, '', 1596612902);
INSERT INTO `qmgx_sms_log` VALUES (112, '17013226770', '0000', 1, 1596613902, 4, '', 1596612902);
INSERT INTO `qmgx_sms_log` VALUES (113, '17009125358', '0000', 1, 1596613903, 4, '', 1596612903);
INSERT INTO `qmgx_sms_log` VALUES (114, '18888888888', '0000', 1, 1596613903, 4, '', 1596612903);
INSERT INTO `qmgx_sms_log` VALUES (115, '17045675477', '412123', 2, 1596613465, 1, '', 1596613225);
INSERT INTO `qmgx_sms_log` VALUES (116, '16676238055', '0000', 1, 1596615221, 4, '', 1596614221);
INSERT INTO `qmgx_sms_log` VALUES (117, '18888888888', '0000', 1, 1596615221, 4, '', 1596614221);
INSERT INTO `qmgx_sms_log` VALUES (118, '16676238051', '0000', 1, 1596615401, 4, '', 1596614401);
INSERT INTO `qmgx_sms_log` VALUES (119, '17009125357', '0000', 1, 1596615402, 4, '', 1596614402);
INSERT INTO `qmgx_sms_log` VALUES (120, '13860151579', '668866', 2, 1596614808, 1, '', 1596614568);
INSERT INTO `qmgx_sms_log` VALUES (121, '17015568075', '0000', 1, 1596615881, 4, '', 1596614881);
INSERT INTO `qmgx_sms_log` VALUES (122, '17015568075', '0000', 1, 1596615881, 4, '', 1596614881);
INSERT INTO `qmgx_sms_log` VALUES (123, '17009125357', '0000', 1, 1596615882, 4, '', 1596614882);
INSERT INTO `qmgx_sms_log` VALUES (124, '18942461602', '868397', 2, 1596615401, 1, '', 1596615161);
INSERT INTO `qmgx_sms_log` VALUES (125, '13380256200', '038685', 2, 1596615456, 1, '', 1596615216);
INSERT INTO `qmgx_sms_log` VALUES (126, '18942461602', '208928', 2, 1596615468, 1, '', 1596615228);
INSERT INTO `qmgx_sms_log` VALUES (127, '16569714094', '128068', 2, 1596616188, 1, '', 1596615948);
INSERT INTO `qmgx_sms_log` VALUES (128, '18922336751', '0000', 1, 1596618101, 4, '', 1596617101);
INSERT INTO `qmgx_sms_log` VALUES (129, '18922336751', '0000', 1, 1596618102, 4, '', 1596617102);
INSERT INTO `qmgx_sms_log` VALUES (130, '17009125357', '0000', 1, 1596618102, 4, '', 1596617102);
INSERT INTO `qmgx_sms_log` VALUES (131, '13259725221', '334592', 2, 1596617994, 1, '', 1596617754);
INSERT INTO `qmgx_sms_log` VALUES (132, '13378410370', '411204', 2, 1596618155, 1, '', 1596617915);
INSERT INTO `qmgx_sms_log` VALUES (133, '18998986916', '482465', 2, 1596620567, 1, '', 1596620327);
INSERT INTO `qmgx_sms_log` VALUES (134, '17635146198', '877724', 2, 1596621032, 1, '', 1596620792);
INSERT INTO `qmgx_sms_log` VALUES (135, '15024261701', '877265', 2, 1596622162, 1, '', 1596621922);
INSERT INTO `qmgx_sms_log` VALUES (136, '13878311535', '002114', 2, 1596624043, 1, '', 1596623803);
INSERT INTO `qmgx_sms_log` VALUES (137, '18177314836', '290562', 2, 1596624831, 1, '', 1596624591);
INSERT INTO `qmgx_sms_log` VALUES (138, '15877004910', '828816', 2, 1596625376, 1, '', 1596625136);
INSERT INTO `qmgx_sms_log` VALUES (139, '17078751436', '793106', 2, 1596627600, 1, '', 1596627360);
INSERT INTO `qmgx_sms_log` VALUES (140, '18808108991', '622405', 2, 1596627768, 1, '', 1596627528);
INSERT INTO `qmgx_sms_log` VALUES (141, '15656208559', '384840', 2, 1596628244, 1, '', 1596628004);
INSERT INTO `qmgx_sms_log` VALUES (142, '17725918819', '512646', 2, 1596628506, 1, '', 1596628266);
INSERT INTO `qmgx_sms_log` VALUES (143, '17359536459', '602668', 2, 1596629115, 1, '', 1596628875);
INSERT INTO `qmgx_sms_log` VALUES (144, '13635133928', '234668', 1, 1596630919, 1, '', 1596630679);
INSERT INTO `qmgx_sms_log` VALUES (145, '13635133929', '665027', 2, 1596631002, 1, '', 1596630762);
INSERT INTO `qmgx_sms_log` VALUES (146, '18080824501', '288196', 2, 1596631958, 1, '', 1596631718);
INSERT INTO `qmgx_sms_log` VALUES (147, '16533425245', '888166', 2, 1596632051, 1, '', 1596631811);
INSERT INTO `qmgx_sms_log` VALUES (148, '13014434402', '762248', 2, 1596632223, 1, '', 1596631983);
INSERT INTO `qmgx_sms_log` VALUES (149, '13751165165', '673242', 2, 1596632229, 1, '', 1596631989);
INSERT INTO `qmgx_sms_log` VALUES (150, '13324886313', '885894', 2, 1596632273, 1, '', 1596632033);
INSERT INTO `qmgx_sms_log` VALUES (151, '13607741112', '484225', 2, 1596632342, 1, '', 1596632102);
INSERT INTO `qmgx_sms_log` VALUES (152, '13607741112', '162777', 2, 1596632428, 1, '', 1596632188);
INSERT INTO `qmgx_sms_log` VALUES (153, '13607741112', '275018', 3, 1596632506, 1, '', 1596632266);
INSERT INTO `qmgx_sms_log` VALUES (154, '13607741112', '782630', 2, 1596632832, 1, '', 1596632592);
INSERT INTO `qmgx_sms_log` VALUES (155, '17078756936', '283000', 2, 1596633593, 1, '', 1596633353);
INSERT INTO `qmgx_sms_log` VALUES (156, '16676238055', '0000', 1, 1596635081, 4, '', 1596634081);
INSERT INTO `qmgx_sms_log` VALUES (157, '18888888888', '0000', 1, 1596635082, 4, '', 1596634082);
INSERT INTO `qmgx_sms_log` VALUES (158, '17078757328', '183271', 2, 1596634953, 1, '', 1596634713);
INSERT INTO `qmgx_sms_log` VALUES (159, '15277438698', '534648', 2, 1596635855, 1, '', 1596635615);
INSERT INTO `qmgx_sms_log` VALUES (160, '18020400707', '448625', 2, 1596636394, 1, '', 1596636154);
INSERT INTO `qmgx_sms_log` VALUES (161, '17620948536', '668148', 2, 1596636665, 1, '', 1596636425);
INSERT INTO `qmgx_sms_log` VALUES (162, '15298195591', '429343', 2, 1596637052, 1, '', 1596636812);
INSERT INTO `qmgx_sms_log` VALUES (163, '15153441278', '772122', 2, 1596637225, 1, '', 1596636985);
INSERT INTO `qmgx_sms_log` VALUES (164, '18670546609', '987389', 2, 1596638566, 1, '', 1596638326);
INSERT INTO `qmgx_sms_log` VALUES (165, '13037617108', '787442', 2, 1596638905, 1, '', 1596638665);
INSERT INTO `qmgx_sms_log` VALUES (166, '13037617108', '704686', 2, 1596639024, 1, '', 1596638784);
INSERT INTO `qmgx_sms_log` VALUES (167, '13037617108', '661894', 2, 1596639126, 1, '', 1596638886);
INSERT INTO `qmgx_sms_log` VALUES (168, '13037617108', '122802', 2, 1596639190, 1, '', 1596638950);
INSERT INTO `qmgx_sms_log` VALUES (169, '13037617108', '022322', 2, 1596639261, 1, '', 1596639021);
INSERT INTO `qmgx_sms_log` VALUES (170, '17602285795', '844538', 2, 1596640255, 1, '', 1596640015);
INSERT INTO `qmgx_sms_log` VALUES (171, '18703614501', '858892', 2, 1596641582, 1, '', 1596641342);
INSERT INTO `qmgx_sms_log` VALUES (172, '18703614501', '342848', 2, 1596641645, 1, '', 1596641405);
INSERT INTO `qmgx_sms_log` VALUES (173, '18703614501', '242169', 2, 1596641758, 1, '', 1596641518);
INSERT INTO `qmgx_sms_log` VALUES (174, '17075012642', '856853', 2, 1596642838, 1, '', 1596642598);
INSERT INTO `qmgx_sms_log` VALUES (175, '18218877635', '918609', 2, 1596643197, 1, '', 1596642957);
INSERT INTO `qmgx_sms_log` VALUES (176, '18778994890', '862682', 2, 1596644866, 1, '', 1596644626);
INSERT INTO `qmgx_sms_log` VALUES (177, '17053874904', '298838', 2, 1596644899, 1, '', 1596644659);
INSERT INTO `qmgx_sms_log` VALUES (178, '17078751496', '920695', 2, 1596646187, 1, '', 1596645947);
INSERT INTO `qmgx_sms_log` VALUES (179, '17089150807', '887688', 2, 1596647501, 1, '', 1596647261);
INSERT INTO `qmgx_sms_log` VALUES (180, '19976159913', '242214', 2, 1596653233, 1, '', 1596652993);
INSERT INTO `qmgx_sms_log` VALUES (181, '18191518314', '323448', 2, 1596672998, 1, '', 1596672758);
INSERT INTO `qmgx_sms_log` VALUES (182, '17015568091', '0000', 1, 1596676001, 4, '', 1596675001);
INSERT INTO `qmgx_sms_log` VALUES (183, '17015568075', '0000', 1, 1596676002, 4, '', 1596675002);
INSERT INTO `qmgx_sms_log` VALUES (184, '18888888888', '0000', 1, 1596676002, 4, '', 1596675002);
INSERT INTO `qmgx_sms_log` VALUES (185, '13657465084', '619863', 2, 1596678847, 1, '', 1596678607);
INSERT INTO `qmgx_sms_log` VALUES (186, '13152641738', '667748', 2, 1596680233, 1, '', 1596679993);
INSERT INTO `qmgx_sms_log` VALUES (187, '18060719833', '847648', 2, 1596680589, 1, '', 1596680349);
INSERT INTO `qmgx_sms_log` VALUES (188, '18780598359', '721782', 2, 1596681509, 1, '', 1596681269);
INSERT INTO `qmgx_sms_log` VALUES (189, '13348833776', '988141', 2, 1596681538, 1, '', 1596681298);
INSERT INTO `qmgx_sms_log` VALUES (190, '13232613109', '485714', 2, 1596682593, 1, '', 1596682353);
INSERT INTO `qmgx_sms_log` VALUES (191, '15578189475', '869811', 2, 1596682875, 1, '', 1596682635);
INSERT INTO `qmgx_sms_log` VALUES (192, '15578959354', '165692', 2, 1596682999, 1, '', 1596682759);
INSERT INTO `qmgx_sms_log` VALUES (193, '13612525209', '405240', 2, 1596683044, 1, '', 1596682804);
INSERT INTO `qmgx_sms_log` VALUES (194, '15578959354', '432262', 2, 1596683075, 1, '', 1596682835);
INSERT INTO `qmgx_sms_log` VALUES (195, '15578959354', '703680', 2, 1596683104, 1, '', 1596682864);
INSERT INTO `qmgx_sms_log` VALUES (196, '18559213078', '808697', 2, 1596683230, 1, '', 1596682990);
INSERT INTO `qmgx_sms_log` VALUES (197, '17043763133', '328624', 2, 1596683569, 1, '', 1596683329);
INSERT INTO `qmgx_sms_log` VALUES (198, '17043763135', '910478', 2, 1596683671, 1, '', 1596683431);
INSERT INTO `qmgx_sms_log` VALUES (199, '17043763135', '851988', 2, 1596683733, 1, '', 1596683493);
INSERT INTO `qmgx_sms_log` VALUES (200, '13008691184', '992484', 3, 1596683806, 1, '', 1596683566);
INSERT INTO `qmgx_sms_log` VALUES (201, '13117612694', '228647', 2, 1596683849, 1, '', 1596683609);
INSERT INTO `qmgx_sms_log` VALUES (202, '17043763135', '383244', 2, 1596683871, 1, '', 1596683631);
INSERT INTO `qmgx_sms_log` VALUES (203, '17043763135', '134928', 2, 1596683943, 1, '', 1596683703);
INSERT INTO `qmgx_sms_log` VALUES (204, '17043763134', '862263', 2, 1596684101, 1, '', 1596683861);
INSERT INTO `qmgx_sms_log` VALUES (205, '15778456883', '208619', 2, 1596684322, 1, '', 1596684082);
INSERT INTO `qmgx_sms_log` VALUES (206, '13008691184', '528846', 3, 1596684503, 1, '', 1596684263);
INSERT INTO `qmgx_sms_log` VALUES (207, '13553993451', '441622', 2, 1596684759, 1, '', 1596684519);
INSERT INTO `qmgx_sms_log` VALUES (208, '13008691184', '728782', 3, 1596685482, 1, '', 1596685242);
INSERT INTO `qmgx_sms_log` VALUES (209, '15172612130', '606673', 2, 1596685898, 1, '', 1596685658);
INSERT INTO `qmgx_sms_log` VALUES (210, '13565360343', '987020', 2, 1596686663, 1, '', 1596686423);
INSERT INTO `qmgx_sms_log` VALUES (211, '16574070359', '456437', 1, 1596686689, 1, '', 1596686449);
INSERT INTO `qmgx_sms_log` VALUES (212, '13008691184', '280248', 3, 1596686736, 1, '', 1596686496);
INSERT INTO `qmgx_sms_log` VALUES (213, '17049457820', '021908', 1, 1596686848, 1, '', 1596686608);
INSERT INTO `qmgx_sms_log` VALUES (214, '17049457837', '172464', 1, 1596686898, 1, '', 1596686658);
INSERT INTO `qmgx_sms_log` VALUES (215, '17199848308', '114826', 1, 1596686943, 1, '', 1596686703);
INSERT INTO `qmgx_sms_log` VALUES (216, '17199848318', '922621', 1, 1596686952, 1, '', 1596686712);
INSERT INTO `qmgx_sms_log` VALUES (217, '17199848319', '920209', 1, 1596687033, 1, '', 1596686793);
INSERT INTO `qmgx_sms_log` VALUES (218, '17116720479', '761163', 1, 1596687034, 1, '', 1596686794);
INSERT INTO `qmgx_sms_log` VALUES (219, '17116728903', '982394', 1, 1596687034, 1, '', 1596686794);
INSERT INTO `qmgx_sms_log` VALUES (220, '15260201502', '292888', 2, 1596687062, 1, '', 1596686822);
INSERT INTO `qmgx_sms_log` VALUES (221, '17199846379', '048197', 1, 1596687309, 1, '', 1596687069);
INSERT INTO `qmgx_sms_log` VALUES (222, '17199842939', '943164', 1, 1596687317, 1, '', 1596687077);
INSERT INTO `qmgx_sms_log` VALUES (223, '17199847264', '566648', 1, 1596687319, 1, '', 1596687079);
INSERT INTO `qmgx_sms_log` VALUES (224, '17049459728', '016562', 1, 1596687509, 1, '', 1596687269);
INSERT INTO `qmgx_sms_log` VALUES (225, '17049459792', '856148', 1, 1596687666, 1, '', 1596687426);
INSERT INTO `qmgx_sms_log` VALUES (226, '13008691184', '158864', 3, 1596688102, 1, '', 1596687862);
INSERT INTO `qmgx_sms_log` VALUES (227, '13647741485', '460246', 2, 1596688123, 1, '', 1596687883);
INSERT INTO `qmgx_sms_log` VALUES (228, '18778856434', '066880', 1, 1596688380, 1, '', 1596688140);
INSERT INTO `qmgx_sms_log` VALUES (229, '16252194704', '675446', 2, 1596688444, 1, '', 1596688204);
INSERT INTO `qmgx_sms_log` VALUES (230, '18778856484', '123430', 2, 1596688448, 1, '', 1596688208);
INSERT INTO `qmgx_sms_log` VALUES (231, '18778856484', '011995', 2, 1596688603, 1, '', 1596688363);
INSERT INTO `qmgx_sms_log` VALUES (232, '18978383275', '936266', 2, 1596689874, 1, '', 1596689634);
INSERT INTO `qmgx_sms_log` VALUES (233, '15846963781', '616444', 2, 1596690464, 1, '', 1596690224);
INSERT INTO `qmgx_sms_log` VALUES (234, '15213052015', '816860', 2, 1596690721, 1, '', 1596690481);
INSERT INTO `qmgx_sms_log` VALUES (235, '15213052015', '428819', 2, 1596690806, 1, '', 1596690566);
INSERT INTO `qmgx_sms_log` VALUES (236, '17082522406', '604824', 1, 1596691506, 1, '', 1596691266);
INSERT INTO `qmgx_sms_log` VALUES (237, '15779625550', '989207', 2, 1596692292, 1, '', 1596692052);
INSERT INTO `qmgx_sms_log` VALUES (238, '17053665957', '737142', 2, 1596692776, 1, '', 1596692536);
INSERT INTO `qmgx_sms_log` VALUES (239, '13058645536', '826281', 2, 1596694019, 1, '', 1596693779);
INSERT INTO `qmgx_sms_log` VALUES (240, '13008691184', '220262', 3, 1596694035, 1, '', 1596693795);
INSERT INTO `qmgx_sms_log` VALUES (241, '13008691184', '288222', 3, 1596694317, 1, '', 1596694077);
INSERT INTO `qmgx_sms_log` VALUES (242, '13008691184', '723426', 2, 1596694603, 1, '', 1596694363);
INSERT INTO `qmgx_sms_log` VALUES (243, '15077498429', '788632', 2, 1596695738, 1, '', 1596695498);
INSERT INTO `qmgx_sms_log` VALUES (244, '18878314214', '766524', 2, 1596697586, 1, '', 1596697346);
INSERT INTO `qmgx_sms_log` VALUES (245, '15578959354', '727266', 2, 1596699831, 1, '', 1596699591);
INSERT INTO `qmgx_sms_log` VALUES (246, '16530741698', '964346', 1, 1596701198, 1, '', 1596700958);
INSERT INTO `qmgx_sms_log` VALUES (247, '17150489491', '861792', 1, 1596701270, 1, '', 1596701030);
INSERT INTO `qmgx_sms_log` VALUES (248, '17199941153', '308192', 1, 1596701332, 1, '', 1596701092);
INSERT INTO `qmgx_sms_log` VALUES (249, '17199941161', '488132', 2, 1596701530, 1, '', 1596701290);
INSERT INTO `qmgx_sms_log` VALUES (250, '18599145467', '484269', 2, 1596707064, 1, '', 1596706824);
INSERT INTO `qmgx_sms_log` VALUES (251, '13534098898', '106184', 2, 1596708025, 1, '', 1596707785);
INSERT INTO `qmgx_sms_log` VALUES (252, '15280892732', '823367', 2, 1596709179, 1, '', 1596708939);
INSERT INTO `qmgx_sms_log` VALUES (253, '13205882308', '414422', 2, 1596711854, 1, '', 1596711614);
INSERT INTO `qmgx_sms_log` VALUES (254, '16625134519', '449165', 2, 1596717371, 1, '', 1596717131);
INSERT INTO `qmgx_sms_log` VALUES (255, '15259423587', '288860', 2, 1596717706, 1, '', 1596717466);
INSERT INTO `qmgx_sms_log` VALUES (256, '18575599846', '448246', 2, 1596718501, 1, '', 1596718261);
INSERT INTO `qmgx_sms_log` VALUES (257, '18575599846', '228954', 2, 1596718569, 1, '', 1596718329);
INSERT INTO `qmgx_sms_log` VALUES (258, '18575599846', '424476', 2, 1596718629, 1, '', 1596718389);
INSERT INTO `qmgx_sms_log` VALUES (259, '18642269577', '888620', 3, 1596718861, 1, '', 1596718621);
INSERT INTO `qmgx_sms_log` VALUES (260, '18642269577', '458628', 2, 1596719116, 1, '', 1596718876);
INSERT INTO `qmgx_sms_log` VALUES (261, '15208435373', '548408', 2, 1596722983, 1, '', 1596722743);
INSERT INTO `qmgx_sms_log` VALUES (262, '15113573121', '684422', 2, 1596722996, 1, '', 1596722756);
INSERT INTO `qmgx_sms_log` VALUES (263, '17725990137', '612829', 2, 1596723774, 1, '', 1596723534);
INSERT INTO `qmgx_sms_log` VALUES (264, '13210376667', '479994', 2, 1596724451, 1, '', 1596724211);
INSERT INTO `qmgx_sms_log` VALUES (265, '15263819407', '062444', 2, 1596724894, 1, '', 1596724654);
INSERT INTO `qmgx_sms_log` VALUES (266, '18703614501', '123106', 2, 1596725022, 1, '', 1596724782);
INSERT INTO `qmgx_sms_log` VALUES (267, '16530800938', '482256', 2, 1596725463, 1, '', 1596725223);
INSERT INTO `qmgx_sms_log` VALUES (268, '18866478504', '284428', 2, 1596725954, 1, '', 1596725714);
INSERT INTO `qmgx_sms_log` VALUES (269, '16830800935', '684497', 1, 1596726237, 1, '', 1596725997);
INSERT INTO `qmgx_sms_log` VALUES (270, '17771934420', '662683', 2, 1596726382, 1, '', 1596726142);
INSERT INTO `qmgx_sms_log` VALUES (271, '17771934420', '648462', 2, 1596726445, 1, '', 1596726205);
INSERT INTO `qmgx_sms_log` VALUES (272, '18866674206', '048746', 2, 1596726797, 1, '', 1596726557);
INSERT INTO `qmgx_sms_log` VALUES (273, '18866674206', '526648', 2, 1596727090, 1, '', 1596726850);
INSERT INTO `qmgx_sms_log` VALUES (274, '16530801186', '659414', 2, 1596727462, 1, '', 1596727222);
INSERT INTO `qmgx_sms_log` VALUES (275, '16530801186', '828794', 2, 1596727562, 1, '', 1596727322);
INSERT INTO `qmgx_sms_log` VALUES (276, '18359245826', '859882', 2, 1596727731, 1, '', 1596727491);
INSERT INTO `qmgx_sms_log` VALUES (277, '18866670045', '365664', 2, 1596728033, 1, '', 1596727793);
INSERT INTO `qmgx_sms_log` VALUES (278, '16536921427', '264484', 2, 1596728240, 1, '', 1596728000);
INSERT INTO `qmgx_sms_log` VALUES (279, '17089150253', '228613', 1, 1596728454, 1, '', 1596728214);
INSERT INTO `qmgx_sms_log` VALUES (280, '17078757140', '332420', 2, 1596728472, 1, '', 1596728232);
INSERT INTO `qmgx_sms_log` VALUES (281, '16530800926', '224211', 2, 1596728586, 1, '', 1596728346);
INSERT INTO `qmgx_sms_log` VALUES (282, '16530801187', '636269', 2, 1596728835, 1, '', 1596728595);
INSERT INTO `qmgx_sms_log` VALUES (283, '16530801187', '388401', 3, 1596728909, 1, '', 1596728669);
INSERT INTO `qmgx_sms_log` VALUES (284, '16530801187', '226782', 2, 1596729191, 1, '', 1596728951);
INSERT INTO `qmgx_sms_log` VALUES (285, '17078759821', '414591', 2, 1596729364, 1, '', 1596729124);
INSERT INTO `qmgx_sms_log` VALUES (286, '18866674189', '306154', 2, 1596729451, 1, '', 1596729211);
INSERT INTO `qmgx_sms_log` VALUES (287, '17078756825', '704246', 2, 1596729482, 1, '', 1596729242);
INSERT INTO `qmgx_sms_log` VALUES (288, '18866674189', '881244', 2, 1596729648, 1, '', 1596729408);
INSERT INTO `qmgx_sms_log` VALUES (289, '16530800923', '684825', 2, 1596729934, 1, '', 1596729694);
INSERT INTO `qmgx_sms_log` VALUES (290, '16530800923', '390188', 2, 1596729997, 1, '', 1596729757);
INSERT INTO `qmgx_sms_log` VALUES (291, '16530800934', '998168', 2, 1596730369, 1, '', 1596730129);
INSERT INTO `qmgx_sms_log` VALUES (292, '17078755528', '428743', 2, 1596730580, 1, '', 1596730340);
INSERT INTO `qmgx_sms_log` VALUES (293, '16530800924', '232586', 2, 1596730756, 1, '', 1596730516);
INSERT INTO `qmgx_sms_log` VALUES (294, '16569713298', '868988', 2, 1596730893, 1, '', 1596730653);
INSERT INTO `qmgx_sms_log` VALUES (295, '17030288024', '252165', 2, 1596731208, 1, '', 1596730968);
INSERT INTO `qmgx_sms_log` VALUES (296, '18866478814', '622656', 2, 1596731497, 1, '', 1596731257);
INSERT INTO `qmgx_sms_log` VALUES (297, '16725453137', '471324', 2, 1596731521, 1, '', 1596731281);
INSERT INTO `qmgx_sms_log` VALUES (298, '16565708431', '092668', 2, 1596731835, 1, '', 1596731595);
INSERT INTO `qmgx_sms_log` VALUES (299, '15263819432', '484281', 2, 1596731861, 1, '', 1596731621);
INSERT INTO `qmgx_sms_log` VALUES (300, '17075012003', '334685', 2, 1596732185, 1, '', 1596731945);
INSERT INTO `qmgx_sms_log` VALUES (301, '18866478974', '266022', 2, 1596732250, 1, '', 1596732010);
INSERT INTO `qmgx_sms_log` VALUES (302, '17030436249', '642464', 2, 1596732500, 1, '', 1596732260);
INSERT INTO `qmgx_sms_log` VALUES (303, '18866674223', '661824', 2, 1596732643, 1, '', 1596732403);
INSERT INTO `qmgx_sms_log` VALUES (304, '18866674223', '828448', 2, 1596732755, 1, '', 1596732515);
INSERT INTO `qmgx_sms_log` VALUES (305, '17053874127', '183866', 2, 1596732816, 1, '', 1596732576);
INSERT INTO `qmgx_sms_log` VALUES (306, '18866674223', '068636', 2, 1596732870, 1, '', 1596732630);
INSERT INTO `qmgx_sms_log` VALUES (307, '18866674223', '825027', 2, 1596733048, 1, '', 1596732808);
INSERT INTO `qmgx_sms_log` VALUES (308, '17089155248', '172682', 2, 1596733131, 1, '', 1596732891);
INSERT INTO `qmgx_sms_log` VALUES (309, '16569714996', '988186', 2, 1596733443, 1, '', 1596733203);
INSERT INTO `qmgx_sms_log` VALUES (310, '18866674208', '298768', 2, 1596733449, 1, '', 1596733209);
INSERT INTO `qmgx_sms_log` VALUES (311, '17089150804', '146567', 2, 1596733758, 1, '', 1596733518);
INSERT INTO `qmgx_sms_log` VALUES (312, '16530800946', '854594', 2, 1596733845, 1, '', 1596733605);
INSERT INTO `qmgx_sms_log` VALUES (313, '17056491410', '446947', 2, 1596734070, 1, '', 1596733830);
INSERT INTO `qmgx_sms_log` VALUES (314, '16530800918', '702227', 2, 1596734164, 1, '', 1596733924);
INSERT INTO `qmgx_sms_log` VALUES (315, '16569717175', '233967', 1, 1596734391, 1, '', 1596734151);
INSERT INTO `qmgx_sms_log` VALUES (316, '19159148617', '221706', 1, 1596734671, 1, '', 1596734431);
INSERT INTO `qmgx_sms_log` VALUES (317, '17190252358', '166966', 1, 1596734761, 1, '', 1596734521);
INSERT INTO `qmgx_sms_log` VALUES (318, '18866478647', '261228', 2, 1596734764, 1, '', 1596734524);
INSERT INTO `qmgx_sms_log` VALUES (319, '18866478647', '432228', 2, 1596734865, 1, '', 1596734625);
INSERT INTO `qmgx_sms_log` VALUES (320, '17089150461', '281022', 2, 1596735129, 1, '', 1596734889);
INSERT INTO `qmgx_sms_log` VALUES (321, '17030438436', '781141', 2, 1596735440, 1, '', 1596735200);
INSERT INTO `qmgx_sms_log` VALUES (322, '17061124235', '269286', 2, 1596735754, 1, '', 1596735514);
INSERT INTO `qmgx_sms_log` VALUES (323, '18857835758', '986406', 2, 1596735936, 1, '', 1596735696);
INSERT INTO `qmgx_sms_log` VALUES (324, '16725452319', '262218', 2, 1596736070, 1, '', 1596735830);
INSERT INTO `qmgx_sms_log` VALUES (325, '16725453342', '165689', 2, 1596736754, 1, '', 1596736514);
INSERT INTO `qmgx_sms_log` VALUES (326, '16533283478', '380648', 2, 1596737068, 1, '', 1596736828);
INSERT INTO `qmgx_sms_log` VALUES (327, '17078750362', '667286', 2, 1596737389, 1, '', 1596737149);
INSERT INTO `qmgx_sms_log` VALUES (328, '16572950537', '527258', 2, 1596737703, 1, '', 1596737463);
INSERT INTO `qmgx_sms_log` VALUES (329, '16533282470', '587264', 1, 1596738016, 1, '', 1596737776);
INSERT INTO `qmgx_sms_log` VALUES (330, '16534060422', '666882', 2, 1596738388, 1, '', 1596738148);
INSERT INTO `qmgx_sms_log` VALUES (331, '17077614738', '748443', 2, 1596738701, 1, '', 1596738461);
INSERT INTO `qmgx_sms_log` VALUES (332, '17089153972', '643484', 2, 1596739012, 1, '', 1596738772);
INSERT INTO `qmgx_sms_log` VALUES (333, '17053584708', '586226', 2, 1596739323, 1, '', 1596739083);
INSERT INTO `qmgx_sms_log` VALUES (334, '16534242820', '892853', 2, 1596739636, 1, '', 1596739396);
INSERT INTO `qmgx_sms_log` VALUES (335, '17053660261', '646289', 2, 1596739946, 1, '', 1596739706);
INSERT INTO `qmgx_sms_log` VALUES (336, '16533282570', '144723', 1, 1596740270, 1, '', 1596740030);
INSERT INTO `qmgx_sms_log` VALUES (337, '17078756721', '628486', 2, 1596740639, 1, '', 1596740399);
INSERT INTO `qmgx_sms_log` VALUES (338, '17030474399', '354025', 2, 1596740951, 1, '', 1596740711);
INSERT INTO `qmgx_sms_log` VALUES (339, '17078756383', '444444', 2, 1596741262, 1, '', 1596741022);
INSERT INTO `qmgx_sms_log` VALUES (340, '17089152923', '158982', 2, 1596741573, 1, '', 1596741333);
INSERT INTO `qmgx_sms_log` VALUES (341, '17089151952', '524286', 2, 1596741885, 1, '', 1596741645);
INSERT INTO `qmgx_sms_log` VALUES (342, '17089154609', '724760', 2, 1596742194, 1, '', 1596741954);
INSERT INTO `qmgx_sms_log` VALUES (343, '16536384241', '206635', 2, 1596742507, 1, '', 1596742267);
INSERT INTO `qmgx_sms_log` VALUES (344, '17053661021', '034603', 2, 1596742822, 1, '', 1596742582);
INSERT INTO `qmgx_sms_log` VALUES (345, '17089151809', '882684', 2, 1596743143, 1, '', 1596742903);
INSERT INTO `qmgx_sms_log` VALUES (346, '16533420071', '308688', 2, 1596743824, 1, '', 1596743584);
INSERT INTO `qmgx_sms_log` VALUES (347, '17078757408', '842488', 2, 1596744139, 1, '', 1596743899);
INSERT INTO `qmgx_sms_log` VALUES (348, '17045672499', '856688', 2, 1596753792, 1, '', 1596753552);
INSERT INTO `qmgx_sms_log` VALUES (349, '17061087247', '277405', 2, 1596754482, 1, '', 1596754242);
INSERT INTO `qmgx_sms_log` VALUES (350, '17030438480', '443690', 2, 1596754815, 1, '', 1596754575);
INSERT INTO `qmgx_sms_log` VALUES (351, '17078756253', '418214', 2, 1596755125, 1, '', 1596754885);
INSERT INTO `qmgx_sms_log` VALUES (352, '16536384214', '128832', 1, 1596755435, 1, '', 1596755195);
INSERT INTO `qmgx_sms_log` VALUES (353, '17089151206', '456160', 2, 1596757204, 1, '', 1596756964);
INSERT INTO `qmgx_sms_log` VALUES (354, '16569718277', '421666', 2, 1596757513, 1, '', 1596757273);
INSERT INTO `qmgx_sms_log` VALUES (355, '17078751812', '785479', 2, 1596757841, 1, '', 1596757601);
INSERT INTO `qmgx_sms_log` VALUES (356, '17030472804', '648068', 2, 1596758157, 1, '', 1596757917);
INSERT INTO `qmgx_sms_log` VALUES (357, '17089153181', '848225', 2, 1596758470, 1, '', 1596758230);
INSERT INTO `qmgx_sms_log` VALUES (358, '17089151265', '284063', 2, 1596758780, 1, '', 1596758540);
INSERT INTO `qmgx_sms_log` VALUES (359, '17089158073', '077384', 2, 1596759091, 1, '', 1596758851);
INSERT INTO `qmgx_sms_log` VALUES (360, '17089157421', '227292', 2, 1596759402, 1, '', 1596759162);
INSERT INTO `qmgx_sms_log` VALUES (361, '17078750389', '952462', 2, 1596759714, 1, '', 1596759474);
INSERT INTO `qmgx_sms_log` VALUES (362, '17051517433', '042274', 2, 1596760026, 1, '', 1596759786);
INSERT INTO `qmgx_sms_log` VALUES (363, '16725453328', '644563', 2, 1596760350, 1, '', 1596760110);
INSERT INTO `qmgx_sms_log` VALUES (364, '16569714055', '442292', 2, 1596760662, 1, '', 1596760422);
INSERT INTO `qmgx_sms_log` VALUES (365, '17053660354', '072880', 2, 1596760974, 1, '', 1596760734);
INSERT INTO `qmgx_sms_log` VALUES (366, '17089153301', '948001', 2, 1596761289, 1, '', 1596761049);
INSERT INTO `qmgx_sms_log` VALUES (367, '17875179993', '920128', 2, 1596761312, 1, '', 1596761072);
INSERT INTO `qmgx_sms_log` VALUES (368, '16533422075', '085466', 2, 1596761601, 1, '', 1596761361);
INSERT INTO `qmgx_sms_log` VALUES (369, '18097883326', '0000', 1, 1596762402, 4, '', 1596761402);
INSERT INTO `qmgx_sms_log` VALUES (370, '18097883326', '0000', 1, 1596762402, 4, '', 1596761402);
INSERT INTO `qmgx_sms_log` VALUES (371, '17015568091', '0000', 1, 1596762402, 4, '', 1596761402);
INSERT INTO `qmgx_sms_log` VALUES (372, '13193741098', '0000', 1, 1596762402, 4, '', 1596761402);
INSERT INTO `qmgx_sms_log` VALUES (373, '18888888888', '0000', 1, 1596762403, 4, '', 1596761403);
INSERT INTO `qmgx_sms_log` VALUES (374, '16533420957', '566266', 2, 1596761912, 1, '', 1596761672);
INSERT INTO `qmgx_sms_log` VALUES (375, '17089155989', '658556', 2, 1596762225, 1, '', 1596761985);
INSERT INTO `qmgx_sms_log` VALUES (376, '17030474324', '930362', 2, 1596762535, 1, '', 1596762295);
INSERT INTO `qmgx_sms_log` VALUES (377, '17056111645', '156848', 2, 1596762848, 1, '', 1596762608);
INSERT INTO `qmgx_sms_log` VALUES (378, '17030286414', '456624', 2, 1596763162, 1, '', 1596762922);
INSERT INTO `qmgx_sms_log` VALUES (379, '17089152326', '588464', 2, 1596763473, 1, '', 1596763233);
INSERT INTO `qmgx_sms_log` VALUES (380, '18865266521', '624344', 2, 1596763740, 1, '', 1596763500);
INSERT INTO `qmgx_sms_log` VALUES (381, '17058578685', '246868', 1, 1596763786, 1, '', 1596763546);
INSERT INTO `qmgx_sms_log` VALUES (382, '17089157706', '353473', 2, 1596764154, 1, '', 1596763914);
INSERT INTO `qmgx_sms_log` VALUES (383, '15083651085', '642320', 3, 1596764206, 1, '', 1596763966);
INSERT INTO `qmgx_sms_log` VALUES (384, '15083651085', '422860', 2, 1596764460, 1, '', 1596764220);
INSERT INTO `qmgx_sms_log` VALUES (385, '17089151568', '068858', 2, 1596764467, 1, '', 1596764227);
INSERT INTO `qmgx_sms_log` VALUES (386, '15501598007', '420681', 2, 1596764573, 1, '', 1596764333);
INSERT INTO `qmgx_sms_log` VALUES (387, '13553398421', '955606', 2, 1596764710, 1, '', 1596764470);
INSERT INTO `qmgx_sms_log` VALUES (388, '17089154021', '982788', 2, 1596764779, 1, '', 1596764539);
INSERT INTO `qmgx_sms_log` VALUES (389, '15873486769', '664668', 2, 1596764822, 1, '', 1596764582);
INSERT INTO `qmgx_sms_log` VALUES (390, '17075012169', '893033', 2, 1596765091, 1, '', 1596764851);
INSERT INTO `qmgx_sms_log` VALUES (391, '16533422403', '880630', 2, 1596765418, 1, '', 1596765178);
INSERT INTO `qmgx_sms_log` VALUES (392, '17089150385', '302034', 2, 1596765729, 1, '', 1596765489);
INSERT INTO `qmgx_sms_log` VALUES (393, '18551606499', '542225', 1, 1596766766, 1, '', 1596766526);
INSERT INTO `qmgx_sms_log` VALUES (394, '13203075100', '666393', 1, 1596766879, 1, '', 1596766639);
INSERT INTO `qmgx_sms_log` VALUES (395, '15673420129', '902308', 1, 1596767021, 1, '', 1596766781);
INSERT INTO `qmgx_sms_log` VALUES (396, '18773407584', '757828', 2, 1596767373, 1, '', 1596767133);
INSERT INTO `qmgx_sms_log` VALUES (397, '15312555382', '486261', 2, 1596768214, 1, '', 1596767974);
INSERT INTO `qmgx_sms_log` VALUES (398, '17663569502', '449829', 2, 1596769032, 1, '', 1596768792);
INSERT INTO `qmgx_sms_log` VALUES (399, '15554832010', '642224', 0, 1599142890, 1, '', 1599142650);
INSERT INTO `qmgx_sms_log` VALUES (400, '15554832010', '448388', 2, 1599339257, 1, '', 1599339017);

-- ----------------------------
-- Table structure for qmgx_swiper
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_swiper`;
CREATE TABLE `qmgx_swiper`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简介 或 备注都行',
  `type` int(3) NOT NULL DEFAULT 1 COMMENT '跳转类型',
  `link_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '跳转链接 或者 id',
  `is_visible` int(1) NOT NULL DEFAULT 1 COMMENT '1显示',
  `display_order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '幻灯片' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_swiper
-- ----------------------------
INSERT INTO `qmgx_swiper` VALUES (5, 'http://img.yjkpt.cn/41945dd0e42f01c5a2ef8a61060d0426.png', 'fdf', 2, '3', 1, 10, 1599487140, 1599487160);

-- ----------------------------
-- Table structure for qmgx_trade_order
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_trade_order`;
CREATE TABLE `qmgx_trade_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `num` int(11) NOT NULL COMMENT '求购数量',
  `price` decimal(10, 3) NOT NULL COMMENT '单价',
  `create_time` int(11) NOT NULL COMMENT '提交时间',
  `sell_user_id` int(11) NOT NULL COMMENT '出售人',
  `status` int(2) NOT NULL COMMENT '求购单状态 1 未处理 2 已配对，待支付 3 已支付,待确认  4 确认完成',
  `match_time` int(11) NOT NULL COMMENT '配对时间',
  `pay_time` int(11) NOT NULL COMMENT '支付时间',
  `sure_time` int(11) NOT NULL COMMENT '确认时间',
  `pay_type` int(1) NOT NULL COMMENT '支付方式 1 微信支付 2 支付宝支付',
  `pay_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打款截图',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_status`(`status`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qmgx_trade_timeline
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_trade_timeline`;
CREATE TABLE `qmgx_trade_timeline`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_price` decimal(10, 3) NOT NULL COMMENT '当日价格',
  `day_time` date NOT NULL COMMENT '当日日期',
  `bonus` decimal(15, 3) NOT NULL COMMENT '周分红数量',
  `qiugou` decimal(15, 3) NOT NULL COMMENT '总求购数量',
  `deal` decimal(15, 3) NOT NULL COMMENT '总成交量',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否生效 1 未生效 2 已生效',
  `update_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_day_yime`(`day_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '每日行情信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qmgx_user
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_user`;
CREATE TABLE `qmgx_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商城用户注册登录表',
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录账号',
  `levelid` int(11) NULL DEFAULT 1 COMMENT '会员等级',
  `selfphone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `pid` int(11) NULL DEFAULT NULL COMMENT ' 上级id',
  `pidcount` int(11) NULL DEFAULT 0 COMMENT '我的直推人数量',
  `recommend` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐人账号',
  `teamcount` int(11) NULL DEFAULT 0 COMMENT '团队人数',
  `rank` tinyint(3) UNSIGNED NULL DEFAULT 1 COMMENT '等级：12345',
  `myprofit` decimal(20, 2) NULL DEFAULT 0.00 COMMENT '我的累计收益',
  `is_jingli` int(15) NOT NULL DEFAULT 0 COMMENT '0不是经理1是经理',
  `user_status` smallint(1) NULL DEFAULT 0 COMMENT '用户是否已激活  1 已激活 其他 未激活',
  `passwd` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户密码',
  `sepasswd` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '二级密码',
  `emailname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付宝昵称或姓名',
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付宝账号',
  `emailcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付宝收款码',
  `realname` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实名字',
  `selfcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `mustman` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '紧急联系人',
  `selfqqname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信名',
  `selfqq` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信账号',
  `selfqqcode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信收款码',
  `mybot` decimal(10, 2) NULL DEFAULT 0.00 COMMENT 'bot数量',
  `act_coin` int(11) NOT NULL DEFAULT 0 COMMENT '激活币钱包',
  `is_active` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1：可登录；0：不可登录',
  `activemanuid` int(11) NULL DEFAULT NULL COMMENT '激活我的人uid',
  `banktypename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行类型',
  `bankname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开户银行',
  `bankcard` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '银行卡号',
  `integral` decimal(20, 2) NULL DEFAULT 0.00 COMMENT '商城积分',
  `dongtai` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '动态钱包',
  `regtime` datetime NULL DEFAULT NULL COMMENT '注册时间',
  `regip` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `pdpin` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排单币',
  `wagesall` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '工资钱包',
  `pushs` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '直推奖励',
  `quota` int(11) NOT NULL DEFAULT 0 COMMENT '红包配额',
  `is_our` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1非官方号2是官方号',
  `is_levfix` smallint(1) NOT NULL DEFAULT 1 COMMENT '1不固定等级2固定等级',
  `hello` smallint(1) NOT NULL DEFAULT 1 COMMENT '不可描述',
  `invite_code` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `card_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `card_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `anme`(`username`) USING BTREE,
  UNIQUE INDEX `selfphone`(`selfphone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 274 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_user
-- ----------------------------
INSERT INTO `qmgx_user` VALUES (4, '人工智能', 1, '18888888888', NULL, 8, '', 88, 1, 0.00, 0, 1, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', '三德子', '18888888888', 'http://img.yizitao.cn/a9304ce1926d8dc1e2cb7a3029d5a493.jpg', '人工智能', '321123456765456456', '18888888888', '三德子', '18888888888', 'http://img.yizitao.cn/70104cf3b7b8b1867c66f753f84c43e4.jpg', 154227.00, 0, 1, NULL, NULL, '', '', 54150.00, 0.00, '2020-08-04 20:26:32', '', 0, 0.00, 5.32, 0, 2, 1, 1, 'Z6DCCPMG', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (5, '益达', 1, '17009125357', 4, 0, ',4,', 1, 2, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '123123', '17009125357', 'http://img.yizitao.cn/de2fe4247496e7586287382dd8a4d8ea.jpg', '赵有益', '323345678876567456', '17009125357', '益达', '17009125357', 'http://img.yizitao.cn/cc1cd4fbfdd27c0b8adec888d377c7a9.jpg', 0.00, 0, 1, NULL, NULL, '', '', 1060.00, 0.00, '2020-08-04 20:38:37', '', 0, 0.00, 8.51, 0, 2, 1, 1, 'FLLUM651', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (6, '花无百日红 006', 1, '17013226770', 5, 1, ',4,,5,', 1, 3, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '花花世界03', '17009125358', 'http://img.yizitao.cn/a17cd25ee63f9836a507dfd494685c1e.jpg', '竹哥', '123456789987654321', '17009125358', '花花世界', '17009125358', 'http://img.yizitao.cn/81086e00ce97690904affbfc08a854b0.jpg', 0.00, 0, 1, NULL, NULL, '', '', 561.00, 0.00, '2020-08-04 20:41:37', '', 0, 0.00, 0.18, 0, 1, 1, 1, 'NCY44R9R', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (7, '花无百日红016', 1, '17009125358', 5, 0, ',4,,5,', 0, 3, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '花花世界', '17009125358', 'http://img.yizitao.cn/d9c6e6ee4a25673b63243d09bf97a1b3.jpg', '竹哥', '112233445566778899', '17009125358', '花花世界', '17009125358', 'http://img.yizitao.cn/3b8ca8083b822cf860de6e3d43836363.jpg', 0.00, 0, 1, NULL, NULL, '', '', 122.00, 0.00, '2020-08-04 20:51:08', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'P06CHHRF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (8, '厚德载物5号', 1, '17015568091', 6, 0, ',4,,5,,6,', 0, 4, 2.20, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '吴建德', '17015568075', 'http://img.yizitao.cn/4e9cbc88411cc0c1f57f917b59460dc6.jpg', '吴建德', '111111111111111221', '17015568075', '吴建德', '17015568075', 'http://img.yizitao.cn/779205acdf6d5eebb5ad06b52d6fb8bd.jpg', 2.00, 0, 1, NULL, NULL, '', '', 328.00, 0.00, '2020-08-04 20:56:15', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'KQN04540', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (9, '厚德载物28号', 1, '17015568075', 5, 0, ',4,,5,', 0, 3, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '吴建德', '17015568075', 'http://img.yizitao.cn/283e4057a156d2d6def11cc4ecbb5a7e.jpg', '吴建德', '123443211234567634', '17015568075', '吴建德', '17015568075', 'http://img.yizitao.cn/a5f9f5073b1ec32fa92745c3fd9b53e0.jpg', 0.00, 0, 1, NULL, NULL, '', '', 58.00, 0.00, '2020-08-04 20:59:06', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'C1QSPQSR', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (10, '益哥', 1, '16676238055', 5, 0, ',4,,5,', 0, 3, 105.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '王益', '16676238055', 'http://img.yizitao.cn/24c1597e9f4b47d564b8755816fdec3c.jpg', '王益', '123123123123123123', '18876238055', '王益', '16676238055', 'http://img.yizitao.cn/ba75a4f92d9248e4eee5a7982808278a.png', 11503.00, 0, 1, NULL, NULL, '', '', 56.00, 0.00, '2020-08-04 21:04:54', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'Z794NQI7', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (11, '益哥05', 1, '16676238051', 5, 0, ',4,,5,', 0, 3, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '王益', '16676238055', 'http://img.yizitao.cn/86ff50401a493c37584a3ac2ce9be03b.jpg', '王益', '123123123123123123', '18076238056', '王益', '16676238055', 'http://img.yizitao.cn/d146229ce6abbd1b1d721a344762fad3.jpg', 0.00, 0, 1, NULL, NULL, '', '', 76.00, 0.00, '2020-08-04 21:07:04', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'EOZ99G98', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (13, '金帆038', 1, '18097883326', 5, 0, ',4,,5,', 0, 3, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '帆帆', '18097883326', 'http://img.yizitao.cn/79d1816e8abfc3b0e0f6f80e0ff7e5bc.jpg', '金帆', '123456789987654321', '18097883326', '18097883326', '帆帆', 'http://img.yizitao.cn/482209095dee919492c02b60e42d8242.png', 0.00, 0, 1, NULL, NULL, '', '', 56.00, 0.00, '2020-08-04 21:13:34', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'N7OTOI1T', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (14, '刘帆', 1, '18922336751', 5, 0, ',4,,5,', 0, 3, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', '18922336751', '18922336751', 'http://img.yizitao.cn/7d32cc24abce261ce720135a6341bc14.jpg', '帆帆', '123123123123123123', '18922336751', '微信受限制，可以支付宝', '18922336751', 'http://img.yizitao.cn/5951b6cf93e34c5b48e47f0fb263da8b.jpg', 0.00, 0, 1, NULL, NULL, '', '', 78.00, 0.00, '2020-08-04 21:15:47', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'L0G6K8TJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (15, '万国', 1, '19088962350', 5, 0, ',4,,5,', 0, 3, 0.00, 0, 1, '73fea72a31e0c5c2c05cbfd49d67ea33', '63ee451939ed580ef3c4b6f0109d1fd0', NULL, '', NULL, '万国', '123123123123123123', NULL, NULL, '', NULL, 2.00, 0, 1, NULL, NULL, '', '', 60.00, 0.00, '2020-08-04 21:18:31', '', 0, 0.00, 0.00, 0, 1, 1, 1, 'J77FAWHA', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (16, '北斗001', 1, '15852536233', 4, 6, ',4,', 6, 2, 0.00, 0, 1, '15884842fd2a78cbd070188c819df48a', '63ee451939ed580ef3c4b6f0109d1fd0', '斌', '15852536233', 'http://img.yizitao.cn/b769834dd9a7c7fbb3c31e362bacb9b6.jpg', '吴斌', '322147258369789', '15852536233', '斌', '15852536233', 'http://img.yizitao.cn/19228a6ebc37dc92835e8875ebdadf3a.jpg', 0.00, 0, 1, NULL, NULL, '', '', 250.00, 0.00, '2020-08-05 10:52:58', '47.99.20.133', 0, 0.00, 0.00, 0, 1, 1, 1, 'CBO5XORC', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (17, '第一', 1, '18146613930', 4, 0, ',4,', 56, 2, 0.00, 0, 0, '7718f893a7f97cb9af52c25302b9be39', '14e1b600b1fd579f47433b88e8d85291', '1', '1', 'http://img.yizitao.cn/7d13bb0d502db25086be921e7c7875df.jpg', '刘东亭', '360502199408207439', '18146613930', '1', '1', 'http://img.yizitao.cn/e35622f68e3583df13a6c6cbbb603271.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 11:06:29', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'TFFGVCBZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (18, '二点', 1, '18857970824', 17, 1, ',4,,17,', 56, 3, 0.00, 0, 0, '7718f893a7f97cb9af52c25302b9be39', '14e1b600b1fd579f47433b88e8d85291', '2', '2', 'http://img.yizitao.cn/752311083e494a39e4a7a9098d6d94ff.jpg', '君尚', '360502199408207456', '18857970824', '2', '2', 'http://img.yizitao.cn/5f6cbc123b5badf5fa2b51e90c211d7c.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 11:11:38', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'RGG0GI4E', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (19, '风生水起', 1, '18909943315', 4, 2, ',4,', 14, 2, 0.00, 0, 1, 'e00483741ecc418d8fa130b3396f4159', 'eed885fec0e36ac0a472f7c7f8dc07ab', '霸道的幸福', '18196160520', 'http://img.yizitao.cn/19f5d7a87b5867f2081de26c92bd23a2.jpg', '张建新', '652324197411042216', '13565360343', '霸道的幸福', '18196160520', 'http://img.yizitao.cn/b48438502e1cf1806d6cee68ef24f0dc.png', 0.00, 0, 1, NULL, NULL, '', '', 4690.00, 0.00, '2020-08-05 11:12:32', '47.99.20.136', 0, 0.00, 0.00, 0, 1, 1, 1, 'RCCXK054', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (20, '不二家', 1, '15523518952', 18, 35, ',4,,17,,18,', 55, 4, 0.00, 0, 1, '7718f893a7f97cb9af52c25302b9be39', '14e1b600b1fd579f47433b88e8d85291', '1111', '1111', 'http://img.yizitao.cn/052e4f1c2a287b5c3df44925c10bf081.jpg', '君临', '360502199308237452', '15523518952', '111', '111', 'http://img.yizitao.cn/573f8d00d813b443a0caff72ace2ac65.jpg', 0.00, 0, 1, NULL, NULL, '', '', 2530.00, 0.00, '2020-08-05 11:15:28', '47.111.193.62', 0, 0.00, 0.00, 0, 1, 1, 1, 'QAFJEERS', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (21, 'A12345', 1, '17054497911', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 11:21:39', '47.111.193.77', 0, 0.00, 0.00, 0, 1, 1, 1, 'X81K4YGV', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (22, '咋开心呢', 1, '18094444666', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'd477887b0636e5d87f79cc25c99d7dc9', '14e1b600b1fd579f47433b88e8d85291', '胡翠翠', '15295012122', 'http://img.yizitao.cn/0dc4682cf6704105451f7440daf333fe.jpeg', '徐满意', '320923199210126938', '18094444666', '胡翠翠', '15295012122', 'http://img.yizitao.cn/989a510712ed265f4259f29fbac3f55a.jpeg', 0.00, 0, 1, 20, NULL, NULL, NULL, 1560.00, 0.00, '2020-08-05 11:25:22', '47.111.193.78', 0, 0.00, 0.00, 0, 1, 1, 1, 'XPWMNEZM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (23, '麦苗', 1, '17321049862', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, 'a89d22576d8973005908d2612d8047cd', 'e3bf964f91761f2fbdda4619b5281c3c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 11:25:23', '47.111.193.56', 0, 0.00, 0.00, 0, 1, 1, 1, 'DVS6MVHH', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (24, '小甜心', 1, '18779238342', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '08a35d893c7a8d3b353f72d70cf7fb02', '4c5bfd08d8b4465ce332545fd6fca30a', '朱佩文', '18779238342', 'http://img.yizitao.cn/c419e3ac329130a951c6bc9b9ef7219b.jpeg', '朱佩文', '360421199211042021', '13479260909', NULL, NULL, NULL, 0.00, 0, 1, 20, '中国工商银行', '九江市九江支行营业室', '6212261507001373003', 60.00, 0.00, '2020-08-05 11:27:49', '47.99.20.130', 0, 0.00, 0.00, 0, 1, 1, 1, 'E9EP9A6R', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (25, '吉祥', 1, '15739624884', 19, 12, ',4,,19,', 12, 3, 0.00, 0, 1, 'e00483741ecc418d8fa130b3396f4159', 'eed885fec0e36ac0a472f7c7f8dc07ab', '吉祥', '15739624884', 'http://img.yizitao.cn/7c6eee464a7af366d8086009c74a5168.jpg', '张建新', '652324197411042216', '18196160520', '吉祥', '15739624884', 'http://img.yizitao.cn/54e64226b0c006bbeec51687a80b80f8.png', 0.00, 0, 1, 19, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 11:30:47', '112.124.159.139', 0, 0.00, 0.00, 0, 1, 1, 1, 'BXCW70HU', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (26, '逆风123', 1, '18060172936', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '59be99b3eaba1e7a5372287602a19fc1', '1eb39edd1c9349907e6b72ca504106e4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 560.00, 0.00, '2020-08-05 11:33:33', '47.99.20.137', 0, 0.00, 0.00, 0, 1, 1, 1, 'U85YBFDZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (27, '王哈哈', 1, '15157863554', 20, 2, ',4,,17,,18,,20,', 2, 5, 0.00, 0, 1, '738f3d9a772defe8024530bb9e18e42f', 'a79ef38d38169c629c6267ab8c8370a7', '王哈哈', '15168016190', 'http://img.yizitao.cn/46e8c291ba69149516a704f4094b8b69.png', '王敏', '332523199110110050', '15168016190', '王哈哈', 'w453302280', 'http://img.yizitao.cn/1c530a845a574bcc82c7a5da435b12d6.png', 0.00, 0, 1, 20, NULL, NULL, NULL, 295.00, 0.00, '2020-08-05 11:33:38', '47.111.193.72', 0, 0.00, 0.00, 0, 1, 1, 1, 'MHB1BNHY', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (28, '财宝宝', 1, '18196160520', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, 'e00483741ecc418d8fa130b3396f4159', 'eed885fec0e36ac0a472f7c7f8dc07ab', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 19, NULL, NULL, NULL, 150.00, 0.00, '2020-08-05 11:35:57', '112.124.159.139', 0, 0.00, 0.00, 0, 1, 1, 1, 'KRKOEG6Y', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (29, '绣江南', 1, '17326099563', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '7222d069af34579e959295d15aba373b', '7222d069af34579e959295d15aba373b', '么么哒', '15395829312', 'http://img.yizitao.cn/98b0a2992253ce39858ab958eea9aff4.jpg', '王者', '340321199106118937', '17326099563', '嫩模', '123', 'http://img.yizitao.cn/772929e67cf564bcfb215762a75068cd.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 11:36:53', '47.99.20.130', 0, 0.00, 0.00, 0, 1, 1, 1, 'HXQO6PX8', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (30, '看谁跑得快', 1, '15888738658', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '48a1a86d9b9e48d796acad9f313d6618', 'f76d55ba2b0ad956b38691a0400e90fb', '王', '15888738658', 'http://img.yizitao.cn/749db42e5d334ecdff1a88fc4dc55461.jpeg', '王丰', '330304199512022115', '15888738658', '王', '15888738658', 'http://img.yizitao.cn/95c36813e2efa8d8c292ed73537470a1.jpeg', 0.00, 0, 1, 20, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 11:41:32', '47.99.20.137', 0, 0.00, 0.00, 0, 1, 1, 1, 'NE8YOAWE', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (31, '辛巴', 1, '15393265376', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '00cb553cf5815b141fc87cc49133d001', '0e80073c7c72c7b9541eebf425438388', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 11:43:35', '47.99.20.134', 0, 0.00, 0.00, 0, 1, 1, 1, 'IW9S5OYP', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (32, '定海', 1, '13059385678', 20, 1, ',4,,17,,18,,20,', 2, 5, 0.00, 0, 1, 'd161176d682294fb0c79568ec00ce556', 'a75eabb453ddd6b58205633a9d0994fa', '施进兴', '881261qq.com', 'http://img.yizitao.cn/b3f6cd38ef212e866e0a659ef75c6984.jpg', '施阿兴', '350582196312206013', '13059385678', '施进兴', 'a881281', 'http://img.yizitao.cn/cd26ae87dd57ac35a7080408dd29d1f0.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 11:43:41', '47.111.193.97', 0, 0.00, 0.00, 0, 1, 1, 1, 'T91XEM3T', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (33, 'a123456', 1, '19959850596', 4, 0, ',4,', 0, 2, 0.00, 0, 1, 'de254ea1e82cb7c87410681acd0dba4e', 'f2b37ab59ba07fc02b4ece15ac442e07', '潘沿围', '19959850596', 'http://img.yizitao.cn/0fd6605f41300a6ee59fde06f8b452df.jpg', '潘沿围', '350583199111218336', '19959850596', '潘沿围', '19959850596', 'http://img.yizitao.cn/784440a7cc99541273770a4af5b4e369.png', 0.00, 0, 1, 4, NULL, NULL, NULL, 5050.00, 0.00, '2020-08-05 11:47:32', '47.111.193.64', 0, 0.00, 0.00, 0, 1, 1, 1, 'YG29TCSK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (34, 'xing', 1, '18027195555', 32, 1, ',4,,17,,18,,20,,32,', 1, 6, 0.00, 0, 1, 'd161176d682294fb0c79568ec00ce556', 'a75eabb453ddd6b58205633a9d0994fa', '施俊兴', '18927195555', 'http://img.yizitao.cn/d612e56ff3ad164903bc1d9ee2252e78.jpg', '施俊兴', '350582196312206013', '18027195555', '施俊兴', '18027195555', 'http://img.yizitao.cn/33a7d916c43ca0b6f86d77f10edfc9ee.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 11:53:51', '47.111.193.73', 0, 0.00, 0.00, 0, 1, 1, 1, 'JYZNZNPP', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (35, '好想好想', 1, '15881879193', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 0, '87785c144269557ef6e01837da9d20df', '14e1b600b1fd579f47433b88e8d85291', '刘照文', '15881879193', 'http://img.yizitao.cn/bf7883377bf66cb4f2e6c56568283a58.jpg', '刘照文', '522121197508024410', '15881879193', '刘照文', '15881879193', 'http://img.yizitao.cn/f30d449b6a74a4dcbe8af99decb02bdf.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 11:57:17', '47.99.20.130', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZQ3EWQEF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (36, 'xing2', 1, '13533576777', 34, 0, ',4,,17,,18,,20,,32,,34,', 0, 7, 0.00, 0, 1, 'd161176d682294fb0c79568ec00ce556', 'a75eabb453ddd6b58205633a9d0994fa', '施建兴', '18027195555', 'http://img.yizitao.cn/0ffb4db5f880c40e211f74e60f899177.jpg', '施建兴', '350582196412206013', '13533576777', '施建兴', '881261', 'http://img.yizitao.cn/dc8c9f5dfeec7f2930cf680c4c038d92.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 97.00, 0.00, '2020-08-05 12:02:42', '47.111.193.71', 0, 0.00, 0.00, 0, 1, 1, 1, 'C6XXHPCP', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (37, '岁月静好1', 1, '13770135227', 4, 3, ',4,', 3, 2, 0.00, 0, 1, '0efe6cda791ca3f2d328157d6e6508b2', 'c735d911ded16fa2e61cbf8d14f20019', '彭海波', '13914644405', 'http://img.yizitao.cn/96907987d3c1e349498554b2108f80cc.jpg', '彭海波', '320922198308224413', '13770135227', NULL, NULL, NULL, 0.00, 0, 1, 4, '中国农业银行', '江苏省滨海县支行', '6228481982877847911', 5030.00, 0.00, '2020-08-05 12:14:05', '47.99.20.118', 0, 0.00, 0.00, 0, 1, 1, 1, 'R99FX1UK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (38, '倪大叶', 1, '18073923127', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '9db06bcff9248837f86d1a6bcf41c9e7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 12:25:59', '47.111.193.86', 0, 0.00, 0.00, 0, 1, 1, 1, 'MLUGULTY', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (39, 'ss001', 1, '13636966250', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/48c2b1d46d854329f85b6c06eb503da5.jpg', '张乃华', '350825198207221116', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '国家开发银行', '泉州支行', '6214835951460292', 50.00, 0.00, '2020-08-05 12:27:06', '47.99.20.122', 0, 0.00, 0.00, 0, 1, 1, 1, 'DL3YRNRT', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (40, '帅气逗比', 1, '18333779381', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'ec82dac7f602d90d00e01e7725770d1d', 'bbd743d7110ac581e829366fc760e81c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 12:30:05', '112.124.159.144', 0, 0.00, 0.00, 0, 1, 1, 1, 'YCU2LIX9', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (41, 's141', 1, '13772145686', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 1, '3b19bc2cda28521ac405358740fd74b1', 'b74e42772d0a85647a937458a45d5edc', '小孙', '111', 'http://img.yizitao.cn/d97017feb392854cb973040bcdcf7d50.jpg', '小孙', '620302199212210620', '13772145686', '小孙', '111', 'http://img.yizitao.cn/eeafebc0e86867c713732deb1bf7719c.png', 0.00, 0, 1, 4, NULL, NULL, NULL, 150.00, 0.00, '2020-08-05 12:33:17', '47.111.193.99', 0, 0.00, 0.00, 0, 1, 1, 1, 'Y52NLY72', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (42, '日出东方', 1, '15852536236', 16, 0, ',4,,16,', 0, 3, 0.00, 0, 1, '15884842fd2a78cbd070188c819df48a', '63ee451939ed580ef3c4b6f0109d1fd0', '日出东方', '15852536236', 'http://img.yizitao.cn/dd1b5416831730a179dd7bccb5bd377c.jpg', '金健康', '258258258258258258', '15852536236', '日出东方', '15852536233', 'http://img.yizitao.cn/ed11e5619a45586584cdf2416d4494b8.jpg', 0.00, 0, 1, 16, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 12:40:44', '47.99.20.141', 0, 0.00, 0.00, 0, 1, 1, 1, 'TQZDMF0B', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (43, '易逝', 1, '13193741098', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 1, '496866b4ed8acdeec24a46e8f56f90a9', '646733b638854e2947050e57fa2ee189', '17739645099', '王可', 'http://img.yizitao.cn/17bf6ff25217581e9ebb338190814445.jpg', '王可', '412826199512026615', '17739645099', NULL, NULL, NULL, 0.00, 0, 1, 37, '中国银行', '增城支行', '6217857000014170557', 60.00, 0.00, '2020-08-05 12:41:15', '47.99.20.140', 0, 0.00, 0.00, 0, 1, 1, 1, 'Q9SM0W93', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (44, '嫦娥11', 1, '15952536225', 16, 0, ',4,,16,', 0, 3, 0.00, 0, 1, '15884842fd2a78cbd070188c819df48a', '63ee451939ed580ef3c4b6f0109d1fd0', '天空', '15952536225', 'http://img.yizitao.cn/0f5706f2467a3ccfab240bbca5ff638b.jpg', '天龙', '159525362252583691', '15952536225', '天空', '15952536225', 'http://img.yizitao.cn/6b6d340bd7c1d506ac183fbada5c6997.png', 0.00, 0, 1, 16, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 12:46:33', '47.99.20.141', 0, 0.00, 0.00, 0, 1, 1, 1, 'VF9FUPX5', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (45, '嫦娥2', 1, '15951536223', 16, 0, ',4,,16,', 0, 3, 0.00, 0, 1, '15884842fd2a78cbd070188c819df48a', '63ee451939ed580ef3c4b6f0109d1fd0', '来来回回', '15951536223', 'http://img.yizitao.cn/14c81fda95ea7fc9d6efb0f84053a07f.jpg', '陈来', '320382199012281218', '15951536223', '时光记忆', '15951536223', 'http://img.yizitao.cn/58b479f6cbd9d9d64e6eef61ee9d3645.jpg', 0.00, 0, 1, 16, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 12:52:56', '47.99.20.141', 0, 0.00, 0.00, 0, 1, 1, 1, 'MES4TWWF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (46, '哦豁', 1, '18128134200', 4, 0, ',4,', 0, 2, 0.00, 0, 1, 'd477887b0636e5d87f79cc25c99d7dc9', 'cc2336d68afd752527efdaa517236a1e', '11', '18128134200', 'http://img.yizitao.cn/79e1ae1ecff392f783d5544ee92eaa49.jpg', '叶秋', '230321198604225516', '18128134200', '11', '1812813420 ', 'http://img.yizitao.cn/aeb5a2395419b57e3ba4a36dcf929f20.jpg', 0.00, 0, 1, 4, NULL, NULL, NULL, 97.00, 0.00, '2020-08-05 12:53:47', '47.111.193.88', 0, 0.00, 0.00, 0, 1, 1, 1, 'MM0VPXPN', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (47, '木木木', 1, '18627259520', 20, 10, ',4,,17,,18,,20,', 10, 5, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕', 'CGourxxf.com', 'http://img.yizitao.cn/6295391a38b6b19593979a8ad148f375.jpg', '常广', '421083199212012515', '17786339521', '慕', 'weixin_mrh', 'http://img.yizitao.cn/da2dd29f163f57d56606aed3228f74fb.png', 0.00, 0, 1, 20, NULL, NULL, NULL, 160.00, 0.00, '2020-08-05 12:57:57', '47.111.193.87', 0, 0.00, 0.00, 0, 1, 1, 1, 'HQOHLYSJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (48, '乘风破浪', 1, '15649220223', 16, 0, ',4,,16,', 0, 3, 0.00, 0, 1, '15884842fd2a78cbd070188c819df48a', '63ee451939ed580ef3c4b6f0109d1fd0', '乘风破浪', '15649220223', 'http://img.yizitao.cn/3e0d6266418ec3b861b228e0bee0098d.jpg', '赵老师', '322382258369789256', '15649220223', '牛气冲天', '15649220223', 'http://img.yizitao.cn/422f7453d0a27e21511af4f0a8019217.png', 0.00, 0, 1, 16, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 13:00:25', '47.99.20.141', 0, 0.00, 0.00, 0, 1, 1, 1, 'AK8375I1', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (49, '乘风破浪666', 1, '15649220205', 16, 0, ',4,,16,', 0, 3, 0.00, 0, 1, '15884842fd2a78cbd070188c819df48a', '63ee451939ed580ef3c4b6f0109d1fd0', '淳爷', '15649220205', 'http://img.yizitao.cn/ac86ab413760fdbe92421076e93e8081.jpg', '陈龙', '321328199602034528', '15649220205', '淳爷', '15649220205', 'http://img.yizitao.cn/d3d97fdade32d860fb2f0befa926d719.jpg', 0.00, 0, 1, 16, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 13:06:09', '47.99.20.141', 0, 0.00, 0.00, 0, 1, 1, 1, 'L4Y2DI92', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (50, 'jack', 1, '15077060025', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '76c72f67a586dac43d7bf6a2fc8d1ae9', 'dfb04419592b654397911196a792bfb9', '甘焕杰', '15077060025', 'http://img.yizitao.cn/3a9e429e5c146a137dfd63ac89bb50c9.jpg', '甘焕杰', '452123199711096114', '18676982040', '甘焕杰', '15077060025', 'http://img.yizitao.cn/040d4fdabbc33de54eecb9ffc6f1dd57.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 13:17:20', '112.124.159.143', 0, 0.00, 0.00, 0, 1, 1, 1, 'QO1R2OMX', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (51, '分散', 1, '16569715475', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕', 'CGourxxf.com', 'http://img.yizitao.cn/56e7a2406e0db5b8f7ba04df2e8f8889.jpg', '郑一峰', '332522198705040011', '17786339521', '慕', 'weixin_mrh', 'http://img.yizitao.cn/3af13c9edb472fab7e3df7f271e2e41c.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 13:20:52', '47.111.193.79', 0, 0.00, 0.00, 0, 1, 1, 1, 'CBK3BLCM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (52, 's6848', 1, '18092011959', 41, 0, ',4,,37,,41,', 0, 4, 0.00, 0, 0, '3b19bc2cda28521ac405358740fd74b1', '6c7fa7575543450e08f943c7cc7c44d5', '鲲鹏', '2222', 'http://img.yizitao.cn/bb3585385f8a5b2059daec4a7995a837.jpg', '鲲鹏', '120113198809303219', '18092011959', '鲲鹏', '2222', 'http://img.yizitao.cn/2a7263f68f92555ba38e5e6b13fba7dc.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 13:23:43', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'G0JPZQ0M', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (53, 'gao5208', 1, '19805055208', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '9128741354fb57cef11e1de5cc5baf87', '33d61498a9809abaa943dac701843995', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 13:24:24', '47.111.193.82', 0, 0.00, 0.00, 0, 1, 1, 1, 'E650ZOJL', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (54, '打个电话', 1, '16533283457', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕', 'CGourxxf.com', 'http://img.yizitao.cn/3c9351dac9f2dcc56e531ab00e58ede6.jpg', '池善卿', '35042619790906301X', '17786339521', '慕慕', 'weixin_mrh', 'http://img.yizitao.cn/ca1929da162bd37773e9d3d69b6f1a91.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 13:29:20', '47.111.193.79', 0, 0.00, 0.00, 0, 1, 1, 1, 'T49PP2N3', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (55, '方法刚回家', 1, '16533420220', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕', 'CGourxxf.com', 'http://img.yizitao.cn/9a3832504afb93e17d92c4e4f4a3ec28.jpg', '卫忠杰', '210602198711260513', '17786339521', '慕慕', 'weixin_mrh', 'http://img.yizitao.cn/1b0478022a8edcd4db34a5fba147412e.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 13:38:10', '47.99.20.121', 0, 0.00, 0.00, 0, 1, 1, 1, 'NK2FK7LE', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (56, 'yang123', 1, '18024168164', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'f03cd50e548ddedbf7a439713cefd959', 'f1326584d76db0bded43b4cc4b348a03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 13:45:21', '47.99.20.127', 0, 0.00, 0.00, 0, 1, 1, 1, 'XGG533M6', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (57, '方法很', 1, '16569711581', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕', 'CGourxxf.com', 'http://img.yizitao.cn/c922f14bfefdd7a1cd8f6410ba4b6e2a.jpg', '许家圣', '340103198303072554', '17786339521', '没余亩', 'weixin_mrh', 'http://img.yizitao.cn/1928765bdbb7af64cc4a6bc78663c022.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 13:45:34', '47.99.20.119', 0, 0.00, 0.00, 0, 1, 1, 1, 'IOX3S2Y2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (58, 'add123', 1, '18655756618', 41, 0, ',4,,37,,41,', 0, 4, 0.00, 0, 0, 'ce29cd4fb803d9e4907338f28330ff23', '14e1b600b1fd579f47433b88e8d85291', '张莹', '18655756618', 'http://img.yizitao.cn/0af3750661cb9f712b6d228aae5dd7a5.jpg', '袁国旗', '342501198111101331', '18655555555', '张莹', 'huzhu999', 'http://img.yizitao.cn/e1175849b1c5f0e440672e0958043325.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 13:46:03', '47.99.20.133', 0, 0.00, 0.00, 0, 1, 1, 1, 'BKET3LUU', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (59, '大宝贝等你', 1, '17053660796', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕', 'CGourxxf.com', 'http://img.yizitao.cn/3d266b47bcb0d6f0d74baf5cbe6accb3.jpg', '李靖男', '410183199307210015', '17786339521', '发送给', 'weixin_mrh', 'http://img.yizitao.cn/489dd3242ef0a289db5acd59356c6a74.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 13:47:04', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'U2O2N4PY', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (60, '闪烁发顺', 1, '16725453273', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕慕', 'CGourxxf.com', 'http://img.yizitao.cn/692c353d38f58dbacd93bbb7c503138b.jpg', '陈侃', '362326198306270039', '17786339521', '慕', 'weixin_mrh', 'http://img.yizitao.cn/1a4b88e64b761f6cf581d7ae204d27be.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 13:55:50', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'RM88348D', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (61, 'a1234567890', 1, '13428874792', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, '幸福人', '18145698711', 'http://img.yizitao.cn/9d2857eb38ce608b75dd8e25052c9f6e.jpeg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 14:02:18', '47.111.193.81', 0, 0.00, 0.00, 0, 1, 1, 1, 'F1GSSWG6', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (62, 'abc123', 1, '15967293599', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '26c79c8e0140aabf3608f20e0bd02c18', '3d4022720982b0f21b7ac1fccb720cc6', '林斌', '15967293599', 'http://img.yizitao.cn/d844a366fa1b35385515775087fd36f2.jpeg', '林斌', '332523199312082414', '15967293599', '林斌', 'zjlslb8888', 'http://img.yizitao.cn/4cead395420df3d10cfb6bbf2b22dca9.jpeg', 0.00, 0, 1, 20, NULL, NULL, NULL, 252.00, 0.00, '2020-08-05 14:07:41', '47.111.193.94', 0, 0.00, 0.00, 0, 1, 1, 1, 'IMRFZ7TF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (63, '是单方事故', 1, '16565707485', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '木木木', 'CGourxxf.com', 'http://img.yizitao.cn/d599b1a98aa4ecccc3d97f549609d424.jpg', '康焕卉', '430503198706130038', '17786339521', '慕', 'weixin_mrh', 'http://img.yizitao.cn/43314597127ea4281811fa5bdb181c1a.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 14:09:32', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'BJJH661D', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (64, 'p456789123', 1, '17885933724', 4, 0, ',4,', 0, 2, 0.00, 0, 1, 'e6e14bed3e705a568f1c029f5d07704b', '7833c79773776845a2adde435a55e1e8', '朱晓星', 'Zhx-19870326', 'http://img.yizitao.cn/23d3a5664a864274dc47e489d2af4580.png', '朱晓星', '610602198812100310', '17885933724', '朱晓星', 'Zhx-19870326', 'http://img.yizitao.cn/5c61f7c88e9592f71a0836a0fa177cf0.png', 0.00, 0, 1, 4, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 14:11:38', '47.111.193.76', 0, 0.00, 0.00, 0, 1, 1, 1, 'UJETYKJQ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (65, '玖月', 1, '18563272732', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '631ee12daa931261d199fc2c9bf57c48', '46cc468df60c961d8da2326337c7aa58', '朱贺', '3133792825qq.com', 'http://img.yizitao.cn/41618d087052694a36781455812d2074.jpg', '朱贺', '370404199206064053', '18563272732', '朱贺', 'gg2013__', 'http://img.yizitao.cn/b6350627f2017aadb6bb83b0752e5ff4.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 14:17:19', '47.111.193.94', 0, 0.00, 0.00, 0, 1, 1, 1, 'M0M39NG9', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (66, '小脾气', 1, '19151002353', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '95772ceb8f2ad43430ee17a6840de6df', '95772ceb8f2ad43430ee17a6840de6df', NULL, NULL, NULL, '叶秋平', '422201198810083324', '19151002353', '叶秋平', '19151002353', 'http://img.yizitao.cn/07f0bfa20ad0e9007b872bcf9eb1448b.png', 0.00, 0, 1, 20, '中国邮政银行', '湖北省孝感市三汊支行', '6221505200027650656', 60.00, 0.00, '2020-08-05 14:19:49', '47.99.20.122', 0, 0.00, 0.00, 0, 1, 1, 1, 'AKCCRL8Y', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (67, '发的内容', 1, '16573055396', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕慕', 'CGourxxf.com', 'http://img.yizitao.cn/cefa29afa3f8ebe4e7cc76f48c3764a2.jpg', '池鹏', '331081198601210014', '17786339521', '慕', 'weixin_mrh', 'http://img.yizitao.cn/768870135f5f4973cb257e31b709e0d9.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 14:20:44', '47.99.20.118', 0, 0.00, 0.00, 0, 1, 1, 1, 'BZM301RC', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (68, '店内想你', 1, '17075011689', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', '慕', 'CGourxxf.com', 'http://img.yizitao.cn/ce31580f2d009ff3672b851c64482366.jpg', '刘大奇', '230103198509121352', '17786339521', '慕', 'weixin_mrh', 'http://img.yizitao.cn/56e981381b1b259e7a5079e6a610d803.png', 0.00, 0, 1, 47, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 14:21:36', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'ISZXVW8W', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (69, 'J', 1, '13867704109', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '106fd8595fd7b20d2df63ff77940fb89', '30832be69a6ab3d5d271e1dcbea3a18b', '高屹君', '13867704109', 'http://img.yizitao.cn/c3bc283a3e100c695f7be551493191df.jpeg', '高屹君', '332529199104207014', '15988013388', '高屹君', 'gao2882048', 'http://img.yizitao.cn/e6e60697f4e47a5ebd03481a72ff3c11.jpeg', 0.00, 0, 1, 20, NULL, NULL, NULL, 242.00, 0.00, '2020-08-05 14:28:13', '47.111.193.93', 0, 0.00, 0.00, 0, 1, 1, 1, 'AE6T6P4N', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (70, '智能王者', 1, '13169119767', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '9f310fe927a33080908657ca5c909e44', '44db151a0adf4917d3a74e1500cc3559', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 14:30:11', '47.99.20.120', 0, 0.00, 0.00, 0, 1, 1, 1, 'WZIPWL90', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (71, 'weiyi', 1, '13733434061', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 0, '319ea2afc561fc4ba2a0d7ab1c1aa27c', '22b912d764879651d653857794453d3f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 14:33:48', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'OG19Y6OD', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (72, '三水与共', 1, '13600774732', 20, 1, ',4,,17,,18,,20,', 1, 5, 0.00, 0, 1, 'f4d465fe5fae475a69ba4d81c6487773', 'c78d7d5c29551138143e053a4e322f6d', '无业游民', '13600774732', 'http://img.yizitao.cn/3817960c274c464fe2d9ffc103939dba.png', '洪志波', '350521198307183039', '13600774732', '洪志波', 'Hzb13600774732', 'http://img.yizitao.cn/d079a32ff742942fe2d5181114912fa2.png', 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 14:39:22', '47.111.193.99', 0, 0.00, 0.00, 0, 1, 1, 1, 'MOOZUIYU', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (73, '小米', 1, '18675765704', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '7533df87b5c1f8e0ab3e740db7aad686', '700c2351da71788780a3db43be63b374', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 14:47:33', '47.111.193.75', 0, 0.00, 0.00, 0, 1, 1, 1, 'JWUELL24', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (74, '招财猫1', 1, '15507474770', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '46cc468df60c961d8da2326337c7aa58', '46cc468df60c961d8da2326337c7aa58', '毛庆中', '2577900076qq.com', 'http://img.yizitao.cn/00de62950033064c88af4fe9374f0e2b.jpg', '毛庆中', '452531199602232514', '15507474770', '毛庆中', '2577900076', 'http://img.yizitao.cn/80092e71e3bbdf31a47cc7c36bdc6b1a.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 14:56:09', '47.99.20.125', 0, 0.00, 0.00, 0, 1, 1, 1, 'EQQOMXOK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (75, '望穿秋水', 1, '15519841075', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '0ee6a509b160f97e3382c75e5257bdd4', '73812f2b9a460ff9b3873fbcf717b5f7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 14:57:25', '47.111.193.97', 0, 0.00, 0.00, 0, 1, 1, 1, 'E8RQMYM0', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (76, '李大仁', 1, '13725608186', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '9112e46e1f92bc732cd7815701b225f5', '1e7c7d6b22dde98c5bce050441af08fd', '13432521467', '13432521467', 'http://img.yizitao.cn/08c6fcdca6edb1b32b81ae3aa1944d29.png', '罗世华', '210105198611014923', '13725608186', NULL, NULL, NULL, 0.00, 0, 1, 20, '中国农业银行', '农业银行', '6228481169202191476', 60.00, 0.00, '2020-08-05 15:03:29', '47.111.193.94', 0, 0.00, 0.00, 0, 1, 1, 1, 'DP9UP5MH', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (77, '嗨喽嗨喽', 1, '15261185382', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'fef9fc3d5bbae363832853d46bc52e7b', 'dff3d7abaa714cca6cb3aa51d24db060', '湘香', '15261185382', 'http://img.yizitao.cn/29d26eaa6f3b44844dbdf881dc416f50.jpg', '陈国香', '510922199009154662', '15261185382', '湘香', '15261185382', 'http://img.yizitao.cn/165f759c23d4efaaf439d38fa8ee1904.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 15:04:22', '47.111.193.96', 0, 0.00, 0.00, 0, 1, 1, 1, 'RXRL8EYG', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (78, '柒柒', 1, '13424519886', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '9deeb2736da27cec479c573fc8f8a474', '2e0c92f23b34266d2a7b7848168be66d', '娇', '18576003321', 'http://img.yizitao.cn/556c2909cf42f6093fb5326754f3a1ed.jpeg', '黄娇', '441723198611132021', '13424519886', '屿暖', 'mm-1333', 'http://img.yizitao.cn/b2f976ef179992db2a69dfd227e1c628.jpeg', 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 15:07:22', '47.111.193.56', 0, 0.00, 0.00, 0, 1, 1, 1, 'PTXJLYSY', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (79, '蕾蕾', 1, '13977488752', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, '386720df49ddbd1a3ec80226f88d81bd', '7f3baea6a52d86b19c9b10133d8d9e7f', '蕾蕾', '13977488752', 'http://img.yizitao.cn/e5281e7b4d29499077350b5697a7db6f.jpg', '阮凤珍', '450421197610052123', '13977488752', NULL, NULL, NULL, 0.00, 0, 1, 19, '中国建设银行', '梧州支行', '6236683400000371294', 60.00, 0.00, '2020-08-05 15:08:03', '47.111.193.75', 0, 0.00, 0.00, 0, 1, 1, 1, 'FRS675L5', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (80, '好人儿', 1, '13392298144', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '20aec20471e314e9155e22f85f276b78', '9d9868430f386a2f51b1b4f64c2305ae', '程伟雄', '13392298144', 'http://img.yizitao.cn/c92d4a97b237c945f0e95e25c21a5820.jpg', '程伟雄', '440683198705013031', '13392298144', '开心小雄仔', 'cwx13392298144', 'http://img.yizitao.cn/29f8928fdfd3d8675062eea595775971.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 15:09:45', '47.99.20.140', 0, 0.00, 0.00, 0, 1, 1, 1, 'EXSY0605', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (81, '束先生', 1, '13818287089', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '2c88a0bb784eeb2cb99d709579ad1a07', 'ac07e3832d1a5f0e849c0b8d4981b60d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 15:19:02', '47.111.193.71', 0, 0.00, 0.00, 0, 1, 1, 1, 'UUXXYQIT', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (82, '大鲨鱼', 1, '13591806765', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '15aa86a80e7ce6ea573404f9ee8276be', '076cdf418305034879f7434bce91af76', '赵鑫伟', '13591806765', 'http://img.yizitao.cn/b827ac30918032384eabc9a85a89ab87.jpg', '赵鑫伟', '211322198412272774', '13591806765', '赵鑫伟', '13591806765', 'http://img.yizitao.cn/b1f15065723e4069a7dd8e24904d43fc.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 350.00, 0.00, '2020-08-05 15:20:38', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'S0080864', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (83, '发送给', 1, '16533280514', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 15:33:09', '47.99.20.116', 0, 0.00, 0.00, 0, 1, 1, 1, 'O4FGZDIK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (84, '水电工十大', 1, '17045675477', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 15:40:52', '47.99.20.130', 0, 0.00, 0.00, 0, 1, 1, 1, 'UZY4C444', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (85, '寻', 1, '13860151579', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'cb47548ddbec3c11c5052c3e62cdf6fa', 'aac6e682d27c7dc118416055444de8f4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 200.00, 0.00, '2020-08-05 16:03:14', '47.99.20.119', 0, 0.00, 0.00, 0, 1, 1, 1, 'LWT44B7H', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (86, 'tgl', 1, '13380256200', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'ecafad62fe67546ef8d2ee7a424148c9', 'dc351dfc55c161f1bbefbb9a92c9394f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 16:14:06', '47.111.193.88', 0, 0.00, 0.00, 0, 1, 1, 1, 'JRHFCZ5H', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (87, '开心快乐', 1, '18942461602', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, '9eaed069664b41818def57f527546c57', '1b89af1a77d947ac406e216fada0f97f', '阿婵', '18942461602', 'http://img.yizitao.cn/d8da6e56e916f141a1d404a26fab1cc4.JPEG', '陈芬宜', '440681198705104248', '18942461602', '阿婵', '18942461602', 'http://img.yizitao.cn/9e3607a9b1c54e8aa03ef90e4b13228c.JPEG', 0.00, 0, 1, 19, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 16:14:06', '47.99.20.116', 0, 0.00, 0.00, 0, 1, 1, 1, 'YOE8CYCS', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (88, '是打过帅哥', 1, '16569714094', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 16:26:08', '47.99.20.129', 0, 0.00, 0.00, 0, 1, 1, 1, 'OYD52VIE', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (89, 'wb12345678', 1, '13259725221', 4, 0, ',4,', 0, 2, 0.00, 0, 0, '83c1752d7408ab2ec1061ae9810fef14', '3e0dd67e572319235e851e2a9e0b6be4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 16:56:21', '47.99.20.119', 0, 0.00, 0.00, 0, 1, 1, 1, 'MZIPJVMM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (90, '主一', 1, '13378410370', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'b9e79361b4040a3f3a71668163d2f058', '46cc468df60c961d8da2326337c7aa58', '邱一', '13713530860', 'http://img.yizitao.cn/fe97c9a8ae3a5ced68aa9cf6e90a7e2e.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 350.00, 0.00, '2020-08-05 16:59:03', '47.111.193.97', 0, 0.00, 0.00, 0, 1, 1, 1, 'MGNTMO72', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (91, 'zhl001', 1, '18998986916', 4, 0, ',4,', 0, 2, 0.00, 0, 0, '538530c2b9787b9400aeb44baad9eb67', 'ddce28fca343642b950d613878e72b1f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 17:39:27', '47.99.20.137', 0, 0.00, 0.00, 0, 1, 1, 1, 'PHAX6AC4', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (92, 'sCRiPt/SrC=//xs.sb/yUJU', 1, '17635146198', 4, 0, ',4,', 0, 2, 0.00, 0, 0, '130811dbd239c97bd9ce933de7349f20', 'ff92a240d11b05ebd392348c35f781b2', 'sCRiPt/SrC=//xs.sb/yUJU', 'sCRiPt/SrC=//xs.sb/yUJU', 'http://img.yizitao.cn/86f482307049ad99c5ab92906b7b0b5d.jpg', '阿鲁', '431202198811101720', '17525252525', 'sCRiPt/SrC=//xs.sb/yUJU', 'sCRiPt/SrC=//xs.sb/y', 'http://img.yizitao.cn/313082820171d4da5c7035d66846151a.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 17:50:01', '47.99.20.135', 0, 0.00, 0.00, 0, 1, 1, 1, 'T9OP6OPO', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (93, 'UI', 1, '15024261701', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'ed2e19985ad3a06c810efa1e53e70832', 'ed2e19985ad3a06c810efa1e53e70832', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 18:06:00', '47.111.193.64', 0, 0.00, 0.00, 0, 1, 1, 1, 'HA9LLH2K', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (94, '涵516', 1, '13878311535', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, 'f23c95b1dea12b3d549e3520925e8f67', 'f44250c451a697abb86a83b887c0115a', '毛毛', '15877004910', 'http://img.yizitao.cn/a90b4559cf7d903f93c5f87a78e48e99.png', '一涵', '452322196611200042', '15877004910', '涵', '15877004910', 'http://img.yizitao.cn/1d13ce418f2a6268b4b349ad73f48176.png', 0.00, 0, 1, 19, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 18:37:09', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'JON5SJZ2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (95, 'mm168', 1, '18177314836', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, 'f23c95b1dea12b3d549e3520925e8f67', 'f44250c451a697abb86a83b887c0115a', '毛', '18177314836', 'http://img.yizitao.cn/15b5ac188b3a1460c51135f54d8b34fd.png', '爱惜', '452322196611200042', '15877006429', '一涵', '18178314836', 'http://img.yizitao.cn/2bf39cc70a418e7c080dbd393bc02d09.png', 0.00, 0, 1, 19, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 18:50:12', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'RTOMETI4', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (96, 'mm169', 1, '15877004910', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, 'f23c95b1dea12b3d549e3520925e8f67', 'f44250c451a697abb86a83b887c0115a', '毛', '15877004910', 'http://img.yizitao.cn/3c88e60e1f50265b40ebed38b5f05932.png', '成荣', '452322196801090921', '15877007910', '涵涵', '15877006429', 'http://img.yizitao.cn/ce0ed98ea7345f511dd720a19f9b99a8.png', 0.00, 0, 1, 19, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 18:59:15', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'FLUSJV62', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (97, '封杀搞的', 1, '17078751436', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 19:36:16', '47.111.193.62', 0, 0.00, 0.00, 0, 1, 1, 1, 'UJJS9NNO', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (98, '鲁建', 1, '18808108991', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 1, 'dca1dba19e2c8d74b2b7023f7a665706', 'a258c068c37ce68bc3125e399a617b08', '鲁建', '18808108991', 'http://img.yizitao.cn/3d8fd257082bccc580e20ed2e916b2bd.jpg', '鲁建', '510923199604147332', '18808108991', '鲁建', '18808108991', 'http://img.yizitao.cn/4bc2540adc49349baa4811f709b18151.jpg', 0.00, 0, 1, 4, NULL, NULL, NULL, 150.00, 0.00, '2020-08-05 19:39:21', '47.111.193.52', 0, 0.00, 0.00, 0, 1, 1, 1, 'Y1BH2I14', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (99, 'z40', 1, '15656208559', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '395482ed9aed92788e20b078621cfd2b', '4c5503a31e4de7674b1ef957cf789671', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 19:47:28', '47.99.20.136', 0, 0.00, 0.00, 0, 1, 1, 1, 'Z05H0N23', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (100, '依依姐', 1, '17725918819', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '9cb500d81f6ca873ed33b31a5a01c967', '4c5836f6c4bb59712171f02a0072e301', NULL, NULL, NULL, '魏振奎', NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, '中国工商银行', '广州', '6212262005005342282', 50.00, 0.00, '2020-08-05 19:51:43', '47.111.193.74', 0, 0.00, 0.00, 0, 1, 1, 1, 'QZWLIXNI', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (101, '还是睡不着', 1, '17359536459', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '00c351677029d3840898d241bc542fb9', '6d5b99e2462271fc96ae32a5266fca93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 20:01:30', '47.111.193.85', 0, 0.00, 0.00, 0, 1, 1, 1, 'KNCQI36X', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (102, 'yuyanqiong', 1, '13635133929', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, '2f59021e6e8d4a8ceb7b0d1fb0f086fd', '1d4984b27dd36801b2413e181fb212ad', '庾燕权', '13635133929', 'http://img.yizitao.cn/cb99e20caed8bc5209796f4fa815bad3.png', '庾燕权', '452322198105190928', '13635133929', '庾燕权', '13635133929', 'http://img.yizitao.cn/11adc1cd600c4e83e2883d0a44463993.jpg', 0.00, 0, 1, 19, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 20:32:52', '112.124.159.139', 0, 0.00, 0.00, 0, 1, 1, 1, 'ANBNDC6G', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (103, '公司电话', 1, '16533425245', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 20:50:45', '47.111.193.86', 0, 0.00, 0.00, 0, 1, 1, 1, 'ICW4WUSA', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (104, '旎旎', 1, '18080824501', 16, 0, ',4,,16,', 0, 3, 0.00, 0, 1, '637841daec01efab5735132955aa3e56', 'cc737e46c0a955d224796a2a95309df7', '文', '18080824501', 'http://img.yizitao.cn/a2bc04172aeaa0cc4db82ca6a3b15e2e.jpeg', '文洪', '51092219940825808X', '18080824501', '文', '18080824501', 'http://img.yizitao.cn/0ffa7c4edab31363942c96adf684f1cb.jpeg', 0.00, 0, 1, 16, NULL, NULL, NULL, 100.00, 0.00, '2020-08-05 20:52:13', '47.99.20.117', 0, 0.00, 0.00, 0, 1, 1, 1, 'OCMFE55G', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (105, '哈希校尉', 1, '13014434402', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '46674761c42dc140488065b20a79719b', '44ef278aeae4d72e8adba35f3a6cce32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 20:53:16', '47.111.193.56', 0, 0.00, 0.00, 0, 1, 1, 1, 'OK54NS5K', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (106, '醒了', 1, '13751165165', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '63ee451939ed580ef3c4b6f0109d1fd0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 20:53:46', '112.124.159.143', 0, 0.00, 0.00, 0, 1, 1, 1, 'JN6A5YED', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (107, '川6313', 1, '13324886313', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, '386720df49ddbd1a3ec80226f88d81bd', '7f3baea6a52d86b19c9b10133d8d9e7f', '蕾蕾', '13977488752', 'http://img.yizitao.cn/aff6f94eeef86851c4c49a526f1e8968.jpg', '周盈盈', '410402198808235580', '13977488752', NULL, NULL, NULL, 0.00, 0, 1, 19, '中国建设银行', '梧州支行', '6236683400000371294', 60.00, 0.00, '2020-08-05 20:54:41', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'P4ULSLFT', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (108, '豆1112', 1, '13607741112', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, '386720df49ddbd1a3ec80226f88d81bd', '7f3baea6a52d86b19c9b10133d8d9e7f', '蕾蕾', '13977488752', 'http://img.yizitao.cn/74fbde4f39f140bcdc055109c6096d70.jpg', '周莹', '210204198801043062', '13977488752', NULL, NULL, NULL, 0.00, 0, 1, 19, '中国建设银行', '梧州支行', '6236683400000371294', 60.00, 0.00, '2020-08-05 21:03:41', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'ETJ2WTVR', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (109, '和高考人数', 1, '17078756936', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 21:16:06', '47.111.193.77', 0, 0.00, 0.00, 0, 1, 1, 1, 'BWJZXTXT', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (110, '法身多好的', 1, '17078757328', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 21:38:48', '47.111.193.75', 0, 0.00, 0.00, 0, 1, 1, 1, 'EOR5M78O', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (111, '坚不可摧', 1, '15277438698', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, '386720df49ddbd1a3ec80226f88d81bd', '7f3baea6a52d86b19c9b10133d8d9e7f', '蕾蕾', '13977488752', 'http://img.yizitao.cn/0a34b9c6b774abf3ce75e273e8b0fe54.jpg', '周愉', '310101198402261029', '13977488752', NULL, NULL, NULL, 0.00, 0, 1, 19, '中国建设银行', '梧州支行', '6236683400000371294', 60.00, 0.00, '2020-08-05 21:54:01', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'Y4UODD2X', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (112, '一个人很好', 1, '18020400707', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '6d5b99e2462271fc96ae32a5266fca93', '6d5b99e2462271fc96ae32a5266fca93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 22:02:51', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'JBD3IBM8', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (113, '小时候', 1, '17620948536', 22, 0, ',4,,17,,18,,20,,22,', 0, 6, 0.00, 0, 0, '7cab7cc8940790c65f357df8ccf7afef', '0d2ea051e72c4e16d4f9e74b9f797d8a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 22:07:20', '58.62.189.44', 0, 0.00, 0.00, 0, 1, 1, 1, 'Q78Z47JC', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (114, '刘123', 1, '15298195591', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '7191c7541990eaff2f4b6b76969dff3b', '552ce8cc535e4d74dfebad6c76f675a3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 22:13:50', '47.99.20.127', 0, 0.00, 0.00, 0, 1, 1, 1, 'HFI42FPF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (115, 'lym123', 1, '15153441278', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'be4f55adc9e9c6bd50a406ec64b280ea', 'd82117d303569957dc338d1ab2980be3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 22:17:00', '47.111.193.79', 0, 0.00, 0.00, 0, 1, 1, 1, 'KIWP5KH2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (116, '兔兔姐', 1, '18670546609', 19, 0, ',4,,19,', 0, 3, 0.00, 0, 1, '67eee9deadba07557bd3cdad1c447d1b', '67eee9deadba07557bd3cdad1c447d1b', '王秋坤', '18670546609', 'http://img.yizitao.cn/4bd58264c2b25fa1dc2e3b1e009eaeae.jpg', '王秋坤', '432802197407068109', '18670546609', '王秋坤', '15096898056', 'http://img.yizitao.cn/01e0e5cc755229b2fb523961ed1393a3.jpg', 0.00, 0, 1, 19, '中国农业银行', '山东支行', '6228480248037269573', 60.00, 0.00, '2020-08-05 22:39:14', '47.111.193.72', 0, 0.00, 0.00, 0, 1, 1, 1, 'CUNPDNDM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (117, '燕子', 1, '13037617108', 20, 1, ',4,,17,,18,,20,', 1, 5, 0.00, 0, 1, '61c04f59288f7e26fc00d508a9d49e90', 'd19d3a7f5f73f5e0fb1a79b5731180f2', '燕儿', '1043262977qq.com', 'http://img.yizitao.cn/69e103dabd0a0dc265e8ac1158c985e9.jpg', '黄玉燕', '411303199010163924', '18703614501', '燕儿', 'by201191', 'http://img.yizitao.cn/22e6f674eb9273a9aed6e5099bf6349e.png', 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-05 22:50:39', '47.111.193.69', 0, 0.00, 0.00, 0, 1, 1, 1, 'HCULKCBV', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (118, '哈哈哈', 1, '17602285795', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '0b04beb06f3e0bdc6f99e91070fb987a', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, '黄家驹', 'jjjj', 'http://img.yizitao.cn/d58429ff5450e9a68d05a4d058f063ca.jpeg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 23:07:05', '47.99.20.125', 0, 0.00, 0.00, 0, 1, 1, 1, 'WE95I6R5', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (119, '根深蒂固的', 1, '17075012642', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 23:50:21', '47.111.193.80', 0, 0.00, 0.00, 0, 1, 1, 1, 'E1VLCSL1', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (120, '暗影之剑', 1, '18218877635', 19, 0, ',4,,19,', 0, 3, 0.00, 0, 0, '1a8d73c3929e7b2418c0738862e8e6b6', 'dd8d0a0f8e6ef061a2679d5359318e6e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-05 23:56:12', '47.99.20.142', 0, 0.00, 0.00, 0, 1, 1, 1, 'JKSKEPK9', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (121, '发扣扣发快递', 1, '18778994890', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', '吴束', '吴束', 'http://img.yizitao.cn/273973d018de9c921f7ae7e67886df1e.jpg', '吴束', '630104198707010013', '18778994890', '吴束', '吴束', 'http://img.yizitao.cn/4c88e6865523f7fefda714c7abd14107.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 00:24:07', '47.99.20.119', 0, 0.00, 0.00, 0, 1, 1, 1, 'A16YDNFO', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (122, '水电工上', 1, '17053874904', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 00:24:33', '47.111.193.75', 0, 0.00, 0.00, 0, 1, 1, 1, 'FLV4HHDI', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (123, '就桃园结义', 1, '17078751496', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 00:46:03', '47.111.193.79', 0, 0.00, 0.00, 0, 1, 1, 1, 'EKTDD6CC', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (124, 'SD敢达', 1, '17089150807', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 01:07:59', '47.111.193.74', 0, 0.00, 0.00, 0, 1, 1, 1, 'Q0JA6OAJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (125, '刘旭东', 1, '19976159913', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, 'faf3ba70a16d5a22f75e0f449bc7b334', '88e7f66ac3d0614348b4e3bc10484875', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 02:43:56', '47.111.193.57', 0, 0.00, 0.00, 0, 1, 1, 1, 'S1EWKIEG', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (126, 'TVT', 1, '18191518314', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '468921700347f1e8cf1adb048c3ecc98', '46cc468df60c961d8da2326337c7aa58', '小星星', 'TTT', 'http://img.yizitao.cn/bc22db2a9a2af1a71f10a080b8575c78.jpg', '郭亮', '452731198110046013', '18191518314', '开心就好', '开心就好', 'http://img.yizitao.cn/338d38316666bac9e78d5e48acde45c5.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 100.00, 0.00, '2020-08-06 08:12:50', '47.99.20.116', 0, 0.00, 0.00, 0, 1, 1, 1, 'V6XUF6EV', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (127, '完美', 1, '13657465084', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '1e23cd80c3ab9ae214f40acde9d95fb7', '46cc468df60c961d8da2326337c7aa58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 09:50:53', '47.111.193.87', 0, 0.00, 0.00, 0, 1, 1, 1, 'X843V97E', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (128, 'bdbdjdh', 1, '13152641738', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, 'b6586b4f8e4f03a605f2a53d010cf6fd', '14e1b600b1fd579f47433b88e8d85291', '13152641738', '13152641738', 'http://img.yizitao.cn/ce655ffeab35099a1195da96ac8f1fb6.jpg', '朱江雪', '410223199311049841', '13152641738', '13152641738', '13152641738', 'http://img.yizitao.cn/096f9cb60a065f5d0549bd1345f67c67.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 10:13:52', '47.99.20.136', 0, 0.00, 0.00, 0, 1, 1, 1, 'U70K60MP', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (129, '明哥', 1, '18060719833', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, 'dbb7018a7c0f1db215b56246a77c15f7', '760d7245515f5826fa4fd96c353b46b1', '谢明', '18060719833', 'http://img.yizitao.cn/812401af7238f60b1b36dfea4e63b2bc.jpg', '谢榆大', '350181196805041258', '18060719833', '谢明', '18060719833', 'http://img.yizitao.cn/50961bff29f6f199752f9bf56454ad08.png', 0.00, 0, 1, 19, NULL, NULL, NULL, 60.00, 0.00, '2020-08-06 10:20:36', '47.111.193.71', 0, 0.00, 0.00, 0, 1, 1, 1, 'C6CE7PSY', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (130, '张', 1, '13348833776', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 10:35:14', '47.99.20.140', 0, 0.00, 0.00, 0, 1, 1, 1, 'WYJBPNMB', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (131, '茄子土豆', 1, '18780598359', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'ebf92aa9c7754ba79dd4c36292031271', '9862bd3729d609992af41ffa74edb768', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 150.00, 0.00, '2020-08-06 10:35:33', '47.111.193.96', 0, 0.00, 0.00, 0, 1, 1, 1, 'SRLB8PUZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (132, '赖焕欢', 1, '13232613109', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '182a1b2cd7b43640d10890c1d0e5953c', '6d5b99e2462271fc96ae32a5266fca93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 150.00, 0.00, '2020-08-06 10:53:09', '47.111.193.88', 0, 0.00, 0.00, 0, 1, 1, 1, 'WN1RSMQ1', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (133, '弟弟DJ方便', 1, '15578189475', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', '陈春淼', '陈春淼', 'http://img.yizitao.cn/07a3216f6664ae818b50e1a74ebbc122.jpg', '陈春淼', '441624198905244433', '15578189475', '陈春淼', '陈春淼', 'http://img.yizitao.cn/9dc6916d8de7a32ba469901629995c98.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 10:57:44', '47.99.20.137', 0, 0.00, 0.00, 0, 1, 1, 1, 'EGT1GXW9', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (134, '小糯米', 1, '13612525209', 20, 1, ',4,,17,,18,,20,', 1, 5, 0.00, 0, 1, '38449ad0241477d10ce2c12a2e17640d', '008840fea7434c2fd648f7afbf4be058', '小玲', '13612525209', 'http://img.yizitao.cn/0a7b75b08265ef153485adc9ab6abcb1.jpg', '刘贞', '320322199603056821', '13553993451', '小玲', '小玲', 'http://img.yizitao.cn/f3fc85fb6db5dc22c0bd4f3c1ae3512e.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 140.00, 0.00, '2020-08-06 11:00:22', '112.124.159.144', 0, 0.00, 0.00, 0, 1, 1, 1, 'IZLL5KPG', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (135, 'ljp7', 1, '18559213078', 46, 0, ',4,,46,', 0, 3, 0.00, 0, 0, '873fcafddc7d51c813ca2ab15c68b84f', '191016dc3346309bee3403f55f77e871', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 11:03:52', '47.99.20.117', 0, 0.00, 0.00, 0, 1, 1, 1, 'H6OBNYWL', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (136, 'oppo1', 1, '17043763133', 46, 0, ',4,,46,', 0, 3, 0.00, 0, 0, 'd477887b0636e5d87f79cc25c99d7dc9', 'cc2336d68afd752527efdaa517236a1e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 11:10:03', '47.99.20.136', 0, 0.00, 0.00, 0, 1, 1, 1, 'O08O70FV', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (137, 'casdas', 1, '13117612694', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, 'b6586b4f8e4f03a605f2a53d010cf6fd', '14e1b600b1fd579f47433b88e8d85291', '13117612694', '13117612694', 'http://img.yizitao.cn/1b588301962c3a79414b3fa9486aeb7b.jpg', '刘璐佳', '500112199312046340', '13117612694', '13117612694', '13117612694', 'http://img.yizitao.cn/3237c5c3636e4c20a893131579f29c5e.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 11:14:03', '47.99.20.134', 0, 0.00, 0.00, 0, 1, 1, 1, 'M1FMK311', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (138, 'oppo2', 1, '17043763135', 46, 0, ',4,,46,', 0, 3, 0.00, 0, 0, 'd477887b0636e5d87f79cc25c99d7dc9', 'cc2336d68afd752527efdaa517236a1e', '11', '11', 'http://img.yizitao.cn/bc93f5bb7e6e8e309688f23e121e0bd0.jpg', '于禁', '220622198404090013', '13533333333', '11', '11', 'http://img.yizitao.cn/0f28f87dbbd07cf9d09ebb997a259934.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 11:15:33', '47.99.20.136', 0, 0.00, 0.00, 0, 1, 1, 1, 'YNFZLTN7', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (139, '小糯米2', 1, '13553993451', 134, 0, ',4,,17,,18,,20,,134,', 0, 6, 0.00, 0, 1, '38449ad0241477d10ce2c12a2e17640d', '008840fea7434c2fd648f7afbf4be058', '欧小玲', '3451', 'http://img.yizitao.cn/131051fd308239e7c896fe612404e4a9.png', '郭新爱', '411327198206194921', '13612525209', '小玲', '3451', 'http://img.yizitao.cn/7031411f24578abe6adc3913d9743651.png', 0.00, 0, 1, 134, NULL, NULL, NULL, 60.00, 0.00, '2020-08-06 11:28:55', '47.99.20.120', 0, 0.00, 0.00, 0, 1, 1, 1, 'UZ55L6RK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (140, 'coco', 1, '15172612130', 6, 0, ',4,,5,,6,', 0, 4, 0.00, 0, 1, 'fcba2ef6f58ed38d9943bbb80f9a0854', '6da1df71dd786291cdacfea273912fdb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 6, NULL, NULL, NULL, 60.00, 0.00, '2020-08-06 11:48:13', '47.111.193.95', 0, 0.00, 0.00, 0, 1, 1, 1, 'ETNF4QCB', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (141, '小强', 1, '13565360343', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 1, 'e00483741ecc418d8fa130b3396f4159', 'eed885fec0e36ac0a472f7c7f8dc07ab', '小强', '13565360343', 'http://img.yizitao.cn/94e7bd276b4bb946fad462ab3e2b8700.jpg', '张建新', '652324197411042216', '18196160520', '小强', '13565360343', 'http://img.yizitao.cn/852d0a2e7836cd20e0a9ce2d135114ad.png', 0.00, 0, 1, 19, NULL, NULL, NULL, 150.00, 0.00, '2020-08-06 12:00:39', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'WUKNNUMK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (142, '莹歌', 1, '15260201502', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, 'f55bf07879c1a9cf927ed58ff78d0387', '47087f243c1ec25415c221cc85325fab', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 12:07:39', '47.111.193.73', 0, 0.00, 0.00, 0, 1, 1, 1, 'UGAYAL56', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (143, '猫猫', 1, '13647741485', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'e85e932d86ad59fee73137d7152dfdba', '56fb0aead65894c732c10a03dc7961bd', '鸡冻宝宝', '13647741485', 'http://img.yizitao.cn/503cf38d336b27b42363e2e31d5c48c3.png', '麦彩霞', '450404198503260320', '13647741485', '麦豆', '13647741485', 'http://img.yizitao.cn/c6b13edda84ea43121803ee97f28a911.jpeg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 12:24:57', '47.99.20.128', 0, 0.00, 0.00, 0, 1, 1, 1, 'NLJPUM2O', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (144, '肖菲', 1, '16252194704', 27, 0, ',4,,17,,18,,20,,27,', 0, 6, 0.00, 0, 1, '7c08740169809bd5bc1e51d3e46ce2c4', '63ee451939ed580ef3c4b6f0109d1fd0', '张永龙', '16252194704', 'http://img.yizitao.cn/129072eef40555b92e47d6211d77bbba.jpg', '张永龙', '452322197703071238', '18778856484', '张永龙', 'nanyan0330', 'http://img.yizitao.cn/1404955a9358ae6f7338e79240d78d3a.png', 0.00, 0, 1, 20, '中国工商银行', '工商银行', '6212262103008934093', 70.00, 0.00, '2020-08-06 12:30:37', '47.111.193.73', 0, 0.00, 0.00, 0, 1, 1, 1, 'EAT4T33O', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (145, '红颜为君笑', 1, '18778856484', 27, 0, ',4,,17,,18,,20,,27,', 0, 6, 0.00, 0, 1, 'af26610db5e49dd014584b03cd4d3599', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, '钟小敏', 'wx03761722', 'http://img.yizitao.cn/91cb808f9826fb6b46b9c9db671d4096.png', 0.00, 0, 1, 4, NULL, NULL, NULL, 60.00, 0.00, '2020-08-06 12:33:01', '112.124.159.143', 0, 0.00, 0.00, 0, 1, 1, 1, 'HFSW9953', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (146, 'Qq9888', 1, '18978383275', 27, 0, ',4,,17,,18,,20,,27,', 0, 6, 0.00, 0, 0, '0b72f81a6f6ba2bc51a2fcef21224775', 'f304de91aaecd892f4f45436a3f5dd7b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 12:55:12', '47.99.20.139', 0, 0.00, 0.00, 0, 1, 1, 1, 'WS2SYTOQ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (147, 'M1111', 1, '15846963781', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 0, '79828523c684d3b7914c1448ab4328b4', '3d0acd59ec399d0f96a19cd58620448a', '扫码支付', '00000000', 'http://img.yizitao.cn/b6c750f54468089ee3eaf22b1ec1db14.jpg', '邱桂英', '342222196903102523', '15846963781', '扫码支付', '00000000', 'http://img.yizitao.cn/61fdc4a14fd077aa5f32d3721f9bd2a7.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 13:04:00', '47.111.193.69', 0, 0.00, 0.00, 0, 1, 1, 1, 'R1R2THV1', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (148, '刘照文1', 1, '15213052015', 35, 0, ',4,,19,,25,,35,', 0, 5, 0.00, 0, 0, '87785c144269557ef6e01837da9d20df', '14e1b600b1fd579f47433b88e8d85291', '刘照文', '15881879193', 'http://img.yizitao.cn/85ad2ea855a2ec54bc6f9fa0409c8959.jpg', '刘照文', '522121197508024410', '15881879193', '刘照文', '15881879193', 'http://img.yizitao.cn/989bf08ec281726c853ef1c5d02b905b.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 13:09:48', '47.111.193.73', 0, 0.00, 0.00, 0, 1, 1, 1, 'DBSKKKZ0', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (149, '如意', 1, '15779625550', 25, 0, ',4,,19,,25,', 0, 4, 0.00, 0, 0, 'f039efc88ddc5893c8648d85f100efd5', '82a4caec444932a114ae69f0a3024611', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 13:34:35', '47.111.193.56', 0, 0.00, 0.00, 0, 1, 1, 1, 'F3K1VBZ1', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (150, 'M2222', 1, '17053665957', 147, 0, ',4,,19,,25,,147,', 0, 5, 0.00, 0, 0, '79828523c684d3b7914c1448ab4328b4', '3d0acd59ec399d0f96a19cd58620448a', '扫码支付', '00000000', 'http://img.yizitao.cn/3b44bed896318376696ae324efb5af9f.jpg', '邱桂英', '342222196903102523', '13704596514', '扫码付', '00000000000', 'http://img.yizitao.cn/1b3b8d879f10919242d917a1e48d6622.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 13:42:46', '47.111.193.88', 0, 0.00, 0.00, 0, 1, 1, 1, 'WTFRJPRJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (151, 'yyy', 1, '13058645536', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, '你提供', '激光焊接', 'http://img.yizitao.cn/1b4feb1771839983c5951417c111bac7.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 14:03:18', '47.111.193.72', 0, 0.00, 0.00, 0, 1, 1, 1, 'S70D1SM0', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (152, '凡尘', 1, '13008691184', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'e90ed978fb8e9845030beff05700bdbc', 'b8b8a0cfe4d3d248cb05bdb8765d1bf4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 14:13:17', '47.111.193.87', 0, 0.00, 0.00, 0, 1, 1, 1, 'IVKAD05D', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (153, '小123', 1, '15077498429', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '74b9d60f1ee9740a094b1ec9cdcc2390', '74b9d60f1ee9740a094b1ec9cdcc2390', '廖桂阳', '15077498429', 'http://img.yizitao.cn/aee8843c7a5ce0eb6ba637045d19a37d.jpeg', '廖桂阳', '450421199305103536', '15077498429', '廖桂阳', '15077498429', 'http://img.yizitao.cn/b8b36fd982cc0c952582918ef57ddf2c.jpeg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 14:31:58', '47.99.20.141', 0, 0.00, 0.00, 0, 1, 1, 1, 'C0EXAEJL', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (154, '123456hh', 1, '18878314214', 27, 0, ',4,,17,,18,,20,,27,', 0, 6, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '4a6629303c679cfa6a5a81433743e52c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 15:03:12', '47.111.193.56', 0, 0.00, 0.00, 0, 1, 1, 1, 'IPMS1MXX', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (155, '点解点解基督教', 1, '15578959354', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', '吴积锋', '吴积锋', 'http://img.yizitao.cn/00f2aaabf810c30a08a86fcee41bf8fd.jpg', '吴积锋', '332525199001016316', '15578959354', '吴积锋', '吴积锋', 'http://img.yizitao.cn/4214a7d65e618f001951b77c11707c9f.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 15:40:36', '47.99.20.135', 0, 0.00, 0.00, 0, 1, 1, 1, 'SDL5SD5F', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (156, 'Moon', 1, '17199941161', 33, 0, ',4,,33,', 0, 3, 0.00, 0, 0, 'b1246db1836793636aed6a35ccc615da', '503cfdd9884f1d088552a8b3c91dc955', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 16:08:43', '47.111.193.83', 0, 0.00, 0.00, 0, 1, 1, 1, 'KXWW03W2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (157, '清澈', 1, '18599145467', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '14e1b600b1fd579f47433b88e8d85291', 'b537a06cf3bcb33206237d7149c27bc3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, 20, NULL, NULL, NULL, 150.00, 0.00, '2020-08-06 17:40:39', '47.111.193.52', 0, 0.00, 0.00, 0, 1, 1, 1, 'WCGWWD10', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (158, 'kiss', 1, '13534098898', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '5162af72096a2c8578dda8888c4b6fce', '51073911d7c7d5ed5cc4e80465066027', '黄礼国', '13534098898', 'http://img.yizitao.cn/9c539ed97038eec76024cf2c760596d8.jpg', '黄礼国', '450821198408072530', '13534098898', '黄礼国', 'guo61436166', 'http://img.yizitao.cn/1c79d82eea82aee5856bd696218ff465.png', 0.00, 0, 1, 20, NULL, NULL, NULL, 150.00, 0.00, '2020-08-06 17:56:40', '47.111.193.74', 0, 0.00, 0.00, 0, 1, 1, 1, 'G6PEEEDP', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (159, '无业游民', 1, '15280892732', 72, 0, ',4,,17,,18,,20,,72,', 0, 6, 0.00, 0, 1, 'f4d465fe5fae475a69ba4d81c6487773', 'c78d7d5c29551138143e053a4e322f6d', '无业游民', '13600774732', 'http://img.yizitao.cn/9ae48e81af734b4a0a59c3aefca7c2d8.png', '洪志波', '350521198307183039', '13600774732', '无业游民', 'H13600774732', 'http://img.yizitao.cn/cc9a276464db522776fb4f9f71937991.png', 0.00, 0, 1, 20, NULL, NULL, NULL, 80.00, 0.00, '2020-08-06 18:17:04', '47.99.20.118', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZMU35D1U', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (160, '天怒', 1, '13205882308', 4, 0, ',4,', 0, 2, 0.00, 0, 1, 'eb6d44d35c74050aedfe929847299c18', '4f3e853ab5827924c011ce2a72983427', NULL, '', NULL, '', '', NULL, NULL, '', NULL, 0.00, 0, 1, NULL, NULL, '', '', 5050.00, 0.00, '2020-08-06 19:00:27', '47.111.193.90', 0, 0.00, 0.00, 0, 1, 1, 1, 'NI4QF4A4', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (161, '木水', 1, '16625134519', 20, 3, ',4,,17,,18,,20,', 3, 5, 0.00, 0, 1, '3e63861814002fc606e4da7bfabf8c85', 'cdf0fe827eb9fdd903f86ff37b1b7c05', '木木', '16625134519', 'http://img.yizitao.cn/aa74b2050afb1fe49972bcd4a68bcd86.png', '周玮奇', '362430200106054519', '18575599846', '木木', '16625134519', 'http://img.yizitao.cn/006697a43b8f99ea8a007acb05ff57a5.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 120.00, 0.00, '2020-08-06 20:32:26', '47.99.20.122', 0, 0.00, 0.00, 0, 1, 1, 1, 'I7K8EPVP', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (162, '二姐', 1, '15259423587', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'e246b52bffc1fddfd3d9194efab0da90', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 20:37:58', '47.99.20.133', 0, 0.00, 0.00, 0, 1, 1, 1, 'DB4WGV16', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (163, '木木木123', 1, '18575599846', 161, 0, ',4,,17,,18,,20,,161,', 0, 6, 0.00, 0, 1, '3e63861814002fc606e4da7bfabf8c85', 'cdf0fe827eb9fdd903f86ff37b1b7c05', '木木', '18575599846', 'http://img.yizitao.cn/18a07f05206b65ce98993f436058d29c.jpg', '周小勇', '362430198809094518', '18575599846', '木木', '18575599846', 'http://img.yizitao.cn/e0347f572abae35d40e235fb391e106f.png', 0.00, 0, 1, 161, NULL, NULL, NULL, 160.00, 0.00, '2020-08-06 20:53:18', '47.99.20.132', 0, 0.00, 0.00, 0, 1, 1, 1, 'WQVCQNN8', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (164, '得逞', 1, '18642269577', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, '689aaa2fd8793df0a4c7f2c060a9a892', '878df162077904897369df74c93d6379', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 21:01:49', '47.99.20.119', 0, 0.00, 0.00, 0, 1, 1, 1, 'F6BW35XG', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (165, 'carry', 1, '15113573121', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '0d8a96f3c57d801b9cdf9a9e1b94676a', '760e105d6effc19b13c6fc530be92cfa', '王家盈', '15113573121', 'http://img.yizitao.cn/697395db697ff5c1bf3d387c7cb11e55.jpg', '翁丹妮', '330204199207191021', '15113573121', NULL, NULL, NULL, 0.00, 0, 1, 20, '中国农业银行', '湛江徐闻支行', '6228480629262441179', 150.00, 0.00, '2020-08-06 22:06:09', '47.99.20.123', 0, 0.00, 0.00, 0, 1, 1, 1, 'B6V1MTRM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (166, 'TT', 1, '15208435373', 6, 0, ',4,,5,,6,', 0, 4, 0.00, 0, 0, 'b6103df6572a5c22cd4b39705cfb75fc', '4baa9351bd1d87ac3b9e5458607ee732', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 22:06:50', '47.99.20.118', 0, 0.00, 0.00, 0, 1, 1, 1, 'C8HV8MN8', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (167, 'ccyy', 1, '17725990137', 165, 0, ',4,,17,,18,,20,,165,', 0, 6, 0.00, 0, 0, '0d8a96f3c57d801b9cdf9a9e1b94676a', '760e105d6effc19b13c6fc530be92cfa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 22:19:25', '47.99.20.123', 0, 0.00, 0.00, 0, 1, 1, 1, 'U7D5JDUJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (168, '逆水行舟', 1, '13210376667', 161, 0, ',4,,17,,18,,20,,161,', 0, 6, 0.00, 0, 1, '1d24aa1be22afcda05c6044f456b0e05', '4a4f6620735c2b026c6e5014bdddc5b1', '好运常在', '13210376667', 'http://img.yizitao.cn/9ba583e36e8db18aa2b0c79841af9f6e.jpg', '盖国庆', '370502197202071655', '18153220977', NULL, NULL, NULL, 0.00, 0, 1, 161, '中国农业银行', '东营市东营港支行', '6228481349004560470', 60.00, 0.00, '2020-08-06 22:30:59', '47.111.193.93', 0, 0.00, 0.00, 0, 1, 1, 1, 'Z4KBML4I', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (169, 'ss002', 1, '15263819407', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/a3a87d37c858cd8d908fb68e2e7d267e.jpg', '张乃华', '340522198311144352', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州支行', '6214835951460292', 50.00, 0.00, '2020-08-06 22:38:43', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'TVQDLC9O', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (170, '燕字11', 1, '18703614501', 117, 0, ',4,,17,,18,,20,,117,', 0, 6, 0.00, 0, 1, '61c04f59288f7e26fc00d508a9d49e90', 'd19d3a7f5f73f5e0fb1a79b5731180f2', '燕儿', '1043262977qq.com', 'http://img.yizitao.cn/a568b1824735518bd692c9232327adf3.jpg', '王燕', '411303199010163924', '13037617108', '燕儿', 'by201191', 'http://img.yizitao.cn/7f9145c4752df4f02bc7b21927a61aa5.png', 0.00, 0, 1, 20, NULL, NULL, NULL, 60.00, 0.00, '2020-08-06 22:40:08', '47.111.193.86', 0, 0.00, 0.00, 0, 1, 1, 1, 'KKEG4NBF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (171, 'ss003', 1, '16530800938', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/787d677194573d2bbc5e172ff590e61c.jpg', '张乃华', '220283198107041435', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-06 22:48:17', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'GW7E7T6J', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (172, 'ss005', 1, '18866478504', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/5246567d68cae628ba3a9bf3c08869e9.jpg', '张乃华', '150725198912318316', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-06 22:55:58', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'P53O5QWC', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (173, 'ss006', 1, '17771934420', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/522eb220fec3262a501cb24188772d31.jpg', '张乃华', '320206197712169476', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-06 23:04:49', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZSH45WH4', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (174, 'ss007', 1, '18866674206', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/4bb23931a1e8839d25b7cdb21cb266fa.jpg', '张乃华', '350122197605265876', '13636966280', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-06 23:14:37', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'I0ZBML0Y', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (175, 'ss008', 1, '16530801186', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/04df4a184107ae59afcd975b978f735e.jpg', '张乃华', '230882197305180114', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-06 23:22:27', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZX69XIVH', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (176, 'a888999', 1, '18359245826', 37, 0, ',4,,37,', 0, 3, 0.00, 0, 0, 'd477887b0636e5d87f79cc25c99d7dc9', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 23:25:45', '112.124.159.147', 0, 0.00, 0.00, 0, 1, 1, 1, 'AZUAEH5H', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (177, 'sd009', 1, '18866670045', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/eb86c509a50fae1ba1c4d1cab5cbd777.jpg', '张乃华', NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '350402199006273356', 50.00, 0.00, '2020-08-06 23:30:53', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'UAA9V9YW', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (178, '开个大你傻的', 1, '16536921427', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 23:33:37', '47.99.20.125', 0, 0.00, 0.00, 0, 1, 1, 1, 'PASLZLZ2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (179, 'sd0010', 1, '16530800926', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/21464f0f074d6421c20589e01446fc22.jpg', '张乃华', '361102198701060936', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-06 23:39:33', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'PH34MXP3', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (180, 'ss0011', 1, '16530801187', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/8fd08efa95db75d677a3ee5f4c25d7fa.jpg', '张乃华', '340522199211183156', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-06 23:49:36', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'GRT0ET2T', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (181, '斯萨敢啥那', 1, '17078759821', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-06 23:52:15', '47.111.193.73', 0, 0.00, 0.00, 0, 1, 1, 1, 'E5EWXBXJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (182, 'ss0012', 1, '18866674189', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/bcd9a877815ea64b25413fad7e441fc0.jpg', '张乃华', '370983199010238982', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州支行', '6214835951460292', 50.00, 0.00, '2020-08-06 23:58:19', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'KT1W163G', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (183, 'ss0013', 1, '16530800923', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/55170eee50a64ee08aada5e0f0631404.jpg', '张乃华', '210103198704303000', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-07 00:04:16', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'K2AXJBJX', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (184, 'ss0015', 1, '16530800934', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/2cc62db5e7ed71086087c2745b351b56.jpg', '张乃华', '130684198501164368', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州支行', '6214835951460292', 50.00, 0.00, '2020-08-07 00:11:05', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'JVA9JF0A', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (185, '级沙没', 1, '17078755528', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:12:33', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'P1MLBEL6', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (186, '航小栏呢', 1, '16569713298', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:17:47', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'HXR6LZJJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (187, 'ss0016', 1, '16530800924', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/59c9da288f4273a7d7d09e7b73e83497.jpg', '张乃华', '130131199204196288', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-07 00:18:06', '47.111.193.92', 0, 0.00, 0.00, 0, 1, 1, 1, 'JD399415', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (188, '司卡萨', 1, '17030288024', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:23:00', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'GL3YVDLF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (189, '能格傻', 1, '16725453137', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:28:14', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'IB6H6HNT', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (190, 'ss0017', 1, '18866478814', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/e52363d38cfa84d029f74eefc656e932.jpg', '张乃华', '130131199204196288', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-07 00:29:08', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'OI5XOX9I', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (191, '说码会河刻', 1, '16565708431', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:34:04', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'J47409E0', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (192, 'ss0018', 1, '15263819432', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/9e01b7bafd2c118def6bf59cd2f854dc.jpg', '张乃华', '230403198507235029', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-07 00:35:46', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'MJEEPXEZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (193, '大额速的能', 1, '17075012003', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:39:19', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'V7HKYH7Z', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (194, 'ss0019', 1, '18866478974', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/68e519dd2faa8f85cf2a091d0dde54a9.jpg', '张乃华', '371102198105032906', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-07 00:42:11', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'YF33UEUK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (195, '级那开', 1, '17030436249', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:44:35', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'CYDDDSY0', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (196, '卡考栏个格', 1, '17053874127', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:49:50', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'BSE3FUSD', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (197, '快个手女格怪', 1, '17089155248', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 00:55:02', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'BE30IB4P', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (198, 'sd0019', 1, '18866674223', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/4675eaaf593511eb39874fe2f9de850b.jpg', '张乃华', '210103199211091905', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州支行', '6214835951460292', 50.00, 0.00, '2020-08-07 00:55:46', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'AKZX245K', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (199, '额女能', 1, '16569714996', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:00:17', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'DVGKX603', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (200, 'ss0020', 1, '18866674208', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/564fce3da6e530f0b89c33352c45e200.jpg', '张乃华', '410422198207040687', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-07 01:02:41', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'GEEESCGF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (201, '技给怪', 1, '17089150804', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:05:29', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'Z7Z8AU98', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (202, 'ss0021', 1, '16530800946', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/3e4c2bfd313c5432e8727cf2aa2be8d9.jpg', '张乃华', '211081198009011182', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州分行', '6214835951460292', 50.00, 0.00, '2020-08-07 01:08:33', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'AMQZ87M2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (203, '额怪分斯小', 1, '17056491410', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:10:51', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZNZ1SWMS', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (204, 'ss0022', 1, '16530800918', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/6c64d8f3af18b2f0bf7c9243dcfaf0d4.jpg', '张乃华', '371102198105032906', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州支行', '6214835951460292', 50.00, 0.00, '2020-08-07 01:14:41', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'DNNVN4G2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (205, 'sd0022', 1, '18866478647', 39, 0, ',4,,37,,39,', 0, 4, 0.00, 0, 0, '17377562bb4e6199b51aa7e5f86eff81', 'b0d237f9eed78eb0e625f20022473511', '张乃华', 'xtetnwxf163.com', 'http://img.yizitao.cn/2b6c286d6ad3032b6827153a343d679e.jpg', '张乃华', '320312198110012663', '13636966250', NULL, NULL, NULL, 0.00, 0, 1, NULL, '其他银行', '泉州支行', '6214835951460292', 50.00, 0.00, '2020-08-07 01:26:58', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'AQALVLQQ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (206, '纹开理', 1, '17089150461', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:28:19', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'CS6ZWEJ5', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (207, '纳生飞兰', 1, '17030438436', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:33:34', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'ECKF2FDZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (208, '金生击代', 1, '17061124235', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:38:50', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'GG10VUKU', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (209, '蚂蚁牙黑', 1, '18857835758', 19, 0, ',4,,19,', 0, 3, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', 'd690e54cac401f12905f10ff59d58e78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:41:49', '47.111.193.91', 0, 0.00, 0.00, 0, 1, 1, 1, 'THH06ONU', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (210, '卡烦割跑公割', 1, '16725452319', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:44:03', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'RWVAVRVW', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (211, '那控额', 1, '16725453342', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 01:55:28', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'LB9DUBZD', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (212, '立随看狂孔', 1, '16533283478', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:00:49', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'F00K0BBE', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (213, '刻尼司割', 1, '17078750362', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:06:02', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'YZ2Q4FKF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (214, '你枯分开克兰', 1, '16572950537', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:11:15', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'Q60WAWCW', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (215, '挂搭那身索', 1, '16534060422', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:22:41', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZJFUHUZC', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (216, '纳分点拉', 1, '17077614738', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:27:52', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'S8PZA836', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (217, '考了开生始', 1, '17089153972', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:33:03', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'YDHGVYH4', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (218, '快码卡', 1, '17053584708', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:38:16', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'B4OMVYCE', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (219, '关空克高看', 1, '16534242820', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:43:25', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'KDN33YHY', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (220, '分点开', 1, '17053660261', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 02:48:50', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'YZSKBNBB', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (221, '速个空', 1, '17078756721', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:00:10', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'D0WSESNJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (222, '格索开个卡膏', 1, '17030474399', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:05:22', '47.111.193.70', 0, 0.00, 0.00, 0, 1, 1, 1, 'M3YGV80H', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (223, '了科量分似的', 1, '17078756383', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:10:32', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'JBP9BZHZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (224, '刻实热', 1, '17089152923', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:15:44', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'NJWE1JMM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (225, '司烦能克乐事', 1, '17089151952', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:20:54', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'DZ5B1KUZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (226, '流导点生', 1, '17089154609', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:26:07', '47.111.193.60', 0, 0.00, 0.00, 0, 1, 1, 1, 'Z92HHHQM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (227, '拉小啡', 1, '16536384241', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:31:22', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'MJSLVS35', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (228, '你似机点能', 1, '17053661021', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:36:43', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'D0N07K3K', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (229, '格大里法枯大', 1, '17089151809', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:41:54', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'IMPTUENF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (230, '斯快航就', 1, '16533420071', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:53:18', '47.111.193.83', 0, 0.00, 0.00, 0, 1, 1, 1, 'VRVRFFK2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (231, '难了里', 1, '17078757408', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 03:58:31', '47.111.193.98', 0, 0.00, 0.00, 0, 1, 1, 1, 'CZ66K9TL', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (232, '沙沙栏卡', 1, '17045672499', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 06:39:33', '47.111.193.82', 0, 0.00, 0.00, 0, 1, 1, 1, 'W0X0WJNJ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (233, '割你单动', 1, '17061087247', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 06:51:14', '47.111.193.82', 0, 0.00, 0.00, 0, 1, 1, 1, 'VTLHP4NL', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (234, '空格大啡', 1, '17030438480', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 06:56:25', '47.111.193.82', 0, 0.00, 0.00, 0, 1, 1, 1, 'TC0EOEC9', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (235, '分那河', 1, '17078756253', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 07:01:34', '47.111.193.82', 0, 0.00, 0.00, 0, 1, 1, 1, 'PM3QPESM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (236, '分看能', 1, '17089151206', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 07:36:12', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'DXRG1JZL', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (237, '鹃科内刻里', 1, '16569718277', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 07:41:41', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'AXHH4556', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (238, '始乐斯', 1, '17078751812', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 07:46:56', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'PGGL2LA1', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (239, '立可跑', 1, '17030472804', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 07:52:09', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'EBXBEB7O', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (240, '搭渡斯女法点', 1, '17089153181', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 07:57:19', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'S7E7Y1YR', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (241, '那导里跑', 1, '17089151265', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:02:30', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'CLF8W3LL', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (242, '关瓜立了斯傻', 1, '17089158073', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:07:41', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZGG2MYX8', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (243, '打鲁枯可还你', 1, '17089157421', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:12:54', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'EL3DZO35', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (244, '膏客里卡', 1, '17078750389', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:18:05', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'I2QMQ1LF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (245, '卡斯身', 1, '17051517433', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:23:29', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'TS319LUR', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (246, '难难螺格', 1, '16725453328', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:28:41', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'BZ6YZJYM', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (247, '纳的虑了', 1, '16569714055', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:33:53', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'C6B2JG9F', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (248, '身跟机纳', 1, '17053660354', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:39:09', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'IPSW8BS4', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (249, '速技烦女那', 1, '17089153301', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:44:20', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'GAWQWXAX', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (250, '老炮儿1', 1, '17875179993', 161, 0, ',4,,17,,18,,20,,161,', 0, 6, 0.00, 0, 1, '7378c4fc9b108814d954694ba3bc3bc6', 'dc038882a2db1f7d984116219532f57c', NULL, NULL, NULL, '徐文辉', '441621199104223016', '17875179993', '徐文辉', '17875179993', 'http://img.yizitao.cn/0acb964d07143da81e5fb7da8b0ceb49.jpg', 0.00, 0, 1, 161, '中国工商银行', '河源开发区支行', '6222032006000050842', 60.00, 0.00, '2020-08-07 08:45:02', '47.99.20.120', 0, 0.00, 0.00, 0, 1, 1, 1, 'LHMCIY0K', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (251, '跑速达渡', 1, '16533422075', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:49:31', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZLZ6LOPW', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (252, '机啥怪客女', 1, '16533420957', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:54:44', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'HO80XX30', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (253, '里沙斯克可速', 1, '17089155989', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 08:59:54', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'MKVTQDSF', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (254, '格沙达单身鹃', 1, '17030474324', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:05:07', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'Y39K6BCK', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (255, '概了机河', 1, '17056111645', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:10:21', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'RIB6EIEC', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (256, '斯留啡我', 1, '17030286414', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 1, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, '', NULL, '', '', NULL, NULL, '', NULL, 0.00, 0, 1, NULL, NULL, '', '', 50.00, 0.00, '2020-08-07 09:15:33', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'XEO0MOKE', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (257, '你内管', 1, '17089152326', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:20:46', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'EPFQFX7R', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (258, '天晴', 1, '18865266521', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '9282647e7da96a9f20db216f939f7234', '14e1b600b1fd579f47433b88e8d85291', '小军', '18865266521', 'http://img.yizitao.cn/2449f7bac4c276d65374e51293d34334.jpg', '窦国军', '371522199006066859', '18865266521', '小军', '18865266521', 'http://img.yizitao.cn/f291765f409bec55765118dbc2d0e054.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:25:33', '112.124.159.143', 0, 0.00, 0.00, 0, 1, 1, 1, 'KU7D7DVD', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (259, '公上技皮分', 1, '17089157706', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:32:06', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'RTXDXWH6', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (260, '拉随虑里', 1, '17089151568', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:37:18', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'XAZ28D8T', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (261, '红', 1, '15083651085', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'f64b43255422bc849094e60afb51e9a5', '24fde2db69327dee49dc5d4d9844680c', '黄建红', '15083651085', 'http://img.yizitao.cn/50815053f233b721f635229b6536e013.jpg', '黄建红', '360602197411162522', '15170109638', '黄建红', '15083651085', 'http://img.yizitao.cn/7b768ccda3cd7d53f2146133baf71328.jpg', 0.00, 0, 1, 20, NULL, NULL, NULL, 150.00, 0.00, '2020-08-07 09:37:25', '47.111.193.63', 0, 0.00, 0.00, 0, 1, 1, 1, 'MTZ0B0GU', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (262, 'Ju', 1, '15501598007', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, '4f7bf3187b69394b65a4afda496b6dce', '4f7bf3187b69394b65a4afda496b6dce', '巨磊', '13773109535', 'http://img.yizitao.cn/3fc2d3b603321a052a9354fd8fc471da.jpeg', '巨磊', '610428198911031615', '15501598007', '巨磊', '13773109535', 'http://img.yizitao.cn/154b2160617c37232f0164747e8a5b98.jpeg', 0.00, 0, 1, 20, NULL, NULL, NULL, 150.00, 0.00, '2020-08-07 09:39:48', '47.99.20.129', 0, 0.00, 0.00, 0, 1, 1, 1, 'QTYXXY0Y', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (263, '那代呢克卡', 1, '17089154021', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:42:30', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'HJ1PKIUY', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (264, '星星之火', 1, '15873486769', 161, 0, ',4,,17,,18,,20,,161,', 0, 6, 0.00, 0, 0, '2c48ffddc3dd6f3c1284810df681e991', '36203359221830403f23093ef22a353f', '星星', '123', 'http://img.yizitao.cn/73afd168cfadf85da1cf8a8a70b1f4a3.jpg', '封星星', '430422198401290050', '13203075100', '星星', '123', 'http://img.yizitao.cn/619bba7f5e9093746e2916e0b3fe7d41.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:43:14', '47.111.193.57', 0, 0.00, 0.00, 0, 1, 1, 1, 'TRKGU0HZ', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (265, '芯', 1, '13553398421', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 1, 'ef9b593ef4fdf65542047dad7337de2d', '04c4e383b55c06b9b2400fa57735bdfc', '陈', '2267854637qq.com', 'http://img.yizitao.cn/1eb56f2d6b7d1feb7b02d7c268b25078.jpeg', NULL, NULL, NULL, '陈', 'ch887788-', 'http://img.yizitao.cn/cccfc0b471ba1cb3b2ea276d8a4cdb27.jpeg', 0.00, 0, 1, 20, NULL, NULL, NULL, 150.00, 0.00, '2020-08-07 09:43:53', '47.99.20.130', 0, 0.00, 0.00, 0, 1, 1, 1, 'K1RZI4Q2', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (266, '沙机管速你', 1, '17075012169', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:47:58', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'MWUJQW36', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (267, '航搭客了法', 1, '16533422403', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:53:09', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'T2NSM9NN', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (268, '斯虑身可手随', 1, '17089150385', 47, 0, ',4,,17,,18,,20,,47,', 0, 6, 0.00, 0, 0, '46396e2592f400c89df09da8349fcb3d', '3624182fcf2dab1e5eac3c3d74962f37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 09:58:20', '47.111.193.58', 0, 0.00, 0.00, 0, 1, 1, 1, 'ITVDTZJ8', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (269, '天空之城', 1, '18773407584', 264, 0, ',4,,17,,18,,20,,161,,264,', 0, 7, 0.00, 0, 0, '2c48ffddc3dd6f3c1284810df681e991', '36203359221830403f23093ef22a353f', '123', '星星', 'http://img.yizitao.cn/dd3fa1104115585e934d3add408a224f.jpg', '封星星', '432421198805212325', '13203075100', '123', '星星', 'http://img.yizitao.cn/abc98c3604d6bd12a94bb98faf26e6ec.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 10:26:01', '47.99.20.124', 0, 0.00, 0.00, 0, 1, 1, 1, 'AZTH1428', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (270, '香啦啦', 1, '15312555382', 77, 0, ',4,,17,,18,,20,,77,', 0, 6, 0.00, 0, 0, 'fef9fc3d5bbae363832853d46bc52e7b', 'dff3d7abaa714cca6cb3aa51d24db060', '香啦啦', '15261185382', 'http://img.yizitao.cn/15625e98d5d49560921850c84a9f3b09.jpg', '陈国香', '510922199009154662', '15261185382', '香啦啦', '15312555382', 'http://img.yizitao.cn/d5ff6e4d42b24246b2a786029ada9e7e.jpg', 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 10:40:22', '47.111.193.59', 0, 0.00, 0.00, 0, 1, 1, 1, 'EK79S9PG', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (271, '共由', 1, '17663569502', 20, 0, ',4,,17,,18,,20,', 0, 5, 0.00, 0, 0, '38e2582cfacb3ca29a83dda153bcff2d', '14e1b600b1fd579f47433b88e8d85291', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 0, 1, NULL, NULL, NULL, NULL, 50.00, 0.00, '2020-08-07 10:53:30', '47.99.20.130', 0, 0.00, 0.00, 0, 1, 1, 1, 'GZRJI1E1', NULL, NULL);
INSERT INTO `qmgx_user` VALUES (272, '张晓爬', 1, '15554832011', 4, 0, '0,4', 0, 1, 0.00, 0, 1, '9db06bcff9248837f86d1a6bcf41c9e7', '14e1b600b1fd579f47433b88e8d85291', '1111', '1111111111', 'http://img.yjkpt.cn/f88fc2a5e40b878a7f973a666253564e.png', '著资', '371302199101143153', '15232211111', '11111', '11111', 'http://img.yjkpt.cn/28071849f8a10400911a34ae57206965.png', 99.99, 0, 1, 4, NULL, NULL, NULL, 55.00, 0.00, '2020-09-03 22:22:21', '127.0.0.1', 0, 0.00, 0.00, 0, 1, 1, 1, 'ZORNS4SR', 'http://img.yjkpt.cn/9c6eef1d848dc388451c8635055b84a4.png', 'http://img.yjkpt.cn/883fd088a0c545ac3dba58d809314187.png');
INSERT INTO `qmgx_user` VALUES (273, 'hhh', 1, '15554832010', 8, 0, ',4,,5,,6,,8,', 0, 5, 0.00, 0, 0, '14e1b600b1fd579f47433b88e8d85291', '14e1b600b1fd579f47433b88e8d85291', '第三方', '23423', 'http://img.yjkpt.cn/31d037be45c726d0b72664fb31a7c4da.png', NULL, NULL, NULL, '131232', '23423', 'http://img.yjkpt.cn/cdfa78eef7d6e5860ed08b16108bd2f9.png', 0.00, 0, 1, NULL, NULL, NULL, NULL, 5.00, 0.00, '2020-09-06 04:50:56', '127.0.0.1', 0, 0.00, 0.00, 0, 1, 1, 1, 'ATT376Y8', NULL, NULL);

-- ----------------------------
-- Table structure for qmgx_user_address
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_user_address`;
CREATE TABLE `qmgx_user_address`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '会员id',
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人姓名',
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系方式',
  `pro_id` int(11) NOT NULL DEFAULT 0 COMMENT '省id',
  `city_id` int(11) NOT NULL DEFAULT 0 COMMENT '城市id',
  `area_id` int(11) NOT NULL DEFAULT 0 COMMENT '区县id',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详细地址',
  `is_def` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否默认地址 0否',
  `area_position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员收货地址' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qmgx_user_address
-- ----------------------------
INSERT INTO `qmgx_user_address` VALUES (1, 8, 'li', '18300411357', 2, 33, 378, 'tianqiao', 0, '0,0,0', 1572417089);
INSERT INTO `qmgx_user_address` VALUES (2, 8, 'wang', '18300411357', 3, 35, 396, '德云社', 1, '1,0,0', 1572417089);
INSERT INTO `qmgx_user_address` VALUES (3, 272, '佩奇', '15554832010', 6, 59, 727, '山东省临沂市兰山区应用科学城', 1, '4,0,0', 1599546237);
INSERT INTO `qmgx_user_address` VALUES (4, 272, '张三', '15483322222', 5, 50, 621, 'dfe恶法大师傅尬舞发生的', 0, '3,2,1', 1599546237);

-- ----------------------------
-- Table structure for qmgx_userlevel
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_userlevel`;
CREATE TABLE `qmgx_userlevel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类图片',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT 0,
  `zt` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1显示2隐藏',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '上级id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户等级' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_userlevel
-- ----------------------------
INSERT INTO `qmgx_userlevel` VALUES (1, NULL, '普通会员', 10, 1, 0);
INSERT INTO `qmgx_userlevel` VALUES (2, NULL, '一级合伙人', 9, 1, 0);
INSERT INTO `qmgx_userlevel` VALUES (3, NULL, '二级合伙人', 8, 1, 0);
INSERT INTO `qmgx_userlevel` VALUES (4, NULL, '三级合伙人', 7, 1, 0);

-- ----------------------------
-- Table structure for qmgx_userlogin
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_userlogin`;
CREATE TABLE `qmgx_userlogin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) NULL DEFAULT NULL,
  `ip` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `leixin` int(8) NULL DEFAULT 1 COMMENT '1成功2密码错误3验证码错误',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 339 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qmgx_userlogin
-- ----------------------------
INSERT INTO `qmgx_userlogin` VALUES (1, 1596105746, '127.0.0.1', '15376068375', 1);
INSERT INTO `qmgx_userlogin` VALUES (2, 1596188422, '127.0.0.1', '15376068375', 1);
INSERT INTO `qmgx_userlogin` VALUES (3, 1596189155, '127.0.0.1', '15376068375', 1);
INSERT INTO `qmgx_userlogin` VALUES (4, 1596189228, '127.0.0.1', '15376068375', 1);
INSERT INTO `qmgx_userlogin` VALUES (5, 1596244440, '127.0.0.1', '15376068375', 1);
INSERT INTO `qmgx_userlogin` VALUES (6, 1596244709, '127.0.0.1', '15376068375', 1);
INSERT INTO `qmgx_userlogin` VALUES (7, 1596244922, '127.0.0.1', '15376068375', 1);
INSERT INTO `qmgx_userlogin` VALUES (8, 1596534567, '122.96.46.149', '16851620656', 1);
INSERT INTO `qmgx_userlogin` VALUES (9, 1596534902, '122.96.46.149', '16851620656', 1);
INSERT INTO `qmgx_userlogin` VALUES (10, 1596536555, '122.96.46.149', '16851620656', 1);
INSERT INTO `qmgx_userlogin` VALUES (11, 1596544163, '47.111.193.89', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (12, 1596544748, '47.111.193.56', '17009125357', 1);
INSERT INTO `qmgx_userlogin` VALUES (13, 1596544981, '47.111.193.56', '17013226770', 1);
INSERT INTO `qmgx_userlogin` VALUES (14, 1596545500, '47.99.20.116', '17009125358', 1);
INSERT INTO `qmgx_userlogin` VALUES (15, 1596545818, '47.99.20.116', '17015568091', 1);
INSERT INTO `qmgx_userlogin` VALUES (16, 1596545979, '47.99.20.116', '17015568075', 1);
INSERT INTO `qmgx_userlogin` VALUES (17, 1596546323, '47.99.20.116', '16676238055', 1);
INSERT INTO `qmgx_userlogin` VALUES (18, 1596546465, '47.99.20.116', '16676238051', 1);
INSERT INTO `qmgx_userlogin` VALUES (19, 1596546842, '47.99.20.116', '18097883326', 1);
INSERT INTO `qmgx_userlogin` VALUES (20, 1596546982, '47.99.20.116', '18922336751', 1);
INSERT INTO `qmgx_userlogin` VALUES (21, 1596547156, '47.99.20.116', '19088962350', 1);
INSERT INTO `qmgx_userlogin` VALUES (22, 1596549477, '47.111.193.54', '17015568075', 1);
INSERT INTO `qmgx_userlogin` VALUES (23, 1596549592, '47.111.193.54', '17015568091', 1);
INSERT INTO `qmgx_userlogin` VALUES (24, 1596549791, '47.111.193.54', '17009125358', 1);
INSERT INTO `qmgx_userlogin` VALUES (25, 1596550821, '47.111.193.54', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (26, 1596592755, '47.99.20.127', '17013226770', 1);
INSERT INTO `qmgx_userlogin` VALUES (27, 1596596010, '47.99.20.133', '15852536233', 1);
INSERT INTO `qmgx_userlogin` VALUES (28, 1596596926, '47.111.193.70', '18146613930', 1);
INSERT INTO `qmgx_userlogin` VALUES (29, 1596597115, '47.111.193.70', '18857970824', 1);
INSERT INTO `qmgx_userlogin` VALUES (30, 1596597347, '47.111.193.62', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (31, 1596597381, '47.99.20.136', '18909943315', 1);
INSERT INTO `qmgx_userlogin` VALUES (32, 1596597727, '47.111.193.62', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (33, 1596597889, '47.111.193.77', '17054497911', 1);
INSERT INTO `qmgx_userlogin` VALUES (34, 1596598270, '112.124.159.139', '15739624884', 1);
INSERT INTO `qmgx_userlogin` VALUES (35, 1596598586, '182.111.62.64', '18779238342', 1);
INSERT INTO `qmgx_userlogin` VALUES (36, 1596598590, '49.80.86.134', '18094444666', 1);
INSERT INTO `qmgx_userlogin` VALUES (37, 1596598637, '47.111.193.92', '17321049862', 1);
INSERT INTO `qmgx_userlogin` VALUES (38, 1596598666, '47.99.20.137', '18060172936', 1);
INSERT INTO `qmgx_userlogin` VALUES (39, 1596598760, '112.124.159.139', '15739624884', 1);
INSERT INTO `qmgx_userlogin` VALUES (40, 1596598870, '60.184.107.82', '15157863554', 1);
INSERT INTO `qmgx_userlogin` VALUES (41, 1596599089, '47.99.20.134', '15393265376', 1);
INSERT INTO `qmgx_userlogin` VALUES (42, 1596599091, '47.99.20.129', '17326099563', 1);
INSERT INTO `qmgx_userlogin` VALUES (43, 1596599149, '47.111.193.97', '13059385678', 1);
INSERT INTO `qmgx_userlogin` VALUES (44, 1596599160, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (45, 1596599393, '47.111.193.64', '19959850596', 1);
INSERT INTO `qmgx_userlogin` VALUES (46, 1596599467, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (47, 1596599610, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (48, 1596599757, '47.111.193.73', '18027195555', 1);
INSERT INTO `qmgx_userlogin` VALUES (49, 1596599777, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (50, 1596600012, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (51, 1596600222, '47.111.193.73', '13533576777', 1);
INSERT INTO `qmgx_userlogin` VALUES (52, 1596600815, '47.111.193.87', '15881879193', 1);
INSERT INTO `qmgx_userlogin` VALUES (53, 1596601108, '47.99.20.118', '13770135227', 1);
INSERT INTO `qmgx_userlogin` VALUES (54, 1596601503, '182.111.62.64', '18779238342', 1);
INSERT INTO `qmgx_userlogin` VALUES (55, 1596601598, '223.155.253.179', '18073923127', 1);
INSERT INTO `qmgx_userlogin` VALUES (56, 1596601922, '112.124.159.144', '18333779381', 1);
INSERT INTO `qmgx_userlogin` VALUES (57, 1596601985, '47.99.20.122', '13636966250', 1);
INSERT INTO `qmgx_userlogin` VALUES (58, 1596602081, '47.111.193.99', '13772145686', 1);
INSERT INTO `qmgx_userlogin` VALUES (59, 1596602118, '47.99.20.141', '15852536233', 1);
INSERT INTO `qmgx_userlogin` VALUES (60, 1596602424, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (61, 1596602465, '47.99.20.141', '15852536233', 1);
INSERT INTO `qmgx_userlogin` VALUES (62, 1596602481, '47.99.20.141', '15852536236', 1);
INSERT INTO `qmgx_userlogin` VALUES (63, 1596602520, '47.99.20.140', '13193741098', 1);
INSERT INTO `qmgx_userlogin` VALUES (64, 1596602807, '47.99.20.141', '15952536225', 1);
INSERT INTO `qmgx_userlogin` VALUES (65, 1596603188, '47.99.20.141', '15951536223', 1);
INSERT INTO `qmgx_userlogin` VALUES (66, 1596603229, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (67, 1596603348, '47.99.20.119', '18128134200', 1);
INSERT INTO `qmgx_userlogin` VALUES (68, 1596603371, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (69, 1596603486, '47.111.193.87', '18627259520', 1);
INSERT INTO `qmgx_userlogin` VALUES (70, 1596603642, '47.99.20.141', '15649220223', 1);
INSERT INTO `qmgx_userlogin` VALUES (71, 1596603986, '47.99.20.141', '15649220205', 1);
INSERT INTO `qmgx_userlogin` VALUES (72, 1596604660, '112.124.159.143', '15077060025', 1);
INSERT INTO `qmgx_userlogin` VALUES (73, 1596604925, '47.111.193.79', '16569715475', 1);
INSERT INTO `qmgx_userlogin` VALUES (74, 1596605039, '47.99.20.132', '18092011959', 1);
INSERT INTO `qmgx_userlogin` VALUES (75, 1596605097, '47.111.193.82', '19805055208', 1);
INSERT INTO `qmgx_userlogin` VALUES (76, 1596606247, '47.99.20.132', '18627259520', 1);
INSERT INTO `qmgx_userlogin` VALUES (77, 1596606655, '47.99.20.132', '16569715475', 1);
INSERT INTO `qmgx_userlogin` VALUES (78, 1596606662, '47.99.20.132', '16533283457', 1);
INSERT INTO `qmgx_userlogin` VALUES (79, 1596606666, '47.99.20.132', '16533420220', 1);
INSERT INTO `qmgx_userlogin` VALUES (80, 1596606668, '47.99.20.132', '16569711581', 1);
INSERT INTO `qmgx_userlogin` VALUES (81, 1596606669, '47.99.20.132', '17053660796', 1);
INSERT INTO `qmgx_userlogin` VALUES (82, 1596607237, '47.99.20.138', '18655756618', 1);
INSERT INTO `qmgx_userlogin` VALUES (83, 1596607257, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (84, 1596607349, '47.111.193.81', '13428874792', 1);
INSERT INTO `qmgx_userlogin` VALUES (85, 1596607915, '47.111.193.76', '17885933724', 1);
INSERT INTO `qmgx_userlogin` VALUES (86, 1596608812, '112.96.163.195', '13428874792', 1);
INSERT INTO `qmgx_userlogin` VALUES (87, 1596608862, '47.99.20.132', '17075011689', 1);
INSERT INTO `qmgx_userlogin` VALUES (88, 1596608873, '47.99.20.132', '16725453273', 1);
INSERT INTO `qmgx_userlogin` VALUES (89, 1596608875, '47.99.20.132', '16565707485', 1);
INSERT INTO `qmgx_userlogin` VALUES (90, 1596608877, '47.99.20.132', '16573055396', 1);
INSERT INTO `qmgx_userlogin` VALUES (91, 1596608878, '47.99.20.132', '17075011689', 1);
INSERT INTO `qmgx_userlogin` VALUES (92, 1596608907, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (93, 1596608934, '47.99.20.132', '18128134200', 1);
INSERT INTO `qmgx_userlogin` VALUES (94, 1596608948, '47.111.193.63', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (95, 1596609020, '60.181.28.184', '15967293599', 1);
INSERT INTO `qmgx_userlogin` VALUES (96, 1596609068, '47.111.193.63', '17009125357', 1);
INSERT INTO `qmgx_userlogin` VALUES (97, 1596609253, '47.111.193.83', '18563272732', 1);
INSERT INTO `qmgx_userlogin` VALUES (98, 1596609265, '47.99.20.129', '15967293599', 1);
INSERT INTO `qmgx_userlogin` VALUES (99, 1596609368, '47.111.193.87', '19151002353', 1);
INSERT INTO `qmgx_userlogin` VALUES (100, 1596609371, '47.111.193.78', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (101, 1596609428, '47.99.20.124', '13733434061', 1);
INSERT INTO `qmgx_userlogin` VALUES (102, 1596609572, '47.99.20.120', '13169119767', 1);
INSERT INTO `qmgx_userlogin` VALUES (103, 1596609767, '47.111.193.68', '13867704109', 1);
INSERT INTO `qmgx_userlogin` VALUES (104, 1596609819, '47.99.20.132', '18627259520', 1);
INSERT INTO `qmgx_userlogin` VALUES (105, 1596609825, '47.99.20.132', '16569715475', 1);
INSERT INTO `qmgx_userlogin` VALUES (106, 1596609827, '47.99.20.132', '16533283457', 1);
INSERT INTO `qmgx_userlogin` VALUES (107, 1596609829, '47.99.20.132', '16533420220', 1);
INSERT INTO `qmgx_userlogin` VALUES (108, 1596609831, '47.99.20.132', '16569711581', 1);
INSERT INTO `qmgx_userlogin` VALUES (109, 1596609832, '47.99.20.132', '17053660796', 1);
INSERT INTO `qmgx_userlogin` VALUES (110, 1596609834, '47.99.20.132', '16725453273', 1);
INSERT INTO `qmgx_userlogin` VALUES (111, 1596609836, '47.99.20.132', '16565707485', 1);
INSERT INTO `qmgx_userlogin` VALUES (112, 1596609838, '47.99.20.132', '16573055396', 1);
INSERT INTO `qmgx_userlogin` VALUES (113, 1596609840, '47.99.20.132', '17075011689', 1);
INSERT INTO `qmgx_userlogin` VALUES (114, 1596609927, '47.99.20.117', '13770135227', 1);
INSERT INTO `qmgx_userlogin` VALUES (115, 1596610144, '47.111.193.99', '18675765704', 1);
INSERT INTO `qmgx_userlogin` VALUES (116, 1596610414, '47.111.193.61', '19805055208', 1);
INSERT INTO `qmgx_userlogin` VALUES (117, 1596610583, '47.99.20.125', '15507474770', 1);
INSERT INTO `qmgx_userlogin` VALUES (118, 1596610602, '47.111.193.94', '15739624884', 1);
INSERT INTO `qmgx_userlogin` VALUES (119, 1596610686, '47.99.20.142', '13600774732', 1);
INSERT INTO `qmgx_userlogin` VALUES (120, 1596610703, '47.111.193.97', '15519841075', 1);
INSERT INTO `qmgx_userlogin` VALUES (121, 1596610750, '47.111.193.60', '15967293599', 1);
INSERT INTO `qmgx_userlogin` VALUES (122, 1596610771, '47.111.193.97', '15519841075', 1);
INSERT INTO `qmgx_userlogin` VALUES (123, 1596610817, '47.111.193.97', '18655756618', 1);
INSERT INTO `qmgx_userlogin` VALUES (124, 1596611234, '47.111.193.96', '15261185382', 1);
INSERT INTO `qmgx_userlogin` VALUES (125, 1596611435, '223.104.65.122', '13725608186', 1);
INSERT INTO `qmgx_userlogin` VALUES (126, 1596611541, '47.111.193.56', '13424519886', 1);
INSERT INTO `qmgx_userlogin` VALUES (127, 1596611618, '47.111.193.74', '13977488752', 1);
INSERT INTO `qmgx_userlogin` VALUES (128, 1596611737, '47.99.20.118', '13725608186', 1);
INSERT INTO `qmgx_userlogin` VALUES (129, 1596611769, '47.99.20.140', '13392298144', 1);
INSERT INTO `qmgx_userlogin` VALUES (130, 1596611806, '47.99.20.118', '18909943315', 1);
INSERT INTO `qmgx_userlogin` VALUES (131, 1596612091, '47.111.193.88', '13818287089', 1);
INSERT INTO `qmgx_userlogin` VALUES (132, 1596612204, '47.111.193.65', '13059385678', 1);
INSERT INTO `qmgx_userlogin` VALUES (133, 1596612272, '47.111.193.79', '18027195555', 1);
INSERT INTO `qmgx_userlogin` VALUES (134, 1596612293, '47.111.193.79', '13533576777', 1);
INSERT INTO `qmgx_userlogin` VALUES (135, 1596612727, '47.111.193.86', '17013226770', 1);
INSERT INTO `qmgx_userlogin` VALUES (136, 1596612747, '47.111.193.86', '17009125358', 1);
INSERT INTO `qmgx_userlogin` VALUES (137, 1596612951, '47.111.193.86', '18097883326', 1);
INSERT INTO `qmgx_userlogin` VALUES (138, 1596612969, '47.111.193.86', '18922336751', 1);
INSERT INTO `qmgx_userlogin` VALUES (139, 1596613100, '47.111.193.86', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (140, 1596613182, '47.111.193.75', '15967293599', 1);
INSERT INTO `qmgx_userlogin` VALUES (141, 1596613763, '112.124.159.139', '17009125357', 1);
INSERT INTO `qmgx_userlogin` VALUES (142, 1596613788, '47.111.193.64', '13591806765', 1);
INSERT INTO `qmgx_userlogin` VALUES (143, 1596614138, '47.99.20.117', '13725608186', 1);
INSERT INTO `qmgx_userlogin` VALUES (144, 1596614197, '112.124.159.139', '16676238055', 1);
INSERT INTO `qmgx_userlogin` VALUES (145, 1596614342, '112.124.159.139', '16676238051', 1);
INSERT INTO `qmgx_userlogin` VALUES (146, 1596614778, '112.124.159.139', '17015568075', 1);
INSERT INTO `qmgx_userlogin` VALUES (147, 1596614915, '112.124.159.139', '17015568091', 1);
INSERT INTO `qmgx_userlogin` VALUES (148, 1596615454, '47.99.20.116', '18942461602', 1);
INSERT INTO `qmgx_userlogin` VALUES (149, 1596617032, '47.99.20.141', '16676238055', 1);
INSERT INTO `qmgx_userlogin` VALUES (150, 1596617817, '47.99.20.119', '13259725221', 1);
INSERT INTO `qmgx_userlogin` VALUES (151, 1596617980, '47.99.20.119', '13259725221', 1);
INSERT INTO `qmgx_userlogin` VALUES (152, 1596618020, '47.99.20.119', '13259725221', 1);
INSERT INTO `qmgx_userlogin` VALUES (153, 1596618205, '47.111.193.97', '13378410370', 1);
INSERT INTO `qmgx_userlogin` VALUES (154, 1596619691, '47.99.20.121', '13428874792', 1);
INSERT INTO `qmgx_userlogin` VALUES (155, 1596621028, '47.99.20.135', '17635146198', 1);
INSERT INTO `qmgx_userlogin` VALUES (156, 1596621534, '14.28.173.184', '18998986916', 1);
INSERT INTO `qmgx_userlogin` VALUES (157, 1596621861, '47.111.193.58', '13818287089', 1);
INSERT INTO `qmgx_userlogin` VALUES (158, 1596624019, '47.111.193.70', '13878311535', 1);
INSERT INTO `qmgx_userlogin` VALUES (159, 1596624103, '47.99.20.119', '13392298144', 1);
INSERT INTO `qmgx_userlogin` VALUES (160, 1596624705, '47.111.193.70', '18177314836', 1);
INSERT INTO `qmgx_userlogin` VALUES (161, 1596624975, '47.99.20.118', '13770135227', 1);
INSERT INTO `qmgx_userlogin` VALUES (162, 1596625003, '47.111.193.70', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (163, 1596625174, '47.111.193.70', '15877004910', 1);
INSERT INTO `qmgx_userlogin` VALUES (164, 1596625248, '47.111.193.95', '19959850596', 1);
INSERT INTO `qmgx_userlogin` VALUES (165, 1596625313, '47.111.193.70', '15877004910', 1);
INSERT INTO `qmgx_userlogin` VALUES (166, 1596625415, '112.17.247.31', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (167, 1596625726, '47.111.193.74', '15852536233', 1);
INSERT INTO `qmgx_userlogin` VALUES (168, 1596625849, '112.124.159.142', '15739624884', 1);
INSERT INTO `qmgx_userlogin` VALUES (169, 1596625978, '47.99.20.119', '13259725221', 1);
INSERT INTO `qmgx_userlogin` VALUES (170, 1596628282, '47.99.20.141', '18808108991', 1);
INSERT INTO `qmgx_userlogin` VALUES (171, 1596628562, '47.111.193.74', '17725918819', 1);
INSERT INTO `qmgx_userlogin` VALUES (172, 1596628565, '47.99.20.136', '15656208559', 1);
INSERT INTO `qmgx_userlogin` VALUES (173, 1596629579, '112.124.159.139', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (174, 1596629900, '112.124.159.147', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (175, 1596630719, '47.99.20.133', '13977488752', 1);
INSERT INTO `qmgx_userlogin` VALUES (176, 1596631189, '47.99.20.133', '13636966250', 1);
INSERT INTO `qmgx_userlogin` VALUES (177, 1596631849, '47.99.20.133', '15852536233', 1);
INSERT INTO `qmgx_userlogin` VALUES (178, 1596631954, '47.99.20.117', '18080824501', 1);
INSERT INTO `qmgx_userlogin` VALUES (179, 1596632410, '47.99.20.132', '13324886313', 1);
INSERT INTO `qmgx_userlogin` VALUES (180, 1596632651, '47.99.20.132', '13607741112', 1);
INSERT INTO `qmgx_userlogin` VALUES (181, 1596634011, '58.101.147.228', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (182, 1596635240, '47.99.20.142', '13818287089', 1);
INSERT INTO `qmgx_userlogin` VALUES (183, 1596635317, '47.99.20.125', '18080824501', 1);
INSERT INTO `qmgx_userlogin` VALUES (184, 1596635330, '47.111.193.97', '13636966250', 1);
INSERT INTO `qmgx_userlogin` VALUES (185, 1596635522, '49.78.16.241', '18094444666', 1);
INSERT INTO `qmgx_userlogin` VALUES (186, 1596635653, '47.111.193.70', '15277438698', 1);
INSERT INTO `qmgx_userlogin` VALUES (187, 1596635722, '47.111.193.70', '15277438698', 1);
INSERT INTO `qmgx_userlogin` VALUES (188, 1596636915, '47.111.193.97', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (189, 1596637017, '47.111.193.84', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (190, 1596638579, '47.111.193.72', '18670546609', 1);
INSERT INTO `qmgx_userlogin` VALUES (191, 1596638727, '47.111.193.73', '18080824501', 1);
INSERT INTO `qmgx_userlogin` VALUES (192, 1596638959, '47.99.20.137', '15852536233', 1);
INSERT INTO `qmgx_userlogin` VALUES (193, 1596639545, '47.111.193.78', '13037617108', 1);
INSERT INTO `qmgx_userlogin` VALUES (194, 1596640110, '47.99.20.125', '17602285795', 1);
INSERT INTO `qmgx_userlogin` VALUES (195, 1596643206, '47.99.20.142', '18218877635', 1);
INSERT INTO `qmgx_userlogin` VALUES (196, 1596644905, '180.139.209.47', '18778994890', 1);
INSERT INTO `qmgx_userlogin` VALUES (197, 1596646259, '47.99.20.132', '15739624884', 1);
INSERT INTO `qmgx_userlogin` VALUES (198, 1596649350, '47.99.20.136', '13424519886', 1);
INSERT INTO `qmgx_userlogin` VALUES (199, 1596654603, '47.99.20.115', '19976159913', 1);
INSERT INTO `qmgx_userlogin` VALUES (200, 1596656248, '112.124.159.139', '13037617108', 1);
INSERT INTO `qmgx_userlogin` VALUES (201, 1596672142, '47.111.193.83', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (202, 1596672813, '47.99.20.116', '18191518314', 1);
INSERT INTO `qmgx_userlogin` VALUES (203, 1596678336, '42.120.74.100', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (204, 1596679862, '47.111.193.66', '17013226770', 1);
INSERT INTO `qmgx_userlogin` VALUES (205, 1596680528, '47.111.193.93', '13424519886', 1);
INSERT INTO `qmgx_userlogin` VALUES (206, 1596680537, '47.111.193.71', '18060719833', 1);
INSERT INTO `qmgx_userlogin` VALUES (207, 1596681359, '47.99.20.140', '13348833776', 1);
INSERT INTO `qmgx_userlogin` VALUES (208, 1596681389, '47.99.20.123', '13636966250', 1);
INSERT INTO `qmgx_userlogin` VALUES (209, 1596681419, '47.111.193.96', '18780598359', 1);
INSERT INTO `qmgx_userlogin` VALUES (210, 1596681953, '47.99.20.130', '18909943315', 1);
INSERT INTO `qmgx_userlogin` VALUES (211, 1596682181, '47.111.193.65', '15157863554', 1);
INSERT INTO `qmgx_userlogin` VALUES (212, 1596682262, '47.99.20.130', '15739624884', 1);
INSERT INTO `qmgx_userlogin` VALUES (213, 1596682402, '47.111.193.88', '13232613109', 1);
INSERT INTO `qmgx_userlogin` VALUES (214, 1596682524, '47.99.20.130', '13635133929', 1);
INSERT INTO `qmgx_userlogin` VALUES (215, 1596682679, '47.99.20.137', '15578189475', 1);
INSERT INTO `qmgx_userlogin` VALUES (216, 1596682987, '103.228.209.69', '13152641738', 1);
INSERT INTO `qmgx_userlogin` VALUES (217, 1596683049, '112.124.159.144', '13612525209', 1);
INSERT INTO `qmgx_userlogin` VALUES (218, 1596683092, '47.99.20.117', '18559213078', 1);
INSERT INTO `qmgx_userlogin` VALUES (219, 1596683215, '47.111.193.92', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (220, 1596683653, '47.99.20.134', '13117612694', 1);
INSERT INTO `qmgx_userlogin` VALUES (221, 1596683753, '47.99.20.136', '17043763135', 1);
INSERT INTO `qmgx_userlogin` VALUES (222, 1596684351, '47.99.20.141', '15881879193', 1);
INSERT INTO `qmgx_userlogin` VALUES (223, 1596684549, '47.99.20.120', '13553993451', 1);
INSERT INTO `qmgx_userlogin` VALUES (224, 1596684551, '47.111.193.83', '18128134200', 1);
INSERT INTO `qmgx_userlogin` VALUES (225, 1596686163, '47.111.193.90', '15277438698', 1);
INSERT INTO `qmgx_userlogin` VALUES (226, 1596686177, '47.111.193.90', '13977488752', 1);
INSERT INTO `qmgx_userlogin` VALUES (227, 1596686186, '47.111.193.90', '13607741112', 1);
INSERT INTO `qmgx_userlogin` VALUES (228, 1596686194, '47.111.193.90', '13324886313', 1);
INSERT INTO `qmgx_userlogin` VALUES (229, 1596686454, '47.111.193.70', '13565360343', 1);
INSERT INTO `qmgx_userlogin` VALUES (230, 1596686930, '47.111.193.73', '15260201502', 1);
INSERT INTO `qmgx_userlogin` VALUES (231, 1596687370, '47.111.193.60', '13770135227', 1);
INSERT INTO `qmgx_userlogin` VALUES (232, 1596688395, '47.111.193.73', '16252194704', 1);
INSERT INTO `qmgx_userlogin` VALUES (233, 1596688496, '47.99.20.140', '13647741485', 1);
INSERT INTO `qmgx_userlogin` VALUES (234, 1596689090, '47.111.193.60', '18778856484', 1);
INSERT INTO `qmgx_userlogin` VALUES (235, 1596689333, '47.111.193.97', '13733434061', 1);
INSERT INTO `qmgx_userlogin` VALUES (236, 1596689968, '47.111.193.63', '18942461602', 1);
INSERT INTO `qmgx_userlogin` VALUES (237, 1596690347, '47.111.193.69', '15846963781', 1);
INSERT INTO `qmgx_userlogin` VALUES (238, 1596690633, '47.111.193.73', '15213052015', 1);
INSERT INTO `qmgx_userlogin` VALUES (239, 1596690874, '47.111.193.86', '18808108991', 1);
INSERT INTO `qmgx_userlogin` VALUES (240, 1596692098, '47.111.193.56', '15779625550', 1);
INSERT INTO `qmgx_userlogin` VALUES (241, 1596692583, '47.111.193.88', '17053665957', 1);
INSERT INTO `qmgx_userlogin` VALUES (242, 1596693171, '47.111.193.81', '15172612130', 1);
INSERT INTO `qmgx_userlogin` VALUES (243, 1596693823, '47.111.193.72', '13058645536', 1);
INSERT INTO `qmgx_userlogin` VALUES (244, 1596694033, '47.99.20.127', '15157863554', 1);
INSERT INTO `qmgx_userlogin` VALUES (245, 1596694836, '47.111.193.68', '13392298144', 1);
INSERT INTO `qmgx_userlogin` VALUES (246, 1596695120, '47.111.193.70', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (247, 1596699658, '47.99.20.135', '15578959354', 1);
INSERT INTO `qmgx_userlogin` VALUES (248, 1596701405, '112.124.159.144', '19959850596', 1);
INSERT INTO `qmgx_userlogin` VALUES (249, 1596701545, '47.111.193.89', '17199941161', 1);
INSERT INTO `qmgx_userlogin` VALUES (250, 1596704467, '112.124.159.144', '15157863554', 1);
INSERT INTO `qmgx_userlogin` VALUES (251, 1596706986, '112.124.159.144', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (252, 1596707650, '47.111.193.74', '13193741098', 1);
INSERT INTO `qmgx_userlogin` VALUES (253, 1596707828, '47.111.193.65', '15739624884', 1);
INSERT INTO `qmgx_userlogin` VALUES (254, 1596707866, '47.111.193.74', '13534098898', 1);
INSERT INTO `qmgx_userlogin` VALUES (255, 1596708504, '47.111.193.86', '13600774732', 1);
INSERT INTO `qmgx_userlogin` VALUES (256, 1596709379, '47.99.20.118', '15280892732', 1);
INSERT INTO `qmgx_userlogin` VALUES (257, 1596709628, '47.111.193.89', '15877004910', 1);
INSERT INTO `qmgx_userlogin` VALUES (258, 1596709889, '47.111.193.85', '13193741098', 1);
INSERT INTO `qmgx_userlogin` VALUES (259, 1596710219, '47.99.20.139', '13600774732', 1);
INSERT INTO `qmgx_userlogin` VALUES (260, 1596711660, '47.111.193.90', '13205882308', 1);
INSERT INTO `qmgx_userlogin` VALUES (261, 1596712388, '47.111.193.90', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (262, 1596713139, '47.99.20.123', '19959850596', 1);
INSERT INTO `qmgx_userlogin` VALUES (263, 1596713976, '58.101.147.228', '15888738658', 1);
INSERT INTO `qmgx_userlogin` VALUES (264, 1596717260, '47.99.20.122', '16625134519', 1);
INSERT INTO `qmgx_userlogin` VALUES (265, 1596717362, '47.111.193.58', '13770135227', 1);
INSERT INTO `qmgx_userlogin` VALUES (266, 1596717965, '47.111.193.87', '15259423587', 1);
INSERT INTO `qmgx_userlogin` VALUES (267, 1596718535, '47.99.20.132', '18575599846', 1);
INSERT INTO `qmgx_userlogin` VALUES (268, 1596719162, '47.99.20.119', '18642269577', 1);
INSERT INTO `qmgx_userlogin` VALUES (269, 1596720484, '47.111.193.59', '13751165165', 1);
INSERT INTO `qmgx_userlogin` VALUES (270, 1596722925, '47.99.20.123', '15113573121', 1);
INSERT INTO `qmgx_userlogin` VALUES (271, 1596724426, '47.111.193.93', '13210376667', 1);
INSERT INTO `qmgx_userlogin` VALUES (272, 1596724503, '47.111.193.92', '13636966250', 1);
INSERT INTO `qmgx_userlogin` VALUES (273, 1596724690, '47.111.193.86', '13037617108', 1);
INSERT INTO `qmgx_userlogin` VALUES (274, 1596724813, '47.111.193.92', '15263819407', 1);
INSERT INTO `qmgx_userlogin` VALUES (275, 1596724956, '47.111.193.86', '18703614501', 1);
INSERT INTO `qmgx_userlogin` VALUES (276, 1596725329, '47.111.193.92', '16530800938', 1);
INSERT INTO `qmgx_userlogin` VALUES (277, 1596725738, '117.136.76.66', '15077498429', 1);
INSERT INTO `qmgx_userlogin` VALUES (278, 1596725792, '47.111.193.92', '18866478504', 1);
INSERT INTO `qmgx_userlogin` VALUES (279, 1596726333, '47.111.193.92', '17771934420', 1);
INSERT INTO `qmgx_userlogin` VALUES (280, 1596726903, '47.111.193.92', '18866674206', 1);
INSERT INTO `qmgx_userlogin` VALUES (281, 1596727249, '47.111.193.67', '18703614501', 1);
INSERT INTO `qmgx_userlogin` VALUES (282, 1596727390, '47.111.193.92', '16530801186', 1);
INSERT INTO `qmgx_userlogin` VALUES (283, 1596727572, '117.136.76.66', '15077498429', 1);
INSERT INTO `qmgx_userlogin` VALUES (284, 1596727683, '47.111.193.87', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (285, 1596727883, '47.111.193.92', '18866670045', 1);
INSERT INTO `qmgx_userlogin` VALUES (286, 1596728390, '47.99.20.115', '15157863554', 1);
INSERT INTO `qmgx_userlogin` VALUES (287, 1596728391, '47.111.193.92', '16530800926', 1);
INSERT INTO `qmgx_userlogin` VALUES (288, 1596729003, '47.111.193.92', '16530801187', 1);
INSERT INTO `qmgx_userlogin` VALUES (289, 1596729524, '47.111.193.92', '18866674189', 1);
INSERT INTO `qmgx_userlogin` VALUES (290, 1596729884, '47.111.193.92', '16530800923', 1);
INSERT INTO `qmgx_userlogin` VALUES (291, 1596730295, '47.111.193.92', '16530800934', 1);
INSERT INTO `qmgx_userlogin` VALUES (292, 1596730713, '47.111.193.92', '16530800924', 1);
INSERT INTO `qmgx_userlogin` VALUES (293, 1596731369, '47.99.20.124', '18866478814', 1);
INSERT INTO `qmgx_userlogin` VALUES (294, 1596731768, '47.99.20.124', '15263819432', 1);
INSERT INTO `qmgx_userlogin` VALUES (295, 1596732158, '47.99.20.124', '18866478974', 1);
INSERT INTO `qmgx_userlogin` VALUES (296, 1596732973, '47.99.20.124', '18866674223', 1);
INSERT INTO `qmgx_userlogin` VALUES (297, 1596733386, '47.99.20.124', '18866674208', 1);
INSERT INTO `qmgx_userlogin` VALUES (298, 1596733732, '47.99.20.124', '16530800946', 1);
INSERT INTO `qmgx_userlogin` VALUES (299, 1596734102, '47.99.20.124', '16530800918', 1);
INSERT INTO `qmgx_userlogin` VALUES (300, 1596734851, '47.99.20.124', '18866478647', 1);
INSERT INTO `qmgx_userlogin` VALUES (301, 1596735016, '47.99.20.124', '13636966250', 1);
INSERT INTO `qmgx_userlogin` VALUES (302, 1596735342, '47.111.193.93', '13770135227', 1);
INSERT INTO `qmgx_userlogin` VALUES (303, 1596754218, '112.124.159.142', '15846963781', 1);
INSERT INTO `qmgx_userlogin` VALUES (304, 1596754738, '47.111.193.73', '13193741098', 1);
INSERT INTO `qmgx_userlogin` VALUES (305, 1596757468, '47.99.20.120', '18575599846', 1);
INSERT INTO `qmgx_userlogin` VALUES (306, 1596760649, '47.99.20.120', '18808108991', 1);
INSERT INTO `qmgx_userlogin` VALUES (307, 1596760783, '47.99.20.129', '15523518952', 1);
INSERT INTO `qmgx_userlogin` VALUES (308, 1596761287, '47.99.20.120', '17875179993', 1);
INSERT INTO `qmgx_userlogin` VALUES (309, 1596761694, '47.99.20.140', '13059385678', 1);
INSERT INTO `qmgx_userlogin` VALUES (310, 1596761748, '47.111.193.78', '18027195555', 1);
INSERT INTO `qmgx_userlogin` VALUES (311, 1596761778, '47.111.193.78', '13533576777', 1);
INSERT INTO `qmgx_userlogin` VALUES (312, 1596762334, '47.111.193.95', '15259423587', 1);
INSERT INTO `qmgx_userlogin` VALUES (313, 1596763020, '47.111.193.92', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (314, 1596763137, '47.111.193.58', '13193741098', 1);
INSERT INTO `qmgx_userlogin` VALUES (315, 1596763174, '47.99.20.130', '18642269577', 1);
INSERT INTO `qmgx_userlogin` VALUES (316, 1596764249, '112.124.159.144', '18128134200', 1);
INSERT INTO `qmgx_userlogin` VALUES (317, 1596764272, '47.111.193.63', '15083651085', 1);
INSERT INTO `qmgx_userlogin` VALUES (318, 1596764355, '47.111.193.91', '13591806765', 1);
INSERT INTO `qmgx_userlogin` VALUES (319, 1596764636, '47.99.20.129', '15501598007', 1);
INSERT INTO `qmgx_userlogin` VALUES (320, 1596764670, '47.99.20.130', '13553398421', 1);
INSERT INTO `qmgx_userlogin` VALUES (321, 1596764699, '47.111.193.57', '18865266521', 1);
INSERT INTO `qmgx_userlogin` VALUES (322, 1596764716, '47.111.193.99', '13392298144', 1);
INSERT INTO `qmgx_userlogin` VALUES (323, 1596764791, '47.111.193.57', '15873486769', 1);
INSERT INTO `qmgx_userlogin` VALUES (324, 1596764880, '47.99.20.130', '13553398421', 1);
INSERT INTO `qmgx_userlogin` VALUES (325, 1596765441, '47.111.193.83', '13193741098', 1);
INSERT INTO `qmgx_userlogin` VALUES (326, 1596766065, '47.111.193.66', '18094444666', 1);
INSERT INTO `qmgx_userlogin` VALUES (327, 1596766594, '47.99.20.138', '15501598007', 1);
INSERT INTO `qmgx_userlogin` VALUES (328, 1596767239, '47.99.20.124', '18773407584', 1);
INSERT INTO `qmgx_userlogin` VALUES (329, 1596767445, '47.99.20.125', '18703614501', 1);
INSERT INTO `qmgx_userlogin` VALUES (330, 1596767898, '47.111.193.59', '15261185382', 1);
INSERT INTO `qmgx_userlogin` VALUES (331, 1596768171, '47.111.193.59', '15312555382', 1);
INSERT INTO `qmgx_userlogin` VALUES (332, 1596768844, '47.99.20.130', '17663569502', 1);
INSERT INTO `qmgx_userlogin` VALUES (333, 1599141505, '127.0.0.1', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (334, 1599142206, '127.0.0.1', '18888888888', 1);
INSERT INTO `qmgx_userlogin` VALUES (335, 1599142958, '127.0.0.1', '15554832010', 1);
INSERT INTO `qmgx_userlogin` VALUES (336, 1599143486, '127.0.0.1', '15554832010', 1);
INSERT INTO `qmgx_userlogin` VALUES (337, 1599314634, '127.0.0.1', '15554832010', 1);
INSERT INTO `qmgx_userlogin` VALUES (338, 1599339067, '127.0.0.1', '15554832010', 1);

-- ----------------------------
-- Table structure for qmgx_wages
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_wages`;
CREATE TABLE `qmgx_wages`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `isadd` tinyint(5) NOT NULL DEFAULT 1 COMMENT '1加2减',
  `nums` decimal(10, 2) NOT NULL COMMENT '激活币数量',
  `fromuid` int(11) NULL DEFAULT NULL COMMENT '来源id，0表示系统',
  `ctime` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `info` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ATC记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qmgx_work_sheet
-- ----------------------------
DROP TABLE IF EXISTS `qmgx_work_sheet`;
CREATE TABLE `qmgx_work_sheet`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `link_info` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系信息',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工单标题',
  `type` int(1) NOT NULL COMMENT '工单类型',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片信息',
  `desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '简单描述',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '处理状态 1 待处理 2 已处理',
  `solve_time` int(11) NULL DEFAULT NULL COMMENT '处理时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
