<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2017/1/11
 * Time: 上午1:30
 */

return [

    // 异常页面的模板文件
    //'exception_tmpl'        => __DIR__ . "/tpl/exception.tpl",


    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl' => __DIR__ . '/tpl/dispatch_jump.tpl',
    'dispatch_error_tmpl' => __DIR__ . '/tpl/dispatch_jump.tpl',

    'extra_file_list' => [__DIR__ . "/extra/authority.php", __DIR__ . "/extra/menu.php"],

    'template' => [
        // 模板路径
        'view_path' => __DIR__ . '/tpl/',
        //开启模板布局
        'layout_on' => true,
        //设置模板布局位置
        'layout_name' => 'public/layout',
    ],

    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------


    //通知类型
    'notice_type' => [
        '1' => [
            'type' => 1,
            'type_name' => '全平台通知',
            'uid_need' => false,
            'color' => '#D10D15',
        ],
        '2' => [
            'type' => 2,
            'type_name' => '个人通知',
            'uid_need' => true,
            'color' => '#E80A89',
        ],
    ],
    //跳转类型
    'link_type' => [
        '2' => [
            'type' => 2,
            'color' => '#E80A89',
            'type_name' => '跳转分类商品筛选',
            'placeholder' => '请输入分类ID，可以在分类列表中获得，留空跳转全部商品',
            'link_need' => '2',//1必须写 2选填
        ],
        '4' => [
            'type' => 4,
            'color' => '#29AFF0',
            'type_name' => '跳转商品详情',
            'placeholder' => '请输入商品ID，可以在商品列表中获得',
            'link_need' => '1',//1必须写 2选填
        ],
        '5' => [
            'type' => 5,
            'color' => '#397B19',
            'type_name' => '跳转外链',
            'placeholder' => '请输入外链地址，需以http://或https://开头',
            'link_need' => '1',//1必须写 2选填
        ],
    ],
    //抽奖商品类型
    'lottery_type' => [
        '1' => [
            'type' => 1,
            'color' => '#D10D15',
            'type_name' => '实物商品'
        ],
        '2' => [
            'type' => 2,
            'color' => '#E80A89',
            'type_name' => '共享豆'
        ],
        '3' => [
            'type' => 3,
            'color' => '#7405F0',
            'type_name' => '商城币'
        ],
        '4' => [
            'type' => 4,
            'color' => '#7405F0',
            'type_name' => '宠物'
        ]
    ],
];