<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2017/2/4
 * Time: 下午6:01
 */



//后台管理是否发生异常
$admin_has_exception = false;
/**
 * 自定义异常处理函数
 *
 * 用于数据库事务回滚处理
 *
 * @param $exception
 */
function admin_exception_handler($exception)
{
    global $admin_has_exception;

    $admin_has_exception = true;

    \think\Error::appException($exception);
}



/**
 * 判断是否具有订单相关操作权限
 *
 * @return bool
 */
function has_order_auth()
{
    // 当前管理员
    $admin = session("H_ADMIN_LOGIN_USER");
    if ($admin['is_root'] || in_array('Order.order_tip', $admin['authority']))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/**
 * 判断是否具有提现相关操作权限
 *
 * @return bool
 */
function has_cash_auth()
{
    // 当前管理员
    $admin = session("H_ADMIN_LOGIN_USER");

    if ($admin['is_root'] || in_array('Finance.*', $admin['authority']) ||
        in_array('Finance.withdraw', $admin['authority']))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


function get_user_mobile($id){
    $name = db('user')->where('id',$id)->value('mobile');
    return $name==''?'未查询到':$name;
}

function get_business_title($id){
    $name = db('business')->where('id',$id)->value('title');
    return $name==''?'未查询到':$name;
}

function get_business_account($id){
    $name = db('business')->where('id',$id)->value('username');
    return $name==''?'未查询到':$name;
}

function get_visible_title($title,$is_visible){
    if(empty($title)){
        return '无';
    }else{
        return $title .= $is_visible==1?'':'<font color="red">（已隐藏）</font>';
    }
}

function get_app_shop($type,$order='display_order DESC,id DESC'){
    if(is_array($type)){
        $type = implode(',',$type);
        $where['type'] = ['in',$type];
    }else{
        $where['type'] = $type;
    }
    $list = db('app_shop')->where($where)->order($order)->column('id,title,is_visible');
    return $list;
}

function is_personal($id){
    $notice = db('notice')->where('id',$id)->find();
    if(!$notice){
        return ['status'=>1,'info'=>'公告ID有误，对应公告不存在'];
    }
    return ['status'=>0];
}

function get_dispatch_type(){
    return config('dispatch_type');
}

function get_order_tip_switch(){
    $rate = db('rate')->where('id',1)->value('order_tip_switch');
    return $rate;
}


function get_lottery_type($id){
    $arr = config('lottery_type');
    $str = @$arr[$id]['type_name'];
    if($str){
        return '<label class="label label-danger" style="background: '.$arr[$id]['color'].'">'.$str.'</label>';
    }else{
        return '';
    }
}