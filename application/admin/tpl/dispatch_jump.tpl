{__NOLAYOUT__}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>跳转提示</title>
    <link href="__PUBLIC__/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        .success
        {
            color: rgb(106, 189, 120);
        }
        .error
        {
            color: rgb(200, 22, 29);
        }
        .title
        {
            text-align: center;
            font-size: 24px;
            padding: 20px 0;
        }
        .detail
        {
            text-align: center;
            font-size: 18px;
            padding: 10px 0;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 title {if condition="$code"}success{else /}error{/if}">
                {:strip_tags($msg)}
            </div>

            <div class="col-xs-12 detail">
                如果您的浏览器不能自动跳转，请点击“跳转地址”跳转页面
                <a id="href" href="{$url}" style="padding: 0 10px;">
                    跳转地址
                </a>
                等待时间
                <b id="wait">{$wait}</b>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12" style="text-align: center;">
                <img src="{if condition="$code"}__PUBLIC__/images/success.png{else /}__PUBLIC__/images/error.png{/if}" alt="" style="max-height: 400px;"/>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        (function(){
            var wait = document.getElementById('wait'),
                href = document.getElementById('href').href;
            var interval = setInterval(function(){
                var time = --wait.innerHTML;
                if(time <= 0) {
                    location.href = href;
                    clearInterval(interval);
                };
            }, 1000);
        })();
    </script>
</body>
</html>
