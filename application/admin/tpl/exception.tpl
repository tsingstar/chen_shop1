<!DOCTYPE html>
<html lang="en">
<head>
    <link href="/static/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="text-align: center;">
                <h1>Sorry..页面没有找到！</h1>
                <p>
                    似乎你所寻找的网页已移动或丢失了。
                <p>或者也许你只是键入错误了一些东西。</p>
                请不要担心，这没事。如果该资源对你很重要，请与管理员联系。
                </p>

                <p>
                    火星不太安全，我可以免费送你回地球
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12" style="text-align: center;">
                <img src="/static/images/elephant.png">
            </div>
        </div>
    </div>
</body>
</html>