<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2017/1/18
 * Time: 上午9:01
 */

use think\Cache;
use think\Session;
use app\common\model\DeveloperException;

/**
 * 生成菜单
 */
function generate_menu()
{
    $admin = session("H_ADMIN_LOGIN_USER");
    //获取缓存
    if (Cache::has("admin_menu".$admin['id']))
    {
        echo Cache::get("admin_menu".$admin['id']);
    }
    else
    {
        //读取菜单配置
        $config = file_get_contents(__DIR__ . '/' . "menu.json");

        //菜单列表
        $menuList = json_decode($config, true);

        //html 内容
        $html = "";

        //生成html 内容
        foreach ($menuList as $section)
        {
            $html .= "<div class=\"menu_section\">";
            $html .= "<ul class=\"nav side-menu\">";

            foreach ($section as $menu)
            {
                $html .= generate_menu_item($menu);
            }

            $html .= "</ul></div>";
        }

        //存储为缓存数据
        Cache::set("admin_menu", $html);

        echo $html;
    }
}

//生成子级菜单
function generate_sub_menu($menuList)
{
    //html 内容
    $html = "";

    //检测是否存在子级菜单
    if (empty($menuList))
    {
        return $html;
    }

    $html .= "<ul class=\"nav child_menu\">";

    foreach ($menuList as $menu)
    {
        $html .= generate_menu_item($menu);
    }

    $html .= "</ul>";

    return $html;
}

/**
 * 生成菜单项
 * @param $menu
 * @return string
 * @throws DeveloperException
 */
function generate_menu_item($menu)
{
    //html 内容
    $html = "";

    //当前管理员
    $admin = session("H_ADMIN_LOGIN_USER");

    //是否为超级管理员
    if (!$admin["is_root"])
    {
        //判断管理员权限
        if (!in_array($menu["authority"], $admin["authority"]) && !empty($menu["authority"]))
        {
            return $html;
        }
    }

    //判断菜单是否显示
    if (array_key_exists("visible", $menu) && $menu["visible"] == false)
    {
        return $html;
    }

    $html .= "<li>";

    //超链
    if ($menu["url"])
    {
        $url = $menu["url"];

        if (is_string($url))
        {
            $html .= "<a href=\"" . url($url) . "\">";
        }
        else
        {
            if (array_key_exists("base", $url) && array_key_exists("params", $url) && is_array($url["params"]))
            {
                $html .= "<a href=\"" . url($url["base"], $url["params"]) . "\">";
            }
            else
            {
                throw new DeveloperException(translate("generate_menu_item:url_error"));
            }
        }
    }
    else
    {
        $html .= "<a>";
    }

    //图标
    if ($menu["icon"])
    {
        $html .= "<i class=\"" . $menu["icon"] . "\"></i>";
    }

    //标题
    $html .= $menu["title"];

    $hasChildren = array_key_exists("children", $menu);

    if ($hasChildren)
    {
        $html .= "<span class=\"menu_chevron fa fa-chevron-left\"></span>";
    }

    $html .= "</a>";

    //子级菜单
    if ($hasChildren)
    {
        $html .= generate_sub_menu($menu["children"]);
    }


    $html .= "</li>";

    return $html;
}