<?php
/**
 * 系统定时任务
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/8/9
 * Time: 14:03
 */

namespace app\admin\controller;


use app\common\model\Jhsms;
use think\Cache;
use think\Controller;
use think\Exception;

class Interval extends Controller
{
    protected function _initialize()
    {
        parent::_initialize();
    }


    /**
     * 重置交易曲线
     */
    public function setTradeLine()
    {
        $line = db('trade_timeline')->where('day_time', date('Y-m-d'))->find();
        if(empty($line)){
            //查询最新一条的价格
            $data = db('trade_timeline')->order('day_time desc')->find();
            unset($data['id']);
            $data['day_time'] = date('Y-m-d');
            db('trade_timeline')->insert($data);
        }else{
            //设置过，更新状态显示
            db('trade_timeline')->where('id', $line['id'])->update([
                'status'=>2,
                'update_time'=>now_datetime()
            ]);
        }
    }
    

    /**
     * 取消配对
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function tradeCancel()
    {
        $order_list = \db('trade_order')->whereIn('status', [2, 3])->where('match_time', '<', time() - config("setting.pay_limit_time") * 60)->select();
        foreach ($order_list as $order) {
            $res = db('trade_order')
                ->where('id', $order['id'])
                ->update([
                    'sell_user_id' => 0,
                    'status' => 1,
                    'match_time' => 0,
                    'pay_time' => 0,
                    'sure_time' => 0,
                    'pay_type' => 0,
                    'pay_img' => 0,
                ]);
        }
    }

    /**
     * 交易中心订单确认完成
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function tradeOk()
    {
        $order_list = \db('trade_order')->where('status', 3)->where('pay_time', '<',time() - config('setting.sure_limit_time') * 60)->select();
        foreach ($order_list as $order) {
            try {
                \db()->startTrans();;
                $update_data = [
                    'status' => 4,
                    'sure_time' => now_datetime()
                ];
                $res = \db('trade_order')->where('id', $order['id'])->update($update_data);
                if (empty($res)) {
                    continue;
                } else {
                    $insert_data = [
                        "uid" => $order['user_id'],
                        "isadd" => 1,
                        "nums" => $order['num'],
                        'fromuid' => 0,
                        "ctime" => now_datetime(),
                        "info" => "求购SCB获得"
                    ];
                    \db('mybot')->insert($insert_data);
                    \db('user')->where('id', $order['user_id'])->setInc('mybot', $order['num']);
                    \db()->commit();
                }
            } catch (\Exception $e) {
                \db()->rollback();
            }
        }

    }


    /**
     * 到时间发放匹配订单情况
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function sure_order()
    {
        if (Cache::store("redis")->has("sure_order_status")) {
            echo "进行中。。。";
            exit();
        } else {
            Cache::store("redis")->set("sure_order_status", 1);
            header("Content-type:text/html;charset=utf-8");
            ignore_user_abort(true);
            set_time_limit(0);
            ini_set('memory_limit', '256M');
            date_default_timezone_set('Asia/Shanghai');
            //查询当前时间下符合需要处理的抢购订单
            $list = db("apply")->where("returntime", "<=", time())->where("status", 0)->order("point_status desc, rank")->select();
            $start_time = time();
            if (count($list) > 0) {


                /*把最下面的匹配订单复制一份，拿到最上面，循环一下，找到已经指定的订单并配对成功，然后从$list中删除这条数据---start*/
                foreach ($list as $delkey => $item) {
                    $userinfo = userallinfo($item['uid']);
                    //5  3501 5500
                    $product_con = db("goods")->where("id", $item['proid'])->find();
                    /*首先看一下这个时间段有没有订单，没有的话就不要让买了*/
                    $price_start = $product_con['price1'];
                    $price_end = $product_con['price2'];
                    //查一下可配对的单子有多少，但是不能包含自己买的单子
                    $order_have_map = "applyid='" . $item['id'] . "' and basicprice>='" . $price_start . "' and basicprice<='" . $price_end . "' and peiduistatus=1 and buyuid<>'" . $userinfo['id'] . "'";
                    $order_have_map = $order_have_map . " and cansaletime<='" . time() . "' ";
                    $order_have = db(config('db_order_table'))->where($order_have_map)->order('id asc')->value('id');
                    if (!empty($order_have)) {
                        /*如果有符合条件的订单并且客户购买，判断一下用户是否预约，把预约的扣除酒滴确定下来*/
                        //确定酒滴扣除，不返还
                        $newdata['status'] = 1;
                        db('apply')->where("id='" . $item['id'] . "'")->update($newdata);//先把原来预约取消
                        //已预约扣除多少酒滴
                        $data_sure['caifen'] = $product_con['point_start'];
                        //下单并绑定uid,然后获取这条订单的所有信息，价格交给下个新订单，并生成收益列表
                        $bind_order['peiduiuid'] = $userinfo['id'];
                        $bind_order['peiduistatus'] = 2;
                        $bind_order['peiduitime'] = time();
                        $bind_order["match_pro_id"] = $product_con["id"];
                        $bind_order["match_pro_title"] = $product_con["title"];
                        //获取订单所有信息
                        $that_order_one = db(config('db_order_table'))->where("id='" . $order_have . "'")->find();
                        $data_sure['price'] = $that_order_one['basicprice'];
                        $data_sure['peiduiid'] = $that_order_one['id'];
                        if (true) {
                            //配对完成给双方发短信,暂未处理是否开启
                            //买家收短信 $userinfo['selfphone'];
                            $jh_sms = new Jhsms();
                            $jh_sms->send_order_tips($userinfo['selfphone']);
                            usleep(30000);
                            $jh_sms->send_order_tips($userinfo['mustman']);
                            //卖家收短信
                            $phone_buy = db("user")->where("id", $that_order_one['buyuid'])->value("selfphone");
                            $jh_sms->send_order_tips($phone_buy);
                        }
                        // 然后再新增一个order数据
                        $data_sure['buyuid'] = $userinfo['id'];
                        $data_sure['productid'] = $product_con['id'];
                        $data_sure['title'] = $product_con['title'];
                        $data_sure['daynum'] = $product_con['daynum'];
                        $data_sure['earnpercent'] = $product_con['daypercent'];
                        //一共能赚多少钱，本金加赚的钱有多少
                        $countprofit = countprofit($data_sure['price'], $data_sure['daynum'], $data_sure['earnpercent']);
                        $data_sure['buyuidatc'] = $product_con['atcnum'];
                        $data_sure['dayaddprice'] = $countprofit['profitall'];
                        $data_sure['basicprice'] = $countprofit['profit_and_day'];
                        $data_sure['ctime'] = time();
                        $data_sure['peiduistatus'] = 1;
                        $data_sure['peiduiuid'] = 0;
                        $data_sure['paystatus'] = 0;
                        $data_sure['paytime'] = 0;
                        $data_sure['payimg'] = '';
                        $cansaletime = ($data_sure['ctime'] + 86400 * $product_con['daynum']);
                        $cansaletimedate = date('Y-m-d', $cansaletime);
                        $data_sure['cansaletime'] = strtotime($cansaletimedate);
                        $now_day_dates = date('Y-m-d');
                        $data_sure['thatdatetime'] = strtotime($now_day_dates);
                        //        $data_sure['ordersn'] = coid($data_sure['buyuid']);
                        //先新增order表，再新增利润表
                        $order_new_id = db(config('db_order_table'))->insertGetId($data_sure);
                        $bind_order["match_oid"] = $order_new_id;
                        db(config('db_order_table'))->where("id='" . $order_have . "'")->update($bind_order);
                        $insert_detail = [];
                        for ($i = 1; $i <= $product_con['daynum']; $i++) {
                            $data_detail['orderid'] = $order_new_id;
                            $data_detail['uid'] = $userinfo['id'];
                            $data_detail['earnmoney'] = $countprofit['profitday'];
                            $detail_time = ($data_sure['thatdatetime'] + 86400 * $i);
                            $data_detail['dotime'] = date('Ymd', $detail_time);
                            $data_detail['status'] = 1;
                            $data_detail['ctime'] = time();
                            $insert_detail[] = $data_detail;
                        }
                        if (!empty($insert_detail)) {
                            db('orderearn')->insertAll($insert_detail);
                        }
                        unset($list[$delkey]);
                    }

                }
                /*把最下面的匹配订单复制一份，拿到最上面，循环一下，找到已经指定的订单并配对成功，然后从$list中删除这条数据---end*/


                foreach ($list as $item) {
                    if (time() - $start_time > 55) {
                        Cache::store('redis')->rm('sure_order_status');
                        exit();
                    }
                    $userinfo = userallinfo($item['uid']);
                    $product_con = db("goods")->where("id", $item['proid'])->find();
                    //查一下预约是否有，如果买单失败就把酒滴退回去
                    /*首先看一下这个时间段有没有订单，没有的话就不要让买了*/
                    $price_start = $product_con['price1'];
                    $price_end = $product_con['price2'];
                    //查一下可配对的单子有多少，但是不能包含自己买的单子
                    $order_have_map = "basicprice>='" . $price_start . "' and basicprice<='" . $price_end . "' and peiduistatus=1 and buyuid<>'" . $userinfo['id'] . "'";
                    $order_have_map = $order_have_map . " and cansaletime<='" . time() . "' ";
                    $order_have = db(config('db_order_table'))->where($order_have_map)->order('id asc')->value('id');
                    //没有匹配的订单，直接返回给客户，告诉客户额度没了
                    if (empty($order_have)) {
                        /*先把预约的酒滴退回去，再把这个预约状态改成退还*/
                        /*新增预约失败，并退还*/
                        $newdata = $item;
                        $newdata['status'] = 2;
                        db('apply')->where("id='" . $item['id'] . "'")->update($newdata);//先把原来预约取消
                        $newdata['title'] = $item['title'] . '-抢购失败退还';
                        unset($newdata['id']);
                        $newdata['isadd'] = 1;
                        $newdata['ctime'] = time();
                        db('apply')->insert($newdata);//再把预约失败还回去
                        //先把会员的油卡加回去
                        db('user')->where("id='" . $userinfo['id'] . "'")->setInc('integral', $item['delpoint']);
                        //再把会员的酒滴历史加上
                        points_his($userinfo['id'], 1, $item['delpoint'], 0, $item['title'] . '-抢购失败退还');
                    } else {
                        /*如果有符合条件的订单并且客户购买，判断一下用户是否预约，把预约的扣除酒滴确定下来*/
                        //确定酒滴扣除，不返还
                        $newdata['status'] = 1;
                        db('apply')->where("id='" . $item['id'] . "'")->update($newdata);//先把原来预约取消
                        //已预约扣除多少酒滴
                        $data_sure['caifen'] = $product_con['point_start'];
                        //下单并绑定uid,然后获取这条订单的所有信息，价格交给下个新订单，并生成收益列表
                        $bind_order['peiduiuid'] = $userinfo['id'];
                        $bind_order['peiduistatus'] = 2;
                        $bind_order['peiduitime'] = time();
                        $bind_order["match_pro_id"] = $product_con["id"];
                        $bind_order["match_pro_title"] = $product_con["title"];
                        //获取订单所有信息
                        $that_order_one = db(config('db_order_table'))->where("id='" . $order_have . "'")->find();
                        $data_sure['price'] = $that_order_one['basicprice'];
                        $data_sure['peiduiid'] = $that_order_one['id'];
                        if (true) {
                            //配对完成给双方发短信,暂未处理是否开启
                            //买家收短信 $userinfo['selfphone'];
                            $jh_sms = new Jhsms();
                            $jh_sms->send_order_tips($userinfo['selfphone']);
                            usleep(300000);
                            $jh_sms->send_order_tips($userinfo['mustman']);
                            //卖家收短信
                            $phone_buy = db("user")->where("id", $that_order_one['buyuid'])->value("selfphone");
                            $jh_sms->send_order_tips($phone_buy);
                        }
                        // 然后再新增一个order数据
                        $data_sure['buyuid'] = $userinfo['id'];
                        $data_sure['productid'] = $product_con['id'];
                        $data_sure['title'] = $product_con['title'];
                        $data_sure['daynum'] = $product_con['daynum'];
                        $data_sure['earnpercent'] = $product_con['daypercent'];
                        //一共能赚多少钱，本金加赚的钱有多少
                        $countprofit = countprofit($data_sure['price'], $data_sure['daynum'], $data_sure['earnpercent']);
                        $data_sure['buyuidatc'] = $product_con['atcnum'];
                        $data_sure['dayaddprice'] = $countprofit['profitall'];
                        $data_sure['basicprice'] = $countprofit['profit_and_day'];
                        $data_sure['ctime'] = time();
                        $data_sure['peiduistatus'] = 1;
                        $data_sure['peiduiuid'] = 0;
                        $data_sure['paystatus'] = 0;
                        $data_sure['paytime'] = 0;
                        $data_sure['payimg'] = '';
                        $cansaletime = ($data_sure['ctime'] + 86400 * $product_con['daynum']);
                        $cansaletimedate = date('Y-m-d', $cansaletime);
                        $data_sure['cansaletime'] = strtotime($cansaletimedate);
                        $now_day_dates = date('Y-m-d');
                        $data_sure['thatdatetime'] = strtotime($now_day_dates);
//        $data_sure['ordersn'] = coid($data_sure['buyuid']);
                        //先新增order表，再新增利润表
                        $order_new_id = db(config('db_order_table'))->insertGetId($data_sure);
                        $bind_order["match_oid"] = $order_new_id;
                        db(config('db_order_table'))->where("id='" . $order_have . "'")->update($bind_order);
                        $insert_detail = [];
                        for ($i = 1; $i <= $product_con['daynum']; $i++) {
                            $data_detail['orderid'] = $order_new_id;
                            $data_detail['uid'] = $userinfo['id'];
                            $data_detail['earnmoney'] = $countprofit['profitday'];
                            $detail_time = ($data_sure['thatdatetime'] + 86400 * $i);
                            $data_detail['dotime'] = date('Ymd', $detail_time);
                            $data_detail['status'] = 1;
                            $data_detail['ctime'] = time();
                            $insert_detail[] = $data_detail;
                        }
                        if (!empty($insert_detail)) {
                            db('orderearn')->insertAll($insert_detail);
                        }
                    }
                }
            }
            echo("处理完成");
            Cache::store("redis")->rm("sure_order_status");
            exit;
        }
    }


    /*生肖鼠预约共享豆返回，5分钟执行一次即可*/
    public function runapply()
    {
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');
        $nowtime = time();
        $list = db('apply')->where('status=0 and nowdate<="' . date('Ymd') . '"' . ' and returntime<' . $nowtime)->order('id desc')->limit(50)->select();
        if (empty($list)) {
            echo 1;
            exit;
        }
        foreach ($list as $k => $v) {
            if ($nowtime > $v['returntime']) {
                $newdata = '';
                //到时间了先把预约废了再加一个新作废预约
                $newdata = $v;
                $newdata['status'] = 2;
                db('apply')->where("id='" . $v['id'] . "'")->update($newdata);//先把原来预约取消

                $newdata['title'] = $v['title'] . '-共享豆退还';
                unset($newdata['id']);
                $newdata['isadd'] = 1;
                $newdata['ctime'] = time();
                db('apply')->insert($newdata);//再把预约失败还回去
                //先把会员的共享豆加回去
                db('user')->where("id='" . $v['uid'] . "'")->setInc('integral', $v['delpoint']);
                //再把会员的共享豆历史加上
                points_his($v['uid'], 1, $v['delpoint'], 0, $v['title'] . '-预约失败退还');
            }
        }
        echo 1;
        exit;
    }

    /*每天每七分钟执行一次好了，就这样*/
    public function doprofit()
    {
        /*本来想一条sql语句搞定的，考虑到以后还有可能添加到用户的user表，还是一条条执行吧*/
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');

        $list = db('orderearn')->where("dotime<='" . date('Ymd') . "' and `status`=1")->limit(50)->select();
        /*如果空数据就不操作*/
        if (empty($list)) {
            echo 1;
            exit;
        }
        foreach ($list as $k => $v) {
            $new_data['status'] = 2;
            $new_data['sendtime'] = time();

            /*ATC发放完成后，再发放各方的奖金，先发放三级分销的*/
            $price_basic = $v['earnmoney'];

            //个人收益添加到个人的用户表
            db('User')->where("id='" . $v['uid'] . "'")->setInc('myprofit', $v['earnmoney']);
            //读取当前人的信息
            $thisuser = userallinfo($v['uid']);
            db('orderearn')->where("id='" . $v['id'] . "'")->update($new_data);
            /*开始发放一二三三级的奖金*/
            //first_user   霸道的幸福37，爱的港湾117，初遇215
            if (!empty($thisuser['pid'])) {
                $parent_first = userallinfo($thisuser['pid']);
                $send_earn = ($price_basic * config('setting.parent_first') / 100);
                $send_earn = round($send_earn, 2);
                if ($send_earn > 0) {
                    //发放奖金
                    db('User')->where("id='" . $thisuser['pid'] . "'")->setInc('pushs', $send_earn);
                    pushs_his($thisuser['pid'], 1, $send_earn, 0, '一级下级' . $thisuser['username'] . '订单成交');
                }
            }
            //secode_user，是否有二级上级，有就发放奖金
            if (!empty($parent_first['pid'])) {
                $parent_second = userallinfo($parent_first['pid']);
                $send_earn = ($price_basic * config('setting.parent_second') / 100);
                $send_earn = round($send_earn, 2);
                if ($send_earn > 0) {
                    //发放奖金
                    db('User')->where("id='" . $parent_first['pid'] . "'")->setInc('pushs', $send_earn);
                    pushs_his($parent_first['pid'], 1, $send_earn, 0, '二级下级' . $thisuser['username'] . '订单成交');
                }
            }
            //third_user，是否有三级上级，有就发放奖金
            if (!empty($parent_second['pid'])) {
                $send_earn = ($price_basic * config('setting.parent_third') / 100);
                $send_earn = round($send_earn, 2);
                if ($send_earn > 0) {
                    //发放奖金
                    db('User')->where("id='" . $parent_second['pid'] . "'")->setInc('pushs', $send_earn);
                    pushs_his($parent_second['pid'], 1, $send_earn, 0, '三级下级' . $thisuser['username'] . '订单成交');
                }
            }
        }
        echo 1;
        exit;
    }

    /**
     * 过期订单收益 (按日 补发一条收益记录)   每日执行一次
     */
    public function orderProfit()
    {
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');
        exit;//不在给补发收益了。
        //查询过期未匹配订单
        $list = db(config('db_order_table'))->where("peiduistatus=1 and cansaletime<'" . time() . "'")->order("id desc")->select();
        if (!empty($list)) {
            foreach ($list as &$v) {
                $earn = round(($v['dayaddprice'] / $v['daynum']), 2);
                $data['earnmoney'] = $earn;
                $data['uid'] = $v['buyuid'];
                $data['orderid'] = $v['id'];
                $data['isadd'] = 1;
                $data['dotime'] = date('Ymd');
                $data['status'] = 1;
                $data['ctime'] = time();
                $data['info'] = "过期无匹配订单补发收益";
                //判断当天是否补发过收益记录   否  补发
                $find_exist = db('orderearn')->where('dotime', date('Ymd'))->where('orderid', $v['id'])->count();
                //不存在 插入一条记录
                if (!$find_exist) {
                    db(config('db_order_table'))->where("id", $v["id"])->setInc("basicprice", $earn);
                    db('orderearn')->insert($data);
                } else {
                    //跳过
                    continue;
                }
            }
        }
        echo 1;
        exit;
    }

    /**
     * 支付时间过期，撤销配对收益记录，清空配对信息  没分钟执行一次
     *
     */
    public function clearPayOrder()
    {
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');
        //查询过期未匹配订单
        $list = db(config('db_order_table'))->where("peiduistatus", 2)->where("peiduitime", "<", time() - config("setting.pay_limit_time") * 60)->where("paystatus", 0)->order("id desc")->select();
        if (!empty($list)) {
            foreach ($list as $v) {
                //获取卖出订单id
                $sale_id = $v["match_oid"];
                //清除收益记录
                db("orderearn")->where("orderid", $sale_id)->delete();
                //重置订单配对状态
                db(config("db_order_table"))->where("id", $v["id"])->update([
                    "peiduistatus" => 1,
                    "peiduiuid" => 0,
                    "peiduitime" => "",
                    "match_pro_id" => 0,
                    "match_pro_title" => 0,
                    "match_oid" => 0,
                    "paystatus" => 0,
                    "paytype" => "",
                    "paytime" => 0,
                    "payimg" => ""
                ]);
                $cancel_order = db(config("db_order_table"))->where("id", $sale_id)->find();
                if (!$cancel_order) {
                    unset($cancel_order['id']);
                    db("order_cancel")->insert($cancel_order);
                }
                db(config("db_order_table"))->where("id", $sale_id)->delete();
            }
        }
        echo 1;
        exit;
    }


    /*超过30分钟没有自动收款的，系统给自动收款*/
    public function autoaccept()
    {
        /*本来想一条sql语句搞定的，考虑到以后还有可能添加到用户的user表，还是一条条执行吧*/
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');
        $timstamp = (time() - config('setting.sure_limit_time') * 60);
        $list = db(config('db_order_table'))->where("peiduistatus=2 and paystatus=1 and paytime<'" . $timstamp . "'")->order("id desc")->select();
        if (!empty($list)) {
            foreach ($list as $v) {
                //执行定时收款确认即可
                sureget($v['id']);
            }
        }
        echo 1;
        exit;
    }

    //xuanfeng 2020-08-03 21:41 每日发放SCB
    public function sendbot()
    {
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');

        $list = db(config('db_order_table'))->where("issendbot=1")->limit(20)->select();
        if (!empty($list)) {
            //开始生成每日的日期
            $datestr = date('Y-m-d');
            //得到开始时间
            $firstday = (strtotime($datestr) + 86400);
            //得到产品表的id daynum botnum
            $good_list = db('goods')->field('id,daynum,botnum')->select();
            $good_newlist = [];
            foreach ($good_list as $k => $v) {
                $good_newlist[$v['id']] = $v;
            }
            // echo '<pre>';print_R($good_newlist);exit;
            //给每一个sendbot生成一条记录
            foreach ($list as $vo) {
                $data = [];
                $good_id = $vo['productid'];
                $good_info = $good_newlist[$good_id];
                //第一天时间戳：
                $that_time = $firstday;
                for ($i = 1; $i <= $good_info['daynum']; $i++) {
                    $data['orderid'] = $vo['id'];
                    $data['uid'] = $vo['buyuid'];
                    $data['ctime'] = $that_time;
                    $data['state'] = 1;//1未计算2已计算
                    $data['proid'] = $good_id;
                    $data['botnum'] = $good_info['botnum'];
                    db('orderbot')->insert($data);
                    $that_time = ($that_time + 86400);
                    usleep(20000);
                    //0.02秒执行一次，防止太快服务器卡死
                }
                //修改状态为已完成
                db(config('db_order_table'))->where("id='" . $vo['id'] . "'")->update(['issendbot' => 2]);
            }
        }
    }


    //xuanfeng 2020-08-03 21:41 每日发放SCB
    public function senddaybot()
    {
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');

        //开始生成每日的日期
        $datestr = date('Y-m-d');
        //得到开始时间
        $firstday = strtotime($datestr);

        $list = db('orderbot')->where("`ctime`='" . $firstday . "' and state=1")->limit(50)->select();
        if (!empty($list)) {
            foreach ($list as $v) {
                //先给用户mybot列的余额加上相应数量
                db('user')->where("id='" . intval($v['uid']) . "'")->setInc('mybot', $v['botnum']);
                //再给相应用户加上mybot历史
                mybot_his($v['uid'], 1, $v['botnum'], 0, '订单成交每日分红');
                db('orderbot')->where("id='" . $v['id'] . "'")->update(['state' => 2]);
                usleep(20000);
            }
        }
    }


    //xuanfeng 2020-08-04 10:59 每日发放矿机收益
    public function senddaylist()
    {
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');

        //开始生成每日的日期
        $datestr = date('Y-m-d');
        //得到开始时间
        $firstday = strtotime($datestr);

        $list = db('shopmylist')->where("`sendtime`='" . $firstday . "' and state=1")->limit(50)->select();
        if (!empty($list)) {
            foreach ($list as $v) {
                $shopinfo = db('shopmy')->where("id='" . $v['shopid'] . "'")->find();
                //先给用户mybot列的余额加上相应数量
                db('user')->where("id='" . intval($v['uid']) . "'")->setInc('mybot', $v['dayearn']);
                //再给相应用户加上mybot历史
                mybot_his($v['uid'], 1, $v['dayearn'], 0, '矿机“' . $shopinfo['title'] . '”每日收益');
                db('shopmylist')->where("id='" . $v['id'] . "'")->update(['state' => 2, 'edittime' => time()]);
                usleep(20000);
                //修改矿机信息：减去
                $shopdata['usedaynum'] = ($shopinfo['usedaynum'] + 1);
                $shopdata['useearn'] = ($shopinfo['useearn'] + $v['dayearn']);
                db('shopmy')->where("id='" . $v['shopid'] . "'")->update($shopdata);

            }
            echo '结算成功';
            exit;
        } else {
            echo '暂无待结算数据';
        }

    }


}
