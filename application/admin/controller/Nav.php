<?php
/**
 * Created by PhpStorm.
 * User:lll
 * Date: 2018/11/24
 * Time: 16:51
 */

namespace app\admin\controller;

use app\common\model\Settings;
use think\Exception;
use think\Db;
use app\common\model\Message;

class Nav extends Common
{
    public function index()
    {
        //标题
        $this->templateTitle("导航模块");

        $where = [];

        $id = trim(input('id'));
        $this->assign('id', $id);
        if ($id) {
            $where['id|title'] = trim($id);
        }

        $type = trim(input('type'));
        $this->assign('type', $type);
        if ($type) {
            $where['type'] = $type;
        }

        $is_visible = trim(input('is_visible'));
        $this->assign('is_visible', $is_visible);
        if ($is_visible != '') {
            $where['is_visible'] = $is_visible;
        }

        //获取所有用户
        $list = Db::name("nav")
            ->where($where)
            ->order("display_order DESC,id DESC")
            ->paginate(10);

        $this->assign("list", $list);
        $this->assign('arr', config('link_type'));
        return $this->fetch();
    }

    //添加
    public function add()
    {
        $link_type = config('link_type');
        if (request()->isGet()) {
            $this->layoutModal();
            $this->templateTitle('添加导航');
            return $this->fetch();
        }

        if (request()->isAjax()) {
            //创建数据
            $data = array(
                "create_time" => now_datetime(),
                "update_time" => now_datetime()
            );

            //幻灯图片
            $data["img_url"] = request()->param("img_url");
            if (empty($data['img_url'])) {
                return $this->ajaxError('请上传幻灯图片');
            }

            $data["description"] = trim(request()->param("description"));

            $data['type'] = intval(request()->param('type'));
            if (empty($data['type'])) {
                return $this->ajaxError('请选择跳转类型');
            }

            $data['link_url'] = str_replace(' ', '', request()->param('link_url'));
            if ($link_type[$data['type']]['link_need'] == 1 && $data['link_url'] == '') {
                return $this->ajaxError($link_type[$data['type']]['placeholder']);
            }

            //排序
            $data["display_order"] = request()->param("display_order");
            //是否显示
            $data["is_visible"] = request()->param("visible");
            try {
                $result = Db::name('nav')
                    ->insertGetId($data);
                $log = '添加-导航';
                if ($result) {
                    $log .= '-' . $result;
                    Message::admin_log(0, $log);
                    $this->ajaxSuccess($log . "成功！");
                } else {
                    Message::admin_log(0, $log);
                    $this->ajaxError($log . '失败');
                }
            } catch (\Exception $exception) {
                global $admin_has_exception;
                $admin_has_exception = true;
                $this->ajaxError($exception->getMessage());
            }
        }
    }

    //编辑
    public function edit()
    {
        //ID
        $Id = request()->param("id");
        $link_type = config('link_type');
        if (request()->isGet()) {
            $this->layoutModal();

            //获取信息
            $info = Db::name("nav")
                ->where("id", $Id)
                ->find();
            $this->assign("info", $info);

            $this->assign("single_image", array("img_url" => $info["img_url"]));
            $this->assign("type", $info['type']);
            $this->assign("link_url", $info['link_url']);

            return $this->fetch();
        }

        if (request()->isAjax()) {
            //
            //创建数据
            $data = array(
                "update_time" => now_datetime(),
            );
            //幻灯图片
            $data["img_url"] = request()->param("img_url");
            if (empty($data['img_url'])) {
                return $this->ajaxError('请上传幻灯图片');
            }

            $data["description"] = trim(request()->param("description"));

            $data['type'] = intval(request()->param('type'));
            if (empty($data['type'])) {
                return $this->ajaxError('请选择跳转类型');
            }

            $data['link_url'] = str_replace(' ', '', request()->param('link_url'));
            if ($link_type[$data['type']]['link_need'] == 1 && $data['link_url'] == '') {
                return $this->ajaxError($link_type[$data['type']]['placeholder']);
            }

            //排序
            $data["display_order"] = request()->param("display_order");
            //是否显示
            $data["is_visible"] = request()->param("visible");

            try {

                //更新分类ID路径
                $save = Db::name("nav")
                    ->where("id", $Id)
                    ->update($data);
                $log = '编辑-导航-' . $Id;
                if ($save) {
                    Message::admin_log(0, $log);
                    $this->ajaxSuccess($log . "成功！");
                } else {
                    Message::admin_log(0, $log);
                    $this->ajaxError($log . '失败');
                }
            } catch (\Exception $exception) {
                global $admin_has_exception;
                $admin_has_exception = true;
                $this->ajaxError($exception->getMessage());
            }
        }
    }

    //删除
    public function remove()
    {
        if (request()->isAjax()) {
            try {
                //获取ID
                $Id = request()->param("id");
                // 删除
                $result = Db::name('nav')
                    ->where('id', $Id)
                    ->delete();
                $log = '删除-导航-' . $Id;
                if ($result) {
                    Message::admin_log(0, $log);
                    $this->ajaxSuccess($log . "成功！");
                } else {
                    Message::admin_log(0, $log);
                    $this->ajaxError($log . '失败');
                }
            } catch (\Exception $exception) {
                global $admin_has_exception;
                $admin_has_exception = true;
                $this->ajaxError($exception->getMessage());
            }

        }
    }

    //更改菜单显示隐藏
    public function visible()
    {
        if (request()->isAjax()) {
            //菜单ID
            $id = request()->param("id");
            //获取可见性状态
            $status = request()->param("status");

            try {
                //修改菜单显示隐藏
                $save = Db::name("nav")
                    ->where("id", $id)
                    ->setField("is_visible", $status);

                if ($status == 1) {
                    $log = '设置-导航-' . $id . '-显示';
                } else {
                    $log = '设置-导航-' . $id . '-隐藏';
                }

                if ($save) {
                    Message::admin_log(0, $log);
                    $this->ajaxSuccess($log . '成功！');

                } else {
                    Message::admin_log(0, $log);
                    $this->ajaxError($log . '失败');
                }
            } catch (\Exception $exception) {
                global $admin_has_exception;
                $admin_has_exception = true;
                $this->ajaxError($exception->getMessage());
            }
        }
    }
}