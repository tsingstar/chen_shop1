<?php
/*
ThinkPHP5.0+整合百度编辑器Ueditor1.4.3.3+
作者：符工@邦明
日期：西元二零一七年元月五日
网址：http://bbs.df81.com/
不要怀念哥，哥只是个搬运工
*/

namespace app\admin\controller;

use think\Controller;
use think\Image;
use app\common\model\Storage;

class Ueditor extends Controller
{

    protected function _initialize()
    {
        if (!session("?H_ADMIN_LOGIN_USER") || session("H_ADMIN_DUE_TIME") < time()) {
            session("H_ADMIN_REFERER", url("Index/index"));

            //判断是否为ajax操作
            if (request()->isAjax()) {
                die(json_encode(array(
                    'state' => '登录过期',
                )));
            } else {
                die("<script>if (window.parent){window.parent.location.href='"
                    . url("Login/index") . "';}else{location.href='"
                    . url("Login/index") . "';}</script>");
            }
        }
    }

    public function index()
    {
        //设置http://www.baidu.com允许跨域访问
        header('Access-Control-Allow-Origin: http://www.baidu.com');
        //设置允许的跨域header
        header('Access-Control-Allow-Headers: X-Requested-With,X_Requested_With');

        error_reporting(E_ERROR);
        header("Content-Type: text/html; charset=utf-8");

        //配置文件路径
        $configFile = APP_PATH . "/../public/static/vendors/ueditor/php/config.json";
        //加载配置参数
        $config = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents($configFile)), true);

        //获取当前操作
        $action = $_GET['action'];

        //根据当前操作执行相关动作
        switch ($action) {
            //获取当前配置
            case 'config':
                $result = json_encode($config);
                break;
            /* 上传图片 */
            case 'uploadimage':
                $fieldName = $config['imageFieldName'];
                $result = $this->uploadFile($fieldName, "images", $config["imageMaxSize"]);
                break;
            /* 上传涂鸦 */
            case 'uploadscrawl':
                $fieldName = $config['scrawlFieldName'];
                $config = array(
                    "pathFormat" => $config['scrawlPathFormat'],
                    "maxSize" => $config['scrawlMaxSize'],
                    "allowFiles" => $config['scrawlAllowFiles'],
                    "oriName" => "scrawl.png"
                );
                $base64 = "base64";
                $result = $this->uploadScrawl($config, $fieldName);
                break;
            /* 上传视频 */
            case 'uploadvideo':
                $fieldName = $config['videoFieldName'];
                $result = $this->uploadFile($fieldName, "videos", $config["videoMaxSize"]);
                break;
            /* 上传文件 */
            case 'uploadfile':
                $fieldName = $config['fileFieldName'];
                $result = $this->uploadFile($fieldName, "files", $config["fileMaxSize"]);
                break;
            /* 列出图片 */
            case 'listimage':
                $allowFiles = $config['imageManagerAllowFiles'];
                $listSize = $config['imageManagerListSize'];
                $path = $config['imageManagerListPath'];
                $get = $_GET;
                $result = $this->fileList($allowFiles, $listSize, $get);
                break;
            /* 列出文件 */
            case 'listfile':
                $allowFiles = $config['fileManagerAllowFiles'];
                $listSize = $config['fileManagerListSize'];
                $path = $config['fileManagerListPath'];
                $get = $_GET;
                $result = $this->fileList($allowFiles, $listSize, $get);
                break;
            /* 抓取远程文件 */
            case 'catchimage':
                $config = array(
                    "pathFormat" => $config['catcherPathFormat'],
                    "maxSize" => $config['catcherMaxSize'],
                    "allowFiles" => $config['catcherAllowFiles'],
                    "oriName" => "remote.png"
                );
                $fieldName = $config['catcherFieldName'];
                /* 抓取远程图片 */
                $list = array();
                isset($_POST[$fieldName]) ? $source = $_POST[$fieldName] : $source = $_GET[$fieldName];

                foreach ($source as $imgUrl) {
                    $info = json_decode($this->saveRemote($config, $imgUrl), true);
                    array_push($list, array(
                        "state" => $info["state"],
                        "url" => $info["url"],
                        "size" => $info["size"],
                        "title" => htmlspecialchars($info["title"]),
                        "original" => htmlspecialchars($info["original"]),
                        "source" => htmlspecialchars($imgUrl)
                    ));
                }
                $result = json_encode(array(
                    'state' => count($list) ? 'SUCCESS' : 'ERROR',
                    'list' => $list
                ));
                break;
            default:
                $result = json_encode(array(
                    'state' => '请求地址出错'
                ));
                break;
        }
        /* 输出结果 */
        if (isset($_GET["callback"])) {
            if (preg_match("/^[\w_]+$/", $_GET["callback"])) {
                die(htmlspecialchars($_GET["callback"]) . '(' . $result . ')');
            } else {
                die(json_encode(array(
                    'state' => 'callback参数不合法'
                )));
            }
        } else {
            die($result);
        }
    }

    //上传文件
    private function uploadFile($fieldName, $subPath, $maxSize)
    {
        $file = request()->file($fieldName);

        //判断文件大小
        if (!$file->checkSize($maxSize)) {
            return json_encode(array("state" => translate("Ueditor-uploadFile:too_max")));
        }


        if ($subPath == 'images') {
            $params['ext'] = config('img_ext');
        } else if ($subPath == 'videos') {
            $params['ext'] = config('videos_ext');
        } else {
            $params['ext'] = config('files_ext');
        }

        $storage = new Storage();
        $type = '.' . $file->getExtension();
        $size = $file->getSize();
        $info = $storage->upload($fieldName, $params);
        if ($info['status'] == 0) {
            $data = array(
                'state' => 'SUCCESS',
                'url' => $info['data'],
                'title' => '',
                'original' => '',
                'type' => $type,
                'size' => $size,
            );
        } else {
            $data = array(
                'state' => $info['info'],
            );
        }
        return json_encode($data);
    }

    //列出图片
    private function fileList($allowFiles, $listSize, $get)
    {
        $dirname = "./uploads";
        $allowFiles = substr(str_replace(".", "|", join("", $allowFiles)), 1);
        /* 获取参数 */
        $size = isset($get['size']) ? htmlspecialchars($get['size']) : $listSize;
        $start = isset($get['start']) ? htmlspecialchars($get['start']) : 0;
        $end = $start + $size;
        /* 获取文件列表 */
        $path = $dirname;
        $files = $this->getFiles($path, $allowFiles);
        if (!count($files)) {
            return json_encode(array(
                "state" => "no match file",
                "list" => array(),
                "start" => $start,
                "total" => count($files)
            ));
        }
        /* 获取指定范围的列表 */
        $len = count($files);
        for ($i = min($end, $len) - 1, $list = array(); $i < $len && $i >= 0 && $i >= $start; $i--) {
            $list[] = $files[$i];
        }
        /* 返回数据 */
        $result = json_encode(array(
            "state" => "SUCCESS",
            "list" => $list,
            "start" => $start,
            "total" => count($files)
        ));
        return $result;
    }

    /*
  * 遍历获取目录下的指定类型的文件
  * @param $path
  * @param array $files
  * @return array
 */
    private function getFiles($path, $allowFiles, &$files = array())
    {
        if (!is_dir($path)) return null;
        if (substr($path, strlen($path) - 1) != '/') $path .= '/';
        $handle = opendir($path);

        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..') {
                $path2 = $path . $file;
                if (is_dir($path2)) {
                    $this->getFiles($path2, $allowFiles, $files);
                } else {
                    if (preg_match("/\.(" . $allowFiles . ")$/i", $file)) {
                        $files[] = array(
                            'url' => substr($path2, 1),
                            'mtime' => filemtime($path2)
                        );
                    }
                }
            }
        }

        return $files;
    }

    //抓取远程图片
    private function saveRemote($config, $fieldName)
    {
        $imgUrl = htmlspecialchars($fieldName);
        $imgUrl = str_replace("&amp;", "&", $imgUrl);
        //http开头验证
        if (strpos($imgUrl, "http") !== 0) {
            $data = array(
                'state' => '链接不是http链接',
            );
            return json_encode($data);
        }
        //获取请求头并检测死链
        $heads = get_headers($imgUrl);
        if (!(stristr($heads[0], "200") && stristr($heads[0], "OK"))) {
            $data = array(
                'state' => '链接不可用',
            );
            return json_encode($data);
        }
        //格式验证(扩展名验证和Content-Type验证)
        $fileType = strtolower(strrchr($imgUrl, '.'));
        if (!in_array($fileType, $config['allowFiles']) || stristr($heads['Content-Type'], "image")) {
            $data = array(
                'state' => '链接contentType不正确',
            );
            return json_encode($data);
        }
        //打开输出缓冲区并获取远程图片
        ob_start();
        $context = stream_context_create(
            array('http' => array(
                'follow_location' => false // don't follow redirects
            ))
        );
        readfile($imgUrl, false, $context);
        $img = ob_get_contents();
        ob_end_clean();
        preg_match("/[\/]([^\/]*)[\.]?[^\.\/]*$/", $imgUrl, $m);
        $dirname = './public/uploads/remote/';
        $file['oriName'] = $m ? $m[1] : "";
        $file['filesize'] = strlen($img);
        $file['ext'] = strtolower(strrchr($config['oriName'], '.'));
        $file['name'] = uniqid() . $file['ext'];
        $file['fullName'] = $dirname . $file['name'];
        $fullName = $file['fullName'];
        //检查文件大小是否超出限制
        if ($file['filesize'] >= ($config["maxSize"])) {
            $data = array(
                'state' => '文件大小超出网站限制',
            );
            return json_encode($data);
        }
        //创建目录失败
        if (!file_exists($dirname) && !mkdir($dirname, 0777, true)) {
            $data = array(
                'state' => '目录创建失败',
            );
            return json_encode($data);
        } else if (!is_writeable($dirname)) {
            $data = array(
                'state' => '目录没有写权限',
            );
            return json_encode($data);
        }
        //移动文件
        if (!(file_put_contents($fullName, $img) && file_exists($fullName))) { //移动失败
            $data = array(
                'state' => '写入文件内容错误',
            );
            return json_encode($data);
        } else { //移动成功
            $data = array(
                'state' => 'SUCCESS',
                'url' => substr($file['fullName'], 1),
                'title' => $file['name'],
                'original' => $file['oriName'],
                'type' => $file['ext'],
                'size' => $file['filesize'],
            );
        }

        return json_encode($data);
    }

    //上传涂鸦文件
    private function uploadScrawl($config, $fieldName)
    {
        //解析图片数据
        $base64Data = $_POST[$fieldName];
        $img = base64_decode($base64Data);

        //涂鸦存储路径
        $subPath = "scrawls/" . date("m") . "/" . date("d") . "/" . date("Y");
        $savePath = config("upload_path") . '/' . $subPath;

        //文件大小
        $file['filesize'] = strlen($img);
        //文件原始名称
        $file['oriName'] = $config['oriName'];
        //文件扩展名
        $file['ext'] = strtolower(strrchr($config['oriName'], '.'));
        //文件名称
        $file['name'] = uniqid() . $file['ext'];
        //文件完整名称
        $file['fullName'] = $savePath . '/' . $file['name'];

        //检查文件大小是否超出限制
        if ($file['filesize'] >= ($config["maxSize"])) {
            return json_encode(array("state" => translate("Ueditor-uploadScrawl:file_size_overrun")));
        }

        //创建目录失败
        if (!file_exists($savePath) && !mkdir($savePath, 0777, true)) {
            return json_encode(array("state" => translate("Ueditor-uploadScrawl:create_dir_error")));
        } else if
        (!is_writeable($savePath)) {
            return json_encode(array("state" => translate("Ueditor-uploadScrawl:write_dir_error")));
        }

        //移动文件
        if (!(file_put_contents($file['fullName'], $img)
            && file_exists($file['fullName']))) {
            return json_encode(array("state" => translate("Ueditor-uploadScrawl:write_file_error")));
        } else {
            //移动成功
            $fileName = "/uploads/" . $subPath . '/' . $file["name"];
            $data = array(
                'state' => 'SUCCESS',
                'url' => substr($fileName, 1),
                'title' => $file['name'],
                'original' => $file['oriName'],
                'type' => $file['ext'],
                'size' => $file['filesize'],
            );

            return json_encode($data);
        }
    }

}