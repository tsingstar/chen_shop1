<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/7/31
 * Time: 17:33
 */

namespace app\admin\controller;


use app\common\model\Message;
use think\Db;

class Shop extends Common
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 导航图管理
     */
    public function navList()
    {
        $list = db("nav")->order("display_order desc")->select();
        $this->assign("list", $list);
        return $this->fetch("nav_list");
    }

    /**
     * 导航图添加
     */
    public function addNav()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            return $this->fetch("add_nav");
        } else {
            if (request()->isAjax()) {
                $data = input("post.");
                $data["create_time"] = time();
                $data["update_time"] = time();
                $res = db("nav")->insert($data);
                if ($res) {
                    Message::admin_log(0, "添加导航图");
                    $this->ajaxSuccess("添加成功");
                } else {
                    $this->ajaxError("系统繁忙");
                }
            }
        }
    }

    /**
     * 删除导航图
     */
    public function delNav()
    {
        $id = input("id");
        if (empty($id)) {
            $this->ajaxError("参数错误");
        }
        $res = db("nav")->where("id", $id)->delete();
        if ($res) {
            Message::admin_log(0, "删除导航图，id:" . $id);
            $this->ajaxSuccess("删除成功");
        } else {
            $this->ajaxError("删除失败");
        }
    }

    /**
     * 导航图标编辑
     */
    public function editNav()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            $news_id = input("news_id");
            $news = db("nav")->where("id", $news_id)->find();
            if (empty($news)) {
                $this->error("导航图标不存在");
            }
            $this->assign("info", $news);
            $this->assign("single_image", ["img_url" => $news["img_url"]]);
            return $this->fetch("edit_nav");
        } else {
            if (request()->isAjax()) {
                $data = input("post.");
                if (empty($data["title"])) {
                    $this->ajaxError("导航图标不能为空");
                }
                $data["update_time"] = time();
                $res = db("nav")->where("id", input("news_id"))->update($data);
                if ($res) {
                    Message::admin_log(0, "编辑导航图标，id：" . input("news_id"));
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxError("系统繁忙");
                }
            }
        }
    }

    /**
     * 轮播图管理
     */
    public function swiperList()
    {
        $list = db("swiper")->order("display_order desc")->select();
        $this->assign("list", $list);
        return $this->fetch("swiper_list");
    }

    /**
     * 轮播图添加
     */
    public function addSwiper()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            return $this->fetch("add_swiper");
        } else {
            if (request()->isAjax()) {
                $data = input("post.");
                $data["create_time"] = time();
                $data["update_time"] = time();
                $res = db("swiper")->insert($data);
                if ($res) {
                    Message::admin_log(0, "添加轮播图");
                    $this->ajaxSuccess("添加成功");
                } else {
                    $this->ajaxError("系统繁忙");
                }
            }
        }
    }

    /**
     * 删除轮播图
     */
    public function delSwiper()
    {
        $id = input("id");
        if (empty($id)) {
            $this->ajaxError("参数错误");
        }
        $res = db("swiper")->where("id", $id)->delete();
        if ($res) {
            Message::admin_log(0, "删除轮播图，id:" . $id);
            $this->ajaxSuccess("删除成功");
        } else {
            $this->ajaxError("删除失败");
        }
    }

    /**
     * 新闻公告
     */
    public function newsList()
    {
        $model = db("news");
        $params = input("get.");
        if (isset($params["cate"]) && $params["cate"] != 0) {
            $model->where("cate", $params['cate']);
        }
        $list = $model->paginate(10, false, ["query" => $params]);
        $this->assign("params", $params);
        $this->assign("list", $list);
        $this->assign("cate", [1 => "服务中心", 2 => "系统消息"]);
        return $this->fetch("news_list");
    }

    /**
     * 新闻添加
     */
    public function addNews()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            return $this->fetch("add_news");
        } else {
            if (request()->isAjax()) {
                $data = input("post.");
                if (empty($data["title"])) {
                    $this->ajaxError("新闻标题不能为空");
                }
                if (empty($data["content"])) {
                    $this->ajaxError("详情不能为空");
                }
                $data["create_time"] = time();
                $data["update_time"] = time();
                $res = db("news")->insert($data);
                if ($res) {
                    Message::admin_log(0, "添加新闻");
                    $this->ajaxSuccess("添加成功");
                } else {
                    $this->ajaxError("系统繁忙");
                }
            }
        }
    }

    /**
     * 新闻编辑
     */
    public function editNews()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            $news_id = input("news_id");
            $news = db("news")->where("id", $news_id)->find();
            if (empty($news)) {
                $this->error("新闻不存在");
            }
            $this->assign("info", $news);
            $this->assign("ueditor_content", ['content' => $news["content"]]);
            return $this->fetch("edit_news");
        } else {
            if (request()->isAjax()) {
                $data = input("post.");
                if (empty($data["title"])) {
                    $this->ajaxError("新闻标题不能为空");
                }
                if (empty($data["content"])) {
                    $this->ajaxError("详情不能为空");
                }
                $data["update_time"] = time();
                $res = db("news")->where("id", input("news_id"))->update($data);
                if ($res) {
                    Message::admin_log(0, "编辑新闻，id：" . input("news_id"));
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxError("系统繁忙");
                }
            }
        }
    }

    /**
     * 删除新闻
     */
    public function delNews()
    {
        $news_id = input("news_id");
        if (empty($news_id)) {
            $this->ajaxError("参数错误");
        }
        $res = db("news")->where("id", $news_id)->delete();
        if ($res) {
            Message::admin_log(0, "删除新闻，id:" . $news_id);
            $this->ajaxSuccess("删除成功");
        } else {
            $this->ajaxError("删除失败");
        }
    }

    /**
     * 银行列表
     */
    public function bankList()
    {
        $model = db("bank_list");
        $list = $model->order("display_order desc")->paginate(10);
        $this->assign("list", $list);
        return $this->fetch("bank_list");
    }

    /**
     * 银行添加
     */
    public function addBank()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            return $this->fetch("add_bank");
        } else {
            if (request()->isAjax()) {
                $data = input("post.");
                if (empty($data["bank_name"])) {
                    $this->ajaxError("银行名称不能为空");
                }
                $data["create_time"] = time();
                $data["update_time"] = time();
                $res = db("bank_list")->insert($data);
                if ($res) {
                    Message::admin_log(0, "添加银行");
                    $this->ajaxSuccess("添加成功");
                } else {
                    $this->ajaxError("系统繁忙");
                }
            }
        }
    }

    /**
     * 银行编辑
     */
    public function editBank()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            $id = input("id");
            $bank = db("bank_list")->where("id", $id)->find();
            if (empty($bank)) {
                $this->error("银行不存在");
            }
            $this->assign("info", $bank);
            return $this->fetch("edit_bank");
        } else {
            if (request()->isAjax()) {
                $data = input("post.");
                if (empty($data["bank_name"])) {
                    $this->ajaxError("银行名称不能为空");
                }
                $data["update_time"] = time();
                $res = db("bank_list")->where("id", input("id"))->update($data);
                if ($res) {
                    Message::admin_log(0, "编辑银行，id:" . input("id"));
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxError("系统繁忙");
                }
            }
        }
    }

    /**
     * 删除银行
     */
    public function delBank()
    {
        $id = input("id");
        if (empty($id)) {
            $this->ajaxError("参数错误");
        }
        $res = db("bank_list")->where("id", $id)->delete();
        if ($res) {
            Message::admin_log(0, "删除银行，id:" . $id);
            $this->ajaxSuccess("删除成功");
        } else {
            $this->ajaxError("删除失败");
        }
    }

    /**
     * 商城商品
     */
    public function goods()
    {
        $model = db("goods");
        $list = $model->order("sort desc")->paginate(10);
        $this->assign("list", $list);
        return $this->fetch("goods");
    }

    /**
     * 添加商品
     */
    public function addGoods()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            return $this->fetch("add_goods");
        } else {
            if (request()->isAjax()) {
                $info = input("post.");
                $info['time_start'] = transtime(input('time_start'));
                $info['time_end'] = transtime(input('time_end'));
                if (empty($info['title'])) {
                    $this->ajaxError("产品名称不能为空");
                }
                if (empty($info['img_url'])) {
                    $this->ajaxError('产品图片有误上传失败');
                }
                $res = db("goods")->insert($info);
                if ($res) {
                    Message::admin_log(0, "添加产品".json_encode($info));
                    $this->ajaxSuccess("添加成功");
                } else {
                    $this->ajaxError('系统繁忙，添加失败');
                }
            }
        }
    }

    /**
     * 删除商品
     */
    public function delGoods()
    {
        $id = intval(input('id'));
        $del = db("goods")->where("id", $id)->delete();
        if ($del) {
            Message::admin_log(0, "删除商品，id：" . $id);
            $this->ajaxSuccess('删除成功！');
        } else {
            $this->ajaxError('删除失败！');
        }
    }

    /**
     * 编辑商品
     */
    public function editGoods()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            $id = input("id");
            $info = db('goods')->where("id", $id)->find();
            if (!$info) {
                $this->error('数据不存在或已删除!!!');
                exit;
            }
            $this->assign('info', $info);
            $this->assign("single_image", ["img_url" => $info["img_url"]]);
            $this->assign("ueditor_content", ['desc'=>$info['desc']]);
            return $this->fetch("edit_goods");
        } else {
            if (request()->isAjax()) {
                $id = input("id");
                $data = input("post.");
                $data['botnum'] = round($data['botnum'],2);
                $data['time_start'] = transtime($data['time_start']);
                $data['time_end'] = transtime($data['time_end']);
                if (empty($data['title'])) {
                    $this->ajaxError("产品名称不能为空");
                }
                if (empty($data['img_url'])) {
                    $this->ajaxError('产品图片有误上传失败');
                }
                $res = db("goods")->where("id", $id)->update($data);
                if ($res) {
                    Message::admin_log(0, "编辑商品成功，id:" . $id.json_encode($data));
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxSuccess("编辑失败");
                }
            }
        }
    }

    /**
     * 商城商品
     */
    public function shop_goods()
    {
        $model = db("shop_goods");
        $list = $model->order("sort desc")->paginate(10);
        $this->assign("list", $list);
        return $this->fetch("shop_goods");
    }
    /**
     * 添加商品
     */
    public function addshopGoods()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            return $this->fetch("add_shop_goods");
        } else {
            if (request()->isAjax()) {
                $info = input("post.");
                if (empty($info['title'])) {
                    $this->ajaxError("产品名称不能为空");
                }
                if (empty($info['img_url'])) {
                    $this->ajaxError('产品图片有误上传失败');
                }
                $res = db("shop_goods")->insert($info);
                if ($res) {
                    Message::admin_log(0, "添加商城产品");
                    $this->ajaxSuccess("添加成功");
                } else {
                    $this->ajaxError('系统繁忙，添加失败');
                }
            }
        }
    }

    /**
     * 删除商品
     */
    public function delshopGoods()
    {
        $id = intval(input('id'));
        $del = db("shop_goods")->where("id", $id)->delete();
        if ($del) {
            Message::admin_log(0, "删除商城商品，id：" . $id);
            $this->ajaxSuccess('删除成功！');
        } else {
            $this->ajaxError('删除失败！');
        }
    }

    /**
     * 编辑商品
     */
    public function editshopGoods()
    {
        if (request()->isGet()) {
            $this->layoutModal();
            $id = input("id");
            $info = db('shop_goods')->where("id", $id)->find();
            if (!$info) {
                $this->error('数据不存在或已删除!!!');
                exit;
            }
            $this->assign('info', $info);
            $this->assign("single_image", ["img_url" => $info["img_url"]]);
            return $this->fetch("edit_shop_goods");
        } else {
            if (request()->isAjax()) {
                $id = input("id");
                $data = input("post.");
                if (empty($data['title'])) {
                    $this->ajaxError("产品名称不能为空");
                }
                if (empty($data['img_url'])) {
                    $this->ajaxError('产品图片有误上传失败');
                }
                $res = db("shop_goods")->where("id", $id)->update($data);
                if ($res) {
                    Message::admin_log(0, "编辑商城商品成功，id:" . $id);
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxSuccess("编辑失败");
                }
            }
        }
    }

    /**
     * 帮助中心
     */
    public function service()
    {
        if (request()->isGet()) {
            $info = db("service")->where("type", 1)->find();
            $this->assign("info", $info);
            if (!empty($info)) {
                $this->assign("ueditor_content", ["content" => $info["content"]]);
            }
            return $this->fetch("service");
        } else {
            if (request()->isAjax()) {
                db("service")->where("type", 1)->delete();
                $data = input("post.");
                $data["type"] = 1;
                $res = db("service")->insert($data);
                if ($res) {
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxError("编辑失败");
                }
            }
        }
    }

    /**
     * 首页通知
     */
    public function indexNotice()
    {
        if (request()->isGet()) {
            $info = db("index_notice")->find();
            $this->assign("info", $info);
            if (!empty($info)) {
                $this->assign("ueditor_content", ["content" => $info["content"]]);
            }
            return $this->fetch("notice");
        } else {
            if (request()->isAjax()) {
                db("index_notice")->where("1=1")->delete();
                $data = input("post.");
                $res = db("index_notice")->insert($data);
                if ($res) {
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxError("编辑失败");
                }
            }
        }
    }

    /**
     * 联系我们
     */
    public function contact()
    {

        if (request()->isGet()) {
            $info = db("service")->where("type", 2)->find();
            $this->assign("info", $info);
            if (!empty($info)) {
                $this->assign("ueditor_content", ["content" => $info["content"]]);
            }
            return $this->fetch("service");
        } else {
            if (request()->isAjax()) {
                db("service")->where("type", 2)->delete();
                $data = input("post.");
                $data["type"] = 2;
                $res = db("service")->insert($data);
                if ($res) {
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxError("编辑失败");
                }
            }
        }
    }

    /**
     * 用户协议及隐私政策
     */
    public function treaty()
    {

        if (request()->isGet()) {
            $info = db("service")->where("type", 3)->find();
            $this->assign("info", $info);
            if (!empty($info)) {
                $this->assign("ueditor_content", ["content" => $info["content"]]);
            }
            return $this->fetch("service");
        } else {
            if (request()->isAjax()) {
                db("service")->where("type", 3)->delete();
                $data = input("post.");
                $data["type"] = 3;
                $res = db("service")->insert($data);
                if ($res) {
                    $this->ajaxSuccess("编辑成功");
                } else {
                    $this->ajaxError("编辑失败");
                }
            }
        }
    }

    /**
     * 预约列表
     */
    public function subList()
    {
        $model = db("apply")->where("id", "gt", 0);
        //搜索条件-真实姓名
        $realname = request()->param("realname");
        $this->assign("realname", $realname);
        if ($realname) {
            $uid = get_uid($realname);
            $model->whereIn("uid", $uid);
        }
        //根据手机号搜索用户
        $selfphone = request()->param("selfphone");
        $this->assign("selfphone", $selfphone);
        if ($selfphone) {
            $uid = db('user')->where("selfphone='".$selfphone."'")->value('id');
            if(!empty($uid)){
                $model->whereIn("uid", $uid);
            }
        }

        $is_levfix = trim(request()->param("is_levfix"));
        if ($is_levfix !== "") {
            $where["status"] = $is_levfix;
            $model->where("status", $is_levfix);
        }
        $this->assign("is_levfix", $is_levfix);
        //是否指定
        $point_status = trim(request()->param("point_status"));
        if ($point_status !== "") {
            $where["point_status"] = $point_status;
            $model->where("point_status", $point_status);
        }
        $this->assign("point_status", $point_status);
        $time_range = input("time_range");
        if (!empty($time_range) && $time_range != "") {
            list($start_time, $end_time) = explode("~", $time_range);

        } else {
            $start_time = date("Y-m-d", strtotime("-7 day"));
            $end_time = date("Y-m-d");
        }
        $model->where("ctime", "gt", strtotime($start_time))->where("ctime", "lt", strtotime($end_time) + 86400);
        $this->assign("range_start", ["time_range" => $start_time]);
        $this->assign("range_end", ["time_range" => $end_time]);
        $good_id = input("good_id", 0);
        if (!empty($good_id)) {
            $model->where("proid", $good_id);
        }
        $this->assign("good_id", $good_id);

        //拉取所有商品
        $good_list = db("goods")->field("id, title")->select();
        $this->assign("good_list", $good_list);

        //获取所有用户
        $list = $model->order("id DESC")->paginate(10);
        $page = $list->render();
        $this->assign("list", $list);
        return $this->fetch();
    }

    //给预约客户指定一个订单（最新生成）
    public function giveorder()
    {
        $this->layoutModal();
        $id = input("id");
        $applyinfo = db('apply')->where("id", $id)->find();
        if (empty($applyinfo)) {
            $this->error('非法操作！');
            exit;
        }
        if ($applyinfo['status'] > 0) {
            //如果预约已经处理或者已退还，不允许指定订单
            $this->error('仅限未处理订单！！');
            exit;
        }
        $this->assign("info", $applyinfo);

        //用户预约的那个产品的相关信息
        $proinfo = db('goods')->where("id", $applyinfo['proid'])->find();
        $this->assign('proinfo', $proinfo);
        //读取订单列表，是否有这个订单，把这个预约的id存到订单里面去
        $order_check = db(config("db_order_table"))->where("applyid='" . $applyinfo['id'] . "'")->find();
        if (!empty($order_check)) {
            $order_check['ctime'] = date('Y-m-d H:i:s', $order_check['ctime']);
        } else {
            $orderc = (time() - 86400 * $proinfo['daynum']);
            $order_check['ctime'] = date('Y-m-d H:i:s', $orderc);
        }
        $this->assign('order_check', $order_check);

        //获取可使用官方号，以便做成出售人
        $our_user = \db('user')->where("is_our", 2)->column('username', 'id');
        $this->assign("our_user", $our_user);
        return $this->fetch();

    }

    /**
     * 指定一个已经存在的订单
     */
    public function giveexistorder()
    {
        $this->layoutModal();
        $id = input("id");
        $applyinfo = db('apply')->where("id", $id)->find();
        if (empty($applyinfo)) {
            $this->error('非法操作！');
            exit;
        }
        if ($applyinfo['status'] > 0) {
            //如果预约已经处理或者已退还，不允许指定订单
            $this->error('仅限未处理订单！！');
            exit;
        }
        $this->assign("info", $applyinfo);

        //用户预约的那个产品的相关信息
        $proinfo = db('goods')->where("id", $applyinfo['proid'])->find();
        $this->assign('proinfo', $proinfo);
        $proid = $applyinfo['proid'];
        $list = \db(config('db_order_table'))->alias('a')->join('user b', 'a.buyuid=b.id')->where('a.productid', $proid)->where('a.xs', 2)->where('a.cansaletime','<', now_datetime())->where('a.peiduistatus', 1)->where('a.applyid', 0)->order('a.price desc, a.id asc')->field('a.id order_id, b.username user_name, b.selfphone telephone, a.title, a.basicprice, a.ctime')->select();
        $this->assign('list', $list);
        $total = \db(config('db_order_table'))->alias('a')->where('a.productid', $proid)->where('a.xs', 2)->where('a.cansaletime','<', now_datetime())->where('a.peiduistatus', 1)->where('a.applyid', 0)->count();
        $this->assign('total', $total);
        return $this->fetch('giveexistorder');



    }

    public function setOrder()
    {
        $order_id = input('order_id');
        $apply_id = input('apply_id');
        //查询订单状态是否正常
        $order = \db(config('db_order_table'))->where('id', $order_id)->find();
        $apply = \db('apply')->where('id', $apply_id)->find();
        if($order['peiduistatus']!=1){
            $this->ajaxError('订单配对状态异常');
        }
        if($order['applyid']>0){
            $this->ajaxError('订单已经被指定过');
        }

        if($apply['point_status']==2){
            $this->ajaxError('预约记录已经指定过');
        }
        //开始指定
        db()->startTrans();
        $r = db('apply')->where('id', $apply_id)->update(['point_status'=>2]);
        $r1 = \db(config('db_order_table'))->where('id', $order_id)->update([
            'applyid'=>$apply_id
        ]);
        if($r && $r1){
            Message::admin_log(0, '指定订单，预约id:'.$apply_id.';订单id：'.$order_id);
            \db()->commit();
            $this->ajaxSuccess('指定成功');
        }else{
            \db()->rollback();
            $this->ajaxError('指定失败');
        }
    }

    //生成已经可以销售的订单
    public function docreateorder()
    {
        $applyid = input('applyid');
        $buyuid = input('buyuid', 0);
        //查询出售人基本信息
        $buyuser = \db('user')->where('id', $buyuid)->find();
        if (empty($buyuser)) {
            $this->ajaxError("出售人不能为空");
        }

        //查询预约单基本信息
        $apply = \db('apply')->where('id', $applyid)->find();
        if (!empty($apply) && $apply['status'] != 0) {
            $this->ajaxError('预约记录不存在或者预约记录已处理');
        }
        $good = \db('goods')->where('id', $apply['proid'])->find();
        if (empty($good)) {
            $this->ajaxError('预约商品信息不存在');
        }
        $basicprice = input('basicprice');
        $ctime = input('ctime');
        $ctime = strtotime($ctime);
        //计算购买时价格和收益金额
        $price = round(($basicprice / (1 + $good['daypercent'] / 100)), 2);
        $dayaddprice = round($price * $good['daypercent'] / 100, 2);

        /*按照原来官方号购买规则生成一个已经可以购买的订单，唯一要注意的是order表的几个列内容
        * 1.basicprice是：$data['basicprice']
        * 2.ctime 是 $data['ctime']
        * 3.applyid 是 $applyid
        */
        $insert_data = [
            'buyuid' => $buyuid,
            'basicprice' => $basicprice,
            'ctime' => $ctime,
            'buyuidatc' => $good['atcnum'],
            'caifen' => $good['point_end'],
            'productid' => $good['id'],
            'title' => $good['title'],
            'daynum' => $good['daynum'],
            'earnpercent' => $good['daypercent'],
            'price' => $price,
            'dayaddprice' => $dayaddprice,
            'xs' => 2
        ];
        $insert_data['peiduistatus'] = 1;
        $insert_data['peiduiuid'] = 0;
        $insert_data['paystatus'] = 0;
        $insert_data['paytime'] = 0;
        $insert_data['payimg'] = '';
        $cansaletime = ($insert_data['ctime'] + 86400 * $good['daynum']);
        $cansaletimedate = date('Y-m-d', $cansaletime);
        $insert_data['cansaletime'] = strtotime($cansaletimedate);
        $now_day_dates = date('Y-m-d', $insert_data['ctime']);
        $insert_data ['thatdatetime'] = strtotime($now_day_dates);
        $insert_data['applyid'] = $applyid;
        //查一下有没有这个订单，如果有就不让编辑了，毕竟订单已经生成了
        $order_check = db(config("db_order_table"))->where("applyid='" . $applyid . "'")->find();
        if (!empty($order_check)) {
            //此处如果已经指定过，则修改之前指定的订单的基本信息
            $res = \db(config("db_order_table"))->where('id', $order_check['id'])->update($insert_data);
            if (empty($res)) {
                $this->ajaxError("指定订单修改失败！");
            } else {
                $this->ajaxSuccess("指定订单修改成功！");
            }
        }
        Message::admin_log(0, "指定订单(" . $basicprice . ")，applyid:" . $applyid);
        //这里写insert
        $res = \db(config("db_order_table"))->insert($insert_data);
        if ($res) {
            \db('apply')->where('id', $applyid)->update(['point_status' => 2]);
            $this->ajaxSuccess("指定订单成功！");
        } else {
            $this->ajaxError("系统繁忙，请稍后再试！");
            exit;
        }
    }

    //抹掉指定记录
    public function deleteapplys()
    {
        $applyid = input('id');
        if (empty($applyid)) {
            $this->error('非法操作！');
        }
        \db(config("db_order_table"))->where("applyid='" . $applyid . "'")->update(['applyid' => 0]);
        \db('apply')->where("id = '" . $applyid . "'")->update(['point_status' => 1]);
        Message::admin_log(0, "抹除订单，applyid:" . $applyid);
        $this->success("抹除成功！！");


    }

}
