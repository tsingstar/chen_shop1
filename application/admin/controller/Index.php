<?php
/**
 * Created by PhpStorm.
 * User: 雨寒
 * Date: 2018/7/18
 * Time: 上午1:08
 */
namespace app\admin\controller;

use think\Db;
use app\common\model\Message;

class Index extends Common
{
    public function index()
    {
        $this->templateTitle('后台管理系统 ');
        /*********************订单概况*************************/
        /*获取订单信息*/
        $order=Db::name('order');

//        /*已完成*/
//        $order3=$order
//            ->where("order_status","in", [3,5])
//            ->field('count(*) as num')
//            ->find();

        /****************** 会员状态 ********************/
        $user_total = \db("user")->count();
        $this->assign("user_total", $user_total);

        $alldata3=json_encode(array(
            array(
                "label"=>"游客",
                "value"=>12,
            ),
            array(
                "label"=>"会员",
                "value"=>12,
            ),
            array(
                "label"=>"黑名单",
                "value"=>23,
            )
        ));
        $this->assign("alldata3",$alldata3);
        /*全局配置*/
        $settings = config("setting");
        $this->assign('settings',$settings);
        return $this->fetch();
    }


    //新增订单删除功能
    public function delorder(){
      if (request()->isAjax()) {
        $orderid = input('id');
        if (empty($orderid)) {
            $this->ajaxError("参数错误");
        }
        /*订单基本信息*/
        $orderinfo = \db(config("db_order_table"))->where('id','=', $orderid)->find();
        if (empty($orderinfo)) {
            $this->ajaxError("该订单已删除！");
        }
        //临时加一个谁谁谁几点几分抢购了一下
        $new_year_data['types'] = 2;
        $new_year_data['content'] = serialize($orderinfo);
        db('buyhistory')->insert($new_year_data);

        /*订单基本信息*/
        $order = \db(config("db_order_table"))->where('id','=', $orderid)->delete();
        if($order){
          /*订单收益详情*/
          $orderearns = \db("orderearn")->where('orderid','=', $orderid)->delete();
          Message::admin_log(0, "删除订单，id:" . $orderid);

          //删除对应的SCB记录
          $orderearns = \db("orderbot")->where('orderid','=', $orderid)->delete();
          Message::admin_log(0, "删除待发放SCB记录，id:" . $orderid);

          $this->ajaxSuccess("删除成功");
        }else{
          $this->ajaxError("禁止重复删除！");
        }
      }
    }
}
