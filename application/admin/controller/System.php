<?php
/**
 * Created by PhpStorm.
 *
 * Date: 2018/8/19
 * Time: 上午9:23
 */

namespace app\admin\controller;

use app\common\model\Message;

class System extends Common
{
    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 系统参数
     */
    public function setConfig()
    {
        if (request()->isGet()) {
            $data = config("setting");
            $this->assign("data", $data);
            $this->assign("single_image", ["logo" => isset($data["logo"]) ? $data["logo"] : '', "wechatpic" => isset($data["wechatpic"]) ? $data["wechatpic"] : '', "alipaytpic" => isset($data["alipaytpic"]) ? $data["alipaytpic"] : '', "pay_code" => isset($data["pay_code"]) ? $data["pay_code"] : '']);
            return $this->fetch("setting");
        } else {
            $data = input("post.");
            $string = "<?php \t return " . strip_whitespace(var_export($data, true)) . "; ?>";
            file_put_contents(APP_PATH . "/extra/setting.php", $string);
            Message::admin_log(0, "修改配置文件");
            $this->ajaxSuccess('编辑成功！');
        }
    }

    /**
     * 短信发送记录
     */
    public function smsLog()
    {
        $db = db("sms_log");
        $param = input("get.");
        if (isset($param["telephone"]) && $param["telephone"] != '') {
            $db->where("telephone", $param["telephone"]);
        }
        if(isset($param['sendtel']) && $param['sendtel']!='' && isset($param['codebtn'])){
          //发送验证码
          $ary = model('Jhsms')->set_phone($param['sendtel'],1);
        }
        if(isset($param['sendtel']) && $param['sendtel']!='' && isset($param['sucbtn'])){
          //发送订单成功通知
          $ary = model('Jhsms')->set_phone($param['sendtel'],2);
        }
        //如果有短信就返回一下接收信息
        if(!empty($ary)){
          echo '<pre>';print_r($ary);
          echo "<a href='javascript:history.back();'>Back</a>";
          exit;
        }

        $this->assign("param", $param);
        $list = $db->paginate(20, false, ["query" => $param]);
        $this->assign("list", $list);
        return $this->fetch("sms_log");
    }
    //查看短信余额和短信频繁发送的禁止号码
    public function smssendlog(){
      header('Content-type:text/html;charset=utf-8');
      $id = input('types');
      if($id==1 || $id==2){
          $get = model('Jhsms')->mysms($id);
      }else{
        echo 'error!!';exit;
      }
    }

    /**
     * 后台操作日志
     */
    public function operaLog()
    {
        $db = db("admin_log");
        $list = $db->order("create_time desc")->paginate(20);
        $this->assign("list", $list);
        return $this->fetch("opera_log");
    }

    /**
     * 会员登录日志
     */
    public function loginLog()
    {
        $this->assign("leixin", [
            "1"=>"成功",
            "2"=>"密码错误",
            "3"=>"验证码错误"
        ]);
        $db =db('userlogin');
        $param = input("get.");
        if(isset($param['telephone']) && $param['telephone']!=''){
            $db->where("user", $param["telephone"]);
        }
        $list = $db->order("id desc")->paginate(20, false, ["query"=>$param]);
        $this->assign("list", $list);
        return $this->fetch("user_login_log");
    }

}
