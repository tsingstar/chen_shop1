<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2017/1/11
 * Time: 下午9:28
 */

namespace app\admin\controller;

use app\common\model\Storage;
use think\Db;

class Utils extends Common
{
    protected function _initialize()
    {
        parent::_initialize();
        if (!session("?H_ADMIN_LOGIN_USER") || session("H_ADMIN_DUE_TIME") < time()) {
            session("H_ADMIN_REFERER", url("Index/index"));
            //判断是否为ajax操作
            if (request()->isAjax()) {
                die(json_encode(array("status" => -1, "url" => url("Login/index"))));
            } else {
                die("<script>if (window.parent){window.parent.location.href='"
                    . url("Login/index") . "';}else{location.href='"
                    . url("Login/index") . "';}</script>");
            }
        }
    }

    /**
     * 基本操作
     */
    public function opera()
    {
        return $this->fetch("index");
    }


    //清除系统缓存
    public function clearCache()
    {
        //清除缓存
        clear_cache();

        //清除缓存成功
        $this->ajaxSuccess("清除缓存成功");
    }

    //上传单图片
    public function uploadSingleImage()
    {
        $storage = new Storage();
        if (request()->isAjax()) {
            //获取上传图片的名称
            $imageName = request()->param("name");
            $result = $storage->upload($imageName, ['ext' => config('img_ext')]);

            if ($result['status'] == 0) {
                return array("status" => 0, "url" => $result['data']);
            } else {
                return array("error" => $result['info']);
            }
        }
    }

    public function uploadSingleVideo()
    {
        $storage = new Storage();
        //bootstrap-fileinput
        if (request()->isAjax()) {
            //获取上传图片的名称
            $imageName = request()->param("name");

            $result = $storage->upload($imageName, ['ext' => config('videos_ext')]);

            if ($result['status'] == 0) {
                return array("status" => 0, "url" => $result['data']);
            } else {
                return array("error" => $result['info']);
            }
        }
    }//获取地区

    public function queryRegion()
    {
        //仅支持AJAX操作
        if (request()->isAjax()) {
            //上级地区ID
            $parentId = request()->param("parent_id");

            //获取地区列表
            $list = Db::name("region")
                ->where("parent_id", $parentId)
                ->where('is_visible', 1)
                ->order("order_num desc")
                ->field("id,title as name")
                ->select();

            return $list;
        }
    }

    //获取分类
    public function queryCategory()
    {
        //仅支持AJAX操作
        if (request()->isAjax()) {
            //上级地区ID
            $parentId = request()->param("parent_id");

            //获取地区列表
            $list = Db::name("category")
                ->where("parent_id", $parentId)
                ->where('is_valid', 1)
                ->order("display_order DESC,id DESC")
                ->field("id,title")
                ->select();

            return $list;
        }
    }

}