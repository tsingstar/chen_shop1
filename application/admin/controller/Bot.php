<?php
/**
 * Created by Editplus
 * xuanfeng
 * Date: 2020/08/04
 * Time: 17:14
 */

namespace app\admin\controller;

use app\admin\model\Wallet;
use app\common\model\Commission;
use app\common\model\Member;
use app\common\model\Message;
use think\Exception;
use think\Db;

class Bot extends Common{

	/*xuanfeng 未到期订单的应发SCB xuanfeng 17:14*/
	public function index(){
		$where['us.id'] = ['gt', 0];
	    //搜索条件-真实姓名
	    $realname = request()->param("realname");
	    $this->assign("realname", $realname);
	    if ($realname) {
		    $uid = get_uid($realname);
		    $where["us.uid"] = ['in', $uid];
	    }

		$state = request()->param("state");
	    $this->assign("state", $state);
	    if ($state) {
		    $where["us.state"] = intval($state);
	    }

	    //获取所有用户
	    $list = Db::name("orderbot")
		    ->alias('us')
		    ->where($where)
		    ->order("us.id DESC")
		    ->paginate(18);
	    $page = $list->render();
	    $list2=$list->all();
	    if (! empty($list2)) {
		    foreach ($list as $k => $v) {
				$list2[$k]['goodname'] = db("goods")->where("id='".$v['proid']."'")->value('title');
		    }
	    }
	    $this->assign("list", $list2);
	    $this->assign("page", $page);
	    return $this->fetch();
	}



    /*xuanfeng 用户拥有的用SCB换的矿机数量 xuanfeng 2020-08-04 22:45*/
	public function shoplist(){
		$where['us.id'] = ['gt', 0];
	    //搜索条件-真实姓名
	    $realname = request()->param("realname");
	    $this->assign("realname", $realname);
	    if ($realname) {
		    $uid = get_uid($realname);
		    $where["us.uid"] = ['in', $uid];
	    }

        $id = request()->param("id");
	    $this->assign("id", $id);
	    if ($id) {
		    $where["us.id"] = $id;
	    }

		$state = request()->param("state");
	    $this->assign("state", $state);
	    if ($state==1) {
		    $shopmy_model = Db::name("shopmy")->alias('us')->where($where)->where('daynum>usedaynum');
	    }else if ($state==2) {
		    $shopmy_model = Db::name("shopmy")->alias('us')->where($where)->where('daynum=usedaynum');
	    }else{
            $shopmy_model = Db::name("shopmy")->alias('us')->where($where);
        }

	    //获取所有用户

	    $list = $shopmy_model->order("us.id DESC")->paginate(8);
	    $page = $list->render();
	    $list2=$list->all();
	    if (! empty($list2)) {
		    foreach ($list as $k => $v) {
				//$list2[$k]['goodname'] = db("goods")->where("id='".$v['proid']."'")->value('title');
		    }
	    }
	    $this->assign("list", $list2);
	    $this->assign("page", $page);
	    return $this->fetch();
	}





    /*xuanfeng 矿机收益情况 xuanfeng 17:14*/
	public function shopbot(){
		$where['us.id'] = ['gt', 0];
	    //搜索条件-真实姓名
	    $realname = request()->param("realname");
	    $this->assign("realname", $realname);
	    if ($realname) {
		    $uid = get_uid($realname);
		    $where["us.uid"] = ['in', $uid];
	    }

		$state = request()->param("state");
	    $this->assign("state", $state);
	    if ($state) {
		    $where["us.state"] = intval($state);
	    }

        $shopid = request()->param("shopid");
	    $this->assign("shopid", $shopid);
	    if ($shopid) {
		    $where["us.shopid"] = $shopid;
	    }


	    //获取所有用户
	    $list = Db::name("shopmylist")
		    ->alias('us')
		    ->where($where)
		    ->order("us.id DESC")
		    ->paginate(18);
	    $page = $list->render();
	    $list2=$list->all();
	    if (! empty($list2)) {
		    foreach ($list2 as $k => $v) {
				$list2[$k]['title'] = db("shopmy")->where("id='".$v['shopid']."'")->value('title');
                //echo db()->getlastsql();exit;
		    }
	    }
	    $this->assign("list", $list2);
	    $this->assign("page", $page);
	    return $this->fetch();
	}


	/*单独给某个人的某条记录发放SCB值记录*/
	public function sendone(){
		ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');

        $id = input('id');
        //开始生成每日的日期
        $datestr = date('Y-m-d');
        //得到开始时间
        $firstday = strtotime($datestr);

        $v = db('shopmylist')->where("id='".$id."' and state=1")->limit(50)->find();
        if(empty($v) || empty($id)){
        	$this->ajaxError("非法操作！");
        }
        //print_R($v);exit;
        $shopinfo = db('shopmy')->where("id='".$v['shopid']."'")->find();

        //先给用户mybot列的余额加上相应数量
        db('user')->where("id='".intval($v['uid'])."'")->setInc('mybot',$v['dayearn']);
        //再给相应用户加上mybot历史
        mybot_his($v['uid'],1,$v['dayearn'],0,'矿机“'.$shopinfo['title'].'”每日收益');
        db('shopmylist')->where("id='".$v['id']."'")->update(['state'=>2,'edittime'=>time()]);
        usleep(20000);
        //修改矿机信息：减去
        $shopdata['usedaynum'] = ($shopinfo['usedaynum']+1);
        $shopdata['useearn']= ($shopinfo['useearn']+$v['dayearn']);
        db('shopmy')->where("id='".$v['shopid']."'")->update($shopdata);
        Message::admin_log(0, "强制发放SCB矿机收益，id:" . $v['shopid']);
        $this->ajaxSuccess("发放成功");
	}

}
