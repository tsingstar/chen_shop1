<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2018/8/17
 * Time: 下午9:27
 */
namespace app\admin\controller;

use think\Controller;
use think\Session;
use think\Db;
class Login extends Controller
{
	public function index()
	{
		if (request()->isGet())
		{
			//输出标题
			$this->assign("templateTitle", config('system_title'));

			//判断是否登录，若登录则跳转到首页
			if (session("?H_ADMIN_LOGIN_USER") && session("H_ADMIN_DUE_TIME") > time())
			{
				$this->redirect("Index/index");
			}
			//输出上次链接
			$this->assign("refer", session("H_ADMIN_REFERER"));

			return $this->fetch("admin/login");
		}

		if (request()->isAjax())
		{
			//验证码
			$captcha = request()->param("captcha");
			if (empty($captcha))
			{
				$this->ajaxError("验证码不能为空！");
			}

			//验证图形验证码
			if (!captcha_check($captcha))
			{
				$this->ajaxError('验证码有误');
			}

			//用户名
			$username = request()->param("username");
			if (empty($username))
			{
				$this->ajaxError('请输入用户名');
			}

			//密码
			$password = request()->param("password");
			if (empty($password))
			{
				$this->ajaxError('请输入登录密码');
			}

			//获取管理员
			$admin = Db::name("admin_user")
				->alias("au")
				->join("__ADMIN_ROLE__ ar", "ar.id=au.role_id", "LEFT")
				->where("au.is_valid", 1)
				->where("au.username", $username)
				->field("au.*,ar.authority")
				->find();

			//判断管理员是否存在
			if (empty($admin))
			{
				$this->ajaxError('用户名或密码错误.');
			}
			//验证密码
			if (md5($admin["salt"] . $password . $admin["salt"]) != $admin["password"])
			{
				$this->ajaxError('用户名或密码错误。');
			}

			//清空缓存数据-登录先不清缓存
			//clear_cache();

			//处理管理员权限
			$admin["authority"] = unserialize($admin["authority"]);

			//保存登录管理员信息
			session("H_ADMIN_LOGIN_USER", $admin);
			//保存登录时间信息
			session("H_ADMIN_DUE_TIME", time() + 12 * 60 * 60);

			return array(
				"status" => 0,
				"info" => '登录成功',
			);
		}
	}

	//退出登录
	public function logout()
	{
		//清除登录信息
		Session::clear();

		$this->redirect("Login/index");
	}

	/**
	 * ajax失败信息
	 *
	 * @param string $info 信息内容
	 * @param array $extra 额外数据
	 */
	private function ajaxError($info = "", $extra = array())
	{
		$result = array(
			"status"=>1,
			"info" => $info,
			"data" => $extra,
		);

		die(json_encode($result, JSON_UNESCAPED_UNICODE));
	}

}
