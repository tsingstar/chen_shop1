<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/8/2
 * Time: 9:37
 */

namespace app\admin\controller;


use Think\Db;

class Database extends Common
{
    protected function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        $Db    = Db::getInstance();
        $list  = $Db->query('SHOW TABLE STATUS');
        $list  = array_map('array_change_key_case', $list);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 优化表
     * @param  String $tables 表名
     */
    public function optimize($tables = null){
        if($tables) {
            $Db   = Db::getInstance();
            if(is_array($tables)){
                $tables = implode('`,`', $tables);
                $list = $Db->query("OPTIMIZE TABLE `{$tables}`");

                if($list){
                    $info = $_SESSION['bk']['uname'].'数据表优化完成';
                    $this->success("数据表".$tables."优化完成！");
                } else {
                    $info = $_SESSION['bk']['uname'].'数据表优化出错请重试！';
                    $this->error("数据表优化出错请重试！");
                }
            } else {
                $list = $Db->query("OPTIMIZE TABLE `{$tables}`");
                if($list){
                    $info = $_SESSION['bk']['uname'].'数据表优化完成';
                    $this->success("数据表'{$tables}'优化完成！");
                } else {
                    $info = $_SESSION['bk']['uname'].'数据表优化出错请重试！';
                    $this->error("数据表'{$tables}'优化出错请重试！");
                }
            }
        } else {
            $this->error("请指定要优化的表！");
        }
    }

    /**
     * 修复表
     * @param  String $tables 表名
     */
    public function repair($tables = null){
        if($tables) {
            $Db   = Db::getInstance();
            if(is_array($tables)){
                $tables = implode('`,`', $tables);
                $list = $Db->query("REPAIR TABLE `{$tables}`");

                if($list){
                    $info = $_SESSION['bk']['uname'].'数据表修复完成';
                    $this->success("数据表".$tables."修复完成！");
                } else {
                    $info = $_SESSION['bk']['uname'].'数据表修复失败';
                    $this->error("数据表修复出错请重试！");
                }
            } else {
                $list = $Db->query("REPAIR TABLE `{$tables}`");
                if($list){
                    $info = $_SESSION['bk']['uname'].'数据表修复完成';
                    $this->success("数据表'{$tables}'修复完成！");
                } else {
                    $info = $_SESSION['bk']['uname'].'数据表修复失败';
                    $this->error("数据表'{$tables}'修复出错请重试！");
                }
            }
        }else {
            $this->error("请指定要修复的表！");
        }
    }

    public function export($tables = null){
        set_time_limit(0);
        ini_set('memory_limit','80M');
        if($tables){
            if(is_array($tables)){
                $arr = $tables;
            }else{
                $arr[] = $tables;
            }
        }else {
            $this->error("请指定要备份的表！");
        }
        $mysql = "";
        foreach($arr as $v){
            $sql=M()->query("show create table `$v`");
            $mysql.=$sql[0]['create table'].";\r\n";
            $newtab = explode("ot_",$v);
            $q3=M($newtab[1])->select();
            foreach($q3 as $data){
                $keys=array_keys($data);
                $keys=array_map('addslashes',$keys);
                $keys=join('`,`',$keys);
                $keys="`".$keys."`";
                $vals=array_values($data);
                $vals=array_map('addslashes',$vals);
                $vals=join("','",$vals);
                $vals="'".$vals."'";
                $mysql.="insert into `$table`($keys) values($vals);\r\n";
            }
        }
        $filename="./Public/database/".date('Ymd-His').".zip";
        $file = fopen($filename,"w");
        if(!$file){
            $info = $_SESSION['bk']['uname'].'数据备份失败';
            $this->error("备份失败！！",U("Database/index"));
            exit;
        }
        fwrite($file,$mysql);
        fclose($file);

        $tables = implode('`,`', $arr);
        $info = $_SESSION['bk']['uname'].'数据表备份成功';
        $this->success("数据表备份成功！！！！",U("Database/index"));

    }

    /*--下载备份文件--*/
    public function down(){
        $bname = "./Public/database/";
        $filesnames = scandir($bname);
        $dir = array();

        foreach($filesnames as $k => $v){
            if($v=='.' || $v=='..'){

            }else{
                $dir[$k]['name'] = $v;
                $dir[$k]['ctime'] = filectime($bname.$v);
            }
        }
        $this->assign('list',$dir);
        $this->display();
    }
    /*删除备份*/
    public function delete(){
        $bname = "./Public/database/";
        $getname = urldecode(I("get.name"));
        $name = $bname.$getname;
        if(unlink($name)){
            $info = $_SESSION['bk']['uname'].'删除备份成功，名称为：'.$getname;
            $this->success("网站备份删除成功！",U("Database/down"));
        }else{
            $info = $_SESSION['bk']['uname'].'删除备份成功，名称为：'.$getname;
            $this->error("网站备份删除失败！",U("Database/down"));
        }
    }

}