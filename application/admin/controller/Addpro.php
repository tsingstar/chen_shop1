<?php
/**
 * Created by PhpStorm.
 * User: 旋风
 * Date: 2020/8/1
 * Time: 下午07:44
 */

namespace app\admin\controller;

use think\Db;

class Addpro extends Common
{

    //订单列表
    public function index(){
		$model = db("goods");
        $list = $model->order("sort desc")->paginate(10);
        $this->assign("list", $list);
        return $this->fetch("goods");	
	}

}
