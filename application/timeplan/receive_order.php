<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2020/5/13
 * Time: 8:46
 */

require_once __DIR__ . '/_.php';
$db = link_db();
$redis = get_redis();
if ($redis->get("sure_order_status")) {
    echo "进行中。。。";
    exit();
} else {
    $redis->set("sure_order_status", 1);
    header("Content-type:text/html;charset=utf-8");
    ignore_user_abort(true);
    set_time_limit(0);
    ini_set('memory_limit', '256M');
    date_default_timezone_set('Asia/Shanghai');
    //查询当前时间下符合需要处理的抢购订单
    $list = select('select id, uid, proid, id,title, delpoint, point_status from sxs_apply where returntime<='.time().' and status=0 order by point_status desc,rank ', $db);
    $start_time = time();
    while (count($list) > 0) {
        foreach ($list as $item) {
            if (time() - $start_time > 55) {
                $redis->del('sure_order_status');
                exit();
            }
            $userinfo = find("select * from sxs_user where id=".$item['uid'], $db);
            $product_con = find("select * from sxs_goods where id=".$item['proid'], $db);
            /*首先看一下这个时间段有没有订单，没有的话就不要让买了*/
            $price_start = $product_con['price1'];
            $price_end = $product_con['price2'];
            //判断预约记录是否已经指定过，如果指定过直接匹配指定的订单
            if($item['point_status'] == 2){
                //根据预约记录id 去订单表查询待匹配的订单
                $order_t = find("select * from sxs_qfbaboziex where applyid=".$item['id'], $db);
            }else{
                //查一下可配对的单子有多少，但是不能包含自己买的单子
                $order_have_map = "basicprice>'" . $price_start . "' and basicprice<='" . $price_end . "' and peiduistatus=1 and buyuid<>'" . $userinfo['id'] . "'";
                $order_have_map = $order_have_map . " and cansaletime<='" . time() . "' ";
                $order_t = find('select * from sxs_qfbaboziex where '.$order_have_map.' order by id asc', $db);
            }
            //没有匹配的订单，直接返回给客户，告诉客户额度没了
            if (empty($order_t)) {
                /*先把预约的酒滴退回去，再把这个预约状态改成退还*/
                /*新增预约失败，并退还*/
                $newdata = $item;
                $newdata['status'] = 2;
                update("update sxs_apply set status=2 where id=".$item['id'], $db);
                $newdata['title'] = $item['title'] . '-抢购失败退还';
                unset($newdata['id']);
                $newdata['isadd'] = 1;
                $newdata['ctime'] = time();
                insertGetId('sxs_apply', $newdata, $db);
                //先把会员的油卡加回去
                update('update sxs_user set integral=integral+'.$item['delpoint']." where id=".$userinfo['id'], $db);
                //再把会员的酒滴历史加上
                points_his($db, $userinfo['id'], 1, $item['delpoint'], 0, $item['title'] . '-抢购失败退还');
            } else {
                $order_have = $order_t['id'];
                /*如果有符合条件的订单并且客户购买，判断一下用户是否预约，把预约的扣除酒滴确定下来*/
                //确定酒滴扣除，不返还
                $newdata['status'] = 1;
                update("update sxs_apply set status=1 where id=".$item['id'], $db);
                //已预约扣除多少酒滴
                $data_sure['caifen'] = $product_con['point_start'];
                //下单并绑定uid,然后获取这条订单的所有信息，价格交给下个新订单，并生成收益列表
                $bind_order['peiduiuid'] = $userinfo['id'];
                $bind_order['peiduistatus'] = 2;
                $bind_order['peiduitime'] = time();
                $bind_order["match_pro_id"] = $product_con["id"];
                $bind_order["match_pro_title"] = $product_con["title"];
                //获取订单所有信息
                $that_order_one = find('select * from sxs_qfbaboziex where id= '.$order_have, $db);
                $data_sure['price'] = $that_order_one['basicprice'];
                $data_sure['peiduiid'] = $that_order_one['id'];
                if (true) {
                    //配对完成给双方发短信,暂未处理是否开启
                    //买家收短信 $userinfo['selfphone'];
                    $jh_sms = new Sms();
                    $jh_sms->send_order_tips($userinfo['selfphone'], $db);
                    //卖家收短信
                    $phone_buy_t = find('select * from sxs_user where id='.$that_order_one['buyuid'], $db);
                    $phone_buy = $phone_buy_t["selfphone"];
                    $jh_sms->send_order_tips($phone_buy, $db);
                }
                // 然后再新增一个order数据
                $data_sure['buyuid'] = $userinfo['id'];
                $data_sure['productid'] = $product_con['id'];
                $data_sure['title'] = $product_con['title'];
                $data_sure['daynum'] = $product_con['daynum'];
                $data_sure['earnpercent'] = $product_con['daypercent'];
                //一共能赚多少钱，本金加赚的钱有多少
                $countprofit = countprofit($data_sure['price'], $data_sure['daynum'], $data_sure['earnpercent']);
                $data_sure['buyuidatc'] = $product_con['atcnum'];
                $data_sure['dayaddprice'] = $countprofit['profitall'];
                $data_sure['basicprice'] = $countprofit['profit_and_day'];
                $data_sure['ctime'] = time();
                $data_sure['peiduistatus'] = 1;
                $data_sure['peiduiuid'] = 0;
                $data_sure['paystatus'] = 0;
                $data_sure['paytime'] = 0;
                $data_sure['payimg'] = '';
                $cansaletime = ($data_sure['ctime'] + 86400 * $product_con['daynum']);
                $cansaletimedate = date('Y-m-d', $cansaletime);
                $data_sure['cansaletime'] = strtotime($cansaletimedate);
                $now_day_dates = date('Y-m-d');
                $data_sure['thatdatetime'] = strtotime($now_day_dates);
                //先新增order表，再新增利润表
                $order_new_id = insertGetId('sxs_qfbaboziex', $data_sure, $db);
                $bind_order["match_oid"] = $order_new_id;
                $upstr = '';
                foreach ($bind_order as $ko=>$bo){
                    $upstr .= $ko.'="'.$bo.'",';
                }
                $upstr = trim($upstr, ',');
                update('update sxs_qfbaboziex set '.$upstr.' where id='.$order_have, $db);
                $insert_detail = [];
                for ($i = 1; $i <= $product_con['daynum']; $i++) {
                    $data_detail['orderid'] = $order_new_id;
                    $data_detail['uid'] = $userinfo['id'];
                    $data_detail['earnmoney'] = $countprofit['profitday'];
                    $detail_time = ($data_sure['thatdatetime'] + 86400 * $i);
                    $data_detail['dotime'] = date('Ymd', $detail_time);
                    $data_detail['status'] = 1;
                    $data_detail['ctime'] = time();
                    $insert_detail[] = $data_detail;
                }
                if (!empty($insert_detail)) {
                    insertAll('sxs_orderearn', $insert_detail, $db);
                }
            }
        }
        $list = [];
    }
    echo("处理完成");
    $redis->del("sure_order_status");
    exit;
}


