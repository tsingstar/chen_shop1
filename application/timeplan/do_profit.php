<?php
/**
 * 订单发放收益，每天每七分钟执行一次好了
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2020/5/15
 * Time: 8:51
 */

include_once __DIR__.'/_.php';
$db = link_db();
ob_start();
header("Content-type:text/html;charset=utf-8");
ini_set('memory_limit', '88M');
ini_set('max_execution_time', '0');
date_default_timezone_set('Asia/Shanghai');
$config = include __DIR__.'/../extra/setting.php';
$list = select('select * from sxs_orderearn where dotime<="'.date('Ymd').'" and status=1 limit 500', $db);
/*如果空数据就不操作*/
if (empty($list)) {
    echo '收益方法结束';
    exit;
}
foreach ($list as $k => $v) {
    $new_data['status'] = 2;
    $new_data['sendtime'] = time();
    /*ATC发放完成后，再发放各方的奖金，先发放三级分销的*/
    $price_basic = $v['earnmoney'];
    //个人收益添加到个人的用户表
    update('update sxs_user set myprofit=myprofit+'.$v['earnmoney'].' where id='.$v['uid'], $db);
    //读取当前人的信息
    if(empty($v['uid'])){
        $thisuser = [];
    }else{
        $thisuser = find("select * from sxs_user where id=".$v['uid'], $db);
        $thisuser = empty($thisuser)?[]:$thisuser;
    }
    update('update sxs_orderearn set status=2,sendtime='.time().' where id='.$v['id'], $db);
    /*开始发放一二三三级的奖金*/
    //first_user   霸道的幸福37，爱的港湾117，初遇215
    if (!empty($thisuser['pid'])) {
        if(empty($v['uid'])){
            $parent_first = [];
        }else{
            $parent_first = find("select * from sxs_user where id=".$thisuser['pid'], $db);
            $parent_first = empty($parent_first)?[]:$parent_first;
        }
        $send_earn = ($price_basic * $config['parent_first'] / 100);
        $send_earn = round($send_earn, 2);
        if ($send_earn > 0) {
            //发放奖金
            update('update sxs_user set pushs=pushs+'.$send_earn.' where id='.$thisuser['pid'],$db);
            $data_t1['uid'] = intval($thisuser['pid']);
            $data_t1['isadd']= 1;
            $data_t1['nums'] = $send_earn;
            $data_t1['fromuid'] = 0;
            $data_t1['ctime']	 = time();
//            $data_t1['info']	 = '一级下级' . $thisuser['username'] . '订单成交';
            $data_t1['info']	 = '一级下级订单收益';
            insertGetId('sxs_pushslist', $data_t1, $db);
        }
    }
    //secode_user，是否有二级上级，有就发放奖金
    if (!empty($parent_first['pid'])) {
        if(empty($v['uid'])){
            $parent_second = [];
        }else{
            $parent_second = find("select * from sxs_user where id=".$parent_first['pid'], $db);
            $parent_second = empty($parent_second)?[]:$parent_second;
        }
        $send_earn = ($price_basic * $config['parent_second'] / 100);
        $send_earn = round($send_earn, 2);
        if ($send_earn > 0) {
            //发放奖金
            update('update sxs_user set pushs=pushs+'.$send_earn.' where id='.$parent_first['pid'],$db);
            $data_t2['uid'] = intval($parent_first['pid']);
            $data_t2['isadd']= 1;
            $data_t2['nums'] = $send_earn;
            $data_t2['fromuid'] = 0;
            $data_t2['ctime']	 = time();
//            $data_t2['info']	 = '二级下级' . $thisuser['username'] . '订单成交';
            $data_t2['info']	 = '二级下级订单收益';
            insertGetId('sxs_pushslist', $data_t2, $db);
        }
    }
    //third_user，是否有三级上级，有就发放奖金
    if (!empty($parent_second['pid'])) {
        $send_earn = ($price_basic * $config('parent_third') / 100);
        $send_earn = round($send_earn, 2);
        if ($send_earn > 0) {
            //发放奖金
            update('update sxs_user set pushs=pushs+'.$send_earn.' where id='.$parent_second['pid'],$db);
            $data_t3['uid'] = intval($parent_second['pid']);
            $data_t3['isadd']= 1;
            $data_t3['nums'] = $send_earn;
            $data_t3['fromuid'] = 0;
            $data_t3['ctime']	 = time();
//            $data_t3['info']	 = '三级下级' . $thisuser['username'] . '订单成交';
            $data_t3['info']	 = '三级下级订单收益';
            insertGetId('sxs_pushslist', $data_t3, $db);
        }
    }
}
echo '发放收益完成';
exit;