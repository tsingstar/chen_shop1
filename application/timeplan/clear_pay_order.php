<?php
/**
 * 支付时间过期，撤销配对收益记录，清空配对信息  每分钟执行一次
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2020/5/15
 * Time: 8:17
 */
include_once __DIR__.'/_.php';
$db = link_db();
ob_start();
header("Content-type:text/html;charset=utf-8");
ini_set('memory_limit', '88M');
ini_set('max_execution_time', '0');
date_default_timezone_set('Asia/Shanghai');
$config = include __DIR__.'/../extra/setting.php';
//查询过期未匹配订单
$list = select('select * from sxs_qfbaboziex where peiduistatus=2 and peiduitime<'.(time()-$config["pay_limit_time"]*60).' and paystatus=0 order by id desc', $db);
if (!empty($list)) {
    foreach ($list as $v) {
        //获取卖出订单id
        $sale_id = $v["match_oid"];
        //清除收益记录
        delete('delete from sxs_orderearn where orderid='.$sale_id, $db);
        //重置订单配对状态
        $updata = [
            "peiduistatus"=>1,
            "peiduiuid"=>0,
            "peiduitime"=>"",
            "match_pro_id"=>0,
            "match_pro_title"=>0,
            "match_oid"=>0,
            "paystatus"=>0,
            "paytype"=>"",
            "paytime"=>0,
            "payimg"=>""
        ];
        $upstr = '';
        foreach ($updata as $ko=>$bo){
            $upstr .= $ko.'="'.$bo.'",';
        }
        $upstr = trim($upstr, ',');
        update('update sxs_qfbaboziex set '.$upstr.' where id='.$v["id"], $db);
        $cancel_order = find("select * from sxs_qfbaboziex where id=".$sale_id, $db);
        if(!$cancel_order){
            unset($cancel_order['id']);
            insertGetId('order_cancel', $cancel_order, $db);
        }
        delete('delete from sxs_qfbaboziex where id='.$sale_id, $db);
    }
}
echo '超时未付款结算完成';
exit;