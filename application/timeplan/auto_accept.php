<?php
/**
 * 已经支付超时未确认订单，自动确认
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2020/5/15
 * Time: 8:30
 */

include_once __DIR__.'/_.php';
$db = link_db();
ob_start();
header("Content-type:text/html;charset=utf-8");
ini_set('memory_limit', '88M');
ini_set('max_execution_time', '0');
date_default_timezone_set('Asia/Shanghai');
$config = include __DIR__.'/../extra/setting.php';
$timstamp = (time() - $config['sure_limit_time'] * 60);
$list = select("select * from sxs_qfbaboziex where peiduistatus=2 and paystatus=1 and paytime<" . $timstamp." order by id desc", $db);
if (!empty($list)) {
    foreach ($list as $v) {
        //执行定时收款确认即可
        $id = $v['id'];
        /*如果没有传过来订单信息，那就直接读取订单信息*/
        $orderinfo = find('select * from sxs_qfbaboziex where id='.$id.' and paystatus=1', $db);
        $data['paystatus'] = 2;
        $s = update('update sxs_qfbaboziex set paystatus=2 where id='.$id, $db);
        if($s){
            //让对应的那个订单可以出售--xuanfeng--2020-03-15
            $dataxs['xs'] = 2;
            update('update sxs_qfbaboziex set xs=2 where id='.$orderinfo['match_oid'], $db);
            /*发放各方的奖金，先发放三级分销的*/
            $price_basic = $orderinfo['dayaddprice'];
            /*是否有一级上级*/
            if(empty($orderinfo['buyuid'])){
                $thisuser = [];
            }else{
                $thisuser = find("select * from sxs_user where id=".$orderinfo['buyuid'], $db);
                $thisuser = empty($thisuser)?[]:$thisuser;
            }
            //查出本人的所有上级（不包含自己），按照一级
            //论功行赏，如果levelid=2,奖励$price_basic的1%   如果是3 奖励 2%    如果是3 奖励3%  如果是1 自动忽略团队奖
            $user_upary = excommend($thisuser['recommend']);
            $is_send_money = array();
            if(!empty($user_upary)){
                $reverse_up = array_reverse($user_upary);
                foreach($reverse_up as $v1){
                    if($v1==$thisuser['id']){
                        continue;
                    }
                    /*查询这个人是什么等级，如果是1级就不发*/
                    $my_level = find('select id, levelid from sxs_user where id='.$v1, $db);
                    if($my_level['levelid']>1){
                        if(in_array($my_level['levelid'],$is_send_money)){
                            continue;//如果该级别发了，那就不继续发了
                        }
                        if($my_level['levelid']==2){
                            $can_have_price = round(($orderinfo['dayaddprice']*0.01),2);
                        }
                        if($my_level['levelid']==3){
                            $can_have_price = round(($orderinfo['dayaddprice']*0.03),2);
                        }
                        if($my_level['levelid']==4){
                            $can_have_price = round(($orderinfo['dayaddprice']*0.06),2);
                        }
                        $is_send_money[] = $my_level['levelid'];
                        //开始发奖，记录下来发奖的历史就行了
                        //发放奖金，平级不拿团队奖
                        update('update sxs_user set wagesall=wagesall+'.$can_have_price.' where id='.$v1, $db);
                        $data_t['uid'] = intval($v1);
                        $data_t['isadd']= 1;
                        $data_t['nums'] = $can_have_price;
                        $data_t['fromuid'] = 0;
                        $data_t['ctime']	 = time();
//                        $data_t['info']	 = '团队奖[下级'.$thisuser['username'].'订单成交]';
                        $data_t['info']	 = '团队奖[下级订单成交]';
                        insertGetId('sxs_wages', $data_t, $db);
                    }
                }
            }
        }
    }
}
echo '自动收款结束成功';
exit;