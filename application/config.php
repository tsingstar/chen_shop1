<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    // +----------------------------------------------------------------------
    // | 应用设置
    // +----------------------------------------------------------------------

    // 应用命名空间
    'app_namespace' => 'app',
    // 应用调试模式
    'app_debug' => true,
    // 应用Trace
    'app_trace' => true,
    // 应用模式状态
    'app_status' => '',
    // 是否支持多模块
    'app_multi_module' => true,
    // 入口自动绑定模块
    'auto_bind_module' => false,
    // 注册的根命名空间
    'root_namespace' => [],
    // 扩展配置文件
    'extra_config_list' => ['database', 'validate'],
    // 扩展函数文件
    'extra_file_list' => [THINK_PATH . 'helper' . EXT],
    // 默认输出类型
    'default_return_type' => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return' => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler' => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler' => 'callback',
    // 默认时区
    'default_timezone' => 'PRC',
    // 是否开启多语言
    'lang_switch_on' => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter' => '',
    // 默认语言
    'default_lang' => 'zh-cn',
    // 应用类库后缀
    'class_suffix' => false,
    // 控制器类后缀
    'controller_suffix' => false,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module' => 'Home',
    // 禁止访问模块
    'deny_module_list' => ['common'],
    // 默认控制器名
    'default_controller' => 'Index',
    // 默认操作名
    'default_action' => 'index',
    // 默认验证器
    'default_validate' => '',
    // 默认的空控制器名
    'empty_controller' => 'Error',
    // 操作方法后缀
    'action_suffix' => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo' => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch' => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr' => '/',
    // URL伪静态后缀
    'url_html_suffix' => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param' => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type' => 0,
    // 是否开启路由
    'url_route_on' => true,
    // 路由配置文件（支持配置多个）
    'route_config_file' => ['route'],
    // 是否强制使用路由
    'url_route_must' => false,
    // 域名部署
    'url_domain_deploy' => false,
    // 域名根，如.thinkphp.cn
    'url_domain_root' => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert' => false,
    // 默认的访问控制器层
    'url_controller_layer' => 'controller',
    // 表单请求类型伪装变量
    'var_method' => '_method',

    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------

    'template' => [
        // 模板引擎类型 支持 php think 支持扩展
        'type' => 'Think',
        // 模板路径
        'view_path' => THINK_PATH . 'tpl/',
        // 模板后缀
        'view_suffix' => 'html',
        // 模板文件名分隔符
        'view_depr' => '/',
        // 模板引擎普通标签开始标记
        'tpl_begin' => '{',
        // 模板引擎普通标签结束标记
        'tpl_end' => '}',
        // 标签库标签开始标记
        'taglib_begin' => '{',
        // 标签库标签结束标记
        'taglib_end' => '}',
    ],

    // 视图输出字符串内容替换
    'view_replace_str' => [
        '__PUBLIC__' => '/static/',
        '__UPLOAD__' => '/uploads/',
        '__ROOT__' => '/',
    ],
    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl' => THINK_PATH . 'tpl' . '/' . 'dispatch_jump.tpl',
    'dispatch_error_tmpl' => THINK_PATH . 'tpl' . '/' . 'dispatch_jump.tpl',

    // +----------------------------------------------------------------------
    // | 异常及错误设置
    // +----------------------------------------------------------------------

    // 异常页面的模板文件
    'exception_tmpl' => THINK_PATH . 'tpl' . '/' . 'think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message' => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg' => true,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle' => '',

    // +----------------------------------------------------------------------
    // | 日志设置
    // +----------------------------------------------------------------------

    'log' => [
        // 日志记录方式，内置 file socket 支持扩展
        'type' => 'File',
        // 日志保存目录
        'path' => LOG_PATH,
        // 日志记录级别
        'level' => ['error'],
        //最多
        'max_files' => 30
    ],


    // +----------------------------------------------------------------------
    // | Trace设置 开启 app_trace 后 有效
    // +----------------------------------------------------------------------
    'trace' => [
        // 内置Html Console 支持扩展
        'type' => 'Html',
    ],

    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------
    'cache' => [
        // 使用复合缓存类型
        'type' => 'complex',
        // 默认使用的缓存
        'default' => [
            // 驱动方式
            'type' => 'File',
            // 缓存保存目录
            'path' => CACHE_PATH,
            // 缓存前缀
            'prefix' => '',
            // 缓存有效期 0表示永久缓存
            'expire' => 0,
        ],
        // 文件缓存
        'file' => [
            // 驱动方式
            'type' => 'File',
            // 缓存保存目录
            'path' => CACHE_PATH,
            // 缓存前缀
            'prefix' => '',
            // 缓存有效期 0表示永久缓存
            'expire' => 0,
        ],
        // redis缓存
        'redis' => [
            // 驱动方式
            'type' => 'redis',
            'select' => 1,
        ],
    ],

    // +----------------------------------------------------------------------
    // | 会话设置
    // +----------------------------------------------------------------------

    'session' => [
        'id' => '',
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => '',
        // SESSION 前缀
        'prefix' => 'think',
        // 驱动方式 支持redis memcache memcached
        'type' => '',
        // 是否自动开启 SESSION
        'auto_start' => true,
    ],

    // +----------------------------------------------------------------------
    // | Cookie设置
    // +----------------------------------------------------------------------
    'cookie' => [
        // cookie 名称前缀
        'prefix' => '',
        // cookie 保存时间
        'expire' => 0,
        // cookie 保存路径
        'path' => '/',
        // cookie 有效域名
        'domain' => '',
        //  cookie 启用安全传输
        'secure' => false,
        // httponly设置
        'httponly' => '',
        // 是否使用 setcookie
        'setcookie' => true,
    ],

    //分页配置
    'paginate' => [
        'type' => 'bootstrap',
        'var_page' => 'page',
        'list_rows' => 15,
    ],

    //聚合短信接口
    'jhsms' => [
        'send_url' => 'http://v.juhe.cn/sms/send',
        'app_key' => '4df8a1c6fdba00d',
        'template_id' => ["code" => "189761", "order" => "18977"],
        'effect_time' => '10',//有效时间 单位分钟
    ],
    //手机正则
    'tel_regular' => '/^1\d{10}$/',
    /*微信支付配置配置*/
    'weixinpay' => array(
        'appid' => "",    /*微信开放平台上的APP应用id*/
        'mch_id' => "",   /*微信申请成功之后邮件中的商户id*/
        'api_key' => "",    /*在微信商户平台上自己设定的api密钥 32位*/
        'notify_url' => '' /*自定义的回调程序地址*/
    ),
    'partner_notify_wx' => '',
    /*微信公众号配置*/
    'wechat' => array(
        'appid' => '',//微信公众号appid
        'appsecret' => ''//微信公众号appsecret
    ),
    /*支付宝支付配置配置*/
    'alipay' => array(
        'gatewayUrl' => "https://openapi.alipay.com/gateway.do",
        'app_id' => "",    /*	支付宝分配给开发者的应用ID*/
        'method' => "alipay.trade.app.pay",    /*接口名称*/
        'notify_url' => '', /*自定义的回调程序地址*/
        'rsaPrivateKeyFilePath' => '',/*私钥的路径*/
        'rsaPrivateKey' => '',/*私钥值*/
        'alipayrsaPublicKey' => ""/*公钥*/
    ),
    //AES加密key
    'aes_encrypt_key' => "ca751e7aeaf600700933bcb1e609a455",

    //资源公共路径
    "public_path" => ROOT_PATH . 'public',
    //文件上传路径
    "upload_path" => ROOT_PATH . 'public' . '/' . 'uploads',
    //海报生成路径
    "poster_path" => '/images/posters',
    //二维码生成路径
    "qrcode_path" => '/images/qrcodes',

    /*激光配置*/
    "jpush" => [
        "app_key" => "",
        "app_secret" => ""
    ],
    'captcha' => [
// 验证码字符集合
        'codeSet' => '0123456789',
// 验证码字体大小(px)
        'fontSize' => 35,
// 验证码位数
        'length' => 4,
// 验证成功后是否重置
        'reset' => true
    ],

    //百度地图应用ak
    'baidu_map_key' => '',
    //高德地图应用key
    'gaode_map_key' => '',

    //七牛
    'qiniu' => [
        'access_key' => 'pwAy_5JHqJgjcXD1yEDSAmLVEKFHhv2dMH9gsrlv',
        'secret_key' => 'J6hdjgFhNUqnJEWcvthVD6oDcNAKD_rBVpfy7ZNm',
        'bucket' => 'qmgx',
        'domain' => 'http://img.yjkpt.cn'
    ],
    //后台订单定时查询时间 1000=1秒
    'order_tip_ajax_time' => 10000,
    //展示区一次调用几个商品
    'show_goods_num' => 10,
    //广告缓存时间 秒
    'advert_cache_time' => 600,
    //商品缓存时间 秒
    'goods_cache_time' => 60,
    //分类缓存时间 秒
    'category_cache_time' => 600,
    //购物车限制商品数量
    'car_max' => 50,
    //其他配置
    'one_img_size' => '1024',//单图上传控件限制大小 KB
    'img_ext' => 'jpg,gif,png,jpeg,bmp,ttf,tif',//图片格式
    'videos_ext' => 'mp4',//视频格式
    'one_video_size' => '10240',//视频大小 10M  10240KB
    'files_ext' => 'doc,docx,xls,xlsx',//文件格式-暂时未用
    'business_imgs' => 9,//商家附图数量
    'goods_imgs' => 5,//商品附图数量

    'db_order_table' => 'qfbaboziex',//订单表
    'work_sheet' => [
        "1" => "预约问题",
        "2" => "充值问题",
        "3" => "账户问题",
        "4" => "其他问题",
        "5" => "实名问题",
        "6" => "活动问题",
    ],
    'team_config' => [
        'level_2' => [
            'pidcount' => 10,
            'teamcount' => 35,
            'pushs' => 1000
        ],
        'level_3' => [
            'pidcount' => 30,
            'teamcount' => 90,
            'pushs' => 2000
        ],
        'level_4' => [
            'pidcount' => 70,
            'teamcount' => 200,
            'pushs' => 3000
        ],

    ],
    'system_title' => "全民共享",

    //运费计算类型
    'dispatch_type' => [
        '1' => [
            'type' => 1,
            'title' => '统一运费',
            'desc' => '同一商品多件，仅收一次运费'
        ],
        '2' => [
            'type' => 2,
            'title' => '按件运费',
            'desc' => '同一商品多件，每件收一次运费'
        ]
    ],
];
