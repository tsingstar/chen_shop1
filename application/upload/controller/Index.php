<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2016/11/16
 * Time: 下午2:05
 */
namespace app\upload\controller;

use think\Controller;
use think\Log;
use app\common\model\TokenException;
use app\common\model\UserException;
use app\common\model\DeveloperException;
use app\common\model\ServerException;
use app\common\model\User;
use app\common\model\Storage;

//图片上传
define("H_UPLOAD_IMAGE", "images");
//视频上传
define("H_UPLOAD_VIDEO", "videos");
//文件上传
define("H_UPLOAD_FILE", "files");

class Index extends Controller
{
    public function index()
    {
        header("Content-type:text/html;charset=utf-8");
        try
        {
//            User::userFromToken(request()->param('access_token'));
            //获取参数
            if (request()->isPost())
            {
                //单图上传
                if (input("?file.image"))
                {
                    $this->exec(H_UPLOAD_IMAGE);
                }
                else
                {
                    throw new DeveloperException('无效的上传！');
                }

            }
            else
            {
                throw new DeveloperException("仅接受POST方式文件上传！");
            }
        }
        catch (DeveloperException $exception)
        {
            //发送开发者级错误
            send_developer_error($exception->getMessage());
        }
        catch (UserException $exception)
        {
            //发送用户级别错误
            send_user_error($exception->getMessage());
        }
        catch (TokenException $exception)
        {
            //发送登录异常错误
            send_token_error($exception->getMessage());
        }
        catch (\Exception $exception)
        {
            //发送服务器级别错误
            send_server_error($exception->getMessage() . $exception->getFile() . $exception->getLine());
        }

    }

    private function exec($type)
    {
        $file = request()->file();

        //URL列表
        $urlList = array();

        if (is_array($file))
        {
            foreach ($file as $item)
            {
                if (is_array($item))
                {
                    foreach ($item as $ii)
                    {
                        $result = $this->upload($ii, $type);

                        if ($result)
                        {
                            $urlList[] = $result;
                        }
                    }
                }
                else
                {
                    $result = $this->upload($item, $type);

                    if ($result)
                    {
                        $urlList[] = $result;
                    }
                }
            }
        }
        else
        {
            $result = $this->upload($file, $type);

            if ($result)
            {
                $urlList[] = $result;
            }
        }

        if (count($urlList) <= 0)
        {
            throw new UserException("上传失败！");
        }

        $response = array(
            "result_code" => "0000",
            "result_info" => "上传成功！",
            "result_data" => [
                'upload_list'=>$urlList
            ]
        );
        //判断是否加密
        if (input('?get.no_encrypt'))
        {
            die(json_encode($response, JSON_UNESCAPED_UNICODE));
        }
        else
        {
            $body = aes_encrypt(config("aes_encrypt_key"), json_encode($response, JSON_UNESCAPED_UNICODE));
            die(json_encode(['body'=>$body],JSON_UNESCAPED_UNICODE));
        }
    }

    /**
     * 上传文件
     *
     * @param object $file 收到的文件对象
     * @return string 服务器端文件URL
     */
    private function upload($file, $type)
    {

        //验证条件
        $validation = array();

        //图像
        if ($type == H_UPLOAD_IMAGE)
        {
            $validation["ext"] = config('img_ext');
            $validation["size"] = config('one_img_size')*1024;
        }

        //视频
        if ($type == H_UPLOAD_VIDEO)
        {
            $validation["ext"] = "mov,mp4,avi,mkv,flv,rm,rmvb";
        }

        $storage = new Storage();
        $result = $storage->uploadTwo($file,$validation,1);

        if ($result['status']==0)
        {
            return $result['data'];
        }
        else
        {
            throw new UserException($result['info']);
        }

    }
}