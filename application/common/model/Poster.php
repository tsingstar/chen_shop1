<?php
/**
 * Created by PhpStorm.
 * User: 雨寒
 * Date: 2019/3/2
 * Time: 下午2:54
 */
namespace app\common\model;
use think\Exception;
use think\Loader;
class Poster extends Common
{

    /**
     * 根据用户生成线下店收款二维码
     *
     * @param array $store 当前店
     * @param int $size 栅格大小
     * @return string
     * @throws ServerException
     */
    static public function getQR($store, $size = 30)
    {

        $model = 3;//改变一次 加一 防止缓存
        //导入qrcode库
        Loader::import("phpqrcode.qrlib");

        //生成二维码的数据
        $data = [
            'type'=>0,
            'offLineImg'=>$store['img_url'],
            'offLineName'=>$store['title'],
            'offLineId'=>$store['id'],
            'onLineId'=>$store['business_id'],
            'description'=>"线下店铺消费"
        ];
        $data = json_encode($data);
        //保存路径
        $savePath = config("public_path") . '/' . config("qrcode_path");
        //创建路径
        self::createDir($savePath);

        //文件名
        $fileName = md5($store['id'].$store['img_url'].$model).".png";
        //完整名称
        $fullName = $savePath . '/' .$fileName;
        if(!file_exists($fullName)){
            try
            {

                set_time_limit(0);
                @ini_set('memory_limit', '256M');

                \QRcode::png($data, $fullName, "H", $size, 2);

                //获取背景图资源
                $background = self::createImage(get_whole_image(config("qrcode_path") . '/' .$fileName));

                $width = imagesx($background);
                $height = imagesy($background);

                $target = imagecreatetruecolor($width, $height);

                imagecopy($target, $background, 0, 0, 0, 0, $width, $height);
                imagedestroy($background);

                if(!empty($store['img_url'])){

                    $logo_w = intval($width*0.18);
                    $logo_l = ($width-$logo_w)/2;
                    $item = [
                        'left'=>$logo_l,
                        'top'=>$logo_l,
                        'width'=>$logo_w,
                        'height'=>$logo_w
                    ];
                    $target = self::mergeImage($target, $item, get_whole_image('/white.png'));

                    $logo_w = intval($width*0.16);
                    $logo_l = ($width-$logo_w)/2;
                    $item = [
                        'left'=>$logo_l,
                        'top'=>$logo_l,
                        'width'=>$logo_w,
                        'height'=>$logo_w
                    ];
                    $target = self::mergeImage($target, $item, get_whole_image($store['img_url']));
                }

                //生成图像
                imagepng($target, $fullName);
                //销毁图像资源
                imagedestroy($target);

            }
            catch (Exception $exception)
            {
                throw new ServerException($exception->getMessage());
            }
        }
        return config("qrcode_path") . '/' .$fileName;

    }

    /**
     * 合并图像
     *
     * @param resource $target 目标图像
     * @param array $data 规则数据
     * @param string $image 待合并的图像地址
     * @return mixed
     */
    static private function mergeImage($target, $data, $image)
    {
        //图片资源
        $image = self::createImage($image);

        //获取图片的尺寸
        $width = imagesx($image);
        $height = imagesy($image);

        //放置图像
        imagecopyresized($target, $image, $data['left'], $data['top']
            , 0, 0, $data['width'], $data['height'], $width, $height);

        //销毁图像资源
        imagedestroy($image);

        //返回合并后的图片资源
        return $target;
    }

    /**
     * 创建文件层级
     *
     * @param string $path 路径
     * @throws ServerException
     */
    static private function createDir($path)
    {
        //如果路径存在，则不做任何处理
        if (is_dir($path))
        {
            return;
        }
        else
        {
            //第三个参数是“true”表示能创建多级目录
            $result = mkdir($path, 0777, true);

            //创建目录失败
            if (!$result)
            {
                throw new ServerException('目录创建失败');
            }
        }

    }

    /**
     * 根据图片URL创建图片资源
     *
     * @param string $image 图片URL
     * @return resource
     * @throws ServerException
     */
    static private function createImage($image)
    {
        //检测是否为图片是否有效
        if (!check_web_image($image))
        {
            throw new ServerException('此图片文件无效' . ":" . $image);
        }

        //获取文件内容
        $content = file_get_contents($image);

        //根据图片内容创建图片资源
        return imagecreatefromstring($content);
    }
}