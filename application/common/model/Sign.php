<?php
/**
 * Created by PhpStorm.
 * User: 雨寒
 * Date: 2018/8/28
 * Time: 下午5:22
 */
namespace app\common\model;

use think\Model;
use think\Db;


class Sign extends Common
{
    /**
     * 签到
     * @param $user
     * @param int $type
     * @return array
     * @throws ServerException
     * @throws UserException
     */
    static public function signInfo($user,$type=1){

        $global = Settings::getSettings(H_SETTINGS_GLOBAL,2);
        $rate = Settings::getRate();
        $last_sign_time = $user['last_sign_time'];//上次签到时间
        $continuous_sign = $user['continuous_sign'];//连续签到天数
        $status = 0;//今天是否签到
        $sugar = 0;//今天签到得多少糖果
        $time = '';//今天签到时间
        $str = '';//今天签到提示信息

        //昨天时间戳
        $benginYesterday = getBeginYesterday();
        $endYesterday = getEndYesterday();
        //今天时间戳
        $benginToday = getBeginToday();

        if($last_sign_time>=$benginToday){
            //今天已签到
            $status = 1;
            if($type==2){
                throw new UserException('今日您已签到！');
            }
        }elseif($last_sign_time>=$benginYesterday){

            if($continuous_sign==7){
                //设置的周期清
                Db::name('user')->where('id',$user['id'])->setField('continuous_sign',0);
                $continuous_sign = 1;
            }else{
                $continuous_sign++;
            }

        }else{
            //昨天未签到-连签断开
            Db::name('user')->where('id',$user['id'])->setField('continuous_sign',0);
            $continuous_sign = 1;
        }

        $data = [];
        for($i=1;$i<=7;$i++){
            $data[$i]['title'] = '第'.$i.'天';
            if($i==7){
                $data[$i]['sugar'] = $rate['sign_last'];
            }else{
                $data[$i]['sugar'] = $rate['sign_normal']*$i;
            }
            if($i < $continuous_sign){
                $data[$i]['status'] = 1;
            }elseif($i == $continuous_sign){
                $data[$i]['status'] = $status;
                $sugar = $data[$i]['sugar'];
            }else{
                $data[$i]['status'] = 0;
            }
        }

        if($status==1){
            $log = Db::name('user_sign_log')->where(['uid'=>$user['id'],'create_time'=>['egt',$benginToday]])->find();
            $time = date('Y年m月d日 H:i:s',$last_sign_time);
            $str = '今日第'.@$log['level'].'名签到，累计签到'.$user['sign_num'].'天，连续签到'.$user['continuous_sign'].'天';
            $sugar = @$log['sugar'];
        }else{
            //只在未签到时判断是否提交签到
            if($type==2){
                $times = now_datetime();
                $sign_num = $user['sign_num']+1;
                $uda = [
                    'last_sign_time'=>$times,
                    'continuous_sign'=>$continuous_sign,
                    'sign_num'=>$sign_num
                ];
                $save = Db::name('user')->where('id',$user['id'])->update($uda);
                if(!$save){
                    throw new ServerException('签到状态保存失败');
                }

                //签到记录
                $count = Db::name('user_sign_log')->where('create_time','egt',$benginToday)->count();
                $count++;
                $inda = [
                    'uid'=>$user['id'],
                    'create_time'=>$times,
                    'sugar'=>$sugar,
                    'continuous_sign'=>$continuous_sign,
                    'level'=>$count
                ];
                $logid = Db::name('user_sign_log')->insertGetId($inda);
                if(!$logid){
                    throw new ServerException('签到记录保存失败');
                }

                //保存糖果记录
                if($sugar>0){
                    User::addDelSugar($user['id'],'1',$logid,$sugar,'签到第'.$continuous_sign.'天',$times,'2');
                }
                $data[$continuous_sign]['status'] = 1;
                $status = 1;
                $time = date('Y年m月d日 H:i:s',$times);
                $str = '今日第'.$count.'名签到，累计签到'.$sign_num.'天，连续签到'.$continuous_sign.'天';
            }
        }

        $arr = [
            'status'=>$status,
            'data'=>array_values($data),
            'tip'=>$global['sugar']['title'],
            'sugar'=>'+'.$sugar,
            'time'=>$time,
            'str'=>$str
        ];
        return $arr;
    }

}

