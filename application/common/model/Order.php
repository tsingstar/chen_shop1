<?php
/**
 * Created by PhpStorm.
 *
 * Date: 2018/12/01
 * Time: 下午5:22
 */

namespace app\common\model;

use think\Db;
use think\Exception;

class Order extends Common
{

    /**
     * 生成订单编号
     * @return string
     */
    static public function generateOrderSn()
    {
        return date("YmdHis") . random(6, true);
    }



    /**
     * 订单列表
     */
    static public function orderList($user_id, $type = 1, $page = 1, $page_size = 10)
    {

    }

    /**
     * 订单详情
     */
    static public function orderInfo($order_id)
    {

    }

}

