<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/8/1
 * Time: 13:50
 */

namespace app\common\model;


class BankList extends Common
{
    protected function initialize()
    {
        parent::initialize();
    }

    /**
     * 获取银行列表
     */
    public static function bankList()
    {
        $bank_list = self::where("status", 1)->order("display_order desc")->column("bank_name", "id");
        return $bank_list;
    }
    
    
    
}