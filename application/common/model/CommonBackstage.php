<?php
/**
 * Created by PhpStorm.
 *
 * Date: 2018/9/9
 * Time: 下午5:21
 */

namespace app\common\model;

use \think\Model;

class CommonBackstage extends Model
{
    public function __construct($data = [])
    {
        parent::__construct($data);

    }

    /**
     * return成功信息
     *
     * @param string $info 信息内容
     * @param array $extra 额外数据
     */
    static public function returnSuccess($info = "", $extra = array())
    {
        $result = array(
            "status"=>0,
            "info" => $info,
            "data" => $extra,
        );

        return $result;
    }

    /**
     * return失败信息
     *
     * @param string $info 信息内容
     * @param array $extra 额外数据
     */
    static public function returnError($info = "", $extra = array())
    {
        $result = array(
            "status"=>1,
            "info" => $info,
            "data" => $extra,
        );

        return $result;
    }
}
