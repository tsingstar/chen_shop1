<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/7/11
 * Time: 17:21
 */

namespace app\common\model;


use think\Cache;

class Utils
{

    /**
     * 根据跳转类型和参数生成跳转链接
     * @param $type
     * @param $param
     * @return string
     */
    public static function getSkipLink($type, $param)
    {
        if ($type == 2) {
            if (strpos($param, "https://") === 0 || strpos($param, "http://") === 0) {
            } else {
                $param = "http://".$param;
            }
            return $param;
        }
        //根据跳转类型拼接url
        $link_type = config("link_type");
        $link_url = $link_type[$type];
        if (!is_array($param)) {
            $param = [$link_url['link_key']=>$param];
        }
        return url($link_url['link_url'], $param);
    }

    /**
     * 获取省市县
     */
    public static function getRegion()
    {

//        if(!Cache::store("file")->get("region")){
            //获取省
            $province = db("region")->where("level", 1)->field("id, title value")->select();
            foreach ($province as &$p){
                //获取市
                $city = db("region")->where("level", 2)->where("parent_id", $p['id'])->field("id, title value")->select();
                foreach ($city as &$c){
                    //获取县区
                    $area = db("region")->where("level", 3)->where("parent_id", $c["id"])->field("id, title value")->select();
                    $c["childs"] = $area;
                }
                $p["childs"] = $city;
            }
            Cache::store("file")->set("region", $province);
//        }
        return Cache::store("file")->get("region");
    }

}