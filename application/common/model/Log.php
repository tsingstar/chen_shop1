<?php
/**
 * Created by PhpStorm.
 * User: 雨寒
 * Date: 2018/8/28
 * Time: 下午5:22
 */
namespace app\common\model;

use app\admin\model\Wallet;
use think\Model;
use think\Db;
class Log extends Common
{
    /**
     * 会员提现记录
     * @param $user_id
     * @param int $page
     * @param int $page_size
     * @return array|false|\PDOStatement|string|\think\Collection
     */
    static public function userWithdraw($user_id,$page=1,$page_size=10){
        $list = Db::name('user_withdraw')
            ->where('uid',$user_id)
            ->order('id DESC')
            ->field('coin,coin_price,frozen_coin,act_money,status,create_time,bank_name,bank_num,user_name')
            ->page($page,$page_size)
            ->select();

        foreach ($list as &$item){
            $item['create_time'] = accurateDate($item['create_time']);
            if($item['status']==1){
                $item['status']='已打款';
            }else{
                $item['status']='待审核';
            }
            $item['style'] = 3;
            $item['icon'] = 0;
        }

        return empty($list)?[]:$list;
    }

    /**
     * 收益记录
     * @param $user
     * @param int $page
     * @param int $page_size
     * @return array
     */
    static public function userProfit($user,$page=1,$page_size=10){
        $list = self::coinLog($user,5,$page,$page_size);
        return $list;
    }

    /**
     * 会员下级记录
     * @param $user_id
     * @param $source
     * @param int $page
     * @param int $page_size
     * @return array|false|\PDOStatement|string|\think\Collection
     * @throws DeveloperException
     */
    static public function userTeam($user_id,$source,$page=1,$page_size=10){
        if($source<=0||$source>2){
            throw new DeveloperException('source有误！');
        }

        $where = [];

        if($source==1){
            $where['parent_id']=$user_id;
        }

        if($source==2){
            $where['parent_id2']=$user_id;
        }

        $list = Db::name('user')
            ->where($where)
            ->order('id DESC')
            ->field('avatar,nick_name,identity,create_time')
            ->page($page,$page_size)
            ->select();

        foreach ($list as &$item){
            if(empty($item['avatar'])){
                $item['avatar'] = get_def_avatar();
            }
            $item['identity'] = config('identity')[$item['identity']];
            $item['create_time'] = accurateDate($item['create_time']);
            $item['str'] = '加入时间';
            $item['style'] = 2;
            $item['icon'] = 0;
        }

        return empty($list)?[]:$list;
    }


    static public function userMyTeam($user_id){
//        if($user['identity']<=1){
//            throw new UserException('您当前身份为游客，可在自营区购物升级身份！');
//        }

        //今日
        $beginToday = getBeginToday();
        $endToday = getEndToday();
        $today_m1 = Db::name('user')
            ->where('parent_id',$user_id)
            ->where('create_time','between',[$beginToday,$endToday])
            ->count();
        $today_m2 = Db::name('user')
            ->where('parent_id2',$user_id)
            ->where('create_time','between',[$beginToday,$endToday])
            ->count();
        $today = $today_m1+$today_m2;

        //本周
        $beginWeek = getBeginWeek();
        $endWeek = getEndWeek();
        $week_m1 = Db::name('user')
            ->where('parent_id',$user_id)
            ->where('create_time','between',[$beginWeek,$endWeek])
            ->count();
        $week_m2 = Db::name('user')
            ->where('parent_id2',$user_id)
            ->where('create_time','between',[$beginWeek,$endWeek])
            ->count();
        $week = $week_m1+$week_m2;

        //本月
        $beginMonth = getBeginMonth();
        $endMonth = getEndMonth();
        $month_m1 = Db::name('user')
            ->where('parent_id',$user_id)
            ->where('create_time','between',[$beginMonth,$endMonth])
            ->count();
        $month_m2 = Db::name('user')
            ->where('parent_id2',$user_id)
            ->where('create_time','between',[$beginMonth,$endMonth])
            ->count();
        $month = $month_m1+$month_m2;

        //本年
        $beginYear = getBeginYear();
        $endYear= getEndYear();
        $year_m1 = Db::name('user')
            ->where('parent_id',$user_id)
            ->where('create_time','between',[$beginYear,$endYear])
            ->count();
        $year_m2 = Db::name('user')
            ->where('parent_id2',$user_id)
            ->where('create_time','between',[$beginYear,$endYear])
            ->count();
        $year = $year_m1+$year_m2;


        //m1
        $m1 = Db::name('user')->where('parent_id',$user_id)->count();
        $list1 = self::userTeam($user_id,1,1,5);
        //m2
        $m2 = Db::name('user')->where('parent_id2',$user_id)->count();
        $list2 = self::userTeam($user_id,2,1,5);

        //全局
        $global = Settings::getSettings(H_SETTINGS_GLOBAL,2);

        $title1 = $global['first_level_team'];
        $title2 = $global['second_level_team'];
        $str = $title1.":".$title2.'=';
        $arr = [
            'today'=>[
                'num'=>$today,
                'str'=>$str.$today_m1.":".$today_m2
            ],
            'week'=>[
                'num'=>$week,
                'str'=>$str.$week_m1.":".$week_m2
            ],
            'month'=>[
                'num'=>$month,
                'str'=>$str.$month_m1.":".$month_m2
            ],
            'year'=>[
                'num'=>$year,
                'str'=>$str.$year_m1.":".$year_m2
            ],
            'data'=>[
                [
                    'title'=>$title1.'/人',
                    'num'=>$m1,
                    'list'=>$list1,
                    'inter'=>'1015',
                    'source'=>'1'
                ],
                [
                    'title'=>$title2.'/人',
                    'num'=>$m2,
                    'list'=>$list2,
                    'inter'=>'1015',
                    'source'=>'2'
                ]
            ]
        ];

        return $arr;
    }
}

