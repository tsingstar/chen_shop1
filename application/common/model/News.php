<?php
/**
 * Created by PhpStorm.
 * User: 雨寒
 * Date: 2018/8/17
 * Time: 下午5:21
 */

namespace app\common\model;

class News extends Common
{
    protected function initialize()
    {
        parent::initialize();
    }

    /**
     * 获取新闻公告列表
     *
     */
    public static function getList($page=1, $page_size=10)
    {
        $list = self::limit(($page-1)*$page_size, $page_size)->select();
        return $list;
    }

}
