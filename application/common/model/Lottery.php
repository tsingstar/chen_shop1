<?php
/**
 * Created by PhpStorm.
 * User:lll
 * Date: 2018/11/17
 * Time: 上午10:00
 */

namespace app\common\model;

use think\Cache;
use \think\Model;
use think\Db;


class Lottery extends Common
{

    /**
     * 抽奖
     * @param $user
     */
    static public function lottery($user){
//        $rate = Settings::getRate();
//        $lottery_switch = $rate['lottery_switch'];
//        //是否开启
//        if($lottery_switch!=1){
//            throw new UserException('抽奖还未开启，请等待开启后再进行抽奖');
//        }
//        //抽奖次数
//        if($user['lottery_num']<=0){
//            throw new UserException('抽奖次数已为0，消费每满'.$rate['lottery_money'].'将赠送一次抽奖机会');
//        }
        //商品
        $list = Db::name('lottery')
            ->where('is_visible',1)
//            ->where('stock','gt',0)
//            ->where('gl','gt',0)
                ->order('id')
            ->select();
//        if(count($list)<=0){
//            throw new UserException('哎呀，来晚了，奖励已被抽干净了！');
//        }
        $gl = 0;
        foreach ($list as &$item){
            $min = $gl+1;
            $gl += $item['gl'];
            $item['min']=$min;
            $item['max']=$gl;
        }
        unset($item);
        $number = rand(1,$gl);
        $choose = [];
        $ind = -1;
        foreach ($list as $k=>$v){
            if(($number>=$v['min']&&$number<=$v['max'])){
                $choose = $v;
                $ind = $k;
                break;
            }
        }
        if($ind>=0 && $choose['stock']>0 && $choose['gl']>0){

            //记录抽奖
            $data = [
                'uid'=>$user['id'],
                'lottery_id'=>$choose['id'],
                'title'=>$choose['title'],
                'goods_title'=>$choose['goods_title'],
                'img_url'=>$choose['img_url'],
                'type'=>$choose['type'],
                'num'=>$choose['num'],
                'create_time'=>now_datetime(),
                'status'=>0,
                'number'=>$number
            ];

            $id = Db::name('lottery_list')->insertGetId($data);
            if(!$id){
                throw new ServerException('服务器繁忙，请稍后再试');
            }

            //发奖
            if(in_array($choose['type'],[2,3,4])){
                $changeData = [
                    'status'=>1,
                    'status_time'=>now_datetime(),
                    'send_status'=>1,
                    'send_time'=>now_datetime()
                ];
                if($choose['type']==2){
                    //共享豆
                    $log_data = [
                        'uid'=>$user['id'],
                        'isadd'=>1,
                        'nums'=>$choose['num'],
                        'ctime'=>now_datetime(),
                        'info'=>'大转盘抽奖获得'
                    ];
                    \db('points')->insert($log_data);
                    \db('user')->where('id', $user['id'])->setInc('integral', $choose['num']);
//                Money::addDelCoin($user['id'],$choose['num'],1,'抽奖所得',5,$id,$user['id']);
                }
                if($choose['type']==3){
                    //商城币
                    $log_data = [
                        'uid'=>$user['id'],
                        'isadd'=>1,
                        'nums'=>$choose['num'],
                        'ctime'=>now_datetime(),
                        'info'=>'大转盘抽奖获得'
                    ];
                    \db('mybot')->insert($log_data);
                    \db('user')->where('id', $user['id'])->setInc('mybot', $choose['num']);
//                Money::addDelSugar($user['id'],$choose['num'],1,'抽奖所得',9,$id,$user['id']);
                }
                if($choose['type']==4){
                    //推广收益
                    $log_data = [
                        'uid'=>$user['id'],
                        'isadd'=>1,
                        'nums'=>$choose['num'],
                        'ctime'=>now_datetime(),
                        'info'=>'大转盘抽奖获得'
                    ];
                    \db('pushslist')->insert($log_data);
                    \db('user')->where('id', $user['id'])->setInc('pushs', $choose['num']);
//                Money::addDelSugar($user['id'],$choose['num'],1,'抽奖所得',9,$id,$user['id']);
                }
                $change = Db::name('lottery_list')->where('id',$id)->update($changeData);
                if(!$change){
                    throw new ServerException('服务器繁忙，请稍后再试');
                }
            }

            //减库存
            $del = Db::name('lottery')->where('id',$choose['id'])->setDec('stock',1);
            if(!$del){
                throw new ServerException('服务器繁忙，请稍后再试');
            }
        }else{
            $choose = [
                'id'=>-1,
                'price_title'=>'哎呀，来晚了，奖励已被抽干净了！'
            ];
        }

        //减抽奖次数
//        $del = Db::name('user')->where('id',$user['id'])->setDec('lottery_num',1);
//        if(!$del){
//            throw new ServerException('服务器繁忙，请稍后再试');
//        }

        return [
            'goods_id'=>$choose['id'],
            'price_title'=>$choose['price_title'],
            'ind'=>$ind+1
//            'lottery_num'=>$user['lottery_num']-1
        ];
    }

}
