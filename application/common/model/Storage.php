<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2018/1/11
 * Time: 下午3:55
 */
namespace app\common\model;

use Qiniu\Storage\UploadManager;
use think\File;

class Storage extends CommonBackstage
{
    // 存储参数
    private $params;

    /**
     * 执行上传操作
     *
     * @param string $name 文件域名
     * @param array $params 若限制扩展名则添加ext、限制类型则添加type
     * @param int $method 上传方式1 七牛
     * @return array
     */
    public function upload($name, $params = [], $method=1)
    {
        // 获取上传的文件信息
        $file = request()->file($name);

        // 检测文件
        $check = $this->checkFile($file, $params);

        if($check['status']==1){
            return $check;
        }

        switch ($method){
            case 1:
                $this->params = config('qiniu');
                return $this->uploadToQiNiu($file);
                //return $this->saveFileLocal($file);
                break;
            default:
                return $this->saveFileLocal($file);
                break;
        }
    }

    /**
     * 执行上传操作
     *
     * @param string $file 文件
     * @param array $params 若限制扩展名则添加ext、限制类型则添加type
     * @param int $method 上传方式 默认本地上传  1 七牛
     * @return array
     */
    public function uploadTwo($file, $params = [], $method=1)
    {

        // 检测文件
        $check = $this->checkFile($file, $params);

        if($check['status']==1){
            return $check;
        }
        switch ($method){
            case 1:
                $this->params = config('qiniu');
                return $this->uploadToQiNiu($file);
                break;
            default:

                break;
        }
    }


    /**
     * 检测文件是否符合参数约束
     *
     * @param File $file 待检测的文件
     * @param array $param 约束条件
     * @throws Exception
     */
    private function checkFile($file, $param)
    {
        // 判断文件是否存在
        if (empty($file))
        {
            return $this->returnError('上传文件不能为空！！！');
        }

        // 检测文件是否有效
        if (!$file->isValid())
        {
            return $this->returnError('无效的上传文件！！！');
        }

        // 检测文件大小
        if (!empty($param['size']) && !$file->checkSize($param['size']))
        {
            return $this->returnError('文件超过最大上传限制！！！');
        }

        // 检测扩展名是否有效
        if (!empty($param['ext']) && !$file->checkExt($param['ext']))
        {
            return $this->returnError('无效的文件扩展名！！！');
        }

        // 检测文件类型是否有效
        if (!empty($param['type']) && !$file->checkMime($param['type']))
        {
            return $this->returnError('无效的文件类型！！！');
        }

        return $this->returnSuccess('验证成功');
    }



    /**
     * 上传文件到七牛云存储
     *
     * @param File $file 文件对象
     * @return string
     * @throws Exception
     */
    private function uploadToQiNiu($file)
    {

        // 获取文件信息
        $fileInfo = $this->getFileInfo($file);

        try
        {
            // 构建鉴权对象
            $auth = new \Qiniu\Auth($this->params['access_key'], $this->params['secret_key']);

            // 生成上传 Token
            $token = $auth->uploadToken($this->params['bucket']);

            // 初始化 UploadManager 对象
            $manager = new UploadManager();

            // 进行文件上传操作。
            list($result, $error) = $manager->putFile($token, $fileInfo['name'], $fileInfo['path']);

            // 上传成功
            if ($error === null)
            {
                // 返回上传后的地址
                $url = "{$this->params['domain']}/{$result['key']}";
                return $this->returnSuccess('上传成功',$url);
            }
            else
            { // 上传失败
                return $this->returnError('七牛云服务异常，上传失败！！！');
            }
        }
        catch (\Exception $exception)
        {
            return $this->returnError($exception->getMessage());
        }
    }
    /**
     * 为七牛云处理文件信息
     *
     * @param File $file
     * @return array
     */
    private function getFileInfo($file)
    {
        // 文件名
        $filename = $file->getInfo('name');

        // 文件扩展
        $extension = substr($filename, strripos($filename, '.'));

        // 文件名
        $fullName = md5(microtime(true) . $filename) . $extension;

        // 返回文件信息
        return [
            'name' => $fullName,
            'path' => $file->getInfo('tmp_name')
        ];
    }

    /**
     * 文件本地保存
     */
    private function saveFileLocal($file)
    {
        $tmp_name = $file->getInfo('tmp_name');
        $root = ROOT_PATH."public";
        $destination = "/upload/".date("Y-m-d");
        is_dir($root.$destination) or mkdir($root.$destination,"0755", true);
        $ext = pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);
        $save_name = md5(time().random(5, true)).".".$ext;
        $res = move_uploaded_file($tmp_name, $root.$destination."/".$save_name);
        if($res){
            return self::returnSuccess("文件上传成功", $destination."/".$save_name);
        }else{
            return self::returnError("文件上传失败");
        }

//        move_uploaded_file()



    }
}
