<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2017/1/9
 * Time: 下午5:21
 */

namespace app\common\model;

use \think\Model;

class Common extends Model
{
    public function __construct($data = [])
    {
        parent::__construct($data);

        request()->post("CURRENT_MODULE", "common");
    }
}
