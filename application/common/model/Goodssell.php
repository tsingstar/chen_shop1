<?php
/**
 * Created by PhpStorm.
 * User:lll
 * Date: 2018/11/17
 * Time: 上午10:00
 */

namespace app\common\model;

use think\Cache;
use \think\Model;
use think\Db;


class Goodssell extends Common
{

    /**
     * 商品详情
     * @param $id
     * @param int $user_id
     * @param int $type
     * @return array
     * @throws UserException
     */
    static public function goodsInfo($id,$user_id=0,$type=0){

        //接口返回的数据模型 初始化 在下面查询和判断中组织数据填充到模型
        $arr = [
            '_id'=>$id,//商品id
            'group_images'=>[],//组图
            'video'=>'',//视频地址
            'original_price'=>'0',//原价
            'title'=>'',//商品标题
            'sales_nums'=>0,//销量
            'choose_title'=>'',//选择规格或数量时显示的名称 如请选择 颜色+尺码
            'spec'=>[
                'def'=>[
                    '_id'=>0,
                    'img_url'=>'',
                    'title'=>'',
                    'price'=>'',
                    'coin'=>'',
                    'group_price'=>'',
                    'group_coin'=>'',
                    'original_price'=>'',
                    'stock'=>''
                ],
                'list'=>[]
            ],//规格
            'detail'=>'',//商品详情
            'is_collection'=>0,//是否收藏
            'status'=>1,
        ];

        $info = Db::name('sell_goods')
            ->alias('g')
            ->join('sell_goods_detail gd','gd.id=g.detail_id','LEFT')
            ->where('g.id',$id)
            ->field('g.id as _id,g.sales_nums,g.video_url,g.img_url,g.price,g.original_price,g.title,g.spec_name,g.stock,g.use_spec,gd.detail,g.status')
            ->find();

        if(!$info){
            throw new UserException('不好意思，商品走丢了！');
        }

        $arr['status'] = $info['status'];

        //浏览+1
        Db::name('sell_goods')->where('id',$id)->setInc('view_nums');

        //组图填充
        $group_images = Db::name('sell_goods_img')
            ->where('goods_id',$id)
            ->field('img_url')
            ->select();

        if(count($group_images)>0){

            foreach ($group_images as $key=>$item){
                $arr['group_images'][$key] = $item['img_url'];
            }
        }else{
            $arr['group_images'][] = $info['img_url'];
        }

        if($type==0){
            //视频不为空加入组图
            if(!empty($info['video_url'])){
                array_unshift($arr['group_images'],$info['video_url']);
            }
        }else{
            $arr['video'] = $info['video_url'];
        }

        //规格
        if($info['use_spec'] == 1){
            //启用规格
            $map['goods_id']=$id;
            $spec_list = Db::name('sell_goods_spec')->where($map)->field('id as _id,title,price,original_price,stock,img_url')->select();
            $choose_title = empty($info['spec_name']) ? '购买数量' : $info['spec_name'];
        }else{
            //不启用
            $spec_list = [];
            $choose_title =  '购买数量';
        }

        $arr['price'] = $info['price'];
        $arr['original_price'] = $info['original_price'];

        //规格填充
        $arr['spec']['list'] = $spec_list;
        $arr['spec']['def']['img_url'] = $info['img_url'];
        $arr['spec']['def']['title'] = $info['title'];
        $arr['spec']['def']['price'] = $arr['price'];
        $arr['spec']['def']['original_price'] = $arr['original_price'];
        $arr['spec']['def']['stock'] = $info['stock'];

        $arr['title'] = $info['title'];
        $arr['sales_nums'] = $info['sales_nums'];
        $arr['choose_title'] = $choose_title;

        $arr['detail'] = $info['detail'];

        $arr['is_collection'] = 0;
        if(!empty($user_id)){
            $is_collection = Db::name('sell_goods_collection')
                ->where('uid',$user_id)
                ->where('goods_id',$id)
                ->find();
            if($is_collection){
                $arr['is_collection'] = 1;
            }
        }
        return $arr;
    }

    static public function check($goods_id){
        $info = Db::name('sell_goods')
            ->where('id',$goods_id)
            ->find();
        if(empty($goods_id)){
            throw new UserException('商品参数有误');
        }

        if(!$info){
            throw new UserException('不好意思，商品找不到了！');
        }

        if($info['status']!=1){
            throw new UserException('该商品已下架！');
        }

        return $info;
    }

    /**
     * 我的收藏
     * @param $user_id
     * @param int $page
     * @param int $page_size
     * @return array|false|\PDOStatement|string|Model
     * @throws UserException
     */
    static public function myCollect($user_id,$page=1,$page_size=10){
        $list = Db::name('sell_goods_collection')
            ->alias('gc')
            ->join('sell_goods g','g.id=gc.goods_id')
            ->where('gc.uid',$user_id)
            ->field('gc.id as _id,g.id as goods_id,g.img_url,g.title,g.price')
            ->order('gc.id DESC')
            ->page($page,$page_size)
            ->select();
        return empty($list)?[]:$list;
    }

    /**
     * 收藏删除
     * @param $user_id
     * @param $del_id
     * @return string
     * @throws UserException
     */
    static public function myCollectRemove($user_id,$del_id){
        $del_id = explode(',',$del_id);
        foreach ($del_id as $key=>$item){
            if(empty($item)){
                unset($del_id[$key]);
            }
        }
        if(count($del_id)<=0){
            throw new UserException('请选择要删除的收藏！');
        }

        Db::name('sell_goods_collection')
            ->where('uid',$user_id)
            ->where('id','in',$del_id)
            ->delete();
        return '删除收藏商品成功';

    }

    /**
     * 添加收藏或取消收藏
     * @param $user_id
     * @param $goods_id
     * @param $is_collection int 原收藏状态 1已收藏 这里去取消 0未收藏这里去收藏
     */
    static public function collectionGoods($user_id,$goods_id,$is_collection){
        $where = [
            'goods_id'=>$goods_id,
            'uid'=>$user_id
        ];
        if(!empty($is_collection)){
            //取消收藏
            Db::name('sell_goods_collection')->where($where)->delete();
            return '取消商品收藏成功';
        }else{
            //添加收藏
            $has = Db::name('sell_goods_collection')->where($where)->find();
            if(!$has){
                self::check($goods_id);
                $where['create_time'] = now_datetime();
                Db::name('sell_goods_collection')->insert($where);
            }
            return '商品收藏成功';
        }
    }

}
