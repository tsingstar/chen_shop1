<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/7/15
 * Time: 8:47
 */

namespace app\common\model;


class Commission extends Common
{
    protected function initialize()
    {
        parent::initialize();
    }

    /**
     * 计算商品佣金
     */
    public static function calcuteCommission($good_id, $num, $spec_id)
    {
        $commission_rate = Settings::getSettings("commission");
        if (empty($spec_id)) {
            $g = db("goods")->where("id", $good_id)->find();
            $commission1 = round($g["price"] * $num * $commission_rate["commission1"] / 100, 2);
            $commission2 = round($g["price"] * $num * $commission_rate["commission2"] / 100, 2);
        } else {
            $g = db("goods_spec")->where("id", $spec_id)->find();
            $commission1 = round($g["price"] * $num * $commission_rate["commission1"] / 100, 2);
            $commission2 = round($g["price"] * $num * $commission_rate["commission2"] / 100, 2);
        }
        return [
            "commission1" => $commission1,
            "commission2" => $commission2
        ];
    }

    //获取分销佣金
    public static function getCommissionTotal($user_id)
    {
        //获取总佣金
        $commission = 0;
        //获取下属一级
        $children1_id = db("user")->where("parent_id", $user_id)->column("id");
        if (!empty($children1_id)) {
            //计算一级下属佣金
            $commission1 = db("order")->whereIn("uid", $children1_id)->where("pay_status", 1)->where("order_status", 'neq', 4)->sum('commission1');
            $commission += $commission1;
            //获取下属二级
            $children2_id = db("user")->whereIn("parent_id", $children1_id)->column("id");
            if (!empty($children2_id)) {
                //计算二级下属佣金
                $commission2 = db("order")->whereIn("uid", $children2_id)->where("pay_status", 1)->where("order_status", 'neq', 4)->sum('commission2');
                $commission += $commission2;
            }
        }
        return $commission;
    }

    //获取未结算分销佣金
    public static function getCommissionNoCal($user_id)
    {
        //获取总佣金
        $commission = 0;
        //获取下属一级
        $children1_id = db("user")->where("parent_id", $user_id)->column("id");
        if (!empty($children1_id)) {
            //计算一级下属佣金
            $commission1 = db("order")->whereIn("uid", $children1_id)->where("pay_status", 1)->where("order_status", 'in', [1, 2])->sum('commission1');
            $commission += $commission1;
            //获取下属二级
            $children2_id = db("user")->whereIn("parent_id", $children1_id)->column("id");
            if (!empty($children2_id)) {
                //计算二级下属佣金
                $commission2 = db("order")->whereIn("uid", $children2_id)->where("pay_status", 1)->where("order_status", 'in', [1, 2])->sum('commission2');
                $commission += $commission2;
            }
        }
        return $commission;
    }

    /**
     * 获取分销订单数量
     */
    public static function getCommissionOrder($user_id)
    {
        $children = self::getTotal($user_id);
        $children_id = array_merge($children["children1"], $children["children2"]);
        return db("order")->whereIn("uid", $children_id)->where("order_status", 'neq', 4)->count();
    }

    /**
     * 获取分销订单
     */
    public static function getOrder($user_id, $type = 0, $page = 1, $page_num = 20)
    {
        $children = self::getTotal($user_id);
        $children_id = array_merge($children["children1"], $children["children2"]);
        $model = db("order")->alias("a")->join("user b", "a.uid=b.id")->whereIn("a.uid", $children_id);
        if ($type == 0) {
            $model->where("a.order_status", 'neq', 4);
        }
        if ($type == 1) {
            $model->where("a.order_status", 1);
        }
        if ($type == 2) {
            $model->where("a.order_status", 2)->where("pay_status", 1);
        }
        if ($type == 3) {
            $model->where("a.order_status", "in",  [3,5])->where("pay_status", 1);
        }

        $list = $model->order("a.create_time desc")->limit(($page - 1) * $page_num, $page_num)->field("a.order_sn, a.uid, a.create_time, a.order_status, a.commission1, a.commission2, b.nick_name")->select();
        foreach ($list as &$item) {
            $item["create_time"] = date("Y-m-d H:i:s", $item["create_time"]);
            if (in_array($item["uid"], $children["children1"])) {
                $item["c_rank"] = "一级";
                $item["commission"] = $item["commission1"];
            }
            if (in_array($item["uid"], $children["children2"])) {
                $item["c_rank"] = "二级";
                $item["commission"] = $item["commission2"];
            }
            if($item["order_status"] == 0){
                $item["order_str"] = "待付款";
            }
            if($item["order_status"] == 1){
                $item["order_str"] = "已付款";
            }
            if($item["order_status"] == 2){
                $item["order_str"] = "待收货";
            }
            if($item["order_status"] == 3 ||$item["order_status"] == 5){
                $item["order_str"] = "已完成";
            }
        }
        return $list;
    }


    //获取我的下线会员id
    public static function getTotal($user_id)
    {
        //获取下属一级
        $children1_id = db("user")->where("parent_id", $user_id)->column("id");
        if (!empty($children1_id)) {
            //获取下属二级
            $children2_id = db("user")->whereIn("parent_id", $children1_id)->column("id");
        } else {
            $children2_id = [];
        }
        return ["children1" => $children1_id, "children2" => $children2_id];
    }

    /**
     * 获取提现明细数量
     */
    public static function getWithdrawLogNum($user_id)
    {
        return db("user_withdraw")->where("uid", $user_id)->count();
    }

    /**
     * 获取提现日志
     */
    public static function getWithdrawLog($user_id, $page = 1, $pageNum = 20, $type = 0)
    {
        $model = db("user_withdraw")->where("uid", $user_id);
        if ($type == 1) {
            $model->where("status", 0);
        }
        if ($type == 2) {
            $model->where("status", 1);
        }
        if ($type == 3) {
            $model->where("status", 2);
        }
        if ($type == 4) {
            $model->where("status", 3);
        }
        return $model->limit(($page - 1) * $pageNum, $pageNum)->select();
    }

    /**
     * 佣金变动记录
     */
    public static function commissionLog($user_id, $commission, $type, $content, $order_id = 0)
    {
        db("commission_log")->insert([
            "uid" => $user_id,
            "content" => $content,
            "commission" => $commission,
            "create_time" => time(),
            "order_id" => $order_id,
            'type' => $type
        ]);
    }


}