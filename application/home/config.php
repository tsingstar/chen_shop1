<?php
/**
 * Created by PhpStorm.
 * User: Sunqj
 * Date: 2017/1/11
 * Time: 上午1:30
 */

return [

    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => __DIR__ . '/tpl/dispatch_jump.tpl',
    'dispatch_error_tmpl'    => __DIR__ . '/tpl/dispatch_jump.tpl',
    'template' => [
        // 模板路径
        'view_path'     =>  __DIR__ . '/tpl/',
        //开启模板布局
        'layout_on' => true,
        //设置模板布局位置
        'layout_name' => 'public/layout',
    ],
    // 视图输出字符串内容替换
    'view_replace_str'       => [
        '__PUBLIC__'=>'/static',
        '__UPLOAD__'    =>  '/uploads/',
        '__ROOT__' => '/',
    ],
];