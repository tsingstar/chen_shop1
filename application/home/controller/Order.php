<?php
/**
 * Created by PhpStorm.
 * User:lll
 * Date: 2018/12/13
 * Time: 18:03
 */
namespace app\home\controller;

use app\common\model\Ordershop as OrderModel;
use app\common\model\Region;
use think\Db;

class Order extends Common
{
    //跳转提交订单前验证
    public function beforeCheck(){
        try{
            $data = input('post.data/a');
            if(!is_array($data)||count($data)<1){
                $this->ajaxError('请选择商品');
            }
            $type = intval(input('post.type'));
            $info = OrderModel::carConfirmOrder($this->currentUser(),$data,$type);
            $arr = [
                'confirm'=>$info,
                'subData'=>$data,
                'subType'=>$type
            ];
            $temporary = makeTemporary($this->currentUser(),$arr);
            $url = url('Order/confirm');
            $this->ajaxSuccess('',['url'=>$url]);
        }catch (\Exception $exception){
            global $wechat_has_exception;
            $wechat_has_exception = true;
            $this->ajaxError($exception->getMessage());
        }
    }

    //确认订单页面
    public function confirm(){
        $arr = getTemporary($this->currentUser());
        if(!$arr){
            $this->error('参数有误');
        }

        $this->assign('info',$arr['confirm']);
        $this->assign('subData',$arr['subData']);
        $this->assign('subType',$arr['subType']);
        return $this->fetch();
    }

    //订单切换地址
    public function changeAddress(){
        try{
            $address_id = intval(input('address_id'));
            $arr = getTemporary($this->currentUser());
            if(!$arr){
                $this->error('参数有误');
            }
            $address = Region::oneAddress($this->currentUserId(),$address_id);
            $arr['confirm']['address']=$address;
            makeTemporary($this->currentUser(),$arr);
            $this->ajaxSuccess('');
        }catch (\Exception $exception){
            global $wechat_has_exception;
            $wechat_has_exception = true;
            $this->ajaxError($exception->getMessage());
        }
    }

    //提交订单
    public function subOrder(){
        try{
            header("Content-type:text/html;charset=utf-8");
            $message = remove_all_tags2(input('post.message'));
            $res = OrderModel::carSubOrder($this->currentUser(),$message);
            $this->ajaxSuccess('',$res);
        }catch (\Exception $exception){
            global $wechat_has_exception;
            $wechat_has_exception = true;
            $this->ajaxError($exception->getMessage());
        }
    }

    //支付
    public function pay(){
        $openid = session('openid');
        if(request()->isAjax()){
            try{
                $order_id = intval(input('post.order_id'));
                $pay_type = intval(input('post.pay_type'));
                $res = OrderModel::orderPay($this->currentUser(),$order_id,$pay_type,$openid);
                $this->ajaxSuccess('',$res);
            }catch (\Exception $exception){
                global $wechat_has_exception;
                $wechat_has_exception = true;
                $this->ajaxError($exception->getMessage());
            }
        }else{
            $this->templateTitle('支付订单');
            $orderSn = remove_all_tags2(input('orderSn'));
            $info = Db::name('sell_order')
                ->where('order_sn',$orderSn)
                ->where('uid',$this->currentUserId())
                ->find();
            if(!$info){
                $this->error('无此订单');
            }

            $this->assign('user',$this->currentUser());
            $this->assign('info',$info);
            $hasOpenid = 0;
            if(!empty($openid)){
                $hasOpenid = 1;
            }
            $this->assign('hasOpenid',$hasOpenid);
            return $this->fetch();
        }
    }

    //支付成功
    public function paySuccess(){
        $this->templateTitle('支付成功');
        return $this->fetch();
    }

    //订单列表
    public function lists(){
        $status = intval(input('status'));
        if(request()->isAjax()){
            $p = intval(input('page', 1));
            $list = OrderModel::orderList($this->currentUserId(),$status,$p,config('ajax_num'));
            $this->ajaxSuccess('请求成功', $list);
        }else{
            $this->templateTitle('我的订单');
            $this->assign('status',$status);
            return $this->fetch();
        }
    }

    //取消
    public function cancel(){
        try{
            $order_id = intval(input('post.order_id'));
            $res = OrderModel::orderCancel($this->currentUser(),$order_id);
            $this->ajaxSuccess($res);
        }catch (\Exception $exception){
            global $wechat_has_exception;
            $wechat_has_exception = true;
            $this->ajaxError($exception->getMessage());
        }
    }

    //收货
    public function receive(){
        try{
            $order_id = intval(input('post.order_id'));
            $res = OrderModel::orderReceive($this->currentUserId(),$order_id);
            $this->ajaxSuccess($res);
        }catch (\Exception $exception){
            global $wechat_has_exception;
            $wechat_has_exception = true;
            $this->ajaxError($exception->getMessage());
        }
    }

    //详情
    public function detail(){
        try{
            $this->templateTitle('订单详情');
            $orderSn = trim(input('orderSn'));
            $info = OrderModel::orderInfo($this->currentUserId(),'',$orderSn);
            $this->assign('info',$info);
            return $this->fetch();
        }catch (\Exception $exception){
            global $wechat_has_exception;
            $wechat_has_exception = true;
            $this->error($exception->getMessage());
        }
    }
}