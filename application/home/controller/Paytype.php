<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/8/2
 * Time: 17:39
 */

namespace app\home\controller;

use app\common\model\Storage;
use app\common\model\User as umodel;
use think\Db;
use app\common\model\Jhsms;

class Paytype extends Common
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /*
     * 检测交易密码
     * */
    public function check_repwd()
    {
        $code = trim($_POST['code']);
        if (empty($code)) {
            echo '请输入交易密码';
            exit;
        }
        $uid = session("user_id");
        $userinfo = userallinfo($uid);
        if (empty($userinfo['id'])) {
            echo '用户不存在！';
            exit;
        }
        if ($userinfo['sepasswd'] != md5(md5($code))) {
            echo '交易密码错误！';
            exit;
        } else {
            echo 1;
            exit;
        }

    }

    /**
     * 设置中心
     */
    public function index()
    {
        $this->templateTitle("收款方式");

        $uid = session("user_id");
        if (empty($uid)) {
            session('tips', '登录过期，请重新登录');
            $this->redirect("Index/login");
        }
        $userinfo = userallinfo($uid);
        $pay_type = input('pay_type');
        $this->assign('pay_type', $pay_type);
        /*所有以及类型*/
        $list_cate = Db::name('bank_list')->where("status=1")->order("display_order desc,id desc")->select();;
        $this->assign('catelist', $list_cate);
        $status = [
            "bank" => 1,
            "wechat" => 1,
            "ali" => 1
        ];
        if (empty($userinfo['realname']) || empty($userinfo['banktypename']) || empty($userinfo['bankname']) || empty($userinfo['bankcard'])) {
            /*没有认证完成银行卡*/
            $status["bank"] = 0;
        }
        if (empty($userinfo['emailname']) || empty($userinfo['email']) || empty($userinfo['emailcode'])) {
            /*没有认证完成支付宝*/
            $status["ali"] = 0;
        }
        if (empty($userinfo['selfqqname']) || empty($userinfo['selfqq']) || empty($userinfo['selfqqcode'])) {
            /*没有认证完成微信*/
            $status['wechat'] = 0;
        }
        $this->assign('status', $status);
        $this->assign("userinfo", $userinfo);
        return $this->fetch("index");
    }

    /*
     * 支付宝支付
     * */
    public function pay_alipay()
    {
        $this->templateTitle("收款方式-支付宝绑定");
        $uid = session("user_id");
        if (empty($uid)) {
            session('tips', '登录过期，请重新登录');
            $this->redirect("Index/login");
        }
        $userinfo = userallinfo($uid);
        if (empty($userinfo['emailname']) || empty($userinfo['email']) || empty($userinfo['emailcode'])) {
            /*没有认证完成支付宝*/
            $this->assign('userinfo', $userinfo);
            return $this->fetch("pay_alipay");
        } else {
            $this->assign('userinfo', $userinfo);
            return $this->fetch('pay_alipayok');
        }
    }

    /*修改支付宝信息*/
    public function doalipay()
    {
        if (request()->isAjax()) {
            $result = array(
                's' => 0,
                'msg' => '绑定成功'
            );
            $flag = true;
            $uid = session("user_id");
            $userinfo = userallinfo($uid);
            if (empty($userinfo['emailname']) || empty($userinfo['email']) || empty($userinfo['emailcode'])) {
                /*没有认证完成支付宝*/
                $info['emailname'] = remove_tag($_POST['username']);
                $info['email'] = remove_tag($_POST['alipay']);
                //七牛云上传图片
                $info['emailcode'] = $_POST['upload_geturl'];
                if ($flag) {
                    $s = Db::name('user')->where('id=' . $uid)->update($info);
                    if ($s) {
                        $result['s'] = 1;
                    } else {
                        $result['msg'] = "绑定失败";
                    }
                }
            } else {
                $result['s'] = 1;
                $result['msg'] = "请勿重复认证!";
            }
            echo json_encode($result);
            exit();
        }
    }

    /*
     * 银行卡支付
     *  */
    public function pay_card()
    {
        $this->templateTitle("收款方式-银行卡绑定");
        $uid = session("user_id");
        if (empty($uid)) {
            session('tips', '登录过期，请重新登录');
            $this->redirect("Index/login");
        }
        $userinfo = userallinfo($uid);
        /*所有以及类型*/
        $list_cate = Db::name('bank_list')->where("status=1")->order("display_order desc,id desc")->select();;
        $this->assign('catelist', $list_cate);

        if (empty($userinfo['realname']) || empty($userinfo['banktypename']) || empty($userinfo['bankname']) || empty($userinfo['bankcard'])) {
            /*没有认证完成银行卡*/
            $this->assign('userinfo', $userinfo);
            return $this->fetch("pay_card");
        } else {
            $this->assign('userinfo', $userinfo);
            return $this->fetch('pay_cardok');
        }
    }

    public function docard()
    {
        if (request()->isAjax()) {
            $result = array(
                's' => 0,
                'msg' => '绑定成功'
            );
            $flag = true;
            $uid = session("user_id");
            $userinfo = userallinfo($uid);
            if (empty($userinfo['realname']) || empty($userinfo['banktypename']) || empty($userinfo['bankname']) || empty($userinfo['bankcard'])) {
                $info['realname'] = remove_tag($_POST['realname']);
                $info['banktypename'] = remove_tag($_POST['banktypename']);
                $info['bankname'] = remove_tag($_POST['bankname']);
                $info['bankcard'] = $_POST['bankcard'];
                if ($flag) {
                    $s = Db::name('user')->where('id=' . $uid)->update($info);
                    if ($s) {
                        $result['s'] = 1;
                    } else {
                        $result['msg'] = "绑定失败";
                    }
                }
            } else {
                $result['s'] = 1;
                $result['msg'] = "请勿重复认证!";
            }
            echo json_encode($result);
            exit();
        }
    }


    /*
     * 微信
    */
    public function pay_wechat()
    {
        $this->templateTitle("收款方式-微信绑定");
        $uid = session("user_id");
        if (empty($uid)) {
            session('tips', '登录过期，请重新登录');
            $this->redirect("Index/login");
        }
        $userinfo = userallinfo($uid);
        if (empty($userinfo['selfqqname']) || empty($userinfo['selfqq']) || empty($userinfo['selfqqcode'])) {
            /*没有认证完成微信*/
            $this->assign('userinfo', $userinfo);
            return $this->fetch('pay_wechat');
        } else {
            $this->assign('userinfo', $userinfo);
            return $this->fetch('pay_wechatok');
        }
    }

    /*
     * 修改微信信息
     * */
    public function doawechat()
    {
        if (request()->isAjax()) {
            $result = array(
                's' => 0,
                'msg' => '绑定成功'
            );
            $flag = true;
            $uid = session("user_id");
            $userinfo = userallinfo($uid);
            if (empty($userinfo['selfqqname']) || empty($userinfo['selfqq']) || empty($userinfo['selfqqcode'])) {
                /*没有认证完成支付宝*/
                $info['selfqqname'] = remove_tag($_POST['selfqqname']);
                $info['selfqq'] = remove_tag($_POST['selfqq']);
                //七牛云上传图片
                $info['selfqqcode'] = $_POST['upload_geturl'];
                if ($flag) {
                    $s = Db::name('user')->where('id=' . $uid)->update($info);
                    if ($s) {
                        $result['s'] = 1;
                    } else {
                        $result['msg'] = "绑定失败";
                    }
                }
            } else {
                $result['s'] = 1;
                $result['msg'] = "请勿重复认证!";
            }
            echo json_encode($result);
            exit();
        }
    }

    /*
     * 安全中心
     * */
    public function safecenter()
    {
        $this->templateTitle("安全中心");
        return $this->fetch('safecenter');
    }

    /*
     * 安全中心之登录密码重置
     * */
    public function loginpwd()
    {
        $this->templateTitle("登录密码");
        $uid = session("user_id");
        $userinfo = userallinfo($uid);
        $this->assign('userinfo', $userinfo);
        return $this->fetch('loginpwd');
    }

    /*
     * 忘记密码发送验证码
     * */
    public function sendsms()
    {
        $uid = session("user_id");
        $user = userallinfo($uid);
        if (!empty($user)) {
            $tel = $user['selfphone'];
        } else {
            $tel = input('phone');
        }
        if (empty($tel)) {
            $this->ajaxError('手机号不能为空哦');
        }
        try {
            $sms = new Jhsms();
            $sms->send_sms($tel, 2);
            $this->ajaxSuccess("发送成功，请注意查收");
        } catch (\Exception $e) {
            $this->ajaxError($e->getMessage());
        }
    }

    /*
     *交易密码重置
     *
     **/
    public function tradepwd()
    {
        $this->templateTitle("交易密码");
        $uid = session("user_id");
        $userinfo = userallinfo($uid);
        $this->assign('userinfo', $userinfo);
        return $this->fetch('tradepwd');
    }

    /*
     * 重置登录密码/二次密码  ajax
     * */
    public function chagnesign()
    {
        $uid = session("user_id");
        $user = userallinfo($uid);
        $tel = $user['selfphone'];
        $codes = trim($_POST['codes']);
        $pwd = trim($_POST['pwds']);
        /*判断是修改登录密码还是支付密码，如果是登录密码，tel应该等于1001*/
        $type_code = trim($_POST['tel']);
        if ($type_code == 1001) {
            $data['passwd'] = md5(md5($pwd));
        } else {
            $data['sepasswd'] = md5(md5($pwd));
        }
        try {
            Jhsms::verifyCaptcha($tel, $codes, 2);
            $s = Db::name('user')->where("id='" . $uid . "'")->update($data);
            if ($s) {
                $this->ajaxSuccess("修改成功");
            } else {
                $this->ajaxError('修改失败');
            }
        } catch (\Exception $e) {
            $this->ajaxError($e->getMessage());
        }
    }

    //new_add
    public function new_add()
    {

        $this->templateTitle("收款方式");
        $uid = session("user_id");
        if (empty($uid)) {
            session('tips', '登录过期，请重新登录');
            $this->redirect("Index/login");
        }
        $userinfo = userallinfo($uid);
        $status = [
            "bank" => 1,
            "wechat" => 1,
            "ali" => 1
        ];
        if (empty($userinfo['realname']) || empty($userinfo['banktypename']) || empty($userinfo['bankname']) || empty($userinfo['bankcard'])) {
            /*没有认证完成银行卡*/
            $status["bank"] = 0;
        }
        if (empty($userinfo['emailname']) || empty($userinfo['email']) || empty($userinfo['emailcode'])) {
            /*没有认证完成支付宝*/
            $status["ali"] = 0;
        }
        if (empty($userinfo['selfqqname']) || empty($userinfo['selfqq']) || empty($userinfo['selfqqcode'])) {
            /*没有认证完成微信*/
            $status['wechat'] = 0;
        }
        $this->assign('status', $status);
        return $this->fetch('new_add');
    }

}