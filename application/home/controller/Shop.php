<?php
/**
 * 矿机
 * Created by atom.
 * User:tsingStar
 * Date: 2020-09-06
 * Time: 08:16:20
 */

namespace app\home\controller;


use app\common\model\Goods;
use app\common\model\Jhsms;
use app\common\model\User;
use think\Exception;

class Shop extends Common
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 商城首页
     */
    public function index()
    {

        //获取首页轮播图
        $swiper_list = db("swiper")->order("display_order desc")->select();
        $this->assign("swiper_list", $swiper_list);

        //导航按钮组
        $nav_list = db("nav")->where('status', 1)->order("display_order desc")->select();
        $this->assign('nav_list', $nav_list);
        //查询商品信息
        $good_list = db("shop_goods")->order('sort desc')->select();
        $this->assign('good_list', $good_list);

        return $this->fetch("index");
    }

    //矿机兑换 xuanfeng 2020-08-04 00:21
    public function getshop(){
        ob_start();
        header("Content-type:text/html;charset=utf-8");
        ini_set('memory_limit', '88M');
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Asia/Shanghai');

        //开始生成每日的日期
        $datestr = date('Y-m-d');
        //得到开始时间
        $firstday = (strtotime($datestr)+86400);

        $good_id = input('id');
        //必须登录
        $user_id = session("user_id");
        if (empty($user_id)) {
            $this->ajaxError("请先登录！");
            exit;
        }
        //获取该矿机所有信息并判断user的SCB够不够
        $shopinfo = db("shop_goods")->where('id="'.$good_id.'" and isshow=1')->find();
        if(empty($shopinfo)){
            $this->ajaxError("该矿机不存在！");
            exit;
        }
        $userinfo = userallinfo($user_id);
        if($shopinfo['price']>$userinfo['mybot']){
            $this->ajaxError("SCB余额不足！");
            exit;
        }
        //print_r($shopinfo);exit;
        //查看一下这个人买了多少次，因为有购买限制
        $num_buy_user = db('shopmy')->where("uid='".$userinfo['id']."' and proid='".$shopinfo['id']."'")->count();

        if(!empty($num_buy_user)){
          if($num_buy_user>=$shopinfo['buynum']){
            $this->ajaxError("受限，该产品最多可购买：".$shopinfo['buynum']."次！");
            exit;
          }
        }
        //echo 444;exit;
        //开始给矿机购买并入库
        unset($shopinfo['buynum']);

        unset($shopinfo['sort']);
        unset($shopinfo['isshow']);
        $shopinfo['uid'] = $user_id;
        $shopinfo['proid'] = $shopinfo['id'];
        unset($shopinfo['id']);
        $shopinfo['buytime'] = time();
        $s = db('shopmy')->insert($shopinfo);
        $sid=db('shopmy')->getLastInsID();
        if($s){
            //先减去会员的SCB
            db('user')->where("id='".$user_id."'")->setDec('mybot',$shopinfo['price']);
            mybot_his($user_id,2,$shopinfo['price'],0,"购买矿机".$shopinfo['title']);
            //生成每日赠送记录
            //第一天时间戳：
            $that_time = $firstday;
            for($i=1;$i<=$shopinfo['daynum'];$i++){
                $data['shopid']  = $sid;
                $data['uid']     = $user_id;
                $data['sendtime']= $that_time;
                $data['state']   = 1;//1未计算2已计算
                $data['dayearn'] = $shopinfo['dayearn'];
                db('shopmylist')->insert($data);
                $that_time = ($that_time+86400);
                usleep(3000);
                //0.02秒执行一次，防止太快服务器卡死
            }
            $this->ajaxSuccess('购买成功！');
        }else{
           $this->ajaxError("当前用户较多，请稍后再试！");
        }

    }







    //


}
