<?php
/**
 * 共享商城
 * User:tsingStar
 * Date: 2020-09-06
 * Time: 08:16:20
 */

namespace app\home\controller;

use app\common\model\Goodssell;

class Market extends Common
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 商城首页
     */
    public function index()
    {

        //获取首页轮播图
        $swiper_list = db("swiper")->where('is_visible', 1)->order("display_order desc")->select();

        foreach ($swiper_list as &$item) {
            if($item['type'] == 2){
                $url = url('Market/goodList', ['cate_id'=>$item['link_url']]);
            }elseif ($item['type'] == 4){
                $url = url('Market/detail', ['id'=>$item['link_url']]);
            }else{
                $url = $item['link_url'];
            }
            $item['url'] = $url;
        }
        unset($item);
        $this->assign("swiper_list", $swiper_list);

        //导航按钮组
        $nav_list = db("nav")->where('is_visible', 1)->order("display_order desc")->select();

        foreach ($nav_list as &$item1) {
            if($item1['type'] == 2){
                $url = url('Market/goodList', ['cate_id'=>$item1['link_url']]);
            }elseif ($item1['type'] == 4){
                $url = url('Market/detail', ['id'=>$item1['link_url']]);
            }else{
                $url = $item1['link_url'];
            }
            $item1['url'] = $url;
        }
        unset($item1);
        $this->assign('nav_list', $nav_list);
        //查询商品信息
        $good_list = db("sell_goods")->where('status', 1)->where('is_recommend', 1)->order('display_order desc')->select();
        $this->assign('good_list', $good_list);

        return $this->fetch("index");
    }

    /**
     * 商品列表
     */
    public function goodList()
    {
        if(request()->isAjax()){
            $cate_id = input('cate_id');
            $page = input('page', 1);
            $m = db('sell_goods');
            if($cate_id){
                $m->where('category_id1|category_id2', $cate_id);
            }
            $list = $m->order('display_order desc')->page($page, 10)->select();
            foreach ($list as &$item)
                $item['url'] = url('Market/detail', ['id'=>$item['id']]);
            unset($item);
            $this->ajaxSuccess('请求成功', $list);
        }
        $cate_id = input('cate_id', 0);
        $this->assign('cate_id', $cate_id);
        return $this->fetch();
    }

    /**
     * 商品详情
     * @param $id
     */
    public function detail()
    {
        try{
            if(request()->isGet()){
                $id = intval(input('id'));
                $uid = $this->currentUserId();
                $goods = Goodssell::goodsInfo($id,$uid,1);
                $this->templateTitle($goods['title']);
                $this->assign('goods',$goods);
                return $this->fetch();
            }
        }catch (\Exception $exception){
            global $wechat_has_exception;
            $wechat_has_exception = true;
            if(request()->isAjax()){
                $this->ajaxError($exception->getMessage());
            }else{
                $this->error($exception->getMessage());
            }
        }
    }

}
