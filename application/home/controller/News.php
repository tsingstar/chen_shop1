<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2019/8/2
 * Time: 17:39
 */

namespace app\home\controller;
use app\common\model\User as umodel;
use think\Db;

class News extends Common
{

    protected function _initialize()
    {
        parent::_initialize();
    }
    /**
     * 系统消息
     */
    public function index()
    {
        $this->templateTitle("系统消息");
	    $info = Db::name( 'news' );
	    $map['status']=1;
	    $list = $info->where ( $map )->order ( 'display_order desc,id desc' )->paginate(10);
	    $this->assign ( 'list', $list );
        return $this->fetch("index");
    }
	/*ajax 加载系统公告*/
	public function ajaxmore(){
		$page = $_POST['page'];/*当前是第几页*/
		$page_num = 5;/*每次读取5条*/
		$limit_first = ($page-1)*$page_num;
		$limit_end   = $page_num;
		
		$map['status']=1;
		$list =Db::name('news')->where ($map)->field('id,title')->order('display_order desc,id desc')->limit ($limit_first,$limit_end)->select();
		if(!empty($list)){
			$ary = $list;
		}else{
			$ary = [];
		}
		echo json_encode($ary);exit();
	}
	
	/*新闻详情*/
	public function details(){
		$this->templateTitle("系统消息");
		$caution = Db::name('news')->where(array('id'=>input('id'),'status'=>1))->find();
		if(empty($caution)){
			session('tips','非法参数');
			$this->redirect("News/index");
		}
		$this->assign('vo',$caution);
		$this->assign('title',"系统消息");
		return $this->fetch("details");
	}
	/*
	 * 联系客服
	 * */
	public function customer(){
		$ty = input('ty');
		if($ty==1){
			$title = '系统消息';
			$this->templateTitle("服务中心详情");
		}else{
			$title = '客服';
			$this->templateTitle("客服详情");
		}
		$this->assign('title',$title);
		
		$caution = Db::name('service')->where(array('type'=>$ty))->find();
		if(empty($caution)){
			session('tips','非法参数');
			$this->redirect("User/index");
		}
		$caution['create_time']='';
	
		$this->assign('vo',$caution);
		$this->assign('title',$title);
		return $this->fetch("details");
	}
}