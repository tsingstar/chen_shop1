<?php
/**
 * Created by PhpStorm.
 * User:tsingStar
 * Date: 2020/9/8
 * Time: 16:44
 */

namespace app\home\controller;


use app\common\model\Lottery;
use think\Db;

class Zhuan extends Common
{
    protected function _initialize()
    {
        parent::_initialize();

    }

    //抽奖
    public function lottery(){
        $this->templateTitle('抽奖');
        $this->assign('user',$this->currentUser());
//        $list = Db::name('lottery_list')
//            ->alias('l')
//            ->join("__USER__ u",'u.id=l.uid')
//            ->field('l.title,u.username,u.selfphone')
//            ->order('l.id DESC')
//            ->limit(14)
//            ->select();
//        $this->assign('list',$list);
        $goods = Db::name('lottery')
            ->where('is_visible',1)
            ->field('id,title, img_url,price_title')
            ->order('id')
            ->select();
        $this->assign('goods',json_encode($goods));
        $this->assign('ori_goods',$goods);
        $color = [];
        foreach ($goods as $item){
            array_push($color, '#4addff');
        }
        $this->assign('good_color', json_encode($color));
        $this->assign('user',$this->currentUser());
        return $this->fetch();
    }

    public function getLottery(){
        if(request()->isAjax()){
            try{
                $user = $this->currentUser();
//                $r = \db('lottery_list')->where('uid', $user['id'])->where('create_time', '>', strtotime(date('Y-m-d')))->find();
                if($user['mybot']<config('setting.lottery')){
                    $this->ajaxError('SCB数量不足');
                }
                \db('user')->where('id', $user['id'])->setDec('mybot', config('setting.lottery'));
                \db('mybot')->insert([
                    'uid'=>$user['id'],
                    'isadd'=>2,
                    'nums'=>config('setting.lottery'),
                    'ctime'=>now_datetime(),
                    'info'=>'大转盘抽奖消耗'
                ]);
//                if(!empty($r)){
//                    $this->ajaxError('今日抽奖次数已达上限');
//                }
                $res = Lottery::lottery($user);
                $this->ajaxSuccess('',$res);
            }catch (\Exception $exception){
                global $wechat_has_exception;
                $wechat_has_exception = true;
                $this->ajaxError($exception->getMessage());
            }
        }
    }

    //抽奖记录
    public function lotteryLists(){
        if(request()->isAjax()){
            $p = intval(input('page', 1));
            $p = empty($p)?1:$p;
            $list = Db::name('lottery_list')
                ->where('uid',$this->currentUserId())
                ->order('id DESC')
                ->page($p,config('ajax_num'))
                ->select();
            foreach ($list as &$item){
                $item['create_time'] = accurateDate($item['create_time']);
                if($item['status']==0){
                    if($item['goods_title'] == '谢谢参与'){
                        $item['a'] = '<a href="javascript:void(0);" class="a1">谢谢参与</a>';
                    }else{
                        $item['a'] = '<a href="'.url('Zhuan/lotteryReceive',['id'=>$item['id']]).'" style="color: red" class="a1">领取</a>';
                    }
                }else{
                    $item['a'] = '<a href="javascript:void(0);" class="a1">已领取</a>';
                }
            }
            unset($item);
            $this->ajaxSuccess('',$list);
        }else{
            $this->templateTitle('中奖记录');
            return $this->fetch();
        }
    }

    //领奖
    public function lotteryReceive(){
        $id = intval(input('id'));
        $info = Db::name('lottery_list')
            ->where('id',$id)
            ->where('uid',$this->currentUserId())
            ->find();
        if(request()->isAjax()){
            try{
                if(!$info){
                    $this->ajaxError('无此中奖记录或不属于您无权操作');
                }
                if($info['status']!=0){
                    $this->ajaxError('已领取勿重复领取');
                }
                $name = trim(input('name'));
                $mobile = trim(input('mobile'));
                $address = trim(input('address'));
                if(empty($name)){
                    $this->ajaxError('请输入收货人称呼');
                }
                if(empty($mobile)){
                    $this->ajaxError('请输入手机号');
                }
                if(empty($address)){
                    $this->ajaxError('请输入收货地址');
                }
                $data = [
                    'status'=>1,
                    'status_time'=>now_datetime(),
                    'send_name'=>$name,
                    'send_mobile'=>$mobile,
                    'send_address'=>$address
                ];
                $save = Db::name('lottery_list')
                    ->where('id',$id)
                    ->update($data);
                if(!$save){
                    $this->ajaxError('服务器繁忙，请稍后再试');
                }
                $this->ajaxSuccess('领取成功');
            }catch (\Exception $exception){
                global $wechat_has_exception;
                $wechat_has_exception = true;
                $this->ajaxError($exception->getMessage());
            }
        }else{
            $this->templateTitle('领取奖品');
            if(!$info){
                $this->showError('无此中奖记录或不属于您无权操作');
            }
            if($info['status']!=0){
                $this->showError('已领取勿重复领取');
            }
            return $this->fetch();
        }
    }



}