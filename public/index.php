<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// [ 应用入口文件 ]

// 定义应用目录
define('APP_PATH', __DIR__ . '/../application/');

//启用模块绑定
//$module = explode(".", $_SERVER["SERVER_NAME"])[0];
//if($module=='sadmin'){
//    define("BIND_MODULE", 'admin');
//}elseif($module=='sapi'){
//    define("BIND_MODULE", 'api');
//}elseif($module=='swechat'){
//    define("BIND_MODULE", 'wechat');
//}else{
//    define("BIND_MODULE", $module);
//}
// 加载框架引导文件
require __DIR__ . '/../thinkphp/start.php';
