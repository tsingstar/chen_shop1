// 瀑布流
$(function () {
    //jQuery代码
    imgLocation()
    function imgLocation() {
        // 选中瀑布流的元素
        var box = $('.flow_box')
        var num = Math.floor($(window).width() / box.eq(0).width())
        var boxHeights = []
        box.each(function (index, value) {
            var boxHeight = box.eq(index).height()
            if (index < num) {
                boxHeights[index] = boxHeight
            } else {
                var minHeight = Math.min.apply(null, boxHeights)
                var minIndex = $.inArray(minHeight, boxHeights)
                $(value).css({
                    'position': 'absolute',
                    'top': minHeight,
                    'left': box.eq(minIndex).position().left
                });
                boxHeights[minIndex] += box.eq(index).height()
            }
        })
    }
})