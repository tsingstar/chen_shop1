var modal = {};
modal.msg = function (msg){
    layer.msg(msg);
};
modal.ajax = function (url,data,successCallBack,errorCallBack) {
    var loadingTag = layer.load();
    $.ajax({
        type:'post',
        url:url,
        data:data,
        cache:false,
        dataType:'JSON',
        success:function(res){
            if(res.result_code=='-1'){
                layer.msg(res.result_info);
                setTimeout(function () {
                    window.location.href=res.result_data.url;
                },1000);
            }else{
                successCallBack(res);
            }
            layer.close(loadingTag);
        },
        error:function(e){
            layer.close(loadingTag);
            if(typeof errorCallBack =='function'){
                errorCallBack(e);
            }
        }
    });
};